package com.arthvedi.organic.requests.service;

import java.util.List;

import com.arthvedi.organic.requests.domain.Request;

/*
 *@Author varma
 */
public interface IRequestService {

	Request create(Request request);

	void deleteRequest(String requestId);

	Request getRequest(String requestId);

	List<Request> getAll();

	Request updateRequest(Request request);

	List<Request> getRequestList();
}
