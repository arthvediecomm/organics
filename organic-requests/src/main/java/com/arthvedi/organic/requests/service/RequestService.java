package com.arthvedi.organic.requests.service;

import static com.arthvedi.organic.enums.Status.ACTIVE;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.requests.domain.Request;
import com.arthvedi.organic.requests.domain.RequestAttributes;
import com.arthvedi.organic.requests.domain.RequestStatusCodes;
import com.arthvedi.organic.requests.repository.RequestRepository;

/*
 *@Author varma
 */
@Component
public class RequestService implements IRequestService {
	@Autowired
	private IRequestAttributesService requestAttributesService;
	@Autowired
	private RequestRepository requestRepository;
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private IRequestStatusCodesService requestStatusCodesService;

	@Override
	public Request create(Request request) {
		request.setUserCreated("VARMA");
		request.setCreatedDate(new LocalDateTime());
		 request.setStatus(ACTIVE.name());
		request = requestRepository.save(request);
		if (request.getId() != null
				&& CollectionUtils.isNotEmpty(request.getRequestAttributeses())) {
			request.setRequestAttributeses(addRequestAttributes(request,
					request.getRequestAttributeses()));
		}
		return request;
	}

	@Transactional
	private Set<RequestAttributes> addRequestAttributes(Request request,
			Set<RequestAttributes> requestAttributeses) {
		Set<RequestAttributes> requestAttributses = new HashSet<RequestAttributes>();
		for (RequestAttributes rqtA : requestAttributeses) {
			RequestAttributes reqTAttr = rqtA;
			reqTAttr.setRequest(request);
			reqTAttr = requestAttributesService.create(reqTAttr);
			requestAttributses.add(reqTAttr);
		}
		return requestAttributses;
	}

	@Override
	@Transactional
	public void deleteRequest(String requestId) {
		Request request = requestRepository.findOne(requestId);
		requestRepository.delete(request);

	}

	/*
	 * @Override public Request getRequest(String requestId) { return
	 * requestRepository.findOne(requestId); }
	 */

	@Override
	public List<Request> getAll() {
		return null;
	}

	@Override
	public Request updateRequest(Request request) {
		Request req = requestRepository.findOne(request.getId());
		try {
			nonNullBeanUtils.copyProperties(req, request);
		} catch (IllegalAccessException e) {
			e.printStackTrace();

		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		req.setModifiedDate(new LocalDateTime());
		req.setUserModified("Admin");

		request = requestRepository.save(req);
		if (request.getStatus() != null
				&& request.getRequestStatusCodes() != null) {
			Set<RequestStatusCodes> requestStatusCodes = new HashSet<RequestStatusCodes>();
			for (RequestStatusCodes rsc : request.getRequestStatusCodes()) {
				RequestStatusCodes reqSc = new RequestStatusCodes();
				reqSc.setAssignee(rsc.getAssignee());
				reqSc.setDescription(rsc.getDescription());
				reqSc.setRequest(request);
				reqSc.setStatus(rsc.getStatus());
				reqSc.setUserCreated(rsc.getUserCreated());
				reqSc.setCreatedDate(new LocalDateTime());
				reqSc = requestStatusCodesService.create(reqSc);
				requestStatusCodes.add(reqSc);
			}
			request.setRequestStatusCodes(requestStatusCodes);
		}

		return request;
	}

	@Override
	public List<Request> getRequestList() {
		List<Request> requests = new ArrayList<Request>();
		List<Request> reqsts = (List<Request>) requestRepository.findAll();
		for (Request req : reqsts) {
			Request rq = new Request();
			rq.setId(req.getId());
			rq.setRequestTypeName(req.getRequestTypeName());
			rq.setPriority(req.getPriority());
			rq.setRequestDetails(req.getRequestDetails());
			rq.setRequestMessage(req.getRequestMessage());
			rq.setStatus(req.getStatus());
			rq.setUserCreated(req.getUserCreated());
			rq.setUserModified(req.getUserModified());
			rq.setUserType(req.getUserType());
			// rq.setBnvUser(req.getBnvUser());
			requests.add(rq);
		}
		return requests;
	}

	@Override
	public Request getRequest(String requestId) {
		// TODO Auto-generated method stub
		return null;
	}

}
