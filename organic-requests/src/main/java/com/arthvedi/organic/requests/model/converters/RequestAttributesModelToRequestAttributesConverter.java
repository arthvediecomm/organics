/**
 *
 */
package com.arthvedi.organic.requests.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.requests.domain.RequestAttributes;
import com.arthvedi.organic.requests.model.RequestAttributesModel;

/**
 * @author Jay
 *
 */
@Component("requestAttributesModelToRequestAttributesConverter")
public class RequestAttributesModelToRequestAttributesConverter implements
		Converter<RequestAttributesModel, RequestAttributes> {
	@Autowired
	private ObjectFactory<RequestAttributes> requestAttributesFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public RequestAttributes convert(final RequestAttributesModel source) {
		RequestAttributes requestAttributes = requestAttributesFactory
				.getObject();
		BeanUtils.copyProperties(source, requestAttributes);

		return requestAttributes;
	}

	@Autowired
	public void setRequestAttributesFactory(
			final ObjectFactory<RequestAttributes> requestAttributesFactory) {
		this.requestAttributesFactory = requestAttributesFactory;
	}

}
