package com.arthvedi.organic.requests.service;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.requests.domain.RequestType;
import com.arthvedi.organic.requests.domain.RequestTypeAttribute;
import com.arthvedi.organic.requests.repository.RequestTypeRepository;

/*
*@Author varma
*/
@Component
public class RequestTypeService implements IRequestTypeService {
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private RequestTypeRepository requestTypeRepository;
	@Autowired
	private IRequestTypeAttributeService requestTypeAttributeService;

	@Override
	@Transactional
	public RequestType create(RequestType requestType) {

		requestType.setUserCreated("VARMA");
		requestType.setCreatedDate(new LocalDateTime());
		//requestType.setStatus(ACTIVE.name());
		requestType = requestTypeRepository.save(requestType);
		if (requestType.getId() != null && CollectionUtils.isNotEmpty(requestType.getRequestTypeAttributes())) {
			requestType.setRequestTypeAttributes(
					addRequestTypeAttributes(requestType, requestType.getRequestTypeAttributes()));
		}
		return requestType;
	}

	@Transactional
	private Set<RequestTypeAttribute> addRequestTypeAttributes(RequestType requestType,
			Set<RequestTypeAttribute> requestTypeAttributes) {
		Set<RequestTypeAttribute> requestTypeAttributeses = new HashSet<RequestTypeAttribute>();
		for (RequestTypeAttribute rqtA : requestTypeAttributes) {
			RequestTypeAttribute reqTAttr = rqtA;
			reqTAttr.setRequestType(requestType);
			reqTAttr = requestTypeAttributeService.create(reqTAttr);
			requestTypeAttributeses.add(reqTAttr);
		}
		return requestTypeAttributeses;
	}

	@Override
	@Transactional
	public void deleteRequestType(String requestTypeId) {
		RequestType requestType = requestTypeRepository
				.findOne(requestTypeId);
		requestTypeRepository.delete(requestType);
	}

	@Override
	public RequestType getRequestType(String requestTypeId) {
		return requestTypeRepository.findOne(requestTypeId);
	}

	@Override
	public List<RequestType> getAll() {
		return (List<RequestType>) requestTypeRepository.findAll();
	}

	@Override
	@Transactional
	public RequestType updateRequestType(RequestType requestType) {
		RequestType reqType = requestTypeRepository.findOne(requestType.getId());
		try {
			nonNullBeanUtils.copyProperties(reqType, requestType);
		} catch (IllegalAccessException e) {
			e.printStackTrace();

		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		reqType.setModifiedDate(new LocalDateTime());
		reqType.setUserModified("Admin");
		if (requestType.getRequestTypeAttributes() != null) {
			requestType.setRequestTypeAttributes(
					addRequestTypeAttributes(requestType, requestType.getRequestTypeAttributes()));
	
		}
		requestType = requestTypeRepository.save(reqType);
		return requestType;
	}

	@Override
	public List<RequestType> getRequestTypeList() {
		List<RequestType> requestTypes = new ArrayList<RequestType>();
		List<RequestType> requestType = (List<RequestType>) requestTypeRepository.findAll();
		for(RequestType rt : requestType){
			RequestType reqType = new RequestType();
			reqType.setId(rt.getId());
			reqType.setName(rt.getName());
			reqType.setPriority(rt.getPriority());
			//reqType.setDescription(rt.getDescription());
			reqType.setStatus(rt.getStatus());
			reqType.setCreatedDate(rt.getCreatedDate());
			requestTypes.add(reqType);
		}
		return requestTypes;
	}

}
