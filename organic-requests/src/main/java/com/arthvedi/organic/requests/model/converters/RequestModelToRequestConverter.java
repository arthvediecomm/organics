/**
 *
 */
package com.arthvedi.organic.requests.model.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.organic.requests.domain.Request;
import com.arthvedi.organic.requests.domain.RequestAttributes;
import com.arthvedi.organic.requests.domain.RequestStatusCodes;
import com.arthvedi.organic.requests.model.RequestModel;

/**
 * @author Jay
 *
 */
@Component("requestModelToRequestConverter")
public class RequestModelToRequestConverter implements
		Converter<RequestModel, Request> {
	@Autowired
	private ObjectFactory<Request> requestFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public Request convert(final RequestModel source) {
		Request request = requestFactory.getObject();
		BeanUtils.copyProperties(source, request);
		/*
		 * if (source.getBnvUserId() != null) { BnvUser bnvUser = new BnvUser();
		 * bnvUser.setId(source.getBnvUserId()); request.setBnvUser(bnvUser); }
		 */

		if (CollectionUtils.isNotEmpty(source.getRequestAttributesModels())) {
			List<RequestAttributes> converted = (List<RequestAttributes>) conversionService
					.convert(source.getRequestAttributesModels(),
							TypeDescriptor.forObject(source
									.getRequestAttributesModels()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									RequestAttributes.class));
			request.getRequestAttributeses().addAll(converted);
		}
		if (CollectionUtils.isNotEmpty(source.getRequestStatusCodesModels())) {
			List<RequestStatusCodes> converted = (List<RequestStatusCodes>) conversionService
					.convert(source.getRequestStatusCodesModels(),
							TypeDescriptor.forObject(source
									.getRequestStatusCodesModels()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									RequestStatusCodes.class));
			request.getRequestStatusCodes().addAll(converted);
		}
		return request;
	}

	@Autowired
	public void setRequestFactory(final ObjectFactory<Request> requestFactory) {
		this.requestFactory = requestFactory;
	}

}
