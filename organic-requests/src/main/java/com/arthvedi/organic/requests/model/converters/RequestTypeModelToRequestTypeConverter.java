/**
 *
 */
package com.arthvedi.organic.requests.model.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.organic.requests.domain.RequestType;
import com.arthvedi.organic.requests.domain.RequestTypeAttribute;
import com.arthvedi.organic.requests.model.RequestTypeModel;

/**
 * @author Jay
 *
 */
@Component("requestTypeModelToRequestTypeConverter")
public class RequestTypeModelToRequestTypeConverter implements Converter<RequestTypeModel, RequestType> {
	@Autowired
	private ObjectFactory<RequestType> requestTypeFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public RequestType convert(final RequestTypeModel source) {
		RequestType requestType = new RequestType();
		BeanUtils.copyProperties(source, requestType);
		if (CollectionUtils.isNotEmpty(source.getRequestTypeAttributeModels())) {
			List<RequestTypeAttribute> converted = (List<RequestTypeAttribute>) conversionService.convert(
					source.getRequestTypeAttributeModels(),
					TypeDescriptor.forObject(source.getRequestTypeAttributeModels()),
					CollectionTypeDescriptor.forType(TypeDescriptor.valueOf(List.class), RequestTypeAttribute.class));
			requestType.getRequestTypeAttributes().addAll(converted);
		}
		return requestType;
	}

	@Autowired
	public void setRequestTypeFactory(final ObjectFactory<RequestType> requestTypeFactory) {
		this.requestTypeFactory = requestTypeFactory;
	}

}
