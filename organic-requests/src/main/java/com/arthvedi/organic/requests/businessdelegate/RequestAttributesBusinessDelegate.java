package com.arthvedi.organic.requests.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.requests.businessdelegate.context.RequestAttributesContext;
import com.arthvedi.organic.requests.domain.RequestAttributes;
import com.arthvedi.organic.requests.model.RequestAttributesModel;
import com.arthvedi.organic.requests.service.IRequestAttributesService;

/*
 *@Author varma
 */

@Service
public class RequestAttributesBusinessDelegate
		implements
		IBusinessDelegate<RequestAttributesModel, RequestAttributesContext, IKeyBuilder<String>, String> {

	@Autowired
	private IRequestAttributesService requestAttributesService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public RequestAttributesModel create(RequestAttributesModel model) {
		validateModel(model);
		RequestAttributes reqattr = requestAttributesService
				.create((RequestAttributes) conversionService.convert(model,
						forObject(model), valueOf(RequestAttributes.class)));
		model = convertToRequestAttributesModel(reqattr);
		model.setStatusMessage("SUCCESS::: RequestAttributes with name "
				+ model.getRequestAttributeName() + " created successfully");
		return model;
	}

	private RequestAttributesModel convertToRequestAttributesModel(
			RequestAttributes reqattr) {
		return (RequestAttributesModel) conversionService.convert(reqattr,
				forObject(reqattr), valueOf(RequestAttributesModel.class));
	}

	private void validateModel(RequestAttributesModel model) {
		Validate.notNull(model, "Invalid Input");
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			RequestAttributesContext context) {

	}

	@Override
	public RequestAttributesModel edit(IKeyBuilder<String> keyBuilder,
			RequestAttributesModel model) {
		RequestAttributes requestAttributes = requestAttributesService
				.getRequestAttributes(keyBuilder.build().toString());

		requestAttributes = requestAttributesService
				.updateRequestAttributes(requestAttributes);

		return model;
	}

	@Override
	public RequestAttributesModel getByKey(IKeyBuilder<String> keyBuilder,
			RequestAttributesContext context) {
		RequestAttributes requestAttributes = requestAttributesService
				.getRequestAttributes(keyBuilder.build().toString());
		RequestAttributesModel model = conversionService.convert(
				requestAttributes, RequestAttributesModel.class);
		return model;
	}

	@Override
	public Collection<RequestAttributesModel> getCollection(
			RequestAttributesContext context) {
		List<RequestAttributes> requestAttributes = new ArrayList<RequestAttributes>();

		List<RequestAttributesModel> requestAttributesModels = (List<RequestAttributesModel>) conversionService
				.convert(requestAttributes, TypeDescriptor
						.forObject(requestAttributes), TypeDescriptor
						.collection(List.class, TypeDescriptor
								.valueOf(RequestAttributesModel.class)));
		return requestAttributesModels;
	}

	@Override
	public RequestAttributesModel edit(IKeyBuilder<String> keyBuilder,
			RequestAttributesModel model, RequestAttributesContext context) {
		return null;
	}

}
