package com.arthvedi.organic.requests.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.requests.businessdelegate.context.RequestContext;
import com.arthvedi.organic.requests.model.RequestModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/request", produces = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class RequestController {

	private IBusinessDelegate<RequestModel, RequestContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<RequestContext> requestContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = { "/create",
			"/sellerapp/create" }, consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<RequestModel> createRequest(
			@RequestBody RequestModel requestModel) {
		requestModel = businessDelegate.create(requestModel);
		return new ResponseEntity<RequestModel>(requestModel,
				HttpStatus.CREATED);
	}

	@RequestMapping(value = { "/{id}/edit", "/admin/{id}/edit" }, method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<RequestModel> edit(
			@PathVariable(value = "id") final String requestId,
			@RequestBody RequestModel requestModel) {
		requestModel = businessDelegate.edit(keyBuilderFactory.getObject()
				.withId(requestId), requestModel);
		return new ResponseEntity<RequestModel>(requestModel,
				HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}/delete", consumes = { MediaType.ALL_VALUE })
	public void deleteRequest(@PathVariable(value = "id") final String requestId) {
		RequestContext requestContext = requestContextFactory.getObject();
		requestContext.setRequestId(requestId);
		businessDelegate
				.delete(keyBuilderFactory.getObject().withId(requestId),
						requestContext);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<RequestModel> getRequest(
			@PathVariable(value = "id") final String requestId) {
		RequestContext requestContext = requestContextFactory.getObject();
		RequestModel model = businessDelegate.getByKey(keyBuilderFactory
				.getObject().withId(requestId), requestContext);
		return new ResponseEntity<RequestModel>(model, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = { "/requestlist",
			"/admin/requestlist" })
	public ResponseEntity<IModelWrapper<Collection<RequestModel>>> getAllRequestList() {
		RequestContext requestContext = requestContextFactory.getObject();
		requestContext.setRequestList("requestList");
		Collection<RequestModel> requestModels = businessDelegate
				.getCollection(requestContext);
		IModelWrapper<Collection<RequestModel>> models = new CollectionModelWrapper<RequestModel>(
				RequestModel.class, requestModels);
		return new ResponseEntity<IModelWrapper<Collection<RequestModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = {
			"/{bnvUserId}/requestlist", "/sellerapp/{bnvUserid}/requestlist" })
	public ResponseEntity<IModelWrapper<Collection<RequestModel>>> getAllRequestList(
			@PathVariable(value = "bnvUserId") final String bnvUserId) {
		RequestContext requestContext = requestContextFactory.getObject();
		requestContext.setRequestList("requestList");
		Collection<RequestModel> requestModels = businessDelegate
				.getCollection(requestContext);
		IModelWrapper<Collection<RequestModel>> models = new CollectionModelWrapper<RequestModel>(
				RequestModel.class, requestModels);
		return new ResponseEntity<IModelWrapper<Collection<RequestModel>>>(
				models, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "requestBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<RequestModel, RequestContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setRequestObjectFactory(
			final ObjectFactory<RequestContext> requestContextFactory) {
		this.requestContextFactory = requestContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(
			final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
