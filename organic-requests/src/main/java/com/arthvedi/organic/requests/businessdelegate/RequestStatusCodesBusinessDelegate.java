package com.arthvedi.organic.requests.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.requests.businessdelegate.context.RequestStatusCodesContext;
import com.arthvedi.organic.requests.domain.RequestStatusCodes;
import com.arthvedi.organic.requests.model.RequestStatusCodesModel;
import com.arthvedi.organic.requests.service.IRequestStatusCodesService;

/*
*@Author varma
*/

@Service
public class RequestStatusCodesBusinessDelegate
		implements IBusinessDelegate<RequestStatusCodesModel, RequestStatusCodesContext, IKeyBuilder<String>, String> {

	@Autowired
	private IRequestStatusCodesService requestStatusCodesService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public RequestStatusCodesModel create(RequestStatusCodesModel model) {
		validateModel(model);
		RequestStatusCodes reqStatusCodes = requestStatusCodesService
					.create((RequestStatusCodes) conversionService.convert(model, forObject(model), valueOf(RequestStatusCodes.class)));
			model = convertToRequestStatusCodesModel(reqStatusCodes);
			model.setStatusMessage("SUCCESS::: Request created successfully");
		return model;	}

	private RequestStatusCodesModel convertToRequestStatusCodesModel(RequestStatusCodes reqStatusCodes) {
		return (RequestStatusCodesModel) conversionService.convert(reqStatusCodes, forObject(reqStatusCodes), valueOf(RequestStatusCodesModel.class));
	}

	private void validateModel(RequestStatusCodesModel model) {
		Validate.notNull(model, "Invalid Input");
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, RequestStatusCodesContext context) {

	}

	@Override
	public RequestStatusCodesModel edit(IKeyBuilder<String> keyBuilder, RequestStatusCodesModel model) {
		RequestStatusCodes requestStatusCodes = requestStatusCodesService.getRequestStatusCodes(keyBuilder.build().toString());
		
		requestStatusCodes = requestStatusCodesService.updateRequestStatusCodes(requestStatusCodes);
		
		return model;
	}

	@Override
	public RequestStatusCodesModel getByKey(IKeyBuilder<String> keyBuilder, RequestStatusCodesContext context) {
		RequestStatusCodes requestStatusCodes = requestStatusCodesService.getRequestStatusCodes(keyBuilder.build().toString());
		RequestStatusCodesModel model = conversionService.convert(requestStatusCodes, RequestStatusCodesModel.class);
		return model;
	}

	@Override
	public Collection<RequestStatusCodesModel> getCollection(RequestStatusCodesContext context) {
		List<RequestStatusCodes> requestStatusCodes = new ArrayList<RequestStatusCodes>();
		
		List<RequestStatusCodesModel> requestStatusCodesModels = (List<RequestStatusCodesModel>) conversionService.convert(
				requestStatusCodes, TypeDescriptor.forObject(requestStatusCodes),
				TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(RequestStatusCodesModel.class)));
		return requestStatusCodesModels;
	}

	@Override
	public RequestStatusCodesModel edit(IKeyBuilder<String> keyBuilder,
			RequestStatusCodesModel model, RequestStatusCodesContext context) {
		return null;
	}

	

}
