package com.arthvedi.organic.requests.model.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.organic.requests.domain.Request;
import com.arthvedi.organic.requests.model.RequestAttributesModel;
import com.arthvedi.organic.requests.model.RequestModel;

@Component("requestToRequestModelConverter")
public class RequestToRequestModelConverter
        implements Converter<Request, RequestModel> {
    @Autowired
    private ObjectFactory<RequestModel> requestModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public RequestModel convert(final Request source) {
        RequestModel requestModel = requestModelFactory.getObject();
        BeanUtils.copyProperties(source, requestModel);
        if (CollectionUtils.isNotEmpty(source.getRequestAttributeses())) {
			List<RequestAttributesModel> converted = (List<RequestAttributesModel>) conversionService.convert(
					source.getRequestAttributeses(), TypeDescriptor.forObject(source.getRequestAttributeses()),
					CollectionTypeDescriptor.forType(TypeDescriptor.valueOf(List.class), RequestAttributesModel.class));
			requestModel.getRequestAttributesModels().addAll(converted);
		}
        return requestModel;
    }

    @Autowired
    public void setRequestModelFactory(
            final ObjectFactory<RequestModel> requestModelFactory) {
        this.requestModelFactory = requestModelFactory;
    }
}
