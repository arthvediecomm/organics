package com.arthvedi.organic.requests.model.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.organic.requests.domain.RequestType;
import com.arthvedi.organic.requests.model.RequestTypeAttributeModel;
import com.arthvedi.organic.requests.model.RequestTypeModel;

@Component("requestTypeToRequestTypeModelConverter")
public class RequestTypeToRequestTypeModelConverter
        implements Converter<RequestType, RequestTypeModel> {
    @Autowired
    private ObjectFactory<RequestTypeModel> requestTypeModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public RequestTypeModel convert(final RequestType source) {
        RequestTypeModel requestTypeModel = requestTypeModelFactory.getObject();
        BeanUtils.copyProperties(source, requestTypeModel);

        if (CollectionUtils.isNotEmpty(source.getRequestTypeAttributes())) {
			List<RequestTypeAttributeModel> requestTypeAttrModel = (List<RequestTypeAttributeModel>) conversionService.convert(
					source.getRequestTypeAttributes(),
					TypeDescriptor.forObject(source.getRequestTypeAttributes()),
					CollectionTypeDescriptor.forType(TypeDescriptor.valueOf(List.class), RequestTypeAttributeModel.class));
			requestTypeModel.getRequestTypeAttributeModels().addAll(requestTypeAttrModel);
		}
        return requestTypeModel;
    }

    @Autowired
    public void setRequestTypeModelFactory(
            final ObjectFactory<RequestTypeModel> requestTypeModelFactory) {
        this.requestTypeModelFactory = requestTypeModelFactory;
    }
}
