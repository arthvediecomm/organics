/**
 *
 */
package com.arthvedi.organic.requests.representation.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.requests.model.RequestTypeAttributeModel;
import com.arthvedi.organic.requests.model.RequestTypeModel;
import com.arthvedi.organic.requests.representation.siren.RequestTypeRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("requestTypeRepresentationToRequestTypeModelConverter")
public class RequestTypeRepresentationToRequestTypeModelConverter
		extends PropertyCopyingConverter<RequestTypeRepresentation, RequestTypeModel> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public RequestTypeModel convert(final RequestTypeRepresentation source) {
		RequestTypeModel target = super.convert(source);

		if (CollectionUtils.isNotEmpty(source.getRequestTypeAttributeRep())) {
			List<RequestTypeAttributeModel> converted = (List<RequestTypeAttributeModel>) conversionService.convert(
					source.getRequestTypeAttributeRep(), TypeDescriptor.forObject(source.getRequestTypeAttributeRep()),
					CollectionTypeDescriptor.forType(TypeDescriptor.valueOf(List.class), RequestTypeAttributeModel.class));
			target.getRequestTypeAttributeModels().addAll(converted);
		}
		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(final ObjectFactory<RequestTypeModel> factory) {
		super.setFactory(factory);
	}

}
