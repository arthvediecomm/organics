package com.arthvedi.organic.requests.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.requests.businessdelegate.context.RequestTypeAttributeContext;
import com.arthvedi.organic.requests.model.RequestTypeAttributeModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/requesttypeattribute", produces = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class RequestTypeAttributeController {

	private IBusinessDelegate<RequestTypeAttributeModel, RequestTypeAttributeContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<RequestTypeAttributeContext> requestTypeAttributeContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<RequestTypeAttributeModel> createRequestTypeAttribute(
			@RequestBody RequestTypeAttributeModel requestTypeAttributeModel) {
		requestTypeAttributeModel = businessDelegate
				.create(requestTypeAttributeModel);
		return new ResponseEntity<RequestTypeAttributeModel>(
				requestTypeAttributeModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<RequestTypeAttributeModel> edit(
			@PathVariable(value = "id") final String requestTypeAttributeId,
			@RequestBody RequestTypeAttributeModel requestTypeAttributeModel) {
		requestTypeAttributeModel = businessDelegate.edit(keyBuilderFactory
				.getObject().withId(requestTypeAttributeId),
				requestTypeAttributeModel);
		return new ResponseEntity<RequestTypeAttributeModel>(
				requestTypeAttributeModel, HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}/delete", consumes = { MediaType.ALL_VALUE })
	public void deleteRequestTypeAttribute(
			@PathVariable(value = "id") final String requestTypeAttributeId) {
		RequestTypeAttributeContext requestTypeAttributeContext = requestTypeAttributeContextFactory
				.getObject();
		requestTypeAttributeContext
				.setRequestTypeAttributeId(requestTypeAttributeId);
		businessDelegate.delete(
				keyBuilderFactory.getObject().withId(requestTypeAttributeId),
				requestTypeAttributeContext);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<RequestTypeAttributeModel> getRequestTypeAttribute(
			@PathVariable(value = "id") final String requestTypeAttributeId) {
		RequestTypeAttributeContext requestTypeAttributeContext = requestTypeAttributeContextFactory
				.getObject();
		RequestTypeAttributeModel model = businessDelegate.getByKey(
				keyBuilderFactory.getObject().withId(requestTypeAttributeId),
				requestTypeAttributeContext);
		return new ResponseEntity<RequestTypeAttributeModel>(model,
				HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = {
			"/requesttypeattributelist", "/sellerapp/requesttypeattributelist" })
	public ResponseEntity<IModelWrapper<Collection<RequestTypeAttributeModel>>> getAllRequestTypeAttributeList() {
		RequestTypeAttributeContext requestTypeAttributeContext = requestTypeAttributeContextFactory
				.getObject();
		requestTypeAttributeContext
				.setRequestTypeAttributeList("requestTypeAttributeList");
		Collection<RequestTypeAttributeModel> requestTypeAttributeModels = businessDelegate
				.getCollection(requestTypeAttributeContext);
		IModelWrapper<Collection<RequestTypeAttributeModel>> models = new CollectionModelWrapper<RequestTypeAttributeModel>(
				RequestTypeAttributeModel.class, requestTypeAttributeModels);
		return new ResponseEntity<IModelWrapper<Collection<RequestTypeAttributeModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = {
			"/{requestTypeId}/requesttypeattributelist",
			"/sellerapp/{requestTypeId}/requesttypeattributelist" })
	public ResponseEntity<IModelWrapper<Collection<RequestTypeAttributeModel>>> getAllRequestTypeAttributeList(
			@PathVariable(value = "requestTypeId") final String requestTypeId) {
		RequestTypeAttributeContext requestTypeAttributeContext = requestTypeAttributeContextFactory
				.getObject();
		requestTypeAttributeContext.setRequestTypeId(requestTypeId);
		Collection<RequestTypeAttributeModel> requestTypeAttributeModels = businessDelegate
				.getCollection(requestTypeAttributeContext);
		IModelWrapper<Collection<RequestTypeAttributeModel>> models = new CollectionModelWrapper<RequestTypeAttributeModel>(
				RequestTypeAttributeModel.class, requestTypeAttributeModels);
		return new ResponseEntity<IModelWrapper<Collection<RequestTypeAttributeModel>>>(
				models, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "requestTypeAttributeBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<RequestTypeAttributeModel, RequestTypeAttributeContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setRequestTypeAttributeObjectFactory(
			final ObjectFactory<RequestTypeAttributeContext> requestTypeAttributeContextFactory) {
		this.requestTypeAttributeContextFactory = requestTypeAttributeContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(
			final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
