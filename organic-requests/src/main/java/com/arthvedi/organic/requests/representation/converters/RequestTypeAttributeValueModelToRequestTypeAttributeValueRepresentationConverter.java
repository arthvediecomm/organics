/**
 *
 */
package com.arthvedi.organic.requests.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.requests.model.RequestTypeAttributeValueModel;
import com.arthvedi.organic.requests.representation.siren.RequestTypeAttributeValueRepresentation;

/**
 * @author varma
 *
 */
@Component("requestTypeAttributeValueModelToRequestTypeAttributeValueRepresentationConverter")
public class RequestTypeAttributeValueModelToRequestTypeAttributeValueRepresentationConverter extends PropertyCopyingConverter<RequestTypeAttributeValueModel, RequestTypeAttributeValueRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public RequestTypeAttributeValueRepresentation convert(final RequestTypeAttributeValueModel source) {
        RequestTypeAttributeValueRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<RequestTypeAttributeValueRepresentation> factory) {
        super.setFactory(factory);
    }

}
