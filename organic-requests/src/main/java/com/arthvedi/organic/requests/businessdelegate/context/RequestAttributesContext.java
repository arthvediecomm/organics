package com.arthvedi.organic.requests.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;

/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RequestAttributesContext implements IBusinessDelegateContext {

	private String requestAttributesId;
	private String all;

	public String getRequestAttributesId() {
		return requestAttributesId;
	}

	public void setRequestAttributesId(String requestAttributesId) {
		this.requestAttributesId = requestAttributesId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

}
