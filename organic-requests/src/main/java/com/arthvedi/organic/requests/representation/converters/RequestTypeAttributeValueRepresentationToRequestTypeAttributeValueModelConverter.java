/**
 *
 */
package com.arthvedi.organic.requests.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.requests.model.RequestTypeAttributeValueModel;
import com.arthvedi.organic.requests.representation.siren.RequestTypeAttributeValueRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("requestTypeAttributeValueRepresentationToRequestTypeAttributeValueModelConverter")
public class RequestTypeAttributeValueRepresentationToRequestTypeAttributeValueModelConverter extends PropertyCopyingConverter<RequestTypeAttributeValueRepresentation, RequestTypeAttributeValueModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public RequestTypeAttributeValueModel convert(final RequestTypeAttributeValueRepresentation source) {
        RequestTypeAttributeValueModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<RequestTypeAttributeValueModel> factory) {
        super.setFactory(factory);
    }

}
