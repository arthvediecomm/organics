package com.arthvedi.organic.requests.service;

import java.util.List;

import com.arthvedi.organic.requests.domain.RequestTypeAttributeValue;
/*
*@Author varma
*/
public interface IRequestTypeAttributeValueService {
	
	RequestTypeAttributeValue create(RequestTypeAttributeValue requestTypeAttributeValue);

	void deleteRequestTypeAttributeValue(String requestTypeAttributeValueId);

	RequestTypeAttributeValue getRequestTypeAttributeValue(String requestTypeAttributeValueId);

	List<RequestTypeAttributeValue> getAll();

	RequestTypeAttributeValue updateRequestTypeAttributeValue(RequestTypeAttributeValue requestTypeAttributeValue);

	List<RequestTypeAttributeValue> getRequestTypeAttributeValuesByRequestTypeAttr(String requestTypeAttributeId);
}
