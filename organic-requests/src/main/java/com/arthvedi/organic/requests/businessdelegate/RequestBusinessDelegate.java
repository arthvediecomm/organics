package com.arthvedi.organic.requests.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.requests.businessdelegate.context.RequestContext;
import com.arthvedi.organic.requests.domain.Request;
import com.arthvedi.organic.requests.model.RequestModel;
import com.arthvedi.organic.requests.service.IRequestService;

/*
 *@Author varma
 */

@Service
public class RequestBusinessDelegate
		implements
		IBusinessDelegate<RequestModel, RequestContext, IKeyBuilder<String>, String> {

	@Autowired
	private IRequestService requestService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public RequestModel create(RequestModel model) {
		validateModel(model);
		Request req = requestService.create((Request) conversionService
				.convert(model, forObject(model), valueOf(Request.class)));
		model = convertToRequestModel(req);
		model.setStatusMessage("SUCCESS::: Request created successfully");

		return model;
	}

	private RequestModel convertToRequestModel(Request req) {
		return (RequestModel) conversionService.convert(req, forObject(req),
				valueOf(RequestModel.class));
	}

	private void validateModel(RequestModel model) {
		Validate.notNull(model, "Invalid Input");
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, RequestContext context) {
		requestService.deleteRequest(context.getRequestId());
	}

	@Override
	public RequestModel edit(IKeyBuilder<String> keyBuilder, RequestModel model) {
		Request request = requestService.getRequest(keyBuilder.build()
				.toString());
		request = requestService.updateRequest((Request) conversionService
				.convert(model, forObject(model), valueOf(Request.class)));
		model = convertToRequestModel(request);
		model.setStatusMessage("SUCCESS:::  Request updated successfully");
		return model;
	}

	@Override
	public RequestModel getByKey(IKeyBuilder<String> keyBuilder,
			RequestContext context) {
		Request request = requestService.getRequest(keyBuilder.build()
				.toString());
		RequestModel model = conversionService.convert(request,
				RequestModel.class);
		return model;
	}

	@Override
	public Collection<RequestModel> getCollection(RequestContext context) {
		List<Request> request = new ArrayList<Request>();
		if (context.getRequestList() != null) {
			request = requestService.getRequestList();
		}
		List<RequestModel> requestModels = (List<RequestModel>) conversionService
				.convert(
						request,
						TypeDescriptor.forObject(request),
						TypeDescriptor.collection(List.class,
								TypeDescriptor.valueOf(RequestModel.class)));
		return requestModels;
	}

	@Override
	public RequestModel edit(IKeyBuilder<String> keyBuilder,
			RequestModel model, RequestContext context) {
		// TODO Auto-generated method stub
		return null;
	}

}
