package com.arthvedi.organic.requests.service;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.requests.domain.RequestTypeAttributeValue;
import com.arthvedi.organic.requests.repository.RequestTypeAttributeValueRepository;
/*
*@Author varma
*/
@Component
public class RequestTypeAttributeValueService implements IRequestTypeAttributeValueService {
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private RequestTypeAttributeValueRepository requestTypeAttributeValueRepository;

	@Override
	@Transactional
	public RequestTypeAttributeValue create(RequestTypeAttributeValue requestTypeAttributeValue) {
		requestTypeAttributeValue.setCreatedDate(new LocalDateTime());
		//requestTypeAttributeValue.setStatus(ACTIVE.name());
		requestTypeAttributeValue.setUserCreated("VARMA");
		return requestTypeAttributeValueRepository.save(requestTypeAttributeValue);
	}

	@Override
	@Transactional
	public void deleteRequestTypeAttributeValue(String requestTypeAttributeValueId) {
		RequestTypeAttributeValue requestTypeAttributeValue = requestTypeAttributeValueRepository
				.findOne(requestTypeAttributeValueId);
		requestTypeAttributeValueRepository.delete(requestTypeAttributeValue);
	}

	@Override
	public RequestTypeAttributeValue getRequestTypeAttributeValue(String requestTypeAttributeValueId) {
		return requestTypeAttributeValueRepository.findOne(requestTypeAttributeValueId);
	}

	@Override
	public List<RequestTypeAttributeValue> getAll() {
		return (List<RequestTypeAttributeValue>) requestTypeAttributeValueRepository.findAll();
	}

	@Override
	@Transactional
	public RequestTypeAttributeValue updateRequestTypeAttributeValue(
			RequestTypeAttributeValue requestTypeAttributeValue) {
		RequestTypeAttributeValue reqTypeAttrValue = requestTypeAttributeValueRepository
				.findOne(requestTypeAttributeValue.getId());
		try {
			nonNullBeanUtils.copyProperties(reqTypeAttrValue, requestTypeAttributeValue);
		} catch (IllegalAccessException e) {
			e.printStackTrace();

		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		reqTypeAttrValue.setModifiedDate(new LocalDateTime());
		reqTypeAttrValue.setUserModified("Admin");
		requestTypeAttributeValue = requestTypeAttributeValueRepository.save(reqTypeAttrValue);
		return requestTypeAttributeValue;
	}

	@Override
	public List<RequestTypeAttributeValue> getRequestTypeAttributeValuesByRequestTypeAttr(
			String requestTypeAttributeId) {
		// TODO Auto-generated method stub
		return null;
	}

	/*@Override
	public List<RequestTypeAttributeValue> getRequestTypeAttributeValuesByRequestTypeAttr(
			String requestTypeAttributeId) {
		return requestTypeAttributeValueRepository.findAll(byRequestTypeAttribute(requestTypeAttributeId));
	}*/

}
