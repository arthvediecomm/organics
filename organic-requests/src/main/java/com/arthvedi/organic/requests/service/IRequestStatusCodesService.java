package com.arthvedi.organic.requests.service;

import java.util.List;

import com.arthvedi.organic.requests.domain.RequestStatusCodes;
/*
*@Author varma
*/
public interface IRequestStatusCodesService {
	
	RequestStatusCodes create(RequestStatusCodes requestStatusCodes);

	void deleteRequestStatusCodes(String requestStatusCodesId);

	RequestStatusCodes getRequestStatusCodes(String requestStatusCodesId);

	List<RequestStatusCodes> getAll();

	RequestStatusCodes updateRequestStatusCodes(RequestStatusCodes requestStatusCodes);
}
