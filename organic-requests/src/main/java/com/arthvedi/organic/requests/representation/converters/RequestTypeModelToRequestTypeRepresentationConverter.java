/**
 *
 */
package com.arthvedi.organic.requests.representation.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.requests.model.RequestTypeModel;
import com.arthvedi.organic.requests.representation.siren.RequestTypeAttributeRepresentation;
import com.arthvedi.organic.requests.representation.siren.RequestTypeRepresentation;

/**
 * @author varma
 *
 */
@Component("requestTypeModelToRequestTypeRepresentationConverter")
public class RequestTypeModelToRequestTypeRepresentationConverter extends PropertyCopyingConverter<RequestTypeModel, RequestTypeRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public RequestTypeRepresentation convert(final RequestTypeModel source) {
        RequestTypeRepresentation target = super.convert(source);
    	if (CollectionUtils.isNotEmpty(source.getRequestTypeAttributeModels())) {
			List<RequestTypeAttributeRepresentation> converted = (List<RequestTypeAttributeRepresentation>) conversionService.convert(
					source.getRequestTypeAttributeModels(), TypeDescriptor.forObject(source.getRequestTypeAttributeModels()),
					CollectionTypeDescriptor.forType(TypeDescriptor.valueOf(List.class), RequestTypeAttributeRepresentation.class));
			target.getRequestTypeAttributeRep().addAll(converted);
		}
        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<RequestTypeRepresentation> factory) {
        super.setFactory(factory);
    }

}
