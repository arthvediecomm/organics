package com.arthvedi.organic.requests.specifications;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.arthvedi.organic.requests.domain.RequestTypeAttribute;

public class RequestTypeAttributeSpecifications {
	public static Specification<RequestTypeAttribute> byRequestType(String requestTypeId) {

		return new Specification<RequestTypeAttribute>() {

			@Override
			public Predicate toPredicate(Root<RequestTypeAttribute> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.get("requestType").get("id"), requestTypeId);
			}

		};
	}
}
