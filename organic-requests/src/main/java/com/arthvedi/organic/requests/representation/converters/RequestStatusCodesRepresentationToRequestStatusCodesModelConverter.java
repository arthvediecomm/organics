/**
 *
 */
package com.arthvedi.organic.requests.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.requests.model.RequestStatusCodesModel;
import com.arthvedi.organic.requests.representation.siren.RequestStatusCodesRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("requestStatusCodesRepresentationToRequestStatusCodesModelConverter")
public class RequestStatusCodesRepresentationToRequestStatusCodesModelConverter extends PropertyCopyingConverter<RequestStatusCodesRepresentation, RequestStatusCodesModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public RequestStatusCodesModel convert(final RequestStatusCodesRepresentation source) {

        RequestStatusCodesModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<RequestStatusCodesModel> factory) {
        super.setFactory(factory);
    }

}
