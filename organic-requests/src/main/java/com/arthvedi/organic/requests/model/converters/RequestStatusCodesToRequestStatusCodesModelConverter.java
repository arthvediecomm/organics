package com.arthvedi.organic.requests.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.requests.domain.RequestStatusCodes;
import com.arthvedi.organic.requests.model.RequestStatusCodesModel;

@Component("requestStatusCodesToRequestStatusCodesModelConverter")
public class RequestStatusCodesToRequestStatusCodesModelConverter
        implements Converter<RequestStatusCodes, RequestStatusCodesModel> {
    @Autowired
    private ObjectFactory<RequestStatusCodesModel> requestStatusCodesModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public RequestStatusCodesModel convert(final RequestStatusCodes source) {
        RequestStatusCodesModel requestStatusCodesModel = requestStatusCodesModelFactory.getObject();
        BeanUtils.copyProperties(source, requestStatusCodesModel);

        return requestStatusCodesModel;
    }

    @Autowired
    public void setRequestStatusCodesModelFactory(
            final ObjectFactory<RequestStatusCodesModel> requestStatusCodesModelFactory) {
        this.requestStatusCodesModelFactory = requestStatusCodesModelFactory;
    }
}
