package com.arthvedi.organic.requests.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;

/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RequestTypeAttributeContext implements IBusinessDelegateContext {

	private String requestTypeAttributeId;
	private String requestTypeAttributeList;
	private String requestTypeId;
	private String all;



	public String getRequestTypeId() {
		return requestTypeId;
	}

	public void setRequestTypeId(String requestTypeId) {
		this.requestTypeId = requestTypeId;
	}

	public String getRequestTypeAttributeId() {
		return requestTypeAttributeId;
	}

	public void setRequestTypeAttributeId(String requestTypeAttributeId) {
		this.requestTypeAttributeId = requestTypeAttributeId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

	public String getRequestTypeAttributeList() {
		return requestTypeAttributeList;
	}

	public void setRequestTypeAttributeList(String requestTypeAttributeList) {
		this.requestTypeAttributeList = requestTypeAttributeList;
	}

}
