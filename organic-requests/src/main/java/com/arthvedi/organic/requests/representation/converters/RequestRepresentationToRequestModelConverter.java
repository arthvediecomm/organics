/**
 *
 */
package com.arthvedi.organic.requests.representation.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.requests.model.RequestAttributesModel;
import com.arthvedi.organic.requests.model.RequestModel;
import com.arthvedi.organic.requests.model.RequestStatusCodesModel;
import com.arthvedi.organic.requests.representation.siren.RequestRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("requestRepresentationToRequestModelConverter")
public class RequestRepresentationToRequestModelConverter
		extends PropertyCopyingConverter<RequestRepresentation, RequestModel> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public RequestModel convert(final RequestRepresentation source) {
		RequestModel target = super.convert(source);
		if (CollectionUtils.isNotEmpty(source.getRequestAttributesRep())) {
			List<RequestAttributesModel> converted = (List<RequestAttributesModel>) conversionService.convert(
					source.getRequestAttributesRep(), TypeDescriptor.forObject(source.getRequestAttributesRep()),
					CollectionTypeDescriptor.forType(TypeDescriptor.valueOf(List.class), RequestAttributesModel.class));
			target.getRequestAttributesModels().addAll(converted);
		}
		if (CollectionUtils.isNotEmpty(source.getRequestStatusCodesRep())) {
			List<RequestStatusCodesModel> converted = (List<RequestStatusCodesModel>) conversionService.convert(
					source.getRequestStatusCodesRep(), TypeDescriptor.forObject(source.getRequestStatusCodesRep()),
					CollectionTypeDescriptor.forType(TypeDescriptor.valueOf(List.class),
							RequestStatusCodesModel.class));
			target.getRequestStatusCodesModels().addAll(converted);
		}
		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(final ObjectFactory<RequestModel> factory) {
		super.setFactory(factory);
	}

}
