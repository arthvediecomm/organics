package com.arthvedi.organic.requests.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.arthvedi.organic.requests.domain.RequestStatusCodes;
import com.arthvedi.organic.requests.repository.RequestStatusCodesRepository;
/*
*@Author varma
*/
@Component
public class RequestStatusCodesService implements IRequestStatusCodesService{

	@Autowired
	private RequestStatusCodesRepository requestStatusCodesRepository;
	@Override
	@Transactional
	public RequestStatusCodes create(RequestStatusCodes requestStatusCodes) {
		return requestStatusCodesRepository.save(requestStatusCodes);
	}

	@Override
	public void deleteRequestStatusCodes(String requestStatusCodesId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public RequestStatusCodes getRequestStatusCodes(String requestStatusCodesId) {
		// TODO Auto-generated method stub
		 return requestStatusCodesRepository.findOne(requestStatusCodesId);
	}

	@Override
	public List<RequestStatusCodes> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RequestStatusCodes updateRequestStatusCodes(RequestStatusCodes requestStatusCodes) {
		// TODO Auto-generated method stub
	return requestStatusCodesRepository.save(requestStatusCodes);
	}

}
