/**
 *
 */
package com.arthvedi.organic.requests.representation.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.requests.model.RequestTypeAttributeModel;
import com.arthvedi.organic.requests.model.RequestTypeAttributeValueModel;
import com.arthvedi.organic.requests.representation.siren.RequestTypeAttributeRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("requestTypeAttributeRepresentationToRequestTypeAttributeModelConverter")
public class RequestTypeAttributeRepresentationToRequestTypeAttributeModelConverter extends PropertyCopyingConverter<RequestTypeAttributeRepresentation, RequestTypeAttributeModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public RequestTypeAttributeModel convert(final RequestTypeAttributeRepresentation source) {
        RequestTypeAttributeModel target = super.convert(source);

    	if (CollectionUtils.isNotEmpty(source.getRequestTypeAttributeValueRep())) {
			List<RequestTypeAttributeValueModel> converted = (List<RequestTypeAttributeValueModel>) conversionService.convert(
					source.getRequestTypeAttributeValueRep(), TypeDescriptor.forObject(source.getRequestTypeAttributeValueRep()),
					CollectionTypeDescriptor.forType(TypeDescriptor.valueOf(List.class), RequestTypeAttributeValueModel.class));
			target.getRequestTypeAttributeValueModels().addAll(converted);
		}
        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<RequestTypeAttributeModel> factory) {
        super.setFactory(factory);
    }

}
