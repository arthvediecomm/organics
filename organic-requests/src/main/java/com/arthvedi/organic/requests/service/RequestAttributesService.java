package com.arthvedi.organic.requests.service;

import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.arthvedi.organic.requests.domain.RequestAttributes;
import com.arthvedi.organic.requests.repository.RequestAttributesRepository;

/*
*@Author varma
*/
@Component
public class RequestAttributesService implements IRequestAttributesService {

	@Autowired
	private RequestAttributesRepository requestAttributesRepository;

	@Override
	@Transactional
	public RequestAttributes create(RequestAttributes requestAttributes) {
		requestAttributes.setCreatedDate(new LocalDateTime());
		//requestAttributes.setStatus(ACTIVE.name());
		requestAttributes.setUserCreated("VARMA");
		return requestAttributesRepository.save(requestAttributes);
	}

	@Override
	public void deleteRequestAttributes(String requestAttributesId) {

	}

	@Override
	public RequestAttributes getRequestAttributes(String requestAttributesId) {
		return requestAttributesRepository.findOne(requestAttributesId);
	}

	@Override
	public List<RequestAttributes> getAll() {
		return null;
	}

	@Override
	public RequestAttributes updateRequestAttributes(RequestAttributes requestAttributes) {
		return requestAttributesRepository.save(requestAttributes);
	}

}
