/**
 *
 */
package com.arthvedi.organic.requests.model.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.organic.requests.domain.RequestTypeAttribute;
import com.arthvedi.organic.requests.domain.RequestTypeAttributeValue;
import com.arthvedi.organic.requests.model.RequestTypeAttributeModel;

/**
 * @author Jay
 *
 */
@Component("requestTypeAttributeModelToRequestTypeAttributeConverter")
public class RequestTypeAttributeModelToRequestTypeAttributeConverter implements Converter<RequestTypeAttributeModel, RequestTypeAttribute> {
    @Autowired
    private ObjectFactory<RequestTypeAttribute> requestTypeAttributeFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public RequestTypeAttribute convert(final RequestTypeAttributeModel source) {
        RequestTypeAttribute requestTypeAttribute = new RequestTypeAttribute();
        BeanUtils.copyProperties(source, requestTypeAttribute);
        if (CollectionUtils.isNotEmpty(source.getRequestTypeAttributeValueModels())) {
			List<RequestTypeAttributeValue> converted = (List<RequestTypeAttributeValue>) conversionService.convert(
					source.getRequestTypeAttributeValueModels(),
					TypeDescriptor.forObject(source.getRequestTypeAttributeValueModels()),
					CollectionTypeDescriptor.forType(TypeDescriptor.valueOf(List.class), RequestTypeAttributeValue.class));
			requestTypeAttribute.getRequestTypeAttributeValues().addAll(converted);
		}
        return requestTypeAttribute;
    }

    @Autowired
    public void setRequestTypeAttributeFactory(final ObjectFactory<RequestTypeAttribute> requestTypeAttributeFactory) {
        this.requestTypeAttributeFactory = requestTypeAttributeFactory;
    }

}
