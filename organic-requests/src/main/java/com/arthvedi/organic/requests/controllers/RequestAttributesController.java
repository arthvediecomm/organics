package com.arthvedi.organic.requests.controllers;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.organic.requests.businessdelegate.context.RequestAttributesContext;
import com.arthvedi.organic.requests.model.RequestAttributesModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/requestattributes", produces = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class RequestAttributesController {

	private IBusinessDelegate<RequestAttributesModel, RequestAttributesContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<RequestAttributesContext> requestAttributesContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<RequestAttributesModel> createRequestAttributes(
			@RequestBody final RequestAttributesModel requestAttributesModel) {
		businessDelegate.create(requestAttributesModel);
		return new ResponseEntity<RequestAttributesModel>(
				requestAttributesModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<RequestAttributesModel> edit(
			@PathVariable(value = "id") final String requestAttributesId,
			@RequestBody final RequestAttributesModel requestAttributesModel) {

		businessDelegate.edit(
				keyBuilderFactory.getObject().withId(requestAttributesId),
				requestAttributesModel);
		return new ResponseEntity<RequestAttributesModel>(
				requestAttributesModel, HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<RequestAttributesModel> getRequestAttributes(
			@PathVariable(value = "id") final String requestAttributesId) {
		RequestAttributesContext requestAttributesContext = requestAttributesContextFactory
				.getObject();

		RequestAttributesModel model = businessDelegate.getByKey(
				keyBuilderFactory.getObject().withId(requestAttributesId),
				requestAttributesContext);
		return new ResponseEntity<RequestAttributesModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "requestAttributesBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<RequestAttributesModel, RequestAttributesContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setRequestAttributesObjectFactory(
			final ObjectFactory<RequestAttributesContext> requestAttributesContextFactory) {
		this.requestAttributesContextFactory = requestAttributesContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(
			final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
