/**
 *
 */
package com.arthvedi.organic.requests.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.requests.model.RequestAttributesModel;
import com.arthvedi.organic.requests.representation.siren.RequestAttributesRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("requestAttributesRepresentationToRequestAttributesModelConverter")
public class RequestAttributesRepresentationToRequestAttributesModelConverter extends PropertyCopyingConverter<RequestAttributesRepresentation, RequestAttributesModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public RequestAttributesModel convert(final RequestAttributesRepresentation source) {

        RequestAttributesModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<RequestAttributesModel> factory) {
        super.setFactory(factory);
    }

}
