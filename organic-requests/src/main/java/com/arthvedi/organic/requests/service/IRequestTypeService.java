package com.arthvedi.organic.requests.service;

import java.util.List;

import com.arthvedi.organic.requests.domain.RequestType;
/*
*@Author varma
*/
public interface IRequestTypeService {
	
	RequestType create(RequestType requestType);

	void deleteRequestType(String requestTypeId);

	RequestType getRequestType(String requestTypeId);

	List<RequestType> getAll();

	RequestType updateRequestType(RequestType requestType);

	List<RequestType> getRequestTypeList();
}
