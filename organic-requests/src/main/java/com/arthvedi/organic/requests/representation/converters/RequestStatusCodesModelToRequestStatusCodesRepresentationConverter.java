/**
 *
 */
package com.arthvedi.organic.requests.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.requests.model.RequestStatusCodesModel;
import com.arthvedi.organic.requests.representation.siren.RequestStatusCodesRepresentation;

/**
 * @author varma
 *
 */
@Component("requestStatusCodesModelToRequestStatusCodesRepresentationConverter")
public class RequestStatusCodesModelToRequestStatusCodesRepresentationConverter extends PropertyCopyingConverter<RequestStatusCodesModel, RequestStatusCodesRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public RequestStatusCodesRepresentation convert(final RequestStatusCodesModel source) {

        RequestStatusCodesRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<RequestStatusCodesRepresentation> factory) {
        super.setFactory(factory);
    }

}
