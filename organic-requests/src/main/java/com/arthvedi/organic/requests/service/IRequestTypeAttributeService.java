package com.arthvedi.organic.requests.service;

import java.util.List;

import com.arthvedi.organic.requests.domain.RequestTypeAttribute;
/*
*@Author varma
*/
public interface IRequestTypeAttributeService {
	
	RequestTypeAttribute create(RequestTypeAttribute requestTypeAttribute);

	void deleteRequestTypeAttribute(String requestTypeAttributeId);

	RequestTypeAttribute getRequestTypeAttribute(String requestTypeAttributeId);

	List<RequestTypeAttribute> getAll();

	RequestTypeAttribute updateRequestTypeAttribute(RequestTypeAttribute requestTypeAttribute);

	List<RequestTypeAttribute> getRequestTypeList();

	List<RequestTypeAttribute> getRequestTypeAttributeByRequestType(String requestTypeId);
}
