package com.arthvedi.organic.requests.service;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.requests.domain.RequestTypeAttribute;
import com.arthvedi.organic.requests.domain.RequestTypeAttributeValue;
import com.arthvedi.organic.requests.repository.RequestTypeAttributeRepository;
/*
*@Author varma
*/
@Component
public class RequestTypeAttributeService implements IRequestTypeAttributeService {
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private IRequestTypeAttributeValueService requestTypeAttributeValueService;
	@Autowired
	private RequestTypeAttributeRepository requestTypeAttributeRepository;

	@Override
	@Transactional
	public RequestTypeAttribute create(RequestTypeAttribute requestTypeAttribute) {
		requestTypeAttribute.setUserCreated("VARMA");
		requestTypeAttribute.setCreatedDate(new LocalDateTime());
		//requestTypeAttribute.setStatus(ACTIVE.name());
		requestTypeAttribute = requestTypeAttributeRepository.save(requestTypeAttribute);
		if (requestTypeAttribute.getId() != null
				&& CollectionUtils.isNotEmpty(requestTypeAttribute.getRequestTypeAttributeValues())) {
			requestTypeAttribute.setRequestTypeAttributeValues(addRequestTypeAttributeValues(requestTypeAttribute,
					requestTypeAttribute.getRequestTypeAttributeValues()));
		}
		return requestTypeAttribute;
	}

	@Transactional
	private Set<RequestTypeAttributeValue> addRequestTypeAttributeValues(RequestTypeAttribute requestTypeAttribute,
			Set<RequestTypeAttributeValue> requestTypeAttributeValues) {
		Set<RequestTypeAttributeValue> requestTypeAttributesValues = new HashSet<RequestTypeAttributeValue>();
		for (RequestTypeAttributeValue rqtAVal : requestTypeAttributeValues) {
			RequestTypeAttributeValue reqTAttrValue = rqtAVal;
			reqTAttrValue.setRequestTypeAttribute(requestTypeAttribute);
			reqTAttrValue = requestTypeAttributeValueService.create(reqTAttrValue);
			requestTypeAttributesValues.add(reqTAttrValue);
		}
		return requestTypeAttributesValues;
	}


	@Override
	@Transactional
	public void deleteRequestTypeAttribute(String requestTypeAttributeId) {
		RequestTypeAttribute requestTypeAttribute = requestTypeAttributeRepository
				.findOne(requestTypeAttributeId);
		requestTypeAttributeRepository.delete(requestTypeAttribute);
	}

	@Override
	public RequestTypeAttribute getRequestTypeAttribute(String requestTypeAttributeId) {
		return requestTypeAttributeRepository.findOne(requestTypeAttributeId);
	}

	@Override
	public List<RequestTypeAttribute> getAll() {
		return null;
	}

	@Override
	@Transactional
	public RequestTypeAttribute updateRequestTypeAttribute(RequestTypeAttribute requestTypeAttribute) {
		RequestTypeAttribute reqTypeAttr = requestTypeAttributeRepository.findOne(requestTypeAttribute.getId());
		try {
			nonNullBeanUtils.copyProperties(reqTypeAttr, requestTypeAttribute);
		} catch (IllegalAccessException e) {
			e.printStackTrace();

		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		reqTypeAttr.setModifiedDate(new LocalDateTime());
		reqTypeAttr.setUserModified("Admin");
		if (requestTypeAttribute.getRequestTypeAttributeValues() != null) {
			requestTypeAttribute.setRequestTypeAttributeValues(addRequestTypeAttributeValues(requestTypeAttribute,
					requestTypeAttribute.getRequestTypeAttributeValues()));
		}
		requestTypeAttribute = requestTypeAttributeRepository.save(reqTypeAttr);
		return requestTypeAttribute;
	}

	@Override
	public List<RequestTypeAttribute> getRequestTypeList() {
		List<RequestTypeAttribute> requestTypeAttributes = new ArrayList<RequestTypeAttribute>();
		List<RequestTypeAttribute> requestTypeAttributs = (List<RequestTypeAttribute>) requestTypeAttributeRepository.findAll();
		for(RequestTypeAttribute reqTypeAttr : requestTypeAttributs){
			RequestTypeAttribute reqTAttr = new RequestTypeAttribute();
			reqTAttr.setId(reqTypeAttr.getId());
			reqTAttr.setRequestType(reqTypeAttr.getRequestType());
			reqTAttr.setRequestTypeAttributeName(reqTypeAttr.getRequestTypeAttributeName());
			reqTAttr.setRequestTypeAttributeDatatype(reqTypeAttr.getRequestTypeAttributeDatatype());
			reqTAttr.setStatus(reqTypeAttr.getStatus());
			reqTAttr.setCreatedDate(reqTypeAttr.getCreatedDate());
			requestTypeAttributes.add(reqTAttr);
		}
		return requestTypeAttributes;
	}

	@Override
	public List<RequestTypeAttribute> getRequestTypeAttributeByRequestType(String requestTypeId) {
		// TODO Auto-generated method stub
		return null;
	}

	/*@Override
	public List<RequestTypeAttribute> getRequestTypeAttributeByRequestType(String requestTypeId) {
		return requestTypeAttributeRepository.findAll(byRequestType(requestTypeId));
	}*/

	

	

}
