package com.arthvedi.organic.requests.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.requests.businessdelegate.context.RequestTypeAttributeValueContext;
import com.arthvedi.organic.requests.domain.RequestTypeAttributeValue;
import com.arthvedi.organic.requests.model.RequestTypeAttributeValueModel;
import com.arthvedi.organic.requests.service.IRequestTypeAttributeValueService;

/*
 *@Author varma
 */

@Service
public class RequestTypeAttributeValueBusinessDelegate
		implements
		IBusinessDelegate<RequestTypeAttributeValueModel, RequestTypeAttributeValueContext, IKeyBuilder<String>, String> {

	@Autowired
	private IRequestTypeAttributeValueService requestTypeAttributeValueService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public RequestTypeAttributeValueModel create(
			RequestTypeAttributeValueModel model) {
		validateModel(model);
		RequestTypeAttributeValue reqTypeAttributeValue = requestTypeAttributeValueService
				.create((RequestTypeAttributeValue) conversionService.convert(
						model, forObject(model),
						valueOf(RequestTypeAttributeValue.class)));
		model = convertToRequestTypeAttributeValueModel(reqTypeAttributeValue);
		model.setStatusMessage("SUCCESS::: RequestTypeAttributeValue created successfully");
		return model;
	}

	private RequestTypeAttributeValueModel convertToRequestTypeAttributeValueModel(
			RequestTypeAttributeValue reqTypeAttributeValue) {
		return (RequestTypeAttributeValueModel) conversionService.convert(
				reqTypeAttributeValue, forObject(reqTypeAttributeValue),
				valueOf(RequestTypeAttributeValueModel.class));
	}

	private void validateModel(RequestTypeAttributeValueModel model) {
		Validate.notNull(model, "Invalid Input");
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			RequestTypeAttributeValueContext context) {
		requestTypeAttributeValueService
				.deleteRequestTypeAttributeValue(context
						.getRequestTypeAttributeValueId());
	}

	@Override
	public RequestTypeAttributeValueModel edit(IKeyBuilder<String> keyBuilder,
			RequestTypeAttributeValueModel model) {
		RequestTypeAttributeValue requestTypeAttributeValue = requestTypeAttributeValueService
				.getRequestTypeAttributeValue(keyBuilder.build().toString());
		requestTypeAttributeValue = requestTypeAttributeValueService
				.updateRequestTypeAttributeValue((RequestTypeAttributeValue) conversionService
						.convert(model, forObject(model),
								valueOf(RequestTypeAttributeValue.class)));
		model = convertToRequestTypeAttributeValueModel(requestTypeAttributeValue);
		model.setStatusMessage("SUCCESS::: RequestTypeAttributeValue updated successfully");
		return model;
	}

	@Override
	public RequestTypeAttributeValueModel getByKey(
			IKeyBuilder<String> keyBuilder,
			RequestTypeAttributeValueContext context) {
		RequestTypeAttributeValue requestTypeAttributeValue = requestTypeAttributeValueService
				.getRequestTypeAttributeValue(keyBuilder.build().toString());
		RequestTypeAttributeValueModel model = conversionService
				.convert(requestTypeAttributeValue,
						RequestTypeAttributeValueModel.class);
		return model;
	}

	@Override
	public Collection<RequestTypeAttributeValueModel> getCollection(
			RequestTypeAttributeValueContext context) {
		List<RequestTypeAttributeValue> requestTypeAttributeValue = new ArrayList<RequestTypeAttributeValue>();
		if (context.getRequestTypeAttributeId() != null) {
			requestTypeAttributeValue = requestTypeAttributeValueService
					.getRequestTypeAttributeValuesByRequestTypeAttr(context
							.getRequestTypeAttributeId());
		}
		List<RequestTypeAttributeValueModel> requestTypeAttributeValueModels = (List<RequestTypeAttributeValueModel>) conversionService
				.convert(requestTypeAttributeValue, TypeDescriptor
						.forObject(requestTypeAttributeValue), TypeDescriptor
						.collection(List.class, TypeDescriptor
								.valueOf(RequestTypeAttributeValueModel.class)));
		return requestTypeAttributeValueModels;
	}

	@Override
	public RequestTypeAttributeValueModel edit(IKeyBuilder<String> keyBuilder,
			RequestTypeAttributeValueModel model,
			RequestTypeAttributeValueContext context) {
		return null;
	}

}
