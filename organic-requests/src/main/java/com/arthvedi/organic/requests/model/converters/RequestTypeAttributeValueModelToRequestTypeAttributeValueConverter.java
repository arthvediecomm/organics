/**
 *
 */
package com.arthvedi.organic.requests.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.requests.domain.RequestTypeAttributeValue;
import com.arthvedi.organic.requests.model.RequestTypeAttributeValueModel;

/**
 * @author Jay
 *
 */
@Component("requestTypeAttributeValueModelToRequestTypeAttributeValueConverter")
public class RequestTypeAttributeValueModelToRequestTypeAttributeValueConverter implements Converter<RequestTypeAttributeValueModel, RequestTypeAttributeValue> {
    @Autowired
    private ObjectFactory<RequestTypeAttributeValue> requestTypeAttributeValueFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public RequestTypeAttributeValue convert(final RequestTypeAttributeValueModel source) {
        RequestTypeAttributeValue requestTypeAttributeValue = new RequestTypeAttributeValue();
        BeanUtils.copyProperties(source, requestTypeAttributeValue);

        return requestTypeAttributeValue;
    }

    @Autowired
    public void setRequestTypeAttributeValueFactory(final ObjectFactory<RequestTypeAttributeValue> requestTypeAttributeValueFactory) {
        this.requestTypeAttributeValueFactory = requestTypeAttributeValueFactory;
    }

}
