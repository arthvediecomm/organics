package com.arthvedi.organic.requests.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;

/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RequestTypeAttributeValueContext implements IBusinessDelegateContext {

	private String requestTypeAttributeValueId;
	private String requestTypeAttributeId;
	private String all;

	public String getRequestTypeAttributeValueId() {
		return requestTypeAttributeValueId;
	}

	public void setRequestTypeAttributeValueId(String requestTypeAttributeValueId) {
		this.requestTypeAttributeValueId = requestTypeAttributeValueId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

	public String getRequestTypeAttributeId() {
		return requestTypeAttributeId;
	}

	public void setRequestTypeAttributeId(String requestTypeAttributeId) {
		this.requestTypeAttributeId = requestTypeAttributeId;
	}

}
