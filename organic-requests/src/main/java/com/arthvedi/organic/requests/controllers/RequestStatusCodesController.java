package com.arthvedi.organic.requests.controllers;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.organic.requests.businessdelegate.context.RequestStatusCodesContext;
import com.arthvedi.organic.requests.model.RequestStatusCodesModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/requestStatusCodes", produces = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class RequestStatusCodesController {

	private IBusinessDelegate<RequestStatusCodesModel, RequestStatusCodesContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<RequestStatusCodesContext> requestStatusCodesContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<RequestStatusCodesModel> createRequestStatusCodes(
			@RequestBody final RequestStatusCodesModel requestStatusCodesModel) {
		businessDelegate.create(requestStatusCodesModel);
		return new ResponseEntity<RequestStatusCodesModel>(
				requestStatusCodesModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<RequestStatusCodesModel> edit(
			@PathVariable(value = "id") final String requestStatusCodesId,
			@RequestBody final RequestStatusCodesModel requestStatusCodesModel) {

		businessDelegate.edit(
				keyBuilderFactory.getObject().withId(requestStatusCodesId),
				requestStatusCodesModel);
		return new ResponseEntity<RequestStatusCodesModel>(
				requestStatusCodesModel, HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<RequestStatusCodesModel> getRequestStatusCodes(
			@PathVariable(value = "id") final String requestStatusCodesId) {
		RequestStatusCodesContext requestStatusCodesContext = requestStatusCodesContextFactory
				.getObject();

		RequestStatusCodesModel model = businessDelegate.getByKey(
				keyBuilderFactory.getObject().withId(requestStatusCodesId),
				requestStatusCodesContext);
		return new ResponseEntity<RequestStatusCodesModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "requestStatusCodesBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<RequestStatusCodesModel, RequestStatusCodesContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setRequestStatusCodesObjectFactory(
			final ObjectFactory<RequestStatusCodesContext> requestStatusCodesContextFactory) {
		this.requestStatusCodesContextFactory = requestStatusCodesContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(
			final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
