package com.arthvedi.organic.requests.service;

import java.util.List;

import com.arthvedi.organic.requests.domain.RequestAttributes;
/*
*@Author varma
*/
public interface IRequestAttributesService {
	
	RequestAttributes create(RequestAttributes requestAttributes);

	void deleteRequestAttributes(String requestAttributesId);

	RequestAttributes getRequestAttributes(String requestAttributesId);

	List<RequestAttributes> getAll();

	RequestAttributes updateRequestAttributes(RequestAttributes requestAttributes);

	
}
