package com.arthvedi.organic.requests.model.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.organic.requests.domain.RequestTypeAttribute;
import com.arthvedi.organic.requests.model.RequestTypeAttributeModel;
import com.arthvedi.organic.requests.model.RequestTypeAttributeValueModel;

@Component("requestTypeAttributeToRequestTypeAttributeModelConverter")
public class RequestTypeAttributeToRequestTypeAttributeModelConverter
        implements Converter<RequestTypeAttribute, RequestTypeAttributeModel> {
    @Autowired
    private ObjectFactory<RequestTypeAttributeModel> requestTypeAttributeModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public RequestTypeAttributeModel convert(final RequestTypeAttribute source) {
        RequestTypeAttributeModel requestTypeAttributeModel = requestTypeAttributeModelFactory.getObject();
        BeanUtils.copyProperties(source, requestTypeAttributeModel);
        if (CollectionUtils.isNotEmpty(source.getRequestTypeAttributeValues())) {
			List<RequestTypeAttributeValueModel> converted = (List<RequestTypeAttributeValueModel>) conversionService.convert(
					source.getRequestTypeAttributeValues(),
					TypeDescriptor.forObject(source.getRequestTypeAttributeValues()),
					CollectionTypeDescriptor.forType(TypeDescriptor.valueOf(List.class), RequestTypeAttributeValueModel.class));
			requestTypeAttributeModel.getRequestTypeAttributeValueModels().addAll(converted);
		}
        return requestTypeAttributeModel;
    }

    @Autowired
    public void setRequestTypeAttributeModelFactory(
            final ObjectFactory<RequestTypeAttributeModel> requestTypeAttributeModelFactory) {
        this.requestTypeAttributeModelFactory = requestTypeAttributeModelFactory;
    }
}
