/**
 *
 */
package com.arthvedi.organic.requests.representation.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.requests.model.RequestModel;
import com.arthvedi.organic.requests.representation.siren.RequestAttributesRepresentation;
import com.arthvedi.organic.requests.representation.siren.RequestRepresentation;

/**
 * @author varma
 *
 */
@Component("requestModelToRequestRepresentationConverter")
public class RequestModelToRequestRepresentationConverter extends PropertyCopyingConverter<RequestModel, RequestRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public RequestRepresentation convert(final RequestModel source) {
        RequestRepresentation target = super.convert(source);
        if (CollectionUtils.isNotEmpty(source.getRequestAttributesModels())) {
			List<RequestAttributesRepresentation> converted = (List<RequestAttributesRepresentation>) conversionService.convert(
					source.getRequestAttributesModels(), TypeDescriptor.forObject(source.getRequestAttributesModels()),
					CollectionTypeDescriptor.forType(TypeDescriptor.valueOf(List.class), RequestAttributesRepresentation.class));
			target.getRequestAttributesRep().addAll(converted);
		}
        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<RequestRepresentation> factory) {
        super.setFactory(factory);
    }

}
