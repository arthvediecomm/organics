package com.arthvedi.organic.requests.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.requests.businessdelegate.context.RequestTypeAttributeContext;
import com.arthvedi.organic.requests.domain.RequestTypeAttribute;
import com.arthvedi.organic.requests.model.RequestTypeAttributeModel;
import com.arthvedi.organic.requests.service.IRequestTypeAttributeService;

/*
*@Author varma
*/

@Service
public class RequestTypeAttributeBusinessDelegate
		implements IBusinessDelegate<RequestTypeAttributeModel, RequestTypeAttributeContext, IKeyBuilder<String>, String> {

	@Autowired
	private IRequestTypeAttributeService requestTypeAttributeService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public RequestTypeAttributeModel create(RequestTypeAttributeModel model) {
		validateModel(model);
		RequestTypeAttribute reqTypeAttribute = requestTypeAttributeService
					.create((RequestTypeAttribute) conversionService.convert(model, forObject(model), valueOf(RequestTypeAttribute.class)));
			model = convertToRequestTypeAttributeModel(reqTypeAttribute);
			model.setStatusMessage("SUCCESS::: RequestTypeAttribute created successfully");
		return model;
	}

	private RequestTypeAttributeModel convertToRequestTypeAttributeModel(RequestTypeAttribute reqTypeAttribute) {
		return (RequestTypeAttributeModel) conversionService.convert(reqTypeAttribute, forObject(reqTypeAttribute), valueOf(RequestTypeAttributeModel.class));
	}

	private void validateModel(RequestTypeAttributeModel model) {
		Validate.notNull(model, "Invalid Input");
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, RequestTypeAttributeContext context) {
		requestTypeAttributeService.deleteRequestTypeAttribute(context.getRequestTypeAttributeId());
	}

	@Override
	public RequestTypeAttributeModel edit(IKeyBuilder<String> keyBuilder, RequestTypeAttributeModel model) {
		RequestTypeAttribute requestTypeAttribute = requestTypeAttributeService.getRequestTypeAttribute(keyBuilder.build().toString());
		requestTypeAttribute = requestTypeAttributeService
				.updateRequestTypeAttribute((RequestTypeAttribute) conversionService.convert(model, forObject(model), valueOf(RequestTypeAttribute.class)));
		model = convertToRequestTypeAttributeModel(requestTypeAttribute);
		model.setStatusMessage("SUCCESS:::  RequestTypeAttribute updated successfully");
		return model;
	}

	@Override
	public RequestTypeAttributeModel getByKey(IKeyBuilder<String> keyBuilder, RequestTypeAttributeContext context) {
		RequestTypeAttribute requestTypeAttribute = requestTypeAttributeService.getRequestTypeAttribute(keyBuilder.build().toString());
		RequestTypeAttributeModel model = conversionService.convert(requestTypeAttribute, RequestTypeAttributeModel.class);
		return model;
	}

	@Override
	public Collection<RequestTypeAttributeModel> getCollection(RequestTypeAttributeContext context) {
		List<RequestTypeAttribute> requestTypeAttribute = new ArrayList<RequestTypeAttribute>();
		if(context.getRequestTypeAttributeList()!=null){
			requestTypeAttribute = requestTypeAttributeService.getRequestTypeList();
		}
		if(context.getRequestTypeId()!=null){
			requestTypeAttribute = requestTypeAttributeService.getRequestTypeAttributeByRequestType(context.getRequestTypeId());
		}
		List<RequestTypeAttributeModel> requestTypeAttributeModels = (List<RequestTypeAttributeModel>) conversionService.convert(
				requestTypeAttribute, TypeDescriptor.forObject(requestTypeAttribute),
				TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(RequestTypeAttributeModel.class)));
		return requestTypeAttributeModels;
	}

	@Override
	public RequestTypeAttributeModel edit(IKeyBuilder<String> keyBuilder,
			RequestTypeAttributeModel model, RequestTypeAttributeContext context) {
		return null;
	}

	
	}


