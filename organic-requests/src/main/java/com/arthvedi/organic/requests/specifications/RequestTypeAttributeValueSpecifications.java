package com.arthvedi.organic.requests.specifications;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.arthvedi.organic.requests.domain.RequestTypeAttributeValue;

public class RequestTypeAttributeValueSpecifications {
	public static Specification<RequestTypeAttributeValue> byRequestTypeAttribute(String requestTypeAttributeId) {

		return new Specification<RequestTypeAttributeValue>() {

			@Override
			public Predicate toPredicate(Root<RequestTypeAttributeValue> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.join("requestTypeAttribute").get("id"), requestTypeAttributeId);
			}

		};
	}
}
