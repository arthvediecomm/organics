package com.arthvedi.organic.requests.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;

/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RequestContext implements IBusinessDelegateContext {

	private String requestId;
	private String requestList;
	private String all;
	private String bnvUserId;

	public String getRequestList() {
		return requestList;
	}

	public void setRequestList(String requestList) {
		this.requestList = requestList;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

	public String getBnvUserId() {
		return bnvUserId;
	}

	public void setBnvUserId(String bnvUserId) {
		this.bnvUserId = bnvUserId;
	}

}
