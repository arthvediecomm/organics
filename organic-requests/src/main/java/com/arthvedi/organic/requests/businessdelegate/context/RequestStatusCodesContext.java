package com.arthvedi.organic.requests.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;
/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RequestStatusCodesContext implements IBusinessDelegateContext {

	private String requestStatusCodesId;

	
	
	public String getRequestStatusCodesId() {
		return requestStatusCodesId;
	}

	public void setRequestStatusCodesId(String requestStatusCodesId) {
		this.requestStatusCodesId = requestStatusCodesId;
	}

}
