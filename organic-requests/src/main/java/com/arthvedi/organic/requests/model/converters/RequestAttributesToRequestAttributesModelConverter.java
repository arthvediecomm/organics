package com.arthvedi.organic.requests.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.requests.domain.RequestAttributes;
import com.arthvedi.organic.requests.model.RequestAttributesModel;

@Component("requestAttributesToRequestAttributesModelConverter")
public class RequestAttributesToRequestAttributesModelConverter implements
		Converter<RequestAttributes, RequestAttributesModel> {
	@Autowired
	private ObjectFactory<RequestAttributesModel> requestAttributesModelFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public RequestAttributesModel convert(final RequestAttributes source) {
		RequestAttributesModel requestAttributesModel = requestAttributesModelFactory
				.getObject();
		BeanUtils.copyProperties(source, requestAttributesModel);

		return requestAttributesModel;
	}

	@Autowired
	public void setRequestAttributesModelFactory(
			final ObjectFactory<RequestAttributesModel> requestAttributesModelFactory) {
		this.requestAttributesModelFactory = requestAttributesModelFactory;
	}
}
