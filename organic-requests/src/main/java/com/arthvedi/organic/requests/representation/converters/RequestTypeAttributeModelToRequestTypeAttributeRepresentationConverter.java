/**
 *
 */
package com.arthvedi.organic.requests.representation.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.requests.model.RequestTypeAttributeModel;
import com.arthvedi.organic.requests.representation.siren.RequestTypeAttributeRepresentation;
import com.arthvedi.organic.requests.representation.siren.RequestTypeAttributeValueRepresentation;

/**
 * @author varma
 *
 */
@Component("requestTypeAttributeModelToRequestTypeAttributeRepresentationConverter")
public class RequestTypeAttributeModelToRequestTypeAttributeRepresentationConverter extends PropertyCopyingConverter<RequestTypeAttributeModel, RequestTypeAttributeRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public RequestTypeAttributeRepresentation convert(final RequestTypeAttributeModel source) {
        RequestTypeAttributeRepresentation target = super.convert(source);
        if (CollectionUtils.isNotEmpty(source.getRequestTypeAttributeValueModels())) {
			List<RequestTypeAttributeValueRepresentation> converted = (List<RequestTypeAttributeValueRepresentation>) conversionService.convert(
					source.getRequestTypeAttributeValueModels(), TypeDescriptor.forObject(source.getRequestTypeAttributeValueModels()),
					CollectionTypeDescriptor.forType(TypeDescriptor.valueOf(List.class), RequestTypeAttributeValueRepresentation.class));
			target.getRequestTypeAttributeValueRep().addAll(converted);
		}
        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<RequestTypeAttributeRepresentation> factory) {
        super.setFactory(factory);
    }

}
