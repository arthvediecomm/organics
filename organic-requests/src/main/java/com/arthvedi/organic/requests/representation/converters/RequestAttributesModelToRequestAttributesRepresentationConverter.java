/**
 *
 */
package com.arthvedi.organic.requests.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.requests.model.RequestAttributesModel;
import com.arthvedi.organic.requests.representation.siren.RequestAttributesRepresentation;

/**
 * @author varma
 *
 */
@Component("requestAttributesModelToRequestAttributesRepresentationConverter")
public class RequestAttributesModelToRequestAttributesRepresentationConverter extends PropertyCopyingConverter<RequestAttributesModel, RequestAttributesRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public RequestAttributesRepresentation convert(final RequestAttributesModel source) {

        RequestAttributesRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<RequestAttributesRepresentation> factory) {
        super.setFactory(factory);
    }

}
