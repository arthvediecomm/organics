package com.arthvedi.organic.requests.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.requests.businessdelegate.context.RequestTypeContext;
import com.arthvedi.organic.requests.model.RequestTypeModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/requesttype", produces = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class RequestTypeController {

	private IBusinessDelegate<RequestTypeModel, RequestTypeContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<RequestTypeContext> requestTypeContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/admin/create")
	public ResponseEntity<RequestTypeModel> createRequestType(
			@RequestBody RequestTypeModel requestTypeModel) {
		requestTypeModel = businessDelegate.create(requestTypeModel);
		return new ResponseEntity<RequestTypeModel>(requestTypeModel,
				HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	public ResponseEntity<RequestTypeModel> edit(
			@PathVariable(value = "id") final String requestTypeId,
			@RequestBody RequestTypeModel requestTypeModel) {
		requestTypeModel = businessDelegate.edit(keyBuilderFactory.getObject()
				.withId(requestTypeId), requestTypeModel);
		return new ResponseEntity<RequestTypeModel>(requestTypeModel,
				HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}/delete", consumes = { MediaType.ALL_VALUE })
	public void deleteRequestType(
			@PathVariable(value = "id") final String requestTypeId) {
		RequestTypeContext requestTypeContext = requestTypeContextFactory
				.getObject();
		requestTypeContext.setRequestTypeId(requestTypeId);
		businessDelegate.delete(
				keyBuilderFactory.getObject().withId(requestTypeId),
				requestTypeContext);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all")
	public ResponseEntity<IModelWrapper<Collection<RequestTypeModel>>> getAll() {
		RequestTypeContext requestTypeContext = requestTypeContextFactory
				.getObject();
		requestTypeContext.setAll("all");
		Collection<RequestTypeModel> requestTypeModels = businessDelegate
				.getCollection(requestTypeContext);
		IModelWrapper<Collection<RequestTypeModel>> models = new CollectionModelWrapper<RequestTypeModel>(
				RequestTypeModel.class, requestTypeModels);
		return new ResponseEntity<IModelWrapper<Collection<RequestTypeModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = { "/requesttypelist",
			"/sellerapp/requesttypelist" })
	public ResponseEntity<IModelWrapper<Collection<RequestTypeModel>>> getAllRequestTypeList() {
		RequestTypeContext requestTypeContext = requestTypeContextFactory
				.getObject();
		requestTypeContext.setRequestTypeList("requestTypeList");
		Collection<RequestTypeModel> requestTypeModels = businessDelegate
				.getCollection(requestTypeContext);
		IModelWrapper<Collection<RequestTypeModel>> models = new CollectionModelWrapper<RequestTypeModel>(
				RequestTypeModel.class, requestTypeModels);
		return new ResponseEntity<IModelWrapper<Collection<RequestTypeModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<RequestTypeModel> getRequestType(
			@PathVariable(value = "id") final String requestTypeId) {
		RequestTypeContext requestTypeContext = requestTypeContextFactory
				.getObject();

		RequestTypeModel model = businessDelegate.getByKey(keyBuilderFactory
				.getObject().withId(requestTypeId), requestTypeContext);
		return new ResponseEntity<RequestTypeModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "requestTypeBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<RequestTypeModel, RequestTypeContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setRequestTypeObjectFactory(
			final ObjectFactory<RequestTypeContext> requestTypeContextFactory) {
		this.requestTypeContextFactory = requestTypeContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(
			final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}
}
