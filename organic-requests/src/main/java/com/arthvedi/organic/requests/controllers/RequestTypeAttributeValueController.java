package com.arthvedi.organic.requests.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.requests.businessdelegate.context.RequestTypeAttributeValueContext;
import com.arthvedi.organic.requests.model.RequestTypeAttributeValueModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/requesttypeattributevalue", produces = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class RequestTypeAttributeValueController {

	private IBusinessDelegate<RequestTypeAttributeValueModel, RequestTypeAttributeValueContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<RequestTypeAttributeValueContext> requestTypeAttributeValueContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<RequestTypeAttributeValueModel> createRequestTypeAttributeValue(
			@RequestBody RequestTypeAttributeValueModel requestTypeAttributeValueModel) {
		requestTypeAttributeValueModel = businessDelegate
				.create(requestTypeAttributeValueModel);
		return new ResponseEntity<RequestTypeAttributeValueModel>(
				requestTypeAttributeValueModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	public ResponseEntity<RequestTypeAttributeValueModel> edit(
			@PathVariable(value = "id") final String requestTypeAttributeValueId,
			@RequestBody RequestTypeAttributeValueModel requestTypeAttributeValueModel) {
		requestTypeAttributeValueModel = businessDelegate.edit(
				keyBuilderFactory.getObject().withId(
						requestTypeAttributeValueId),
				requestTypeAttributeValueModel);
		return new ResponseEntity<RequestTypeAttributeValueModel>(
				requestTypeAttributeValueModel, HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}/delete", consumes = { MediaType.ALL_VALUE })
	public void deleteRequestTypeAttributeValue(
			@PathVariable(value = "id") final String requestTypeAttributeValueId) {
		RequestTypeAttributeValueContext requestTypeAttributeContext = requestTypeAttributeValueContextFactory
				.getObject();
		requestTypeAttributeContext
				.setRequestTypeAttributeValueId(requestTypeAttributeValueId);
		businessDelegate.delete(
				keyBuilderFactory.getObject().withId(
						requestTypeAttributeValueId),
				requestTypeAttributeContext);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<RequestTypeAttributeValueModel> getRequestTypeAttributeValue(
			@PathVariable(value = "id") final String requestTypeAttributeValueId) {
		RequestTypeAttributeValueContext requestTypeAttributeValueContext = requestTypeAttributeValueContextFactory
				.getObject();
		RequestTypeAttributeValueModel model = businessDelegate.getByKey(
				keyBuilderFactory.getObject().withId(
						requestTypeAttributeValueId),
				requestTypeAttributeValueContext);
		return new ResponseEntity<RequestTypeAttributeValueModel>(model,
				HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = {
			"/{requestTypeAttributeId}/requesttypeattributevaluelist",
			"/sellerapp/{requestTypeAttributeId}/requesttypeattributevaluelist" })
	public ResponseEntity<IModelWrapper<Collection<RequestTypeAttributeValueModel>>> getRequestTypeAttributeListByRequestTypeAttribute(
			@PathVariable(value = "requestTypeAttributeId") final String requestTypeAttributeId) {
		RequestTypeAttributeValueContext requestTypeAttributeValueContext = requestTypeAttributeValueContextFactory
				.getObject();
		requestTypeAttributeValueContext
				.setRequestTypeAttributeId(requestTypeAttributeId);
		Collection<RequestTypeAttributeValueModel> requestTypeAttributeValueModels = businessDelegate
				.getCollection(requestTypeAttributeValueContext);
		IModelWrapper<Collection<RequestTypeAttributeValueModel>> models = new CollectionModelWrapper<RequestTypeAttributeValueModel>(
				RequestTypeAttributeValueModel.class,
				requestTypeAttributeValueModels);
		return new ResponseEntity<IModelWrapper<Collection<RequestTypeAttributeValueModel>>>(
				models, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "requestTypeAttributeValueBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<RequestTypeAttributeValueModel, RequestTypeAttributeValueContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setRequestTypeAttributeValueObjectFactory(
			final ObjectFactory<RequestTypeAttributeValueContext> requestTypeAttributeValueContextFactory) {
		this.requestTypeAttributeValueContextFactory = requestTypeAttributeValueContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(
			final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
