package com.arthvedi.organic.requests.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.requests.businessdelegate.context.RequestTypeContext;
import com.arthvedi.organic.requests.domain.RequestType;
import com.arthvedi.organic.requests.model.RequestTypeModel;
import com.arthvedi.organic.requests.service.IRequestTypeService;

/*
 *@Author varma
 */

@Service
public class RequestTypeBusinessDelegate
		implements
		IBusinessDelegate<RequestTypeModel, RequestTypeContext, IKeyBuilder<String>, String> {

	@Autowired
	private IRequestTypeService requestTypeService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public RequestTypeModel create(RequestTypeModel model) {
		validateModel(model);
		RequestType reqType = requestTypeService
				.create((RequestType) conversionService.convert(model,
						forObject(model), valueOf(RequestType.class)));
		model = convertToRequestTypeModel(reqType);
		model.setStatusMessage("SUCCESS::: RequestType created successfully");
		return model;
	}

	private RequestTypeModel convertToRequestTypeModel(RequestType reqType) {
		return (RequestTypeModel) conversionService.convert(reqType,
				forObject(reqType), valueOf(RequestTypeModel.class));
	}

	private void validateModel(RequestTypeModel model) {
		Validate.notNull(model, "Invalid Input");
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			RequestTypeContext context) {
		requestTypeService.deleteRequestType(context.getRequestTypeId());
	}

	@Override
	public RequestTypeModel edit(IKeyBuilder<String> keyBuilder,
			RequestTypeModel model) {
		RequestType requestType = requestTypeService.getRequestType(keyBuilder
				.build().toString());
		requestType = requestTypeService
				.updateRequestType((RequestType) conversionService.convert(
						model, forObject(model), valueOf(RequestType.class)));
		model = convertToRequestTypeModel(requestType);
		model.setStatusMessage("SUCCESS:::  RequestType updated successfully");

		return model;
	}

	@Override
	public RequestTypeModel getByKey(IKeyBuilder<String> keyBuilder,
			RequestTypeContext context) {
		RequestType requestType = requestTypeService.getRequestType(keyBuilder
				.build().toString());
		RequestTypeModel model = conversionService.convert(requestType,
				RequestTypeModel.class);
		return model;
	}

	@Override
	public Collection<RequestTypeModel> getCollection(RequestTypeContext context) {
		List<RequestType> requestType = new ArrayList<RequestType>();
		if (context.getRequestTypeList() != null) {
			requestType = requestTypeService.getRequestTypeList();
		}
		List<RequestTypeModel> requestTypeModels = (List<RequestTypeModel>) conversionService
				.convert(
						requestType,
						TypeDescriptor.forObject(requestType),
						TypeDescriptor.collection(List.class,
								TypeDescriptor.valueOf(RequestTypeModel.class)));
		return requestTypeModels;
	}

	@Override
	public RequestTypeModel edit(IKeyBuilder<String> keyBuilder,
			RequestTypeModel model, RequestTypeContext context) {
		return null;
	}

}
