package com.arthvedi.organic.requests.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.requests.domain.RequestTypeAttributeValue;
import com.arthvedi.organic.requests.model.RequestTypeAttributeValueModel;

@Component("requestTypeAttributeValueToRequestTypeAttributeValueModelConverter")
public class RequestTypeAttributeValueToRequestTypeAttributeValueModelConverter
        implements Converter<RequestTypeAttributeValue, RequestTypeAttributeValueModel> {
    @Autowired
    private ObjectFactory<RequestTypeAttributeValueModel> requestTypeAttributeValueModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public RequestTypeAttributeValueModel convert(final RequestTypeAttributeValue source) {
        RequestTypeAttributeValueModel requestTypeAttributeValueModel = requestTypeAttributeValueModelFactory.getObject();
        BeanUtils.copyProperties(source, requestTypeAttributeValueModel);

        return requestTypeAttributeValueModel;
    }

    @Autowired
    public void setRequestTypeAttributeValueModelFactory(
            final ObjectFactory<RequestTypeAttributeValueModel> requestTypeAttributeValueModelFactory) {
        this.requestTypeAttributeValueModelFactory = requestTypeAttributeValueModelFactory;
    }
}
