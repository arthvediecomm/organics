/**
 *
 */
package com.arthvedi.organic.requests.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.requests.domain.RequestStatusCodes;
import com.arthvedi.organic.requests.model.RequestStatusCodesModel;

/**
 * @author Jay
 *
 */
@Component("requestStatusCodesModelToRequestStatusCodesConverter")
public class RequestStatusCodesModelToRequestStatusCodesConverter implements Converter<RequestStatusCodesModel, RequestStatusCodes> {
    @Autowired
    private ObjectFactory<RequestStatusCodes> requestStatusCodesFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public RequestStatusCodes convert(final RequestStatusCodesModel source) {
        RequestStatusCodes requestStatusCodes = requestStatusCodesFactory.getObject();
        BeanUtils.copyProperties(source, requestStatusCodes);

        return requestStatusCodes;
    }

    @Autowired
    public void setRequestStatusCodesFactory(final ObjectFactory<RequestStatusCodes> requestStatusCodesFactory) {
        this.requestStatusCodesFactory = requestStatusCodesFactory;
    }

}
