package com.arthvedi.organic.order.businessdelegate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.order.businessdelegate.context.PaymentGatewayModeContext;
import com.arthvedi.organic.order.domain.PaymentGatewayMode;
import com.arthvedi.organic.order.model.PaymentGatewayModeModel;
import com.arthvedi.organic.order.service.IPaymentGatewayModeService;

/*
 *@Author varma
 */

@Service
public class PaymentGatewayModeBusinessDelegate
		implements
		IBusinessDelegate<PaymentGatewayModeModel, PaymentGatewayModeContext, IKeyBuilder<String>, String> {

	@Autowired
	private IPaymentGatewayModeService paymentGatewayModeService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public PaymentGatewayModeModel create(PaymentGatewayModeModel model) {

		return null;
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			PaymentGatewayModeContext context) {

	}

	@Override
	public PaymentGatewayModeModel edit(IKeyBuilder<String> keyBuilder,
			PaymentGatewayModeModel model) {
		PaymentGatewayMode paymentGatewayMode = paymentGatewayModeService
				.getPaymentGatewayMode(keyBuilder.build().toString());

		paymentGatewayMode = paymentGatewayModeService
				.updatePaymentGatewayMode(paymentGatewayMode);

		return model;
	}

	@Override
	public PaymentGatewayModeModel getByKey(IKeyBuilder<String> keyBuilder,
			PaymentGatewayModeContext context) {
		PaymentGatewayMode paymentGatewayMode = paymentGatewayModeService
				.getPaymentGatewayMode(keyBuilder.build().toString());
		PaymentGatewayModeModel model = conversionService.convert(
				paymentGatewayMode, PaymentGatewayModeModel.class);
		return model;
	}

	@Override
	public Collection<PaymentGatewayModeModel> getCollection(
			PaymentGatewayModeContext context) {
		List<PaymentGatewayMode> paymentGatewayMode = new ArrayList<PaymentGatewayMode>();
		if (context.getAll() != null) {
			paymentGatewayMode = paymentGatewayModeService.getAll();
		}
		List<PaymentGatewayModeModel> paymentGatewayModeModels = (List<PaymentGatewayModeModel>) conversionService
				.convert(paymentGatewayMode, TypeDescriptor
						.forObject(paymentGatewayMode), TypeDescriptor
						.collection(List.class, TypeDescriptor
								.valueOf(PaymentGatewayModeModel.class)));
		return paymentGatewayModeModels;
	}

	@Override
	public PaymentGatewayModeModel edit(IKeyBuilder<String> keyBuilder,
			PaymentGatewayModeModel model, PaymentGatewayModeContext context) {
		// TODO Auto-generated method stub
		return null;
	}

}
