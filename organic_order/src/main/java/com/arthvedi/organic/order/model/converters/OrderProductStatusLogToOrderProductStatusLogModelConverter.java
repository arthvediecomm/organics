package com.arthvedi.organic.order.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.OrderProductStatusLog;
import com.arthvedi.organic.order.model.OrderProductStatusLogModel;

@Component("orderProductStatusLogToOrderProductStatusLogModelConverter")
public class OrderProductStatusLogToOrderProductStatusLogModelConverter
        implements Converter<OrderProductStatusLog, OrderProductStatusLogModel> {
    @Autowired
    private ObjectFactory<OrderProductStatusLogModel> orderProductStatusLogModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public OrderProductStatusLogModel convert(final OrderProductStatusLog source) {
        OrderProductStatusLogModel orderProductStatusLogModel = orderProductStatusLogModelFactory.getObject();
        BeanUtils.copyProperties(source, orderProductStatusLogModel);

        return orderProductStatusLogModel;
    }

    @Autowired
    public void setOrderProductStatusLogModelFactory(
            final ObjectFactory<OrderProductStatusLogModel> orderProductStatusLogModelFactory) {
        this.orderProductStatusLogModelFactory = orderProductStatusLogModelFactory;
    }
}
