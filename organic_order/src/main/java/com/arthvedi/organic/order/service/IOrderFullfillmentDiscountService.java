package com.arthvedi.organic.order.service;

import java.util.List;

import com.arthvedi.organic.order.domain.OrderFullfillmentDiscount;
/*
*@Author varma
*/
public interface IOrderFullfillmentDiscountService {
	
	OrderFullfillmentDiscount create(OrderFullfillmentDiscount orderFullfillmentDiscount);

	void deleteOrderFullfillmentDiscount(String orderFullfillmentDiscountId);

	OrderFullfillmentDiscount getOrderFullfillmentDiscount(String orderFullfillmentDiscountId);

	List<OrderFullfillmentDiscount> getAll();

	OrderFullfillmentDiscount updateOrderFullfillmentDiscount(OrderFullfillmentDiscount orderFullfillmentDiscount);
}
