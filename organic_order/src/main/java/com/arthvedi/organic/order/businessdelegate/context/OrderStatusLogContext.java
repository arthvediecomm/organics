package com.arthvedi.organic.order.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;
/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class OrderStatusLogContext implements IBusinessDelegateContext {

	private String all;
	private String orderStatusLogId;

	
	
	public String getOrderStatusLogId() {
		return orderStatusLogId;
	}

	public void setOrderStatusLogId(String orderStatusLogId) {
		this.orderStatusLogId = orderStatusLogId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

}
