/**
 *
 */
package com.arthvedi.organic.order.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.OrderProduct;
import com.arthvedi.organic.order.model.OrderProductModel;

/**
 * @author Jay
 *
 */
@Component("orderProductModelToOrderProductConverter")
public class OrderProductModelToOrderProductConverter implements Converter<OrderProductModel, OrderProduct> {
    @Autowired
    private ObjectFactory<OrderProduct> orderProductFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public OrderProduct convert(final OrderProductModel source) {
        OrderProduct orderProduct = orderProductFactory.getObject();
        BeanUtils.copyProperties(source, orderProduct);

        return orderProduct;
    }

    @Autowired
    public void setOrderProductFactory(final ObjectFactory<OrderProduct> orderProductFactory) {
        this.orderProductFactory = orderProductFactory;
    }

}
