package com.arthvedi.organic.order.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.order.businessdelegate.context.OrderChargesContext;
import com.arthvedi.organic.order.model.OrderChargesModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/ordercharges", produces = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class OrderChargesController {

	private IBusinessDelegate<OrderChargesModel, OrderChargesContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<OrderChargesContext> orderChargesContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create")
	public ResponseEntity<OrderChargesModel> createOrderCharges(
			@RequestBody OrderChargesModel orderChargesModel) {
		businessDelegate.create(orderChargesModel);
		return new ResponseEntity<OrderChargesModel>(orderChargesModel,
				HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	public ResponseEntity<OrderChargesModel> edit(
			@PathVariable(value = "id") String orderChargesId,
			@RequestBody OrderChargesModel orderChargesModel) {

		businessDelegate.edit(
				keyBuilderFactory.getObject().withId(orderChargesId),
				orderChargesModel);
		return new ResponseEntity<OrderChargesModel>(orderChargesModel,
				HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all")
	public ResponseEntity<IModelWrapper> getAll() {
		OrderChargesContext orderChargesContext = orderChargesContextFactory
				.getObject();
		orderChargesContext.setAll("all");
		Collection<OrderChargesModel> orderChargesModels = businessDelegate
				.getCollection(orderChargesContext);
		IModelWrapper<Collection<OrderChargesModel>> models = new CollectionModelWrapper<OrderChargesModel>(
				OrderChargesModel.class, orderChargesModels);
		return new ResponseEntity<IModelWrapper>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<OrderChargesModel> getOrderCharges(
			@PathVariable(value = "id") String orderChargesId) {
		OrderChargesContext orderChargesContext = orderChargesContextFactory
				.getObject();

		OrderChargesModel model = businessDelegate.getByKey(keyBuilderFactory
				.getObject().withId(orderChargesId), orderChargesContext);
		return new ResponseEntity<OrderChargesModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "orderChargesBusinessDelegate")
	public void setBusinessDelegate(
			IBusinessDelegate<OrderChargesModel, OrderChargesContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setOrderChargesObjectFactory(
			ObjectFactory<OrderChargesContext> orderChargesContextFactory) {
		this.orderChargesContextFactory = orderChargesContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
