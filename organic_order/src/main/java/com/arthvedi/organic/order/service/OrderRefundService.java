package com.arthvedi.organic.order.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.OrderRefund;
import com.arthvedi.organic.order.repository.OrderRefundRepository;
/*
*@Author varma
*/
@Component
public class OrderRefundService implements IOrderRefundService{

	@Autowired
	private OrderRefundRepository orderRefundRepository;
	@Override
	public OrderRefund create(OrderRefund orderRefund) {
		return orderRefundRepository.save(orderRefund);
	}

	@Override
	public void deleteOrderRefund(String orderRefundId) {
		
	}

	@Override
	public OrderRefund getOrderRefund(String orderRefundId) {
		 return orderRefundRepository.findOne(orderRefundId);
	}

	@Override
	public List<OrderRefund> getAll() {
		return null;
	}

	@Override
	public OrderRefund updateOrderRefund(OrderRefund orderRefund) {
	return orderRefundRepository.save(orderRefund);
	}

}
