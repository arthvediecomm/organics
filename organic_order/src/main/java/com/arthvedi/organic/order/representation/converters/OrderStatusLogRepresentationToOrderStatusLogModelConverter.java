/**
 *
 */
package com.arthvedi.organic.order.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.order.model.OrderStatusLogModel;
import com.arthvedi.organic.order.representation.siren.OrderStatusLogRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("orderStatusLogRepresentationToOrderStatusLogModelConverter")
public class OrderStatusLogRepresentationToOrderStatusLogModelConverter extends PropertyCopyingConverter<OrderStatusLogRepresentation, OrderStatusLogModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public OrderStatusLogModel convert(final OrderStatusLogRepresentation source) {

        OrderStatusLogModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<OrderStatusLogModel> factory) {
        super.setFactory(factory);
    }

}
