/**
 *
 */
package com.arthvedi.organic.order.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.order.model.SellerBranchInvoiceModel;
import com.arthvedi.organic.order.representation.siren.SellerBranchInvoiceRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("sellerBranchInvoiceRepresentationToSellerBranchInvoiceModelConverter")
public class SellerBranchInvoiceRepresentationToSellerBranchInvoiceModelConverter extends PropertyCopyingConverter<SellerBranchInvoiceRepresentation, SellerBranchInvoiceModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public SellerBranchInvoiceModel convert(final SellerBranchInvoiceRepresentation source) {

        SellerBranchInvoiceModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<SellerBranchInvoiceModel> factory) {
        super.setFactory(factory);
    }

}
