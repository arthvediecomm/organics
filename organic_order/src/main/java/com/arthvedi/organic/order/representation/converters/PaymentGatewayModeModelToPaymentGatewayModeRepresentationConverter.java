/**
 *
 */
package com.arthvedi.organic.order.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.order.model.PaymentGatewayModeModel;
import com.arthvedi.organic.order.representation.siren.PaymentGatewayModeRepresentation;

/**
 * @author varma
 *
 */
@Component("paymentGatewayModeModelToPaymentGatewayModeRepresentationConverter")
public class PaymentGatewayModeModelToPaymentGatewayModeRepresentationConverter extends PropertyCopyingConverter<PaymentGatewayModeModel, PaymentGatewayModeRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public PaymentGatewayModeRepresentation convert(final PaymentGatewayModeModel source) {

        PaymentGatewayModeRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<PaymentGatewayModeRepresentation> factory) {
        super.setFactory(factory);
    }

}
