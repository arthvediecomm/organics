/**
 *
 */
package com.arthvedi.organic.order.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.order.model.OrderDiscountModel;
import com.arthvedi.organic.order.representation.siren.OrderDiscountRepresentation;

/**
 * @author varma
 *
 */
@Component("orderDiscountModelToOrderDiscountRepresentationConverter")
public class OrderDiscountModelToOrderDiscountRepresentationConverter extends PropertyCopyingConverter<OrderDiscountModel, OrderDiscountRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public OrderDiscountRepresentation convert(final OrderDiscountModel source) {

        OrderDiscountRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<OrderDiscountRepresentation> factory) {
        super.setFactory(factory);
    }

}
