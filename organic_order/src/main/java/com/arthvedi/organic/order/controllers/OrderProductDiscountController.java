package com.arthvedi.organic.order.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.order.businessdelegate.context.OrderProductDiscountContext;
import com.arthvedi.organic.order.model.OrderProductDiscountModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/orderproductdiscount", produces = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class OrderProductDiscountController {

	private IBusinessDelegate<OrderProductDiscountModel, OrderProductDiscountContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<OrderProductDiscountContext> orderProductDiscountContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create")
	public ResponseEntity<OrderProductDiscountModel> createOrderProductDiscount(
			@RequestBody final OrderProductDiscountModel orderProductDiscountModel) {
		businessDelegate.create(orderProductDiscountModel);
		return new ResponseEntity<OrderProductDiscountModel>(
				orderProductDiscountModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	public ResponseEntity<OrderProductDiscountModel> edit(
			@PathVariable(value = "id") final String orderProductDiscountId,
			@RequestBody final OrderProductDiscountModel orderProductDiscountModel) {

		businessDelegate.edit(
				keyBuilderFactory.getObject().withId(orderProductDiscountId),
				orderProductDiscountModel);
		return new ResponseEntity<OrderProductDiscountModel>(
				orderProductDiscountModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all")
	public ResponseEntity<IModelWrapper> getAll() {
		OrderProductDiscountContext orderProductDiscountContext = orderProductDiscountContextFactory
				.getObject();
		orderProductDiscountContext.setAll("all");
		Collection<OrderProductDiscountModel> orderProductDiscountModels = businessDelegate
				.getCollection(orderProductDiscountContext);
		IModelWrapper<Collection<OrderProductDiscountModel>> models = new CollectionModelWrapper<OrderProductDiscountModel>(
				OrderProductDiscountModel.class, orderProductDiscountModels);
		return new ResponseEntity<IModelWrapper>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<OrderProductDiscountModel> getOrderProductDiscount(
			@PathVariable(value = "id") final String orderProductDiscountId) {
		OrderProductDiscountContext orderProductDiscountContext = orderProductDiscountContextFactory
				.getObject();

		OrderProductDiscountModel model = businessDelegate.getByKey(
				keyBuilderFactory.getObject().withId(orderProductDiscountId),
				orderProductDiscountContext);
		return new ResponseEntity<OrderProductDiscountModel>(model,
				HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "orderProductDiscountBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<OrderProductDiscountModel, OrderProductDiscountContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setOrderProductDiscountObjectFactory(
			final ObjectFactory<OrderProductDiscountContext> orderProductDiscountContextFactory) {
		this.orderProductDiscountContextFactory = orderProductDiscountContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(
			final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
