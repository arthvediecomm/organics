package com.arthvedi.organic.order.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.OrderProduct;
import com.arthvedi.organic.order.model.OrderProductModel;

@Component("orderProductToOrderProductModelConverter")
public class OrderProductToOrderProductModelConverter
        implements Converter<OrderProduct, OrderProductModel> {
    @Autowired
    private ObjectFactory<OrderProductModel> orderProductModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public OrderProductModel convert(final OrderProduct source) {
        OrderProductModel orderProductModel = orderProductModelFactory.getObject();
        BeanUtils.copyProperties(source, orderProductModel);

        return orderProductModel;
    }

    @Autowired
    public void setOrderProductModelFactory(
            final ObjectFactory<OrderProductModel> orderProductModelFactory) {
        this.orderProductModelFactory = orderProductModelFactory;
    }
}
