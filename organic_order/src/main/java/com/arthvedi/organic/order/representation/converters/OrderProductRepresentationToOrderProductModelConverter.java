/**
 *
 */
package com.arthvedi.organic.order.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.order.model.OrderProductModel;
import com.arthvedi.organic.order.representation.siren.OrderProductRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("orderProductRepresentationToOrderProductModelConverter")
public class OrderProductRepresentationToOrderProductModelConverter extends PropertyCopyingConverter<OrderProductRepresentation, OrderProductModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public OrderProductModel convert(final OrderProductRepresentation source) {

        OrderProductModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<OrderProductModel> factory) {
        super.setFactory(factory);
    }

}
