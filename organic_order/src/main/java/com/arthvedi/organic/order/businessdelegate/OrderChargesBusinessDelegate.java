package com.arthvedi.organic.order.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.order.businessdelegate.context.OrderChargesContext;
import com.arthvedi.organic.order.domain.OrderCharges;
import com.arthvedi.organic.order.model.OrderChargesModel;
import com.arthvedi.organic.order.service.IOrderChargesService;

/*
*@Author varma
*/

@Service
public class OrderChargesBusinessDelegate
		implements IBusinessDelegate<OrderChargesModel, OrderChargesContext, IKeyBuilder<String>, String> {

	@Autowired
	private IOrderChargesService orderChargesService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public OrderChargesModel create(OrderChargesModel model) {

		vaildateModel(model);
		OrderCharges orderCharges =orderChargesService.create((OrderCharges)conversionService.convert(model, forObject(model), valueOf(OrderCharges.class)));
		model=convertToOrderChargesModel(orderCharges);
		
		
		
		return model;
	}

	private OrderChargesModel convertToOrderChargesModel(OrderCharges orderCharges) {
	
		return (OrderChargesModel) conversionService.convert(orderCharges, forObject(orderCharges), valueOf(OrderCharges.class));
	}

	private void vaildateModel(OrderChargesModel model) {
		
		
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, OrderChargesContext context) {

	}

	@Override
	public OrderChargesModel edit(IKeyBuilder<String> keyBuilder, OrderChargesModel model) {
		OrderCharges orderCharges = orderChargesService.getOrderCharges(keyBuilder.build().toString());
		
		orderCharges = orderChargesService.updateOrderCharges(orderCharges);
		
		return model;
	}

	@Override
	public OrderChargesModel getByKey(IKeyBuilder<String> keyBuilder, OrderChargesContext context) {
		OrderCharges orderCharges = orderChargesService.getOrderCharges(keyBuilder.build().toString());
		OrderChargesModel model = conversionService.convert(orderCharges, OrderChargesModel.class);
		return model;
	}

	@Override
	public Collection<OrderChargesModel> getCollection(OrderChargesContext context) {
		List<OrderCharges> orderCharges = new ArrayList<OrderCharges>();
		if (context.getAll() != null) {
			orderCharges = orderChargesService.getAll();
		}
		List<OrderChargesModel> orderChargesModels = (List<OrderChargesModel>) conversionService.convert(
				orderCharges, TypeDescriptor.forObject(orderCharges),
				TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(OrderChargesModel.class)));
		return orderChargesModels;
	}

	@Override
	public OrderChargesModel edit(IKeyBuilder<String> keyBuilder,
			OrderChargesModel model, OrderChargesContext context) {
		return null;
	}

}
