package com.arthvedi.organic.order.service;

import java.util.List;

import com.arthvedi.organic.order.domain.OrderProductStatusLog;
/*
*@Author varma
*/
public interface IOrderProductStatusLogService {
	
	OrderProductStatusLog create(OrderProductStatusLog orderProductStatusLog);

	void deleteOrderProductStatusLog(String orderProductStatusLogId);

	OrderProductStatusLog getOrderProductStatusLog(String orderProductStatusLogId);

	List<OrderProductStatusLog> getAll();

	OrderProductStatusLog updateOrderProductStatusLog(OrderProductStatusLog orderProductStatusLog);
}
