package com.arthvedi.organic.order.service;

import java.util.List;

import com.arthvedi.organic.order.domain.OrderCharges;
/*
*@Author varma
*/
public interface IOrderChargesService {
	
	OrderCharges create(OrderCharges orderCharges);

	void deleteOrderCharges(String orderChargesId);

	OrderCharges getOrderCharges(String orderChargesId);

	List<OrderCharges> getAll();

	OrderCharges updateOrderCharges(OrderCharges orderCharges);
}
