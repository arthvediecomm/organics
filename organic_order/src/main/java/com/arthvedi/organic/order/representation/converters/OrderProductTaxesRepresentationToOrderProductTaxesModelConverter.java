/**
 *
 */
package com.arthvedi.organic.order.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.order.model.OrderProductTaxesModel;
import com.arthvedi.organic.order.representation.siren.OrderProductTaxesRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("orderProductTaxesRepresentationToOrderProductTaxesModelConverter")
public class OrderProductTaxesRepresentationToOrderProductTaxesModelConverter extends PropertyCopyingConverter<OrderProductTaxesRepresentation, OrderProductTaxesModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public OrderProductTaxesModel convert(final OrderProductTaxesRepresentation source) {

        OrderProductTaxesModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<OrderProductTaxesModel> factory) {
        super.setFactory(factory);
    }

}
