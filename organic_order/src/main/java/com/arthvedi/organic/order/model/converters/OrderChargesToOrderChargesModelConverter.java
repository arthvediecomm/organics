package com.arthvedi.organic.order.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.OrderCharges;
import com.arthvedi.organic.order.model.OrderChargesModel;

@Component("orderChargesToOrderChargesModelConverter")
public class OrderChargesToOrderChargesModelConverter implements
		Converter<OrderCharges, OrderChargesModel> {
	@Autowired
	private ObjectFactory<OrderChargesModel> orderChargesModelFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public OrderChargesModel convert(final OrderCharges source) {
		OrderChargesModel orderChargesModel = orderChargesModelFactory
				.getObject();
		BeanUtils.copyProperties(source, orderChargesModel);

		return orderChargesModel;
	}

	@Autowired
	public void setOrderChargesModelFactory(
			final ObjectFactory<OrderChargesModel> orderChargesModelFactory) {
		this.orderChargesModelFactory = orderChargesModelFactory;
	}
}
