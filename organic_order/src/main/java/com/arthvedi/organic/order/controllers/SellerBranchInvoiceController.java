package com.arthvedi.organic.order.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.order.businessdelegate.context.SellerBranchInvoiceContext;
import com.arthvedi.organic.order.model.SellerBranchInvoiceModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/sellerbranchinvoice", produces = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class SellerBranchInvoiceController {

	private IBusinessDelegate<SellerBranchInvoiceModel, SellerBranchInvoiceContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<SellerBranchInvoiceContext> sellerBranchInvoiceContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create")
	public ResponseEntity<SellerBranchInvoiceModel> createSellerBranchInvoice(
			@RequestBody final SellerBranchInvoiceModel sellerBranchInvoiceModel) {
		businessDelegate.create(sellerBranchInvoiceModel);
		return new ResponseEntity<SellerBranchInvoiceModel>(
				sellerBranchInvoiceModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	public ResponseEntity<SellerBranchInvoiceModel> edit(
			@PathVariable(value = "id") final String sellerBranchInvoiceId,
			@RequestBody final SellerBranchInvoiceModel sellerBranchInvoiceModel) {

		businessDelegate.edit(
				keyBuilderFactory.getObject().withId(sellerBranchInvoiceId),
				sellerBranchInvoiceModel);
		return new ResponseEntity<SellerBranchInvoiceModel>(
				sellerBranchInvoiceModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all")
	public ResponseEntity<IModelWrapper> getAll() {
		SellerBranchInvoiceContext sellerBranchInvoiceContext = sellerBranchInvoiceContextFactory
				.getObject();
		sellerBranchInvoiceContext.setAll("all");
		Collection<SellerBranchInvoiceModel> sellerBranchInvoiceModels = businessDelegate
				.getCollection(sellerBranchInvoiceContext);
		IModelWrapper<Collection<SellerBranchInvoiceModel>> models = new CollectionModelWrapper<SellerBranchInvoiceModel>(
				SellerBranchInvoiceModel.class, sellerBranchInvoiceModels);
		return new ResponseEntity<IModelWrapper>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<SellerBranchInvoiceModel> getSellerBranchInvoice(
			@PathVariable(value = "id") final String sellerBranchInvoiceId) {
		SellerBranchInvoiceContext sellerBranchInvoiceContext = sellerBranchInvoiceContextFactory
				.getObject();

		SellerBranchInvoiceModel model = businessDelegate.getByKey(
				keyBuilderFactory.getObject().withId(sellerBranchInvoiceId),
				sellerBranchInvoiceContext);
		return new ResponseEntity<SellerBranchInvoiceModel>(model,
				HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "sellerBranchInvoiceBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<SellerBranchInvoiceModel, SellerBranchInvoiceContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setSellerBranchInvoiceObjectFactory(
			final ObjectFactory<SellerBranchInvoiceContext> sellerBranchInvoiceContextFactory) {
		this.sellerBranchInvoiceContextFactory = sellerBranchInvoiceContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(
			final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
