package com.arthvedi.organic.order.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.order.businessdelegate.context.OrderDiscountContext;
import com.arthvedi.organic.order.domain.OrderDiscount;
import com.arthvedi.organic.order.model.OrderDiscountModel;
import com.arthvedi.organic.order.service.IOrderDiscountService;

@Service
public class OrderDiscountBusinessDelegate
		implements
		IBusinessDelegate<OrderDiscountModel, OrderDiscountContext, IKeyBuilder<String>, String> {

	@Autowired
	private IOrderDiscountService orderDiscountService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public OrderDiscountModel create(OrderDiscountModel model) {

		vaildateModel(model);
		OrderDiscount orderDiscount = orderDiscountService
				.create((OrderDiscount) conversionService.convert(model,
						forObject(model), valueOf(OrderDiscount.class)));
		model = convertToOrderDiscountModel(orderDiscount);
		return model;
	}

	private OrderDiscountModel convertToOrderDiscountModel(
			OrderDiscount orderDiscount) {

		return (OrderDiscountModel) conversionService.convert(orderDiscount,
				forObject(orderDiscount), valueOf(OrderDiscount.class));
	}

	private void vaildateModel(OrderDiscountModel model) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			OrderDiscountContext context) {

	}

	@Override
	public OrderDiscountModel edit(IKeyBuilder<String> keyBuilder,
			OrderDiscountModel model) {
		OrderDiscount orderDiscount = orderDiscountService
				.getOrderDiscount(keyBuilder.build().toString());

		orderDiscount = orderDiscountService.updateOrderDiscount(orderDiscount);

		return model;
	}

	@Override
	public OrderDiscountModel getByKey(IKeyBuilder<String> keyBuilder,
			OrderDiscountContext context) {
		OrderDiscount orderDiscount = orderDiscountService
				.getOrderDiscount(keyBuilder.build().toString());
		OrderDiscountModel model = conversionService.convert(orderDiscount,
				OrderDiscountModel.class);
		return model;
	}

	@Override
	public Collection<OrderDiscountModel> getCollection(
			OrderDiscountContext context) {
		List<OrderDiscount> orderDiscount = new ArrayList<OrderDiscount>();
		if (context.getAll() != null) {
			orderDiscount = orderDiscountService.getAll();
		}
		List<OrderDiscountModel> orderDiscountModels = (List<OrderDiscountModel>) conversionService
				.convert(orderDiscount,
						TypeDescriptor.forObject(orderDiscount), TypeDescriptor
								.collection(List.class, TypeDescriptor
										.valueOf(OrderDiscountModel.class)));
		return orderDiscountModels;
	}

	@Override
	public OrderDiscountModel edit(IKeyBuilder<String> keyBuilder,
			OrderDiscountModel model, OrderDiscountContext context) {
		// TODO Auto-generated method stub
		return null;
	}

}
