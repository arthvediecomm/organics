package com.arthvedi.organic.order.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.OrderProductTaxes;
import com.arthvedi.organic.order.model.OrderProductTaxesModel;

@Component("orderProductTaxesToOrderProductTaxesModelConverter")
public class OrderProductTaxesToOrderProductTaxesModelConverter
        implements Converter<OrderProductTaxes, OrderProductTaxesModel> {
    @Autowired
    private ObjectFactory<OrderProductTaxesModel> orderProductTaxesModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public OrderProductTaxesModel convert(final OrderProductTaxes source) {
        OrderProductTaxesModel orderProductTaxesModel = orderProductTaxesModelFactory.getObject();
        BeanUtils.copyProperties(source, orderProductTaxesModel);

        return orderProductTaxesModel;
    }

    @Autowired
    public void setOrderProductTaxesModelFactory(
            final ObjectFactory<OrderProductTaxesModel> orderProductTaxesModelFactory) {
        this.orderProductTaxesModelFactory = orderProductTaxesModelFactory;
    }
}
