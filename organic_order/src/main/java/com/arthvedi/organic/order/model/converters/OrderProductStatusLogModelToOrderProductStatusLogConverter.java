/**
 *
 */
package com.arthvedi.organic.order.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.OrderProductStatusLog;
import com.arthvedi.organic.order.model.OrderProductStatusLogModel;

/**
 * @author Jay
 *
 */
@Component("orderProductStatusLogModelToOrderProductStatusLogConverter")
public class OrderProductStatusLogModelToOrderProductStatusLogConverter implements Converter<OrderProductStatusLogModel, OrderProductStatusLog> {
    @Autowired
    private ObjectFactory<OrderProductStatusLog> orderProductStatusLogFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public OrderProductStatusLog convert(final OrderProductStatusLogModel source) {
        OrderProductStatusLog orderProductStatusLog = orderProductStatusLogFactory.getObject();
        BeanUtils.copyProperties(source, orderProductStatusLog);

        return orderProductStatusLog;
    }

    @Autowired
    public void setOrderProductStatusLogFactory(final ObjectFactory<OrderProductStatusLog> orderProductStatusLogFactory) {
        this.orderProductStatusLogFactory = orderProductStatusLogFactory;
    }

}
