package com.arthvedi.organic.order.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.PaymentGatewayMode;
import com.arthvedi.organic.order.repository.PaymentGatewayModeRepository;
/*
*@Author varma
*/
@Component
public class PaymentGatewayModeService implements IPaymentGatewayModeService{

	@Autowired
	private PaymentGatewayModeRepository paymentGatewayModeRepository;
	@Override
	public PaymentGatewayMode create(PaymentGatewayMode paymentGatewayMode) {
		return paymentGatewayModeRepository.save(paymentGatewayMode);
	}

	@Override
	public void deletePaymentGatewayMode(String paymentGatewayModeId) {
		
	}

	@Override
	public PaymentGatewayMode getPaymentGatewayMode(String paymentGatewayModeId) {
		 return paymentGatewayModeRepository.findOne(paymentGatewayModeId);
	}

	@Override
	public List<PaymentGatewayMode> getAll() {
		return null;
	}

	@Override
	public PaymentGatewayMode updatePaymentGatewayMode(PaymentGatewayMode paymentGatewayMode) {
	return paymentGatewayModeRepository.save(paymentGatewayMode);
	}

}
