/**
 *
 */
package com.arthvedi.organic.order.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.OrderProductDiscount;
import com.arthvedi.organic.order.model.OrderProductDiscountModel;

/**
 * @author Jay
 *
 */
@Component("orderProductDiscountModelToOrderProductDiscountConverter")
public class OrderProductDiscountModelToOrderProductDiscountConverter implements
		Converter<OrderProductDiscountModel, OrderProductDiscount> {
	@Autowired
	private ObjectFactory<OrderProductDiscount> orderProductDiscountFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public OrderProductDiscount convert(final OrderProductDiscountModel source) {
		OrderProductDiscount orderProductDiscount = orderProductDiscountFactory
				.getObject();
		BeanUtils.copyProperties(source, orderProductDiscount);

		return orderProductDiscount;
	}

	@Autowired
	public void setOrderProductDiscountFactory(
			final ObjectFactory<OrderProductDiscount> orderProductDiscountFactory) {
		this.orderProductDiscountFactory = orderProductDiscountFactory;
	}

}
