package com.arthvedi.organic.order.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.PaymentGateway;
import com.arthvedi.organic.order.repository.PaymentGatewayRepository;
/*
*@Author varma
*/
@Component
public class PaymentGatewayService implements IPaymentGatewayService{

	@Autowired
	private PaymentGatewayRepository paymentGatewayRepository;
	@Override
	public PaymentGateway create(PaymentGateway paymentGateway) {
		return paymentGatewayRepository.save(paymentGateway);
	}

	@Override
	public void deletePaymentGateway(String paymentGatewayId) {
		
	}

	@Override
	public PaymentGateway getPaymentGateway(String paymentGatewayId) {
		 return paymentGatewayRepository.findOne(paymentGatewayId);
	}

	@Override
	public List<PaymentGateway> getAll() {
		return null;
	}

	@Override
	public PaymentGateway updatePaymentGateway(PaymentGateway paymentGateway) {
	return paymentGatewayRepository.save(paymentGateway);
	}

}
