/**
 *
 */
package com.arthvedi.organic.order.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.order.model.OrderChargesModel;
import com.arthvedi.organic.order.representation.siren.OrderChargesRepresentation;

/**
 * @author varma
 *
 */
@Component("orderChargesModelToOrderChargesRepresentationConverter")
public class OrderChargesModelToOrderChargesRepresentationConverter extends PropertyCopyingConverter<OrderChargesModel, OrderChargesRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public OrderChargesRepresentation convert(final OrderChargesModel source) {

        OrderChargesRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<OrderChargesRepresentation> factory) {
        super.setFactory(factory);
    }

}
