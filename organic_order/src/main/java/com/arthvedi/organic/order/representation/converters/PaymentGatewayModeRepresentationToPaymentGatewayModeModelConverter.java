/**
 *
 */
package com.arthvedi.organic.order.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.order.model.PaymentGatewayModeModel;
import com.arthvedi.organic.order.representation.siren.PaymentGatewayModeRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("paymentGatewayModeRepresentationToPaymentGatewayModeModelConverter")
public class PaymentGatewayModeRepresentationToPaymentGatewayModeModelConverter extends PropertyCopyingConverter<PaymentGatewayModeRepresentation, PaymentGatewayModeModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public PaymentGatewayModeModel convert(final PaymentGatewayModeRepresentation source) {

        PaymentGatewayModeModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<PaymentGatewayModeModel> factory) {
        super.setFactory(factory);
    }

}
