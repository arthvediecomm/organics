package com.arthvedi.organic.order.service;

import java.util.List;

import com.arthvedi.organic.order.domain.OrderProductTaxes;
/*
*@Author varma
*/
public interface IOrderProductTaxesService {
	
	OrderProductTaxes create(OrderProductTaxes orderProductTaxes);

	void deleteOrderProductTaxes(String orderProductTaxesId);

	OrderProductTaxes getOrderProductTaxes(String orderProductTaxesId);

	List<OrderProductTaxes> getAll();

	OrderProductTaxes updateOrderProductTaxes(OrderProductTaxes orderProductTaxes);
}
