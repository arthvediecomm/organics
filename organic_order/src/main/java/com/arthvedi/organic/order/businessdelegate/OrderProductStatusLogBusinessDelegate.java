package com.arthvedi.organic.order.businessdelegate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.order.businessdelegate.context.OrderProductStatusLogContext;
import com.arthvedi.organic.order.domain.OrderProductStatusLog;
import com.arthvedi.organic.order.model.OrderProductStatusLogModel;
import com.arthvedi.organic.order.service.IOrderProductStatusLogService;

/*
 *@Author varma
 */

@Service
public class OrderProductStatusLogBusinessDelegate
		implements
		IBusinessDelegate<OrderProductStatusLogModel, OrderProductStatusLogContext, IKeyBuilder<String>, String> {

	@Autowired
	private IOrderProductStatusLogService orderProductStatusLogService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public OrderProductStatusLogModel create(OrderProductStatusLogModel model) {

		return null;
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			OrderProductStatusLogContext context) {

	}

	@Override
	public OrderProductStatusLogModel edit(IKeyBuilder<String> keyBuilder,
			OrderProductStatusLogModel model) {
		OrderProductStatusLog orderProductStatusLog = orderProductStatusLogService
				.getOrderProductStatusLog(keyBuilder.build().toString());

		orderProductStatusLog = orderProductStatusLogService
				.updateOrderProductStatusLog(orderProductStatusLog);

		return model;
	}

	@Override
	public OrderProductStatusLogModel getByKey(IKeyBuilder<String> keyBuilder,
			OrderProductStatusLogContext context) {
		OrderProductStatusLog orderProductStatusLog = orderProductStatusLogService
				.getOrderProductStatusLog(keyBuilder.build().toString());
		OrderProductStatusLogModel model = conversionService.convert(
				orderProductStatusLog, OrderProductStatusLogModel.class);
		return model;
	}

	@Override
	public Collection<OrderProductStatusLogModel> getCollection(
			OrderProductStatusLogContext context) {
		List<OrderProductStatusLog> orderProductStatusLog = new ArrayList<OrderProductStatusLog>();
		if (context.getAll() != null) {
			orderProductStatusLog = orderProductStatusLogService.getAll();
		}
		List<OrderProductStatusLogModel> orderProductStatusLogModels = (List<OrderProductStatusLogModel>) conversionService
				.convert(orderProductStatusLog, TypeDescriptor
						.forObject(orderProductStatusLog), TypeDescriptor
						.collection(List.class, TypeDescriptor
								.valueOf(OrderProductStatusLogModel.class)));
		return orderProductStatusLogModels;
	}

	@Override
	public OrderProductStatusLogModel edit(IKeyBuilder<String> keyBuilder,
			OrderProductStatusLogModel model,
			OrderProductStatusLogContext context) {
		return null;
	}

}
