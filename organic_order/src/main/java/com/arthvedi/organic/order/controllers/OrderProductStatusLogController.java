package com.arthvedi.organic.order.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.order.businessdelegate.context.OrderProductStatusLogContext;
import com.arthvedi.organic.order.model.OrderProductStatusLogModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/orderproductstatuslog", produces = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class OrderProductStatusLogController {

	private IBusinessDelegate<OrderProductStatusLogModel, OrderProductStatusLogContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<OrderProductStatusLogContext> orderProductStatusLogContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create")
	public ResponseEntity<OrderProductStatusLogModel> createOrderProductStatusLog(
			@RequestBody OrderProductStatusLogModel orderProductStatusLogModel) {
		businessDelegate.create(orderProductStatusLogModel);
		return new ResponseEntity<OrderProductStatusLogModel>(
				orderProductStatusLogModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	public ResponseEntity<OrderProductStatusLogModel> edit(
			@PathVariable(value = "id") String orderProductStatusLogId,
			@RequestBody OrderProductStatusLogModel orderProductStatusLogModel) {

		businessDelegate.edit(
				keyBuilderFactory.getObject().withId(orderProductStatusLogId),
				orderProductStatusLogModel);
		return new ResponseEntity<OrderProductStatusLogModel>(
				orderProductStatusLogModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all")
	public ResponseEntity<IModelWrapper> getAll() {
		OrderProductStatusLogContext orderProductStatusLogContext = orderProductStatusLogContextFactory
				.getObject();
		orderProductStatusLogContext.setAll("all");
		Collection<OrderProductStatusLogModel> orderProductStatusLogModels = businessDelegate
				.getCollection(orderProductStatusLogContext);
		IModelWrapper<Collection<OrderProductStatusLogModel>> models = new CollectionModelWrapper<OrderProductStatusLogModel>(
				OrderProductStatusLogModel.class, orderProductStatusLogModels);
		return new ResponseEntity<IModelWrapper>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<OrderProductStatusLogModel> getOrderProductStatusLog(
			@PathVariable(value = "id") String orderProductStatusLogId) {
		OrderProductStatusLogContext orderProductStatusLogContext = orderProductStatusLogContextFactory
				.getObject();

		OrderProductStatusLogModel model = businessDelegate.getByKey(
				keyBuilderFactory.getObject().withId(orderProductStatusLogId),
				orderProductStatusLogContext);
		return new ResponseEntity<OrderProductStatusLogModel>(model,
				HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "orderProductStatusLogBusinessDelegate")
	public void setBusinessDelegate(
			IBusinessDelegate<OrderProductStatusLogModel, OrderProductStatusLogContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setOrderProductStatusLogObjectFactory(
			final ObjectFactory<OrderProductStatusLogContext> orderProductStatusLogContextFactory) {
		this.orderProductStatusLogContextFactory = orderProductStatusLogContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
