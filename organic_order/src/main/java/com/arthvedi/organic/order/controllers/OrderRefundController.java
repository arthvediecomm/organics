package com.arthvedi.organic.order.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.order.businessdelegate.context.OrderRefundContext;
import com.arthvedi.organic.order.model.OrderRefundModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/orderrefund", produces = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class OrderRefundController {

	private IBusinessDelegate<OrderRefundModel, OrderRefundContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<OrderRefundContext> orderRefundContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create")
	public ResponseEntity<OrderRefundModel> createOrderRefund(
			@RequestBody final OrderRefundModel orderRefundModel) {
		businessDelegate.create(orderRefundModel);
		return new ResponseEntity<OrderRefundModel>(orderRefundModel,
				HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	public ResponseEntity<OrderRefundModel> edit(
			@PathVariable(value = "id") final String orderRefundId,
			@RequestBody final OrderRefundModel orderRefundModel) {

		businessDelegate.edit(
				keyBuilderFactory.getObject().withId(orderRefundId),
				orderRefundModel);
		return new ResponseEntity<OrderRefundModel>(orderRefundModel,
				HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all")
	public ResponseEntity<IModelWrapper> getAll() {
		OrderRefundContext orderRefundContext = orderRefundContextFactory
				.getObject();
		orderRefundContext.setAll("all");
		Collection<OrderRefundModel> orderRefundModels = businessDelegate
				.getCollection(orderRefundContext);
		IModelWrapper<Collection<OrderRefundModel>> models = new CollectionModelWrapper<OrderRefundModel>(
				OrderRefundModel.class, orderRefundModels);
		return new ResponseEntity<IModelWrapper>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<OrderRefundModel> getOrderRefund(
			@PathVariable(value = "id") final String orderRefundId) {
		OrderRefundContext orderRefundContext = orderRefundContextFactory
				.getObject();

		OrderRefundModel model = businessDelegate.getByKey(keyBuilderFactory
				.getObject().withId(orderRefundId), orderRefundContext);
		return new ResponseEntity<OrderRefundModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "orderRefundBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<OrderRefundModel, OrderRefundContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setOrderRefundObjectFactory(
			final ObjectFactory<OrderRefundContext> orderRefundContextFactory) {
		this.orderRefundContextFactory = orderRefundContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(
			final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
