/**
 *
 */
package com.arthvedi.organic.order.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.order.model.PaymentGatewayModel;
import com.arthvedi.organic.order.representation.siren.PaymentGatewayRepresentation;

/**
 * @author varma
 *
 */
@Component("paymentGatewayModelToPaymentGatewayRepresentationConverter")
public class PaymentGatewayModelToPaymentGatewayRepresentationConverter extends PropertyCopyingConverter<PaymentGatewayModel, PaymentGatewayRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public PaymentGatewayRepresentation convert(final PaymentGatewayModel source) {

        PaymentGatewayRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<PaymentGatewayRepresentation> factory) {
        super.setFactory(factory);
    }

}
