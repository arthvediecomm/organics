package com.arthvedi.organic.order.service;

import java.util.List;

import com.arthvedi.organic.order.domain.OrderFullfillment;
/*
*@Author varma
*/
public interface IOrderFullfillmentService {
	
	OrderFullfillment create(OrderFullfillment orderFullfillment);

	void deleteOrderFullfillment(String orderFullfillmentId);

	OrderFullfillment getOrderFullfillment(String orderFullfillmentId);

	List<OrderFullfillment> getAll();

	OrderFullfillment updateOrderFullfillment(OrderFullfillment orderFullfillment);
}
