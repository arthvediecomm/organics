package com.arthvedi.organic.order.service;

import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.OrderCharges;
import com.arthvedi.organic.order.repository.OrderChargesRepository;
/*
*@Author varma
*/
@Component
public class OrderChargesService implements IOrderChargesService{

	@Autowired
	private OrderChargesRepository orderChargesRepository;
	@Override
	public OrderCharges create(OrderCharges orderCharges) {
		orderCharges.setCreatedDate(new LocalDateTime());
		orderCharges.setUserCreated("Dinakar");
		
		return orderChargesRepository.save(orderCharges);
	}

	@Override
	public void deleteOrderCharges(String orderChargesId) {
		
	}

	@Override
	public OrderCharges getOrderCharges(String orderChargesId) {
		 return orderChargesRepository.findOne(orderChargesId);
	}

	@Override
	public List<OrderCharges> getAll() {
		return null;
	}

	@Override
	public OrderCharges updateOrderCharges(OrderCharges orderCharges) {
	return orderChargesRepository.save(orderCharges);
	}

}
