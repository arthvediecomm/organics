/**
 *
 */
package com.arthvedi.organic.order.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.OrderDiscount;
import com.arthvedi.organic.order.model.OrderDiscountModel;

/**
 * @author Jay
 *
 */
@Component("orderDiscountModelToOrderDiscountConverter")
public class OrderDiscountModelToOrderDiscountConverter implements
		Converter<OrderDiscountModel, OrderDiscount> {
	@Autowired
	private ObjectFactory<OrderDiscount> orderDiscountFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public OrderDiscount convert(final OrderDiscountModel source) {
		OrderDiscount orderDiscount = orderDiscountFactory.getObject();
		BeanUtils.copyProperties(source, orderDiscount);

		return orderDiscount;
	}

	@Autowired
	public void setOrderDiscountFactory(
			final ObjectFactory<OrderDiscount> orderDiscountFactory) {
		this.orderDiscountFactory = orderDiscountFactory;
	}

}
