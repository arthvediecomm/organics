package com.arthvedi.organic.order.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.order.businessdelegate.context.OrderStatusLogContext;
import com.arthvedi.organic.order.model.OrderStatusLogModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/orderstatuslog", produces = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class OrderStatusLogController {

	private IBusinessDelegate<OrderStatusLogModel, OrderStatusLogContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<OrderStatusLogContext> orderStatusLogContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create")
	public ResponseEntity<OrderStatusLogModel> createOrderStatusLog(
			@RequestBody final OrderStatusLogModel orderStatusLogModel) {
		businessDelegate.create(orderStatusLogModel);
		return new ResponseEntity<OrderStatusLogModel>(orderStatusLogModel,
				HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	public ResponseEntity<OrderStatusLogModel> edit(
			@PathVariable(value = "id") final String orderStatusLogId,
			@RequestBody final OrderStatusLogModel orderStatusLogModel) {

		businessDelegate.edit(
				keyBuilderFactory.getObject().withId(orderStatusLogId),
				orderStatusLogModel);
		return new ResponseEntity<OrderStatusLogModel>(orderStatusLogModel,
				HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all")
	public ResponseEntity<IModelWrapper> getAll() {
		OrderStatusLogContext orderStatusLogContext = orderStatusLogContextFactory
				.getObject();
		orderStatusLogContext.setAll("all");
		Collection<OrderStatusLogModel> orderStatusLogModels = businessDelegate
				.getCollection(orderStatusLogContext);
		IModelWrapper<Collection<OrderStatusLogModel>> models = new CollectionModelWrapper<OrderStatusLogModel>(
				OrderStatusLogModel.class, orderStatusLogModels);
		return new ResponseEntity<IModelWrapper>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<OrderStatusLogModel> getOrderStatusLog(
			@PathVariable(value = "id") final String orderStatusLogId) {
		OrderStatusLogContext orderStatusLogContext = orderStatusLogContextFactory
				.getObject();

		OrderStatusLogModel model = businessDelegate.getByKey(keyBuilderFactory
				.getObject().withId(orderStatusLogId), orderStatusLogContext);
		return new ResponseEntity<OrderStatusLogModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "orderStatusLogBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<OrderStatusLogModel, OrderStatusLogContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setOrderStatusLogObjectFactory(
			final ObjectFactory<OrderStatusLogContext> orderStatusLogContextFactory) {
		this.orderStatusLogContextFactory = orderStatusLogContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(
			final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
