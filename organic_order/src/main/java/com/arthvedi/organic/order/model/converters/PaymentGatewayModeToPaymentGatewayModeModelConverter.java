package com.arthvedi.organic.order.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.PaymentGatewayMode;
import com.arthvedi.organic.order.model.PaymentGatewayModeModel;

@Component("paymentGatewayModeToPaymentGatewayModeModelConverter")
public class PaymentGatewayModeToPaymentGatewayModeModelConverter
        implements Converter<PaymentGatewayMode, PaymentGatewayModeModel> {
    @Autowired
    private ObjectFactory<PaymentGatewayModeModel> paymentGatewayModeModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public PaymentGatewayModeModel convert(final PaymentGatewayMode source) {
        PaymentGatewayModeModel paymentGatewayModeModel = paymentGatewayModeModelFactory.getObject();
        BeanUtils.copyProperties(source, paymentGatewayModeModel);

        return paymentGatewayModeModel;
    }

    @Autowired
    public void setPaymentGatewayModeModelFactory(
            final ObjectFactory<PaymentGatewayModeModel> paymentGatewayModeModelFactory) {
        this.paymentGatewayModeModelFactory = paymentGatewayModeModelFactory;
    }
}
