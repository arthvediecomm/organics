package com.arthvedi.organic.order.businessdelegate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.order.businessdelegate.context.SellerBranchInvoiceContext;
import com.arthvedi.organic.order.model.SellerBranchInvoiceModel;
import com.arthvedi.organic.order.service.ISellerBranchInvoiceService;
import com.arthvedi.organic.seller.domain.SellerBranchInvoice;

/*
 *@Author varma
 */

@Service
public class SellerBranchInvoiceBusinessDelegate
		implements
		IBusinessDelegate<SellerBranchInvoiceModel, SellerBranchInvoiceContext, IKeyBuilder<String>, String> {

	@Autowired
	private ISellerBranchInvoiceService sellerBranchInvoiceService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerBranchInvoiceModel create(SellerBranchInvoiceModel model) {

		return null;
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			SellerBranchInvoiceContext context) {

	}

	@Override
	public SellerBranchInvoiceModel edit(IKeyBuilder<String> keyBuilder,
			SellerBranchInvoiceModel model) {
		SellerBranchInvoice sellerBranchInvoice = sellerBranchInvoiceService
				.getSellerBranchInvoice(keyBuilder.build().toString());

		sellerBranchInvoice = sellerBranchInvoiceService
				.updateSellerBranchInvoice(sellerBranchInvoice);

		return model;
	}

	@Override
	public SellerBranchInvoiceModel getByKey(IKeyBuilder<String> keyBuilder,
			SellerBranchInvoiceContext context) {
		SellerBranchInvoice sellerBranchInvoice = sellerBranchInvoiceService
				.getSellerBranchInvoice(keyBuilder.build().toString());
		SellerBranchInvoiceModel model = conversionService.convert(
				sellerBranchInvoice, SellerBranchInvoiceModel.class);
		return model;
	}

	@Override
	public Collection<SellerBranchInvoiceModel> getCollection(
			SellerBranchInvoiceContext context) {
		List<SellerBranchInvoice> sellerBranchInvoice = new ArrayList<SellerBranchInvoice>();
		if (context.getAll() != null) {
			sellerBranchInvoice = sellerBranchInvoiceService.getAll();
		}
		List<SellerBranchInvoiceModel> sellerBranchInvoiceModels = (List<SellerBranchInvoiceModel>) conversionService
				.convert(sellerBranchInvoice, TypeDescriptor
						.forObject(sellerBranchInvoice), TypeDescriptor
						.collection(List.class, TypeDescriptor
								.valueOf(SellerBranchInvoiceModel.class)));
		return sellerBranchInvoiceModels;
	}

	@Override
	public SellerBranchInvoiceModel edit(IKeyBuilder<String> keyBuilder,
			SellerBranchInvoiceModel model, SellerBranchInvoiceContext context) {
		return null;
	}

}
