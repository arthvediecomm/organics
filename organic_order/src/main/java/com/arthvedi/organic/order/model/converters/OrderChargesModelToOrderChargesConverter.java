/**
 *
 */
package com.arthvedi.organic.order.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.EcommerceOrder;
import com.arthvedi.organic.order.domain.OrderCharges;
import com.arthvedi.organic.order.model.OrderChargesModel;

/**
 * @author Jay
 *
 */
@Component("orderChargesModelToOrderChargesConverter")
public class OrderChargesModelToOrderChargesConverter implements
		Converter<OrderChargesModel, OrderCharges> {
	@Autowired
	private ObjectFactory<OrderCharges> orderChargesFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public OrderCharges convert(final OrderChargesModel source) {
		OrderCharges orderCharges = orderChargesFactory.getObject();
		BeanUtils.copyProperties(source, orderCharges);

		if (source.getOrderId() != null) {
			EcommerceOrder ecommerceOrder = new EcommerceOrder();
			ecommerceOrder.setId(source.getOrderId());
			orderCharges.setEcommerceOrder(ecommerceOrder);
		}

		return orderCharges;

	}

	@Autowired
	public void setOrderChargesFactory(
			final ObjectFactory<OrderCharges> orderChargesFactory) {
		this.orderChargesFactory = orderChargesFactory;
	}

}
