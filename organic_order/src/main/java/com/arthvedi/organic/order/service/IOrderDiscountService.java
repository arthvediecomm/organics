package com.arthvedi.organic.order.service;

import java.util.List;

import com.arthvedi.organic.order.domain.OrderDiscount;
/*
*@Author varma
*/
public interface IOrderDiscountService {
	
	OrderDiscount create(OrderDiscount orderDiscount);

	void deleteOrderDiscount(String orderDiscountId);

	OrderDiscount getOrderDiscount(String orderDiscountId);

	List<OrderDiscount> getAll();

	OrderDiscount updateOrderDiscount(OrderDiscount orderDiscount);
}
