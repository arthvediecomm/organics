/**
 *
 */
package com.arthvedi.organic.order.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.order.model.OrderProductFeaturesModel;
import com.arthvedi.organic.order.representation.siren.OrderProductFeaturesRepresentation;

/**
 * @author varma
 *
 */
@Component("orderProductFeaturesModelToOrderProductFeaturesRepresentationConverter")
public class OrderProductFeaturesModelToOrderProductFeaturesRepresentationConverter extends PropertyCopyingConverter<OrderProductFeaturesModel, OrderProductFeaturesRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public OrderProductFeaturesRepresentation convert(final OrderProductFeaturesModel source) {

        OrderProductFeaturesRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<OrderProductFeaturesRepresentation> factory) {
        super.setFactory(factory);
    }

}
