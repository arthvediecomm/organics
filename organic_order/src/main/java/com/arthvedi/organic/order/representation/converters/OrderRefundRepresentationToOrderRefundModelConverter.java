/**
 *
 */
package com.arthvedi.organic.order.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.order.model.OrderRefundModel;
import com.arthvedi.organic.order.representation.siren.OrderRefundRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("orderRefundRepresentationToOrderRefundModelConverter")
public class OrderRefundRepresentationToOrderRefundModelConverter extends PropertyCopyingConverter<OrderRefundRepresentation, OrderRefundModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public OrderRefundModel convert(final OrderRefundRepresentation source) {

        OrderRefundModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<OrderRefundModel> factory) {
        super.setFactory(factory);
    }

}
