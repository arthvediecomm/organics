package com.arthvedi.organic.order.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.OrderFullfillment;
import com.arthvedi.organic.order.repository.OrderFullfillmentRepository;
/*
*@Author varma
*/
@Component
public class OrderFullfillmentService implements IOrderFullfillmentService{

	@Autowired
	private OrderFullfillmentRepository orderFullfillmentRepository;
	@Override
	public OrderFullfillment create(OrderFullfillment orderFullfillment) {
		return orderFullfillmentRepository.save(orderFullfillment);
	}

	@Override
	public void deleteOrderFullfillment(String orderFullfillmentId) {
		
	}

	@Override
	public OrderFullfillment getOrderFullfillment(String orderFullfillmentId) {
		 return orderFullfillmentRepository.findOne(orderFullfillmentId);
	}

	@Override
	public List<OrderFullfillment> getAll() {
		return null;
	}

	@Override
	public OrderFullfillment updateOrderFullfillment(OrderFullfillment orderFullfillment) {
	return orderFullfillmentRepository.save(orderFullfillment);
	}

}
