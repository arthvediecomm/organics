package com.arthvedi.organic.order.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.OrderProduct;
import com.arthvedi.organic.order.repository.OrderProductRepository;
/*
*@Author varma
*/
@Component
public class OrderProductService implements IOrderProductService{

	@Autowired
	private OrderProductRepository orderProductRepository;
	@Override
	public OrderProduct create(OrderProduct orderProduct) {
		// TODO Auto-generated method stub
		return orderProductRepository.save(orderProduct);
	}

	@Override
	public void deleteOrderProduct(String orderProductId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public OrderProduct getOrderProduct(String orderProductId) {
		// TODO Auto-generated method stub
		 return orderProductRepository.findOne(orderProductId);
	}

	@Override
	public List<OrderProduct> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OrderProduct updateOrderProduct(OrderProduct orderProduct) {
		// TODO Auto-generated method stub
	return orderProductRepository.save(orderProduct);
	}

}
