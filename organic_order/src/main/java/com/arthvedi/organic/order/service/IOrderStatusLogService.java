package com.arthvedi.organic.order.service;

import java.util.List;

import com.arthvedi.organic.order.domain.OrderStatusLog;
/*
*@Author varma
*/
public interface IOrderStatusLogService {
	
	OrderStatusLog create(OrderStatusLog orderStatusLog);

	void deleteOrderStatusLog(String orderStatusLogId);

	OrderStatusLog getOrderStatusLog(String orderStatusLogId);

	List<OrderStatusLog> getAll();

	OrderStatusLog updateOrderStatusLog(OrderStatusLog orderStatusLog);
}
