/**
 *
 */
package com.arthvedi.organic.order.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.OrderProductTaxes;
import com.arthvedi.organic.order.model.OrderProductTaxesModel;

/**
 * @author Jay
 *
 */
@Component("orderProductTaxesModelToOrderProductTaxesConverter")
public class OrderProductTaxesModelToOrderProductTaxesConverter implements Converter<OrderProductTaxesModel, OrderProductTaxes> {
    @Autowired
    private ObjectFactory<OrderProductTaxes> orderProductTaxesFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public OrderProductTaxes convert(final OrderProductTaxesModel source) {
        OrderProductTaxes orderProductTaxes = orderProductTaxesFactory.getObject();
        BeanUtils.copyProperties(source, orderProductTaxes);

        return orderProductTaxes;
    }

    @Autowired
    public void setOrderProductTaxesFactory(final ObjectFactory<OrderProductTaxes> orderProductTaxesFactory) {
        this.orderProductTaxesFactory = orderProductTaxesFactory;
    }

}
