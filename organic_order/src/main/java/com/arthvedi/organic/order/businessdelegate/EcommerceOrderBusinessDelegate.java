package com.arthvedi.organic.order.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.order.businessdelegate.context.EcommerceOrderContext;
import com.arthvedi.organic.order.domain.EcommerceOrder;
import com.arthvedi.organic.order.model.EcommerceOrderModel;
import com.arthvedi.organic.order.service.IEcommerceOrderService;

/*
 *@Author varma
 */

@Service
public class EcommerceOrderBusinessDelegate
		implements
		IBusinessDelegate<EcommerceOrderModel, EcommerceOrderContext, IKeyBuilder<String>, String> {

	@Autowired
	private IEcommerceOrderService EcommerceOrderService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public EcommerceOrderModel create(EcommerceOrderModel model) {

		vaildateModel(model);
		EcommerceOrder EcommerceOrder = EcommerceOrderService
				.create((EcommerceOrder) conversionService.convert(model,
						forObject(model), valueOf(EcommerceOrder.class)));
		model = convertToEcommerceOrderModel(EcommerceOrder);

		return model;
	}

	private EcommerceOrderModel convertToEcommerceOrderModel(
			EcommerceOrder ecommerceOrder) {
		return (EcommerceOrderModel) conversionService.convert(ecommerceOrder,
				forObject(ecommerceOrder), valueOf(EcommerceOrderModel.class));

	}

	private void vaildateModel(EcommerceOrderModel model) {

	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			EcommerceOrderContext context) {

	}

	@Override
	public EcommerceOrderModel edit(IKeyBuilder<String> keyBuilder,
			EcommerceOrderModel model) {
		EcommerceOrder ecommerceOrder = EcommerceOrderService
				.getEcommerceOrder(keyBuilder.build().toString());

		ecommerceOrder = EcommerceOrderService
				.updateEcommerceOrder((EcommerceOrder) conversionService
						.convert(model, forObject(model),
								valueOf(EcommerceOrder.class)));
		model = convertToEcommerceOrderModel(ecommerceOrder);

		return model;
	}

	@Override
	public EcommerceOrderModel getByKey(IKeyBuilder<String> keyBuilder,
			EcommerceOrderContext context) {
		EcommerceOrder ecommerceOrder = EcommerceOrderService
				.getEcommerceOrder(keyBuilder.build().toString());
		EcommerceOrderModel ecommerceOrderModel = conversionService.convert(
				ecommerceOrder, EcommerceOrderModel.class);
		return ecommerceOrderModel;
	}

	@Override
	public Collection<EcommerceOrderModel> getCollection(
			EcommerceOrderContext context) {
		List<EcommerceOrder> orders = new ArrayList<EcommerceOrder>();
		if (context.getAll() != null) {
			orders = EcommerceOrderService.getAll();
		}
		/*
		 * if(context.getSellerBranchId()!=null){ orders =
		 * EcommerceOrderService.get }
		 */
		List<EcommerceOrderModel> orderModels = (List<EcommerceOrderModel>) conversionService
				.convert(orders, TypeDescriptor.forObject(orders),
						TypeDescriptor.collection(List.class, TypeDescriptor
								.valueOf(EcommerceOrderModel.class)));
		return orderModels;
	}

	@Override
	public EcommerceOrderModel edit(IKeyBuilder<String> keyBuilder,
			EcommerceOrderModel model, EcommerceOrderContext context) {
		return null;
	}

}
