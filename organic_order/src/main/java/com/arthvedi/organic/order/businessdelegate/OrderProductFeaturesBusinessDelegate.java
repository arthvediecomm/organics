package com.arthvedi.organic.order.businessdelegate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.order.businessdelegate.context.OrderProductFeaturesContext;
import com.arthvedi.organic.order.domain.OrderProductFeatures;
import com.arthvedi.organic.order.model.OrderProductFeaturesModel;
import com.arthvedi.organic.order.service.IOrderProductFeaturesService;

/*
 *@Author varma
 */

@Service
public class OrderProductFeaturesBusinessDelegate
		implements
		IBusinessDelegate<OrderProductFeaturesModel, OrderProductFeaturesContext, IKeyBuilder<String>, String> {

	@Autowired
	private IOrderProductFeaturesService orderProductFeaturesService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public OrderProductFeaturesModel create(OrderProductFeaturesModel model) {

		return null;
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			OrderProductFeaturesContext context) {

	}

	@Override
	public OrderProductFeaturesModel edit(IKeyBuilder<String> keyBuilder,
			OrderProductFeaturesModel model) {
		OrderProductFeatures orderProductFeatures = orderProductFeaturesService
				.getOrderProductFeatures(keyBuilder.build().toString());

		orderProductFeatures = orderProductFeaturesService
				.updateOrderProductFeatures(orderProductFeatures);

		return model;
	}

	@Override
	public OrderProductFeaturesModel getByKey(IKeyBuilder<String> keyBuilder,
			OrderProductFeaturesContext context) {
		OrderProductFeatures orderProductFeatures = orderProductFeaturesService
				.getOrderProductFeatures(keyBuilder.build().toString());
		OrderProductFeaturesModel model = conversionService.convert(
				orderProductFeatures, OrderProductFeaturesModel.class);
		return model;
	}

	@Override
	public Collection<OrderProductFeaturesModel> getCollection(
			OrderProductFeaturesContext context) {
		List<OrderProductFeatures> orderProductFeatures = new ArrayList<OrderProductFeatures>();
		if (context.getAll() != null) {
			orderProductFeatures = orderProductFeaturesService.getAll();
		}
		List<OrderProductFeaturesModel> orderProductFeaturesModels = (List<OrderProductFeaturesModel>) conversionService
				.convert(orderProductFeatures, TypeDescriptor
						.forObject(orderProductFeatures), TypeDescriptor
						.collection(List.class, TypeDescriptor
								.valueOf(OrderProductFeaturesModel.class)));
		return orderProductFeaturesModels;
	}

	@Override
	public OrderProductFeaturesModel edit(IKeyBuilder<String> keyBuilder,
			OrderProductFeaturesModel model, OrderProductFeaturesContext context) {
		return null;
	}

}
