/**
 *
 */
package com.arthvedi.organic.order.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.PaymentGateway;
import com.arthvedi.organic.order.model.PaymentGatewayModel;

/**
 * @author Jay
 *
 */
@Component("paymentGatewayModelToPaymentGatewayConverter")
public class PaymentGatewayModelToPaymentGatewayConverter implements Converter<PaymentGatewayModel, PaymentGateway> {
    @Autowired
    private ObjectFactory<PaymentGateway> paymentGatewayFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public PaymentGateway convert(final PaymentGatewayModel source) {
        PaymentGateway paymentGateway = paymentGatewayFactory.getObject();
        BeanUtils.copyProperties(source, paymentGateway);

        return paymentGateway;
    }

    @Autowired
    public void setPaymentGatewayFactory(final ObjectFactory<PaymentGateway> paymentGatewayFactory) {
        this.paymentGatewayFactory = paymentGatewayFactory;
    }

}
