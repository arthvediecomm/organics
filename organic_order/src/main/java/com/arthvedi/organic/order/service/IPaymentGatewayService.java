package com.arthvedi.organic.order.service;

import java.util.List;

import com.arthvedi.organic.order.domain.PaymentGateway;
/*
*@Author varma
*/
public interface IPaymentGatewayService {
	
	PaymentGateway create(PaymentGateway paymentGateway);

	void deletePaymentGateway(String paymentGatewayId);

	PaymentGateway getPaymentGateway(String paymentGatewayId);

	List<PaymentGateway> getAll();

	PaymentGateway updatePaymentGateway(PaymentGateway paymentGateway);
}
