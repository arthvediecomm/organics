package com.arthvedi.organic.order.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.order.businessdelegate.context.OrderProductContext;
import com.arthvedi.organic.order.domain.OrderProduct;
import com.arthvedi.organic.order.model.OrderProductModel;
import com.arthvedi.organic.order.service.IOrderProductService;

/*
 *@Author varma
 */

@Service
public class OrderProductBusinessDelegate
		implements
		IBusinessDelegate<OrderProductModel, OrderProductContext, IKeyBuilder<String>, String> {

	@Autowired
	private IOrderProductService orderProductService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public OrderProductModel create(OrderProductModel model) {
		vaildateModel(model);
		OrderProduct orderProduct = orderProductService
				.create((OrderProduct) conversionService.convert(model,
						forObject(model), valueOf(OrderProduct.class)));
		model = convertToOrderProductModel(orderProduct);

		return model;
	}

	private OrderProductModel convertToOrderProductModel(
			OrderProduct orderProduct) {

		return (OrderProductModel) conversionService.convert(orderProduct,
				forObject(orderProduct), valueOf(OrderProduct.class));
	}

	private void vaildateModel(OrderProductModel model) {

	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			OrderProductContext context) {

	}

	@Override
	public OrderProductModel edit(IKeyBuilder<String> keyBuilder,
			OrderProductModel model) {
		OrderProduct orderProduct = orderProductService
				.getOrderProduct(keyBuilder.build().toString());

		orderProduct = orderProductService.updateOrderProduct(orderProduct);

		return model;
	}

	@Override
	public OrderProductModel getByKey(IKeyBuilder<String> keyBuilder,
			OrderProductContext context) {
		OrderProduct orderProduct = orderProductService
				.getOrderProduct(keyBuilder.build().toString());
		OrderProductModel model = conversionService.convert(orderProduct,
				OrderProductModel.class);
		return model;
	}

	@Override
	public Collection<OrderProductModel> getCollection(
			OrderProductContext context) {
		List<OrderProduct> orderProduct = new ArrayList<OrderProduct>();
		if (context.getAll() != null) {
			orderProduct = orderProductService.getAll();
		}
		List<OrderProductModel> orderProductModels = (List<OrderProductModel>) conversionService
				.convert(
						orderProduct,
						TypeDescriptor.forObject(orderProduct),
						TypeDescriptor.collection(List.class,
								TypeDescriptor.valueOf(OrderProductModel.class)));
		return orderProductModels;
	}

	@Override
	public OrderProductModel edit(IKeyBuilder<String> keyBuilder,
			OrderProductModel model, OrderProductContext context) {
		return null;
	}

}
