/**
 *
 */
package com.arthvedi.organic.order.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.OrderFullfillmentDiscount;
import com.arthvedi.organic.order.model.OrderFullfillmentDiscountModel;

/**
 * @author Jay
 *
 */
@Component("orderFullfillmentDiscountModelToOrderFullfillmentDiscountConverter")
public class OrderFullfillmentDiscountModelToOrderFullfillmentDiscountConverter implements Converter<OrderFullfillmentDiscountModel, OrderFullfillmentDiscount> {
    @Autowired
    private ObjectFactory<OrderFullfillmentDiscount> orderFullfillmentDiscountFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public OrderFullfillmentDiscount convert(final OrderFullfillmentDiscountModel source) {
        OrderFullfillmentDiscount orderFullfillmentDiscount = orderFullfillmentDiscountFactory.getObject();
        BeanUtils.copyProperties(source, orderFullfillmentDiscount);

        return orderFullfillmentDiscount;
    }

    @Autowired
    public void setOrderFullfillmentDiscountFactory(final ObjectFactory<OrderFullfillmentDiscount> orderFullfillmentDiscountFactory) {
        this.orderFullfillmentDiscountFactory = orderFullfillmentDiscountFactory;
    }

}
