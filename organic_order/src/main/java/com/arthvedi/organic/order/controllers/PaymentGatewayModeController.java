package com.arthvedi.organic.order.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.order.businessdelegate.context.PaymentGatewayModeContext;
import com.arthvedi.organic.order.model.PaymentGatewayModeModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/paymentgatewaymode", produces = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class PaymentGatewayModeController {

	private IBusinessDelegate<PaymentGatewayModeModel, PaymentGatewayModeContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<PaymentGatewayModeContext> paymentGatewayModeContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create")
	public ResponseEntity<PaymentGatewayModeModel> createPaymentGatewayMode(
			@RequestBody final PaymentGatewayModeModel paymentGatewayModeModel) {
		businessDelegate.create(paymentGatewayModeModel);
		return new ResponseEntity<PaymentGatewayModeModel>(
				paymentGatewayModeModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	public ResponseEntity<PaymentGatewayModeModel> edit(
			@PathVariable(value = "id") final String paymentGatewayModeId,
			@RequestBody final PaymentGatewayModeModel paymentGatewayModeModel) {

		businessDelegate.edit(
				keyBuilderFactory.getObject().withId(paymentGatewayModeId),
				paymentGatewayModeModel);
		return new ResponseEntity<PaymentGatewayModeModel>(
				paymentGatewayModeModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all")
	public ResponseEntity<IModelWrapper> getAll() {
		PaymentGatewayModeContext paymentGatewayModeContext = paymentGatewayModeContextFactory
				.getObject();
		paymentGatewayModeContext.setAll("all");
		Collection<PaymentGatewayModeModel> paymentGatewayModeModels = businessDelegate
				.getCollection(paymentGatewayModeContext);
		IModelWrapper<Collection<PaymentGatewayModeModel>> models = new CollectionModelWrapper<PaymentGatewayModeModel>(
				PaymentGatewayModeModel.class, paymentGatewayModeModels);
		return new ResponseEntity<IModelWrapper>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<PaymentGatewayModeModel> getPaymentGatewayMode(
			@PathVariable(value = "id") final String paymentGatewayModeId) {
		PaymentGatewayModeContext paymentGatewayModeContext = paymentGatewayModeContextFactory
				.getObject();

		PaymentGatewayModeModel model = businessDelegate.getByKey(
				keyBuilderFactory.getObject().withId(paymentGatewayModeId),
				paymentGatewayModeContext);
		return new ResponseEntity<PaymentGatewayModeModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "paymentGatewayModeBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<PaymentGatewayModeModel, PaymentGatewayModeContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setPaymentGatewayModeObjectFactory(
			final ObjectFactory<PaymentGatewayModeContext> paymentGatewayModeContextFactory) {
		this.paymentGatewayModeContextFactory = paymentGatewayModeContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(
			final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
