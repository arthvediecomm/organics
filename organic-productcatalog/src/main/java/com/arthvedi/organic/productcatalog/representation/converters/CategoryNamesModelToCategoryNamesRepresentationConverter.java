/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.productcatalog.model.CategoryNamesModel;
import com.arthvedi.organic.productcatalog.representation.siren.CategoryNamesRepresentation;

/**
 * @author varma
 *
 */
@Component("categoryNamesModelToCategoryNamesRepresentationConverter")
public class CategoryNamesModelToCategoryNamesRepresentationConverter extends PropertyCopyingConverter<CategoryNamesModel, CategoryNamesRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public CategoryNamesRepresentation convert(final CategoryNamesModel source) {

        CategoryNamesRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<CategoryNamesRepresentation> factory) {
        super.setFactory(factory);
    }

}
