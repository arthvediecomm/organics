package com.arthvedi.organic.productcatalog.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.productcatalog.businessdelegate.context.CategoryNamesContext;
import com.arthvedi.organic.productcatalog.model.CategoryNamesModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/categorynames", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class CategoryNamesController {

	private IBusinessDelegate<CategoryNamesModel, CategoryNamesContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<CategoryNamesContext> categoryNamesContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<CategoryNamesModel> createCategoryNames(@RequestBody final CategoryNamesModel categoryNamesModel) {
		businessDelegate.create(categoryNamesModel);
		return new ResponseEntity<CategoryNamesModel>(categoryNamesModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<CategoryNamesModel> edit(@PathVariable(value = "id") final String categoryNamesId,
			@RequestBody final CategoryNamesModel categoryNamesModel) {

		businessDelegate.edit(keyBuilderFactory.getObject().withId(categoryNamesId), categoryNamesModel);
		return new ResponseEntity<CategoryNamesModel>(categoryNamesModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<CategoryNamesModel>>> getAll() {
		CategoryNamesContext categoryNamesContext = categoryNamesContextFactory.getObject();
		categoryNamesContext.setAll("all");
		Collection<CategoryNamesModel> categoryNamesModels = businessDelegate.getCollection(categoryNamesContext);
		IModelWrapper<Collection<CategoryNamesModel>> models = new CollectionModelWrapper<CategoryNamesModel>(
				CategoryNamesModel.class, categoryNamesModels);
		return new ResponseEntity<IModelWrapper<Collection<CategoryNamesModel>>>(models, HttpStatus.OK);
	}
	@RequestMapping(method = RequestMethod.GET, value = "/categorynameslist", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<CategoryNamesModel>>> getCategoryNamesList() {
		CategoryNamesContext categoryNamesContext = categoryNamesContextFactory.getObject();
		categoryNamesContext.setCategoryNamesList("categoryNamesList");
		Collection<CategoryNamesModel> categoryNamesModels = businessDelegate.getCollection(categoryNamesContext);
		IModelWrapper<Collection<CategoryNamesModel>> models = new CollectionModelWrapper<CategoryNamesModel>(
				CategoryNamesModel.class, categoryNamesModels);
		return new ResponseEntity<IModelWrapper<Collection<CategoryNamesModel>>>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<CategoryNamesModel> getCategoryNames(@PathVariable(value = "id") final String categoryNamesId) {
		CategoryNamesContext categoryNamesContext = categoryNamesContextFactory.getObject();

		CategoryNamesModel model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(categoryNamesId),
				categoryNamesContext);
		return new ResponseEntity<CategoryNamesModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "categoryNamesBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<CategoryNamesModel, CategoryNamesContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setCategoryNamesObjectFactory(final ObjectFactory<CategoryNamesContext> categoryNamesContextFactory) {
		this.categoryNamesContextFactory = categoryNamesContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
