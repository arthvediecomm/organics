package com.arthvedi.organic.productcatalog.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.productcatalog.businessdelegate.context.CategoryContext;
import com.arthvedi.organic.productcatalog.domain.Category;
import com.arthvedi.organic.productcatalog.model.CategoryModel;
import com.arthvedi.organic.productcatalog.service.ICategoryFeatureService;
import com.arthvedi.organic.productcatalog.service.ICategoryImagesService;
import com.arthvedi.organic.productcatalog.service.ICategoryNamesService;
import com.arthvedi.organic.productcatalog.service.ICategoryService;
import com.arthvedi.organic.productcatalog.service.IProductCategoryService;

@Service
public class CategoryBusinessDelegate
		implements
		IBusinessDelegate<CategoryModel, CategoryContext, IKeyBuilder<String>, String> {

	@Autowired
	private ICategoryService categoryService;
	@Autowired
	private ConversionService conversionService;
	@Autowired
	private ICategoryFeatureService categoryFeatureService;
	@Autowired
	private ICategoryNamesService categoryNamesService;
	@Autowired
	private IProductCategoryService productCategoryService;
	@Autowired
	private ICategoryImagesService categoryImagesService;

	@Override
	public CategoryModel create(CategoryModel model) {
		validateModel(model);
		boolean categoryName = checkCategoryNameExist(model.getCategoryName());
		if (categoryName) {
			model.setStatusMessage("Failure::: " + model.getCategoryName()
					+ " Already Exists");
			return model;
		} else {
			Category category = categoryService
					.create((Category) conversionService.convert(model,
							forObject(model), valueOf(Category.class)));
			model = convertToCategoryModel(category);
			model.setStatusMessage("Success::: " + model.getCategoryName()
					+ " Created Successfully");

		}
		return model;
	}

	private boolean checkCategoryNameExist(String categoryName) {
		return categoryService.checkCategoryNameExist(categoryName);
	}

	private void validateModel(CategoryModel model) {
	}

	private CategoryModel convertToCategoryModel(Category category) {
		return (CategoryModel) conversionService.convert(category,
				forObject(category), valueOf(CategoryModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, CategoryContext context) {
	}

	@Override
	public CategoryModel edit(IKeyBuilder<String> keyBuilder,
			CategoryModel model) {
		Category category = categoryService.getCategory(keyBuilder.build()
				.toString());

		category = categoryService.updateCategory((Category) conversionService
				.convert(model, forObject(model), valueOf(Category.class)));

		model = convertToCategoryModel(category);
		return model;
	}

	@Override
	public CategoryModel getByKey(IKeyBuilder<String> keyBuilder,
			CategoryContext context) {
		Category category = categoryService.getCategory(keyBuilder.build()
				.toString());
		CategoryModel model = conversionService.convert(category,
				CategoryModel.class);
		return model;
	}

	@Override
	public Collection<CategoryModel> getCollection(CategoryContext context) {
		List<Category> category = new ArrayList<Category>();
		if (context.getAll() != null) {
			category = categoryService.getAll();
		}
		if (context.getCategoryList() != null) {
			category = categoryService.getCategoryList();
		}

		List<CategoryModel> categoryModels = (List<CategoryModel>) conversionService
				.convert(
						category,
						TypeDescriptor.forObject(category),
						TypeDescriptor.collection(List.class,
								TypeDescriptor.valueOf(CategoryModel.class)));
		return categoryModels;
	}

	@Override
	public CategoryModel edit(IKeyBuilder<String> keyBuilder,
			CategoryModel model, CategoryContext context) {
		return null;
	}

}
