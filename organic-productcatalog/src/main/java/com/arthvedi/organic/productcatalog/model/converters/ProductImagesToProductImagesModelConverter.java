package com.arthvedi.organic.productcatalog.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.productcatalog.domain.ProductImages;
import com.arthvedi.organic.productcatalog.model.ProductImagesModel;

@Component("productImagesToProductImagesModelConverter")
public class ProductImagesToProductImagesModelConverter
        implements Converter<ProductImages, ProductImagesModel> {
    @Autowired
    private ObjectFactory<ProductImagesModel> productImagesModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public ProductImagesModel convert(final ProductImages source) {
        ProductImagesModel productImagesModel = productImagesModelFactory.getObject();
        BeanUtils.copyProperties(source, productImagesModel);

        return productImagesModel;
    }

    @Autowired
    public void setProductImagesModelFactory(
            final ObjectFactory<ProductImagesModel> productImagesModelFactory) {
        this.productImagesModelFactory = productImagesModelFactory;
    }
}
