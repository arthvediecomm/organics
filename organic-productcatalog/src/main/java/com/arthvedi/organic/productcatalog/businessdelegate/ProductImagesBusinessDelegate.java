package com.arthvedi.organic.productcatalog.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.productcatalog.businessdelegate.context.ProductImagesContext;
import com.arthvedi.organic.productcatalog.domain.ProductImages;
import com.arthvedi.organic.productcatalog.model.ProductImagesModel;
import com.arthvedi.organic.productcatalog.service.IProductImagesService;

/*
 *@Author varma
 */

@Service
public class ProductImagesBusinessDelegate
		implements
		IBusinessDelegate<ProductImagesModel, ProductImagesContext, IKeyBuilder<String>, String> {

	@Autowired
	private IProductImagesService productImagesService;
	@Autowired
	private ConversionService conversionService;

	@Override
	@Transactional
	public ProductImagesModel create(ProductImagesModel model) {
		ProductImages productImages = productImagesService
				.create((ProductImages) conversionService.convert(model,
						forObject(model), valueOf(ProductImages.class)));
		model = convertToPRoductImagesModel(productImages);

		return model;
	}

	private ProductImagesModel convertToPRoductImagesModel(
			ProductImages productImages) {
		return (ProductImagesModel) conversionService.convert(productImages,
				forObject(productImages), valueOf(ProductImagesModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			ProductImagesContext context) {

	}

	@Override
	public ProductImagesModel edit(IKeyBuilder<String> keyBuilder,
			ProductImagesModel model) {
		ProductImages productImages = productImagesService
				.getProductImages(keyBuilder.build().toString());

		productImages = productImagesService
				.updateProductImages((ProductImages) conversionService.convert(
						model, forObject(model), valueOf(ProductImages.class)));
		model = convertToPRoductImagesModel(productImages);
		return model;
	}

	@Override
	public ProductImagesModel getByKey(IKeyBuilder<String> keyBuilder,
			ProductImagesContext context) {
		ProductImages productImages = productImagesService
				.getProductImages(keyBuilder.build().toString());
		ProductImagesModel model = conversionService.convert(productImages,
				ProductImagesModel.class);
		return model;
	}

	@Override
	public Collection<ProductImagesModel> getCollection(
			ProductImagesContext context) {
		List<ProductImages> productImages = new ArrayList<ProductImages>();

		List<ProductImagesModel> productImagesModels = (List<ProductImagesModel>) conversionService
				.convert(productImages,
						TypeDescriptor.forObject(productImages), TypeDescriptor
								.collection(List.class, TypeDescriptor
										.valueOf(ProductImagesModel.class)));
		return productImagesModels;
	}

	@Override
	public ProductImagesModel edit(IKeyBuilder<String> keyBuilder,
			ProductImagesModel model, ProductImagesContext context) {
		return null;
	}

}
