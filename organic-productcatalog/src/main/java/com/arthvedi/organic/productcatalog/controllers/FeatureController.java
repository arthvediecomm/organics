package com.arthvedi.organic.productcatalog.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.productcatalog.businessdelegate.context.FeatureContext;
import com.arthvedi.organic.productcatalog.model.FeatureModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/feature", produces = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class FeatureController {

	private IBusinessDelegate<FeatureModel, FeatureContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<FeatureContext> featureContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<FeatureModel> createFeature(
			@RequestBody final FeatureModel featureModel) {
		businessDelegate.create(featureModel);
		return new ResponseEntity<FeatureModel>(featureModel,
				HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<FeatureModel> edit(
			@PathVariable(value = "id") final String featureId,
			@RequestBody final FeatureModel featureModel) {

		businessDelegate.edit(keyBuilderFactory.getObject().withId(featureId),
				featureModel);
		return new ResponseEntity<FeatureModel>(featureModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<FeatureModel>>> getAll() {
		FeatureContext featureContext = featureContextFactory.getObject();
		featureContext.setAll("all");
		Collection<FeatureModel> featureModels = businessDelegate
				.getCollection(featureContext);
		IModelWrapper<Collection<FeatureModel>> models = new CollectionModelWrapper<FeatureModel>(
				FeatureModel.class, featureModels);
		return new ResponseEntity<IModelWrapper<Collection<FeatureModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/featurelist", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<FeatureModel>>> getFeatureList() {
		FeatureContext featureContext = featureContextFactory.getObject();
		featureContext.setFeatureList("featureList");
		Collection<FeatureModel> featureModels = businessDelegate
				.getCollection(featureContext);
		IModelWrapper<Collection<FeatureModel>> models = new CollectionModelWrapper<FeatureModel>(
				FeatureModel.class, featureModels);
		return new ResponseEntity<IModelWrapper<Collection<FeatureModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/featurelistbycategory/{categoryId}")
	public ResponseEntity<IModelWrapper<Collection<FeatureModel>>> getAll(
			@PathVariable(value = "categoryId") final String categoryId) {
		FeatureContext featureContext = new FeatureContext();
		featureContext.setCategoryId(categoryId);
		Collection<FeatureModel> featureModels = businessDelegate
				.getCollection(featureContext);
		IModelWrapper<Collection<FeatureModel>> models = new CollectionModelWrapper<FeatureModel>(
				FeatureModel.class, featureModels);
		return new ResponseEntity<IModelWrapper<Collection<FeatureModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/featurelistbycategorys/{status}")
	public ResponseEntity<IModelWrapper<Collection<FeatureModel>>> getByStatus(
			@PathVariable(value = "status") final String status) {
		FeatureContext featureContext = new FeatureContext();
		featureContext.setStatus(status);
		Collection<FeatureModel> featureModels = businessDelegate
				.getCollection(featureContext);
		IModelWrapper<Collection<FeatureModel>> models = new CollectionModelWrapper<FeatureModel>(
				FeatureModel.class, featureModels);
		return new ResponseEntity<IModelWrapper<Collection<FeatureModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<FeatureModel> getFeature(
			@PathVariable(value = "id") final String featureId) {
		FeatureContext featureContext = featureContextFactory.getObject();

		FeatureModel model = businessDelegate.getByKey(keyBuilderFactory
				.getObject().withId(featureId), featureContext);
		return new ResponseEntity<FeatureModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "featureBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<FeatureModel, FeatureContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setFeatureObjectFactory(
			final ObjectFactory<FeatureContext> featureContextFactory) {
		this.featureContextFactory = featureContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(
			final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
