/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.productcatalog.model.CategoryFeatureModel;
import com.arthvedi.organic.productcatalog.representation.siren.CategoryFeatureRepresentation;

/**
 * @author varma
 *
 */
@Component("categoryFeatureModelToCategoryFeatureRepresentationConverter")
public class CategoryFeatureModelToCategoryFeatureRepresentationConverter extends PropertyCopyingConverter<CategoryFeatureModel, CategoryFeatureRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public CategoryFeatureRepresentation convert(final CategoryFeatureModel source) {

        CategoryFeatureRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<CategoryFeatureRepresentation> factory) {
        super.setFactory(factory);
    }

}
