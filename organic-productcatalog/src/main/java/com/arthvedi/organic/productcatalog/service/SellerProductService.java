package com.arthvedi.organic.productcatalog.service;

import static com.arthvedi.organic.enums.Status.ACTIVE;
import static com.arthvedi.organic.productcatalog.specification.SellerProductSpecification.byProduct;
import static com.arthvedi.organic.productcatalog.specification.SellerProductSpecification.byStatus;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.productcatalog.domain.SellerProduct;
import com.arthvedi.organic.productcatalog.respository.SellerProductRepository;

/*
 *@Author varma
 */
@Component
public class SellerProductService implements ISellerProductService {
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private SellerProductRepository sellerProductRepository;

	@Override
	public SellerProduct create(SellerProduct sellerProduct) {

		sellerProduct.setUserCreated("varma");
		sellerProduct.setCreatedDate(new LocalDateTime());
		sellerProduct.setStatus(ACTIVE.name());
		return sellerProductRepository.save(sellerProduct);
	}

	@Override
	public void deleteSellerProduct(String sellerProductId) {

	}

	@Override
	public SellerProduct getSellerProduct(String sellerProductId) {
		return sellerProductRepository.findOne(sellerProductId);
	}

	@Override
	public List<SellerProduct> getAll() {
		return null;
	}

	@Override
	public SellerProduct updateSellerProduct(SellerProduct sellerProduct) {
		SellerProduct sellerProducts = sellerProductRepository
				.findOne(sellerProduct.getId());
		try {
			nonNullBeanUtils.copyProperties(sellerProducts, sellerProduct);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException r) {
			r.printStackTrace();
		}
		sellerProducts.setUserModified("admin");
		sellerProducts.setModifiedDate(new LocalDateTime());
		sellerProduct = sellerProductRepository.save(sellerProducts);
		return sellerProduct;
	}

	@Override
	public List<SellerProduct> getSellerProductList() {
		List<SellerProduct> sellerProducts = (List<SellerProduct>) sellerProductRepository
				.findAll();
		List<SellerProduct> sellerProductss = new ArrayList<SellerProduct>();
		for (SellerProduct sellerPro : sellerProducts) {
			SellerProduct sellerProduct = new SellerProduct();
			sellerProduct.setDescription(sellerPro.getDescription());
			sellerProduct.setBaseUnit(sellerPro.getBaseUnit());
			sellerProduct.setMaxQuantity(sellerPro.getMeasurementUnit());
			sellerProductss.add(sellerProduct);
		}

		return sellerProductss;

	}

	@Override
	public List<SellerProduct> getSellerProductByProduct(String productId) {
		return sellerProductRepository.findAll(byProduct(productId));
	}

	@Override
	public List<SellerProduct> getByStatus(String status) {
		return sellerProductRepository.findAll(byStatus(status));
	}

}
