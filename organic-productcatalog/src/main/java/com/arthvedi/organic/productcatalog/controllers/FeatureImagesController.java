package com.arthvedi.organic.productcatalog.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.productcatalog.businessdelegate.context.FeatureImagesContext;
import com.arthvedi.organic.productcatalog.model.FeatureImagesModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/featureImages", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class FeatureImagesController {

	private IBusinessDelegate<FeatureImagesModel, FeatureImagesContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<FeatureImagesContext> featureImagesContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<FeatureImagesModel> createFeatureImages(@RequestBody final FeatureImagesModel featureImagesModel) {
		businessDelegate.create(featureImagesModel);
		return new ResponseEntity<FeatureImagesModel>(featureImagesModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	public ResponseEntity<FeatureImagesModel> edit(@PathVariable(value = "id") final String featureImagesId,
			@RequestBody final FeatureImagesModel featureImagesModel) {

		businessDelegate.edit(keyBuilderFactory.getObject().withId(featureImagesId), featureImagesModel);
		return new ResponseEntity<FeatureImagesModel>(featureImagesModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<IModelWrapper<Collection<FeatureImagesModel>>> getAll() {
		FeatureImagesContext featureImagesContext = featureImagesContextFactory.getObject();
		featureImagesContext.setAll("all");
		Collection<FeatureImagesModel> featureImagesModels = businessDelegate.getCollection(featureImagesContext);
		IModelWrapper<Collection<FeatureImagesModel>> models = new CollectionModelWrapper<FeatureImagesModel>(
				FeatureImagesModel.class, featureImagesModels);
		return new ResponseEntity<IModelWrapper<Collection<FeatureImagesModel>>>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<FeatureImagesModel> getFeatureImages(@PathVariable(value = "id") final String featureImagesId) {
		FeatureImagesContext featureImagesContext = featureImagesContextFactory.getObject();

		FeatureImagesModel model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(featureImagesId),
				featureImagesContext);
		return new ResponseEntity<FeatureImagesModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "featureImagesBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<FeatureImagesModel, FeatureImagesContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setFeatureImagesObjectFactory(final ObjectFactory<FeatureImagesContext> featureImagesContextFactory) {
		this.featureImagesContextFactory = featureImagesContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
