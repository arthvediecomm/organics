package com.arthvedi.organic.productcatalog.service;

import java.util.List;

import com.arthvedi.organic.productcatalog.domain.Product;

/*
 *@Author varma
 */
public interface IProductService {

	Product create(Product product);

	void deleteProduct(String productId);

	Product getProduct(String productId);

	List<Product> getAll();

	Product updateProduct(Product product);

	List<Product> getProductList();

	boolean checkProductExist(String productName);

	List<Product> getProductByCategory(String categoryId);

	List<Product> getProductByStatus(String status);

	List<Product> getProductByProductNames(String productNames);


}
