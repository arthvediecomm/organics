package com.arthvedi.organic.productcatalog.service;

import static com.arthvedi.organic.enums.Status.ACTIVE;
import static com.arthvedi.organic.productcatalog.specification.SellerProductFeatureSpecifications.bySellerProduct;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.productcatalog.domain.SellerProductFeature;
import com.arthvedi.organic.productcatalog.respository.SellerProductFeatureRepository;

/*
 *@Author varma
 */
@Component
public class SellerProductFeatureService implements
		ISellerProductFeatureService {
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private SellerProductFeatureRepository sellerProductFeatureRepository;

	@Override
	@Transactional
	public SellerProductFeature create(SellerProductFeature sellerProductFeature) {
		sellerProductFeature.setCreatedDate(new LocalDateTime());
		sellerProductFeature.setUserCreated("VARMA");
		sellerProductFeature.setStatus(ACTIVE.name());
		return sellerProductFeatureRepository.save(sellerProductFeature);
	}

	@Override
	public SellerProductFeature getSellerProductFeature(
			String sellerProductFeatureId) {
		return sellerProductFeatureRepository.findOne(sellerProductFeatureId);
	}

	@Override
	@Transactional
	public SellerProductFeature updateSellerProductFeature(
			SellerProductFeature sellerProductFeature) {
		SellerProductFeature spf = sellerProductFeatureRepository
				.findOne(sellerProductFeature.getId());
		try {
			nonNullBeanUtils.copyProperties(spf, sellerProductFeature);
		} catch (IllegalAccessException e) {
			e.printStackTrace();

		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		spf.setModifiedDate(new LocalDateTime());
		spf.setUserModified("Admin");
		sellerProductFeature = sellerProductFeatureRepository.save(spf);
		return sellerProductFeature;
	}

	@Override
	public List<SellerProductFeature> getSellerProductFeatureBySellerProduct(
			String sellerProductId) {
		return sellerProductFeatureRepository
				.findAll(bySellerProduct(sellerProductId));
	}

}
