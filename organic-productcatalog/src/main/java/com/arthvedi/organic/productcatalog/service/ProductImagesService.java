package com.arthvedi.organic.productcatalog.service;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.productcatalog.domain.ProductImages;
import com.arthvedi.organic.productcatalog.respository.ProductImagesRepository;

/*
 *@Author varma
 */
@Component
public class ProductImagesService implements IProductImagesService {
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private ProductImagesRepository productImagesRepository;

	@Override
	public ProductImages create(ProductImages productImages) {
		productImages.setUserCreated("varma");
		productImages.setCreatedDate(new LocalDateTime());
		return productImagesRepository.save(productImages);
	}

	@Override
	public void deleteProductImages(String productImagesId) {

	}

	@Override
	public ProductImages getProductImages(String productImagesId) {
		return productImagesRepository.findOne(productImagesId);
	}

	@Override
	public List<ProductImages> getAll() {
		return null;
	}

	@Override
	public ProductImages updateProductImages(ProductImages productImages) {
		ProductImages productImage = productImagesRepository
				.findOne(productImages.getId());
		try {
			nonNullBeanUtils.copyProperties(productImage, productImages);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		productImage.setUserModified("admin");
		productImage.setModifiedDate(new LocalDateTime());
		productImages = productImagesRepository.save(productImage);

		return productImages;
	}

}
