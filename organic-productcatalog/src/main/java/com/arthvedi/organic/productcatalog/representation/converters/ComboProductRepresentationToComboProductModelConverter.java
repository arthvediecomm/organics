/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.productcatalog.model.ComboProductModel;
import com.arthvedi.organic.productcatalog.representation.siren.ComboProductRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("comboProductRepresentationToComboProductModelConverter")
public class ComboProductRepresentationToComboProductModelConverter extends PropertyCopyingConverter<ComboProductRepresentation, ComboProductModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public ComboProductModel convert(final ComboProductRepresentation source) {

        ComboProductModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<ComboProductModel> factory) {
        super.setFactory(factory);
    }

}
