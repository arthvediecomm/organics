package com.arthvedi.organic.productcatalog.specification;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.productcatalog.domain.Category;
@Component
public class CategorySpecifications {

	public static Specification<Category> byParentCategory(String categoryId) {

		return new Specification<Category>() {

			@Override
			public Predicate toPredicate(Root<Category> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.get("parentCategory").get("id"), categoryId);
			}

		};
	}

	public static Specification<Category> byCategoryLevel(String level) {

		return new Specification<Category>() {

			@Override
			public Predicate toPredicate(Root<Category> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.get("level"), level);
			}

		};
	}
	public static Specification<Category> distinctCategoryByDiffProducts(List<String> productIds){
		
		return new Specification<Category>(){

			@Override
			public Predicate toPredicate(Root<Category> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				query.distinct(true);
				return cb.in(root.join("productCategories").get("product").get("id")).value(productIds);
			}
			
		};
	}
}
