package com.arthvedi.organic.productcatalog.service;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Set;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.productcatalog.domain.BrandImages;
import com.arthvedi.organic.productcatalog.respository.BrandImagesRepository;

/*
 *@Author varma
 */
@Component
public class BrandImagesService implements IBrandImagesService {
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private BrandImagesRepository brandImagesRepository;

	@Override
	public BrandImages create(BrandImages brandImages) {
		brandImages.setUserCreated("varma");
		brandImages.setCreatedDate(new LocalDateTime());
		return brandImagesRepository.save(brandImages);
	}

	@Override
	public void deleteBrandImages(String brandImagesId) {

	}

	@Override
	public BrandImages getBrandImages(String brandImagesId) {
		return brandImagesRepository.findOne(brandImagesId);
	}

	@Override
	public List<BrandImages> getAll() {
		return null;
	}

	@Override
	public BrandImages updateBrandImages(BrandImages brandImages) {
		BrandImages brandImagess = brandImagesRepository.findOne(brandImages
				.getId());
		try {
			nonNullBeanUtils.copyProperties(brandImagess, brandImages);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		brandImagess.setUserModified("admin");
		brandImagess.setModifiedDate(new LocalDateTime());
		brandImages = brandImagesRepository.save(brandImagess);
		return brandImages;
	}

}
