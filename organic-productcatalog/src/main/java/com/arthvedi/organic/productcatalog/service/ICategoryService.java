package com.arthvedi.organic.productcatalog.service;

import java.util.List;

import com.arthvedi.organic.productcatalog.domain.Category;

/*
 *@Author varma
 */
public interface ICategoryService {

	Category create(Category category);

	void deleteCategory(String categoryId);

	Category getCategory(String categoryId);

	List<Category> getAll();

	Category updateCategory(Category category);

	List<Category> getCategoryList();

	List<Category> getDistinctCategoriesByDiffProducts(
			List<String> childProductIds);

	// boolean getCategoryByCategoryName(String categoryName);

	boolean checkCategoryNameExist(String categoryName);
}
