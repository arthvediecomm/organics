/**
 *
 */
package com.arthvedi.organic.productcatalog.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.productcatalog.domain.Category;
import com.arthvedi.organic.productcatalog.domain.Product;
import com.arthvedi.organic.productcatalog.domain.ProductCategory;
import com.arthvedi.organic.productcatalog.model.ProductCategoryModel;

/**
 * @author Jay
 *
 */
@Component("productCategoryModelToProductCategoryConverter")
public class ProductCategoryModelToProductCategoryConverter implements
		Converter<ProductCategoryModel, ProductCategory> {
	@Autowired
	private ObjectFactory<ProductCategory> productCategoryFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public ProductCategory convert(final ProductCategoryModel source) {
		ProductCategory productCategory = productCategoryFactory.getObject();
		BeanUtils.copyProperties(source, productCategory);
		if (source.getCategoryId() != null) {
			Category category = new Category();
			category.setId(source.getCategoryId());
			productCategory.setCategory(category);
		}

		if (source.getProductId() != null) {
			Product product = new Product();
			product.setId(source.getCategoryId());
			productCategory.setProduct(product);

		}

		return productCategory;
	}

	@Autowired
	public void setProductCategoryFactory(
			final ObjectFactory<ProductCategory> productCategoryFactory) {
		this.productCategoryFactory = productCategoryFactory;
	}

}
