package com.arthvedi.organic.productcatalog.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.productcatalog.businessdelegate.context.CategoryNamesContext;
import com.arthvedi.organic.productcatalog.domain.CategoryNames;
import com.arthvedi.organic.productcatalog.model.CategoryNamesModel;
import com.arthvedi.organic.productcatalog.service.ICategoryNamesService;

/*
*@Author varma
*/

@Service
public class CategoryNamesBusinessDelegate
		implements IBusinessDelegate<CategoryNamesModel, CategoryNamesContext, IKeyBuilder<String>, String> {

	@Autowired
	private ICategoryNamesService categoryNamesService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public CategoryNamesModel create(CategoryNamesModel model) {
CategoryNames categoryNames=categoryNamesService.create((CategoryNames)conversionService.convert(model, forObject(model),valueOf(CategoryNames.class)));
	model=convertToCategoryNamesModel(categoryNames);	
		return model;
	}

	private CategoryNamesModel convertToCategoryNamesModel(CategoryNames categoryNames) {
		return (CategoryNamesModel)conversionService .convert(categoryNames, forObject(categoryNames),valueOf(CategoryNamesModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, CategoryNamesContext context) {

	}

	@Override
	public CategoryNamesModel edit(IKeyBuilder<String> keyBuilder, CategoryNamesModel model) {
		CategoryNames categoryNames = categoryNamesService.getCategoryNames(keyBuilder.build().toString());
		
		categoryNames = categoryNamesService.updateCategoryNames((CategoryNames)conversionService.convert(model, forObject(model),valueOf(CategoryNames.class)));
		model=convertToCategoryNamesModel(categoryNames);
		return model;
	}

	@Override
	public CategoryNamesModel getByKey(IKeyBuilder<String> keyBuilder, CategoryNamesContext context) {
		CategoryNames categoryNames = categoryNamesService.getCategoryNames(keyBuilder.build().toString());
		CategoryNamesModel model = conversionService.convert(categoryNames, CategoryNamesModel.class);
		return model;
	}

	@Override
	public Collection<CategoryNamesModel> getCollection(CategoryNamesContext context) {
		List<CategoryNames> categoryNames = new ArrayList<CategoryNames>();
		if(context.getAll()!=null){
			categoryNames=categoryNamesService.getAll();
		}
		if(context.getCategoryNamesList()!=null){
			categoryNames=categoryNamesService.getCategoryNamesList();
		}
		List<CategoryNamesModel> categoryNamesModels = (List<CategoryNamesModel>) conversionService.convert(
				categoryNames, TypeDescriptor.forObject(categoryNames),
				TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(CategoryNamesModel.class)));
		return categoryNamesModels;
	}

	@Override
	public CategoryNamesModel edit(IKeyBuilder<String> keyBuilder,
			CategoryNamesModel model, CategoryNamesContext context) {
		return null;
	}

}
