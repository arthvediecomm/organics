package com.arthvedi.organic.productcatalog.html.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;

@Component("productPageContext")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ProductPageContext implements IBusinessDelegateContext {

	private String zoneId;
	private String languageId;

	public String getZoneId() {
		return zoneId;
	}

	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}

	public String getLanguageId() {
		return languageId;
	}

	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}

}
