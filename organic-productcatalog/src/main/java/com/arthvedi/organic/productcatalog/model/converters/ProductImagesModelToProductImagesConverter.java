/**
 *
 */
package com.arthvedi.organic.productcatalog.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.productcatalog.domain.Product;
import com.arthvedi.organic.productcatalog.domain.ProductImages;
import com.arthvedi.organic.productcatalog.model.ProductImagesModel;

/**
 * @author Jay
 *
 */
@Component("productImagesModelToProductImagesConverter")
public class ProductImagesModelToProductImagesConverter implements
		Converter<ProductImagesModel, ProductImages> {
	@Autowired
	private ObjectFactory<ProductImages> productImagesFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public ProductImages convert(final ProductImagesModel source) {
		ProductImages productImages = new ProductImages();
		BeanUtils.copyProperties(source, productImages);
		if (source.getProductId() != null) {
			Product product = new Product();
			product.setId(source.getProductId());
			productImages.setProduct(product);
		}

		return productImages;
	}

	@Autowired
	public void setProductImagesFactory(
			final ObjectFactory<ProductImages> productImagesFactory) {
		this.productImagesFactory = productImagesFactory;
	}

}
