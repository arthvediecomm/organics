package com.arthvedi.organic.productcatalog.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.productcatalog.domain.CategoryFeature;
import com.arthvedi.organic.productcatalog.model.CategoryFeatureModel;

@Component("categoryFeatureToCategoryFeatureModelConverter")
public class CategoryFeatureToCategoryFeatureModelConverter
        implements Converter<CategoryFeature, CategoryFeatureModel> {
    @Autowired
    private ObjectFactory<CategoryFeatureModel> categoryFeatureModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public CategoryFeatureModel convert(final CategoryFeature source) {
        CategoryFeatureModel categoryFeatureModel = categoryFeatureModelFactory.getObject();
        BeanUtils.copyProperties(source, categoryFeatureModel);

        return categoryFeatureModel;
    }

    @Autowired
    public void setCategoryFeatureModelFactory(
            final ObjectFactory<CategoryFeatureModel> categoryFeatureModelFactory) {
        this.categoryFeatureModelFactory = categoryFeatureModelFactory;
    }
}
