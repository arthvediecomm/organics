package com.arthvedi.organic.productcatalog.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.productcatalog.domain.CategoryImages;
import com.arthvedi.organic.productcatalog.model.CategoryImagesModel;

@Component("categoryImagesToCategoryImagesModelConverter")
public class CategoryImagesToCategoryImagesModelConverter
        implements Converter<CategoryImages, CategoryImagesModel> {
    @Autowired
    private ObjectFactory<CategoryImagesModel> categoryImagesModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public CategoryImagesModel convert(final CategoryImages source) {
        CategoryImagesModel categoryImagesModel = categoryImagesModelFactory.getObject();
        BeanUtils.copyProperties(source, categoryImagesModel);

        return categoryImagesModel;
    }

    @Autowired
    public void setCategoryImagesModelFactory(
            final ObjectFactory<CategoryImagesModel> categoryImagesModelFactory) {
        this.categoryImagesModelFactory = categoryImagesModelFactory;
    }
}
