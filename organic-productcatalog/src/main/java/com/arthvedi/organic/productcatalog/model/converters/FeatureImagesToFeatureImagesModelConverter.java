package com.arthvedi.organic.productcatalog.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.productcatalog.domain.FeatureImages;
import com.arthvedi.organic.productcatalog.model.FeatureImagesModel;

@Component("featureImagesToFeatureImagesModelConverter")
public class FeatureImagesToFeatureImagesModelConverter
        implements Converter<FeatureImages, FeatureImagesModel> {
    @Autowired
    private ObjectFactory<FeatureImagesModel> featureImagesModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public FeatureImagesModel convert(final FeatureImages source) {
        FeatureImagesModel featureImagesModel = featureImagesModelFactory.getObject();
        BeanUtils.copyProperties(source, featureImagesModel);

        return featureImagesModel;
    }

    @Autowired
    public void setFeatureImagesModelFactory(
            final ObjectFactory<FeatureImagesModel> featureImagesModelFactory) {
        this.featureImagesModelFactory = featureImagesModelFactory;
    }
}
