package com.arthvedi.organic.productcatalog.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;

/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SellerProductFeatureContext implements IBusinessDelegateContext {

	private String all;
	private String sellerProductFeatureId;
	private String sellerProductId;

	public String getSellerProductFeatureId() {
		return sellerProductFeatureId;
	}

	public void setSellerProductFeatureId(String sellerProductFeatureId) {
		this.sellerProductFeatureId = sellerProductFeatureId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

	public String getSellerProductId() {
		return sellerProductId;
	}

	public void setSellerProductId(String sellerProductId) {
		this.sellerProductId = sellerProductId;
	}

}
