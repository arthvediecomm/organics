/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.productcatalog.model.SellerProductModel;
import com.arthvedi.organic.productcatalog.representation.siren.SellerProductRepresentation;

/**
 * @author varma
 *
 */
@Component("sellerProductModelToSellerProductRepresentationConverter")
public class SellerProductModelToSellerProductRepresentationConverter extends PropertyCopyingConverter<SellerProductModel, SellerProductRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public SellerProductRepresentation convert(final SellerProductModel source) {

        SellerProductRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<SellerProductRepresentation> factory) {
        super.setFactory(factory);
    }

}
