package com.arthvedi.organic.productcatalog.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.productcatalog.businessdelegate.context.BrandContext;
import com.arthvedi.organic.productcatalog.model.BrandModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/brand", produces = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class BrandController {

	private IBusinessDelegate<BrandModel, BrandContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<BrandContext> brandContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<BrandModel> createBrand(
			@RequestBody final BrandModel brandModel) {
		businessDelegate.create(brandModel);
		return new ResponseEntity<BrandModel>(brandModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<BrandModel> edit(
			@PathVariable(value = "id") final String brandId,
			@RequestBody final BrandModel brandModel) {

		businessDelegate.edit(keyBuilderFactory.getObject().withId(brandId),
				brandModel);
		return new ResponseEntity<BrandModel>(brandModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<BrandModel>>> getAll() {
		BrandContext brandContext = brandContextFactory.getObject();
		brandContext.setAll("all");
		Collection<BrandModel> brandModels = businessDelegate
				.getCollection(brandContext);
		IModelWrapper<Collection<BrandModel>> models = new CollectionModelWrapper<BrandModel>(
				BrandModel.class, brandModels);
		return new ResponseEntity<IModelWrapper<Collection<BrandModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/brandlist", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<IModelWrapper<Collection<BrandModel>>> getBrandList() {
		BrandContext brandContext = brandContextFactory.getObject();
		brandContext.setBrandList("brandList");
		Collection<BrandModel> brandModels = businessDelegate
				.getCollection(brandContext);
		IModelWrapper<Collection<BrandModel>> models = new CollectionModelWrapper<BrandModel>(
				BrandModel.class, brandModels);
		return new ResponseEntity<IModelWrapper<Collection<BrandModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<BrandModel> getBrand(
			@PathVariable(value = "id") final String brandId) {
		BrandContext brandContext = brandContextFactory.getObject();

		BrandModel model = businessDelegate.getByKey(keyBuilderFactory
				.getObject().withId(brandId), brandContext);
		return new ResponseEntity<BrandModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "brandBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<BrandModel, BrandContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setBrandObjectFactory(
			final ObjectFactory<BrandContext> brandContextFactory) {
		this.brandContextFactory = brandContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(
			final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
