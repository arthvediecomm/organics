package com.arthvedi.organic.productcatalog.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.arthvedi.organic.productcatalog.domain.SellerProduct;

public class SellerProductSpecification {
	public static Specification<SellerProduct> byProduct(final String productId) {
		return new Specification<SellerProduct>() {

			@Override
			public Predicate toPredicate(Root<SellerProduct> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.get("product").get("id"),
						productId);
			}
		};

	}

	public static Specification<SellerProduct> byStatus(String status) {
		return new Specification<SellerProduct>() {

			@Override
			public Predicate toPredicate(Root<SellerProduct> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.get("status"), status);
			}
		};

	}
}