/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.productcatalog.model.SeoModel;
import com.arthvedi.organic.productcatalog.representation.siren.SeoRepresentation;

/**
 * @author varma
 *
 */
@Component("seoModelToSeoRepresentationConverter")
public class SeoModelToSeoRepresentationConverter extends PropertyCopyingConverter<SeoModel, SeoRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public SeoRepresentation convert(final SeoModel source) {

        SeoRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<SeoRepresentation> factory) {
        super.setFactory(factory);
    }

}
