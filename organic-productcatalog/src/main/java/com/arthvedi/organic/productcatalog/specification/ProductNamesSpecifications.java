package com.arthvedi.organic.productcatalog.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.arthvedi.organic.productcatalog.domain.ProductNames;

public class ProductNamesSpecifications {

	public static Specification<ProductNames> bylanguageAndProduct(
			String languageId, String productId) {

		return new Specification<ProductNames>() {

			@Override
			public Predicate toPredicate(Root<ProductNames> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.and(
						cb.equal(root.get("language").get("id"), languageId),
						cb.equal(root.get("product").get("id"), productId));
			}

		};
	}

}
