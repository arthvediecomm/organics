package com.arthvedi.organic.productcatalog.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;
/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CategoryNamesContext implements IBusinessDelegateContext {

	private String categoryNamesId;
	private String all;
private String categoryNamesList;
	
	
	public String getCategoryNamesList() {
	return categoryNamesList;
}

public void setCategoryNamesList(String categoryNamesList) {
	this.categoryNamesList = categoryNamesList;
}

	public String getCategoryNamesId() {
		return categoryNamesId;
	}

	public void setCategoryNamesId(String categoryNamesId) {
		this.categoryNamesId = categoryNamesId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

}
