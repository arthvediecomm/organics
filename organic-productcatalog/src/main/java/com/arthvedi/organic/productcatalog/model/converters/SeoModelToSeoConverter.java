/**
 *
 */
package com.arthvedi.organic.productcatalog.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.productcatalog.domain.Seo;
import com.arthvedi.organic.productcatalog.model.SeoModel;

/**
 * @author Jay
 *
 */
@Component("seoModelToSeoConverter")
public class SeoModelToSeoConverter implements Converter<SeoModel, Seo> {
    @Autowired
    private ObjectFactory<Seo> seoFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public Seo convert(final SeoModel source) {
        Seo seo = seoFactory.getObject();
        BeanUtils.copyProperties(source, seo);

        return seo;
    }

    @Autowired
    public void setSeoFactory(final ObjectFactory<Seo> seoFactory) {
        this.seoFactory = seoFactory;
    }

}
