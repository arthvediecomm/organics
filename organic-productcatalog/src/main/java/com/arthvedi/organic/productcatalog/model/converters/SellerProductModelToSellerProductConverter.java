/**
 *
 */
package com.arthvedi.organic.productcatalog.model.converters;

import java.math.BigDecimal;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.commons.domain.ReturnPolicy;
import com.arthvedi.organic.productcatalog.domain.Product;
import com.arthvedi.organic.productcatalog.domain.SellerProduct;
import com.arthvedi.organic.productcatalog.model.SellerProductModel;
import com.arthvedi.organic.seller.domain.SellerBranch;

/**
 * @author Jay
 *
 */
@Component("sellerProductModelToSellerProductConverter")
public class SellerProductModelToSellerProductConverter implements
		Converter<SellerProductModel, SellerProduct> {
	@Autowired
	private ObjectFactory<SellerProduct> sellerProductFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerProduct convert(final SellerProductModel source) {
		SellerProduct sellerProduct = sellerProductFactory.getObject();
		BeanUtils.copyProperties(source, sellerProduct);
		if (source.getProductId() != null) {
			Product product = new Product();
			product.setId(source.getProductId());
			sellerProduct.setProduct(product);
		}

		if (source.getReturnPolicyId() != null) {
			ReturnPolicy returnPolicy = new ReturnPolicy();
			returnPolicy.setId(source.getReturnPolicyId());
			sellerProduct.setReturnPolicy(returnPolicy);
		}

		if (source.getSellerBranchId() != null) {
			SellerBranch sellerBranch = new SellerBranch();
			sellerBranch.setId(source.getSellerBranchId());

			sellerProduct.setSellerBranch(sellerBranch);
		}
		if(source.getProductMrp()!=null){
			
			sellerProduct.setProductMrp(new BigDecimal(source.getProductMrp()));
		
		}
		if(source.getSellerPrice()!=null){
			sellerProduct.setSellerPrice(new BigDecimal(source.getSellerPrice()));
		}

		return sellerProduct;
	}

	@Autowired
	public void setSellerProductFactory(
			final ObjectFactory<SellerProduct> sellerProductFactory) {
		this.sellerProductFactory = sellerProductFactory;
	}

}
