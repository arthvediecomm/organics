package com.arthvedi.organic.productcatalog.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.productcatalog.businessdelegate.context.CategoryContext;
import com.arthvedi.organic.productcatalog.model.CategoryModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/category", produces = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class CategoryController {

	private IBusinessDelegate<CategoryModel, CategoryContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<CategoryContext> categoryContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<CategoryModel> createCategory(
			@RequestBody CategoryModel categoryModel) {
		categoryModel = businessDelegate.create(categoryModel);
		return new ResponseEntity<CategoryModel>(categoryModel,
				HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<CategoryModel> edit(
			@PathVariable(value = "id") final String categoryId,
			@RequestBody final CategoryModel categoryModel) {

		businessDelegate.edit(keyBuilderFactory.getObject().withId(categoryId),
				categoryModel);
		return new ResponseEntity<CategoryModel>(categoryModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<CategoryModel>>> getAll() {
		CategoryContext categoryContext = categoryContextFactory.getObject();
		categoryContext.setAll("all");
		Collection<CategoryModel> categoryModels = businessDelegate
				.getCollection(categoryContext);
		IModelWrapper<Collection<CategoryModel>> models = new CollectionModelWrapper<CategoryModel>(
				CategoryModel.class, categoryModels);
		return new ResponseEntity<IModelWrapper<Collection<CategoryModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/categorylist", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<CategoryModel>>> getCategoryList() {
		CategoryContext categoryContext = categoryContextFactory.getObject();
		categoryContext.setCategoryList("categoryList");
		Collection<CategoryModel> categoryModels = businessDelegate
				.getCollection(categoryContext);
		IModelWrapper<Collection<CategoryModel>> models = new CollectionModelWrapper<CategoryModel>(
				CategoryModel.class, categoryModels);
		return new ResponseEntity<IModelWrapper<Collection<CategoryModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<CategoryModel> getCategory(
			@PathVariable(value = "id") final String categoryId) {
		CategoryContext categoryContext = categoryContextFactory.getObject();

		CategoryModel model = businessDelegate.getByKey(keyBuilderFactory
				.getObject().withId(categoryId), categoryContext);
		return new ResponseEntity<CategoryModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "categoryBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<CategoryModel, CategoryContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setCategoryObjectFactory(
			final ObjectFactory<CategoryContext> categoryContextFactory) {
		this.categoryContextFactory = categoryContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(
			final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
