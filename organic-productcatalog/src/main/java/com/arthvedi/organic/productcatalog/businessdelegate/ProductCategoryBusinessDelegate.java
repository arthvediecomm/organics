package com.arthvedi.organic.productcatalog.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.productcatalog.businessdelegate.context.ProductCategoryContext;
import com.arthvedi.organic.productcatalog.domain.ProductCategory;
import com.arthvedi.organic.productcatalog.model.ProductCategoryModel;
import com.arthvedi.organic.productcatalog.service.IProductCategoryService;

/*
*@Author varma
*/

@Service
public class ProductCategoryBusinessDelegate
		implements IBusinessDelegate<ProductCategoryModel, ProductCategoryContext, IKeyBuilder<String>, String> {

	@Autowired
	private IProductCategoryService productCategoryService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public ProductCategoryModel create(ProductCategoryModel model) {
		ProductCategory productCategory = productCategoryService.create(
				(ProductCategory) conversionService.convert(model, forObject(model), valueOf(ProductCategory.class)));
model=convertToProductCategoryModel(productCategory);
		return model;
	}

	private ProductCategoryModel convertToProductCategoryModel(ProductCategory productCategory) {
		return (ProductCategoryModel)conversionService.convert(productCategory, forObject(productCategory), valueOf(ProductCategoryModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, ProductCategoryContext context) {

	}

	@Override
	public ProductCategoryModel edit(IKeyBuilder<String> keyBuilder, ProductCategoryModel model) {
		ProductCategory productCategory = productCategoryService.getProductCategory(keyBuilder.build().toString());

		productCategory = productCategoryService.updateProductCategory(productCategory);
model=convertToProductCategoryModel(productCategory);
		return model;
	}

	@Override
	public ProductCategoryModel getByKey(IKeyBuilder<String> keyBuilder, ProductCategoryContext context) {
		ProductCategory productCategory = productCategoryService.getProductCategory(keyBuilder.build().toString());
		ProductCategoryModel model = conversionService.convert(productCategory, ProductCategoryModel.class);
		return model;
	}

	@Override
	public Collection<ProductCategoryModel> getCollection(ProductCategoryContext context) {
		List<ProductCategory> productCategory = new ArrayList<ProductCategory>();
if(context.getAll()!=null){
	productCategory=productCategoryService.getAll();
}
if(context.getProductCategoryList()!=null){
	productCategory=productCategoryService.getProductCategoryList();
}
		List<ProductCategoryModel> productCategoryModels = (List<ProductCategoryModel>) conversionService.convert(
				productCategory, TypeDescriptor.forObject(productCategory),
				TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(ProductCategoryModel.class)));
		return productCategoryModels;
	}

	@Override
	public ProductCategoryModel edit(IKeyBuilder<String> keyBuilder,
			ProductCategoryModel model, ProductCategoryContext context) {
		return null;
	}

}
