package com.arthvedi.organic.productcatalog.service;

import java.util.List;

import com.arthvedi.organic.productcatalog.domain.ProductNames;

/*
 *@Author varma
 */
public interface IProductNamesService {

	ProductNames create(ProductNames productNames);

	void deleteProductNames(String productNamesId);

	ProductNames getProductNames(String productNamesId);

	List<ProductNames> getAll();

	ProductNames updateProductNames(ProductNames productNames);

	List<ProductNames> getProductNamesList();

}
