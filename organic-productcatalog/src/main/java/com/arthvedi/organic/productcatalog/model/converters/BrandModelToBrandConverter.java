/**
 *
 */
package com.arthvedi.organic.productcatalog.model.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.organic.productcatalog.domain.Brand;
import com.arthvedi.organic.productcatalog.domain.BrandImages;
import com.arthvedi.organic.productcatalog.model.BrandModel;

/**
 * @author Jay
 *
 */
@Component("brandModelToBrandConverter")
public class BrandModelToBrandConverter implements Converter<BrandModel, Brand> {
	@Autowired
	private ObjectFactory<Brand> brandFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public Brand convert(final BrandModel source) {
		Brand brand = new Brand();
		BeanUtils.copyProperties(source, brand);
		if (CollectionUtils.isNotEmpty(source.getBrandImagesModels())) {
			List<BrandImages> converted = (List<BrandImages>) conversionService
					.convert(source.getBrandImagesModels(), TypeDescriptor
							.forObject(source.getBrandImagesModels()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									BrandImages.class));
			brand.getBrandImageses().addAll(converted);

		}
		return brand;
	}

	@Autowired
	public void setBrandFactory(final ObjectFactory<Brand> brandFactory) {
		this.brandFactory = brandFactory;
	}

}
