package com.arthvedi.organic.productcatalog.service;

import static com.arthvedi.organic.enums.Status.ACTIVE;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.productcatalog.domain.ProductFeature;
import com.arthvedi.organic.productcatalog.respository.ProductFeatureRepository;

/*
 *@Author varma
 */
@Component
public class ProductFeatureService implements IProductFeatureService {
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private ProductFeatureRepository productFeatureRepository;

	@Override
	public ProductFeature create(ProductFeature productFeature) {
		productFeature.setUserCreated("varma");
		productFeature.setCreatedDate(new LocalDateTime());
		productFeature.setStatus(ACTIVE.name());
		return productFeatureRepository.save(productFeature);
	}

	@Override
	public void deleteProductFeature(String productFeatureId) {

	}

	@Override
	public ProductFeature getProductFeature(String productFeatureId) {
		return productFeatureRepository.findOne(productFeatureId);
	}

	@Override
	public List<ProductFeature> getAll() {
		return null;
	}

	@Override
	public ProductFeature updateProductFeature(ProductFeature productFeature) {
		ProductFeature productFeatures = productFeatureRepository
				.findOne(productFeature.getId());
		try {
			nonNullBeanUtils.copyProperties(productFeatures, productFeature);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		productFeatures.setUserModified("admin");
		productFeatures.setModifiedDate(new LocalDateTime());
		productFeature = productFeatureRepository.save(productFeatures);
		return productFeature;
	}

	@Override
	public List<ProductFeature> getProductFeatureList() {
		List<ProductFeature> productFeatures = (List<ProductFeature>) productFeatureRepository
				.findAll();
		List<ProductFeature> productFeaturess = new ArrayList<ProductFeature>();
		for (ProductFeature productFe : productFeatures) {
			ProductFeature proFea = new ProductFeature();
			proFea.setFeature(productFe.getFeature());
			proFea.setProduct(productFe.getProduct());

			productFeaturess.add(proFea);
		}
		return productFeaturess;
	}

}
