package com.arthvedi.organic.productcatalog.service;

import java.util.List;

import com.arthvedi.organic.productcatalog.domain.FeatureImages;
/*
*@Author varma
*/
public interface IFeatureImagesService {
	
	FeatureImages create(FeatureImages featureImages);

	void deleteFeatureImages(String featureImagesId);

	FeatureImages getFeatureImages(String featureImagesId);

	List<FeatureImages> getAll();

	FeatureImages updateFeatureImages(FeatureImages featureImages);
}
