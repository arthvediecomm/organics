package com.arthvedi.organic.productcatalog.service;

import static com.arthvedi.organic.enums.Status.ACTIVE;
import static com.arthvedi.organic.productcatalog.specification.ProductSpecification.byCategory;
import static com.arthvedi.organic.productcatalog.specification.ProductSpecification.byStatus;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.productcatalog.domain.Product;
import com.arthvedi.organic.productcatalog.domain.ProductCategory;
import com.arthvedi.organic.productcatalog.domain.ProductFeature;
import com.arthvedi.organic.productcatalog.domain.ProductImages;
import com.arthvedi.organic.productcatalog.domain.ProductNames;
import com.arthvedi.organic.productcatalog.respository.ProductRepository;

@Component
public class ProductService implements IProductService {
	@Autowired
	NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private ProductRepository productRepository;
	@Autowired
	private IProductImagesService productImagesService;
	@Autowired
	private IProductFeatureService productFeatureService;
	@Autowired
	private IProductNamesService productNamesService;
	@Autowired
	private IProductCategoryService productCategoryService;

	@Override
	public Product create(Product product) {
		product.setUserCreated("varma");
		product.setCreatedDate(new LocalDateTime());
		product.setStatus(ACTIVE.name());
		product = productRepository.save(product);
		if (product.getId() != null
				&& CollectionUtils.isNotEmpty(product.getProductImageses())) {
			product.setProductImageses(addingProductImages(product,
					product.getProductImageses()));

		}
		if (product.getId() != null
				&& CollectionUtils.isNotEmpty(product.getProductFeatures())) {
			product.setProductFeatures(addProductFeature(product,
					product.getProductFeatures()));
		}
		if (product.getId() != null
				&& CollectionUtils.isNotEmpty(product.getProductNameses())) {
			product.setProductNameses(addProductNames(product,
					product.getProductNameses()));
		}
		if (product.getId() != null
				&& CollectionUtils.isNotEmpty(product.getProductCategories())) {
			product.setProductCategories(addProductCategory(product,
					product.getProductCategories()));

		}
		return product;
	}

	@Transactional
	private Set<ProductCategory> addProductCategory(Product product,
			Set<ProductCategory> productCategories) {
		Set<ProductCategory> productCategory = new HashSet<ProductCategory>();
		for (ProductCategory productCat : productCategories) {
			ProductCategory productCate = productCat;
			productCate.setProduct(product);
			productCate = productCategoryService.create(productCate);
			productCategory.add(productCate);
		}
		return productCategory;
	}

	@Transactional
	private Set<ProductNames> addProductNames(Product product,
			Set<ProductNames> productNameses) {
		Set<ProductNames> productNames = new HashSet<ProductNames>();
		for (ProductNames productName : productNameses) {
			ProductNames proNames = productName;
			proNames.setProduct(product);
			proNames = productNamesService.create(proNames);
			productNames.add(proNames);
		}
		return productNames;
	}

	@Transactional
	private Set<ProductFeature> addProductFeature(Product product,
			Set<ProductFeature> productFeatures) {
		Set<ProductFeature> productFeature = new HashSet<ProductFeature>();
		for (ProductFeature productFeat : productFeatures) {
			ProductFeature proFeaturete = productFeat;
			proFeaturete.setProduct(product);
			proFeaturete = productFeatureService.create(proFeaturete);
			productFeature.add(proFeaturete);
		}
		return productFeature;
	}

	@Transactional
	private Set<ProductImages> addingProductImages(Product product,
			Set<ProductImages> productImageses) {
		Set<ProductImages> productImages = new HashSet<ProductImages>();
		for (ProductImages pI : productImageses) {
			ProductImages productImage = pI;
			productImage.setProduct(product);
			productImage = productImagesService.create(productImage);
			productImages.add(productImage);
		}
		return productImages;
	}

	@Override
	public void deleteProduct(String productId) {

	}

	@Override
	public Product getProduct(String productId) {
		return productRepository.findOne(productId);
	}

	@Override
	public List<Product> getAll() {
		return null;
	}

	@Override
	public Product updateProduct(Product product) {
		Product products = productRepository.findOne(product.getId());
		try {
			nonNullBeanUtils.copyProperties(products, product);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		products.setUserModified("admin");
		products.setModifiedDate(new LocalDateTime());
		if (product.getId() != null
				&& CollectionUtils.isNotEmpty(product.getProductImageses())) {
			product.setProductImageses(addingProductImages(products,
					products.getProductImageses()));

		}
		if (product.getId() != null
				&& CollectionUtils.isNotEmpty(product.getProductNameses())) {
			product.setProductNameses(addProductNames(product,
					product.getProductNameses()));
		}
		if (product.getId() != null
				&& CollectionUtils.isNotEmpty(product.getProductFeatures())) {
			product.setProductFeatures(addProductFeature(product,
					product.getProductFeatures()));
		}
		product = productRepository.save(products);
		return product;
	}

	@Override
	public List<Product> getProductList() {
		List<Product> products = (List<Product>) productRepository.findAll();
		List<Product> productss = new ArrayList<Product>();
		for (Product product : products) {
			Product pro = new Product();
			pro.setProductName(product.getProductName());
			pro.setDescription(product.getDescription());
			pro.setMrp(product.getMrp());
			pro.setMeasurementUnit(product.getMeasurementUnit());
			pro.setQuantity(product.getQuantity());
			pro.setBaseUnit(product.getBaseUnit());
			pro.setStatus(product.getStatus());
			productss.add(pro);
		}

		return productss;
	}

	@Override
	public boolean checkProductExist(String productName) {
		return productRepository.findProductNameExist(productName) != null;
	}

	@Override
	public List<Product> getProductByCategory(String categoryId) {
		return productRepository.findAll(byCategory(categoryId));
	}

	@Override
	public List<Product> getProductByStatus(String status) {
		return productRepository.findAll(byStatus(status));
	}

	@Override
	public List<Product> getProductByProductNames(String productNames) {
		List<Product> products = (List<Product>) productRepository.findAll();
		List<Product> pro = new ArrayList<Product>();
		for (Product product : products) {
			Product Products = new Product();
			Products.setProductName(product.getProductName());
			pro.add(Products);
		}

		return pro; /* productRepository.findAll(byProductName(productNames)) */
	}

}
