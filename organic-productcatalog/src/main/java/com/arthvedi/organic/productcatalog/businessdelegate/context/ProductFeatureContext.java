package com.arthvedi.organic.productcatalog.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;

/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ProductFeatureContext implements IBusinessDelegateContext {

	private String productFeatureId;
	private String all;
	private String productFeatureList;

	public String getProductFeatureList() {
		return productFeatureList;
	}

	public void setProductFeatureList(String productFeatureList) {
		this.productFeatureList = productFeatureList;
	}

	public String getProductFeatureId() {
		return productFeatureId;
	}

	public void setProductFeatureId(String productFeatureId) {
		this.productFeatureId = productFeatureId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

}
