/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.productcatalog.model.ComboProductModel;
import com.arthvedi.organic.productcatalog.representation.siren.ComboProductRepresentation;

/**
 * @author varma
 *
 */
@Component("comboProductModelToComboProductRepresentationConverter")
public class ComboProductModelToComboProductRepresentationConverter extends PropertyCopyingConverter<ComboProductModel, ComboProductRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public ComboProductRepresentation convert(final ComboProductModel source) {

        ComboProductRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<ComboProductRepresentation> factory) {
        super.setFactory(factory);
    }

}
