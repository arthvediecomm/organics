package com.arthvedi.organic.productcatalog.service;

import java.util.List;

import com.arthvedi.organic.productcatalog.domain.ProductFeature;
/*
*@Author varma
*/
public interface IProductFeatureService {
	
	ProductFeature create(ProductFeature productFeature);

	void deleteProductFeature(String productFeatureId);

	ProductFeature getProductFeature(String productFeatureId);

	List<ProductFeature> getAll();

	ProductFeature updateProductFeature(ProductFeature productFeature);

	List<ProductFeature> getProductFeatureList();
}
