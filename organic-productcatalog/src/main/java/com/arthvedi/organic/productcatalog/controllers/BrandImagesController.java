package com.arthvedi.organic.productcatalog.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.productcatalog.businessdelegate.context.BrandImagesContext;
import com.arthvedi.organic.productcatalog.model.BrandImagesModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/brandImages", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class BrandImagesController {

	private IBusinessDelegate<BrandImagesModel, BrandImagesContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<BrandImagesContext> brandImagesContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<BrandImagesModel> createBrandImages(@RequestBody final BrandImagesModel brandImagesModel) {
		businessDelegate.create(brandImagesModel);
		return new ResponseEntity<BrandImagesModel>(brandImagesModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	public ResponseEntity<BrandImagesModel> edit(@PathVariable(value = "id") final String brandImagesId,
			@RequestBody BrandImagesModel brandImagesModel) {

		brandImagesModel=	businessDelegate.edit(keyBuilderFactory.getObject().withId(brandImagesId), brandImagesModel);
		return new ResponseEntity<BrandImagesModel>(brandImagesModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<IModelWrapper<Collection<BrandImagesModel>>> getAll() {
		BrandImagesContext brandImagesContext = brandImagesContextFactory.getObject();
		brandImagesContext.setAll("all");
		Collection<BrandImagesModel> brandImagesModels = businessDelegate.getCollection(brandImagesContext);
		IModelWrapper<Collection<BrandImagesModel>> models = new CollectionModelWrapper<BrandImagesModel>(
				BrandImagesModel.class, brandImagesModels);
		return new ResponseEntity<IModelWrapper<Collection<BrandImagesModel>>>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<BrandImagesModel> getBrandImages(@PathVariable(value = "id") final String brandImagesId) {
		BrandImagesContext brandImagesContext = brandImagesContextFactory.getObject();

		BrandImagesModel model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(brandImagesId),
				brandImagesContext);
		return new ResponseEntity<BrandImagesModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "brandImagesBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<BrandImagesModel, BrandImagesContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setBrandImagesObjectFactory(final ObjectFactory<BrandImagesContext> brandImagesContextFactory) {
		this.brandImagesContextFactory = brandImagesContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
