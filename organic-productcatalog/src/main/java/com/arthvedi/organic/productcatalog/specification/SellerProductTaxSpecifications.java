/*
 * package com.arthvedi.organic.productcatalog.specification;
 * 
 * import javax.persistence.criteria.CriteriaBuilder; import
 * javax.persistence.criteria.CriteriaQuery; import
 * javax.persistence.criteria.Predicate; import javax.persistence.criteria.Root;
 * 
 * import org.springframework.data.jpa.domain.Specification;
 * 
 * public class SellerProductTaxSpecifications { public static
 * Specification<SellerProductTax>
 * checkSellerProductExistsBySellerProduct(String sellerProductId) { return new
 * Specification<SellerProductTax>() {
 * 
 * @Override public Predicate toPredicate(Root<SellerProductTax> root,
 * CriteriaQuery<?> query, CriteriaBuilder cb) { return
 * cb.equal(root.get("sellerProduct").get("id"), sellerProductId); } }; }
 * 
 * public static Specification<SellerProductTax>
 * checkSellerProductExistsByTax(String taxId) { return new
 * Specification<SellerProductTax>() {
 * 
 * @Override public Predicate toPredicate(Root<SellerProductTax> root,
 * CriteriaQuery<?> query, CriteriaBuilder cb) { return
 * cb.equal(root.get("tax").get("id"), taxId); } }; } }
 */