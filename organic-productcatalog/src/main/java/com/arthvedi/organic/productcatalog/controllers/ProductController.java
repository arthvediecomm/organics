package com.arthvedi.organic.productcatalog.controllers;

import static com.arthvedi.organic.enums.ProductType.COMBO;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.productcatalog.businessdelegate.context.ProductContext;
import com.arthvedi.organic.productcatalog.model.ProductModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/product", produces = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class ProductController {

	private IBusinessDelegate<ProductModel, ProductContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<ProductContext> productContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create")
	public ResponseEntity<ProductModel> createProduct(
			@RequestBody ProductModel productModel) {
		productModel = businessDelegate.create(productModel);
		return new ResponseEntity<ProductModel>(productModel,
				HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/combo/create")
	public ResponseEntity<ProductModel> createComboProduct(
			@RequestBody ProductModel productModel) {
		productModel.setProductType(COMBO.name());
		productModel = businessDelegate.create(productModel);
		return new ResponseEntity<ProductModel>(productModel,
				HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	public ResponseEntity<ProductModel> edit(
			@PathVariable(value = "id") final String productId,
			@RequestBody final ProductModel productModel) {

		businessDelegate.edit(keyBuilderFactory.getObject().withId(productId),
				productModel);
		return new ResponseEntity<ProductModel>(productModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<IModelWrapper<Collection<ProductModel>>> getAll() {
		ProductContext productContext = productContextFactory.getObject();
		productContext.setAll("all");
		Collection<ProductModel> productModels = businessDelegate
				.getCollection(productContext);
		IModelWrapper<Collection<ProductModel>> models = new CollectionModelWrapper<ProductModel>(
				ProductModel.class, productModels);
		return new ResponseEntity<IModelWrapper<Collection<ProductModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/productlist", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<IModelWrapper<Collection<ProductModel>>> getProductList() {
		ProductContext productContext = productContextFactory.getObject();
		productContext.setProductList("productList");
		Collection<ProductModel> productModels = businessDelegate
				.getCollection(productContext);
		IModelWrapper<Collection<ProductModel>> models = new CollectionModelWrapper<ProductModel>(
				ProductModel.class, productModels);
		return new ResponseEntity<IModelWrapper<Collection<ProductModel>>>(
				models, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/productlistbycategory/{status}", consumes = { MediaType.ALL_VALUE })
	
	public ResponseEntity<IModelWrapper<Collection<ProductModel>>> getByStatus(
			@PathVariable(value = "status") final String status) {
		ProductContext productContext = productContextFactory.getObject();
		productContext.setStatus(status);
		Collection<ProductModel> productModels = businessDelegate
				.getCollection(productContext);
		IModelWrapper<Collection<ProductModel>> models = new CollectionModelWrapper<ProductModel>(
				ProductModel.class, productModels);
		return new ResponseEntity<IModelWrapper<Collection<ProductModel>>>(
				models, HttpStatus.OK);
	}
	@RequestMapping(method = RequestMethod.GET, value = "/productlistbycategorys/{categoryId}", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<IModelWrapper<Collection<ProductModel>>> getProductByCategory(
			@PathVariable(value = "categoryId") final String categoryId) {
		ProductContext productContext = productContextFactory.getObject();
		productContext.setCategoryId("categoryId");
		Collection<ProductModel> productModels = businessDelegate
				.getCollection(productContext);
		IModelWrapper<Collection<ProductModel>> models = new CollectionModelWrapper<ProductModel>(
				ProductModel.class, productModels);
		return new ResponseEntity<IModelWrapper<Collection<ProductModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/productbyproductname/{productName}", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<IModelWrapper<Collection<ProductModel>>> getByCategoryName(
			@PathVariable(value = "productName") final String productName) {
		ProductContext productContext = productContextFactory.getObject();
		productContext.setProductNames("productNames");
		Collection<ProductModel> productModels = businessDelegate
				.getCollection(productContext);
		IModelWrapper<Collection<ProductModel>> models = new CollectionModelWrapper<ProductModel>(
				ProductModel.class, productModels);
		return new ResponseEntity<IModelWrapper<Collection<ProductModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<ProductModel> getProduct(
			@PathVariable(value = "id") final String productId) {
		ProductContext productContext = productContextFactory.getObject();

		ProductModel model = businessDelegate.getByKey(keyBuilderFactory
				.getObject().withId(productId), productContext);
		return new ResponseEntity<ProductModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */

	@Resource(name = "productBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<ProductModel, ProductContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setProductObjectFactory(
			final ObjectFactory<ProductContext> productContextFactory) {
		this.productContextFactory = productContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(
			final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
