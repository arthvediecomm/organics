package com.arthvedi.organic.productcatalog.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.productcatalog.businessdelegate.context.CategoryFeatureContext;
import com.arthvedi.organic.productcatalog.domain.CategoryFeature;
import com.arthvedi.organic.productcatalog.model.CategoryFeatureModel;
import com.arthvedi.organic.productcatalog.service.ICategoryFeatureService;

/*
 *@Author varma
 */

@Service
public class CategoryFeatureBusinessDelegate
		implements
		IBusinessDelegate<CategoryFeatureModel, CategoryFeatureContext, IKeyBuilder<String>, String> {

	@Autowired
	private ICategoryFeatureService categoryFeatureService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public CategoryFeatureModel create(CategoryFeatureModel model) {

		CategoryFeature categoryFeature = categoryFeatureService
				.create((CategoryFeature) conversionService.convert(model,
						forObject(model), valueOf(CategoryFeature.class)));

		model = convertToCategoryFeatureModel(categoryFeature);
		return model;
	}

	private CategoryFeatureModel convertToCategoryFeatureModel(
			CategoryFeature categoryFeature) {
		return (CategoryFeatureModel) conversionService.convert(
				categoryFeature, forObject(categoryFeature),
				valueOf(CategoryFeatureModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			CategoryFeatureContext context) {

	}

	@Override
	public CategoryFeatureModel edit(IKeyBuilder<String> keyBuilder,
			CategoryFeatureModel model) {
		CategoryFeature categoryFeature = categoryFeatureService
				.getCategoryFeature(keyBuilder.build().toString());

		categoryFeature = categoryFeatureService
				.updateCategoryFeature((CategoryFeature) conversionService
						.convert(model, forObject(model),
								valueOf(CategoryFeature.class)));
		model = convertToCategoryFeatureModel(categoryFeature);
		return model;
	}

	@Override
	public CategoryFeatureModel getByKey(IKeyBuilder<String> keyBuilder,
			CategoryFeatureContext context) {
		CategoryFeature categoryFeature = categoryFeatureService
				.getCategoryFeature(keyBuilder.build().toString());
		CategoryFeatureModel model = conversionService.convert(categoryFeature,
				CategoryFeatureModel.class);
		return model;
	}

	@Override
	public Collection<CategoryFeatureModel> getCollection(
			CategoryFeatureContext context) {
		List<CategoryFeature> categoryFeature = new ArrayList<CategoryFeature>();
		if (context.getAll() != null) {
			categoryFeature = categoryFeatureService.getAll();
		}
		if (context.getCategoryFeatureList() != null) {
			categoryFeature = categoryFeatureService.getCategoryFeatureList();
		}
		List<CategoryFeatureModel> categoryFeatureModels = (List<CategoryFeatureModel>) conversionService
				.convert(categoryFeature, TypeDescriptor
						.forObject(categoryFeature), TypeDescriptor.collection(
						List.class,
						TypeDescriptor.valueOf(CategoryFeatureModel.class)));
		return categoryFeatureModels;
	}

	@Override
	public CategoryFeatureModel edit(IKeyBuilder<String> keyBuilder,
			CategoryFeatureModel model, CategoryFeatureContext context) {
		return null;
	}

}
