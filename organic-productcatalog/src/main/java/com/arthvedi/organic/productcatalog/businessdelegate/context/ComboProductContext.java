package com.arthvedi.organic.productcatalog.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;
/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ComboProductContext implements IBusinessDelegateContext {

	private String comboProductId;
	private String all;
private String comboProductList;
	


	public String getComboProductList() {
	return comboProductList;
}

public void setComboProductList(String comboProductList) {
	this.comboProductList = comboProductList;
}

	public String getComboProductId() {
		return comboProductId;
	}

	public void setComboProductId(String comboProductId) {
		this.comboProductId = comboProductId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

}
