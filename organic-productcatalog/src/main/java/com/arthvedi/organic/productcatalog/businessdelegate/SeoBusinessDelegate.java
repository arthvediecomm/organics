package com.arthvedi.organic.productcatalog.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.productcatalog.businessdelegate.context.SeoContext;
import com.arthvedi.organic.productcatalog.domain.Seo;
import com.arthvedi.organic.productcatalog.model.SeoModel;
import com.arthvedi.organic.productcatalog.service.ISeoService;

/*
 *@Author varma
 */

@Service
public class SeoBusinessDelegate implements
		IBusinessDelegate<SeoModel, SeoContext, IKeyBuilder<String>, String> {

	@Autowired
	private ISeoService seoService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public SeoModel create(SeoModel model) {
		Seo seo = seoService.create((Seo) conversionService.convert(model,
				forObject(model), valueOf(Seo.class)));
		model = convertToSeoModel(seo);
		return model;
	}

	private SeoModel convertToSeoModel(Seo seo) {
		return (SeoModel) conversionService.convert(seo, forObject(seo),
				valueOf(SeoModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, SeoContext context) {

	}

	@Override
	public SeoModel edit(IKeyBuilder<String> keyBuilder, SeoModel model) {
		Seo seo = seoService.getSeo(keyBuilder.build().toString());
		seo = seoService.updateSeo((Seo) conversionService.convert(model,
				forObject(model), valueOf(Seo.class)));
		model = convertToSeoModel(seo);
		return model;
	}

	@Override
	public SeoModel getByKey(IKeyBuilder<String> keyBuilder, SeoContext context) {
		Seo seo = seoService.getSeo(keyBuilder.build().toString());
		SeoModel model = conversionService.convert(seo, SeoModel.class);
		return model;
	}

	@Override
	public Collection<SeoModel> getCollection(SeoContext context) {
		List<Seo> seo = new ArrayList<Seo>();
		if (context.getAll() != null) {
			seo = seoService.getAll();
		}
		List<SeoModel> seoModels = (List<SeoModel>) conversionService.convert(
				seo,
				TypeDescriptor.forObject(seo),
				TypeDescriptor.collection(List.class,
						TypeDescriptor.valueOf(SeoModel.class)));
		return seoModels;
	}

	@Override
	public SeoModel edit(IKeyBuilder<String> keyBuilder, SeoModel model,
			SeoContext context) {
		return null;
	}

}
