package com.arthvedi.organic.productcatalog.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;
/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ProductImagesContext implements IBusinessDelegateContext {

	private String productImagesId;
	private String all;
	
	public String getProductImagesId() {
		return productImagesId;
	}

	public void setProductImagesId(String productImagesId) {
		this.productImagesId = productImagesId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

}
