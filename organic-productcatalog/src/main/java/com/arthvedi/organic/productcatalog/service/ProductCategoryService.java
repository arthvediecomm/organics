package com.arthvedi.organic.productcatalog.service;

import static com.arthvedi.organic.enums.Status.ACTIVE;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.productcatalog.domain.ProductCategory;
import com.arthvedi.organic.productcatalog.respository.ProductCategoryRepository;

/*
 *@Author varma
 */
@Component
public class ProductCategoryService implements IProductCategoryService {
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;

	@Autowired
	private ProductCategoryRepository productCategoryRepository;

	@Override
	public ProductCategory create(ProductCategory productCategory) {
		productCategory.setCreatedDate(new LocalDateTime());
		productCategory.setUserCreated("varma");
		productCategory.setStatus(ACTIVE.name());
		return productCategoryRepository.save(productCategory);
	}

	@Override
	public void deleteProductCategory(String productCategoryId) {

	}

	@Override
	public ProductCategory getProductCategory(String productCategoryId) {
		return productCategoryRepository.findOne(productCategoryId);
	}

	@Override
	public List<ProductCategory> getAll() {
		return null;
	}

	@Override
	public ProductCategory updateProductCategory(ProductCategory productCategory) {
		ProductCategory productCategorys = productCategoryRepository
				.findOne(productCategory.getId());
		try {
			nonNullBeanUtils.copyProperties(productCategorys, productCategory);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		productCategorys.setUserCreated("admin");
		productCategorys.setModifiedDate(new LocalDateTime());
		productCategory = productCategoryRepository.save(productCategorys);
		return productCategory;
	}

	@Override
	public List<ProductCategory> getProductCategoryList() {
		List<ProductCategory> productCategories = (List<ProductCategory>) productCategoryRepository
				.findAll();
		List<ProductCategory> productCategoriess = new ArrayList<ProductCategory>();
		for (ProductCategory productCategory : productCategories) {
			ProductCategory productCat = new ProductCategory();

			productCat.setDescription(productCategory.getDescription());
			productCategoriess.add(productCat);
		}

		return productCategoriess;

	}

}
