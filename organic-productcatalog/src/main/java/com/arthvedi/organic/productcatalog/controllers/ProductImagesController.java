package com.arthvedi.organic.productcatalog.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.productcatalog.businessdelegate.context.ProductImagesContext;
import com.arthvedi.organic.productcatalog.model.ProductImagesModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/productImages", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class ProductImagesController {

	private IBusinessDelegate<ProductImagesModel, ProductImagesContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<ProductImagesContext> productImagesContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<ProductImagesModel> createProductImages(@RequestBody final ProductImagesModel productImagesModel) {
		businessDelegate.create(productImagesModel);
		return new ResponseEntity<ProductImagesModel>(productImagesModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	public ResponseEntity<ProductImagesModel> edit(@PathVariable(value = "id") final String productImagesId,
			@RequestBody final ProductImagesModel productImagesModel) {

		businessDelegate.edit(keyBuilderFactory.getObject().withId(productImagesId), productImagesModel);
		return new ResponseEntity<ProductImagesModel>(productImagesModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<IModelWrapper<Collection<ProductImagesModel>>> getAll() {
		ProductImagesContext productImagesContext = productImagesContextFactory.getObject();
		productImagesContext.setAll("all");
		Collection<ProductImagesModel> productImagesModels = businessDelegate.getCollection(productImagesContext);
		IModelWrapper<Collection<ProductImagesModel>> models = new CollectionModelWrapper<ProductImagesModel>(
				ProductImagesModel.class, productImagesModels);
		return new ResponseEntity<IModelWrapper<Collection<ProductImagesModel>>>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<ProductImagesModel> getProductImages(@PathVariable(value = "id") final String productImagesId) {
		ProductImagesContext productImagesContext = productImagesContextFactory.getObject();

		ProductImagesModel model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(productImagesId),
				productImagesContext);
		return new ResponseEntity<ProductImagesModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "productImagesBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<ProductImagesModel, ProductImagesContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setProductImagesObjectFactory(final ObjectFactory<ProductImagesContext> productImagesContextFactory) {
		this.productImagesContextFactory = productImagesContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
