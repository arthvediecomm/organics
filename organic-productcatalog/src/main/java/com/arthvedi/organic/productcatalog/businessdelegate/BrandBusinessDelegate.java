package com.arthvedi.organic.productcatalog.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.productcatalog.businessdelegate.context.BrandContext;
import com.arthvedi.organic.productcatalog.domain.Brand;
import com.arthvedi.organic.productcatalog.domain.BrandImages;
import com.arthvedi.organic.productcatalog.model.BrandImagesModel;
import com.arthvedi.organic.productcatalog.model.BrandModel;
import com.arthvedi.organic.productcatalog.service.IBrandImagesService;
import com.arthvedi.organic.productcatalog.service.IBrandService;

/*
 *@Author varma
 */

@Service
public class BrandBusinessDelegate
		implements
		IBusinessDelegate<BrandModel, BrandContext, IKeyBuilder<String>, String> {

	@Autowired
	private IBrandService brandService;
	@Autowired
	private ConversionService conversionService;
	@Autowired
	private IBrandImagesService brandImageService;

	public BrandModel create(BrandModel model) {
		boolean brandName = checkBrandNameExist(model.getName());
		if (brandName) {
			model.setStatusMessage("FAILURE::: Brand with name '"
					+ model.getName() + "' Already Exists");
		} else {
			Brand brand = brandService.create((Brand) conversionService
					.convert(model, forObject(model), valueOf(Brand.class)));

			model = convertToBrandModel(brand);
			model.setStatusMessage("SUCCESS::: Brand with name '"
					+ model.getName() + "' created successfully");
		}
		return model;
	}

	private boolean checkBrandNameExist(String name) {
		return brandService.checkBrandNameExist(name);
	}

	@Transactional
	private Set<BrandImages> addBrandImages(Brand brand,
			List<BrandImagesModel> brandImagesModels) {
		Set<BrandImages> brandImages = new HashSet<BrandImages>();
		for (BrandImagesModel brandImagesesModel : brandImagesModels) {
			brandImagesesModel.setBrandId(brand.getId());
			BrandImages brandImagess = brandImageService
					.create((BrandImages) conversionService.convert(
							brandImagesesModel, forObject(brandImagesesModel),
							valueOf(BrandImages.class)));
			brandImages.add(brandImagess);

		}
		return brandImages;
	}

	/*
	 * private boolean checkBrandNameExist(String name) { return
	 * brandService.checkBrandNameExist(name); }
	 */

	private BrandModel convertToBrandModel(Brand brand) {
		return (BrandModel) conversionService.convert(brand, forObject(brand),
				valueOf(BrandModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, BrandContext context) {

	}

	@Override
	public BrandModel edit(IKeyBuilder<String> keyBuilder, BrandModel model) {
		Brand brand = brandService.getBrand(keyBuilder.build().toString());
		brand = brandService.updateBrand((Brand) conversionService.convert(
				model, forObject(model), valueOf(Brand.class)));
		model = convertToBrandModel(brand);
		model.setStatusMessage("SUCCESS::: Brand with name '" + model.getName()
				+ "' updated successfully");
		return model;
	}

	@Override
	public BrandModel getByKey(IKeyBuilder<String> keyBuilder,
			BrandContext context) {
		Brand brand = brandService.getBrand(keyBuilder.build().toString());
		BrandModel model = conversionService.convert(brand, BrandModel.class);
		return model;
	}

	@Override
	public Collection<BrandModel> getCollection(BrandContext context) {
		List<Brand> brand = new ArrayList<Brand>();
		if (context.getAll() != null) {
			brand = brandService.getAll();
		}
		if (context.getBrandList() != null) {
			brand = brandService.getBrandList();
		}
		List<BrandModel> brandModels = (List<BrandModel>) conversionService
				.convert(
						brand,
						TypeDescriptor.forObject(brand),
						TypeDescriptor.collection(List.class,
								TypeDescriptor.valueOf(BrandModel.class)));
		return brandModels;
	}

	@Override
	public BrandModel edit(IKeyBuilder<String> keyBuilder, BrandModel model,
			BrandContext context) {
		return null;
	}
}
