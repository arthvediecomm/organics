/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.productcatalog.model.FeatureImagesModel;
import com.arthvedi.organic.productcatalog.representation.siren.FeatureImagesRepresentation;

/**
 * @author varma
 *
 */
@Component("featureImagesModelToFeatureImagesRepresentationConverter")
public class FeatureImagesModelToFeatureImagesRepresentationConverter extends PropertyCopyingConverter<FeatureImagesModel, FeatureImagesRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public FeatureImagesRepresentation convert(final FeatureImagesModel source) {

        FeatureImagesRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<FeatureImagesRepresentation> factory) {
        super.setFactory(factory);
    }

}
