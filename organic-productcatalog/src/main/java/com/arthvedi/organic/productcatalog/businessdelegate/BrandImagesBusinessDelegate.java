package com.arthvedi.organic.productcatalog.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.productcatalog.businessdelegate.context.BrandImagesContext;
import com.arthvedi.organic.productcatalog.domain.BrandImages;
import com.arthvedi.organic.productcatalog.model.BrandImagesModel;
import com.arthvedi.organic.productcatalog.service.IBrandImagesService;

/*
 *@Author varma
 */

@Service
public class BrandImagesBusinessDelegate
		implements
		IBusinessDelegate<BrandImagesModel, BrandImagesContext, IKeyBuilder<String>, String> {

	@Autowired
	private IBrandImagesService brandImagesService;
	@Autowired
	private ConversionService conversionService;

	@Override
	@Transactional
	public BrandImagesModel create(BrandImagesModel model) {
		BrandImages brandImages = brandImagesService
				.create((BrandImages) conversionService.convert(model,
						forObject(model), valueOf(BrandImages.class)));
		model = convertToBrandImagesModel(brandImages);
		return model;
	}

	private BrandImagesModel convertToBrandImagesModel(BrandImages brandImages) {
		return (BrandImagesModel) conversionService.convert(brandImages,
				forObject(brandImages), valueOf(BrandImagesModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			BrandImagesContext context) {

	}

	@Override
	public BrandImagesModel edit(IKeyBuilder<String> keyBuilder,
			BrandImagesModel model) {
		BrandImages brandImages = brandImagesService.getBrandImages(keyBuilder
				.build().toString());

		brandImages = brandImagesService
				.updateBrandImages((BrandImages) conversionService.convert(
						model, forObject(model), valueOf(BrandImages.class)));
		model = convertToBrandImagesModel(brandImages);
		model.setStatusMessage("SUCCESS::: Product Created Successfully");
		return model;
	}

	@Override
	public BrandImagesModel getByKey(IKeyBuilder<String> keyBuilder,
			BrandImagesContext context) {
		BrandImages brandImages = brandImagesService.getBrandImages(keyBuilder
				.build().toString());
		BrandImagesModel model = conversionService.convert(brandImages,
				BrandImagesModel.class);
		return model;
	}

	@Override
	public Collection<BrandImagesModel> getCollection(BrandImagesContext context) {
		List<BrandImages> brandImages = new ArrayList<BrandImages>();

		List<BrandImagesModel> brandImagesModels = (List<BrandImagesModel>) conversionService
				.convert(
						brandImages,
						TypeDescriptor.forObject(brandImages),
						TypeDescriptor.collection(List.class,
								TypeDescriptor.valueOf(BrandImagesModel.class)));
		return brandImagesModels;
	}

	@Override
	public BrandImagesModel edit(IKeyBuilder<String> keyBuilder,
			BrandImagesModel model, BrandImagesContext context) {
		return null;
	}

}
