package com.arthvedi.organic.productcatalog.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;

/*
 *@Author varma
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ProductNamesContext implements IBusinessDelegateContext {

	private String productNamesId;
	private String all;
	private String productNamesList;
	private String categoryId;

	
	
	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getProductNamesList() {
		return productNamesList;
	}

	public void setProductNamesList(String productNamesList) {
		this.productNamesList = productNamesList;
	}

	public String getProductNamesId() {
		return productNamesId;
	}

	public void setProductNamesId(String productNamesId) {
		this.productNamesId = productNamesId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

}
