package com.arthvedi.organic.productcatalog.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.productcatalog.businessdelegate.context.FeatureContext;
import com.arthvedi.organic.productcatalog.domain.Feature;
import com.arthvedi.organic.productcatalog.model.FeatureModel;
import com.arthvedi.organic.productcatalog.service.IFeatureService;

/*
 *@Author varma
 */

@Service
public class FeatureBusinessDelegate
		implements
		IBusinessDelegate<FeatureModel, FeatureContext, IKeyBuilder<String>, String> {

	@Autowired
	private IFeatureService featureService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public FeatureModel create(FeatureModel model) {
		Feature feature = featureService.create((Feature) conversionService
				.convert(model, forObject(model), valueOf(Feature.class)));
		model = convertToFeatureModel(feature);
		return model;
	}

	private FeatureModel convertToFeatureModel(Feature feature) {
		return (FeatureModel) conversionService.convert(feature,
				forObject(feature), valueOf(FeatureModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, FeatureContext context) {

	}

	@Override
	public FeatureModel edit(IKeyBuilder<String> keyBuilder, FeatureModel model) {
		Feature feature = featureService.getFeature(keyBuilder.build()
				.toString());

		feature = featureService.updateFeature((Feature) conversionService
				.convert(model, forObject(model), valueOf(Feature.class)));
		model = convertToFeatureModel(feature);
		return model;
	}

	@Override
	public FeatureModel getByKey(IKeyBuilder<String> keyBuilder,
			FeatureContext context) {
		Feature feature = featureService.getFeature(keyBuilder.build()
				.toString());
		FeatureModel model = conversionService.convert(feature,
				FeatureModel.class);
		return model;
	}

	@Override
	public Collection<FeatureModel> getCollection(FeatureContext context) {
		List<Feature> feature = new ArrayList<Feature>();
		if (context.getAll() != null) {
			feature = featureService.getAll();
		}
		if (context.getFeatureList() != null) {
			feature = featureService.getFeatureList();
		}

		if (context.getCategoryId() != null) {
			feature = featureService.getFeatureByCategoryId(context
					.getCategoryId());
		}
		if(context.getStatus()!=null){
			feature=featureService.getFeatureByStatus(context.getStatus());
		}
		List<FeatureModel> featureModels = (List<FeatureModel>) conversionService
				.convert(
						feature,
						TypeDescriptor.forObject(feature),
						TypeDescriptor.collection(List.class,
								TypeDescriptor.valueOf(FeatureModel.class)));
		return featureModels;
	}

	@Override
	public FeatureModel edit(IKeyBuilder<String> keyBuilder,
			FeatureModel model, FeatureContext context) {
		return null;
	}

}
