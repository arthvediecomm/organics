/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.productcatalog.model.ProductImagesModel;
import com.arthvedi.organic.productcatalog.representation.siren.ProductImagesRepresentation;

/**
 * @author varma
 *
 */
@Component("productImagesModelToProductImagesRepresentationConverter")
public class ProductImagesModelToProductImagesRepresentationConverter extends PropertyCopyingConverter<ProductImagesModel, ProductImagesRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public ProductImagesRepresentation convert(final ProductImagesModel source) {

        ProductImagesRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<ProductImagesRepresentation> factory) {
        super.setFactory(factory);
    }

}
