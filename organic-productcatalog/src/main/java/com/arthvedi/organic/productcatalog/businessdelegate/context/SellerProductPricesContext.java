package com.arthvedi.organic.productcatalog.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;

/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SellerProductPricesContext implements IBusinessDelegateContext {

	private String sellerProductPricesId;
	private String all;
	private String sellerProductPricesList;

	public String getSellerProductPricesList() {
		return sellerProductPricesList;
	}

	public void setSellerProductPricesList(String sellerProductPricesList) {
		this.sellerProductPricesList = sellerProductPricesList;
	}

	public String getSellerProductPricesId() {
		return sellerProductPricesId;
	}

	public void setSellerProductPricesId(String sellerProductPricesId) {
		this.sellerProductPricesId = sellerProductPricesId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

}
