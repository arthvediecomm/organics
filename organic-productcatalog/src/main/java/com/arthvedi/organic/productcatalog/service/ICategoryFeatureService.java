package com.arthvedi.organic.productcatalog.service;

import java.util.List;

import com.arthvedi.organic.productcatalog.domain.CategoryFeature;
/*
*@Author varma
*/
public interface ICategoryFeatureService {
	
	CategoryFeature create(CategoryFeature categoryFeature);

	void deleteCategoryFeature(String categoryFeatureId);

	CategoryFeature getCategoryFeature(String categoryFeatureId);

	List<CategoryFeature> getAll();

	CategoryFeature updateCategoryFeature(CategoryFeature categoryFeature);

	List<CategoryFeature> getCategoryFeatureList();
}
