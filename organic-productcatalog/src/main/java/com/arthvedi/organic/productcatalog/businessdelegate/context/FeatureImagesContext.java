package com.arthvedi.organic.productcatalog.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;
/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class FeatureImagesContext implements IBusinessDelegateContext {

	private String featureImagesId;
	private String all;
	
	public String getFeatureImagesId() {
		return featureImagesId;
	}

	public void setFeatureImagesId(String featureImagesId) {
		this.featureImagesId = featureImagesId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

}
