package com.arthvedi.organic.productcatalog.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.productcatalog.businessdelegate.context.ProductFeatureContext;
import com.arthvedi.organic.productcatalog.model.ProductFeatureModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/productfeature", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class ProductFeatureController {

	private IBusinessDelegate<ProductFeatureModel, ProductFeatureContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<ProductFeatureContext> productFeatureContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<ProductFeatureModel> createProductFeature(@RequestBody final ProductFeatureModel productFeatureModel) {
		businessDelegate.create(productFeatureModel);
		return new ResponseEntity<ProductFeatureModel>(productFeatureModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<ProductFeatureModel> edit(@PathVariable(value = "id") final String productFeatureId,
			@RequestBody final ProductFeatureModel productFeatureModel) {

		businessDelegate.edit(keyBuilderFactory.getObject().withId(productFeatureId), productFeatureModel);
		return new ResponseEntity<ProductFeatureModel>(productFeatureModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<ProductFeatureModel>>> getAll() {
		ProductFeatureContext productFeatureContext = productFeatureContextFactory.getObject();
		productFeatureContext.setAll("all");
		Collection<ProductFeatureModel> productFeatureModels = businessDelegate.getCollection(productFeatureContext);
		IModelWrapper<Collection<ProductFeatureModel>> models = new CollectionModelWrapper<ProductFeatureModel>(
				ProductFeatureModel.class, productFeatureModels);
		return new ResponseEntity<IModelWrapper<Collection<ProductFeatureModel>>>(models, HttpStatus.OK);
	}
	@RequestMapping(method = RequestMethod.GET, value = "/productfeaturelist", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<ProductFeatureModel>>> getProductFeatureList() {
		ProductFeatureContext productFeatureContext = productFeatureContextFactory.getObject();
		productFeatureContext.setProductFeatureList("productFeatureList");
		Collection<ProductFeatureModel> productFeatureModels = businessDelegate.getCollection(productFeatureContext);
		IModelWrapper<Collection<ProductFeatureModel>> models = new CollectionModelWrapper<ProductFeatureModel>(
				ProductFeatureModel.class, productFeatureModels);
		return new ResponseEntity<IModelWrapper<Collection<ProductFeatureModel>>>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<ProductFeatureModel> getProductFeature(@PathVariable(value = "id") final String productFeatureId) {
		ProductFeatureContext productFeatureContext = productFeatureContextFactory.getObject();

		ProductFeatureModel model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(productFeatureId),
				productFeatureContext);
		return new ResponseEntity<ProductFeatureModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "productFeatureBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<ProductFeatureModel, ProductFeatureContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setProductFeatureObjectFactory(final ObjectFactory<ProductFeatureContext> productFeatureContextFactory) {
		this.productFeatureContextFactory = productFeatureContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
