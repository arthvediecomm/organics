/*package com.arthvedi.organic.productcatalog.html.businessdelegate;
package com.arthvedi.handcraft.productcatalog.html.businessdelegate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.organic.commons.domain.Language;
import com.arthvedi.organic.commons.service.ILanguageService;
import com.arthvedi.organic.html.model.ListPageModel;
import com.arthvedi.organic.productcatalog.domain.Brand;
import com.arthvedi.organic.productcatalog.domain.Category;
import com.arthvedi.organic.productcatalog.domain.CategoryImages;
import com.arthvedi.organic.productcatalog.domain.CategoryNames;
import com.arthvedi.organic.productcatalog.domain.Feature;
import com.arthvedi.organic.productcatalog.domain.Product;
import com.arthvedi.organic.productcatalog.domain.ProductImages;
import com.arthvedi.organic.productcatalog.domain.ProductNames;
import com.arthvedi.organic.productcatalog.html.context.ListPageContext;
import com.arthvedi.organic.productcatalog.model.BrandModel;
import com.arthvedi.organic.productcatalog.model.CategoryModel;
import com.arthvedi.organic.productcatalog.model.FeatureModel;
import com.arthvedi.organic.productcatalog.model.ProductModel;
import com.arthvedi.organic.productcatalog.service.IBrandService;
import com.arthvedi.organic.productcatalog.service.ICategoryNamesService;
import com.arthvedi.organic.productcatalog.service.ICategoryService;
import com.arthvedi.organic.productcatalog.service.IFeatureService;
import com.arthvedi.organic.productcatalog.service.IProductNamesService;
import com.arthvedi.organic.productcatalog.service.IProductService;
import com.arthvedi.organic.seller.domain.SellerBranch;
import com.arthvedi.organic.seller.model.SellerBranchModel;
import com.arthvedi.organic.seller.service.ISellerBranchService;

@Service
@PropertySource("classpath:application.properties")
public class ListPageBusinessDelegate
		implements IBusinessDelegate<ListPageModel, ListPageContext, IKeyBuilder<String>, String> {

	@Value("${imageUrl}")
	private String imageBaseUrl;
	@Autowired
	private ConversionService conversionService;
	@Autowired
	private ICategoryService categoryService;
	@Autowired
	private IFeatureService featureService;
	@Autowired
	private ICategoryNamesService categoryNamesService;
	@Autowired
	private ILanguageService languageService;
	@Autowired
	private IBrandService brandService;
	@Autowired
	private ISellerBranchService sellerBranchService;
	@Autowired
	private IProductService productService;
	@Autowired
	private IProductNamesService productNamesService;
	@Autowired
	private ObjectFactory<ListPageModel> listPageModelFactory;

	@Override
	public ListPageModel create(ListPageModel model) {
		return null;
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, ListPageContext context) {

	}

	@Override
	public ListPageModel edit(IKeyBuilder<String> keyBuilder, ListPageModel model) {
		return null;
	}

	@Override
	public ListPageModel getByKey(IKeyBuilder<String> keyBuilder, ListPageContext context) {
		ListPageModel model = listPageModelFactory.getObject();
		String zoneId = context.getZoneId();
		String languageId = context.getLanguageId();
		// getting default language if language is null
		if (zoneId != null && languageId == null) {
			Language language = languageService.getCityDefaultLanguageByZoneId(zoneId);
			languageId = language.getId();
		}

		// Selected Main Category
		Category category = categoryService.getCategory(keyBuilder.build().toString());
		model.setCategoryModel(conversionService.convert(category, CategoryModel.class));
		String categoryId = category.getId();

		// Features of selected Category
List<Feature> features = featureService.getFeaturesByCategory(categoryId);
		List<FeatureModel> featureModels = new ArrayList<FeatureModel>();
		if (CollectionUtils.isNotEmpty(features)) {
			List<FeatureModel> convertedFeatureModels = (List<FeatureModel>) conversionService.convert(features,
					TypeDescriptor.forObject(features),
					CollectionTypeDescriptor.forType(TypeDescriptor.valueOf(List.class), FeatureModel.class));
			featureModels.addAll(convertedFeatureModels);
			Map<String, List<FeatureModel>> featuresModelsAsMap = new HashMap<String, List<FeatureModel>>();
			for (FeatureModel featureModel : featureModels) {
				if (!featuresModelsAsMap.containsKey(featureModel.getFeatureName())) {
					List<FeatureModel> featureValuesList = new ArrayList<FeatureModel>();
					featureValuesList.add(featureModel);
					featuresModelsAsMap.put(featureModel.getFeatureName(), featureValuesList);
				} else {
					featuresModelsAsMap.get(featureModel.getFeatureName()).add(featureModel);
				}
			}
			model.setFeatureModels(featuresModelsAsMap);
		}

		// Level 2 cateogries of selected main Category
		List<Category> subCategories = categoryService.getSubCategoriesWithChildsByParentCategory(categoryId);
		List<CategoryModel> subCategoryModels = new ArrayList<CategoryModel>();
		if (subCategories != null) {
			for (Category subCategory : subCategories) {
				CategoryModel subCategoryModel = conversionService.convert(subCategory, CategoryModel.class);
				if (languageId != null) {
					CategoryNames categoryName = categoryNamesService
							.getCategoryNamesByLanguageCategory(subCategory.getId(), languageId);
					subCategoryModel.setCategoryNativeName(categoryName.getName());

				}
				if (CollectionUtils.isNotEmpty(subCategory.getCategoryImages())) {
					List<CategoryImages> imagesList = new ArrayList<CategoryImages>(subCategory.getCategoryImages());
					Map<String, CategoryImages> subCategoryImagesAsMap = imagesList.stream()
							.collect(Collectors.toMap(x -> x.getImageType().toString(), x -> x));
					subCategoryModel
							.setCategoryImageUrl(imageBaseUrl + subCategoryImagesAsMap.get("MAINIMAGE").getImageFolder()
									+ "/" + subCategoryImagesAsMap.get("MAINIMAGE").getImageName());
				}
				subCategoryModels.add(subCategoryModel);
			}
			model.setSubCategoryModels(subCategoryModels);
		}

		// Brands supplying selected category
		List<Brand> brands = brandService.getBrandByCategoryId(categoryId);
		List<BrandModel> brandModels = new ArrayList<BrandModel>();
		if (CollectionUtils.isNotEmpty(brands)) {
			List<BrandModel> convertedBrandModels = (List<BrandModel>) conversionService.convert(brands,
					TypeDescriptor.forObject(brands),
					CollectionTypeDescriptor.forType(TypeDescriptor.valueOf(List.class), BrandModel.class));
			brandModels.addAll(convertedBrandModels);

			model.setBrandModels(brandModels);
		}
		// Seller Branches of selected category and zone
		List<SellerBranch> sellerBranches = new ArrayList<SellerBranch>();
		if (zoneId != null) {
			sellerBranches = sellerBranchService.getSellerBranchByCategoryZone(zoneId, categoryId);
		} else {
			sellerBranches = sellerBranchService.getSellerBranchByZone(zoneId);
		}
		if (CollectionUtils.isNotEmpty(sellerBranches)) {
			List<SellerBranchModel> sellerBranchModels = (List<SellerBranchModel>) conversionService.convert(
					sellerBranches, TypeDescriptor.valueOf(List.class),
					CollectionTypeDescriptor.forType(TypeDescriptor.valueOf(List.class), SellerBranchModel.class));
			model.setSellerBranchModels(sellerBranchModels);
		}

		// Products for selected criteria
		List<Product> products = productService.getProductByCriteriaContext(context);
		if (CollectionUtils.isNotEmpty(products)) {
			List<ProductModel> productModels = new ArrayList<ProductModel>();
			for (Product product : products) {
				ProductModel productModel = conversionService.convert(product, ProductModel.class);
				if (languageId != null) {
					ProductNames productName = productNamesService.getProductNameByProductLanguage(product.getId(),languageId);
					productModel.setProductNativeName(productName.getName());
				}
				if (product.getProductImageses() != null) {
					List<ProductImages> imagesList = new ArrayList<ProductImages>(product.getProductImageses());
					Map<String, ProductImages> productImagesAsMap = imagesList.stream()
							.collect(Collectors.toMap(x -> x.getImageType().toString(), x -> x));
					productModel
							.setProductImageUrl(imageBaseUrl + productImagesAsMap.get("LISTIMAGE").getImageFolder()
									+ "/" + productImagesAsMap.get("LISTIMAGE").getImageName());
				}
				productModels.add(productModel);
			}
			model.setProductModels(productModels);
		}

		return model;
	}

	@Override
	public Collection<ListPageModel> getCollection(ListPageContext context) {
		// TODO Auto-generated method stub
		return null;
	}

}
*/