package com.arthvedi.organic.productcatalog.model.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.organic.productcatalog.domain.Product;
import com.arthvedi.organic.productcatalog.model.ComboProductModel;
import com.arthvedi.organic.productcatalog.model.ProductFeatureModel;
import com.arthvedi.organic.productcatalog.model.ProductImagesModel;
import com.arthvedi.organic.productcatalog.model.ProductModel;
import com.arthvedi.organic.productcatalog.model.ProductNamesModel;

@Component("productToProductModelConverter")
public class ProductToProductModelConverter implements
		Converter<Product, ProductModel> {
	@Autowired
	private ObjectFactory<ProductModel> productModelFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public ProductModel convert(final Product source) {

		ProductModel productModel = new ProductModel();
		BeanUtils.copyProperties(source, productModel);
		if (source.getMrp() != null) {
			productModel.setMrp(source.getMrp().toString());
		}

		if (CollectionUtils.isNotEmpty(source
				.getComboProductsForChildProductId())) {
			List<ComboProductModel> converted = (List<ComboProductModel>) conversionService
					.convert(source.getComboProductsForChildProductId(),
							TypeDescriptor.forObject(source
									.getComboProductsForChildProductId()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									ComboProductModel.class));
			productModel.getComboProductsForChildProductIdModel().addAll(
					converted);
		}
		if (CollectionUtils.isNotEmpty(source
				.getComboProductsForParentProductId())) {
			List<ComboProductModel> converted = (List<ComboProductModel>) conversionService
					.convert(source.getComboProductsForParentProductId(),
							TypeDescriptor.forObject(source
									.getComboProductsForParentProductId()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									ComboProductModel.class));
			productModel.getComboProductsForParentProductIdModel().addAll(
					converted);
		}
		if (CollectionUtils.isNotEmpty(source.getProductNameses())) {
			List<ProductNamesModel> convert = (List<ProductNamesModel>) conversionService
					.convert(source.getProductNameses(), TypeDescriptor
							.forObject(source.getProductNameses()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									ProductNamesModel.class));
			productModel.getProductNamesesModel().addAll(convert);
		}
		if (CollectionUtils.isEmpty(source.getProductFeatures())) {
			List<ProductFeatureModel> convert = (List<ProductFeatureModel>) conversionService
					.convert(source.getProductFeatures(), TypeDescriptor
							.forObject(source.getProductFeatures()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									ProductFeatureModel.class));
			productModel.getProductFeaturesModel().addAll(convert);
		}
		if (CollectionUtils.isNotEmpty(source.getProductImageses())) {
			List<ProductImagesModel> convert = (List<ProductImagesModel>) conversionService
					.convert(source.getProductImageses(), TypeDescriptor
							.forObject(source.getProductImageses()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									ProductImagesModel.class));

			productModel.getProductImagesesModel().addAll(convert);
		}
		return productModel;
	}

	@Autowired
	public void setProductModelFactory(
			final ObjectFactory<ProductModel> productModelFactory) {
		this.productModelFactory = productModelFactory;
	}
}
