package com.arthvedi.organic.productcatalog.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.productcatalog.businessdelegate.context.FeatureImagesContext;
import com.arthvedi.organic.productcatalog.domain.FeatureImages;
import com.arthvedi.organic.productcatalog.model.FeatureImagesModel;
import com.arthvedi.organic.productcatalog.service.IFeatureImagesService;

/*
 *@Author varma
 */

@Service
public class FeatureImagesBusinessDelegate
		implements
		IBusinessDelegate<FeatureImagesModel, FeatureImagesContext, IKeyBuilder<String>, String> {

	@Autowired
	private IFeatureImagesService featureImagesService;
	@Autowired
	private ConversionService conversionService;

	@Override
	@Transactional
	public FeatureImagesModel create(FeatureImagesModel model) {
		FeatureImages featureImages = featureImagesService
				.create((FeatureImages) conversionService.convert(model,
						forObject(model), valueOf(FeatureImages.class)));
		model = convertToFeatureImagesModel(featureImages);
		return model;
	}

	private FeatureImagesModel convertToFeatureImagesModel(
			FeatureImages featureImages) {
		return (FeatureImagesModel) conversionService.convert(featureImages,
				forObject(featureImages), valueOf(FeatureImagesModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			FeatureImagesContext context) {

	}

	@Override
	public FeatureImagesModel edit(IKeyBuilder<String> keyBuilder,
			FeatureImagesModel model) {
		FeatureImages featureImages = featureImagesService
				.getFeatureImages(keyBuilder.build().toString());

		featureImages = featureImagesService
				.updateFeatureImages((FeatureImages) conversionService.convert(
						model, forObject(model), valueOf(FeatureImages.class)));
		model = convertToFeatureImagesModel(featureImages);
		return model;
	}

	@Override
	public FeatureImagesModel getByKey(IKeyBuilder<String> keyBuilder,
			FeatureImagesContext context) {
		FeatureImages featureImages = featureImagesService
				.getFeatureImages(keyBuilder.build().toString());
		FeatureImagesModel model = conversionService.convert(featureImages,
				FeatureImagesModel.class);
		return model;
	}

	@Override
	public Collection<FeatureImagesModel> getCollection(
			FeatureImagesContext context) {
		List<FeatureImages> featureImages = new ArrayList<FeatureImages>();

		List<FeatureImagesModel> featureImagesModels = (List<FeatureImagesModel>) conversionService
				.convert(featureImages,
						TypeDescriptor.forObject(featureImages), TypeDescriptor
								.collection(List.class, TypeDescriptor
										.valueOf(FeatureImagesModel.class)));
		return featureImagesModels;
	}

	@Override
	public FeatureImagesModel edit(IKeyBuilder<String> keyBuilder,
			FeatureImagesModel model, FeatureImagesContext context) {
		return null;
	}

}
