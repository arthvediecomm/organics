package com.arthvedi.organic.productcatalog.service;

import java.util.List;

import com.arthvedi.organic.productcatalog.domain.ProductCategory;
/*
*@Author varma
*/
public interface IProductCategoryService {
	
	ProductCategory create(ProductCategory productCategory);

	void deleteProductCategory(String productCategoryId);

	ProductCategory getProductCategory(String productCategoryId);

	List<ProductCategory> getAll();

	ProductCategory updateProductCategory(ProductCategory productCategory);

	List<ProductCategory> getProductCategoryList();
}
