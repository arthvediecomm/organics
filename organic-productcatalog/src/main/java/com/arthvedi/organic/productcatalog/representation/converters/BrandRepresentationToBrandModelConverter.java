/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.productcatalog.model.BrandImagesModel;
import com.arthvedi.organic.productcatalog.model.BrandModel;
import com.arthvedi.organic.productcatalog.representation.siren.BrandRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("brandRepresentationToBrandModelConverter")
public class BrandRepresentationToBrandModelConverter extends
		PropertyCopyingConverter<BrandRepresentation, BrandModel> {
	@Autowired
	private ConversionService conversionService;

	@Override
	public BrandModel convert(final BrandRepresentation source) {

		BrandModel target = super.convert(source);
		if (CollectionUtils.isNotEmpty(source.getBrandImagesRepresentation())) {
			List<BrandImagesModel> convert = (List<BrandImagesModel>) conversionService
					.convert(source.getBrandImagesRepresentation(),
							TypeDescriptor.forObject(source
									.getBrandImagesRepresentation()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									BrandImagesModel.class));
			target.getBrandImagesModels().addAll(convert);
		}
		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(final ObjectFactory<BrandModel> factory) {
		super.setFactory(factory);
	}

}
