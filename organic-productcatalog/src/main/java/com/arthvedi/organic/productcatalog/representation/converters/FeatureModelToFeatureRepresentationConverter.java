/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.productcatalog.model.FeatureModel;
import com.arthvedi.organic.productcatalog.representation.siren.FeatureRepresentation;

/**
 * @author varma
 *
 */
@Component("featureModelToFeatureRepresentationConverter")
public class FeatureModelToFeatureRepresentationConverter extends PropertyCopyingConverter<FeatureModel, FeatureRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public FeatureRepresentation convert(final FeatureModel source) {

        FeatureRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<FeatureRepresentation> factory) {
        super.setFactory(factory);
    }

}
