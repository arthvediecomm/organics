package com.arthvedi.organic.productcatalog.service;

import java.util.List;

import com.arthvedi.organic.productcatalog.domain.ComboProduct;
/*
*@Author varma
*/
public interface IComboProductService {
	
	ComboProduct create(ComboProduct comboProduct);

	void deleteComboProduct(String comboProductId);

	ComboProduct getComboProduct(String comboProductId);

	List<ComboProduct> getAll();

	ComboProduct updateComboProduct(ComboProduct comboProduct);

	List<ComboProduct> getComboProductList();
}
