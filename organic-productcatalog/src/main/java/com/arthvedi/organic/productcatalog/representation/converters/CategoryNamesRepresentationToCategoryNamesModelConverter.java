/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.productcatalog.model.CategoryNamesModel;
import com.arthvedi.organic.productcatalog.representation.siren.CategoryNamesRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("categoryNamesRepresentationToCategoryNamesModelConverter")
public class CategoryNamesRepresentationToCategoryNamesModelConverter extends PropertyCopyingConverter<CategoryNamesRepresentation, CategoryNamesModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public CategoryNamesModel convert(final CategoryNamesRepresentation source) {

        CategoryNamesModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<CategoryNamesModel> factory) {
        super.setFactory(factory);
    }

}
