package com.arthvedi.organic.productcatalog.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.productcatalog.businessdelegate.context.ProductNamesContext;
import com.arthvedi.organic.productcatalog.domain.ProductNames;
import com.arthvedi.organic.productcatalog.model.ProductNamesModel;
import com.arthvedi.organic.productcatalog.service.IProductNamesService;

/*
 *@Author varma
 */

@Service
public class ProductNamesBusinessDelegate
		implements
		IBusinessDelegate<ProductNamesModel, ProductNamesContext, IKeyBuilder<String>, String> {

	@Autowired
	private IProductNamesService productNamesService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public ProductNamesModel create(ProductNamesModel model) {
		boolean productName = chechProductNamesExists();
		if (productName) {
			model.setStatusMessage("FAILURE:::");
		}
		ProductNames productNames = productNamesService
				.create((ProductNames) conversionService.convert(model,
						forObject(model), valueOf(ProductNames.class)));
		model = convertToProductNamesModel(productNames);
		return model;
	}

	private boolean chechProductNamesExists() {
		return false;
	}

	private ProductNamesModel convertToProductNamesModel(
			ProductNames productNames) {
		return (ProductNamesModel) conversionService.convert(productNames,
				forObject(productNames), valueOf(ProductNamesModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			ProductNamesContext context) {

	}

	@Override
	public ProductNamesModel edit(IKeyBuilder<String> keyBuilder,
			ProductNamesModel model) {
		ProductNames productNames = productNamesService
				.getProductNames(keyBuilder.build().toString());
		productNames = productNamesService
				.updateProductNames((ProductNames) conversionService.convert(
						model, forObject(model), valueOf(ProductNames.class)));
		model = convertToProductNamesModel(productNames);
		return model;
	}

	@Override
	public ProductNamesModel getByKey(IKeyBuilder<String> keyBuilder,
			ProductNamesContext context) {
		ProductNames productNames = productNamesService
				.getProductNames(keyBuilder.build().toString());
		ProductNamesModel model = conversionService.convert(productNames,
				ProductNamesModel.class);
		return model;
	}

	@Override
	public Collection<ProductNamesModel> getCollection(
			ProductNamesContext context) {
		List<ProductNames> productNames = new ArrayList<ProductNames>();
		if (context.getAll() != null) {
			productNames = productNamesService.getAll();
		}
		if (context.getProductNamesList() != null) {
			productNames = productNamesService.getProductNamesList();
		}

		List<ProductNamesModel> productNamesModels = (List<ProductNamesModel>) conversionService
				.convert(

						productNames,
						TypeDescriptor.forObject(productNames),
						TypeDescriptor.collection(List.class,
								TypeDescriptor.valueOf(ProductNamesModel.class)));
		return productNamesModels;
	}

	@Override
	public ProductNamesModel edit(IKeyBuilder<String> keyBuilder,
			ProductNamesModel model, ProductNamesContext context) {
		return null;
	}

}
