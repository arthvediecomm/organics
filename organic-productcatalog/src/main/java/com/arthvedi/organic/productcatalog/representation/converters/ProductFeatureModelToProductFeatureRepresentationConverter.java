/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.productcatalog.model.ProductFeatureModel;
import com.arthvedi.organic.productcatalog.representation.siren.ProductFeatureRepresentation;

/**
 * @author varma
 *
 */
@Component("productFeatureModelToProductFeatureRepresentationConverter")
public class ProductFeatureModelToProductFeatureRepresentationConverter extends PropertyCopyingConverter<ProductFeatureModel, ProductFeatureRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public ProductFeatureRepresentation convert(final ProductFeatureModel source) {

        ProductFeatureRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<ProductFeatureRepresentation> factory) {
        super.setFactory(factory);
    }

}
