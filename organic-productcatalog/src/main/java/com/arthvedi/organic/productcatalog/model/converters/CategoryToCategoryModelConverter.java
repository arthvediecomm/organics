package com.arthvedi.organic.productcatalog.model.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.organic.productcatalog.domain.Category;
import com.arthvedi.organic.productcatalog.model.CategoryFeatureModel;
import com.arthvedi.organic.productcatalog.model.CategoryImagesModel;
import com.arthvedi.organic.productcatalog.model.CategoryModel;
import com.arthvedi.organic.productcatalog.model.CategoryNamesModel;
import com.arthvedi.organic.productcatalog.model.ProductCategoryModel;

@Component("categoryToCategoryModelConverter")
public class CategoryToCategoryModelConverter implements
		Converter<Category, CategoryModel> {
	@Autowired
	private ObjectFactory<CategoryModel> categoryModelFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public CategoryModel convert(final Category source) {
		CategoryModel categoryModel = categoryModelFactory.getObject();
		BeanUtils.copyProperties(source, categoryModel);

		if (CollectionUtils.isNotEmpty(source.getProductCategories())) {
			List<ProductCategoryModel> converted = (List<ProductCategoryModel>) conversionService
					.convert(source.getProductCategories(), TypeDescriptor
							.forObject(source.getProductCategories()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									ProductCategoryModel.class));
			categoryModel.getProductCategoriesModel().addAll(converted);
		}
		if (CollectionUtils.isNotEmpty(source.getCategoryNameses())) {
			List<CategoryNamesModel> converted = (List<CategoryNamesModel>) conversionService
					.convert(source.getCategoryNameses(), TypeDescriptor
							.forObject(source.getCategoryNameses()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									CategoryNamesModel.class));
			categoryModel.getCategoryNamesesModel().addAll(converted);
		}
		if (CollectionUtils.isNotEmpty(source.getCategoryFeatures())) {
			List<CategoryFeatureModel> converted = (List<CategoryFeatureModel>) conversionService
					.convert(source.getCategoryFeatures(), TypeDescriptor
							.forObject(source.getCategoryFeatures()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									CategoryFeatureModel.class));
			categoryModel.getCategoryFeaturesModel().addAll(converted);
		}
		if (CollectionUtils.isNotEmpty(source.getCategoryImageses())) {
			List<CategoryImagesModel> convert = (List<CategoryImagesModel>) conversionService
					.convert(source.getCategoryImageses(), TypeDescriptor
							.forObject(source.getCategoryImageses()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									CategoryImagesModel.class));
			categoryModel.getCategoryImagesesModel().addAll(convert);
		}
		return categoryModel;
	}

	@Autowired
	public void setCategoryModelFactory(
			final ObjectFactory<CategoryModel> categoryModelFactory) {
		this.categoryModelFactory = categoryModelFactory;
	}
}
