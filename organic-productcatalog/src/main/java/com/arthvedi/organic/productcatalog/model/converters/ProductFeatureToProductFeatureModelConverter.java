package com.arthvedi.organic.productcatalog.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.productcatalog.domain.ProductFeature;
import com.arthvedi.organic.productcatalog.model.ProductFeatureModel;

@Component("productFeatureToProductFeatureModelConverter")
public class ProductFeatureToProductFeatureModelConverter
        implements Converter<ProductFeature, ProductFeatureModel> {
    @Autowired
    private ObjectFactory<ProductFeatureModel> productFeatureModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public ProductFeatureModel convert(final ProductFeature source) {
        ProductFeatureModel productFeatureModel = productFeatureModelFactory.getObject();
        BeanUtils.copyProperties(source, productFeatureModel);

        return productFeatureModel;
    }

    @Autowired
    public void setProductFeatureModelFactory(
            final ObjectFactory<ProductFeatureModel> productFeatureModelFactory) {
        this.productFeatureModelFactory = productFeatureModelFactory;
    }
}
