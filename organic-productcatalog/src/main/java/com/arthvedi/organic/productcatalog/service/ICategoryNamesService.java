package com.arthvedi.organic.productcatalog.service;

import java.util.List;

import com.arthvedi.organic.productcatalog.domain.CategoryNames;
/*
*@Author varma
*/
public interface ICategoryNamesService {
	
	CategoryNames create(CategoryNames categoryNames);

	void deleteCategoryNames(String categoryNamesId);

	CategoryNames getCategoryNames(String categoryNamesId);

	List<CategoryNames> getAll();

	CategoryNames updateCategoryNames(CategoryNames categoryNames);

	List<CategoryNames> getCategoryNamesList();
}
