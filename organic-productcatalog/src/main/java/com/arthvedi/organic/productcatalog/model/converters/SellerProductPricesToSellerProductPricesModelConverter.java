package com.arthvedi.organic.productcatalog.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.productcatalog.domain.SellerProductPrices;
import com.arthvedi.organic.productcatalog.model.SellerProductPricesModel;

@Component("sellerProductPricesToSellerProductPricesModelConverter")
public class SellerProductPricesToSellerProductPricesModelConverter
        implements Converter<SellerProductPrices, SellerProductPricesModel> {
    @Autowired
    private ObjectFactory<SellerProductPricesModel> sellerProductPricesModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public SellerProductPricesModel convert(final SellerProductPrices source) {
        SellerProductPricesModel sellerProductPricesModel = sellerProductPricesModelFactory.getObject();
        BeanUtils.copyProperties(source, sellerProductPricesModel);
        
        if(source.getOfferPrice()!=null){
        	sellerProductPricesModel.setOfferPrice(source.getOfferPrice().toString());
        }
        if(source.getSellerPrice()!=null){
        	sellerProductPricesModel.setSellerPrice(source.getSellerPrice().toString());
        }
        if(source.getSellingPrice()!=null){
        	sellerProductPricesModel.setSellingPrice(source.getSellingPrice().toString());
        }

        if(source.getDiscount()!=null){
        	sellerProductPricesModel.setDiscount(source.getDiscount().toString());
        }
        return sellerProductPricesModel;
    }

    @Autowired
    public void setSellerProductPricesModelFactory(
            final ObjectFactory<SellerProductPricesModel> sellerProductPricesModelFactory) {
        this.sellerProductPricesModelFactory = sellerProductPricesModelFactory;
    }
}
