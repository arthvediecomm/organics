package com.arthvedi.organic.productcatalog.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.productcatalog.domain.BrandImages;
import com.arthvedi.organic.productcatalog.model.BrandImagesModel;

@Component("brandImagesToBrandImagesModelConverter")
public class BrandImagesToBrandImagesModelConverter
        implements Converter<BrandImages, BrandImagesModel> {
    @Autowired
    private ObjectFactory<BrandImagesModel> brandImagesModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public BrandImagesModel convert(final BrandImages source) {
        BrandImagesModel brandImagesModel = brandImagesModelFactory.getObject();
        BeanUtils.copyProperties(source, brandImagesModel);

        return brandImagesModel;
    }

    @Autowired
    public void setBrandImagesModelFactory(
            final ObjectFactory<BrandImagesModel> brandImagesModelFactory) {
        this.brandImagesModelFactory = brandImagesModelFactory;
    }
}
