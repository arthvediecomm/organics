/**
 *
 */
package com.arthvedi.organic.productcatalog.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.commons.domain.Language;
import com.arthvedi.organic.productcatalog.domain.Product;
import com.arthvedi.organic.productcatalog.domain.ProductNames;
import com.arthvedi.organic.productcatalog.domain.Seo;
import com.arthvedi.organic.productcatalog.model.ProductNamesModel;

/**
 * @author Jay
 *
 */
@Component("productNamesModelToProductNamesConverter")
public class ProductNamesModelToProductNamesConverter implements
		Converter<ProductNamesModel, ProductNames> {
	@Autowired
	private ObjectFactory<ProductNames> productNamesFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public ProductNames convert(final ProductNamesModel source) {
		ProductNames productNames = new ProductNames();
		BeanUtils.copyProperties(source, productNames);

		if (source.getSeoKeyword() != null) {
			Seo seo = new Seo();
			seo.setSeoDescription(source.getSeoDescription());
			seo.setSeoKeyword(source.getSeoKeyword());
			seo.setSeoTitle(source.getSeoTitle());
			if (source.getSeoId() != null) {
				seo.setId(source.getSeoId());
			}
			productNames.setSeo(seo);
		}
		if (source.getProductId() != null) {
			Product product = new Product();
			product.setId(source.getProductId());
			productNames.setProduct(product);
		}
		if (source.getLanguageId() != null) {
			Language language = new Language();
			language.setId(source.getLanguageId());
			productNames.setLanguage(language);
		}
		return productNames;
	}

	@Autowired
	public void setProductNamesFactory(
			final ObjectFactory<ProductNames> productNamesFactory) {
		this.productNamesFactory = productNamesFactory;
	}

}
