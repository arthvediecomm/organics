/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.productcatalog.model.CategoryImagesModel;
import com.arthvedi.organic.productcatalog.representation.siren.CategoryImagesRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("categoryImagesRepresentationToCategoryImagesModelConverter")
public class CategoryImagesRepresentationToCategoryImagesModelConverter extends PropertyCopyingConverter<CategoryImagesRepresentation, CategoryImagesModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public CategoryImagesModel convert(final CategoryImagesRepresentation source) {

        CategoryImagesModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<CategoryImagesModel> factory) {
        super.setFactory(factory);
    }

}
