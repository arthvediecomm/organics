/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.productcatalog.model.CategoryModel;
import com.arthvedi.organic.productcatalog.representation.siren.CategoryFeatureRepresentation;
import com.arthvedi.organic.productcatalog.representation.siren.CategoryImagesRepresentation;
import com.arthvedi.organic.productcatalog.representation.siren.CategoryNamesRepresentation;
import com.arthvedi.organic.productcatalog.representation.siren.CategoryRepresentation;
import com.arthvedi.organic.productcatalog.representation.siren.ProductCategoryRepresentation;

/**
 * @author varma
 *
 */
@Component("categoryModelToCategoryRepresentationConverter")
public class CategoryModelToCategoryRepresentationConverter extends
		PropertyCopyingConverter<CategoryModel, CategoryRepresentation> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public CategoryRepresentation convert(final CategoryModel source) {

		CategoryRepresentation target = super.convert(source);

		if (CollectionUtils.isNotEmpty(source.getCategoryNamesesModel())) {
			List<CategoryNamesRepresentation> converted = (List<CategoryNamesRepresentation>) conversionService
					.convert(source.getCategoryNamesesModel(), TypeDescriptor
							.forObject(source.getCategoryNamesesModel()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									CategoryNamesRepresentation.class));
			target.getCategoryNamesesRepresentation().addAll(converted);
		}
		if (CollectionUtils.isNotEmpty(source.getCategoryFeaturesModel())) {
			List<CategoryFeatureRepresentation> converted = (List<CategoryFeatureRepresentation>) conversionService
					.convert(source.getCategoryFeaturesModel(), TypeDescriptor
							.forObject(source.getCategoryFeaturesModel()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									CategoryFeatureRepresentation.class));
			target.getCategoryFeaturesRepresentation().addAll(converted);
		}
		if (CollectionUtils.isNotEmpty(source.getProductCategoriesModel())) {
			List<ProductCategoryRepresentation> converted = (List<ProductCategoryRepresentation>) conversionService
					.convert(source.getProductCategoriesModel(), TypeDescriptor
							.forObject(source.getProductCategoriesModel()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									ProductCategoryRepresentation.class));
			target.getProductCategoriesRepresentation().addAll(converted);
		}
		if (CollectionUtils.isNotEmpty(source.getCategoryImagesesModel())) {
			List<CategoryImagesRepresentation> convert = (List<CategoryImagesRepresentation>) conversionService
					.convert(source.getCategoryImagesesModel(), TypeDescriptor
							.forObject(source.getCategoryImagesesModel()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									CategoryImagesRepresentation.class));
			target.getCategoryImagesesRepresentation().addAll(convert);
		}

		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(final ObjectFactory<CategoryRepresentation> factory) {
		super.setFactory(factory);
	}

}
