package com.arthvedi.organic.productcatalog.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.productcatalog.domain.FeatureImages;
import com.arthvedi.organic.productcatalog.respository.FeatureImagesRepository;
/*
*@Author varma
*/
@Component
public class FeatureImagesService implements IFeatureImagesService{

	@Autowired
	private FeatureImagesRepository featureImagesRepository;
	@Override
	public FeatureImages create(FeatureImages featureImages) {
		// TODO Auto-generated method stub
		return featureImagesRepository.save(featureImages);
	}

	@Override
	public void deleteFeatureImages(String featureImagesId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public FeatureImages getFeatureImages(String featureImagesId) {
		// TODO Auto-generated method stub
		 return featureImagesRepository.findOne(featureImagesId);
	}

	@Override
	public List<FeatureImages> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FeatureImages updateFeatureImages(FeatureImages featureImages) {
		// TODO Auto-generated method stub
	return featureImagesRepository.save(featureImages);
	}

}
