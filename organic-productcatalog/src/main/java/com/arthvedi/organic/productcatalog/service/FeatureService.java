package com.arthvedi.organic.productcatalog.service;

import static com.arthvedi.organic.enums.Status.ACTIVE;
import static com.arthvedi.organic.productcatalog.specification.FeatureSpecification.byCategory;
import static com.arthvedi.organic.productcatalog.specification.FeatureSpecification.byStatus;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.productcatalog.domain.Feature;
import com.arthvedi.organic.productcatalog.respository.FeatureRepository;

@Component
public class FeatureService implements IFeatureService {
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private FeatureRepository featureRepository;

	@Override
	public Feature create(Feature feature) {
		feature.setCreatedDate(new LocalDateTime());
		feature.setUserCreated("varma");
		feature.setStatus(ACTIVE.name());
		feature = featureRepository.save(feature);
		return feature;
	}

	@Override
	public void deleteFeature(String featureId) {

	}

	@Override
	public Feature getFeature(String featureId) {
		return featureRepository.findOne(featureId);
	}

	@Override
	public List<Feature> getAll() {
		return null;
	}

	@Override
	public Feature updateFeature(Feature feature) {
		Feature features = featureRepository.findOne(feature.getId());
		try {
			nonNullBeanUtils.copyProperties(features, feature);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		features.setUserModified("admin");
		features.setModifiedDate(new LocalDateTime());

		feature = featureRepository.save(features);
		return feature;
	}

	@Override
	public List<Feature> getFeatureList() {
		List<Feature> features = (List<Feature>) featureRepository.findAll();
		List<Feature> featuress = new ArrayList<Feature>();
		for (Feature fea : features) {
			Feature feature = new Feature();
			feature.setFeatureName(fea.getFeatureName());
			feature.setCategoryFeatures(fea.getCategoryFeatures());

			featuress.add(feature);
		}
		return featuress;
	}

	/*
	 * private Specification<Feature> byCategory(String categoryId) { return
	 * (Specification<Feature>)
	 * featureRepository.findAll(byCategory(categoryId)); }
	 */

	@Override
	public List<Feature> getFeatureByCategoryId(String categoryId) {
		return featureRepository.findAll(byCategory(categoryId));
	}

	@Override
	public List<Feature> getFeatureByStatus(String status) {
		return featureRepository.findAll(byStatus(status));
	}

}
