/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.productcatalog.model.ProductImagesModel;
import com.arthvedi.organic.productcatalog.representation.siren.ProductImagesRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("productImagesRepresentationToProductImagesModelConverter")
public class ProductImagesRepresentationToProductImagesModelConverter extends PropertyCopyingConverter<ProductImagesRepresentation, ProductImagesModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public ProductImagesModel convert(final ProductImagesRepresentation source) {

        ProductImagesModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<ProductImagesModel> factory) {
        super.setFactory(factory);
    }

}
