package com.arthvedi.organic.productcatalog.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.productcatalog.domain.CategoryNames;
import com.arthvedi.organic.productcatalog.model.CategoryNamesModel;

@Component("categoryNamesToCategoryNamesModelConverter")
public class CategoryNamesToCategoryNamesModelConverter
        implements Converter<CategoryNames, CategoryNamesModel> {
    @Autowired
    private ObjectFactory<CategoryNamesModel> categoryNamesModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public CategoryNamesModel convert(final CategoryNames source) {
        CategoryNamesModel categoryNamesModel = categoryNamesModelFactory.getObject();
        BeanUtils.copyProperties(source, categoryNamesModel);

        return categoryNamesModel;
    }

    @Autowired
    public void setCategoryNamesModelFactory(
            final ObjectFactory<CategoryNamesModel> categoryNamesModelFactory) {
        this.categoryNamesModelFactory = categoryNamesModelFactory;
    }
}
