package com.arthvedi.organic.productcatalog.html.context;

import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ListPageContext implements IBusinessDelegateContext {
	private List<String> subCategoryIds;
	private String categoryId;
	private List<String> brandIds;
	private List<String> featureIds;
	private String zoneId;
	private String languageId;
	private List<String> sellerBranchIds;
	private Integer pageNo;
	private Integer pageSize;
	private String sortType;
	private String sortBy;
	private String searchQuery;
	

	public String getSearchQuery() {
		return searchQuery;
	}

	public void setSearchQuery(String searchQuery) {
		this.searchQuery = searchQuery;
	}

	public String getSortType() {
		return sortType;
	}

	public void setSortType(String sortType) {
		this.sortType = sortType;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public List<String> getSubCategoryIds() {
		return subCategoryIds;
	}

	public void setSubCategoryIds(List<String> subCategoryIds) {
		this.subCategoryIds = subCategoryIds;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public List<String> getBrandIds() {
		return brandIds;
	}

	public void setBrandIds(List<String> brandIds) {
		this.brandIds = brandIds;
	}

	public List<String> getFeatureIds() {
		return featureIds;
	}

	public void setFeatureIds(List<String> featureIds) {
		this.featureIds = featureIds;
	}

	public String getZoneId() {
		return zoneId;
	}

	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}

	public String getLanguageId() {
		return languageId;
	}

	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}

	public List<String> getSellerBranchIds() {
		return sellerBranchIds;
	}

	public void setSellerBranchIds(List<String> sellerBranchIds) {
		this.sellerBranchIds = sellerBranchIds;
	}

}
