package com.arthvedi.organic.productcatalog.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.productcatalog.businessdelegate.context.SellerProductPricesContext;
import com.arthvedi.organic.productcatalog.domain.SellerProductPrices;
import com.arthvedi.organic.productcatalog.model.SellerProductPricesModel;
import com.arthvedi.organic.productcatalog.service.ISellerProductPricesService;

@Service
public class SellerProductPricesBusinessDelegate
		implements
		IBusinessDelegate<SellerProductPricesModel, SellerProductPricesContext, IKeyBuilder<String>, String> {

	@Autowired
	private ISellerProductPricesService sellerProductPricesService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerProductPricesModel create(SellerProductPricesModel model) {
		SellerProductPrices sellerProductPrices = sellerProductPricesService
				.create((SellerProductPrices) conversionService.convert(model,
						forObject(model), valueOf(SellerProductPrices.class)));
		model = convertToSellerProductPricesModel(sellerProductPrices);
		return model;
	}

	private SellerProductPricesModel convertToSellerProductPricesModel(
			SellerProductPrices sellerProductPrices) {
		return (SellerProductPricesModel) conversionService.convert(
				sellerProductPrices, forObject(sellerProductPrices),
				valueOf(SellerProductPricesModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			SellerProductPricesContext context) {

	}

	@Override
	public SellerProductPricesModel edit(IKeyBuilder<String> keyBuilder,
			SellerProductPricesModel model) {
		SellerProductPrices sellerProductPrices = sellerProductPricesService
				.getSellerProductPrices(keyBuilder.build().toString());

		sellerProductPrices = sellerProductPricesService
				.updateSellerProductPrices((SellerProductPrices) conversionService
						.convert(model, forObject(model),
								valueOf(SellerProductPrices.class)));
		model = convertToSellerProductPricesModel(sellerProductPrices);
		return model;
	}

	@Override
	public SellerProductPricesModel getByKey(IKeyBuilder<String> keyBuilder,
			SellerProductPricesContext context) {
		SellerProductPrices sellerProductPrices = sellerProductPricesService
				.getSellerProductPrices(keyBuilder.build().toString());
		SellerProductPricesModel model = conversionService.convert(
				sellerProductPrices, SellerProductPricesModel.class);
		return model;
	}

	@Override
	public Collection<SellerProductPricesModel> getCollection(
			SellerProductPricesContext context) {
		List<SellerProductPrices> sellerProductPrices = new ArrayList<SellerProductPrices>();
		if (context.getAll() != null) {
			sellerProductPrices = sellerProductPricesService.getAll();
		}
		List<SellerProductPricesModel> sellerProductPricesModels = (List<SellerProductPricesModel>) conversionService
				.convert(sellerProductPrices, TypeDescriptor
						.forObject(sellerProductPrices), TypeDescriptor
						.collection(List.class, TypeDescriptor
								.valueOf(SellerProductPricesModel.class)));
		return sellerProductPricesModels;
	}

	@Override
	public SellerProductPricesModel edit(IKeyBuilder<String> keyBuilder,
			SellerProductPricesModel model, SellerProductPricesContext context) {
		return null;
	}

}
