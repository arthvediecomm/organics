package com.arthvedi.organic.productcatalog.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;
/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CategoryImagesContext implements IBusinessDelegateContext {

	private String categoryImagesId;
	private String all;
	
	public String getCategoryImagesId() {
		return categoryImagesId;
	}

	public void setCategoryImagesId(String categoryImagesId) {
		this.categoryImagesId = categoryImagesId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

}
