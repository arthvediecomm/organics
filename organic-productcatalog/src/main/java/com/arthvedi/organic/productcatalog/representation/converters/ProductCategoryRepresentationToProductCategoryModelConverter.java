/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.productcatalog.model.ProductCategoryModel;
import com.arthvedi.organic.productcatalog.representation.siren.ProductCategoryRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("productCategoryRepresentationToProductCategoryModelConverter")
public class ProductCategoryRepresentationToProductCategoryModelConverter extends PropertyCopyingConverter<ProductCategoryRepresentation, ProductCategoryModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public ProductCategoryModel convert(final ProductCategoryRepresentation source) {

        ProductCategoryModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<ProductCategoryModel> factory) {
        super.setFactory(factory);
    }

}
