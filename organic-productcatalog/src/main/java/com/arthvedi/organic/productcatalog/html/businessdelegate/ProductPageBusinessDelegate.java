package com.arthvedi.organic.productcatalog.html.businessdelegate;
/*package com.arthvedi.handcraft.productcatalog.html.businessdelegate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.handcraft.commons.domain.Language;
import com.arthvedi.handcraft.commons.service.ILanguageService;
import com.arthvedi.handcraft.html.model.ProductPageModel;
import com.arthvedi.handcraft.productcatalog.domain.Product;
import com.arthvedi.handcraft.productcatalog.domain.ProductNames;
import com.arthvedi.handcraft.productcatalog.domain.SellerProduct;
import com.arthvedi.handcraft.productcatalog.html.context.ProductPageContext;
import com.arthvedi.handcraft.productcatalog.model.ProductModel;
import com.arthvedi.handcraft.productcatalog.model.SellerProductModel;
import com.arthvedi.handcraft.productcatalog.service.IProductNamesService;
import com.arthvedi.handcraft.productcatalog.service.IProductService;
import com.arthvedi.handcraft.productcatalog.service.ISellerProductService;

@Service
public class ProductPageBusinessDelegate
		implements IBusinessDelegate<ProductPageModel, ProductPageContext, IKeyBuilder<String>, String> {

	private ObjectFactory<ProductPageModel> productPageModelFactory;
	@Value("${imageUrl}")
	private String imageBaseUrl;
	@Autowired
	private IProductService productService;
	@Autowired
	private ILanguageService languageService;
	@Autowired
	private IProductNamesService productNamesService;

	@Autowired
	private ISellerProductService sellerProductService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public ProductPageModel create(ProductPageModel model) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, ProductPageContext context) {
		// TODO Auto-generated method stub

	}

	@Override
	public ProductPageModel edit(IKeyBuilder<String> keyBuilder, ProductPageModel model) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProductPageModel getByKey(IKeyBuilder<String> keyBuilder, ProductPageContext context) {
		ProductPageModel model = new ProductPageModel();
		String zoneId = context.getZoneId();
		String langId = context.getLanguageId();
		// getting default language if language is null
		if (zoneId != null && langId == null) {
			Language language = languageService.getCityDefaultLanguageByZoneId(zoneId);
			langId = language.getId();
		}

		Product product = productService.getProduct(keyBuilder.build().toString());
		if (product != null) {
			ProductModel productModel = conversionService.convert(product, ProductModel.class);
			if (langId != null) {
				ProductNames productName = productNamesService.getProductNameByProductLanguage(product.getId(), langId);
				productModel.setProductNativeName(productName.getName());
			}
			if (CollectionUtils.isNotEmpty(product.getProductImageses())) {
				List<ProductImages> imageses = new ArrayList<ProductImages>(product.getProductImageses());
				Map<String, ProductImages> productImages = imageses.stream()
						.collect(Collectors.toMap(x -> x.getImageType().toString(), x -> x));
				productModel.setProductImageUrl(imageBaseUrl + productImages.get("MAINIMAGE").getImageFolder() + '/'
						+ productImages.get("MAINIMAGE").getImageName());

			}
			if (zoneId != null) {
				List<SellerProduct> sellerProduct = sellerProductService.getSellerProductByZoneAndStatus(zoneId,
						"ACTIVE");

				if (CollectionUtils.isNotEmpty(sellerProduct)) {
					List<SellerProductModel> sellerProductModels = new ArrayList<SellerProductModel>();
					for (SellerProduct selProduct : sellerProduct) {
						SellerProductModel sellerProductModel = conversionService.convert(selProduct,
								SellerProductModel.class);
						sellerProductModels.add(sellerProductModel);
					}

				}

			}
			model.setProductModel(productModel);
		}

		return model;
	}

	@Override
	public Collection<ProductPageModel> getCollection(ProductPageContext context) {
		// TODO Auto-generated method stub
		return null;
	}

}
*/