/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.productcatalog.model.SeoModel;
import com.arthvedi.organic.productcatalog.representation.siren.SeoRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("seoRepresentationToSeoModelConverter")
public class SeoRepresentationToSeoModelConverter extends PropertyCopyingConverter<SeoRepresentation, SeoModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public SeoModel convert(final SeoRepresentation source) {

        SeoModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<SeoModel> factory) {
        super.setFactory(factory);
    }

}
