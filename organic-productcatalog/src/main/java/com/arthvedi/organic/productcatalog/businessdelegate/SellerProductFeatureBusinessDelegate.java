package com.arthvedi.organic.productcatalog.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.productcatalog.businessdelegate.context.SellerProductFeatureContext;
import com.arthvedi.organic.productcatalog.domain.SellerProductFeature;
import com.arthvedi.organic.productcatalog.model.SellerProductFeatureModel;
import com.arthvedi.organic.productcatalog.service.ISellerProductFeatureService;

@Service
public class SellerProductFeatureBusinessDelegate implements
		IBusinessDelegate<SellerProductFeatureModel, SellerProductFeatureContext, IKeyBuilder<String>, String> {

	@Autowired
	private ISellerProductFeatureService sellerProductFeatureService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerProductFeatureModel create(SellerProductFeatureModel model) {
		validateModel(model);
		SellerProductFeature sellerProductFeature = sellerProductFeatureService.create(
				(SellerProductFeature) conversionService.convert(model, forObject(model), valueOf(SellerProductFeature.class)));

		model = convertToSellerProductFeatureModel(sellerProductFeature);
		model.setStatusMessage("SUCCESS:::  SellerProduct Feature Created Successfully");
		return model;
	}

	private void validateModel(SellerProductFeatureModel model) {
		Validate.notNull(model, "invalid input");		
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, SellerProductFeatureContext context) {

	}

	@Override
	public SellerProductFeatureModel edit(IKeyBuilder<String> keyBuilder, SellerProductFeatureModel model) {
		SellerProductFeature sellerProductFeature = sellerProductFeatureService
				.getSellerProductFeature(keyBuilder.build().toString());
		sellerProductFeature = sellerProductFeatureService
				.updateSellerProductFeature((SellerProductFeature) conversionService.convert(model, forObject(model),
						valueOf(SellerProductFeature.class)));
		model = convertToSellerProductFeatureModel(sellerProductFeature);
		model.setStatusMessage("SUCCESS:::  SellerProductFeature updated successfully");
		return model;
	}

	private SellerProductFeatureModel convertToSellerProductFeatureModel(SellerProductFeature sellerProductFeature) {
		return (SellerProductFeatureModel) conversionService.convert(sellerProductFeature,
				forObject(sellerProductFeature), valueOf(SellerProductFeatureModel.class));
	}

	@Override
	public SellerProductFeatureModel getByKey(IKeyBuilder<String> keyBuilder, SellerProductFeatureContext context) {
		SellerProductFeature sellerProductFeature = sellerProductFeatureService
				.getSellerProductFeature(keyBuilder.build().toString());
		SellerProductFeatureModel model = conversionService.convert(sellerProductFeature,
				SellerProductFeatureModel.class);
		return model;
	}

	@Override
	public Collection<SellerProductFeatureModel> getCollection(SellerProductFeatureContext context) {
		List<SellerProductFeature> sellerProductFeature = new ArrayList<SellerProductFeature>();
		if (context.getSellerProductId() != null) {
			sellerProductFeature = sellerProductFeatureService
					.getSellerProductFeatureBySellerProduct(context.getSellerProductId());
		}
		List<SellerProductFeatureModel> sellerProductFeatureModels = (List<SellerProductFeatureModel>) conversionService
				.convert(sellerProductFeature, TypeDescriptor.forObject(sellerProductFeature),
						TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(SellerProductFeatureModel.class)));
		return sellerProductFeatureModels;
	}

	@Override
	public SellerProductFeatureModel edit(IKeyBuilder<String> keyBuilder, SellerProductFeatureModel model,
			SellerProductFeatureContext context) {
		throw new UnsupportedOperationException("Operation not implemented yet");
	}

}
