package com.arthvedi.organic.productcatalog.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.productcatalog.businessdelegate.context.ComboProductContext;
import com.arthvedi.organic.productcatalog.domain.ComboProduct;
import com.arthvedi.organic.productcatalog.model.ComboProductModel;
import com.arthvedi.organic.productcatalog.service.IComboProductService;

/*
*@Author varma
*/

@Service
public class ComboProductBusinessDelegate
		implements IBusinessDelegate<ComboProductModel, ComboProductContext, IKeyBuilder<String>, String> {

	@Autowired
	private IComboProductService comboProductService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public ComboProductModel create(ComboProductModel model) {
		ComboProduct comboProduct = comboProductService
				.create((ComboProduct) conversionService.convert(model, forObject(model), valueOf(ComboProduct.class)));
		model = convertToComboProductModel(comboProduct);

		return model;
	}

	private ComboProductModel convertToComboProductModel(ComboProduct comboProduct) {

		return (ComboProductModel) conversionService.convert(comboProduct, forObject(comboProduct),
				valueOf(ComboProductModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, ComboProductContext context) {

	}

	@Override
	public ComboProductModel edit(IKeyBuilder<String> keyBuilder, ComboProductModel model) {
		ComboProduct comboProduct = comboProductService.getComboProduct(keyBuilder.build().toString());

		comboProduct = comboProductService.updateComboProduct(
				(ComboProduct) conversionService.convert(model, forObject(model), valueOf(ComboProduct.class)));
		model = convertToComboProductModel(comboProduct);
		return model;
	}

	@Override
	public ComboProductModel getByKey(IKeyBuilder<String> keyBuilder, ComboProductContext context) {
		ComboProduct comboProduct = comboProductService.getComboProduct(keyBuilder.build().toString());
		ComboProductModel model = conversionService.convert(comboProduct, ComboProductModel.class);
		return model;
	}

	@Override
	public Collection<ComboProductModel> getCollection(ComboProductContext context) {
		List<ComboProduct> comboProduct = new ArrayList<ComboProduct>();
		if (context.getAll() != null) {
			comboProduct = comboProductService.getAll();
		}
		if(context.getComboProductList()!=null){
			comboProduct=comboProductService.getComboProductList();
		}
		List<ComboProductModel> comboProductModels = (List<ComboProductModel>) conversionService.convert(comboProduct,
				TypeDescriptor.forObject(comboProduct),
				TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(ComboProductModel.class)));
		return comboProductModels;
	}

	@Override
	public ComboProductModel edit(IKeyBuilder<String> keyBuilder,
			ComboProductModel model, ComboProductContext context) {
		return null;
	}

}
