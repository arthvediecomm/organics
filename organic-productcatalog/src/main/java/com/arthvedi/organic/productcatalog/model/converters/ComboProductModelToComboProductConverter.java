/**
 *
 */
package com.arthvedi.organic.productcatalog.model.converters;

import java.math.BigDecimal;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.productcatalog.domain.ComboProduct;
import com.arthvedi.organic.productcatalog.domain.Product;
import com.arthvedi.organic.productcatalog.model.ComboProductModel;

/**
 * @author Jay
 *
 */
@Component("comboProductModelToComboProductConverter")
public class ComboProductModelToComboProductConverter implements
		Converter<ComboProductModel, ComboProduct> {
	@Autowired
	private ObjectFactory<ComboProduct> comboProductFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public ComboProduct convert(final ComboProductModel source) {
		ComboProduct comboProduct = new ComboProduct();
		BeanUtils.copyProperties(source, comboProduct);
		if (source.getProductByChildProductId() != null) {
			Product childProduct = new Product();
			childProduct.setId(source.getProductByChildProductId());
			comboProduct.setProductByChildProductId(childProduct);
		}
		if (source.getProductByParentProductId() != null) {
			Product parentProduct = new Product();
			parentProduct.setId(source.getProductByParentProductId());
			comboProduct.setProductByParentProductId(parentProduct);
		}
		if (source.getPrice() != null) {
			BigDecimal price = new BigDecimal(source.getPrice());
			comboProduct.setPrice(price);

		}
		return comboProduct;
	}

	@Autowired
	public void setComboProductFactory(
			final ObjectFactory<ComboProduct> comboProductFactory) {
		this.comboProductFactory = comboProductFactory;
	}

}
