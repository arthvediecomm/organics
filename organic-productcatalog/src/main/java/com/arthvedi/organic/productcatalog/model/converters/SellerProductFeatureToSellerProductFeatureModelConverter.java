package com.arthvedi.organic.productcatalog.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.productcatalog.domain.SellerProductFeature;
import com.arthvedi.organic.productcatalog.model.SellerProductFeatureModel;

@Component("sellerProductFeatureToSellerProductFeatureModelConverter")
public class SellerProductFeatureToSellerProductFeatureModelConverter
		implements Converter<SellerProductFeature, SellerProductFeatureModel> {
	@Autowired
	private ObjectFactory<SellerProductFeatureModel> sellerProductFeatureModelFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerProductFeatureModel convert(final SellerProductFeature source) {
		SellerProductFeatureModel sellerProductFeatureModel = sellerProductFeatureModelFactory.getObject();
		BeanUtils.copyProperties(source, sellerProductFeatureModel);
		if (source.getFeature() != null) {
			sellerProductFeatureModel.setFeatureId(source.getFeature().getId());
			sellerProductFeatureModel.setFeatureName(source.getFeature().getFeatureName());
			//sellerProductFeatureModel.setFeatureValue(source.getFeature().getFeatureValue());
			
		}
		if(source.getSellerProduct()!=null){
			sellerProductFeatureModel.setSellerProductId(source.getSellerProduct().getId());
			sellerProductFeatureModel.setSellerProductName(source.getSellerProduct().getSellerProductName());
		}
		return sellerProductFeatureModel;
	}

	@Autowired
	public void setSellerProductFeatureModelFactory(
			final ObjectFactory<SellerProductFeatureModel> sellerProductFeatureModelFactory) {
		this.sellerProductFeatureModelFactory = sellerProductFeatureModelFactory;
	}
}
