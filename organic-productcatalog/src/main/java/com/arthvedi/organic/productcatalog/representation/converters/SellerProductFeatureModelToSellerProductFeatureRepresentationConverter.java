package com.arthvedi.organic.productcatalog.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.productcatalog.model.SellerProductFeatureModel;
import com.arthvedi.organic.productcatalog.representation.siren.SellerProductFeatureRepresentation;

@Component("sellerProductFeatureModelToSellerProductFeatureRepresentationConverter")
public class SellerProductFeatureModelToSellerProductFeatureRepresentationConverter
		extends PropertyCopyingConverter<SellerProductFeatureModel, SellerProductFeatureRepresentation> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerProductFeatureRepresentation convert(final SellerProductFeatureModel source) {

		SellerProductFeatureRepresentation target = super.convert(source);

		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(final ObjectFactory<SellerProductFeatureRepresentation> factory) {
		super.setFactory(factory);
	}
}
