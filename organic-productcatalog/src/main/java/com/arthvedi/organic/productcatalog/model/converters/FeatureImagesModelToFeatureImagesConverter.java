/**
 *
 */
package com.arthvedi.organic.productcatalog.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.productcatalog.domain.FeatureImages;
import com.arthvedi.organic.productcatalog.model.FeatureImagesModel;

/**
 * @author Jay
 *
 */
@Component("featureImagesModelToFeatureImagesConverter")
public class FeatureImagesModelToFeatureImagesConverter implements Converter<FeatureImagesModel, FeatureImages> {
    @Autowired
    private ObjectFactory<FeatureImages> featureImagesFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public FeatureImages convert(final FeatureImagesModel source) {
        FeatureImages featureImages = featureImagesFactory.getObject();
        BeanUtils.copyProperties(source, featureImages);

        return featureImages;
    }

    @Autowired
    public void setFeatureImagesFactory(final ObjectFactory<FeatureImages> featureImagesFactory) {
        this.featureImagesFactory = featureImagesFactory;
    }

}
