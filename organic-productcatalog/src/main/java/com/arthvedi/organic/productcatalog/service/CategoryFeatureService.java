package com.arthvedi.organic.productcatalog.service;

import static com.arthvedi.organic.enums.Status.ACTIVE;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.productcatalog.domain.CategoryFeature;
import com.arthvedi.organic.productcatalog.respository.CategoryFeatureRepository;

/*
 *@Author varma
 */
@Component
public class CategoryFeatureService implements ICategoryFeatureService {
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private CategoryFeatureRepository categoryFeatureRepository;

	@Override
	public CategoryFeature create(CategoryFeature categoryFeature) {
		categoryFeature.setCreatedDate(new LocalDateTime());
		categoryFeature.setUserCreated("varma");
		categoryFeature.setStatus(ACTIVE.name());
		categoryFeature = categoryFeatureRepository.save(categoryFeature);
		return categoryFeature;
	}

	@Override
	public void deleteCategoryFeature(String categoryFeatureId) {

	}

	@Override
	public CategoryFeature getCategoryFeature(String categoryFeatureId) {
		return categoryFeatureRepository.findOne(categoryFeatureId);
	}

	@Override
	public List<CategoryFeature> getAll() {
		return null;
	}

	@Override
	public CategoryFeature updateCategoryFeature(CategoryFeature categoryFeature) {
		CategoryFeature categoryFeatures = categoryFeatureRepository
				.findOne(categoryFeature.getId());
		try {
			nonNullBeanUtils.copyProperties(categoryFeatures, categoryFeature);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		categoryFeatures.setUserModified("admin");
		categoryFeatures.setModifiedDate(new LocalDateTime());
		categoryFeature = categoryFeatureRepository.save(categoryFeatures);
		return categoryFeature;
	}

	@Override
	public List<CategoryFeature> getCategoryFeatureList() {
		List<CategoryFeature> categoryFeatures = (List<CategoryFeature>) categoryFeatureRepository
				.findAll();
		List<CategoryFeature> categoryFeaturess = new ArrayList<CategoryFeature>();
		for (CategoryFeature catfea : categoryFeatures) {
			CategoryFeature categoryFeature = new CategoryFeature();
			// categoryFeature.set
			categoryFeaturess.add(categoryFeature);

		}

		return categoryFeaturess;
	}

}
