package com.arthvedi.organic.productcatalog.service;

import java.util.List;

import com.arthvedi.organic.productcatalog.domain.CategoryImages;
/*
*@Author varma
*/
public interface ICategoryImagesService {
	
	CategoryImages create(CategoryImages categoryImages);

	void deleteCategoryImages(String categoryImagesId);

	CategoryImages getCategoryImages(String categoryImagesId);

	List<CategoryImages> getAll();

	CategoryImages updateCategoryImages(CategoryImages categoryImages);
}
