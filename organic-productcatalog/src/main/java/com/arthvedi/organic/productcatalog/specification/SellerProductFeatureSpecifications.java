package com.arthvedi.organic.productcatalog.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.arthvedi.organic.productcatalog.domain.SellerProductFeature;

public class SellerProductFeatureSpecifications {
	public static Specification<SellerProductFeature> bySellerProduct(String sellerProductId) {
		return new Specification<SellerProductFeature>() {

			@Override
			public Predicate toPredicate(Root<SellerProductFeature> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.get("sellerProduct").get("id"), sellerProductId);
			}
		};
	}
}
