package com.arthvedi.organic.productcatalog.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.arthvedi.organic.productcatalog.domain.Product;

public class ProductSpecification {

	public static Specification<Product> byCategory(String categoryId) {
		return new Specification<Product>() {
			@Override
			public Predicate toPredicate(Root<Product> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.join("productCategories").get("category")
						.get("id"), categoryId);
			}
		};
	}

	public static Specification<Product> byStatus(String status) {
		return new Specification<Product>() {
			@Override
			public Predicate toPredicate(Root<Product> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.get("status"), status);
			}

		};
	}

	public static Specification<Product> byProductName(String productName) {
		return new Specification<Product>() {
			@Override
			public Predicate toPredicate(Root<Product> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.get("productName"), productName);

				// cb.or(cb.equal(root.get("productName"), productName),
				// cb.equal(
				// root.join("productNameses").get("name"), productName));

			}
		};
	}

}
