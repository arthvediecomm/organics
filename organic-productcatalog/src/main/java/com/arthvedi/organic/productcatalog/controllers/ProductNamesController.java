package com.arthvedi.organic.productcatalog.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.productcatalog.businessdelegate.context.ProductNamesContext;
import com.arthvedi.organic.productcatalog.model.ProductNamesModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/productNames", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class ProductNamesController {

	private IBusinessDelegate<ProductNamesModel, ProductNamesContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<ProductNamesContext> productNamesContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<ProductNamesModel> createProductNames(@RequestBody final ProductNamesModel productNamesModel) {
		businessDelegate.create(productNamesModel);
		return new ResponseEntity<ProductNamesModel>(productNamesModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<ProductNamesModel> edit(@PathVariable(value = "id") final String productNamesId,
			@RequestBody final ProductNamesModel productNamesModel) {

		businessDelegate.edit(keyBuilderFactory.getObject().withId(productNamesId), productNamesModel);
		return new ResponseEntity<ProductNamesModel>(productNamesModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<ProductNamesModel>>> getAll() {
		ProductNamesContext productNamesContext = productNamesContextFactory.getObject();
		productNamesContext.setAll("all");
		Collection<ProductNamesModel> productNamesModels = businessDelegate.getCollection(productNamesContext);
		IModelWrapper<Collection<ProductNamesModel>> models = new CollectionModelWrapper<ProductNamesModel>(
				ProductNamesModel.class, productNamesModels);
		return new ResponseEntity<IModelWrapper<Collection<ProductNamesModel>>>(models, HttpStatus.OK);
	}
	@RequestMapping(method = RequestMethod.GET, value = "/productnameslist", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<ProductNamesModel>>> getProductNamesList() {
		ProductNamesContext productNamesContext = productNamesContextFactory.getObject();
		productNamesContext.setProductNamesList("productNamesList");
		Collection<ProductNamesModel> productNamesModels = businessDelegate.getCollection(productNamesContext);
		IModelWrapper<Collection<ProductNamesModel>> models = new CollectionModelWrapper<ProductNamesModel>(
				ProductNamesModel.class, productNamesModels);
		return new ResponseEntity<IModelWrapper<Collection<ProductNamesModel>>>(models, HttpStatus.OK);
	}
	@RequestMapping(method = RequestMethod.GET, value = "/productnamesByCategory/{CategoryId}", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<IModelWrapper<Collection<ProductNamesModel>>> getByCategoryId() {
		ProductNamesContext productNamesContext = productNamesContextFactory.getObject();
		productNamesContext.setCategoryId("categoryId");
		Collection<ProductNamesModel> productNamesModels = businessDelegate.getCollection(productNamesContext);
		IModelWrapper<Collection<ProductNamesModel>> models = new CollectionModelWrapper<ProductNamesModel>(
				ProductNamesModel.class, productNamesModels);
		return new ResponseEntity<IModelWrapper<Collection<ProductNamesModel>>>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<ProductNamesModel> getProductNames(@PathVariable(value = "id") final String productNamesId) {
		ProductNamesContext productNamesContext = productNamesContextFactory.getObject();

		ProductNamesModel model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(productNamesId),
				productNamesContext);
		return new ResponseEntity<ProductNamesModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "productNamesBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<ProductNamesModel, ProductNamesContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setProductNamesObjectFactory(final ObjectFactory<ProductNamesContext> productNamesContextFactory) {
		this.productNamesContextFactory = productNamesContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
