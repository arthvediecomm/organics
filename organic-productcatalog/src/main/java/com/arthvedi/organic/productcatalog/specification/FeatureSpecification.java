package com.arthvedi.organic.productcatalog.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.arthvedi.organic.productcatalog.domain.Feature;

public class FeatureSpecification {

	public static Specification<Feature> byCategory(final String categoryId) {
		return new Specification<Feature>() {
			@Override
			public Predicate toPredicate(Root<Feature> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.join("categoryFeatures").get("category")
						.get("id"), categoryId);
			}

		};

	}

	public static Specification<Feature> byStatus(String status) {
		return new Specification<Feature>() {

			@Override
			public Predicate toPredicate(Root<Feature> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.get("status"), status);
			}
		};

	}

}
