package com.arthvedi.organic.productcatalog.specification;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.arthvedi.organic.productcatalog.domain.Feature;

public class FeatureSpecifications {

	public static Specification<Feature> byCategory(String categoryId) {
		return new Specification<Feature>() {
			@Override
			public Predicate toPredicate(Root<Feature> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.join("categoryFeatures").get("category").get("id"), categoryId);
			}

		};
	}
	
	public static Specification<Feature> byProduct(String productId) {
		return new Specification<Feature>() {
			@Override
			public Predicate toPredicate(Root<Feature> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.join("productFeatures").get("product").get("id"), productId);
			}

		};
	}
	public static Specification<Feature> byFeatureIdsList(List<String> featureIds){
		return new Specification<Feature>(){

			@Override
			public Predicate toPredicate(Root<Feature> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.in(root.get("id")).value(featureIds);
			}
			
		};
	}
}
