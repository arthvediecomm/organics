package com.arthvedi.organic.productcatalog.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.productcatalog.domain.Feature;
import com.arthvedi.organic.productcatalog.model.FeatureModel;

@Component("featureToFeatureModelConverter")
public class FeatureToFeatureModelConverter
        implements Converter<Feature, FeatureModel> {
    @Autowired
    private ObjectFactory<FeatureModel> featureModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public FeatureModel convert(final Feature source) {
        FeatureModel featureModel = featureModelFactory.getObject();
        BeanUtils.copyProperties(source, featureModel);

        return featureModel;
    }

    @Autowired
    public void setFeatureModelFactory(
            final ObjectFactory<FeatureModel> featureModelFactory) {
        this.featureModelFactory = featureModelFactory;
    }
}
