package com.arthvedi.organic.productcatalog.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.productcatalog.domain.ProductNames;
import com.arthvedi.organic.productcatalog.model.ProductNamesModel;

@Component("productNamesToProductNamesModelConverter")
public class ProductNamesToProductNamesModelConverter
        implements Converter<ProductNames, ProductNamesModel> {
    @Autowired
    private ObjectFactory<ProductNamesModel> productNamesModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public ProductNamesModel convert(final ProductNames source) {
        ProductNamesModel productNamesModel = productNamesModelFactory.getObject();
        BeanUtils.copyProperties(source, productNamesModel);

        return productNamesModel;
    }

    @Autowired
    public void setProductNamesModelFactory(
            final ObjectFactory<ProductNamesModel> productNamesModelFactory) {
        this.productNamesModelFactory = productNamesModelFactory;
    }
}
