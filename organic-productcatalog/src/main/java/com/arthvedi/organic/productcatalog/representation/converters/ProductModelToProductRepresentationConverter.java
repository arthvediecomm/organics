/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.productcatalog.model.ProductModel;
import com.arthvedi.organic.productcatalog.representation.siren.ProductFeatureRepresentation;
import com.arthvedi.organic.productcatalog.representation.siren.ProductImagesRepresentation;
import com.arthvedi.organic.productcatalog.representation.siren.ProductNamesRepresentation;
import com.arthvedi.organic.productcatalog.representation.siren.ProductRepresentation;

/**
 * @author varma
 *
 */
@Component("productModelToProductRepresentationConverter")
public class ProductModelToProductRepresentationConverter extends
		PropertyCopyingConverter<ProductModel, ProductRepresentation> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public ProductRepresentation convert(final ProductModel source) {
		ProductRepresentation target = super.convert(source);
		if (CollectionUtils.isNotEmpty(source.getProductNamesesModel())) {
			List<ProductNamesRepresentation> convert = (List<ProductNamesRepresentation>) conversionService
					.convert(source.getProductNamesesModel(), TypeDescriptor
							.forObject(source.getProductNamesesModel()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									ProductNamesRepresentation.class));
			target.getProductNamesesRepresentation().addAll(convert);
		}
		if (CollectionUtils.isNotEmpty(source.getProductImagesesModel())) {
			List<ProductImagesRepresentation> convert = (List<ProductImagesRepresentation>) conversionService
					.convert(source.getProductImagesesModel(), TypeDescriptor
							.forObject(source.getProductImagesesModel()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									ProductImagesRepresentation.class));
			target.getProductImagesesRepresentation().addAll(convert);
		}
		if (CollectionUtils.isNotEmpty(source.getProductFeaturesModel())) {
			List<ProductFeatureRepresentation> convert = (List<ProductFeatureRepresentation>) conversionService
					.convert(source.getProductFeaturesModel(), TypeDescriptor
							.forObject(source.getProductFeaturesModel()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									ProductFeatureRepresentation.class));
			target.getProductFeaturesRepresentation().addAll(convert);
		}
		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(final ObjectFactory<ProductRepresentation> factory) {
		super.setFactory(factory);
	}

}
