/**
 *
 */
package com.arthvedi.organic.productcatalog.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.productcatalog.domain.Brand;
import com.arthvedi.organic.productcatalog.domain.BrandImages;
import com.arthvedi.organic.productcatalog.model.BrandImagesModel;

@Component("brandImagesModelToBrandImagesConverter")
public class BrandImagesModelToBrandImagesConverter implements
		Converter<BrandImagesModel, BrandImages> {
	@Autowired
	private ObjectFactory<BrandImages> brandImagesFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public BrandImages convert(final BrandImagesModel source) {
		BrandImages brandImages = new BrandImages();
		BeanUtils.copyProperties(source, brandImages);
		if (source.getBrandId() != null) {
			Brand brand = new Brand();
			brand.setId(source.getBrandId());
			brandImages.setBrand(brand);
		}
		return brandImages;
	}

	@Autowired
	public void setBrandImagesFactory(
			final ObjectFactory<BrandImages> brandImagesFactory) {
		this.brandImagesFactory = brandImagesFactory;
	}

}
