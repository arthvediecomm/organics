package com.arthvedi.organic.productcatalog.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.productcatalog.businessdelegate.context.ProductCategoryContext;
import com.arthvedi.organic.productcatalog.model.ProductCategoryModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/productcategory", produces = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class ProductCategoryController {

	private IBusinessDelegate<ProductCategoryModel, ProductCategoryContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<ProductCategoryContext> productCategoryContextFactory;

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<ProductCategoryModel> getProductCategory(
			@PathVariable(value = "id") final String productCategoryId) {
		ProductCategoryContext productCategoryContext = productCategoryContextFactory
				.getObject();

		ProductCategoryModel model = businessDelegate.getByKey(
				keyBuilderFactory.getObject().withId(productCategoryId),
				productCategoryContext);
		return new ResponseEntity<ProductCategoryModel>(model, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<ProductCategoryModel> createProductCategory(
			@RequestBody final ProductCategoryModel productCategoryModel) {
		businessDelegate.create(productCategoryModel);
		return new ResponseEntity<ProductCategoryModel>(productCategoryModel,
				HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<ProductCategoryModel> edit(
			@PathVariable(value = "id") final String productCategoryId,
			@RequestBody final ProductCategoryModel productCategoryModel) {

		businessDelegate.edit(
				keyBuilderFactory.getObject().withId(productCategoryId),
				productCategoryModel);
		return new ResponseEntity<ProductCategoryModel>(productCategoryModel,
				HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<ProductCategoryModel>>> getAll() {
		ProductCategoryContext productCategoryContext = productCategoryContextFactory
				.getObject();
		productCategoryContext.setAll("all");
		Collection<ProductCategoryModel> productCategoryModels = businessDelegate
				.getCollection(productCategoryContext);
		IModelWrapper<Collection<ProductCategoryModel>> models = new CollectionModelWrapper<ProductCategoryModel>(
				ProductCategoryModel.class, productCategoryModels);
		return new ResponseEntity<IModelWrapper<Collection<ProductCategoryModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/productcategorylist", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<ProductCategoryModel>>> getProductCategoryList() {
		ProductCategoryContext productCategoryContext = productCategoryContextFactory
				.getObject();
		productCategoryContext.setProductCategoryList("productCategoryList");
		Collection<ProductCategoryModel> productCategoryModels = businessDelegate
				.getCollection(productCategoryContext);
		IModelWrapper<Collection<ProductCategoryModel>> models = new CollectionModelWrapper<ProductCategoryModel>(
				ProductCategoryModel.class, productCategoryModels);
		return new ResponseEntity<IModelWrapper<Collection<ProductCategoryModel>>>(
				models, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "productCategoryBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<ProductCategoryModel, ProductCategoryContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setProductCategoryObjectFactory(
			final ObjectFactory<ProductCategoryContext> productCategoryContextFactory) {
		this.productCategoryContextFactory = productCategoryContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(
			final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
