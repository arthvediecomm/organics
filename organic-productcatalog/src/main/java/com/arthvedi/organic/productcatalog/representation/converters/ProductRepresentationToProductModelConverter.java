/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.productcatalog.model.ComboProductModel;
import com.arthvedi.organic.productcatalog.model.ProductCategoryModel;
import com.arthvedi.organic.productcatalog.model.ProductFeatureModel;
import com.arthvedi.organic.productcatalog.model.ProductImagesModel;
import com.arthvedi.organic.productcatalog.model.ProductModel;
import com.arthvedi.organic.productcatalog.model.ProductNamesModel;
import com.arthvedi.organic.productcatalog.representation.siren.ProductRepresentation;

/**
 * @author arthvedi
 *
 */

@Component("productRepresentationToProductModelConverter")
public class ProductRepresentationToProductModelConverter extends
		PropertyCopyingConverter<ProductRepresentation, ProductModel> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public ProductModel convert(final ProductRepresentation source) {

		ProductModel target = super.convert(source);

		if (CollectionUtils.isNotEmpty(source
				.getComboProductsForChildProductIdRepresentation())) {
			List<ComboProductModel> converted = (List<ComboProductModel>) conversionService
					.convert(
							source.getComboProductsForChildProductIdRepresentation(),
							TypeDescriptor.forObject(source
									.getComboProductsForChildProductIdRepresentation()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									ComboProductModel.class));
			target.getComboProductsForChildProductIdModel().addAll(converted);
		}
		if (CollectionUtils.isNotEmpty(source
				.getComboProductsForParentProductIdRepresentation())) {
			List<ComboProductModel> converted = (List<ComboProductModel>) conversionService
					.convert(
							source.getComboProductsForParentProductIdRepresentation(),
							TypeDescriptor.forObject(source
									.getComboProductsForParentProductIdRepresentation()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									ComboProductModel.class));
			target.getComboProductsForParentProductIdModel().addAll(converted);
		}
		if (CollectionUtils
				.isNotEmpty(source.getProductNamesesRepresentation())) {
			List<ProductNamesModel> convert = (List<ProductNamesModel>) conversionService
					.convert(source.getProductNamesesRepresentation(),
							TypeDescriptor.forObject(source
									.getProductNamesesRepresentation()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									ProductNamesModel.class));
			target.getProductNamesesModel().addAll(convert);
		}
		if (CollectionUtils.isNotEmpty(source
				.getProductImagesesRepresentation())) {
			List<ProductImagesModel> convert = (List<ProductImagesModel>) conversionService
					.convert(source.getProductImagesesRepresentation(),
							TypeDescriptor.forObject(source
									.getProductImagesesRepresentation()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									ProductImagesModel.class));
			target.getProductImagesesModel().addAll(convert);
		}
		if (CollectionUtils.isNotEmpty(source
				.getProductFeaturesRepresentation())) {
			List<ProductFeatureModel> convert = (List<ProductFeatureModel>) conversionService
					.convert(source.getProductFeaturesRepresentation(),
							TypeDescriptor.forObject(source
									.getProductFeaturesRepresentation()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									ProductFeatureModel.class));
			target.getProductFeaturesModel().addAll(convert);
		}
		if (CollectionUtils.isNotEmpty(source
				.getProductCategoriesRepresentation())) {
			List<ProductCategoryModel> convert = (List<ProductCategoryModel>) conversionService
					.convert(source.getProductCategoriesRepresentation(),
							TypeDescriptor.forObject(source
									.getProductCategoriesRepresentation()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									ProductCategoryModel.class));
			target.getProductCategoriesModel().addAll(convert);
		}
		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(final ObjectFactory<ProductModel> factory) {
		super.setFactory(factory);
	}
}
