package com.arthvedi.organic.productcatalog.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.productcatalog.businessdelegate.context.ProductFeatureContext;
import com.arthvedi.organic.productcatalog.domain.ProductFeature;
import com.arthvedi.organic.productcatalog.model.ProductFeatureModel;
import com.arthvedi.organic.productcatalog.service.IProductFeatureService;

/*
 *@Author varma
 */

@Service
public class ProductFeatureBusinessDelegate
		implements
		IBusinessDelegate<ProductFeatureModel, ProductFeatureContext, IKeyBuilder<String>, String> {

	@Autowired
	private IProductFeatureService productFeatureService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public ProductFeatureModel create(ProductFeatureModel model) {
		ProductFeature productFeature = productFeatureService
				.create((ProductFeature) conversionService.convert(model,
						forObject(model), valueOf(ProductFeature.class)));
		model = convertToProductFeatureModel(productFeature);
		return model;
	}

	private ProductFeatureModel convertToProductFeatureModel(
			ProductFeature productFeature) {
		return (ProductFeatureModel) conversionService.convert(productFeature,
				forObject(productFeature), valueOf(ProductFeatureModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			ProductFeatureContext context) {

	}

	@Override
	public ProductFeatureModel edit(IKeyBuilder<String> keyBuilder,
			ProductFeatureModel model) {
		ProductFeature productFeature = productFeatureService
				.getProductFeature(keyBuilder.build().toString());
		productFeature = productFeatureService
				.updateProductFeature((ProductFeature) conversionService
						.convert(model, forObject(model),
								valueOf(ProductFeature.class)));
		model = convertToProductFeatureModel(productFeature);
		return model;
	}

	@Override
	public ProductFeatureModel getByKey(IKeyBuilder<String> keyBuilder,
			ProductFeatureContext context) {
		ProductFeature productFeature = productFeatureService
				.getProductFeature(keyBuilder.build().toString());
		ProductFeatureModel model = conversionService.convert(productFeature,
				ProductFeatureModel.class);
		return model;
	}

	@Override
	public Collection<ProductFeatureModel> getCollection(
			ProductFeatureContext context) {
		List<ProductFeature> productFeature = new ArrayList<ProductFeature>();
		if (context.getAll() != null) {
			productFeature = productFeatureService.getAll();
		}
		if (context.getProductFeatureList() != null) {
			productFeature = productFeatureService.getProductFeatureList();
		}
		List<ProductFeatureModel> productFeatureModels = (List<ProductFeatureModel>) conversionService
				.convert(productFeature, TypeDescriptor
						.forObject(productFeature), TypeDescriptor.collection(
						List.class,
						TypeDescriptor.valueOf(ProductFeatureModel.class)));
		return productFeatureModels;
	}

	@Override
	public ProductFeatureModel edit(IKeyBuilder<String> keyBuilder,
			ProductFeatureModel model, ProductFeatureContext context) {
		return null;
	}

}
