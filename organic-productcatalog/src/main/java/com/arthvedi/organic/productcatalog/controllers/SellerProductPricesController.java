package com.arthvedi.organic.productcatalog.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.productcatalog.businessdelegate.context.SellerProductPricesContext;
import com.arthvedi.organic.productcatalog.model.SellerProductPricesModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/sellerproductprices", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class SellerProductPricesController {

	private IBusinessDelegate<SellerProductPricesModel, SellerProductPricesContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<SellerProductPricesContext> sellerProductPricesContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<SellerProductPricesModel> createSellerProductPrices(@RequestBody final SellerProductPricesModel sellerProductPricesModel) {
		businessDelegate.create(sellerProductPricesModel);
		return new ResponseEntity<SellerProductPricesModel>(sellerProductPricesModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<SellerProductPricesModel> edit(@PathVariable(value = "id") final String sellerProductPricesId,
			@RequestBody final SellerProductPricesModel sellerProductPricesModel) {

		businessDelegate.edit(keyBuilderFactory.getObject().withId(sellerProductPricesId), sellerProductPricesModel);
		return new ResponseEntity<SellerProductPricesModel>(sellerProductPricesModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<SellerProductPricesModel>>> getAll() {
		SellerProductPricesContext sellerProductPricesContext = sellerProductPricesContextFactory.getObject();
		sellerProductPricesContext.setAll("all");
		Collection<SellerProductPricesModel> sellerProductPricesModels = businessDelegate.getCollection(sellerProductPricesContext);
		IModelWrapper<Collection<SellerProductPricesModel>> models = new CollectionModelWrapper<SellerProductPricesModel>(
				SellerProductPricesModel.class, sellerProductPricesModels);
		return new ResponseEntity<IModelWrapper<Collection<SellerProductPricesModel>>>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<SellerProductPricesModel> getSellerProductPrices(@PathVariable(value = "id") final String sellerProductPricesId) {
		SellerProductPricesContext sellerProductPricesContext = sellerProductPricesContextFactory.getObject();

		SellerProductPricesModel model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(sellerProductPricesId),
				sellerProductPricesContext);
		return new ResponseEntity<SellerProductPricesModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "sellerProductPricesBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<SellerProductPricesModel, SellerProductPricesContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setSellerProductPricesObjectFactory(final ObjectFactory<SellerProductPricesContext> sellerProductPricesContextFactory) {
		this.sellerProductPricesContextFactory = sellerProductPricesContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
