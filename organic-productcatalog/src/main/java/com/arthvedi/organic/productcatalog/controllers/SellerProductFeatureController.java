package com.arthvedi.organic.productcatalog.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.productcatalog.businessdelegate.context.SellerProductFeatureContext;
import com.arthvedi.organic.productcatalog.model.SellerProductFeatureModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/sellerproductfeature", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class SellerProductFeatureController {

	private IBusinessDelegate<SellerProductFeatureModel, SellerProductFeatureContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<SellerProductFeatureContext> sellerProductFeatureContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create")
	public ResponseEntity<SellerProductFeatureModel> createSellerProductFeature(
			@RequestBody  SellerProductFeatureModel sellerProductFeatureModel) {
		sellerProductFeatureModel = businessDelegate.create(sellerProductFeatureModel);
		return new ResponseEntity<SellerProductFeatureModel>(sellerProductFeatureModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	public ResponseEntity<SellerProductFeatureModel> edit(
			@PathVariable(value = "id") final String sellerProductFeatureId,
			@RequestBody  SellerProductFeatureModel sellerProductFeatureModel) {
		sellerProductFeatureModel = businessDelegate.edit(keyBuilderFactory.getObject().withId(sellerProductFeatureId), sellerProductFeatureModel);
		return new ResponseEntity<SellerProductFeatureModel>(sellerProductFeatureModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all")
	public ResponseEntity<IModelWrapper<Collection<SellerProductFeatureModel>>> getAll() {
		SellerProductFeatureContext sellerProductFeatureContext = sellerProductFeatureContextFactory.getObject();
		sellerProductFeatureContext.setAll("all");
		Collection<SellerProductFeatureModel> sellerProductFeatureModels = businessDelegate
				.getCollection(sellerProductFeatureContext);
		IModelWrapper<Collection<SellerProductFeatureModel>> models = new CollectionModelWrapper<SellerProductFeatureModel>(
				SellerProductFeatureModel.class, sellerProductFeatureModels);
		return new ResponseEntity<IModelWrapper<Collection<SellerProductFeatureModel>>>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = {"/{sellerProductId}/sellerproductfeaturesbysellerproduct","/sellerapp/{sellerProductId}/sellerproductfeaturesbysellerproduct"})
	public ResponseEntity<IModelWrapper<Collection<SellerProductFeatureModel>>> getAll(@PathVariable(value="sellerProductId") final String sellerProductId) {
		SellerProductFeatureContext sellerProductFeatureContext = sellerProductFeatureContextFactory.getObject();
		sellerProductFeatureContext.setSellerProductId(sellerProductId);
		Collection<SellerProductFeatureModel> sellerProductFeatureModels = businessDelegate
				.getCollection(sellerProductFeatureContext);
		IModelWrapper<Collection<SellerProductFeatureModel>> models = new CollectionModelWrapper<SellerProductFeatureModel>(
				SellerProductFeatureModel.class, sellerProductFeatureModels);
		return new ResponseEntity<IModelWrapper<Collection<SellerProductFeatureModel>>>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<SellerProductFeatureModel> getSellerProductFeature(
			@PathVariable(value = "id") final String sellerProductFeatureId) {
		SellerProductFeatureContext sellerProductFeatureContext = sellerProductFeatureContextFactory.getObject();
		SellerProductFeatureModel model = businessDelegate
				.getByKey(keyBuilderFactory.getObject().withId(sellerProductFeatureId), sellerProductFeatureContext);
		return new ResponseEntity<SellerProductFeatureModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "sellerProductFeatureBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<SellerProductFeatureModel, SellerProductFeatureContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setSellerProductFeatureObjectFactory(
			final ObjectFactory<SellerProductFeatureContext> sellerProductFeatureContextFactory) {
		this.sellerProductFeatureContextFactory = sellerProductFeatureContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
