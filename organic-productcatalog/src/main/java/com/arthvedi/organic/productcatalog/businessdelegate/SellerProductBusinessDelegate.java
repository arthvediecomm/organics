package com.arthvedi.organic.productcatalog.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.productcatalog.businessdelegate.context.SellerProductContext;
import com.arthvedi.organic.productcatalog.domain.SellerProduct;
import com.arthvedi.organic.productcatalog.model.SellerProductModel;
import com.arthvedi.organic.productcatalog.service.ISellerProductService;

@Service
public class SellerProductBusinessDelegate
		implements
		IBusinessDelegate<SellerProductModel, SellerProductContext, IKeyBuilder<String>, String> {

	@Autowired
	private ISellerProductService sellerProductService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerProductModel create(SellerProductModel model) {
		SellerProduct sellerProduct = sellerProductService
				.create((SellerProduct) conversionService.convert(model,
						forObject(model), valueOf(SellerProduct.class)));
		model = convertToSellerProductModel(sellerProduct);
		return model;
	}

	private SellerProductModel convertToSellerProductModel(
			SellerProduct sellerProduct) {
		return (SellerProductModel) conversionService.convert(sellerProduct,
				forObject(sellerProduct), valueOf(SellerProductModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			SellerProductContext context) {

	}

	@Override
	public SellerProductModel edit(IKeyBuilder<String> keyBuilder,
			SellerProductModel model) {
		SellerProduct sellerProduct = sellerProductService
				.getSellerProduct(keyBuilder.build().toString());

		sellerProduct = sellerProductService
				.updateSellerProduct((SellerProduct) conversionService.convert(
						model, forObject(model), valueOf(SellerProduct.class)));
		model = convertToSellerProductModel(sellerProduct);
		return model;
	}

	@Override
	public SellerProductModel getByKey(IKeyBuilder<String> keyBuilder,
			SellerProductContext context) {
		SellerProduct sellerProduct = sellerProductService
				.getSellerProduct(keyBuilder.build().toString());
		SellerProductModel model = conversionService.convert(sellerProduct,
				SellerProductModel.class);
		return model;
	}

	@Override
	public Collection<SellerProductModel> getCollection(
			SellerProductContext context) {
		List<SellerProduct> sellerProduct = new ArrayList<SellerProduct>();
		if (context.getSellerProductList() != null) {
			sellerProduct = sellerProductService.getSellerProductList();
		}
		if (context.getProductId() != null) {
			sellerProduct = sellerProductService
					.getSellerProductByProduct(context.getProductId());
		}
		if (context.getStatus() != null) {
			sellerProduct = sellerProductService.getByStatus(context
					.getStatus());
		}
		List<SellerProductModel> sellerProductModels = (List<SellerProductModel>) conversionService
				.convert(sellerProduct,
						TypeDescriptor.forObject(sellerProduct), TypeDescriptor
								.collection(List.class, TypeDescriptor
										.valueOf(SellerProductModel.class)));
		return sellerProductModels;
	}

	@Override
	public SellerProductModel edit(IKeyBuilder<String> keyBuilder,
			SellerProductModel model, SellerProductContext context) {
		return null;
	}

}
