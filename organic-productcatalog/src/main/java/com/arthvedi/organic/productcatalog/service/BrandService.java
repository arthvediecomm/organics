package com.arthvedi.organic.productcatalog.service;

import static com.arthvedi.organic.enums.Status.ACTIVE;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.productcatalog.domain.Brand;
import com.arthvedi.organic.productcatalog.domain.BrandImages;
import com.arthvedi.organic.productcatalog.respository.BrandRepository;

/*
 *@Author varma
 */
@Component
public class BrandService implements IBrandService {
	@Autowired
	NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private BrandRepository brandRepository;
	@Autowired
	private BrandImagesService brandImagesService;

	@Override
	public Brand create(Brand brand) {
		brand.setCreatedDate(new LocalDateTime());
		brand.setUserCreated("varma");
		brand.setStatus(ACTIVE.name());
		brand = brandRepository.save(brand);
		if (brand.getId() != null
				&& CollectionUtils.isNotEmpty(brand.getBrandImageses())) {
			brand.setBrandImageses(addBrandImages(brand,
					brand.getBrandImageses()));
		}
		return brand;
	}

	@Override
	public void deleteBrand(String brandId) {

	}

	@Override
	public Brand getBrand(String brandId) {
		return brandRepository.findOne(brandId);
	}

	@Override
	public List<Brand> getAll() {
		return null;
	}

	@Override
	public Brand updateBrand(Brand brand) {
		Brand brands = brandRepository.findOne(brand.getId());
		try {
			nonNullBeanUtils.copyProperties(brands, brand);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		brands.setModifiedDate(new LocalDateTime());
		brands.setUserModified("admin");
		if (brand.getBrandImageses() != null
				&& brand.getBrandImageses() != null) {
			brand.setBrandImageses(addBrandImages(brand,
					brand.getBrandImageses()));
		}
		brand = brandRepository.save(brands);
		return brand;
	}

	private Set<BrandImages> addBrandImages(Brand brand,
			Set<BrandImages> brandImageses) {
		Set<BrandImages> brandImages = new HashSet<>();
		for (BrandImages brandImg : brandImageses) {
			BrandImages brandImage = brandImg;
			brandImage.setBrand(brand);
			brandImage = brandImagesService.create(brandImage);
			brandImages.add(brandImage);
		}
		return brandImages;
	}

	@Override
	public List<Brand> getBrandList() {
		List<Brand> brands = (List<Brand>) brandRepository.findAll();
		List<Brand> brandss = new ArrayList<>();
		for (Brand brand : brands) {
			Brand bran = new Brand();
			bran.setName(brand.getName());
			bran.setStatus(brand.getStatus());
			bran.setId(brand.getId());
			brandss.add(bran);
		}

		return brandss;
	}

	@Override
	public boolean checkBrandNameExist(String name) {
		return brandRepository.findByBrandName(name) != null;
	}

}
