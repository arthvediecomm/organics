package com.arthvedi.organic.productcatalog.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.productcatalog.domain.ProductCategory;
import com.arthvedi.organic.productcatalog.model.ProductCategoryModel;

@Component("productCategoryToProductCategoryModelConverter")
public class ProductCategoryToProductCategoryModelConverter
        implements Converter<ProductCategory, ProductCategoryModel> {
    @Autowired
    private ObjectFactory<ProductCategoryModel> productCategoryModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public ProductCategoryModel convert(final ProductCategory source) {
        ProductCategoryModel productCategoryModel = productCategoryModelFactory.getObject();
        BeanUtils.copyProperties(source, productCategoryModel);

        return productCategoryModel;
    }

    @Autowired
    public void setProductCategoryModelFactory(
            final ObjectFactory<ProductCategoryModel> productCategoryModelFactory) {
        this.productCategoryModelFactory = productCategoryModelFactory;
    }
}
