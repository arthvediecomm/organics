package com.arthvedi.organic.productcatalog.model.converters;

import java.math.BigDecimal;

import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.productcatalog.domain.Feature;
import com.arthvedi.organic.productcatalog.domain.SellerProduct;
import com.arthvedi.organic.productcatalog.domain.SellerProductFeature;
import com.arthvedi.organic.productcatalog.model.SellerProductFeatureModel;

	@Component("sellerProductFeatureModelToSellerProductFeatureConverter")
	public class SellerProductFeatureModelToSellerProductFeatureConverter implements Converter<SellerProductFeatureModel, SellerProductFeature> {

		@Override
		public SellerProductFeature convert(final SellerProductFeatureModel source) {
			SellerProductFeature productFeature = new SellerProductFeature();
			BeanUtils.copyProperties(source, productFeature);
			if(source.getSellerProductId()!=null){
				SellerProduct sellerProduct = new SellerProduct();
				sellerProduct.setId(source.getSellerProductId());
				productFeature.setSellerProduct(sellerProduct);
			}
			if(source.getFeatureId()!=null){
				Feature feature = new Feature();
				feature.setId(source.getFeatureId());
				productFeature.setFeature(feature);
			}
			if(source.getExtraCharge()!=null){
				productFeature.setExtraCharge(new BigDecimal(source.getExtraCharge()));
			}
			return productFeature;
		}
}
