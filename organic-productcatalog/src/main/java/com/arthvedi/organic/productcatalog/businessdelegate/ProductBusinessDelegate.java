package com.arthvedi.organic.productcatalog.businessdelegate;

import static com.arthvedi.organic.enums.ProductType.COMBO;
import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.productcatalog.businessdelegate.context.ProductContext;
import com.arthvedi.organic.productcatalog.domain.ComboProduct;
import com.arthvedi.organic.productcatalog.domain.Product;
import com.arthvedi.organic.productcatalog.model.ComboProductModel;
import com.arthvedi.organic.productcatalog.model.ProductModel;
import com.arthvedi.organic.productcatalog.service.IComboProductService;
import com.arthvedi.organic.productcatalog.service.IProductFeatureService;
import com.arthvedi.organic.productcatalog.service.IProductImagesService;
import com.arthvedi.organic.productcatalog.service.IProductNamesService;
import com.arthvedi.organic.productcatalog.service.IProductService;

@Service
public class ProductBusinessDelegate
		implements
		IBusinessDelegate<ProductModel, ProductContext, IKeyBuilder<String>, String> {

	@Autowired
	private IProductService productService;
	@Autowired
	private ConversionService conversionService;
	@Autowired
	private IProductNamesService productNamesService;
	@Autowired
	IComboProductService comboProductService;
	@Autowired
	IProductFeatureService productFeatureService;
	@Autowired
	IProductImagesService productImagesService;

	@Override
	public ProductModel create(ProductModel model) {
		validateModel(model);
		boolean productName = checkProductNameExists(model.getProductName());
		if (productName) {
			if (!model.getProductType().equals(COMBO.name())) {

				Product product = productService
						.create((Product) conversionService.convert(model,
								forObject(model), valueOf(Product.class)));
				model = convertToProductModel(product);

				model.setStatusMesage("SUCCESS::: Product Created Successfully");
			}

		} else {
			Product product = createComboProduct(model);
			model = convertToProductModel(product);
			model.setStatusMesage("SUCCESS::: Product Created Successfully");
		}

		return model;

	}

	private ProductModel convertToProductModel(Product product) {
		return (ProductModel) conversionService.convert(product,
				forObject(product), valueOf(ProductModel.class));
	}

	/*
	 * @Transactional private Set<ProductFeature> addProductFeature(Product
	 * product, List<ProductFeatureModel> productFeaturesModel) {
	 * Set<ProductFeature> productFeatures = new HashSet<ProductFeature>(); for
	 * (ProductFeatureModel productFeatureModel : productFeaturesModel) {
	 * productFeatureModel.setProductid(product.getId()); ProductFeature
	 * productFeature = productFeatureService .create((ProductFeature)
	 * conversionService.convert( productFeatureModel,
	 * forObject(productFeatureModel), valueOf(ProductFeature.class)));
	 * productFeatures.add(productFeature); } return productFeatures; }
	 * 
	 * @Transactional private Set<ProductNames> addProductNames(Product product,
	 * List<ProductNamesModel> productNamesesModel) { Set<ProductNames>
	 * productNames = new HashSet<ProductNames>(); for (ProductNamesModel
	 * productnameModel : productNamesesModel) {
	 * productnameModel.setProductId(product.getId()); ProductNames
	 * productNamess = productNamesService .create((ProductNames)
	 * conversionService.convert( productNamesesModel,
	 * forObject(productNamesesModel), valueOf(ProductNames.class)));
	 * productNames.add(productNamess); } return productNames; }
	 */

	@Transactional
	private Product createComboProduct(final ProductModel model) {
		validateModel(model);
		Product product = productService.create((Product) conversionService
				.convert(model, forObject(model), valueOf(Product.class)));
		List<String> childProductIds = new ArrayList<String>();
		if (product.getId() != null) {
			for (ComboProductModel comboProductModel : model
					.getComboProductsForChildProductIdModel()) {
				comboProductModel.setProductByParentProductId(product.getId());
				ComboProduct comboProduct = comboProductService
						.create((ComboProduct) conversionService.convert(
								comboProductModel,
								forObject(comboProductModel),
								valueOf(ComboProduct.class)));
				childProductIds.add(comboProductModel
						.getProductByChildProductId());
			}
		}
		return product;
	}

	private boolean checkProductNameExists(String productName) {
		return productService.checkProductExist(productName);
	}

	private void validateModel(ProductModel model) {

	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, ProductContext context) {

	}

	@Override
	public ProductModel edit(IKeyBuilder<String> keyBuilder, ProductModel model) {
		Product product = productService.getProduct(keyBuilder.build()
				.toString());

		product = productService.updateProduct((Product) conversionService
				.convert(model, forObject(model), valueOf(Product.class)));

		return model;
	}

	@Override
	public ProductModel getByKey(IKeyBuilder<String> keyBuilder,
			ProductContext context) {
		Product product = productService.getProduct(keyBuilder.build()
				.toString());
		ProductModel model = conversionService.convert(product,
				ProductModel.class);
		return model;
	}

	@Override
	public Collection<ProductModel> getCollection(ProductContext context) {
		List<Product> product = new ArrayList<Product>();
		if (context.getAll() != null) {
			product = productService.getAll();
		}
		if (context.getProductList() != null) {
			product = productService.getProductList();
		}
		if (context.getCategoryId() != null) {
			product = productService.getProductByCategory(context
					.getCategoryId());
		}
		if (context.getStatus() != null) {
			product = productService.getProductByStatus(context.getStatus());
		}
		if (context.getProductNames() != null) {
			product = productService.getProductByProductNames(context
					.getProductNames());
		}

		List<ProductModel> productModels = (List<ProductModel>) conversionService
				.convert(
						product,
						TypeDescriptor.forObject(product),
						TypeDescriptor.collection(List.class,
								TypeDescriptor.valueOf(ProductModel.class)));
		return productModels;
	}

	@Override
	public ProductModel edit(IKeyBuilder<String> keyBuilder,
			ProductModel model, ProductContext context) {
		return null;
	}

}