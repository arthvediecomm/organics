/**
 *
 */
package com.arthvedi.organic.productcatalog.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.productcatalog.domain.Feature;
import com.arthvedi.organic.productcatalog.model.FeatureModel;

/**
 * @author Jay
 *
 */
@Component("featureModelToFeatureConverter")
public class FeatureModelToFeatureConverter implements Converter<FeatureModel, Feature> {
    @Autowired
    private ObjectFactory<Feature> featureFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public Feature convert(final FeatureModel source) {
        Feature feature = featureFactory.getObject();
        BeanUtils.copyProperties(source, feature);

        return feature;
    }

    @Autowired
    public void setFeatureFactory(final ObjectFactory<Feature> featureFactory) {
        this.featureFactory = featureFactory;
    }

}
