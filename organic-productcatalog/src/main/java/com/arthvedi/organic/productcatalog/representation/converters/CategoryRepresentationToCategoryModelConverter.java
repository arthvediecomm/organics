/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.productcatalog.model.CategoryFeatureModel;
import com.arthvedi.organic.productcatalog.model.CategoryImagesModel;
import com.arthvedi.organic.productcatalog.model.CategoryModel;
import com.arthvedi.organic.productcatalog.model.CategoryNamesModel;
import com.arthvedi.organic.productcatalog.model.ProductCategoryModel;
import com.arthvedi.organic.productcatalog.representation.siren.CategoryRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("categoryRepresentationToCategoryModelConverter")
public class CategoryRepresentationToCategoryModelConverter extends
		PropertyCopyingConverter<CategoryRepresentation, CategoryModel> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public CategoryModel convert(final CategoryRepresentation source) {

		CategoryModel target = super.convert(source);
		if (CollectionUtils.isNotEmpty(source
				.getProductCategoriesRepresentation())) {
			List<ProductCategoryModel> converted = (List<ProductCategoryModel>) conversionService
					.convert(source.getProductCategoriesRepresentation(),
							TypeDescriptor.forObject(source
									.getProductCategoriesRepresentation()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									ProductCategoryModel.class));
			target.getProductCategoriesModel().addAll(converted);
		}
		if (CollectionUtils.isNotEmpty(source
				.getCategoryNamesesRepresentation())) {
			List<CategoryNamesModel> converted = (List<CategoryNamesModel>) conversionService
					.convert(source.getCategoryNamesesRepresentation(),
							TypeDescriptor.forObject(source
									.getCategoryNamesesRepresentation()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									CategoryNamesModel.class));
			target.getCategoryNamesesModel().addAll(converted);
		}
		if (CollectionUtils.isNotEmpty(source
				.getCategoryFeaturesRepresentation())) {
			List<CategoryFeatureModel> convert = (List<CategoryFeatureModel>) conversionService
					.convert(source.getCategoryFeaturesRepresentation(),
							TypeDescriptor.forObject(source
									.getCategoryFeaturesRepresentation()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									CategoryFeatureModel.class));
			target.getCategoryFeaturesModel().addAll(convert);
		}

		if (CollectionUtils.isNotEmpty(source
				.getCategoryImagesesRepresentation())) {
			List<CategoryImagesModel> convert = (List<CategoryImagesModel>) conversionService
					.convert(source.getCategoryImagesesRepresentation(),
							TypeDescriptor.forObject(source
									.getCategoryImagesesRepresentation()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									CategoryImagesModel.class));
			target.getCategoryImagesesModel().addAll(convert);
		}

		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(final ObjectFactory<CategoryModel> factory) {
		super.setFactory(factory);
	}

}
