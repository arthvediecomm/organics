package com.arthvedi.organic.productcatalog.service;

import java.util.List;

import com.arthvedi.organic.productcatalog.domain.Seo;
/*
*@Author varma
*/
public interface ISeoService {
	
	Seo create(Seo seo);

	void deleteSeo(String seoId);

	Seo getSeo(String seoId);

	List<Seo> getAll();

	Seo updateSeo(Seo seo);
}
