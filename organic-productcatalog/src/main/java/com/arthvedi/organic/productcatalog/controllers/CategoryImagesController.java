package com.arthvedi.organic.productcatalog.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.productcatalog.businessdelegate.context.CategoryImagesContext;
import com.arthvedi.organic.productcatalog.model.CategoryImagesModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/categoryimages", produces = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class CategoryImagesController {

	private IBusinessDelegate<CategoryImagesModel, CategoryImagesContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<CategoryImagesContext> categoryImagesContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<CategoryImagesModel> createCategoryImages(
			@RequestBody final CategoryImagesModel categoryImagesModel) {
		businessDelegate.create(categoryImagesModel);
		return new ResponseEntity<CategoryImagesModel>(categoryImagesModel,
				HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	public ResponseEntity<CategoryImagesModel> edit(
			@PathVariable(value = "id") final String categoryImagesId,
			@RequestBody final CategoryImagesModel categoryImagesModel) {
		businessDelegate.edit(
				keyBuilderFactory.getObject().withId(categoryImagesId),
				categoryImagesModel);
		return new ResponseEntity<CategoryImagesModel>(categoryImagesModel,
				HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<IModelWrapper<Collection<CategoryImagesModel>>> getAll() {
		CategoryImagesContext categoryImagesContext = categoryImagesContextFactory
				.getObject();
		categoryImagesContext.setAll("all");
		Collection<CategoryImagesModel> categoryImagesModels = businessDelegate
				.getCollection(categoryImagesContext);
		IModelWrapper<Collection<CategoryImagesModel>> models = new CollectionModelWrapper<CategoryImagesModel>(
				CategoryImagesModel.class, categoryImagesModels);
		return new ResponseEntity<IModelWrapper<Collection<CategoryImagesModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<CategoryImagesModel> getCategoryImages(
			@PathVariable(value = "id") final String categoryImagesId) {
		CategoryImagesContext categoryImagesContext = categoryImagesContextFactory
				.getObject();

		CategoryImagesModel model = businessDelegate.getByKey(keyBuilderFactory
				.getObject().withId(categoryImagesId), categoryImagesContext);
		return new ResponseEntity<CategoryImagesModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "categoryImagesBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<CategoryImagesModel, CategoryImagesContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setCategoryImagesObjectFactory(
			final ObjectFactory<CategoryImagesContext> categoryImagesContextFactory) {
		this.categoryImagesContextFactory = categoryImagesContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(
			final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
