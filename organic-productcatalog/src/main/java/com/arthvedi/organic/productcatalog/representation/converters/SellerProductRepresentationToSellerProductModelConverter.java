/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.productcatalog.model.SellerProductModel;
import com.arthvedi.organic.productcatalog.representation.siren.SellerProductRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("sellerProductRepresentationToSellerProductModelConverter")
public class SellerProductRepresentationToSellerProductModelConverter extends PropertyCopyingConverter<SellerProductRepresentation, SellerProductModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public SellerProductModel convert(final SellerProductRepresentation source) {

        SellerProductModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<SellerProductModel> factory) {
        super.setFactory(factory);
    }

}
