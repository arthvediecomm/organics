/**
 *
 */
package com.arthvedi.organic.productcatalog.model.converters;

import java.math.BigDecimal;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.productcatalog.domain.SellerProduct;
import com.arthvedi.organic.productcatalog.domain.SellerProductPrices;
import com.arthvedi.organic.productcatalog.model.SellerProductPricesModel;

/**
 * @author Jay
 *
 */
@Component("sellerProductPricesModelToSellerProductPricesConverter")
public class SellerProductPricesModelToSellerProductPricesConverter implements Converter<SellerProductPricesModel, SellerProductPrices> {
    @Autowired
    private ObjectFactory<SellerProductPrices> sellerProductPricesFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public SellerProductPrices convert(final SellerProductPricesModel source) {
        SellerProductPrices sellerProductPrices = sellerProductPricesFactory.getObject();
        BeanUtils.copyProperties(source, sellerProductPrices);
       if(source.getSellerProductId()!=null){
SellerProduct sellerProduct=new SellerProduct();
sellerProduct.setId(source.getSellerProductId());
sellerProductPrices.setSellerProduct(sellerProduct);
       
       }

       if(source.getOfferPrice()!=null){
    	   sellerProductPrices.setOfferPrice(new BigDecimal(source.getOfferPrice()));
    	   
       }
       if(source.getSellerPrice()!=null){
    	   sellerProductPrices.setSellerPrice(new BigDecimal(source.getSellerPrice()));
       }
       if(source.getSellingPrice()!=null){
    	   sellerProductPrices.setSellingPrice(new BigDecimal(source.getSellingPrice()));
       }
       if(source.getDiscount()!=null){
    	   sellerProductPrices.setDiscount(new BigDecimal(source.getDiscount()));
       }
        return sellerProductPrices;
    }

    @Autowired
    public void setSellerProductPricesFactory(final ObjectFactory<SellerProductPrices> sellerProductPricesFactory) {
        this.sellerProductPricesFactory = sellerProductPricesFactory;
    }

}
