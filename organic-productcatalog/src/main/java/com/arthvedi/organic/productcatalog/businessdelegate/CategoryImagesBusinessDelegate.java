package com.arthvedi.organic.productcatalog.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.productcatalog.businessdelegate.context.CategoryImagesContext;
import com.arthvedi.organic.productcatalog.domain.CategoryImages;
import com.arthvedi.organic.productcatalog.model.CategoryImagesModel;
import com.arthvedi.organic.productcatalog.service.ICategoryImagesService;

/*
 *@Author varma
 */

@Service
public class CategoryImagesBusinessDelegate
		implements
		IBusinessDelegate<CategoryImagesModel, CategoryImagesContext, IKeyBuilder<String>, String> {
	@Autowired
	private ICategoryImagesService categoryImagesService;
	@Autowired
	private ConversionService conversionService;

	@Override
	@Transactional
	public CategoryImagesModel create(CategoryImagesModel model) {
		CategoryImages categoryImages = categoryImagesService
				.create((CategoryImages) conversionService.convert(model,
						forObject(model), valueOf(CategoryImages.class)));
		model = convertToCategoryImagesModel(categoryImages);

		return model;
	}

	private CategoryImagesModel convertToCategoryImagesModel(
			CategoryImages categoryImages) {

		return (CategoryImagesModel) conversionService.convert(categoryImages,
				forObject(categoryImages),valueOf(CategoryImagesModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			CategoryImagesContext context) {

	}

	@Override
	public CategoryImagesModel edit(IKeyBuilder<String> keyBuilder,
			CategoryImagesModel model) {
		CategoryImages categoryImages = categoryImagesService
				.getCategoryImages(keyBuilder.build().toString());

		categoryImages = categoryImagesService
				.updateCategoryImages((CategoryImages) conversionService
						.convert(model, forObject(model),
								valueOf(CategoryImages.class)));
		model = convertToCategoryImagesModel(categoryImages);
		return model;
	}

	@Override
	public CategoryImagesModel getByKey(IKeyBuilder<String> keyBuilder,
			CategoryImagesContext context) {
		CategoryImages categoryImages = categoryImagesService
				.getCategoryImages(keyBuilder.build().toString());
		CategoryImagesModel model = conversionService.convert(categoryImages,
				CategoryImagesModel.class);
		return model;
	}

	@Override
	public Collection<CategoryImagesModel> getCollection(
			CategoryImagesContext context) {
		List<CategoryImages> categoryImages = new ArrayList<CategoryImages>();

		List<CategoryImagesModel> categoryImagesModels = (List<CategoryImagesModel>) conversionService
				.convert(categoryImages, TypeDescriptor
						.forObject(categoryImages), TypeDescriptor.collection(
						List.class,
						TypeDescriptor.valueOf(CategoryImagesModel.class)));
		return categoryImagesModels;
	}

	@Override
	public CategoryImagesModel edit(IKeyBuilder<String> keyBuilder,
			CategoryImagesModel model, CategoryImagesContext context) {
		return null;
	}

}
