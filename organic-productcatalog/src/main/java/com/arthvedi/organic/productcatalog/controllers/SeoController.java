package com.arthvedi.organic.productcatalog.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.productcatalog.businessdelegate.context.SeoContext;
import com.arthvedi.organic.productcatalog.model.SeoModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/seo", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class SeoController {

	private IBusinessDelegate<SeoModel, SeoContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<SeoContext> seoContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<SeoModel> createSeo(@RequestBody final SeoModel seoModel) {
		businessDelegate.create(seoModel);
		return new ResponseEntity<SeoModel>(seoModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<SeoModel> edit(@PathVariable(value = "id") final String seoId,
			@RequestBody final SeoModel seoModel) {

		businessDelegate.edit(keyBuilderFactory.getObject().withId(seoId), seoModel);
		return new ResponseEntity<SeoModel>(seoModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<SeoModel>>> getAll() {
		SeoContext seoContext = seoContextFactory.getObject();
		seoContext.setAll("all");
		Collection<SeoModel> seoModels = businessDelegate.getCollection(seoContext);
		IModelWrapper<Collection<SeoModel>> models = new CollectionModelWrapper<SeoModel>(
				SeoModel.class, seoModels);
		return new ResponseEntity<IModelWrapper<Collection<SeoModel>>>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<SeoModel> getSeo(@PathVariable(value = "id") final String seoId) {
		SeoContext seoContext = seoContextFactory.getObject();

		SeoModel model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(seoId),
				seoContext);
		return new ResponseEntity<SeoModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "seoBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<SeoModel, SeoContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setSeoObjectFactory(final ObjectFactory<SeoContext> seoContextFactory) {
		this.seoContextFactory = seoContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
