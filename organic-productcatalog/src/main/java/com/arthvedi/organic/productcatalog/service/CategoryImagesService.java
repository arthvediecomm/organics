package com.arthvedi.organic.productcatalog.service;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.productcatalog.domain.CategoryImages;
import com.arthvedi.organic.productcatalog.respository.CategoryImagesRepository;

/*
 *@Author varma
 */
@Component
public class CategoryImagesService implements ICategoryImagesService {
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private CategoryImagesRepository categoryImagesRepository;

	@Override
	public CategoryImages create(CategoryImages categoryImages) {
		categoryImages.setUserCreated("varma");
		categoryImages.setCreatedDate(new LocalDateTime());
		categoryImages = categoryImagesRepository.save(categoryImages);
		return categoryImages;
	}

	@Override
	public void deleteCategoryImages(String categoryImagesId) {

	}

	@Override
	public CategoryImages getCategoryImages(String categoryImagesId) {
		return categoryImagesRepository.findOne(categoryImagesId);
	}

	@Override
	public List<CategoryImages> getAll() {
		List<CategoryImages> categoryImages = (List<CategoryImages>) categoryImagesRepository
				.findAll();
		List<CategoryImages> catImages = new ArrayList<CategoryImages>();
		CategoryImages categoryImage = new CategoryImages();
		for (CategoryImages categoryImg : categoryImages) {
			categoryImage.setImageFolder(categoryImg.getImageFolder());
			categoryImage.setImageName(categoryImg.getImageName());
			categoryImage.setImageType(categoryImg.getImageType());
			catImages.add(categoryImage);

		}

		return catImages;
	}

	@Override
	public CategoryImages updateCategoryImages(CategoryImages categoryImages) {
		CategoryImages categoryImagess = categoryImagesRepository
				.findOne(categoryImages.getId());
		try {
			nonNullBeanUtils.copyProperties(categoryImagess, categoryImages);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		categoryImagess.setUserModified("admin");
		categoryImagess.setModifiedDate(new LocalDateTime());
		categoryImages = categoryImagesRepository.save(categoryImagess);
		return categoryImages;
	}

}
