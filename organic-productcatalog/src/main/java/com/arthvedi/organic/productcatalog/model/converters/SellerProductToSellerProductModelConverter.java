package com.arthvedi.organic.productcatalog.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.productcatalog.domain.SellerProduct;
import com.arthvedi.organic.productcatalog.model.SellerProductModel;

@Component("sellerProductToSellerProductModelConverter")
public class SellerProductToSellerProductModelConverter implements
		Converter<SellerProduct, SellerProductModel> {
	@Autowired
	private  ObjectFactory<SellerProductModel> sellerProductModelFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerProductModel convert(final SellerProduct source) {
		SellerProductModel sellerProductModel = sellerProductModelFactory
				.getObject();
		BeanUtils.copyProperties(source, sellerProductModel);
		if (source.getProductMrp() != null) {
			sellerProductModel.setProductMrp(source.getProductMrp().toString());
		}
		if (source.getSellingPrice() != null) {
			sellerProductModel.setSellingPrice(source.getSellerPrice()
					.toString());
		}
		if (source.getOfferPrice() != null) {
			sellerProductModel.setOfferPrice(source.getOfferPrice().toString());
		}
		if(source.getSellerPrice() != null) {
			sellerProductModel.setSellerPrice(source.getSellerPrice()
					.toString());
		}
		return sellerProductModel;
	}

	@Autowired
	public void setSellerProductModelFactory(
			final ObjectFactory<SellerProductModel> sellerProductModelFactory) {
		this.sellerProductModelFactory = sellerProductModelFactory;
	}
}
