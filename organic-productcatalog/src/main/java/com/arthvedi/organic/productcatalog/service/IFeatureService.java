package com.arthvedi.organic.productcatalog.service;

import java.util.List;

import com.arthvedi.organic.productcatalog.domain.Feature;
/*
*@Author varma
*/
public interface IFeatureService {
	
	Feature create(Feature feature);

	void deleteFeature(String featureId);

	Feature getFeature(String featureId);

	List<Feature> getAll();

	Feature updateFeature(Feature feature);

	List<Feature> getFeatureList();


	List<Feature> getFeatureByCategoryId(String categoryId);

	List<Feature> getFeatureByStatus(String status);

	
}
