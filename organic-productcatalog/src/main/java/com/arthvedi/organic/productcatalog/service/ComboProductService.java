package com.arthvedi.organic.productcatalog.service;

import static com.arthvedi.organic.enums.Status.ACTIVE;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.productcatalog.domain.ComboProduct;
import com.arthvedi.organic.productcatalog.respository.ComboProductRepository;
/*
*@Author varma
*/
@Component
public class ComboProductService implements IComboProductService{
@Autowired
private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private ComboProductRepository comboProductRepository;
	@Override
	public ComboProduct create(ComboProduct comboProduct) {
	comboProduct.setCreatedDate(new LocalDateTime());
	comboProduct.setUserCreated("varma");
	comboProduct.setStatus(ACTIVE.name());
		comboProduct= comboProductRepository.save(comboProduct);
		return comboProduct;
	}

	@Override
	public void deleteComboProduct(String comboProductId) {
		
	}

	@Override
	public ComboProduct getComboProduct(String comboProductId) {
		 return comboProductRepository.findOne(comboProductId);
	}

	@Override
	public List<ComboProduct> getAll() {
		return null;
	}

	@Override

	public ComboProduct updateComboProduct(ComboProduct comboProduct) {
	ComboProduct comboProducts=comboProductRepository.findOne(comboProduct.getId());
	try{
		nonNullBeanUtils.copyProperties(comboProducts, comboProduct);
	}catch(IllegalAccessException e){
		e.printStackTrace();
	}catch(InvocationTargetException e){
		e.printStackTrace();
	}
	comboProducts.setUserModified("admin");
	comboProducts.setModifiedDate(new LocalDateTime());
	comboProduct=comboProductRepository.save(comboProducts);
		return comboProduct;
	}

	@Override
	public List<ComboProduct> getComboProductList() {
		List<ComboProduct> comboProducts=(List<ComboProduct>)comboProductRepository.findAll();
		List<ComboProduct> comboProductss=new ArrayList<ComboProduct>();
		for(ComboProduct comPro:comboProducts){
			ComboProduct comboProduct=new ComboProduct();
			comboProduct.setPrice(comPro.getPrice());
			comboProduct.setUnits(comPro.getUnits());
			comboProductss.add(comboProduct);
		}
		return comboProductss;
	}

}
