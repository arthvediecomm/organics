/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.productcatalog.model.SellerProductPricesModel;
import com.arthvedi.organic.productcatalog.representation.siren.SellerProductPricesRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("sellerProductPricesRepresentationToSellerProductPricesModelConverter")
public class SellerProductPricesRepresentationToSellerProductPricesModelConverter extends PropertyCopyingConverter<SellerProductPricesRepresentation, SellerProductPricesModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public SellerProductPricesModel convert(final SellerProductPricesRepresentation source) {

        SellerProductPricesModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<SellerProductPricesModel> factory) {
        super.setFactory(factory);
    }

}
