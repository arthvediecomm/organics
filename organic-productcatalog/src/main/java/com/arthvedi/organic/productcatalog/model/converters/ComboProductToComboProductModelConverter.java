package com.arthvedi.organic.productcatalog.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.productcatalog.domain.ComboProduct;
import com.arthvedi.organic.productcatalog.model.ComboProductModel;

@Component("comboProductToComboProductModelConverter")
public class ComboProductToComboProductModelConverter
        implements Converter<ComboProduct, ComboProductModel> {
    @Autowired
    private ObjectFactory<ComboProductModel> comboProductModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public ComboProductModel convert(final ComboProduct source) {
        ComboProductModel comboProductModel = comboProductModelFactory.getObject();
        BeanUtils.copyProperties(source, comboProductModel);

        return comboProductModel;
    }

    @Autowired
    public void setComboProductModelFactory(
            final ObjectFactory<ComboProductModel> comboProductModelFactory) {
        this.comboProductModelFactory = comboProductModelFactory;
    }
}
