/**
 *
 */
package com.arthvedi.organic.productcatalog.model.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.organic.productcatalog.domain.Category;
import com.arthvedi.organic.productcatalog.domain.CategoryFeature;
import com.arthvedi.organic.productcatalog.domain.CategoryImages;
import com.arthvedi.organic.productcatalog.domain.CategoryNames;
import com.arthvedi.organic.productcatalog.domain.ProductCategory;
import com.arthvedi.organic.productcatalog.model.CategoryModel;

/**
 * @author Jay
 *
 */
@Component("categoryModelToCategoryConverter")
public class CategoryModelToCategoryConverter implements
		Converter<CategoryModel, Category> {
	@Autowired
	private ObjectFactory<Category> categoryFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public Category convert(final CategoryModel source) {
		Category category = new Category();
		BeanUtils.copyProperties(source, category);

		if (source.getCategoryId() != null) {
			Category parentCategory = new Category();
			parentCategory.setId(source.getCategoryId());
			category.setCategory(parentCategory);
		}
		if (CollectionUtils.isNotEmpty(source.getCategoryImagesesModel())) {
			List<CategoryImages> convert = (List<CategoryImages>) conversionService
					.convert(source.getCategoryImagesesModel(), TypeDescriptor
							.forObject(source.getCategoryImagesesModel()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									CategoryImages.class));
			category.getCategoryImageses().addAll(convert);
		}
		if (CollectionUtils.isNotEmpty(source.getProductCategoriesModel())) {
			List<ProductCategory> converted = (List<ProductCategory>) conversionService
					.convert(source.getProductCategoriesModel(), TypeDescriptor
							.forObject(source.getProductCategoriesModel()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									ProductCategory.class));
			category.getProductCategories().addAll(converted);
		}
		if (CollectionUtils.isNotEmpty(source.getCategoryNamesesModel())) {
			List<CategoryNames> converted = (List<CategoryNames>) conversionService
					.convert(source.getCategoryNamesesModel(), TypeDescriptor
							.forObject(source.getCategoryNamesesModel()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									CategoryNames.class));
			category.getCategoryNameses().addAll(converted);
		}
		if (CollectionUtils.isNotEmpty(source.getCategoryFeaturesModel())) {
			List<CategoryFeature> converted = (List<CategoryFeature>) conversionService
					.convert(source.getCategoryFeaturesModel(), TypeDescriptor
							.forObject(source.getCategoryFeaturesModel()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									CategoryFeature.class));
			category.getCategoryFeatures().addAll(converted);
		}

		return category;
	}

	@Autowired
	public void setCategoryFactory(final ObjectFactory<Category> categoryFactory) {
		this.categoryFactory = categoryFactory;
	}

}
