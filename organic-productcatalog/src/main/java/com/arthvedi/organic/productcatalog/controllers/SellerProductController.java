package com.arthvedi.organic.productcatalog.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.productcatalog.businessdelegate.context.SellerProductContext;
import com.arthvedi.organic.productcatalog.model.SellerProductModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/sellerproduct", produces = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class SellerProductController {

	private IBusinessDelegate<SellerProductModel, SellerProductContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<SellerProductContext> sellerProductContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<SellerProductModel> createSellerProduct(
			@RequestBody final SellerProductModel sellerProductModel) {
		businessDelegate.create(sellerProductModel);
		return new ResponseEntity<SellerProductModel>(sellerProductModel,
				HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	public ResponseEntity<SellerProductModel> edit(
			@PathVariable(value = "id") final String sellerProductId,
			@RequestBody final SellerProductModel sellerProductModel) {

		businessDelegate.edit(
				keyBuilderFactory.getObject().withId(sellerProductId),
				sellerProductModel);
		return new ResponseEntity<SellerProductModel>(sellerProductModel,
				HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<IModelWrapper<Collection<SellerProductModel>>> getAll() {
		SellerProductContext sellerProductContext = sellerProductContextFactory
				.getObject();
		sellerProductContext.setAll("all");
		Collection<SellerProductModel> sellerProductModels = businessDelegate
				.getCollection(sellerProductContext);
		IModelWrapper<Collection<SellerProductModel>> models = new CollectionModelWrapper<SellerProductModel>(
				SellerProductModel.class, sellerProductModels);
		return new ResponseEntity<IModelWrapper<Collection<SellerProductModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/sellerproductlist", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<IModelWrapper<Collection<SellerProductModel>>> getSellerProductList() {
		SellerProductContext sellerProductContext = sellerProductContextFactory
				.getObject();
		sellerProductContext.setSellerProductList("sellerProductList");
		Collection<SellerProductModel> sellerProductModels = businessDelegate
				.getCollection(sellerProductContext);
		IModelWrapper<Collection<SellerProductModel>> models = new CollectionModelWrapper<SellerProductModel>(
				SellerProductModel.class, sellerProductModels);
		return new ResponseEntity<IModelWrapper<Collection<SellerProductModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/sellerproductbyproducts/{productId}", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<IModelWrapper<Collection<SellerProductModel>>> get(
			@PathVariable(value = "productId") String productId) {
		SellerProductContext sellerProductContext = sellerProductContextFactory
				.getObject();
		sellerProductContext.setProductId(productId);
		Collection<SellerProductModel> sellerProductModels = businessDelegate
				.getCollection(sellerProductContext);
		IModelWrapper<Collection<SellerProductModel>> models = new CollectionModelWrapper<SellerProductModel>(
				SellerProductModel.class, sellerProductModels);
		return new ResponseEntity<IModelWrapper<Collection<SellerProductModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/sellerproductbyproduct/{status}", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<IModelWrapper<Collection<SellerProductModel>>> getByStatus(
			@PathVariable(value = "status") String status) {
		SellerProductContext sellerProductContext = sellerProductContextFactory
				.getObject();
		sellerProductContext.setStatus(status);
		Collection<SellerProductModel> sellerProductModels = businessDelegate
				.getCollection(sellerProductContext);
		IModelWrapper<Collection<SellerProductModel>> models = new CollectionModelWrapper<SellerProductModel>(
				SellerProductModel.class, sellerProductModels);
		return new ResponseEntity<IModelWrapper<Collection<SellerProductModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<SellerProductModel> getSellerProduct(
			@PathVariable(value = "id") final String sellerProductId) {
		SellerProductContext sellerProductContext = sellerProductContextFactory
				.getObject();

		SellerProductModel model = businessDelegate.getByKey(keyBuilderFactory
				.getObject().withId(sellerProductId), sellerProductContext);
		return new ResponseEntity<SellerProductModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "sellerProductBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<SellerProductModel, SellerProductContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setSellerProductObjectFactory(
			final ObjectFactory<SellerProductContext> sellerProductContextFactory) {
		this.sellerProductContextFactory = sellerProductContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(
			final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
