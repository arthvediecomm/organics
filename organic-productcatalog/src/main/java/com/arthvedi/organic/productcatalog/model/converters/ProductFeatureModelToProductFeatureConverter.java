/**
 *
 */
package com.arthvedi.organic.productcatalog.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.productcatalog.domain.Feature;
import com.arthvedi.organic.productcatalog.domain.Product;
import com.arthvedi.organic.productcatalog.domain.ProductFeature;
import com.arthvedi.organic.productcatalog.model.ProductFeatureModel;

/**
 * @author Jay
 *
 */
@Component("productFeatureModelToProductFeatureConverter")
public class ProductFeatureModelToProductFeatureConverter implements
		Converter<ProductFeatureModel, ProductFeature> {
	@Autowired
	private ObjectFactory<ProductFeature> productFeatureFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public ProductFeature convert(final ProductFeatureModel source) {
		ProductFeature productFeature = new ProductFeature();
		BeanUtils.copyProperties(source, productFeature);
		if (source.getProductid() != null) {
			Product product = new Product();
			product.setId(source.getProductid());
			productFeature.setProduct(product);
		}
		if (source.getFeatureId() != null) {
			Feature feature = new Feature();
			feature.setId(source.getFeatureId());
			productFeature.setFeature(feature);
		}

		return productFeature;
	}

	@Autowired
	public void setProductFeatureFactory(
			final ObjectFactory<ProductFeature> productFeatureFactory) {
		this.productFeatureFactory = productFeatureFactory;
	}

}
