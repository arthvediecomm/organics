/**
 *
 */
package com.arthvedi.organic.productcatalog.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.commons.domain.Language;
import com.arthvedi.organic.productcatalog.domain.Category;
import com.arthvedi.organic.productcatalog.domain.CategoryNames;
import com.arthvedi.organic.productcatalog.domain.Seo;
import com.arthvedi.organic.productcatalog.model.CategoryNamesModel;

/**
 * @author Jay
 *
 */
@Component("categoryNamesModelToCategoryNamesConverter")
public class CategoryNamesModelToCategoryNamesConverter implements
		Converter<CategoryNamesModel, CategoryNames> {
	@Autowired
	private ObjectFactory<CategoryNames> categoryNamesFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public CategoryNames convert(final CategoryNamesModel source) {
		CategoryNames categoryNames = categoryNamesFactory.getObject();
		BeanUtils.copyProperties(source, categoryNames);
		if (source.getCategoryId() != null) {
			Category category = new Category();
			category.setId(source.getCategoryId());
			categoryNames.setCategory(category);
		}
		if (source.getLanguageId() != null) {
			Language language = new Language();
			language.setId(source.getLanguageId());
			categoryNames.setLanguage(language);
		}

		if (source.getSeoTitle() != null) {
			Seo seo = new Seo();
			seo.setSeoDescription(source.getSeoDescription());
			seo.setSeoKeyword(source.getSeoKeyword());

			seo.setSeoTitle(source.getSeoTitle());
			if (source.getSeoId() != null) {
				seo.setId(source.getSeoId());
			}
			categoryNames.setSeo(seo);
		}
		return categoryNames;
	}

	@Autowired
	public void setCategoryNamesFactory(
			final ObjectFactory<CategoryNames> categoryNamesFactory) {
		this.categoryNamesFactory = categoryNamesFactory;
	}

}
