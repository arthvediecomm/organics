package com.arthvedi.organic.productcatalog.service;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.productcatalog.domain.ProductNames;
import com.arthvedi.organic.productcatalog.domain.Seo;
import com.arthvedi.organic.productcatalog.respository.ProductNamesRepository;

/*
 *@Author varma
 */
@Component
public class ProductNamesService implements IProductNamesService {
	@Autowired
	NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	ISeoService seoService;

	@Autowired
	private ProductNamesRepository productNamesRepository;

	@Override
	public ProductNames create(ProductNames productNames) {
		productNames.setCreatedDate(new LocalDateTime());
		productNames.setUserCreated("varma");
		if (productNames.getSeo() != null) {
			Seo seo = seoService.create(productNames.getSeo());
			productNames.setSeo(seo);
		}
		productNames = productNamesRepository.save(productNames);
		return productNames;
	}

	@Override
	public void deleteProductNames(String productNamesId) {

	}

	@Override
	public ProductNames getProductNames(String productNamesId) {
		return productNamesRepository.findOne(productNamesId);
	}

	@Override
	public List<ProductNames> getAll() {
		return null;
	}

	@Override
	public ProductNames updateProductNames(ProductNames productNames) {
		ProductNames productNamess = productNamesRepository
				.findOne(productNames.getId());
		try {
			nonNullBeanUtils.copyProperties(productNamess, productNames);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		productNamess.setUserModified("admin");
		productNamess.setModifiedDate(new LocalDateTime());
		productNames = productNamesRepository.save(productNamess);

		return productNames;
	}

	@Override
	public List<ProductNames> getProductNamesList() {
		List<ProductNames> productNames = (List<ProductNames>) productNamesRepository
				.findAll();
		List<ProductNames> productNamess = new ArrayList<ProductNames>();
		for (ProductNames proNames : productNames) {
			ProductNames proName = new ProductNames();
			proName.setProduct(proNames.getProduct());

			productNamess.add(proName);
		}
		return productNamess;
	}

}
