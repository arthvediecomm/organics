/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.productcatalog.model.FeatureImagesModel;
import com.arthvedi.organic.productcatalog.representation.siren.FeatureImagesRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("featureImagesRepresentationToFeatureImagesModelConverter")
public class FeatureImagesRepresentationToFeatureImagesModelConverter extends PropertyCopyingConverter<FeatureImagesRepresentation, FeatureImagesModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public FeatureImagesModel convert(final FeatureImagesRepresentation source) {

        FeatureImagesModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<FeatureImagesModel> factory) {
        super.setFactory(factory);
    }

}
