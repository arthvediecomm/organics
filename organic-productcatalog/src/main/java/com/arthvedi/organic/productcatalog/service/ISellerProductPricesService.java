package com.arthvedi.organic.productcatalog.service;

import java.util.List;

import com.arthvedi.organic.productcatalog.domain.SellerProductPrices;
/*
*@Author varma
*/
public interface ISellerProductPricesService {
	
	SellerProductPrices create(SellerProductPrices sellerProductPrices);

	void deleteSellerProductPrices(String sellerProductPricesId);

	SellerProductPrices getSellerProductPrices(String sellerProductPricesId);

	List<SellerProductPrices> getAll();

	SellerProductPrices updateSellerProductPrices(SellerProductPrices sellerProductPrices);
}
