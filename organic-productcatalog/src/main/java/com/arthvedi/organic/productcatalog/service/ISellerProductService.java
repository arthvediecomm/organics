package com.arthvedi.organic.productcatalog.service;

import java.util.List;

import com.arthvedi.organic.productcatalog.domain.SellerProduct;

/*
 *@Author varma
 */
public interface ISellerProductService {

	SellerProduct create(SellerProduct sellerProduct);

	void deleteSellerProduct(String sellerProductId);

	SellerProduct getSellerProduct(String sellerProductId);

	List<SellerProduct> getAll();

	SellerProduct updateSellerProduct(SellerProduct sellerProduct);

	List<SellerProduct> getSellerProductList();

	List<SellerProduct> getSellerProductByProduct(String productId);

	List<SellerProduct> getByStatus(String status);
}
