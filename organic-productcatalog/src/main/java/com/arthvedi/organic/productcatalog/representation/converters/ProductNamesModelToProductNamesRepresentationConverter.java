/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.productcatalog.model.ProductNamesModel;
import com.arthvedi.organic.productcatalog.representation.siren.ProductNamesRepresentation;

/**
 * @author varma
 *
 */
@Component("productNamesModelToProductNamesRepresentationConverter")
public class ProductNamesModelToProductNamesRepresentationConverter extends
		PropertyCopyingConverter<ProductNamesModel, ProductNamesRepresentation> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public ProductNamesRepresentation convert(final ProductNamesModel source) {

		ProductNamesRepresentation target = super.convert(source);

		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(
			final ObjectFactory<ProductNamesRepresentation> factory) {
		super.setFactory(factory);
	}

}
