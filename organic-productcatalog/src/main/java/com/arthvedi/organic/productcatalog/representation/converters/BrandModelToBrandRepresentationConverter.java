/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.productcatalog.model.BrandModel;
import com.arthvedi.organic.productcatalog.representation.siren.BrandImagesRepresentation;
import com.arthvedi.organic.productcatalog.representation.siren.BrandRepresentation;

/**
 * @author varma
 *
 */
@Component("brandModelToBrandRepresentationConverter")
public class BrandModelToBrandRepresentationConverter extends
		PropertyCopyingConverter<BrandModel, BrandRepresentation> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public BrandRepresentation convert(final BrandModel source) {

		BrandRepresentation target = super.convert(source);
		if (CollectionUtils.isNotEmpty(source.getBrandImagesModels())) {
			List<BrandImagesRepresentation> convert = (List<BrandImagesRepresentation>) conversionService
					.convert(source.getBrandImagesModels(), TypeDescriptor
							.forObject(source.getBrandImagesModels()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									BrandImagesRepresentation.class));
			target.getBrandImagesRepresentation().addAll(convert);
		}

		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(final ObjectFactory<BrandRepresentation> factory) {
		super.setFactory(factory);
	}

}
