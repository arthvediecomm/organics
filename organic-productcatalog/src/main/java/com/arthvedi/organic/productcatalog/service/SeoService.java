package com.arthvedi.organic.productcatalog.service;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.productcatalog.domain.Seo;
import com.arthvedi.organic.productcatalog.respository.SeoRepository;

/*
 *@Author varma
 */
@Component
public class SeoService implements ISeoService {
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private SeoRepository seoRepository;

	@Override
	public Seo create(Seo seo) {
		seo.setCreatedDate(new LocalDateTime());
		seo.setUserCreated("varma");

		return seoRepository.save(seo);
	}

	@Override
	public void deleteSeo(String seoId) {

	}

	@Override
	public Seo getSeo(String seoId) {
		return seoRepository.findOne(seoId);
	}

	@Override
	public List<Seo> getAll() {
		return null;
	}

	@Override
	public Seo updateSeo(Seo seo) {
		Seo seos = seoRepository.findOne(seo.getId());
		try {
			nonNullBeanUtils.copyProperties(seos, seo);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		seos.setUserModified("admin");
		seos.setModifiedDate(new LocalDateTime());
		seo = seoRepository.save(seo);
		return seo;
	}

}
