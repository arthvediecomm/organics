package com.arthvedi.organic.productcatalog.service;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.productcatalog.domain.SellerProductPrices;
import com.arthvedi.organic.productcatalog.respository.SellerProductPricesRepository;
/*
*@Author varma
*/
@Component
public class SellerProductPricesService implements ISellerProductPricesService{
@Autowired
NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private SellerProductPricesRepository sellerProductPricesRepository;
	@Override
	public SellerProductPrices create(SellerProductPrices sellerProductPrices) {
sellerProductPrices.setCreatedDate(new LocalDateTime());
sellerProductPrices.setUserCreated("varma");
		
		return sellerProductPricesRepository.save(sellerProductPrices);
	}

	@Override
	public void deleteSellerProductPrices(String sellerProductPricesId) {
		
	}

	@Override
	public SellerProductPrices getSellerProductPrices(String sellerProductPricesId) {
		 return sellerProductPricesRepository.findOne(sellerProductPricesId);
	}

	@Override
	public List<SellerProductPrices> getAll() {
		return null;
	}

	@Override
	public SellerProductPrices updateSellerProductPrices(SellerProductPrices sellerProductPrices) {
	SellerProductPrices sellerProductPricess=sellerProductPricesRepository.findOne(sellerProductPrices.getId());
		try{
			nonNullBeanUtils.copyProperties(sellerProductPricess, sellerProductPrices);
		}catch(IllegalAccessException e){
			e.printStackTrace();
		}catch(InvocationTargetException t){
			t.printStackTrace();
		}
		sellerProductPricess.setUserModified("admin");
		sellerProductPricess.setModifiedDate(new LocalDateTime());
		
		sellerProductPrices=sellerProductPricesRepository.save(sellerProductPricess);
		return sellerProductPrices;
	}

}
