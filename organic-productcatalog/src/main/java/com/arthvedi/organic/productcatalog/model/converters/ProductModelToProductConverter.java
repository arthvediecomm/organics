/**
 *
 */
package com.arthvedi.organic.productcatalog.model.converters;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.organic.productcatalog.domain.Brand;
import com.arthvedi.organic.productcatalog.domain.ComboProduct;
import com.arthvedi.organic.productcatalog.domain.Product;
import com.arthvedi.organic.productcatalog.domain.ProductCategory;
import com.arthvedi.organic.productcatalog.domain.ProductFeature;
import com.arthvedi.organic.productcatalog.domain.ProductImages;
import com.arthvedi.organic.productcatalog.domain.ProductNames;
import com.arthvedi.organic.productcatalog.model.ProductModel;

/**
 * @author Jay
 *
 */
@Component("productModelToProductConverter")
public class ProductModelToProductConverter implements
		Converter<ProductModel, Product> {
	@Autowired
	private ObjectFactory<Product> productFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public Product convert(final ProductModel source) {
		Product product = new Product();
		BeanUtils.copyProperties(source, product);
		if (source.getMrp() != null) {
			product.setMrp(new BigDecimal(source.getMrp()));
		}
		if (source.getBrandId() != null) {
			Brand brand = new Brand();
			brand.setId(source.getBrandId());
			product.setBrand(brand);
		}
		if (CollectionUtils.isNotEmpty(source
				.getComboProductsForChildProductIdModel())) {
			List<ComboProduct> converted = (List<ComboProduct>) conversionService
					.convert(source.getComboProductsForChildProductIdModel(),
							TypeDescriptor.forObject(source
									.getComboProductsForChildProductIdModel()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									ComboProduct.class));
			product.getComboProductsForChildProductId().addAll(converted);
		}
		if (CollectionUtils.isNotEmpty(source
				.getComboProductsForParentProductIdModel())) {
			List<ComboProduct> converted = (List<ComboProduct>) conversionService
					.convert(
							source.getComboProductsForParentProductIdModel(),
							TypeDescriptor.forObject(source
									.getComboProductsForParentProductIdModel()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									ComboProduct.class));
			product.getComboProductsForParentProductId().addAll(converted);
		}
		if (CollectionUtils.isNotEmpty(source.getProductNamesesModel())) {
			List<ProductNames> convert = (List<ProductNames>) conversionService
					.convert(source.getProductNamesesModel(), TypeDescriptor
							.forObject(source.getProductNamesesModel()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									ProductNames.class));
			product.getProductNameses().addAll(convert);
		}
		if (CollectionUtils.isNotEmpty(source.getProductImagesesModel())) {
			List<ProductImages> convert = (List<ProductImages>) conversionService
					.convert(source.getProductImagesesModel(), TypeDescriptor
							.forObject(source.getProductImagesesModel()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									ProductImages.class));

			product.getProductImageses().addAll(convert);
		}
		if (CollectionUtils.isNotEmpty(source.getProductFeaturesModel())) {
			List<ProductFeature> convert = (List<ProductFeature>) conversionService
					.convert(source.getProductFeaturesModel(), TypeDescriptor
							.forObject(source.getProductFeaturesModel()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									ProductFeature.class));

			product.getProductFeatures().addAll(convert);
		}
		if (CollectionUtils.isNotEmpty(source.getProductCategoriesModel())) {
			List<ProductCategory> convert = (List<ProductCategory>) conversionService
					.convert(source.getProductCategoriesModel(), TypeDescriptor
							.forObject(source.getProductCategoriesModel()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									ProductCategory.class));
			product.getProductCategories().addAll(convert);
		}
		return product;
	}

	@Autowired
	public void setProductFactory(final ObjectFactory<Product> productFactory) {
		this.productFactory = productFactory;
	}

}
