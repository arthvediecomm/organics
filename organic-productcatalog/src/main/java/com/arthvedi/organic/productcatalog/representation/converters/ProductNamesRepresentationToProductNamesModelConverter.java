/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.productcatalog.model.ProductNamesModel;
import com.arthvedi.organic.productcatalog.representation.siren.ProductNamesRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("productNamesRepresentationToProductNamesModelConverter")
public class ProductNamesRepresentationToProductNamesModelConverter extends PropertyCopyingConverter<ProductNamesRepresentation, ProductNamesModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public ProductNamesModel convert(final ProductNamesRepresentation source) {

        ProductNamesModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<ProductNamesModel> factory) {
        super.setFactory(factory);
    }

}
