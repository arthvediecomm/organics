package com.arthvedi.organic.productcatalog.service;

import java.util.List;

import com.arthvedi.organic.productcatalog.domain.ProductImages;
/*
*@Author varma
*/
public interface IProductImagesService {
	
	ProductImages create(ProductImages productImages);

	void deleteProductImages(String productImagesId);

	ProductImages getProductImages(String productImagesId);

	List<ProductImages> getAll();

	ProductImages updateProductImages(ProductImages productImages);
}
