package com.arthvedi.organic.productcatalog.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;
/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BrandImagesContext implements IBusinessDelegateContext {

	private String brandImagesId;
	private String all;
	
	public String getBrandImagesId() {
		return brandImagesId;
	}

	public void setBrandImagesId(String brandImagesId) {
		this.brandImagesId = brandImagesId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

}
