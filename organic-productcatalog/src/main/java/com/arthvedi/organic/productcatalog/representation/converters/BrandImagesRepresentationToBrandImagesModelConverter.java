/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.productcatalog.model.BrandImagesModel;
import com.arthvedi.organic.productcatalog.representation.siren.BrandImagesRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("brandImagesRepresentationToBrandImagesModelConverter")
public class BrandImagesRepresentationToBrandImagesModelConverter extends PropertyCopyingConverter<BrandImagesRepresentation, BrandImagesModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public BrandImagesModel convert(final BrandImagesRepresentation source) {

        BrandImagesModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<BrandImagesModel> factory) {
        super.setFactory(factory);
    }

}
