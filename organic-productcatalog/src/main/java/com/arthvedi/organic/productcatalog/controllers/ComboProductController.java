package com.arthvedi.organic.productcatalog.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.productcatalog.businessdelegate.context.ComboProductContext;
import com.arthvedi.organic.productcatalog.model.ComboProductModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/comboProduct", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class ComboProductController {

	private IBusinessDelegate<ComboProductModel, ComboProductContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<ComboProductContext> comboProductContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<ComboProductModel> createComboProduct(@RequestBody final ComboProductModel comboProductModel) {
		businessDelegate.create(comboProductModel);
		return new ResponseEntity<ComboProductModel>(comboProductModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<ComboProductModel> edit(@PathVariable(value = "id") final String comboProductId,
			@RequestBody final ComboProductModel comboProductModel) {

		businessDelegate.edit(keyBuilderFactory.getObject().withId(comboProductId), comboProductModel);
		return new ResponseEntity<ComboProductModel>(comboProductModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<ComboProductModel>>> getAll() {
		ComboProductContext comboProductContext = comboProductContextFactory.getObject();
		comboProductContext.setAll("all");
		Collection<ComboProductModel> comboProductModels = businessDelegate.getCollection(comboProductContext);
		IModelWrapper<Collection<ComboProductModel>> models = new CollectionModelWrapper<ComboProductModel>(
				ComboProductModel.class, comboProductModels);
		return new ResponseEntity<IModelWrapper<Collection<ComboProductModel>>>(models, HttpStatus.OK);
	}
	@RequestMapping(method = RequestMethod.GET, value = "/comboproductlist", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<ComboProductModel>>> getComboProductList() {
		ComboProductContext comboProductContext = comboProductContextFactory.getObject();
		comboProductContext.setComboProductList("comboProductList");
		Collection<ComboProductModel> comboProductModels = businessDelegate.getCollection(comboProductContext);
		IModelWrapper<Collection<ComboProductModel>> models = new CollectionModelWrapper<ComboProductModel>(
				ComboProductModel.class, comboProductModels);
		return new ResponseEntity<IModelWrapper<Collection<ComboProductModel>>>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<ComboProductModel> getComboProduct(@PathVariable(value = "id") final String comboProductId) {
		ComboProductContext comboProductContext = comboProductContextFactory.getObject();

		ComboProductModel model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(comboProductId),
				comboProductContext);
		return new ResponseEntity<ComboProductModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "comboProductBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<ComboProductModel, ComboProductContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setComboProductObjectFactory(final ObjectFactory<ComboProductContext> comboProductContextFactory) {
		this.comboProductContextFactory = comboProductContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
