package com.arthvedi.organic.productcatalog.service;

import java.util.List;

import com.arthvedi.organic.productcatalog.domain.Brand;
/*
*@Author varma
*/
public interface IBrandService {
	
	Brand create(Brand brand);

	void deleteBrand(String brandId);

	Brand getBrand(String brandId);

	List<Brand> getAll();

	Brand updateBrand(Brand brand);

	List<Brand> getBrandList();

	boolean checkBrandNameExist(String name);


}
