package com.arthvedi.organic.productcatalog.service;

import static com.arthvedi.organic.enums.Status.ACTIVE;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.productcatalog.domain.Category;
import com.arthvedi.organic.productcatalog.domain.CategoryFeature;
import com.arthvedi.organic.productcatalog.domain.CategoryImages;
import com.arthvedi.organic.productcatalog.domain.CategoryNames;
import com.arthvedi.organic.productcatalog.respository.CategoryRepository;

@Component
public class CategoryService implements ICategoryService {
	@Autowired
	NullAwareBeanUtilsBean nonNullBranUtils;
	@Autowired
	private CategoryRepository categoryRepository;
	@Autowired
	private ICategoryFeatureService categoryFeatureService;
	@Autowired
	private ICategoryImagesService categoryImagesService;
	@Autowired
	private ICategoryNamesService categoryNamesService;

	@Override
	public Category create(Category category) {
		category.setUserCreated("varma");
		category.setCreatedDate(new LocalDateTime());
		category.setStatus(ACTIVE.name());
		category = categoryRepository.save(category);
		if (category.getId() != null
				&& CollectionUtils.isNotEmpty(category.getCategoryImageses())) {
			category.setCategoryImageses(addCategoryImages(category,
					category.getCategoryImageses()));
		}
		if (category.getId() != null
				&& CollectionUtils.isNotEmpty(category.getCategoryFeatures())) {
			category.setCategoryFeatures(addCategoryFeature(category,

			category.getCategoryFeatures()));
		}
		if (category.getId() != null
				&& CollectionUtils.isNotEmpty(category.getCategoryNameses())) {
			category.setCategoryNameses(addCategoryNames(category,
					category.getCategoryNameses()));

		}

		return category;
	}

	@Transactional
	private Set<CategoryNames> addCategoryNames(Category category,
			Set<CategoryNames> categoryNameses) {
		Set<CategoryNames> categoryNames = new HashSet<CategoryNames>();
		for (CategoryNames catNames : categoryNameses) {
			CategoryNames categoryNam = catNames;
			categoryNam.setCategory(category);
			categoryNam = categoryNamesService.create(categoryNam);
			categoryNames.add(categoryNam);
		}
		return categoryNames;
	}

	@Transactional
	private Set<CategoryFeature> addCategoryFeature(Category category,
			Set<CategoryFeature> categoryFeatures) {
		Set<CategoryFeature> categoryFea = new HashSet<CategoryFeature>();
		for (CategoryFeature categoryFeature : categoryFeatures) {
			CategoryFeature catFeatur = categoryFeature;
			catFeatur.setCategory(category);
			catFeatur = categoryFeatureService.create(catFeatur);
			categoryFea.add(catFeatur);
		}
		return categoryFea;
	}

	@Transactional
	private Set<CategoryImages> addCategoryImages(Category category,
			Set<CategoryImages> categoryImageses) {
		Set<CategoryImages> categoryImages = new HashSet<CategoryImages>();
		for (CategoryImages categoryImage : categoryImageses) {
			CategoryImages catImage = categoryImage;
			catImage.setCategory(category);
			catImage = categoryImagesService.create(catImage);
			categoryImages.add(catImage);

		}
		return categoryImages;
	}

	@Override
	public void deleteCategory(String categoryId) {

	}

	@Override
	public Category getCategory(String categoryId) {
		return categoryRepository.findOne(categoryId);
	}

	@Override
	public List<Category> getAll() {
		return null;
	}

	@Override
	public Category updateCategory(Category category) {
		Category categorys = categoryRepository.findOne(category.getId());
		try {
			nonNullBranUtils.copyProperties(categorys, category);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

		categorys.setUserModified("admin");
		categorys.setModifiedDate(new LocalDateTime());
		category = categoryRepository.save(categorys);
		return category;
	}

	@Override
	public List<Category> getCategoryList() {
		List<Category> categories = (List<Category>) categoryRepository
				.findAll();
		List<Category> categoriess = new ArrayList<Category>();
		for (Category category : categories) {
			Category catego = new Category();
			catego.setCategoryName(category.getCategoryName());
			catego.setDescription(category.getDescription());
			catego.setCategory(category.getCategory());
			catego.setStatus(category.getStatus());
			catego.setLevel(category.getLevel());
			catego.setId(category.getId());
			categoriess.add(catego);
		}

		return categoriess;
	}

	@Override
	public List<Category> getDistinctCategoriesByDiffProducts(
			List<String> childProductIds) {
		return null;
	}

	/*
	 * @Override public boolean getCategoryByCategoryName(String categoryName) {
	 * return categoryRepository.findByCategoryName(categoryName) ; }
	 */
	@Override
	public boolean checkCategoryNameExist(String categoryName) {
		return categoryRepository.findByCategoryName(categoryName) != null;
	}

}
