package com.arthvedi.organic.productcatalog.model.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.organic.productcatalog.domain.Brand;
import com.arthvedi.organic.productcatalog.model.BrandImagesModel;
import com.arthvedi.organic.productcatalog.model.BrandModel;

@Component("brandToBrandModelConverter")
public class BrandToBrandModelConverter implements Converter<Brand, BrandModel> {
	@Autowired
	private ObjectFactory<BrandModel> brandModelFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public BrandModel convert(final Brand source) {
		BrandModel brandModel = brandModelFactory.getObject();
		BeanUtils.copyProperties(source, brandModel);
		if (CollectionUtils.isNotEmpty(source.getBrandImageses())) {
			List<BrandImagesModel> convert = (List<BrandImagesModel>) conversionService
					.convert(source.getBrandImageses(), TypeDescriptor
							.forObject(source.getBrandImageses()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									BrandImagesModel.class));
			brandModel.getBrandImagesModels().addAll(convert);
		}

		return brandModel;
	}

	@Autowired
	public void setBrandModelFactory(
			final ObjectFactory<BrandModel> brandModelFactory) {
		this.brandModelFactory = brandModelFactory;
	}
}
