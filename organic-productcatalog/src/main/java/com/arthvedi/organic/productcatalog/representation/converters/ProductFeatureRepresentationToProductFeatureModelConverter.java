/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.productcatalog.model.ProductFeatureModel;
import com.arthvedi.organic.productcatalog.representation.siren.ProductFeatureRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("productFeatureRepresentationToProductFeatureModelConverter")
public class ProductFeatureRepresentationToProductFeatureModelConverter extends PropertyCopyingConverter<ProductFeatureRepresentation, ProductFeatureModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public ProductFeatureModel convert(final ProductFeatureRepresentation source) {

        ProductFeatureModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<ProductFeatureModel> factory) {
        super.setFactory(factory);
    }

}
