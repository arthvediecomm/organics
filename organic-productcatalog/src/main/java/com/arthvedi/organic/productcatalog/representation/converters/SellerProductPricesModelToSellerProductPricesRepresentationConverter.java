/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.productcatalog.model.SellerProductPricesModel;
import com.arthvedi.organic.productcatalog.representation.siren.SellerProductPricesRepresentation;

/**
 * @author varma
 *
 */
@Component("sellerProductPricesModelToSellerProductPricesRepresentationConverter")
public class SellerProductPricesModelToSellerProductPricesRepresentationConverter extends PropertyCopyingConverter<SellerProductPricesModel, SellerProductPricesRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public SellerProductPricesRepresentation convert(final SellerProductPricesModel source) {

        SellerProductPricesRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<SellerProductPricesRepresentation> factory) {
        super.setFactory(factory);
    }

}
