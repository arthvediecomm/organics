package com.arthvedi.organic.productcatalog.service;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.productcatalog.domain.CategoryNames;
import com.arthvedi.organic.productcatalog.domain.Seo;
import com.arthvedi.organic.productcatalog.respository.CategoryNamesRepository;

/*
 *@Author varma
 */
@Component
public class CategoryNamesService implements ICategoryNamesService {
	@Autowired
	NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private CategoryNamesRepository categoryNamesRepository;
	@Autowired
	private ISeoService seoService;

	@Override
	public CategoryNames create(CategoryNames categoryNames) {
		categoryNames.setUserCreated("varma");
		categoryNames.setCreatedDate(new LocalDateTime());
		if (categoryNames.getSeo() != null) {
			Seo seo = seoService.create(categoryNames.getSeo());
			categoryNames.setSeo(seo);
		}

		categoryNames = categoryNamesRepository.save(categoryNames);

		return categoryNames;
	}

	@Override
	public void deleteCategoryNames(String categoryNamesId) {

	}

	@Override
	public CategoryNames getCategoryNames(String categoryNamesId) {
		return categoryNamesRepository.findOne(categoryNamesId);
	}

	@Override
	public List<CategoryNames> getAll() {
		return null;
	}

	@Override
	public CategoryNames updateCategoryNames(CategoryNames categoryNames) {
		CategoryNames categoryNamess = categoryNamesRepository
				.findOne(categoryNames.getId());
		try {
			nonNullBeanUtils.copyProperties(categoryNamess, categoryNames);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		categoryNamess.setUserModified("admin");
		categoryNamess.setModifiedDate(new LocalDateTime());
		categoryNames = categoryNamesRepository.save(categoryNamess);
		return categoryNames;
	}

	@Override
	public List<CategoryNames> getCategoryNamesList() {
		List<CategoryNames> categoryNames = (List<CategoryNames>) categoryNamesRepository
				.findAll();
		List<CategoryNames> categoryNamess = new ArrayList<CategoryNames>();
		for (CategoryNames catnams : categoryNames) {
			CategoryNames categoryName = new CategoryNames();
			categoryName.setCategory(catnams.getCategory());
			categoryName.setName(catnams.getName());

			categoryNamess.add(categoryName);
		}
		return categoryNamess;
	}

}
