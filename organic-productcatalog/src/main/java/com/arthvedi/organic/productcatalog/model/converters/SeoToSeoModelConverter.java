package com.arthvedi.organic.productcatalog.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.productcatalog.domain.Seo;
import com.arthvedi.organic.productcatalog.model.SeoModel;

@Component("seoToSeoModelConverter")
public class SeoToSeoModelConverter
        implements Converter<Seo, SeoModel> {
    @Autowired
    private ObjectFactory<SeoModel> seoModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public SeoModel convert(final Seo source) {
        SeoModel seoModel = seoModelFactory.getObject();
        BeanUtils.copyProperties(source, seoModel);

        return seoModel;
    }

    @Autowired
    public void setSeoModelFactory(
            final ObjectFactory<SeoModel> seoModelFactory) {
        this.seoModelFactory = seoModelFactory;
    }
}
