/**
 *
 */
package com.arthvedi.organic.productcatalog.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.productcatalog.domain.Category;
import com.arthvedi.organic.productcatalog.domain.CategoryImages;
import com.arthvedi.organic.productcatalog.model.CategoryImagesModel;

@Component("categoryImagesModelToCategoryImagesConverter")
public class CategoryImagesModelToCategoryImagesConverter implements
		Converter<CategoryImagesModel, CategoryImages> {
	@Autowired
	private ObjectFactory<CategoryImages> categoryImagesFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public CategoryImages convert(final CategoryImagesModel source) {
		CategoryImages categoryImages = categoryImagesFactory.getObject();
		BeanUtils.copyProperties(source, categoryImages);
		if (source.getCategoryId() != null) {
			Category category = new Category();
			category.setId(source.getCategoryId());
			categoryImages.setCategory(category);
		}

		return categoryImages;
	}

	@Autowired
	public void setCategoryImagesFactory(
			final ObjectFactory<CategoryImages> categoryImagesFactory) {
		this.categoryImagesFactory = categoryImagesFactory;
	}

}
