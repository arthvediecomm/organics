package com.arthvedi.organic.productcatalog.service;

import java.util.List;

import com.arthvedi.organic.productcatalog.domain.BrandImages;
/*
*@Author varma
*/
public interface IBrandImagesService {
	
	BrandImages create(BrandImages brandImages);

	void deleteBrandImages(String brandImagesId);

	BrandImages getBrandImages(String brandImagesId);

	List<BrandImages> getAll();

	BrandImages updateBrandImages(BrandImages brandImages);
}
