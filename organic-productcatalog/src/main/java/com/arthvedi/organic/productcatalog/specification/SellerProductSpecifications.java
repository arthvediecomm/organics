package com.arthvedi.organic.productcatalog.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.arthvedi.organic.productcatalog.domain.SellerProduct;

public class SellerProductSpecifications {

	public static Specification<SellerProduct> bySellerBranch(String sellerBranchId) {
		return new Specification<SellerProduct>() {

			@Override
			public Predicate toPredicate(Root<SellerProduct> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.get("sellerBranch").get("id"), sellerBranchId);
			}
		};
	}
	public static Specification<SellerProduct> byCategory(String categoryId) {
		return new Specification<SellerProduct>() {

			@Override
			public Predicate toPredicate(Root<SellerProduct> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.join("product").join("productCategories").join("category").get("id"), categoryId);
			}
		};
	}
	public static Specification<SellerProduct> byStatus(String status) {
		return new Specification<SellerProduct>() {
			@Override
			public Predicate toPredicate(Root<SellerProduct> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.get("status"), status);
			}
		};

	}
	
	
	public static Specification<SellerProduct> byZone(String zoneId){
		return new Specification<SellerProduct>(){

			@Override
			public Predicate toPredicate(Root<SellerProduct> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				// TODO Auto-generated method stub
				return cb.equal(root.join("sellerBranch").join("sellerBranchZones").get("zone").get("id"), zoneId);
			}
			
		};
	}
	public static Specification<SellerProduct> bySearchingProductName(String searchQuery){
		
		return new Specification<SellerProduct>(){

			@Override
			public Predicate toPredicate(Root<SellerProduct> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.like(root.get("sellerProductName"),"%"+searchQuery+"%");
			}
			
		};
	}
	public static Specification<SellerProduct> byProduct(String productId){
		return new Specification<SellerProduct>(){

			@Override
			public Predicate toPredicate(Root<SellerProduct> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				// TODO Auto-generated method stub
				return cb.equal(root.get("product").get("id"),productId);
			}
			
		};
	}

}
