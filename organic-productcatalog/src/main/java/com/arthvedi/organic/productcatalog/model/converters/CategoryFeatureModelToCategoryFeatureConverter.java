/**
 *
 */
package com.arthvedi.organic.productcatalog.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.productcatalog.domain.Category;
import com.arthvedi.organic.productcatalog.domain.CategoryFeature;
import com.arthvedi.organic.productcatalog.domain.Feature;
import com.arthvedi.organic.productcatalog.model.CategoryFeatureModel;

/**
 * @author Jay
 *
 */
@Component("categoryFeatureModelToCategoryFeatureConverter")
public class CategoryFeatureModelToCategoryFeatureConverter implements
		Converter<CategoryFeatureModel, CategoryFeature> {
	@Autowired
	private ObjectFactory<CategoryFeature> categoryFeatureFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public CategoryFeature convert(final CategoryFeatureModel source) {
		CategoryFeature categoryFeature = new CategoryFeature();
		BeanUtils.copyProperties(source, categoryFeature);
		if (source.getCategoryId() != null) {
			Category category = new Category();
			category.setId(source.getCategoryId());
			categoryFeature.setCategory(category);
		}
		if (source.getFeatureId() != null) {
			Feature feature = new Feature();
			feature.setId(source.getFeatureId());
			categoryFeature.setFeature(feature);
		}

		return categoryFeature;
	}

	@Autowired
	public void setCategoryFeatureFactory(
			final ObjectFactory<CategoryFeature> categoryFeatureFactory) {
		this.categoryFeatureFactory = categoryFeatureFactory;
	}

}
