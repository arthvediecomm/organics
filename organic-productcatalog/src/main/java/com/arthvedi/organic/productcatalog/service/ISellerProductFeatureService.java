package com.arthvedi.organic.productcatalog.service;

import java.util.List;

import com.arthvedi.organic.productcatalog.domain.SellerProductFeature;

/*
 *@Author varma
 */
public interface ISellerProductFeatureService {

	SellerProductFeature create(SellerProductFeature sellerProductFeature);

	SellerProductFeature getSellerProductFeature(String sellerProductFeatureId);

	SellerProductFeature updateSellerProductFeature(
			SellerProductFeature sellerProductFeature);

	List<SellerProductFeature> getSellerProductFeatureBySellerProduct(
			String sellerProductId);
}
