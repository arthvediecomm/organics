package com.arthvedi.organic.productcatalog.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;

/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CategoryFeatureContext implements IBusinessDelegateContext {

	private String categoryFeatureId;
	private String all;
	private String categoryFeatureList;

	public String getCategoryFeatureList() {
		return categoryFeatureList;
	}

	public void setCategoryFeatureList(String categoryFeatureList) {
		this.categoryFeatureList = categoryFeatureList;
	}

	public String getCategoryFeatureId() {
		return categoryFeatureId;
	}

	public void setCategoryFeatureId(String categoryFeatureId) {
		this.categoryFeatureId = categoryFeatureId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

}
