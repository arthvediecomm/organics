package com.arthvedi.organic.html.enums;

public enum HtmlPages {
	PRODUCTLISTPAGE("productlistpage"),
	PRODUCTPAGE("productpage");

	private String url;

	HtmlPages(String url) {
		this.url = url;
	}
	
	 public String  getUrl(){
		return url;
	}

}
