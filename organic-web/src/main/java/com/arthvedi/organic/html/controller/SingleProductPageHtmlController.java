/**
 *
 *//*
package com.arthvedi.organic.html.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.organic.html.enums.HtmlPages;
import com.arthvedi.organic.html.model.SingleProductPageModel;

*//**
 * @author venky1
 *
 *//*
@ImportResource("classpath:spring-thymeleaf.xml")
@Controller
public class SingleProductPageHtmlController {

    private IBusinessDelegate<SingleProductPageModel, SingleProductPageContext, IKeyBuilder<String>, String> businessDelegate;
    private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
    private ObjectFactory<SingleProductPageContext> singleProductPageContextFactory;

    *//**
     * @param req
     * @param string
     * @return
     *//*
    private String getCookie(final HttpServletRequest request, final String cookieName) {

        Cookie[] cookie = request.getCookies();
        String cookieValue = null;
        if (cookie != null) {

            for (Cookie c : cookie) {

                if (c.getName().equals(cookieName)) {
                    cookieValue = c.getValue();
                }

            }

        }
        return cookieValue;
    }

    @RequestMapping(value = "/item/{productName}", method = RequestMethod.GET)
    public String getSingleProductPage(final HttpServletRequest req, final Model userInterfaceModel,
            @PathVariable("productName") final String productName, @RequestParam(value = "id") final String productId,
            @RequestParam(defaultValue = "N-A", value = "bid") final String brandId) {
        SingleProductPageModel singleProductPageModel = new SingleProductPageModel();
        String languageId = getCookie(req, "lid");
        String cityId = getCookie(req, "cid");
        SingleProductPageContext context = singleProductPageContextFactory.getObject();
        context.setBrandId(brandId);
        context.setCityId(cityId);
        context.setLanguageId(languageId);
        singleProductPageModel = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(productId), context);
        userInterfaceModel.addAttribute("singleProductPage", singleProductPageModel);
        return HtmlPages.PRODUCTPAGE.getUrl();
    }

}
*/