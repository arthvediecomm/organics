/**
 *
 *//*
package com.arthvedi.organic.html.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.organic.html.enums.HtmlPages;
import com.arthvedi.organic.html.model.ListPageModel;
import com.arthvedi.organic.productcatalog.html.context.ListPageContext;

*//**
 * @author venky1
 *
 *//*
@ImportResource("classpath:spring-thymeleaf.xml")
@Controller
public class ListPageHtmlController {
    private IBusinessDelegate<ListPageModel, ListPageContext, IKeyBuilder<String>, String> businessDelegate;
    private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
    private ObjectFactory<ListPageContext> listPageContextFactory;

    public String getCookie(final HttpServletRequest req, final String cookieName) {

        Cookie[] cookies = req.getCookies();
        String cookieValue = null;
        if (cookies != null) {
            for (Cookie c : cookies) {
                if (c.getName().equals(cookieName)) {
                    cookieValue = c.getValue();
                }
            }
        }
        return cookieValue;
    }

    @RequestMapping(value = "/items/category/{categoryName}", method = RequestMethod.GET)
    public String getProductsPageByCriteria(final HttpServletRequest req, final Model uiModel,
            @PathVariable("categoryName") final String categoryName,
            @RequestParam(defaultValue = "N-A", value = "id") final String categoryId,
            @RequestParam(defaultValue = "N-A", value = "scid") final List<String> subCategoryIds,
            @RequestParam(defaultValue = "N-A", value = "fid") final List<String> featureIds,
            @RequestParam(defaultValue = "N-A", value = "bid") final List<String> brandIds,
            @RequestParam(defaultValue = "N-A", value = "sid") final List<String> sellerBranchIds,
            @RequestParam(defaultValue = "1", value = "page", required = false) final int page,
            @RequestParam(defaultValue = "16", value = "pageSize", required = false) final int pageSize,
            @RequestParam(defaultValue = "item", value = "sort", required = false) final String sort,
            @RequestParam(defaultValue = "asc", value = "type", required = false) final String type) {
        ListPageModel model = new ListPageModel();
        String languageId = getCookie(req, "lid");
        ListPageContext context = listPageContextFactory.getObject();
        context.setCategoryId(categoryId);
        context.setLanguageId(languageId);
        context.setSubCategoryIds(subCategoryIds);
        context.setBrandIds(brandIds);
        context.setFeatureIds(featureIds);
        context.setSellerBranchIds(sellerBranchIds);
        context.setSortBy(sort);
        context.setSortType(type);
        context.setPageNo(page);
        context.setPageSize(pageSize);
        model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(categoryId), context);
        uiModel.addAttribute("listPageModel", model);
        return HtmlPages.PRODUCTLISTPAGE.getUrl();
    }

    *//**
     * @param businessDelegate
     *//*
    @Resource(name = "listPageBusinessDelegate")
    public void setBusinessDelegate(final IBusinessDelegate<ListPageModel, ListPageContext, IKeyBuilder<String>, String> businessDelegate) {
        this.businessDelegate = businessDelegate;
    }

    @Autowired
    public void setFeatureObjectFactory(final ObjectFactory<ListPageContext> listPageContextFactory) {
        this.listPageContextFactory = listPageContextFactory;
    }

    *//**
     * @param factory
     *//*
    @Resource
    public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
        keyBuilderFactory = factory;
    }
}
*/