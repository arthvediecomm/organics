package com.arthvedi.organic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.convert.support.GenericConversionService;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * Class to register the beans to the application context and to enable auto
 * configuration
 *
 * @author rbuddepu
 *
 */
@Configuration
@ComponentScan(basePackages = { "com.arthvedi" })
@EnableAutoConfiguration
@EnableScheduling
public class Application extends SpringBootServletInitializer {

	public final static String CONVERSION_SERVICE_BEANNAME = "conversionService";
	private static Class<Application> applicationClass = Application.class;

	public static void main(final String[] args) {
		SpringApplication.run(applicationClass, args);
	}

	@Override
	protected SpringApplicationBuilder configure(
			final SpringApplicationBuilder application) {
		return application.sources(applicationClass);
	}

	/**
	 * Creating the Spring Conversion service bean
	 */
	@Bean(name = CONVERSION_SERVICE_BEANNAME)
	@Primary
	public GenericConversionService conversionService() {
		return new GenericConversionService();
	}

	/**
	 * create the Mapper for Jackson
	 */
	@Bean
	@Primary
	public ObjectMapper jacksonObjectMapper() {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper;
	}

	/**
	 * create the Mapper for siren
	 */
	@Bean
	public ObjectMapper sirenObjectMapper() {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS,
				false);
		return objectMapper;
	}
	/*
	 * @Bean AuthenticationManager
	 * authenticationManager(AuthenticationManagerBuilder auth) { return
	 * auth.getOrBuild(); }
	 */
}
