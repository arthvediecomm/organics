/**
 *
 */
package com.arthvedi.organic.requests.representation.siren;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.requests.model.RequestAttributesModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.annotations.Siren4JSubEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("requestAttributesRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "requestAttributes", uri = "/requestAttributess/{id}")
@Representation(RequestAttributesModel.class)
public class RequestAttributesRepresentation extends BaseResource {
	private String id;
	private String requestTypeId;
	private String requestTypeAttributeName;
	private String requestTypeAttributeDatatype;
	private String status;
	@Siren4JSubEntity
	private List<RequestTypeAttributeValueRepresentation> requestTypeAttributeValueRep = new ArrayList<RequestTypeAttributeValueRepresentation>(
			0);

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRequestTypeId() {
		return requestTypeId;
	}

	public void setRequestTypeId(String requestTypeId) {
		this.requestTypeId = requestTypeId;
	}

	public String getRequestTypeAttributeName() {
		return requestTypeAttributeName;
	}

	public void setRequestTypeAttributeName(String requestTypeAttributeName) {
		this.requestTypeAttributeName = requestTypeAttributeName;
	}

	public String getRequestTypeAttributeDatatype() {
		return requestTypeAttributeDatatype;
	}

	public void setRequestTypeAttributeDatatype(String requestTypeAttributeDatatype) {
		this.requestTypeAttributeDatatype = requestTypeAttributeDatatype;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<RequestTypeAttributeValueRepresentation> getRequestTypeAttributeValueRep() {
		return requestTypeAttributeValueRep;
	}

	public void setRequestTypeAttributeValueRep(
			List<RequestTypeAttributeValueRepresentation> requestTypeAttributeValueRep) {
		this.requestTypeAttributeValueRep = requestTypeAttributeValueRep;
	}

}
