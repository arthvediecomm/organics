/**
 *
 */
package com.arthvedi.organic.requests.representation.siren;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.requests.model.RequestTypeAttributeValueModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.CollectionResource;

/**
 * @author arthvedi
 *
 */
@Component("requestTypeAttributeValueRepresentations")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "requestTypeAttributeValues", uri = "/requestTypeAttributeValues")
@Representation(RequestTypeAttributeValueModel.class)
public class RequestTypeAttributeValueRepresentations extends CollectionResource<RequestTypeAttributeValueRepresentation> {

}
