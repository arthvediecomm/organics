/**
 *
 */
package com.arthvedi.organic.requests.representation.siren;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.requests.model.RequestTypeAttributeValueModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("requestTypeAttributeValueRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "requestTypeAttributeValue", uri = "/requestTypeAttributeValues/{id}")
@Representation(RequestTypeAttributeValueModel.class)
public class RequestTypeAttributeValueRepresentation extends BaseResource {
	private String id;
	private String requestTypeAttributeId;
	private String requestTypeAttributeValue;
	private String status;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRequestTypeAttributeId() {
		return requestTypeAttributeId;
	}

	public void setRequestTypeAttributeId(String requestTypeAttributeId) {
		this.requestTypeAttributeId = requestTypeAttributeId;
	}

	public String getRequestTypeAttributeValue() {
		return requestTypeAttributeValue;
	}

	public void setRequestTypeAttributeValue(String requestTypeAttributeValue) {
		this.requestTypeAttributeValue = requestTypeAttributeValue;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
