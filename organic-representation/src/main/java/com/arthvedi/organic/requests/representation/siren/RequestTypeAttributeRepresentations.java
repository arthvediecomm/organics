/**
 *
 */
package com.arthvedi.organic.requests.representation.siren;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.requests.model.RequestTypeAttributeModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.CollectionResource;

/**
 * @author arthvedi
 *
 */
@Component("requestTypeAttributeRepresentations")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "requestTypeAttributes", uri = "/requestTypeAttributes")
@Representation(RequestTypeAttributeModel.class)
public class RequestTypeAttributeRepresentations extends CollectionResource<RequestTypeAttributeRepresentation> {

}
