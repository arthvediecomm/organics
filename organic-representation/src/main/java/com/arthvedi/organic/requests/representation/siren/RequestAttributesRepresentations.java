/**
 *
 */
package com.arthvedi.organic.requests.representation.siren;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.requests.model.RequestAttributesModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.CollectionResource;

/**
 * @author arthvedi
 *
 */
@Component("requestAttributesRepresentations")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "requestAttributess", uri = "/requestAttributess")
@Representation(RequestAttributesModel.class)
public class RequestAttributesRepresentations extends CollectionResource<RequestAttributesRepresentation> {

}
