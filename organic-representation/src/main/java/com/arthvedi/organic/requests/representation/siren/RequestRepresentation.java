/**
 *
 */
package com.arthvedi.organic.requests.representation.siren;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.requests.model.RequestModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.annotations.Siren4JSubEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("requestRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "request", uri = "/requests/{id}")
@Representation(RequestModel.class)
public class RequestRepresentation extends BaseResource {
	private String id;
	private String ecommUserId;
	private String requestTypeName;
	private String requestDetails;
	private String userType;
	private String priority;
	private String status;
	private String requestMessage;
	@Siren4JSubEntity
	private List<RequestAttributesRepresentation> requestAttributesRep = new ArrayList<RequestAttributesRepresentation>(
			0);
	@Siren4JSubEntity
	private List<RequestStatusCodesRepresentation> requestStatusCodesRep = new ArrayList<RequestStatusCodesRepresentation>(
			0);

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
	public String getEcommUserId() {
		return ecommUserId;
	}

	public void setEcommUserId(String ecommUserId) {
		this.ecommUserId = ecommUserId;
	}

	public String getRequestTypeName() {
		return requestTypeName;
	}

	public void setRequestTypeName(String requestTypeName) {
		this.requestTypeName = requestTypeName;
	}

	public String getRequestDetails() {
		return requestDetails;
	}

	public void setRequestDetails(String requestDetails) {
		this.requestDetails = requestDetails;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRequestMessage() {
		return requestMessage;
	}

	public void setRequestMessage(String requestMessage) {
		this.requestMessage = requestMessage;
	}

	public List<RequestAttributesRepresentation> getRequestAttributesRep() {
		return requestAttributesRep;
	}

	public void setRequestAttributesRep(List<RequestAttributesRepresentation> requestAttributesRep) {
		this.requestAttributesRep = requestAttributesRep;
	}

	public List<RequestStatusCodesRepresentation> getRequestStatusCodesRep() {
		return requestStatusCodesRep;
	}

	public void setRequestStatusCodesRep(List<RequestStatusCodesRepresentation> requestStatusCodesRep) {
		this.requestStatusCodesRep = requestStatusCodesRep;
	}

}
