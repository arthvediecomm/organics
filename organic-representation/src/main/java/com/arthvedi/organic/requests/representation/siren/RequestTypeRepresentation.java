/**
 *
 */
package com.arthvedi.organic.requests.representation.siren;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.requests.model.RequestTypeModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.annotations.Siren4JSubEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("requestTypeRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "requestType", uri = "/requestTypes/{id}")
@Representation(RequestTypeModel.class)
public class RequestTypeRepresentation extends BaseResource {
	private String id;
	private String name;
	private String priority;
	private String status;
	@Siren4JSubEntity
	private List<RequestTypeAttributeRepresentation> requestTypeAttributeRep = new ArrayList<RequestTypeAttributeRepresentation>(
			0);

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<RequestTypeAttributeRepresentation> getRequestTypeAttributeRep() {
		return requestTypeAttributeRep;
	}

	public void setRequestTypeAttributeRep(List<RequestTypeAttributeRepresentation> requestTypeAttributeRep) {
		this.requestTypeAttributeRep = requestTypeAttributeRep;
	}

}
