/**
 *
 */
package com.arthvedi.organic.commons.representation.siren;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.commons.model.LanguageModel;
import com.arthvedi.organic.productcatalog.representation.siren.CategoryNamesRepresentation;
import com.arthvedi.organic.productcatalog.representation.siren.ProductNamesRepresentation;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.annotations.Siren4JSubEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("languageRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "language", uri = "/languages/{id}")
@Representation(LanguageModel.class)
public class LanguageRepresentation extends BaseResource {
	private String id;
	private String status;
	private String name;
	private String statusMessage;
	@Siren4JSubEntity
	private List<CategoryNamesRepresentation> categoryNamesesRepresentation = new ArrayList<CategoryNamesRepresentation>(
			0);
	@Siren4JSubEntity
	private List<CityRepresentation> citiesRepresentation = new ArrayList<CityRepresentation>(
			0);
	@Siren4JSubEntity
	private List<ProductNamesRepresentation> productNamesesRepresentation = new ArrayList<ProductNamesRepresentation>(
			0);

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<CategoryNamesRepresentation> getCategoryNamesesRepresentation() {
		return categoryNamesesRepresentation;
	}

	public void setCategoryNamesesRepresentation(
			List<CategoryNamesRepresentation> categoryNamesesRepresentation) {
		this.categoryNamesesRepresentation = categoryNamesesRepresentation;
	}

	public List<CityRepresentation> getCitiesRepresentation() {
		return citiesRepresentation;
	}

	public void setCitiesRepresentation(
			List<CityRepresentation> citiesRepresentation) {
		this.citiesRepresentation = citiesRepresentation;
	}

	public List<ProductNamesRepresentation> getProductNamesesRepresentation() {
		return productNamesesRepresentation;
	}

	public void setProductNamesesRepresentation(
			List<ProductNamesRepresentation> productNamesesRepresentation) {
		this.productNamesesRepresentation = productNamesesRepresentation;
	}

}
