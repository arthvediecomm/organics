/**
 *
 */
package com.arthvedi.organic.commons.representation.siren;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.commons.model.ReturnPolicyModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("returnPolicyRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "returnPolicy", uri = "/returnPolicys/{id}")
@Representation(ReturnPolicyModel.class)
public class ReturnPolicyRepresentation extends BaseResource {
	private String id;
	private String refundAmount;
	private String refundDescription;
	private String taxesInclude;
	private String chargesInclude;
	private String noOfDays;

	public String getNoOfDays() {
		return noOfDays;
	}

	public void setNoOfDays(String noOfDays) {
		this.noOfDays = noOfDays;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(String refundAmount) {
		this.refundAmount = refundAmount;
	}

	public String getRefundDescription() {
		return refundDescription;
	}

	public void setRefundDescription(String refundDescription) {
		this.refundDescription = refundDescription;
	}

	public String getTaxesInclude() {
		return taxesInclude;
	}

	public void setTaxesInclude(String taxesInclude) {
		this.taxesInclude = taxesInclude;
	}

	public String getChargesInclude() {
		return chargesInclude;
	}

	public void setChargesInclude(String chargesInclude) {
		this.chargesInclude = chargesInclude;
	}

}
