/**
 *
 */
package com.arthvedi.organic.commons.representation.siren;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.commons.model.OrganicRoleModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("organicRoleRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "organicRole", uri = "/organicRoles/{id}")
@Representation(OrganicRoleModel.class)
public class OrganicRoleRepresentation extends BaseResource {
	private String id;
	private String roleName;
	private String status;

	private List<OrganicUserRepresentation> organicUsersRepresentation = new ArrayList<OrganicUserRepresentation>(0);

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<OrganicUserRepresentation> getOrganicUsersRepresentation() {
		return organicUsersRepresentation;
	}

	public void setOrganicUsersRepresentation(List<OrganicUserRepresentation> organicUsersRepresentation) {
		this.organicUsersRepresentation = organicUsersRepresentation;
	}

}
