/**
 *
 */
package com.arthvedi.organic.commons.representation.siren;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.commons.model.ZoneModel;
import com.arthvedi.organic.seller.representation.siren.SellerBranchZoneRepresentation;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.annotations.Siren4JSubEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("zoneRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "zone", uri = "/zones/{id}")
@Representation(ZoneModel.class)
public class ZoneRepresentation extends BaseResource {
	private String id;
	private String cityId;
	private String zoneName;
	private String geographicalZoneName;
	private String status;
	private String description;
	private String statusMessage;

	@Siren4JSubEntity
	private List<SellerBranchZoneRepresentation> sellerBranchZoneRepresentations = new ArrayList<SellerBranchZoneRepresentation>(
			0);
	@Siren4JSubEntity
	private List<ZoneAreaRepresentation> zoneAreaRepresentations = new ArrayList<ZoneAreaRepresentation>(
			0);

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	public String getGeographicalZoneName() {
		return geographicalZoneName;
	}

	public void setGeographicalZoneName(String geographicalZoneName) {
		this.geographicalZoneName = geographicalZoneName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<SellerBranchZoneRepresentation> getSellerBranchZoneRepresentations() {
		return sellerBranchZoneRepresentations;
	}

	public void setSellerBranchZoneRepresentations(
			List<SellerBranchZoneRepresentation> sellerBranchZoneRepresentations) {
		this.sellerBranchZoneRepresentations = sellerBranchZoneRepresentations;
	}

	public List<ZoneAreaRepresentation> getZoneAreaRepresentations() {
		return zoneAreaRepresentations;
	}

	public void setZoneAreaRepresentations(
			List<ZoneAreaRepresentation> zoneAreaRepresentations) {
		this.zoneAreaRepresentations = zoneAreaRepresentations;
	}

}
