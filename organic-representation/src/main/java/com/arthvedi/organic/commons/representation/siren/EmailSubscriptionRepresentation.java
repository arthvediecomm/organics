/**
 *
 */
package com.arthvedi.organic.commons.representation.siren;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.commons.model.EmailSubscriptionModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("emailSubscriptionRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "emailSubscription", uri = "/emailSubscriptions/{id}")
@Representation(EmailSubscriptionModel.class)
public class EmailSubscriptionRepresentation extends BaseResource {
	private String id;
	private String userEmail;
	private String emailSubscriptioncol;
	private String subscriptionStatus;
	private String statusMessage;

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getEmailSubscriptioncol() {
		return emailSubscriptioncol;
	}

	public void setEmailSubscriptioncol(String emailSubscriptioncol) {
		this.emailSubscriptioncol = emailSubscriptioncol;
	}

	public String getSubscriptionStatus() {
		return subscriptionStatus;
	}

	public void setSubscriptionStatus(String subscriptionStatus) {
		this.subscriptionStatus = subscriptionStatus;
	}

}
