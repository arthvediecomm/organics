/**
 *
 */
package com.arthvedi.organic.commons.representation.siren;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.commons.model.AddressModel;
import com.arthvedi.organic.seller.representation.siren.SellerBranchAddressRepresentation;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.annotations.Siren4JSubEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("addressRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "address", uri = "/addresss/{id}")
@Representation(AddressModel.class)
public class AddressRepresentation extends BaseResource {
	private String id;
	private String line1;
	private String town;
	private String city;
	private String district;
	private String contactPerson;
	private String mobileNo;
	private String zipcode;
	private String landmark;
	private String country;
	private String type;
	private String state;
	private String defaultStatus;
	private String statusMessage;

	@Siren4JSubEntity
	private List<SellerBranchAddressRepresentation> sellerBranchAddressesRepresentation = new ArrayList<SellerBranchAddressRepresentation>(
			0);

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLine1() {
		return line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getLandmark() {
		return landmark;
	}

	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDefaultStatus() {
		return defaultStatus;
	}

	public void setDefaultStatus(String defaultStatus) {
		this.defaultStatus = defaultStatus;
	}

	public List<SellerBranchAddressRepresentation> getSellerBranchAddressesRepresentation() {
		return sellerBranchAddressesRepresentation;
	}

	public void setSellerBranchAddressesRepresentation(
			List<SellerBranchAddressRepresentation> sellerBranchAddressesRepresentation) {
		this.sellerBranchAddressesRepresentation = sellerBranchAddressesRepresentation;
	}

}
