/**
 *
 */
package com.arthvedi.organic.commons.representation.siren;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.commons.model.ChargeModel;
import com.arthvedi.organic.seller.representation.siren.SellerBranchChargesRepresentation;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.annotations.Siren4JSubEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("chargeRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "charge", uri = "/charges/{id}")
@Representation(ChargeModel.class)
public class ChargeRepresentation extends BaseResource {
	private String id;
	private String chargeName;
	private String status;
	private String statusMessage;

	@Siren4JSubEntity
	private List<SellerBranchChargesRepresentation> sellerBranchChargesesRepresentation = new ArrayList<SellerBranchChargesRepresentation>(
			0);

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getChargeName() {
		return chargeName;
	}

	public void setChargeName(String chargeName) {
		this.chargeName = chargeName;
	}

	public List<SellerBranchChargesRepresentation> getSellerBranchChargesesRepresentation() {
		return sellerBranchChargesesRepresentation;
	}

	public void setSellerBranchChargesesRepresentation(
			List<SellerBranchChargesRepresentation> sellerBranchChargesesRepresentation) {
		this.sellerBranchChargesesRepresentation = sellerBranchChargesesRepresentation;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
