/**
 *
 */
package com.arthvedi.organic.offer.representation.siren;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.offer.model.OfferAuditModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("offerAuditRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "offerAudit", uri = "/offerAudits/{id}")
@Representation(OfferAuditModel.class)
public class OfferAuditRepresentation extends BaseResource {
  private String id;
	private String offerFulfillmentId;
	private String ecommUserId;
	private String usesCount;
	private String status;
	private String statusMessage;
	
	
	
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOfferFulfillmentId() {
		return offerFulfillmentId;
	}
	public void setOfferFulfillmentId(String offerFulfillmentId) {
		this.offerFulfillmentId = offerFulfillmentId;
	}
	public String getEcommUserId() {
		return ecommUserId;
	}
	public void setEcommUserId(String ecommUserId) {
		this.ecommUserId = ecommUserId;
	}
	public String getUsesCount() {
		return usesCount;
	}
	public void setUsesCount(String usesCount) {
		this.usesCount = usesCount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	

}
