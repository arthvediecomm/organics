/**
 *
 */
package com.arthvedi.organic.offer.representation.siren;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.offer.model.OfferFulfillmentModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("offerFulfillmentRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "offerFulfillment", uri = "/offerFulfillments/{id}")
@Representation(OfferFulfillmentModel.class)
public class OfferFulfillmentRepresentation extends BaseResource {
	private String id;
	private String seoId;
	private String seoTitle;
	private String seoKeywords;
	private String seoDescription;
	private String offerName;
	private String offerShortDescription;
	private String offerLongDescription;
	private Byte applyToOrderAmount;
	private String startDate;
	private String endDate;
	private String maxUses;
	private String maxUsesPerCustomer;
	private String offerDiscountAmount;
	private String offerDiscountType;
	private String offerAmountType;
	private String offerType;
	private String status;
	private String thirdPartyContributionAmount;
	private String couponCode;
	private String productApplicableStatus;
	private String eleContributionAmount;
	private String contributionAmountType;
	private String minOrderAmount;
	private String statusMessage;
	
	private List<OfferConfigRepresentation> offerConfigRepresentation = new ArrayList<OfferConfigRepresentation>(0);
	private  List<OfferAuditRepresentation> offerAuditRepresentation= new ArrayList<OfferAuditRepresentation>(0);
	private  List<OfferExcludeConfigRepresentation> offerExcludeConfigRepresentation = new ArrayList<OfferExcludeConfigRepresentation>(0);
	
	
	
	public String getSeoTitle() {
		return seoTitle;
	}
	public void setSeoTitle(String seoTitle) {
		this.seoTitle = seoTitle;
	}
	public String getSeoKeywords() {
		return seoKeywords;
	}
	public void setSeoKeywords(String seoKeywords) {
		this.seoKeywords = seoKeywords;
	}
	public String getSeoDescription() {
		return seoDescription;
	}
	public void setSeoDescription(String seoDescription) {
		this.seoDescription = seoDescription;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSeoId() {
		return seoId;
	}
	public void setSeoId(String seoId) {
		this.seoId = seoId;
	}
	public String getOfferName() {
		return offerName;
	}
	public void setOfferName(String offerName) {
		this.offerName = offerName;
	}
	public String getOfferShortDescription() {
		return offerShortDescription;
	}
	public void setOfferShortDescription(String offerShortDescription) {
		this.offerShortDescription = offerShortDescription;
	}
	public String getOfferLongDescription() {
		return offerLongDescription;
	}
	public void setOfferLongDescription(String offerLongDescription) {
		this.offerLongDescription = offerLongDescription;
	}
	public Byte getApplyToOrderAmount() {
		return applyToOrderAmount;
	}
	public void setApplyToOrderAmount(Byte applyToOrderAmount) {
		this.applyToOrderAmount = applyToOrderAmount;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getMaxUses() {
		return maxUses;
	}
	public void setMaxUses(String maxUses) {
		this.maxUses = maxUses;
	}
	public String getMaxUsesPerCustomer() {
		return maxUsesPerCustomer;
	}
	public void setMaxUsesPerCustomer(String maxUsesPerCustomer) {
		this.maxUsesPerCustomer = maxUsesPerCustomer;
	}
	public String getOfferDiscountAmount() {
		return offerDiscountAmount;
	}
	public void setOfferDiscountAmount(String offerDiscountAmount) {
		this.offerDiscountAmount = offerDiscountAmount;
	}
	public String getOfferDiscountType() {
		return offerDiscountType;
	}
	public void setOfferDiscountType(String offerDiscountType) {
		this.offerDiscountType = offerDiscountType;
	}
	public String getOfferAmountType() {
		return offerAmountType;
	}
	public void setOfferAmountType(String offerAmountType) {
		this.offerAmountType = offerAmountType;
	}
	public String getOfferType() {
		return offerType;
	}
	public void setOfferType(String offerType) {
		this.offerType = offerType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getThirdPartyContributionAmount() {
		return thirdPartyContributionAmount;
	}
	public void setThirdPartyContributionAmount(String thirdPartyContributionAmount) {
		this.thirdPartyContributionAmount = thirdPartyContributionAmount;
	}
	public String getCouponCode() {
		return couponCode;
	}
	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}
	public String getProductApplicableStatus() {
		return productApplicableStatus;
	}
	public void setProductApplicableStatus(String productApplicableStatus) {
		this.productApplicableStatus = productApplicableStatus;
	}
	public String getEleContributionAmount() {
		return eleContributionAmount;
	}
	public void setEleContributionAmount(String eleContributionAmount) {
		this.eleContributionAmount = eleContributionAmount;
	}
	public String getContributionAmountType() {
		return contributionAmountType;
	}
	public void setContributionAmountType(String contributionAmountType) {
		this.contributionAmountType = contributionAmountType;
	}
	public String getMinOrderAmount() {
		return minOrderAmount;
	}
	public void setMinOrderAmount(String minOrderAmount) {
		this.minOrderAmount = minOrderAmount;
	}
	public List<OfferConfigRepresentation> getOfferConfigRepresentation() {
		return offerConfigRepresentation;
	}
	public void setOfferConfigRepresentation(List<OfferConfigRepresentation> offerConfigRepresentation) {
		this.offerConfigRepresentation = offerConfigRepresentation;
	}
	public List<OfferAuditRepresentation> getOfferAuditRepresentation() {
		return offerAuditRepresentation;
	}
	public void setOfferAuditRepresentation(List<OfferAuditRepresentation> offerAuditRepresentation) {
		this.offerAuditRepresentation = offerAuditRepresentation;
	}
	public List<OfferExcludeConfigRepresentation> getOfferExcludeConfigRepresentation() {
		return offerExcludeConfigRepresentation;
	}
	public void setOfferExcludeConfigRepresentation(
			List<OfferExcludeConfigRepresentation> offerExcludeConfigRepresentation) {
		this.offerExcludeConfigRepresentation = offerExcludeConfigRepresentation;
	}


}
