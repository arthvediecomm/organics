/**
 *
 */
package com.arthvedi.organic.offer.representation.siren;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.offer.model.OfferConfigModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.annotations.Siren4JSubEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("offerConfigRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "offerConfig", uri = "/offerConfigs/{id}")
@Representation(OfferConfigModel.class)
public class OfferConfigRepresentation extends BaseResource {
	private String id;
	private String offerFulfillmentId;
	private String sellerId;
	private String sellerProductId;
	private String cityId;
	private String productId;
	private String zoneId;
	private String categoryId;
	private String sellerBranchId;
	private String offerConfigAttrValue;
	private String offerConfigAttrName;
	private String statusMessage;

	@Siren4JSubEntity
	private List<OfferExcludeConfigRepresentation> offerExcludeConfigRep = new ArrayList<OfferExcludeConfigRepresentation>(
			0);

	public List<OfferExcludeConfigRepresentation> getOfferExcludeConfigRep() {
		return offerExcludeConfigRep;
	}

	public void setOfferExcludeConfigRep(
			List<OfferExcludeConfigRepresentation> offerExcludeConfigRep) {
		this.offerExcludeConfigRep = offerExcludeConfigRep;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOfferFulfillmentId() {
		return offerFulfillmentId;
	}

	public void setOfferFulfillmentId(String offerFulfillmentId) {
		this.offerFulfillmentId = offerFulfillmentId;
	}

	public String getSellerId() {
		return sellerId;
	}

	public void setSellerId(String sellerId) {
		this.sellerId = sellerId;
	}

	public String getSellerProductId() {
		return sellerProductId;
	}

	public void setSellerProductId(String sellerProductId) {
		this.sellerProductId = sellerProductId;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getZoneId() {
		return zoneId;
	}

	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getSellerBranchId() {
		return sellerBranchId;
	}

	public void setSellerBranchId(String sellerBranchId) {
		this.sellerBranchId = sellerBranchId;
	}

	public String getOfferConfigAttrValue() {
		return offerConfigAttrValue;
	}

	public void setOfferConfigAttrValue(String offerConfigAttrValue) {
		this.offerConfigAttrValue = offerConfigAttrValue;
	}

	public String getOfferConfigAttrName() {
		return offerConfigAttrName;
	}

	public void setOfferConfigAttrName(String offerConfigAttrName) {
		this.offerConfigAttrName = offerConfigAttrName;
	}

}
