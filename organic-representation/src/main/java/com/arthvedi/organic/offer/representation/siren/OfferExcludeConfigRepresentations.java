/**
 *
 */
package com.arthvedi.organic.offer.representation.siren;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.offer.model.OfferExcludeConfigModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.CollectionResource;

/**
 * @author arthvedi
 *
 */
@Component("offerExcludeConfigRepresentations")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "offerExcludeConfigs", uri = "/offerExcludeConfigs")
@Representation(OfferExcludeConfigModel.class)
public class OfferExcludeConfigRepresentations extends CollectionResource<OfferExcludeConfigRepresentation> {

}
