package com.arthvedi.organic.productcatalog.representation.siren;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.productcatalog.model.SellerProductFeatureModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.CollectionResource;

@Component("sellerProductFeatureRepresentations")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "sellerProductFeaures", uri = "/sellerProductFeatures")
@Representation(SellerProductFeatureModel.class)
public class SellerProductFeatureRepresentations extends CollectionResource<SellerProductFeatureRepresentation> {
}
