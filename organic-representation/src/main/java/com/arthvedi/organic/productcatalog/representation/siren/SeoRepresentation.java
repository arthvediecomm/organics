/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.siren;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.productcatalog.model.SeoModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("seoRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "seo", uri = "/seos/{id}")
@Representation(SeoModel.class)
public class SeoRepresentation extends BaseResource {
	private String id;
	private String seoTitle;
	private String seoKeyword;
	private String seoDescription;
	
	

	private List<ProductNamesRepresentation> productNamesesRepresentation = new ArrayList<ProductNamesRepresentation>(
			0);
	private List<CategoryNamesRepresentation> categoryNamesesRepresentation = new ArrayList<CategoryNamesRepresentation>(
			0);

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSeoTitle() {
		return seoTitle;
	}

	public void setSeoTitle(String seoTitle) {
		this.seoTitle = seoTitle;
	}

	public String getSeoKeyword() {
		return seoKeyword;
	}

	public void setSeoKeyword(String seoKeyword) {
		this.seoKeyword = seoKeyword;
	}

	public String getSeoDescription() {
		return seoDescription;
	}

	public void setSeoDescription(String seoDescription) {
		this.seoDescription = seoDescription;
	}

	public List<ProductNamesRepresentation> getProductNamesesRepresentation() {
		return productNamesesRepresentation;
	}

	public void setProductNamesesRepresentation(
			List<ProductNamesRepresentation> productNamesesRepresentation) {
		this.productNamesesRepresentation = productNamesesRepresentation;
	}

	public List<CategoryNamesRepresentation> getCategoryNamesesRepresentation() {
		return categoryNamesesRepresentation;
	}

	public void setCategoryNamesesRepresentation(
			List<CategoryNamesRepresentation> categoryNamesesRepresentation) {
		this.categoryNamesesRepresentation = categoryNamesesRepresentation;
	}

}
