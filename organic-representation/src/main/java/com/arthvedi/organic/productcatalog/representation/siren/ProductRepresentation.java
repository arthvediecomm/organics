/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.siren;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.productcatalog.model.ProductModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.annotations.Siren4JSubEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("productRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "product", uri = "/products/{id}")
@Representation(ProductModel.class)
public class ProductRepresentation extends BaseResource {
	private String id;
	private String brandId;
	private String productName;
	private String description;
	private String status;
	private String mrp;
	private BigDecimal sellingPrice;

	private String measurementUnit;
	private String quantity;
	private String baseUnit;
	private String statusMessage;
	@Siren4JSubEntity
	private List<ComboProductRepresentation> comboProductsForParentProductIdRepresentation = new ArrayList<ComboProductRepresentation>(
			0);
	@Siren4JSubEntity
	private List<ComboProductRepresentation> comboProductsForChildProductIdRepresentation = new ArrayList<ComboProductRepresentation>(
			0);
	@Siren4JSubEntity
	private List<ProductNamesRepresentation> productNamesesRepresentation = new ArrayList<ProductNamesRepresentation>(
			0);
	@Siren4JSubEntity
	private List<ProductFeatureRepresentation> productFeaturesRepresentation = new ArrayList<ProductFeatureRepresentation>(
			0);
	@Siren4JSubEntity
	private List<SellerProductRepresentation> sellerProductsRepresentation = new ArrayList<SellerProductRepresentation>(
			0);
	@Siren4JSubEntity
	private List<ProductCategoryRepresentation> productCategoriesRepresentation = new ArrayList<ProductCategoryRepresentation>(
			0);
	@Siren4JSubEntity
	private List<ProductImagesRepresentation> productImagesesRepresentation = new ArrayList<ProductImagesRepresentation>(
			0);

	public List<ProductImagesRepresentation> getProductImagesesRepresentation() {
		return productImagesesRepresentation;
	}

	public void setProductImagesesRepresentation(
			List<ProductImagesRepresentation> productImagesesRepresentation) {
		this.productImagesesRepresentation = productImagesesRepresentation;
	}

	public BigDecimal getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(BigDecimal sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBrandId() {
		return brandId;
	}

	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMrp() {
		return mrp;
	}

	public void setMrp(String mrp) {
		this.mrp = mrp;
	}

	public String getMeasurementUnit() {
		return measurementUnit;
	}

	public void setMeasurementUnit(String measurementUnit) {
		this.measurementUnit = measurementUnit;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public List<ComboProductRepresentation> getComboProductsForParentProductIdRepresentation() {
		return comboProductsForParentProductIdRepresentation;
	}

	public void setComboProductsForParentProductIdRepresentation(
			List<ComboProductRepresentation> comboProductsForParentProductIdRepresentation) {
		this.comboProductsForParentProductIdRepresentation = comboProductsForParentProductIdRepresentation;
	}

	public List<ComboProductRepresentation> getComboProductsForChildProductIdRepresentation() {
		return comboProductsForChildProductIdRepresentation;
	}

	public void setComboProductsForChildProductIdRepresentation(
			List<ComboProductRepresentation> comboProductsForChildProductIdRepresentation) {
		this.comboProductsForChildProductIdRepresentation = comboProductsForChildProductIdRepresentation;
	}

	public List<ProductNamesRepresentation> getProductNamesesRepresentation() {
		return productNamesesRepresentation;
	}

	public void setProductNamesesRepresentation(
			List<ProductNamesRepresentation> productNamesesRepresentation) {
		this.productNamesesRepresentation = productNamesesRepresentation;
	}

	public List<ProductFeatureRepresentation> getProductFeaturesRepresentation() {
		return productFeaturesRepresentation;
	}

	public void setProductFeaturesRepresentation(
			List<ProductFeatureRepresentation> productFeaturesRepresentation) {
		this.productFeaturesRepresentation = productFeaturesRepresentation;
	}

	public List<SellerProductRepresentation> getSellerProductsRepresentation() {
		return sellerProductsRepresentation;
	}

	public void setSellerProductsRepresentation(
			List<SellerProductRepresentation> sellerProductsRepresentation) {
		this.sellerProductsRepresentation = sellerProductsRepresentation;
	}

	public List<ProductCategoryRepresentation> getProductCategoriesRepresentation() {
		return productCategoriesRepresentation;
	}

	public void setProductCategoriesRepresentation(
			List<ProductCategoryRepresentation> productCategoriesRepresentation) {
		this.productCategoriesRepresentation = productCategoriesRepresentation;
	}

}
