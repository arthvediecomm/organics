/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.siren;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.productcatalog.model.FeatureModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.CollectionResource;

/**
 * @author arthvedi
 *
 */
@Component("featureRepresentations")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "features", uri = "/features")
@Representation(FeatureModel.class)
public class FeatureRepresentations extends CollectionResource<FeatureRepresentation> {

}
