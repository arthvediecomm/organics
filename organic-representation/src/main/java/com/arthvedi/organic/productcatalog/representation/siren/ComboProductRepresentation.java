/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.siren;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.productcatalog.model.ComboProductModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("comboProductRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "comboProduct", uri = "/comboProducts/{id}")
@Representation(ComboProductModel.class)
public class ComboProductRepresentation extends BaseResource {
  private String id;
	private String productByParentProductId;
	private String productByChildProductId;
	private String status;
	private String price;
	private String units;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getProductByParentProductId() {
		return productByParentProductId;
	}
	public void setProductByParentProductId(String productByParentProductId) {
		this.productByParentProductId = productByParentProductId;
	}
	public String getProductByChildProductId() {
		return productByChildProductId;
	}
	public void setProductByChildProductId(String productByChildProductId) {
		this.productByChildProductId = productByChildProductId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getUnits() {
		return units;
	}
	public void setUnits(String units) {
		this.units = units;
	}

	
	
}
