/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.siren;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.productcatalog.model.SellerProductModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("sellerProductRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "sellerProduct", uri = "/sellerProducts/{id}")
@Representation(SellerProductModel.class)
public class SellerProductRepresentation extends BaseResource {
	private String id;
	private String returnPolicyId;
	private String offerPrice;

	public String getOfferPrice() {
		return offerPrice;
	}

	public void setOfferPrice(String offerPrice) {
		this.offerPrice = offerPrice;
	}

	public String getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(String sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	private String productId;
	private String sellerBranchId;
	private String status;
	private String sellerPrice;
	private String description;
	private String sellingPrice;
	private String minQuantity;
	private String maxQuantity;
	private String sellerStock;
	private String stockableStatus;
	private String baseUnit;
	private String measurementUnit;
	private String quantity;
	private String sellerProductName;
	private String productMrp;
	private List<SellerProductPricesRepresentation> sellerProductPricesesRepresentation = new ArrayList<SellerProductPricesRepresentation>(
			0);

	public String getSellerPrice() {
		return sellerPrice;
	}

	public void setSellerPrice(String sellerPrice) {
		this.sellerPrice = sellerPrice;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getReturnPolicyId() {
		return returnPolicyId;
	}

	public void setReturnPolicyId(String returnPolicyId) {
		this.returnPolicyId = returnPolicyId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getSellerBranchId() {
		return sellerBranchId;
	}

	public void setSellerBranchId(String sellerBranchId) {
		this.sellerBranchId = sellerBranchId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMinQuantity() {
		return minQuantity;
	}

	public void setMinQuantity(String minQuantity) {
		this.minQuantity = minQuantity;
	}

	public String getMaxQuantity() {
		return maxQuantity;
	}

	public void setMaxQuantity(String maxQuantity) {
		this.maxQuantity = maxQuantity;
	}

	public String getSellerStock() {
		return sellerStock;
	}

	public void setSellerStock(String sellerStock) {
		this.sellerStock = sellerStock;
	}

	public String getStockableStatus() {
		return stockableStatus;
	}

	public void setStockableStatus(String stockableStatus) {
		this.stockableStatus = stockableStatus;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public String getMeasurementUnit() {
		return measurementUnit;
	}

	public void setMeasurementUnit(String measurementUnit) {
		this.measurementUnit = measurementUnit;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getSellerProductName() {
		return sellerProductName;
	}

	public void setSellerProductName(String sellerProductName) {
		this.sellerProductName = sellerProductName;
	}

	public String getProductMrp() {
		return productMrp;
	}

	public void setProductMrp(String productMrp) {
		this.productMrp = productMrp;
	}

	public List<SellerProductPricesRepresentation> getSellerProductPricesesRepresentation() {
		return sellerProductPricesesRepresentation;
	}

	public void setSellerProductPricesesRepresentation(
			List<SellerProductPricesRepresentation> sellerProductPricesesRepresentation) {
		this.sellerProductPricesesRepresentation = sellerProductPricesesRepresentation;
	}

}
