/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.siren;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.productcatalog.model.CategoryModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.annotations.Siren4JSubEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("categoryRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "category", uri = "/categorys/{id}")
@Representation(CategoryModel.class)
public class CategoryRepresentation extends BaseResource {
	private String id;
	private String categoryId;
	private String category;
	private String categoryName;
	private String description;
	private String status;
	private String level;
	private String statusMessage;

	@Siren4JSubEntity
	private List<CategoryRepresentation> categoriesRepresentation = new ArrayList<CategoryRepresentation>(
			0);
	@Siren4JSubEntity
	private List<ProductCategoryRepresentation> productCategoriesRepresentation = new ArrayList<>(
			0);
	@Siren4JSubEntity
	private List<CategoryFeatureRepresentation> categoryFeaturesRepresentation = new ArrayList<>(
			0);
	@Siren4JSubEntity
	private List<CategoryNamesRepresentation> categoryNamesesRepresentation = new ArrayList<>(
			0);
	@Siren4JSubEntity
	private List<CategoryImagesRepresentation> categoryImagesesRepresentation = new ArrayList<>(
			0);

	public List<CategoryImagesRepresentation> getCategoryImagesesRepresentation() {
		return categoryImagesesRepresentation;
	}

	public void setCategoryImagesesRepresentation(
			List<CategoryImagesRepresentation> categoryImagesesRepresentation) {
		this.categoryImagesesRepresentation = categoryImagesesRepresentation;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getId() {

		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public List<CategoryRepresentation> getCategoriesRepresentation() {
		return categoriesRepresentation;
	}

	public void setCategoriesRepresentation(
			List<CategoryRepresentation> categoriesRepresentation) {
		this.categoriesRepresentation = categoriesRepresentation;
	}

	public List<ProductCategoryRepresentation> getProductCategoriesRepresentation() {
		return productCategoriesRepresentation;
	}

	public void setProductCategoriesRepresentation(
			List<ProductCategoryRepresentation> productCategoriesRepresentation) {
		this.productCategoriesRepresentation = productCategoriesRepresentation;
	}

	public List<CategoryFeatureRepresentation> getCategoryFeaturesRepresentation() {
		return categoryFeaturesRepresentation;
	}

	public void setCategoryFeaturesRepresentation(
			List<CategoryFeatureRepresentation> categoryFeaturesRepresentation) {
		this.categoryFeaturesRepresentation = categoryFeaturesRepresentation;
	}

	public List<CategoryNamesRepresentation> getCategoryNamesesRepresentation() {
		return categoryNamesesRepresentation;
	}

	public void setCategoryNamesesRepresentation(
			List<CategoryNamesRepresentation> categoryNamesesRepresentation) {
		this.categoryNamesesRepresentation = categoryNamesesRepresentation;
	}

}
