/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.siren;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.productcatalog.model.BrandModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.annotations.Siren4JSubEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("brandRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "brand", uri = "/brands/{id}")
@Representation(BrandModel.class)
public class BrandRepresentation extends BaseResource {
	private String id;
	private String name;
	private String status;
	private String statusMessage;
	@Siren4JSubEntity
	private List<ProductRepresentation> productsRepresentation = new ArrayList<>(
			0);
	@Siren4JSubEntity
	private List<BrandImagesRepresentation> brandImagesRepresentation = new ArrayList<>(
			0);

	public List<BrandImagesRepresentation> getBrandImagesRepresentation() {
		return brandImagesRepresentation;
	}

	public void setBrandImagesRepresentation(
			List<BrandImagesRepresentation> brandImagesRepresentation) {
		this.brandImagesRepresentation = brandImagesRepresentation;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<ProductRepresentation> getProductsRepresentation() {
		return productsRepresentation;
	}

	public void setProductsRepresentation(
			List<ProductRepresentation> productsRepresentation) {
		this.productsRepresentation = productsRepresentation;
	}

}
