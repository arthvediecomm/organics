/**
 *
 */
package com.arthvedi.organic.productcatalog.representation.siren;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.productcatalog.model.FeatureModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("featureRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "feature", uri = "/features/{id}")
@Representation(FeatureModel.class)
public class FeatureRepresentation extends BaseResource {

	private String id;
	private String featureName;
	private String featureValue;
	private String code;
	private String status;

	private List<ProductFeatureRepresentation> productFeaturesRepresentation = new ArrayList<ProductFeatureRepresentation>(
			0);
	private List<CategoryFeatureRepresentation> categoryFeaturesRepresentation = new ArrayList<CategoryFeatureRepresentation>(
			0);

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFeatureName() {
		return featureName;
	}

	public void setFeatureName(String featureName) {
		this.featureName = featureName;
	}

	public String getFeatureValue() {
		return featureValue;
	}

	public void setFeatureValue(String featureValue) {
		this.featureValue = featureValue;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<ProductFeatureRepresentation> getProductFeaturesRepresentation() {
		return productFeaturesRepresentation;
	}

	public void setProductFeaturesRepresentation(List<ProductFeatureRepresentation> productFeaturesRepresentation) {
		this.productFeaturesRepresentation = productFeaturesRepresentation;
	}

	public List<CategoryFeatureRepresentation> getCategoryFeaturesRepresentation() {
		return categoryFeaturesRepresentation;
	}

	public void setCategoryFeaturesRepresentation(List<CategoryFeatureRepresentation> categoryFeaturesRepresentation) {
		this.categoryFeaturesRepresentation = categoryFeaturesRepresentation;
	}

}
