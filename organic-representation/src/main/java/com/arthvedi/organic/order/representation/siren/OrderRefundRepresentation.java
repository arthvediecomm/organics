/**
 *
 */
package com.arthvedi.organic.order.representation.siren;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.order.model.OrderRefundModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("orderRefundRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "orderRefund", uri = "/orderRefunds/{id}")
@Representation(OrderRefundModel.class)
public class OrderRefundRepresentation extends BaseResource {
  private String id;
	private String orderId;
	private String refundActualAmount;
	private String refundPayableAmount;
	private String refundDescription;
	private String refundStatus;
	private String refundPaidReferenceNo;
	private String refundPaidStatus;
	private String statusMessage;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getRefundActualAmount() {
		return refundActualAmount;
	}
	public void setRefundActualAmount(String refundActualAmount) {
		this.refundActualAmount = refundActualAmount;
	}
	public String getRefundPayableAmount() {
		return refundPayableAmount;
	}
	public void setRefundPayableAmount(String refundPayableAmount) {
		this.refundPayableAmount = refundPayableAmount;
	}
	public String getRefundDescription() {
		return refundDescription;
	}
	public void setRefundDescription(String refundDescription) {
		this.refundDescription = refundDescription;
	}
	public String getRefundStatus() {
		return refundStatus;
	}
	public void setRefundStatus(String refundStatus) {
		this.refundStatus = refundStatus;
	}
	public String getRefundPaidReferenceNo() {
		return refundPaidReferenceNo;
	}
	public void setRefundPaidReferenceNo(String refundPaidReferenceNo) {
		this.refundPaidReferenceNo = refundPaidReferenceNo;
	}
	public String getRefundPaidStatus() {
		return refundPaidStatus;
	}
	public void setRefundPaidStatus(String refundPaidStatus) {
		this.refundPaidStatus = refundPaidStatus;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}


	
	
}
