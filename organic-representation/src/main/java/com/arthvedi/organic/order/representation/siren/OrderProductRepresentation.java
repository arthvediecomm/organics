/**
 *
 */
package com.arthvedi.organic.order.representation.siren;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.order.model.OrderProductModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.annotations.Siren4JSubEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("orderProductRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "orderProduct", uri = "/orderProducts/{id}")
@Representation(OrderProductModel.class)
public class OrderProductRepresentation extends BaseResource {
	private String id;
	private String order;
	private String sellerProduct;
	private String marketPrice;
	private String sellerPrice;
	private String finalUnitPrice;
	private String status;
	private String cartStatusFlag;
	private String noOfUnits;
	private String unitOfMeasurement;
	private String otherDiscounts;
	private String totalTaxes;
	private String orderProductcol;
	private String statusMessage;
	@Siren4JSubEntity
	private List<OrderProductTaxesRepresentation> orderProductTaxesRep = new ArrayList<OrderProductTaxesRepresentation>(0);
	@Siren4JSubEntity
	private List<OrderProductStatusLogRepresentation> orderProductStatusLogRep = new ArrayList<OrderProductStatusLogRepresentation>(0);
	@Siren4JSubEntity
	private List<OrderProductFeaturesRepresentation> orderProductFeaturesRep = new ArrayList<OrderProductFeaturesRepresentation>(0);
	@Siren4JSubEntity
	private List<OrderProductDiscountRepresentation> orderProductDiscountRep = new ArrayList<OrderProductDiscountRepresentation>(0);
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public String getSellerProduct() {
		return sellerProduct;
	}
	public void setSellerProduct(String sellerProduct) {
		this.sellerProduct = sellerProduct;
	}
	public String getMarketPrice() {
		return marketPrice;
	}
	public void setMarketPrice(String marketPrice) {
		this.marketPrice = marketPrice;
	}
	public String getSellerPrice() {
		return sellerPrice;
	}
	public void setSellerPrice(String sellerPrice) {
		this.sellerPrice = sellerPrice;
	}
	public String getFinalUnitPrice() {
		return finalUnitPrice;
	}
	public void setFinalUnitPrice(String finalUnitPrice) {
		this.finalUnitPrice = finalUnitPrice;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCartStatusFlag() {
		return cartStatusFlag;
	}
	public void setCartStatusFlag(String cartStatusFlag) {
		this.cartStatusFlag = cartStatusFlag;
	}
	public String getNoOfUnits() {
		return noOfUnits;
	}
	public void setNoOfUnits(String noOfUnits) {
		this.noOfUnits = noOfUnits;
	}
	public String getUnitOfMeasurement() {
		return unitOfMeasurement;
	}
	public void setUnitOfMeasurement(String unitOfMeasurement) {
		this.unitOfMeasurement = unitOfMeasurement;
	}
	public String getOtherDiscounts() {
		return otherDiscounts;
	}
	public void setOtherDiscounts(String otherDiscounts) {
		this.otherDiscounts = otherDiscounts;
	}
	public String getTotalTaxes() {
		return totalTaxes;
	}
	public void setTotalTaxes(String totalTaxes) {
		this.totalTaxes = totalTaxes;
	}
	public String getOrderProductcol() {
		return orderProductcol;
	}
	public void setOrderProductcol(String orderProductcol) {
		this.orderProductcol = orderProductcol;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	public List<OrderProductTaxesRepresentation> getOrderProductTaxesRep() {
		return orderProductTaxesRep;
	}
	public void setOrderProductTaxesRep(List<OrderProductTaxesRepresentation> orderProductTaxesRep) {
		this.orderProductTaxesRep = orderProductTaxesRep;
	}
	public List<OrderProductStatusLogRepresentation> getOrderProductStatusLogRep() {
		return orderProductStatusLogRep;
	}
	public void setOrderProductStatusLogRep(List<OrderProductStatusLogRepresentation> orderProductStatusLogRep) {
		this.orderProductStatusLogRep = orderProductStatusLogRep;
	}
	public List<OrderProductFeaturesRepresentation> getOrderProductFeaturesRep() {
		return orderProductFeaturesRep;
	}
	public void setOrderProductFeaturesRep(List<OrderProductFeaturesRepresentation> orderProductFeaturesRep) {
		this.orderProductFeaturesRep = orderProductFeaturesRep;
	}
	public List<OrderProductDiscountRepresentation> getOrderProductDiscountRep() {
		return orderProductDiscountRep;
	}
	public void setOrderProductDiscountRep(List<OrderProductDiscountRepresentation> orderProductDiscountRep) {
		this.orderProductDiscountRep = orderProductDiscountRep;
	}


}
