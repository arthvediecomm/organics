/**
 *
 */
package com.arthvedi.organic.order.representation.siren;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.order.model.OrderProductStatusLogModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("orderProductStatusLogRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "orderProductStatusLog", uri = "/orderProductStatusLogs/{id}")
@Representation(OrderProductStatusLogModel.class)
public class OrderProductStatusLogRepresentation extends BaseResource {
  private String id;
	private String orderProductId;
	private String orderProductStatusCode;
	private String orderProductStatusDate;
	private String orderProductStatusDescription;
private String StatusMessage;
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getOrderProductId() {
	return orderProductId;
}
public void setOrderProductId(String orderProductId) {
	this.orderProductId = orderProductId;
}
public String getOrderProductStatusCode() {
	return orderProductStatusCode;
}
public void setOrderProductStatusCode(String orderProductStatusCode) {
	this.orderProductStatusCode = orderProductStatusCode;
}
public String getOrderProductStatusDate() {
	return orderProductStatusDate;
}
public void setOrderProductStatusDate(String orderProductStatusDate) {
	this.orderProductStatusDate = orderProductStatusDate;
}
public String getOrderProductStatusDescription() {
	return orderProductStatusDescription;
}
public void setOrderProductStatusDescription(String orderProductStatusDescription) {
	this.orderProductStatusDescription = orderProductStatusDescription;
}
public String getStatusMessage() {
	return StatusMessage;
}
public void setStatusMessage(String statusMessage) {
	StatusMessage = statusMessage;
}


}
