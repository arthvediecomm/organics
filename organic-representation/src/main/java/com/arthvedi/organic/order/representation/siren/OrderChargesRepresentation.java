/**
 *
 */
package com.arthvedi.organic.order.representation.siren;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.order.model.OrderChargesModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("orderChargesRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "orderCharges", uri = "/orderChargess/{id}")
@Representation(OrderChargesModel.class)
public class OrderChargesRepresentation extends BaseResource {
  private String id;
	private String orderId;
	private String orderChargescol;
	private String chargesCode;
	private String chargesValue;
	private String statusMessage;
	
	
	
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	public String getId() {
		
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getOrderChargescol() {
		return orderChargescol;
	}
	public void setOrderChargescol(String orderChargescol) {
		this.orderChargescol = orderChargescol;
	}
	public String getChargesCode() {
		return chargesCode;
	}
	public void setChargesCode(String chargesCode) {
		this.chargesCode = chargesCode;
	}
	public String getChargesValue() {
		return chargesValue;
	}
	public void setChargesValue(String chargesValue) {
		this.chargesValue = chargesValue;
	}
	
	
}
