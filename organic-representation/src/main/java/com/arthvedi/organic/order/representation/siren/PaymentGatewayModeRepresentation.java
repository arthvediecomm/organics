/**
 *
 */
package com.arthvedi.organic.order.representation.siren;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.order.model.PaymentGatewayModeModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.annotations.Siren4JSubEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("paymentGatewayModeRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "paymentGatewayMode", uri = "/paymentGatewayModes/{id}")
@Representation(PaymentGatewayModeModel.class)
public class PaymentGatewayModeRepresentation extends BaseResource {
	private String id;
	private String name;
	private String code;
	private String type;
	private String statusMessage;
	@Siren4JSubEntity
	private List<OrderFullfillmentRepresentation> orderFullfillmentRep = new ArrayList<OrderFullfillmentRepresentation>(
			0);

	public String getId() {

		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public List<OrderFullfillmentRepresentation> getOrderFullfillmentRep() {
		return orderFullfillmentRep;
	}

	public void setOrderFullfillmentRep(List<OrderFullfillmentRepresentation> orderFullfillmentRep) {
		this.orderFullfillmentRep = orderFullfillmentRep;
	}

}
