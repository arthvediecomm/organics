/**
 *
 */
package com.arthvedi.organic.order.representation.siren;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.order.model.OrderFullfillmentDiscountModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("orderFullfillmentDiscountRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "orderFullfillmentDiscount", uri = "/orderFullfillmentDiscounts/{id}")
@Representation(OrderFullfillmentDiscountModel.class)
public class OrderFullfillmentDiscountRepresentation extends BaseResource {
  
	
	private String id;
	private String orderFullfillmentId;
	private String discountId;
private String statusMessage;




public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getOrderFullfillmentId() {
	return orderFullfillmentId;
}
public void setOrderFullfillmentId(String orderFullfillmentId) {
	this.orderFullfillmentId = orderFullfillmentId;
}
public String getDiscountId() {
	return discountId;
}
public void setDiscountId(String discountId) {
	this.discountId = discountId;
}
public String getStatusMessage() {
	return statusMessage;
}
public void setStatusMessage(String statusMessage) {
	this.statusMessage = statusMessage;
}

}
