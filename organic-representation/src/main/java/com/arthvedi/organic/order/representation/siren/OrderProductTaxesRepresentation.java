/**
 *
 */
package com.arthvedi.organic.order.representation.siren;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.order.model.OrderProductTaxesModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("orderProductTaxesRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "orderProductTaxes", uri = "/orderProductTaxess/{id}")
@Representation(OrderProductTaxesModel.class)
public class OrderProductTaxesRepresentation extends BaseResource {
  
private String id;
	private String orderProductId;
	private String taxTypeCode;
	private String taxPercentage;
private String statusMessage;



public String getId() {
	
	
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getOrderProductId() {
	return orderProductId;
}
public void setOrderProductId(String orderProductId) {
	this.orderProductId = orderProductId;
}
public String getTaxTypeCode() {
	return taxTypeCode;
}
public void setTaxTypeCode(String taxTypeCode) {
	this.taxTypeCode = taxTypeCode;
}
public String getTaxPercentage() {
	return taxPercentage;
}
public void setTaxPercentage(String taxPercentage) {
	this.taxPercentage = taxPercentage;
}
public String getStatusMessage() {
	return statusMessage;
}
public void setStatusMessage(String statusMessage) {
	this.statusMessage = statusMessage;
}




}
