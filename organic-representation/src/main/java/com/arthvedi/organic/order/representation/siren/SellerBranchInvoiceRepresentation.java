/**
 *
 */
package com.arthvedi.organic.order.representation.siren;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.order.model.SellerBranchInvoiceModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.annotations.Siren4JSubEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("sellerBranchInvoiceRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "sellerBranchInvoice", uri = "/sellerBranchInvoices/{id}")
@Representation(SellerBranchInvoiceModel.class)
public class SellerBranchInvoiceRepresentation extends BaseResource {
  
private String id;
	private String sellerBranchId;
	private String billingPeriodFrom;
	private String billingPeriodTo;
	private String amount;
	private String marginAmount;
	private String paidStatus;
	private String statusMessage;
	@Siren4JSubEntity
	private List<EcommerceOrderRepresentation> handcraftOrderRep = new ArrayList<EcommerceOrderRepresentation>(0);
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSellerBranchId() {
		return sellerBranchId;
	}
	public void setSellerBranchId(String sellerBranchId) {
		this.sellerBranchId = sellerBranchId;
	}
	public String getBillingPeriodFrom() {
		return billingPeriodFrom;
	}
	public void setBillingPeriodFrom(String billingPeriodFrom) {
		this.billingPeriodFrom = billingPeriodFrom;
	}
	public String getBillingPeriodTo() {
		return billingPeriodTo;
	}
	public void setBillingPeriodTo(String billingPeriodTo) {
		this.billingPeriodTo = billingPeriodTo;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	public String getMarginAmount() {
		return marginAmount;
	}
	public void setMarginAmount(String marginAmount) {
		this.marginAmount = marginAmount;
	}
	public List<EcommerceOrderRepresentation> getHandcraftOrderRep() {
		return handcraftOrderRep;
	}
	public void setHandcraftOrderRep(List<EcommerceOrderRepresentation> handcraftOrderRep) {
		this.handcraftOrderRep = handcraftOrderRep;
	}
	public String getPaidStatus() {
		return paidStatus;
	}
	public void setPaidStatus(String paidStatus) {
		this.paidStatus = paidStatus;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	

	
	
	
}
