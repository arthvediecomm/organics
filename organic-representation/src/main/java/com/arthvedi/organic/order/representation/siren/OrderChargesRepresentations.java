/**
 *
 */
package com.arthvedi.organic.order.representation.siren;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.order.model.OrderChargesModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.CollectionResource;

/**
 * @author arthvedi
 *
 */
@Component("orderChargesRepresentations")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "orderChargess", uri = "/orderChargess")
@Representation(OrderChargesModel.class)
public class OrderChargesRepresentations extends CollectionResource<OrderChargesRepresentation> {

}
