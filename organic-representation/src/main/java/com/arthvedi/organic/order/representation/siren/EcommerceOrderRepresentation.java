/**
 *
 */
package com.arthvedi.organic.order.representation.siren;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.order.model.EcommerceOrderModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.annotations.Siren4JSubEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("handcraftOrderRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "handcraftOrder", uri = "/handcraftOrders/{id}")
@Representation(EcommerceOrderModel.class)
public class EcommerceOrderRepresentation extends BaseResource {
	private String id;
	private String orderFullfillmentId;
	private String sellerBranchId;
	private String sellerBranchInvoiceId;
	private String deliveryUserId;
	private String code;
	private String actualAmount;
	private String payableAmount;
	private String status;
	private String deliveryStartDate;
	private String deliveryEndDate;
	private String deliveryType;
	private String billedStatus;
	private String placedTime;
	private String confirmedTime;
	private String shippedTime;
	private String deliveredTime;
	private String cancelledTime;
	private String packedTime;
	private String cartStatusFlag;
	private String totalCharges;
	private String totalTaxes;
	private String statusMessage;
	@Siren4JSubEntity
	private List<OrderDiscountRepresentation> orderDiscountRep = new ArrayList<OrderDiscountRepresentation>(
			0);
	@Siren4JSubEntity
	private List<OrderProductRepresentation> orderProductRep = new ArrayList<OrderProductRepresentation>(
			0);
	@Siren4JSubEntity
	private List<OrderChargesRepresentation> orderChargesRep = new ArrayList<OrderChargesRepresentation>(
			0);
	@Siren4JSubEntity
	private List<OrderRefundRepresentation> orderRefundRep = new ArrayList<OrderRefundRepresentation>(
			0);
	@Siren4JSubEntity
	private List<OrderStatusLogRepresentation> orderStatusLogRep = new ArrayList<OrderStatusLogRepresentation>(
			0);

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrderFullfillmentId() {
		return orderFullfillmentId;
	}

	public void setOrderFullfillmentId(String orderFullfillmentId) {
		this.orderFullfillmentId = orderFullfillmentId;
	}

	public String getSellerBranchId() {
		return sellerBranchId;
	}

	public void setSellerBranchId(String sellerBranchId) {
		this.sellerBranchId = sellerBranchId;
	}

	public String getSellerBranchInvoiceId() {
		return sellerBranchInvoiceId;
	}

	public void setSellerBranchInvoiceId(String sellerBranchInvoiceId) {
		this.sellerBranchInvoiceId = sellerBranchInvoiceId;
	}

	public String getDeliveryUserId() {
		return deliveryUserId;
	}

	public void setDeliveryUserId(String deliveryUserId) {
		this.deliveryUserId = deliveryUserId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getActualAmount() {
		return actualAmount;
	}

	public void setActualAmount(String actualAmount) {
		this.actualAmount = actualAmount;
	}

	public String getPayableAmount() {
		return payableAmount;
	}

	public void setPayableAmount(String payableAmount) {
		this.payableAmount = payableAmount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDeliveryStartDate() {
		return deliveryStartDate;
	}

	public void setDeliveryStartDate(String deliveryStartDate) {
		this.deliveryStartDate = deliveryStartDate;
	}

	public String getDeliveryEndDate() {
		return deliveryEndDate;
	}

	public void setDeliveryEndDate(String deliveryEndDate) {
		this.deliveryEndDate = deliveryEndDate;
	}

	public String getDeliveryType() {
		return deliveryType;
	}

	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}

	public String getBilledStatus() {
		return billedStatus;
	}

	public void setBilledStatus(String billedStatus) {
		this.billedStatus = billedStatus;
	}

	public String getPlacedTime() {
		return placedTime;
	}

	public void setPlacedTime(String placedTime) {
		this.placedTime = placedTime;
	}

	public String getConfirmedTime() {
		return confirmedTime;
	}

	public void setConfirmedTime(String confirmedTime) {
		this.confirmedTime = confirmedTime;
	}

	public String getShippedTime() {
		return shippedTime;
	}

	public void setShippedTime(String shippedTime) {
		this.shippedTime = shippedTime;
	}

	public String getDeliveredTime() {
		return deliveredTime;
	}

	public void setDeliveredTime(String deliveredTime) {
		this.deliveredTime = deliveredTime;
	}

	public String getCancelledTime() {
		return cancelledTime;
	}

	public void setCancelledTime(String cancelledTime) {
		this.cancelledTime = cancelledTime;
	}

	public String getPackedTime() {
		return packedTime;
	}

	public void setPackedTime(String packedTime) {
		this.packedTime = packedTime;
	}

	public String getCartStatusFlag() {
		return cartStatusFlag;
	}

	public void setCartStatusFlag(String cartStatusFlag) {
		this.cartStatusFlag = cartStatusFlag;
	}

	public String getTotalCharges() {
		return totalCharges;
	}

	public void setTotalCharges(String totalCharges) {
		this.totalCharges = totalCharges;
	}

	public String getTotalTaxes() {
		return totalTaxes;
	}

	public void setTotalTaxes(String totalTaxes) {
		this.totalTaxes = totalTaxes;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public List<OrderDiscountRepresentation> getOrderDiscountRep() {
		return orderDiscountRep;
	}

	public void setOrderDiscountRep(
			List<OrderDiscountRepresentation> orderDiscountRep) {
		this.orderDiscountRep = orderDiscountRep;
	}

	public List<OrderProductRepresentation> getOrderProductRep() {
		return orderProductRep;
	}

	public void setOrderProductRep(
			List<OrderProductRepresentation> orderProductRep) {
		this.orderProductRep = orderProductRep;
	}

	public List<OrderChargesRepresentation> getOrderChargesRep() {
		return orderChargesRep;
	}

	public void setOrderChargesRep(
			List<OrderChargesRepresentation> orderChargesRep) {
		this.orderChargesRep = orderChargesRep;
	}

	public List<OrderRefundRepresentation> getOrderRefundRep() {
		return orderRefundRep;
	}

	public void setOrderRefundRep(List<OrderRefundRepresentation> orderRefundRep) {
		this.orderRefundRep = orderRefundRep;
	}

	public List<OrderStatusLogRepresentation> getOrderStatusLogRep() {
		return orderStatusLogRep;
	}

	public void setOrderStatusLogRep(
			List<OrderStatusLogRepresentation> orderStatusLogRep) {
		this.orderStatusLogRep = orderStatusLogRep;
	}

}
