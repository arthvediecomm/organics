/**
 *
 */
package com.arthvedi.organic.order.representation.siren;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.order.model.PaymentGatewayModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.CollectionResource;

/**
 * @author arthvedi
 *
 */
@Component("paymentGatewayRepresentations")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "paymentGateways", uri = "/paymentGateways")
@Representation(PaymentGatewayModel.class)
public class PaymentGatewayRepresentations extends CollectionResource<PaymentGatewayRepresentation> {

}
