/**
 *
 */
package com.arthvedi.organic.order.representation.siren;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.order.model.OrderProductDiscountModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("orderProductDiscountRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "orderProductDiscount", uri = "/orderProductDiscounts/{id}")
@Representation(OrderProductDiscountModel.class)
public class OrderProductDiscountRepresentation extends BaseResource {
  private String id;
	private String orderProductId;
	private String discountId;
private String statusMessage;


public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getOrderProductId() {
	return orderProductId;
}
public void setOrderProductId(String orderProductId) {
	this.orderProductId = orderProductId;
}
public String getDiscountId() {
	return discountId;
}
public void setDiscountId(String discountId) {
	this.discountId = discountId;
}
public String getStatusMessage() {
	return statusMessage;
}
public void setStatusMessage(String statusMessage) {
	this.statusMessage = statusMessage;
}



}
