/**
 *
 */
package com.arthvedi.organic.seller.representation.siren;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.productcatalog.representation.siren.SellerProductRepresentation;
import com.arthvedi.organic.seller.model.SellerBranchModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.annotations.Siren4JSubEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("sellerBranchRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "sellerBranch", uri = "/sellerBranchs/{id}")
@Representation(SellerBranchModel.class)
public class SellerBranchRepresentation extends BaseResource {

	private String id;
	private String sellerId;
	private String branchName;
	private String sellerEmailId;
	private String sellerPhoneNo;
	private String branchStatus;
	private String landlineNo;
	private String minimumOrderAmount;
	private String tinNumber;
	private String statusMessage;

	@Siren4JSubEntity
	private List<SellerBranchAddressRepresentation> sellerBranchAddressesRepresentation = new ArrayList<SellerBranchAddressRepresentation>(
			0);
	@Siren4JSubEntity
	private List<SellerBranchChargesRepresentation> sellerBranchChargesesRepresentation = new ArrayList<SellerBranchChargesRepresentation>(
			0);
	@Siren4JSubEntity
	private List<SellerBranchUserRepresentation> sellerBranchUsersRepresentation = new ArrayList<SellerBranchUserRepresentation>(
			0);
	@Siren4JSubEntity
	private List<SellerProductRepresentation> sellerProductsRepresentation = new ArrayList<SellerProductRepresentation>(
			0);
	@Siren4JSubEntity
	private List<SellerBranchBankAccountRepresentation> sellerBranchBankAccountsRepresentation = new ArrayList<SellerBranchBankAccountRepresentation>(
			0);
	@Siren4JSubEntity
	private List<SellerBranchMarginRepresentation> sellerBranchMarginsRepresentation = new ArrayList<SellerBranchMarginRepresentation>(
			0);
	@Siren4JSubEntity
	private Set<SellerBranchZoneRepresentation> sellerBranchZonesRepresentation = new HashSet<SellerBranchZoneRepresentation>(
			0);
	@Siren4JSubEntity
	private List<SellerBranchImagesRepresentation> sellerBranchImagesRepresentations = new ArrayList<SellerBranchImagesRepresentation>(
			0);

	public Set<SellerBranchZoneRepresentation> getSellerBranchZonesRepresentation() {
		return sellerBranchZonesRepresentation;
	}

	public void setSellerBranchZonesRepresentation(
			Set<SellerBranchZoneRepresentation> sellerBranchZonesRepresentation) {
		this.sellerBranchZonesRepresentation = sellerBranchZonesRepresentation;
	}

	public List<SellerBranchImagesRepresentation> getSellerBranchImagesRepresentations() {
		return sellerBranchImagesRepresentations;
	}

	public void setSellerBranchImagesRepresentations(
			List<SellerBranchImagesRepresentation> sellerBranchImagesRepresentations) {
		this.sellerBranchImagesRepresentations = sellerBranchImagesRepresentations;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public List<SellerBranchAddressRepresentation> getSellerBranchAddressesRepresentation() {
		return sellerBranchAddressesRepresentation;
	}

	public void setSellerBranchAddressesRepresentation(
			List<SellerBranchAddressRepresentation> sellerBranchAddressesRepresentation) {
		this.sellerBranchAddressesRepresentation = sellerBranchAddressesRepresentation;
	}

	public List<SellerBranchChargesRepresentation> getSellerBranchChargesesRepresentation() {
		return sellerBranchChargesesRepresentation;
	}

	public void setSellerBranchChargesesRepresentation(
			List<SellerBranchChargesRepresentation> sellerBranchChargesesRepresentation) {
		this.sellerBranchChargesesRepresentation = sellerBranchChargesesRepresentation;
	}

	public List<SellerBranchUserRepresentation> getSellerBranchUsersRepresentation() {
		return sellerBranchUsersRepresentation;
	}

	public void setSellerBranchUsersRepresentation(
			List<SellerBranchUserRepresentation> sellerBranchUsersRepresentation) {
		this.sellerBranchUsersRepresentation = sellerBranchUsersRepresentation;
	}

	public List<SellerProductRepresentation> getSellerProductsRepresentation() {
		return sellerProductsRepresentation;
	}

	public void setSellerProductsRepresentation(
			List<SellerProductRepresentation> sellerProductsRepresentation) {
		this.sellerProductsRepresentation = sellerProductsRepresentation;
	}

	public List<SellerBranchBankAccountRepresentation> getSellerBranchBankAccountsRepresentation() {
		return sellerBranchBankAccountsRepresentation;
	}

	public void setSellerBranchBankAccountsRepresentation(
			List<SellerBranchBankAccountRepresentation> sellerBranchBankAccountsRepresentation) {
		this.sellerBranchBankAccountsRepresentation = sellerBranchBankAccountsRepresentation;
	}

	public List<SellerBranchMarginRepresentation> getSellerBranchMarginsRepresentation() {
		return sellerBranchMarginsRepresentation;
	}

	public void setSellerBranchMarginsRepresentation(
			List<SellerBranchMarginRepresentation> sellerBranchMarginsRepresentation) {
		this.sellerBranchMarginsRepresentation = sellerBranchMarginsRepresentation;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSellerId() {
		return sellerId;
	}

	public void setSellerId(String sellerId) {
		this.sellerId = sellerId;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getSellerEmailId() {
		return sellerEmailId;
	}

	public void setSellerEmailId(String sellerEmailId) {
		this.sellerEmailId = sellerEmailId;
	}

	public String getSellerPhoneNo() {
		return sellerPhoneNo;
	}

	public void setSellerPhoneNo(String sellerPhoneNo) {
		this.sellerPhoneNo = sellerPhoneNo;
	}

	public String getBranchStatus() {
		return branchStatus;
	}

	public void setBranchStatus(String branchStatus) {
		this.branchStatus = branchStatus;
	}

	public String getLandlineNo() {
		return landlineNo;
	}

	public void setLandlineNo(String landlineNo) {
		this.landlineNo = landlineNo;
	}

	public String getMinimumOrderAmount() {
		return minimumOrderAmount;
	}

	public void setMinimumOrderAmount(String minimumOrderAmount) {
		this.minimumOrderAmount = minimumOrderAmount;
	}

	public String getTinNumber() {
		return tinNumber;
	}

	public void setTinNumber(String tinNumber) {
		this.tinNumber = tinNumber;
	}

}
