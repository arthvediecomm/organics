/**
 *
 */
package com.arthvedi.organic.seller.representation.siren;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.seller.model.SellerBranchImagesModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("sellerBranchImagesRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "sellerBranchImages", uri = "/sellerBranchImagess/{id}")
@Representation(SellerBranchImagesModel.class)
public class SellerBranchImagesRepresentation extends BaseResource {
	private String id;
	private String sellerBranchId;
	private String imageName;
	private String imageType;
	private String imageLocation;
	private String status;
	private String imagesStatus;
	private String statusMessage;
	private String imageFolder;

	public String getId() {
		return id;
	}

	public String getImageFolder() {
		return imageFolder;
	}

	public String getImageLocation() {
		return imageLocation;
	}

	public String getImageName() {
		return imageName;
	}

	public String getImageType() {
		return imageType;
	}

	public String getSellerBranchId() {
		return sellerBranchId;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public void setImageFolder(final String imageFolder) {
		this.imageFolder = imageFolder;
	}

	public void setImageLocation(final String imageLocation) {
		this.imageLocation = imageLocation;
	}

	public void setImageName(final String imageName) {
		this.imageName = imageName;
	}

	public void setImageType(final String imageType) {
		this.imageType = imageType;
	}

	public void setSellerBranchId(final String sellerBranchId) {
		this.sellerBranchId = sellerBranchId;
	}

	public String getImagesStatus() {
		return imagesStatus;
	}

	public void setImagesStatus(String imagesStatus) {
		this.imagesStatus = imagesStatus;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

}
