/**
 *
 */
package com.arthvedi.organic.seller.representation.siren;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.seller.model.SellerModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.annotations.Siren4JSubEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("sellerRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "seller", uri = "/sellers/{id}")
@Representation(SellerModel.class)
public class SellerRepresentation extends BaseResource {
	protected String id;
	private String sellerName;
	private String description;
	private String status;
	private String sellerType;
	private String statusMessage;

	@Siren4JSubEntity
	private List<SellerImagesRepresentation> sellerImagesesRepresentation = new ArrayList<SellerImagesRepresentation>(
			0);

	public List<SellerImagesRepresentation> getSellerImagesesRepresentation() {
		return sellerImagesesRepresentation;
	}

	public void setSellerImagesesRepresentation(
			List<SellerImagesRepresentation> sellerImagesesRepresentation) {
		this.sellerImagesesRepresentation = sellerImagesesRepresentation;
	}

	@Siren4JSubEntity
	private List<SellerBranchRepresentation> sellerBranchesRepresentation = new ArrayList<SellerBranchRepresentation>(
			0);

	public List<SellerBranchRepresentation> getSellerBranchesRepresentation() {
		return sellerBranchesRepresentation;
	}

	public void setSellerBranchesRepresentation(
			List<SellerBranchRepresentation> sellerBranchesRepresentation) {
		this.sellerBranchesRepresentation = sellerBranchesRepresentation;
	}

	public String getId() {
		return id;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSellerName() {
		return sellerName;
	}

	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSellerType() {
		return sellerType;
	}

	public void setSellerType(String sellerType) {
		this.sellerType = sellerType;
	}

}
