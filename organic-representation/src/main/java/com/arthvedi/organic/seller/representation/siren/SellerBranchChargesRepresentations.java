/**
 *
 */
package com.arthvedi.organic.seller.representation.siren;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.seller.model.SellerBranchChargesModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.CollectionResource;

/**
 * @author arthvedi
 *
 */
@Component("sellerBranchChargesRepresentations")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "sellerBranchChargess", uri = "/sellerBranchChargess")
@Representation(SellerBranchChargesModel.class)
public class SellerBranchChargesRepresentations extends CollectionResource<SellerBranchChargesRepresentation> {

}
