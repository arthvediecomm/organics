/**
 *
 */
package com.arthvedi.organic.seller.representation.siren;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.seller.model.SellerBranchTaxModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("sellerBranchTaxRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "sellerBranchTax", uri = "/sellerBranchTaxs/{id}")
@Representation(SellerBranchTaxModel.class)
public class SellerBranchTaxRepresentation extends BaseResource {
	private String id;
	private String taxId;
	private String sellerBranchId;
	private String status;
	private String statusMessage;
	private String taxName;
	private List<SellerBranchTaxableChargesRepresentation> sellerBranchTaxableChargesRep = new ArrayList<SellerBranchTaxableChargesRepresentation>(
			0);
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTaxId() {
		return taxId;
	}
	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}
	public String getSellerBranchId() {
		return sellerBranchId;
	}
	public void setSellerBranchId(String sellerBranchId) {
		this.sellerBranchId = sellerBranchId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	public List<SellerBranchTaxableChargesRepresentation> getSellerBranchTaxableChargesRep() {
		return sellerBranchTaxableChargesRep;
	}
	public void setSellerBranchTaxableChargesRep(
			List<SellerBranchTaxableChargesRepresentation> sellerBranchTaxableChargesRep) {
		this.sellerBranchTaxableChargesRep = sellerBranchTaxableChargesRep;
	}
	public String getTaxName() {
		return taxName;
	}
	public void setTaxName(String taxName) {
		this.taxName = taxName;
	}
	
	

}
