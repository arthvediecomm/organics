/**
 *
 */
package com.arthvedi.organic.seller.representation.siren;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.seller.model.SellerBranchTaxableChargesModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("sellerBranchTaxableChargesRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "sellerBranchTaxableCharges", uri = "/sellerBranchTaxableChargess/{id}")
@Representation(SellerBranchTaxableChargesModel.class)
public class SellerBranchTaxableChargesRepresentation extends BaseResource {
	private String id;
	private String sellerBranchTaxId;
	private String sellerBranchChargesId;
	private String status;
	private String statusMessage;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSellerBranchTaxId() {
		return sellerBranchTaxId;
	}
	public void setSellerBranchTaxId(String sellerBranchTaxId) {
		this.sellerBranchTaxId = sellerBranchTaxId;
	}
	public String getSellerBranchChargesId() {
		return sellerBranchChargesId;
	}
	public void setSellerBranchChargesId(String sellerBranchChargesId) {
		this.sellerBranchChargesId = sellerBranchChargesId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}


}
