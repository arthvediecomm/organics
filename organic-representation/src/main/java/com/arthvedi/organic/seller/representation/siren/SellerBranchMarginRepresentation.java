/**
 *
 */
package com.arthvedi.organic.seller.representation.siren;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.seller.model.SellerBranchMarginModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("sellerBranchMarginRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "sellerBranchMargin", uri = "/sellerBranchMargins/{id}")
@Representation(SellerBranchMarginModel.class)
public class SellerBranchMarginRepresentation extends BaseResource {

	private String id;
	private String sellerBranchId;
	private String margin;
	private String period;
	private String amount;
	private String amountType;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSellerBranchId() {
		return sellerBranchId;
	}

	public void setSellerBranchId(String sellerBranchId) {
		this.sellerBranchId = sellerBranchId;
	}

	public String getMargin() {
		return margin;
	}

	public void setMargin(String margin) {
		this.margin = margin;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getAmountType() {
		return amountType;
	}

	public void setAmountType(String amountType) {
		this.amountType = amountType;
	}

}
