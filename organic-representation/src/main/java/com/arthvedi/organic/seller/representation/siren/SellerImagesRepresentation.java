/**
 *
 */
package com.arthvedi.organic.seller.representation.siren;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.seller.model.SellerImagesModel;
import com.google.code.siren4j.annotations.Siren4JEntity;

/**
 * @author venky
 *
 */
@Component("sellerImagesRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "sellerImages", uri = "/sellerImageses/{id}")
@Representation(SellerImagesModel.class)
public class SellerImagesRepresentation {

	private String id;
	private String sellerId;
	private String sellerName;
	private String imageFolder;
	
	private String sellerType;

	

	
	public String getImageFolder() {
		return imageFolder;
	}


	public void setImageFolder(String imageFolder) {
		this.imageFolder = imageFolder;
	}


	public String getId() {
		return id;
	}

	
	public String getSellerId() {
		return sellerId;
	}

	public String getSellerName() {
		return sellerName;
	}

	public String getSellerType() {
		return sellerType;
	}

	public void setId(final String id) {
		this.id = id;
	}



	public void setSellerId(final String sellerId) {
		this.sellerId = sellerId;
	}

	public void setSellerName(final String sellerName) {
		this.sellerName = sellerName;
	}

	public void setSellerType(final String sellerType) {
		this.sellerType = sellerType;
	}

}
