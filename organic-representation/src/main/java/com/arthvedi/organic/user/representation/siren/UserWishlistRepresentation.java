/**
 *
 */
package com.arthvedi.organic.user.representation.siren;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.user.model.UserWishlistModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("userWishlistRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "userWishlist", uri = "/userWishlists/{id}")
@Representation(UserWishlistModel.class)
public class UserWishlistRepresentation extends BaseResource {

	private String id;
	private String description;
	private String ecommerceUserId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEcommerceUserId() {
		return ecommerceUserId;
	}

	public void setEcommerceUserId(String ecommerceUserId) {
		this.ecommerceUserId = ecommerceUserId;
	}

}
