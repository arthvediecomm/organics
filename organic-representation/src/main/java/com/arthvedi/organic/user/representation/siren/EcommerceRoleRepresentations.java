/**
 *
 */
package com.arthvedi.organic.user.representation.siren;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.user.model.EcommerceRoleModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.CollectionResource;

/**
 * @author arthvedi
 *
 */
@Component("ecommerceRoleRepresentations")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "ecommerceRoles", uri = "/ecommerceRoles")
@Representation(EcommerceRoleModel.class)
public class EcommerceRoleRepresentations extends CollectionResource<EcommerceRoleRepresentation> {

}
