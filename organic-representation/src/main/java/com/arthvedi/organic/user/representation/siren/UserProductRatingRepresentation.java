/**
 *
 */
package com.arthvedi.organic.user.representation.siren;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.user.model.UserProductRatingModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("userProductRatingRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "userProductRating", uri = "/userProductRatings/{id}")
@Representation(UserProductRatingModel.class)
public class UserProductRatingRepresentation extends BaseResource {

	private String id;
	private String rating;
	private String productRatingStatus;
	private String comments;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getProductRatingStatus() {
		return productRatingStatus;
	}

	public void setProductRatingStatus(String productRatingStatus) {
		this.productRatingStatus = productRatingStatus;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

}
