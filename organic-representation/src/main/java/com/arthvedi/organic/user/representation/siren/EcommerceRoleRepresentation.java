/**
 *
 */
package com.arthvedi.organic.user.representation.siren;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.representation.Representation;
import com.arthvedi.organic.user.model.EcommerceRoleModel;
import com.google.code.siren4j.annotations.Siren4JEntity;
import com.google.code.siren4j.annotations.Siren4JSubEntity;
import com.google.code.siren4j.resource.BaseResource;

/**
 * @author varma
 *
 */
@Component("ecommerceRoleRepresentation")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Siren4JEntity(name = "ecommerceRole", uri = "/ecommerceRoles/{id}")
@Representation(EcommerceRoleModel.class)
public class EcommerceRoleRepresentation extends BaseResource {

	private String id;
	private String roleName;
	private String status;
	@Siren4JSubEntity
	private List<EcommerceUserRepresentation> ecommerceUsersRepresentation = new ArrayList<EcommerceUserRepresentation>(
			0);

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<EcommerceUserRepresentation> getEcommerceUsersRepresentation() {
		return ecommerceUsersRepresentation;
	}

	public void setEcommerceUsersRepresentation(
			List<EcommerceUserRepresentation> ecommerceUsersRepresentation) {
		this.ecommerceUsersRepresentation = ecommerceUsersRepresentation;
	}

}
