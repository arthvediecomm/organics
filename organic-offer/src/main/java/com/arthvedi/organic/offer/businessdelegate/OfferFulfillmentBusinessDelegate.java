package com.arthvedi.organic.offer.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.offer.businessdelegate.context.OfferFulfillmentContext;
import com.arthvedi.organic.offer.domain.OfferFulfillment;
import com.arthvedi.organic.offer.model.OfferFulfillmentModel;
import com.arthvedi.organic.offer.service.IOfferFulfillmentService;

/*
 *@Author varma
 */

@Service
public class OfferFulfillmentBusinessDelegate
		implements
		IBusinessDelegate<OfferFulfillmentModel, OfferFulfillmentContext, IKeyBuilder<String>, String> {

	@Autowired
	private IOfferFulfillmentService offerFulfillmentService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public OfferFulfillmentModel create(OfferFulfillmentModel model) {
		validateModel(model);
		OfferFulfillment offerFulfillment = offerFulfillmentService
				.create((OfferFulfillment) conversionService.convert(model,
						forObject(model), valueOf(OfferFulfillment.class)));
		model = convertToOfferFulfillmentModel(offerFulfillment);
		model.setStatusMessage("SUCCESS::: Offer created successfully");
		return model;
	}

	private OfferFulfillmentModel convertToOfferFulfillmentModel(
			OfferFulfillment offerFulfillment) {
		return (OfferFulfillmentModel) conversionService.convert(
				offerFulfillment, forObject(offerFulfillment),
				valueOf(OfferFulfillmentModel.class));
	}

	private void validateModel(OfferFulfillmentModel model) {
		Validate.notNull(model, "Invalid Input");
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			OfferFulfillmentContext context) {
		throw new UnsupportedOperationException("Operation not implemented yet");
	}

	@Override
	public OfferFulfillmentModel edit(IKeyBuilder<String> keyBuilder,
			OfferFulfillmentModel model) {
		OfferFulfillment offerFulfillment = offerFulfillmentService
				.getOfferFulfillment(keyBuilder.build().toString());
		validateModel(model);
		offerFulfillment = offerFulfillmentService
				.updateOfferFulfillment((OfferFulfillment) conversionService
						.convert(model, forObject(model),
								valueOf(OfferFulfillment.class)));
		model = convertToOfferFulfillmentModel(offerFulfillment);
		model.setStatusMessage("SUCCESS::: Offer updated successfully");
		return model;
	}

	@Override
	public OfferFulfillmentModel getByKey(IKeyBuilder<String> keyBuilder,
			OfferFulfillmentContext context) {
		OfferFulfillment offerFulfillment = offerFulfillmentService
				.getOfferFulfillment(keyBuilder.build().toString());
		OfferFulfillmentModel model = conversionService.convert(
				offerFulfillment, OfferFulfillmentModel.class);
		return model;
	}

	@Override
	public Collection<OfferFulfillmentModel> getCollection(
			OfferFulfillmentContext context) {
		List<OfferFulfillment> offerFulfillment = new ArrayList<OfferFulfillment>();

		List<OfferFulfillmentModel> offerFulfillmentModels = (List<OfferFulfillmentModel>) conversionService
				.convert(offerFulfillment, TypeDescriptor
						.forObject(offerFulfillment), TypeDescriptor
						.collection(List.class, TypeDescriptor
								.valueOf(OfferFulfillmentModel.class)));
		return offerFulfillmentModels;
	}

	@Override
	public OfferFulfillmentModel edit(IKeyBuilder<String> keyBuilder,
			OfferFulfillmentModel model, OfferFulfillmentContext context) {
		return null;
	}

}
