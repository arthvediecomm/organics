package com.arthvedi.organic.offer.service;

import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.offer.domain.OfferAudit;
import com.arthvedi.organic.offer.domain.OfferConfig;
import com.arthvedi.organic.offer.domain.OfferExcludeConfig;
import com.arthvedi.organic.offer.domain.OfferFulfillment;
import com.arthvedi.organic.offer.repository.OfferFulfillmentRepository;
import com.arthvedi.organic.productcatalog.domain.Seo;
import com.arthvedi.organic.productcatalog.service.ISeoService;

/*
 *@Author varma
 */
@Component
public class OfferFulfillmentService implements IOfferFulfillmentService {
	@Autowired
	private IOfferExcludeConfigService offerExcludeConfigService;
	@Autowired
	private IOfferConfigService offerConfigService;
	@Autowired
	private OfferFulfillmentRepository offerFulfillmentRepository;
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private ISeoService seoService;

	@Override
	@Transactional
	public OfferFulfillment create(OfferFulfillment offerFulfillment) {
		offerFulfillment.setUserCreated("VARMA");
		offerFulfillment.setCreatedDate(new LocalDateTime());
		if (offerFulfillment.getSeo() != null) {
			Seo seo = seoService.create(offerFulfillment.getSeo());
			offerFulfillment.setSeo(seo);
		}
		offerFulfillment = offerFulfillmentRepository.save(offerFulfillment);

		if (offerFulfillment.getId() != null
				&& CollectionUtils.isNotEmpty(offerFulfillment
						.getOfferConfigs())) {
			offerFulfillment.setOfferConfigs(addOfferConfigs(offerFulfillment,
					offerFulfillment.getOfferConfigs()));
		}
		if (offerFulfillment.getId() != null
				&& CollectionUtils.isNotEmpty(offerFulfillment
						.getOfferExcludeConfigs())) {
			offerFulfillment
					.setOfferExcludeConfigs(addOfferExcludeConfigs(
							offerFulfillment,
							offerFulfillment.getOfferExcludeConfigs()));
		}
		if (offerFulfillment.getId() != null
				&& CollectionUtils
						.isNotEmpty(offerFulfillment.getOfferAudits())) {
			offerFulfillment.setOfferAudits(addOfferAudits(offerFulfillment,
					offerFulfillment.getOfferAudits()));
		}
		return offerFulfillment;
	}

	@Transactional
	private Set<OfferConfig> addOfferConfigs(OfferFulfillment offerFulfillment,
			Set<OfferConfig> offerConfigs) {
		Set<OfferConfig> offerconfgs = new HashSet<OfferConfig>();
		for (OfferConfig ofrcfgs : offerConfigs) {
			OfferConfig offrConfg = ofrcfgs;
			offrConfg.setOfferFulfillment(offerFulfillment);
			offrConfg = offerConfigService.create(offrConfg);
			offerconfgs.add(offrConfg);
		}
		return offerconfgs;
	}

	@Transactional
	private Set<OfferExcludeConfig> addOfferExcludeConfigs(
			OfferFulfillment offerFulfillment,
			Set<OfferExcludeConfig> offerExcludeConfigs) {
		Set<OfferExcludeConfig> offerExcludeconfgs = new HashSet<OfferExcludeConfig>();
		for (OfferExcludeConfig ofrexcfgs : offerExcludeConfigs) {
			OfferExcludeConfig offrxConfg = ofrexcfgs;
			offrxConfg.setOfferFulfillment(offerFulfillment);
			offrxConfg = offerExcludeConfigService.create(offrxConfg);
			offerExcludeconfgs.add(offrxConfg);
		}
		return offerExcludeconfgs;
	}

	@Transactional
	private Set<OfferAudit> addOfferAudits(OfferFulfillment offerFulfillment,
			Set<OfferAudit> offerAudits) {
		return null;
	}

	@Override
	public void deleteOfferFulfillment(String offerFulfillmentId) {
		throw new UnsupportedOperationException("Operation not implemented yet");
	}

	@Override
	public OfferFulfillment getOfferFulfillment(String offerFulfillmentId) {
		return offerFulfillmentRepository.findOne(offerFulfillmentId);
	}

	@Override
	public List<OfferFulfillment> getAll() {
		return (List<OfferFulfillment>) offerFulfillmentRepository.findAll();
	}

	@Override
	@Transactional
	public OfferFulfillment updateOfferFulfillment(
			OfferFulfillment offerFulfillment) {
		OfferFulfillment offerFulfillmnt = offerFulfillmentRepository
				.findOne(offerFulfillment.getId());
		try {
			nonNullBeanUtils.copyProperties(offerFulfillmnt, offerFulfillment);
		} catch (IllegalAccessException e) {
			e.printStackTrace();

		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		offerFulfillmnt.setModifiedDate(new LocalDateTime());
		offerFulfillmnt.setUserModified("Admin");

		offerFulfillment = offerFulfillmentRepository.save(offerFulfillmnt);
		return offerFulfillment;
	}

}
