package com.arthvedi.organic.offer.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;

/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class OfferAuditContext implements IBusinessDelegateContext {

	private String offerAuditId;
	private String offerAuditList;
	private String all;

	
	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

	public String getOfferAuditId() {
		return offerAuditId;
	}

	public void setOfferAuditId(String offerAuditId) {
		this.offerAuditId = offerAuditId;
	}

	public String getOfferAuditList() {
		return offerAuditList;
	}

	public void setOfferAuditList(String offerAuditList) {
		this.offerAuditList = offerAuditList;
	}

}
