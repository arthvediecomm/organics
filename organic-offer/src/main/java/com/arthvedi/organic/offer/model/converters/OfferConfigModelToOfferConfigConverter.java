/**
 *
 */
package com.arthvedi.organic.offer.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.commons.domain.City;
import com.arthvedi.organic.commons.domain.Zone;
import com.arthvedi.organic.offer.domain.OfferConfig;
import com.arthvedi.organic.offer.domain.OfferFulfillment;
import com.arthvedi.organic.offer.model.OfferConfigModel;
import com.arthvedi.organic.productcatalog.domain.Category;
import com.arthvedi.organic.productcatalog.domain.Product;
import com.arthvedi.organic.productcatalog.domain.SellerProduct;
import com.arthvedi.organic.seller.domain.Seller;
import com.arthvedi.organic.seller.domain.SellerBranch;

/**
 * @author Jay
 *
 */
@Component("offerConfigModelToOfferConfigConverter")
public class OfferConfigModelToOfferConfigConverter implements
		Converter<OfferConfigModel, OfferConfig> {
	@Autowired
	private ObjectFactory<OfferConfig> offerConfigFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public OfferConfig convert(final OfferConfigModel source) {
		OfferConfig offerConfig = offerConfigFactory.getObject();
		BeanUtils.copyProperties(source, offerConfig);
		if (source.getOfferFulfillmentId() != null) {
			OfferFulfillment offerFulfillment = new OfferFulfillment();
			offerFulfillment.setId(source.getOfferFulfillmentId());
			offerConfig.setOfferFulfillment(offerFulfillment);
		}

		if (source.getCategoryId() != null) {
			Category cat = new Category();
			cat.setId(source.getId());
			offerConfig.setCategory(cat);
		}
		if (source.getCityId() != null) {
			City city = new City();
			city.setId(source.getCityId());
			offerConfig.setCity(city);
		}
		if (source.getProductId() != null) {
			Product product = new Product();
			product.setId(source.getProductId());
			offerConfig.setProduct(product);
		}
		if (source.getSellerBranchId() != null) {
			SellerBranch sellerBranch = new SellerBranch();
			sellerBranch.setId(source.getSellerBranchId());
			offerConfig.setSellerBranch(sellerBranch);
		}
		if (source.getSellerId() != null) {
			Seller seller = new Seller();
			seller.setId(source.getSellerId());
			offerConfig.setSeller(seller);
		}
		if (source.getSellerProductId() != null) {
			SellerProduct sellerProduct = new SellerProduct();
			sellerProduct.setId(source.getSellerProductId());
			offerConfig.setSellerProduct(sellerProduct);
		}
		if (source.getZoneId() != null) {
			Zone zone = new Zone();
			zone.setId(source.getZoneId());
			offerConfig.setZone(zone);
		}
		return offerConfig;
	}

	@Autowired
	public void setOfferConfigFactory(
			final ObjectFactory<OfferConfig> offerConfigFactory) {
		this.offerConfigFactory = offerConfigFactory;
	}

}
