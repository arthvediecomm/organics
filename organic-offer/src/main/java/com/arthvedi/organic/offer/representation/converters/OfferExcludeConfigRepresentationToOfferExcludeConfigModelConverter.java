/**
 *
 */
package com.arthvedi.organic.offer.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.offer.model.OfferExcludeConfigModel;
import com.arthvedi.organic.offer.representation.siren.OfferExcludeConfigRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("offerExcludeConfigRepresentationToOfferExcludeConfigModelConverter")
public class OfferExcludeConfigRepresentationToOfferExcludeConfigModelConverter
		extends
		PropertyCopyingConverter<OfferExcludeConfigRepresentation, OfferExcludeConfigModel> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public OfferExcludeConfigModel convert(
			final OfferExcludeConfigRepresentation source) {

		OfferExcludeConfigModel target = super.convert(source);

		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(final ObjectFactory<OfferExcludeConfigModel> factory) {
		super.setFactory(factory);
	}

}
