package com.arthvedi.organic.offer.service;

import java.util.List;

import com.arthvedi.organic.offer.domain.OfferFulfillment;

/*
 *@Author varma
 */
public interface IOfferFulfillmentService {

	OfferFulfillment create(OfferFulfillment offerFulfillment);

	void deleteOfferFulfillment(String offerFulfillmentId);

	OfferFulfillment getOfferFulfillment(String offerFulfillmentId);

	List<OfferFulfillment> getAll();

	OfferFulfillment updateOfferFulfillment(OfferFulfillment offerFulfillment);
}
