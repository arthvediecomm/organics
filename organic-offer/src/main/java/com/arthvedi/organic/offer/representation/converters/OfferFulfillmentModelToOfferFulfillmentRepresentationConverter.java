/**
 *
 */
package com.arthvedi.organic.offer.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.offer.model.OfferFulfillmentModel;
import com.arthvedi.organic.offer.representation.siren.OfferFulfillmentRepresentation;

/**
 * @author varma
 *
 */
@Component("offerFulfillmentModelToOfferFulfillmentRepresentationConverter")
public class OfferFulfillmentModelToOfferFulfillmentRepresentationConverter
		extends
		PropertyCopyingConverter<OfferFulfillmentModel, OfferFulfillmentRepresentation> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public OfferFulfillmentRepresentation convert(
			final OfferFulfillmentModel source) {

		OfferFulfillmentRepresentation target = super.convert(source);

		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(
			final ObjectFactory<OfferFulfillmentRepresentation> factory) {
		super.setFactory(factory);
	}

}
