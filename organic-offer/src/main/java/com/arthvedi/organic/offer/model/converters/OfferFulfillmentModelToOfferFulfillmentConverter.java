/**
 *
 */
package com.arthvedi.organic.offer.model.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.organic.offer.domain.OfferAudit;
import com.arthvedi.organic.offer.domain.OfferConfig;
import com.arthvedi.organic.offer.domain.OfferExcludeConfig;
import com.arthvedi.organic.offer.domain.OfferFulfillment;
import com.arthvedi.organic.offer.model.OfferFulfillmentModel;
import com.arthvedi.organic.productcatalog.domain.Seo;

/**
 * @author Jay
 *
 */
@Component("offerFulfillmentModelToOfferFulfillmentConverter")
public class OfferFulfillmentModelToOfferFulfillmentConverter implements
		Converter<OfferFulfillmentModel, OfferFulfillment> {
	@Autowired
	private ObjectFactory<OfferFulfillment> offerFulfillmentFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public OfferFulfillment convert(final OfferFulfillmentModel source) {
		OfferFulfillment offerFulfillment = offerFulfillmentFactory.getObject();
		BeanUtils.copyProperties(source, offerFulfillment);
		if (source.getSeoTitle() != null) {
			Seo seo = new Seo();
			seo.setSeoTitle(source.getSeoTitle());
			seo.setSeoKeyword(source.getSeoKeywords());
			seo.setSeoDescription(source.getSeoDescription());
			if (source.getSeoId() != null) {
				seo.setId(source.getSeoId());
			}
			offerFulfillment.setSeo(seo);

		}
		if (CollectionUtils.isNotEmpty(source.getOfferConfigModel())) {
			List<OfferConfig> converted = (List<OfferConfig>) conversionService
					.convert(source.getOfferConfigModel(), TypeDescriptor
							.forObject(source.getOfferConfigModel()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									OfferConfig.class));
			offerFulfillment.getOfferConfigs().addAll(converted);
		}
		if (CollectionUtils.isNotEmpty(source.getOfferExcludeConfigModel())) {
			List<OfferExcludeConfig> converted = (List<OfferExcludeConfig>) conversionService
					.convert(source.getOfferExcludeConfigModel(),
							TypeDescriptor.forObject(source
									.getOfferExcludeConfigModel()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									OfferExcludeConfig.class));
			offerFulfillment.getOfferExcludeConfigs().addAll(converted);
		}
		if (CollectionUtils.isNotEmpty(source.getOfferAuditModel())) {
			List<OfferAudit> converted = (List<OfferAudit>) conversionService
					.convert(source.getOfferAuditModel(), TypeDescriptor
							.forObject(source.getOfferAuditModel()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									OfferAudit.class));
			offerFulfillment.getOfferAudits().addAll(converted);
		}
		return offerFulfillment;
	}

	@Autowired
	public void setOfferFulfillmentFactory(
			final ObjectFactory<OfferFulfillment> offerFulfillmentFactory) {
		this.offerFulfillmentFactory = offerFulfillmentFactory;
	}

}
