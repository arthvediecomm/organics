package com.arthvedi.organic.offer.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.offer.businessdelegate.context.OfferAuditContext;
import com.arthvedi.organic.offer.domain.OfferAudit;
import com.arthvedi.organic.offer.model.OfferAuditModel;
import com.arthvedi.organic.offer.service.IOfferAuditService;

/*
*@Author varma
*/

@Service
public class OfferAuditBusinessDelegate
		implements IBusinessDelegate<OfferAuditModel, OfferAuditContext, IKeyBuilder<String>, String> {

	@Autowired
	private IOfferAuditService offerAuditService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public OfferAuditModel create(OfferAuditModel model) {
validateModel(model);
		
		OfferAudit offerAudit = offerAuditService
				.create((OfferAudit) conversionService.convert(model, forObject(model), valueOf(OfferAudit.class)));
		model = convertToOfferAuditModel(offerAudit);
		model.setStatusMessage("SUCCESS::: OfferAudit Created Successfully.");
		
		return model;
	}

	private OfferAuditModel convertToOfferAuditModel(OfferAudit offerAudit) {
		
		return (OfferAuditModel) conversionService.convert(offerAudit, forObject(offerAudit),
				valueOf(OfferAuditModel.class));
	}

	private void validateModel(OfferAuditModel model) {
		
		Validate.notNull(model, "Invalid Input");		
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, OfferAuditContext context) {

	}

	@Override
	public OfferAuditModel edit(IKeyBuilder<String> keyBuilder, OfferAuditModel model) {
		OfferAudit offerAudit = offerAuditService.getOfferAudit(keyBuilder.build().toString());
		
		offerAudit = offerAuditService.updateOfferAudit(offerAudit);
		
		return model;
	}

	@Override
	public OfferAuditModel getByKey(IKeyBuilder<String> keyBuilder, OfferAuditContext context) {
		OfferAudit offerAudit = offerAuditService.getOfferAudit(keyBuilder.build().toString());
		OfferAuditModel model = conversionService.convert(offerAudit, OfferAuditModel.class);
		return model;
	}

	@Override
	public Collection<OfferAuditModel> getCollection(OfferAuditContext context) {
		List<OfferAudit> offerAudit = new ArrayList<OfferAudit>();
		
		List<OfferAuditModel> offerAuditModels = (List<OfferAuditModel>) conversionService.convert(
				offerAudit, TypeDescriptor.forObject(offerAudit),
				TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(OfferAuditModel.class)));
		return offerAuditModels;
	}

	@Override
	public OfferAuditModel edit(IKeyBuilder<String> keyBuilder,
			OfferAuditModel model, OfferAuditContext context) {
		return null;
	}

}
