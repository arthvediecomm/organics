/**
 *
 */
package com.arthvedi.organic.offer.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.offer.model.OfferExcludeConfigModel;
import com.arthvedi.organic.offer.representation.siren.OfferExcludeConfigRepresentation;

/**
 * @author varma
 *
 */
@Component("offerExcludeConfigModelToOfferExcludeConfigRepresentationConverter")
public class OfferExcludeConfigModelToOfferExcludeConfigRepresentationConverter
		extends
		PropertyCopyingConverter<OfferExcludeConfigModel, OfferExcludeConfigRepresentation> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public OfferExcludeConfigRepresentation convert(
			final OfferExcludeConfigModel source) {

		OfferExcludeConfigRepresentation target = super.convert(source);

		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(
			final ObjectFactory<OfferExcludeConfigRepresentation> factory) {
		super.setFactory(factory);
	}

}
