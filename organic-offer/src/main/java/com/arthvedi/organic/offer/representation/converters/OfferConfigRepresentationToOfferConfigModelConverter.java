/**
 *
 */
package com.arthvedi.organic.offer.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.offer.model.OfferConfigModel;
import com.arthvedi.organic.offer.representation.siren.OfferConfigRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("offerConfigRepresentationToOfferConfigModelConverter")
public class OfferConfigRepresentationToOfferConfigModelConverter extends
		PropertyCopyingConverter<OfferConfigRepresentation, OfferConfigModel> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public OfferConfigModel convert(final OfferConfigRepresentation source) {

		OfferConfigModel target = super.convert(source);

		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(final ObjectFactory<OfferConfigModel> factory) {
		super.setFactory(factory);
	}

}
