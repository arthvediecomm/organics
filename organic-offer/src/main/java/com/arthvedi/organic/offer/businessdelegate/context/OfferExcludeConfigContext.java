package com.arthvedi.organic.offer.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;

/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class OfferExcludeConfigContext implements IBusinessDelegateContext {

	private String offerExcludeConfigId;
	private String offerExcludeConfigList;
	private String all;

	public String getOfferExcludeConfigId() {
		return offerExcludeConfigId;
	}

	public void setOfferExcludeConfigId(String offerExcludeConfigId) {
		this.offerExcludeConfigId = offerExcludeConfigId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

	public String getOfferExcludeConfigList() {
		return offerExcludeConfigList;
	}

	public void setOfferExcludeConfigList(String offerExcludeConfigList) {
		this.offerExcludeConfigList = offerExcludeConfigList;
	}

}
