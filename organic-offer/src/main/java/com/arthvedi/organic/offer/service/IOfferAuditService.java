package com.arthvedi.organic.offer.service;

import java.util.List;

import com.arthvedi.organic.offer.domain.OfferAudit;

/*
 *@Author varma
 */
public interface IOfferAuditService {

	OfferAudit create(OfferAudit offerAudit);

	void deleteOfferAudit(String offerAuditId);

	OfferAudit getOfferAudit(String offerAuditId);

	List<OfferAudit> getAll();

	OfferAudit updateOfferAudit(OfferAudit offerAudit);
}
