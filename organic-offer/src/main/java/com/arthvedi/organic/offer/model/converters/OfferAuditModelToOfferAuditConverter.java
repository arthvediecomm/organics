/**
 *
 */
package com.arthvedi.organic.offer.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.offer.domain.OfferAudit;
import com.arthvedi.organic.offer.domain.OfferFulfillment;
import com.arthvedi.organic.offer.model.OfferAuditModel;
import com.arthvedi.organic.user.domain.EcommerceUser;

/**
 * @author Jay
 *
 */
@Component("offerAuditModelToOfferAuditConverter")
public class OfferAuditModelToOfferAuditConverter implements
		Converter<OfferAuditModel, OfferAudit> {
	@Autowired
	private ObjectFactory<OfferAudit> offerAuditFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public OfferAudit convert(final OfferAuditModel source) {
		OfferAudit offerAudit = offerAuditFactory.getObject();
		BeanUtils.copyProperties(source, offerAudit);
		if (source.getOfferFulfillmentId() != null) {
			OfferFulfillment offerFulfillment = new OfferFulfillment();
			offerFulfillment.setId(source.getOfferFulfillmentId());
			offerAudit.setOfferFulfillment(offerFulfillment);
		}

		if (source.getEcommUserId() != null) {
			EcommerceUser ecommUser = new EcommerceUser();
			ecommUser.setId(source.getEcommUserId());
			offerAudit.setEcommUser(ecommUser);
		}
		return offerAudit;
	}

	@Autowired
	public void setOfferAuditFactory(
			final ObjectFactory<OfferAudit> offerAuditFactory) {
		this.offerAuditFactory = offerAuditFactory;
	}

}
