package com.arthvedi.organic.offer.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.offer.domain.OfferExcludeConfig;
import com.arthvedi.organic.offer.model.OfferExcludeConfigModel;

@Component("offerExcludeConfigToOfferExcludeConfigModelConverter")
public class OfferExcludeConfigToOfferExcludeConfigModelConverter implements
		Converter<OfferExcludeConfig, OfferExcludeConfigModel> {
	@Autowired
	private ObjectFactory<OfferExcludeConfigModel> offerExcludeConfigModelFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public OfferExcludeConfigModel convert(final OfferExcludeConfig source) {
		OfferExcludeConfigModel offerExcludeConfigModel = offerExcludeConfigModelFactory
				.getObject();
		BeanUtils.copyProperties(source, offerExcludeConfigModel);

		return offerExcludeConfigModel;
	}

	@Autowired
	public void setOfferExcludeConfigModelFactory(
			final ObjectFactory<OfferExcludeConfigModel> offerExcludeConfigModelFactory) {
		this.offerExcludeConfigModelFactory = offerExcludeConfigModelFactory;
	}
}
