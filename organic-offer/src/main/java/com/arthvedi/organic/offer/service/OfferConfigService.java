package com.arthvedi.organic.offer.service;

import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.arthvedi.organic.offer.domain.OfferConfig;
import com.arthvedi.organic.offer.repository.OfferConfigRepository;

/*
 *@Author varma
 */
@Component
public class OfferConfigService implements IOfferConfigService {

	@Autowired
	private OfferConfigRepository offerConfigRepository;

	@Override
	@Transactional
	public OfferConfig create(OfferConfig offerConfig) {
		offerConfig.setCreatedDate(new LocalDateTime());
		offerConfig.setUserCreated("Dinakar");
		return offerConfigRepository.save(offerConfig);
	}

	@Override
	public void deleteOfferConfig(String offerConfigId) {

	}

	@Override
	public OfferConfig getOfferConfig(String offerConfigId) {
		return offerConfigRepository.findOne(offerConfigId);
	}

	@Override
	public List<OfferConfig> getAll() {
		return null;
	}

	@Override
	public OfferConfig updateOfferConfig(OfferConfig offerConfig) {
		return offerConfigRepository.save(offerConfig);
	}

}
