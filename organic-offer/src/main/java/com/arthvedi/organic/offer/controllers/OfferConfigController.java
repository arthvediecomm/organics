package com.arthvedi.organic.offer.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.offer.businessdelegate.context.OfferConfigContext;
import com.arthvedi.organic.offer.model.OfferConfigModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/offerConfig", produces = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class OfferConfigController {

	private IBusinessDelegate<OfferConfigModel, OfferConfigContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<OfferConfigContext> offerConfigContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create")
	public ResponseEntity<OfferConfigModel> createOfferConfig(
			@RequestBody OfferConfigModel offerConfigModel) {
		offerConfigModel = businessDelegate.create(offerConfigModel);
		return new ResponseEntity<OfferConfigModel>(offerConfigModel,
				HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	public ResponseEntity<OfferConfigModel> edit(
			@PathVariable(value = "id") final String offerConfigId,
			@RequestBody OfferConfigModel offerConfigModel) {
		offerConfigModel = businessDelegate.edit(keyBuilderFactory.getObject()
				.withId(offerConfigId), offerConfigModel);
		return new ResponseEntity<OfferConfigModel>(offerConfigModel,
				HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all")
	public ResponseEntity<IModelWrapper<Collection<OfferConfigModel>>> getAll() {
		OfferConfigContext offerConfigContext = offerConfigContextFactory
				.getObject();
		offerConfigContext.setAll("all");
		Collection<OfferConfigModel> offerConfigModels = businessDelegate
				.getCollection(offerConfigContext);
		IModelWrapper<Collection<OfferConfigModel>> models = new CollectionModelWrapper<OfferConfigModel>(
				OfferConfigModel.class, offerConfigModels);
		return new ResponseEntity<IModelWrapper<Collection<OfferConfigModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = { "/offerConfiglist",
			"/sellerapp/offerConfiglist" })
	public ResponseEntity<IModelWrapper<Collection<OfferConfigModel>>> getAllOfferConfigList() {
		OfferConfigContext offerConfigContext = offerConfigContextFactory
				.getObject();
		offerConfigContext.setOfferConfigList("offerConfigList");
		Collection<OfferConfigModel> offerConfigModels = businessDelegate
				.getCollection(offerConfigContext);
		IModelWrapper<Collection<OfferConfigModel>> models = new CollectionModelWrapper<OfferConfigModel>(
				OfferConfigModel.class, offerConfigModels);
		return new ResponseEntity<IModelWrapper<Collection<OfferConfigModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<OfferConfigModel> getOfferConfig(
			@PathVariable(value = "id") final String offerConfigId) {
		OfferConfigContext offerConfigContext = offerConfigContextFactory
				.getObject();

		OfferConfigModel model = businessDelegate.getByKey(keyBuilderFactory
				.getObject().withId(offerConfigId), offerConfigContext);
		return new ResponseEntity<OfferConfigModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "offerConfigBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<OfferConfigModel, OfferConfigContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setOfferConfigObjectFactory(
			final ObjectFactory<OfferConfigContext> offerConfigContextFactory) {
		this.offerConfigContextFactory = offerConfigContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(
			final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}
}
