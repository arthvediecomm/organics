package com.arthvedi.organic.offer.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.offer.businessdelegate.context.OfferFulfillmentContext;
import com.arthvedi.organic.offer.model.OfferFulfillmentModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/offerfulfillment", produces = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class OfferFulfillmentController {

	private IBusinessDelegate<OfferFulfillmentModel, OfferFulfillmentContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<OfferFulfillmentContext> offerFulfillmentContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create")
	public ResponseEntity<OfferFulfillmentModel> createOfferFulfillment(
			@RequestBody OfferFulfillmentModel offerFulfillmentModel) {
		offerFulfillmentModel = businessDelegate.create(offerFulfillmentModel);
		return new ResponseEntity<OfferFulfillmentModel>(offerFulfillmentModel,
				HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	public ResponseEntity<OfferFulfillmentModel> edit(
			@PathVariable(value = "id") final String offerFulfillmentId,
			@RequestBody OfferFulfillmentModel offerFulfillmentModel) {
		offerFulfillmentModel = businessDelegate.edit(keyBuilderFactory
				.getObject().withId(offerFulfillmentId), offerFulfillmentModel);
		return new ResponseEntity<OfferFulfillmentModel>(offerFulfillmentModel,
				HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all")
	public ResponseEntity<IModelWrapper<Collection<OfferFulfillmentModel>>> getAll() {
		OfferFulfillmentContext offerFulfillmentContext = offerFulfillmentContextFactory
				.getObject();
		offerFulfillmentContext.setAll("all");
		Collection<OfferFulfillmentModel> offerFulfillmentModels = businessDelegate
				.getCollection(offerFulfillmentContext);
		IModelWrapper<Collection<OfferFulfillmentModel>> models = new CollectionModelWrapper<OfferFulfillmentModel>(
				OfferFulfillmentModel.class, offerFulfillmentModels);
		return new ResponseEntity<IModelWrapper<Collection<OfferFulfillmentModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = {
			"/offerFulfillmentlist", "/sellerapp/offerFulfillmentlist" })
	public ResponseEntity<IModelWrapper<Collection<OfferFulfillmentModel>>> getAllOfferFulfillmentList() {
		OfferFulfillmentContext offerFulfillmentContext = offerFulfillmentContextFactory
				.getObject();
		offerFulfillmentContext.setOfferFulfillmentList("offerFulfillmentList");
		Collection<OfferFulfillmentModel> offerFulfillmentModels = businessDelegate
				.getCollection(offerFulfillmentContext);
		IModelWrapper<Collection<OfferFulfillmentModel>> models = new CollectionModelWrapper<OfferFulfillmentModel>(
				OfferFulfillmentModel.class, offerFulfillmentModels);
		return new ResponseEntity<IModelWrapper<Collection<OfferFulfillmentModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<OfferFulfillmentModel> getOfferFulfillment(
			@PathVariable(value = "id") final String offerFulfillmentId) {
		OfferFulfillmentContext offerFulfillmentContext = offerFulfillmentContextFactory
				.getObject();

		OfferFulfillmentModel model = businessDelegate.getByKey(
				keyBuilderFactory.getObject().withId(offerFulfillmentId),
				offerFulfillmentContext);
		return new ResponseEntity<OfferFulfillmentModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "offerFulfillmentBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<OfferFulfillmentModel, OfferFulfillmentContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setOfferFulfillmentObjectFactory(
			final ObjectFactory<OfferFulfillmentContext> offerFulfillmentContextFactory) {
		this.offerFulfillmentContextFactory = offerFulfillmentContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(
			final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}
}
