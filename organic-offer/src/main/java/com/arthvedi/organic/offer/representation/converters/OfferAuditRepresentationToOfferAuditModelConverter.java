/**
 *
 */
package com.arthvedi.organic.offer.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.offer.model.OfferAuditModel;
import com.arthvedi.organic.offer.representation.siren.OfferAuditRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("offerAuditRepresentationToOfferAuditModelConverter")
public class OfferAuditRepresentationToOfferAuditModelConverter extends
		PropertyCopyingConverter<OfferAuditRepresentation, OfferAuditModel> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public OfferAuditModel convert(final OfferAuditRepresentation source) {

		OfferAuditModel target = super.convert(source);

		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(final ObjectFactory<OfferAuditModel> factory) {
		super.setFactory(factory);
	}

}
