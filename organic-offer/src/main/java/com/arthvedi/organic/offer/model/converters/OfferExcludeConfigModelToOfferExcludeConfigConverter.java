/**
 *
 */
package com.arthvedi.organic.offer.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.commons.domain.City;
import com.arthvedi.organic.commons.domain.Zone;
import com.arthvedi.organic.offer.domain.OfferExcludeConfig;
import com.arthvedi.organic.offer.domain.OfferFulfillment;
import com.arthvedi.organic.offer.model.OfferExcludeConfigModel;
import com.arthvedi.organic.productcatalog.domain.Category;
import com.arthvedi.organic.productcatalog.domain.Product;
import com.arthvedi.organic.productcatalog.domain.SellerProduct;
import com.arthvedi.organic.seller.domain.Seller;
import com.arthvedi.organic.seller.domain.SellerBranch;

/**
 * @author Jay
 *
 */
@Component("offerExcludeConfigModelToOfferExcludeConfigConverter")
public class OfferExcludeConfigModelToOfferExcludeConfigConverter implements
		Converter<OfferExcludeConfigModel, OfferExcludeConfig> {
	@Autowired
	private ObjectFactory<OfferExcludeConfig> offerExcludeConfigFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public OfferExcludeConfig convert(final OfferExcludeConfigModel source) {
		OfferExcludeConfig offerExcludeConfig = offerExcludeConfigFactory
				.getObject();
		BeanUtils.copyProperties(source, offerExcludeConfig);

		if (source.getOfferFulfillmentId() != null) {
			OfferFulfillment offerFulfillment = new OfferFulfillment();
			offerFulfillment.setId(source.getOfferFulfillmentId());
			offerExcludeConfig.setOfferFulfillment(offerFulfillment);
		}
		if (source.getCategoryId() != null) {
			Category cat = new Category();
			cat.setId(source.getId());
			offerExcludeConfig.setCategory(cat);
		}
		if (source.getCityId() != null) {
			City city = new City();
			city.setId(source.getCityId());
			offerExcludeConfig.setCity(city);
		}
		if (source.getProductId() != null) {
			Product product = new Product();
			product.setId(source.getProductId());
			offerExcludeConfig.setProduct(product);
		}
		if (source.getSellerBranchId() != null) {
			SellerBranch sellerBranch = new SellerBranch();
			sellerBranch.setId(source.getSellerBranchId());
			offerExcludeConfig.setSellerBranch(sellerBranch);
		}
		if (source.getSellerId() != null) {
			Seller seller = new Seller();
			seller.setId(source.getSellerId());
			offerExcludeConfig.setSeller(seller);
		}
		if (source.getSellerProductId() != null) {
			SellerProduct sellerProduct = new SellerProduct();
			sellerProduct.setId(source.getSellerProductId());
			offerExcludeConfig.setSellerProduct(sellerProduct);
		}
		if (source.getZoneId() != null) {
			Zone zone = new Zone();
			zone.setId(source.getZoneId());
			offerExcludeConfig.setZone(zone);
		}
		return offerExcludeConfig;
	}

	@Autowired
	public void setOfferExcludeConfigFactory(
			final ObjectFactory<OfferExcludeConfig> offerExcludeConfigFactory) {
		this.offerExcludeConfigFactory = offerExcludeConfigFactory;
	}

}
