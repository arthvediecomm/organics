package com.arthvedi.organic.offer.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.offer.businessdelegate.context.OfferExcludeConfigContext;
import com.arthvedi.organic.offer.model.OfferExcludeConfigModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/offerExcludeConfig", produces = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class OfferExcludeConfigController {

	private IBusinessDelegate<OfferExcludeConfigModel, OfferExcludeConfigContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<OfferExcludeConfigContext> offerExcludeConfigContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create")
	public ResponseEntity<OfferExcludeConfigModel> createOfferExcludeConfig(
			@RequestBody OfferExcludeConfigModel offerExcludeConfigModel) {
		offerExcludeConfigModel = businessDelegate
				.create(offerExcludeConfigModel);
		return new ResponseEntity<OfferExcludeConfigModel>(
				offerExcludeConfigModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	public ResponseEntity<OfferExcludeConfigModel> edit(
			@PathVariable(value = "id") final String offerExcludeConfigId,
			@RequestBody OfferExcludeConfigModel offerExcludeConfigModel) {
		offerExcludeConfigModel = businessDelegate.edit(keyBuilderFactory
				.getObject().withId(offerExcludeConfigId),
				offerExcludeConfigModel);
		return new ResponseEntity<OfferExcludeConfigModel>(
				offerExcludeConfigModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all")
	public ResponseEntity<IModelWrapper<Collection<OfferExcludeConfigModel>>> getAll() {
		OfferExcludeConfigContext offerExcludeConfigContext = offerExcludeConfigContextFactory
				.getObject();
		offerExcludeConfigContext.setAll("all");
		Collection<OfferExcludeConfigModel> offerExcludeConfigModels = businessDelegate
				.getCollection(offerExcludeConfigContext);
		IModelWrapper<Collection<OfferExcludeConfigModel>> models = new CollectionModelWrapper<OfferExcludeConfigModel>(
				OfferExcludeConfigModel.class, offerExcludeConfigModels);
		return new ResponseEntity<IModelWrapper<Collection<OfferExcludeConfigModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = {
			"/offerExcludeConfiglist", "/sellerapp/offerExcludeConfiglist" })
	public ResponseEntity<IModelWrapper<Collection<OfferExcludeConfigModel>>> getAllOfferExcludeConfigList() {
		OfferExcludeConfigContext offerExcludeConfigContext = offerExcludeConfigContextFactory
				.getObject();
		offerExcludeConfigContext
				.setOfferExcludeConfigList("offerExcludeConfigList");
		Collection<OfferExcludeConfigModel> offerExcludeConfigModels = businessDelegate
				.getCollection(offerExcludeConfigContext);
		IModelWrapper<Collection<OfferExcludeConfigModel>> models = new CollectionModelWrapper<OfferExcludeConfigModel>(
				OfferExcludeConfigModel.class, offerExcludeConfigModels);
		return new ResponseEntity<IModelWrapper<Collection<OfferExcludeConfigModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<OfferExcludeConfigModel> getOfferExcludeConfig(
			@PathVariable(value = "id") final String offerExcludeConfigId) {
		OfferExcludeConfigContext offerExcludeConfigContext = offerExcludeConfigContextFactory
				.getObject();

		OfferExcludeConfigModel model = businessDelegate.getByKey(
				keyBuilderFactory.getObject().withId(offerExcludeConfigId),
				offerExcludeConfigContext);
		return new ResponseEntity<OfferExcludeConfigModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "offerExcludeConfigBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<OfferExcludeConfigModel, OfferExcludeConfigContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setOfferExcludeConfigObjectFactory(
			final ObjectFactory<OfferExcludeConfigContext> offerExcludeConfigContextFactory) {
		this.offerExcludeConfigContextFactory = offerExcludeConfigContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(
			final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}
}
