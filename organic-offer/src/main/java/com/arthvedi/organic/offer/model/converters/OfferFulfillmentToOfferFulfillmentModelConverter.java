package com.arthvedi.organic.offer.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.offer.domain.OfferFulfillment;
import com.arthvedi.organic.offer.model.OfferFulfillmentModel;

@Component("offerFulfillmentToOfferFulfillmentModelConverter")
public class OfferFulfillmentToOfferFulfillmentModelConverter implements
		Converter<OfferFulfillment, OfferFulfillmentModel> {
	@Autowired
	private ObjectFactory<OfferFulfillmentModel> offerFulfillmentModelFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public OfferFulfillmentModel convert(final OfferFulfillment source) {
		OfferFulfillmentModel offerFulfillmentModel = offerFulfillmentModelFactory
				.getObject();
		BeanUtils.copyProperties(source, offerFulfillmentModel);

		return offerFulfillmentModel;
	}

	@Autowired
	public void setOfferFulfillmentModelFactory(
			final ObjectFactory<OfferFulfillmentModel> offerFulfillmentModelFactory) {
		this.offerFulfillmentModelFactory = offerFulfillmentModelFactory;
	}
}
