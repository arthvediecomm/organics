package com.arthvedi.organic.offer.service;

import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.arthvedi.organic.offer.domain.OfferExcludeConfig;
import com.arthvedi.organic.offer.repository.OfferExcludeConfigRepository;

/*
 *@Author varma
 */
@Component
public class OfferExcludeConfigService implements IOfferExcludeConfigService {

	@Autowired
	private OfferExcludeConfigRepository offerExcludeConfigRepository;

	@Override
	@Transactional
	public OfferExcludeConfig create(OfferExcludeConfig offerExcludeConfig) {
		offerExcludeConfig.setUserCreated("Dinakar");
		offerExcludeConfig.setCreatedDate(new LocalDateTime());
		return offerExcludeConfigRepository.save(offerExcludeConfig);
	}

	@Override
	public void deleteOfferExcludeConfig(String offerExcludeConfigId) {
		// TODO Auto-generated method stub

	}

	@Override
	public OfferExcludeConfig getOfferExcludeConfig(String offerExcludeConfigId) {
		// TODO Auto-generated method stub
		return offerExcludeConfigRepository.findOne(offerExcludeConfigId);
	}

	@Override
	public List<OfferExcludeConfig> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OfferExcludeConfig updateOfferExcludeConfig(
			OfferExcludeConfig offerExcludeConfig) {
		// TODO Auto-generated method stub
		return offerExcludeConfigRepository.save(offerExcludeConfig);
	}

}
