package com.arthvedi.organic.offer.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;

/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class OfferConfigContext implements IBusinessDelegateContext {

	private String offerConfigId;
	private String all;
	private String offerConfigList;

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

	public String getOfferConfigId() {
		return offerConfigId;
	}

	public void setOfferConfigId(String offerConfigId) {
		this.offerConfigId = offerConfigId;
	}

	public String getOfferConfigList() {
		return offerConfigList;
	}

	public void setOfferConfigList(String offerConfigList) {
		this.offerConfigList = offerConfigList;
	}

}
