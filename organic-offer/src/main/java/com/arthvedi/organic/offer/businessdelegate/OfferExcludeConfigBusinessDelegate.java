package com.arthvedi.organic.offer.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.offer.businessdelegate.context.OfferExcludeConfigContext;
import com.arthvedi.organic.offer.domain.OfferExcludeConfig;
import com.arthvedi.organic.offer.model.OfferExcludeConfigModel;
import com.arthvedi.organic.offer.service.IOfferExcludeConfigService;

/*
*@Author varma
*/

@Service
public class OfferExcludeConfigBusinessDelegate
		implements IBusinessDelegate<OfferExcludeConfigModel, OfferExcludeConfigContext, IKeyBuilder<String>, String> {

	@Autowired
	private IOfferExcludeConfigService offerExcludeConfigService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public OfferExcludeConfigModel create(OfferExcludeConfigModel model) {
validateModel(model);
		
OfferExcludeConfig offerExcludeConfig = offerExcludeConfigService
				.create((OfferExcludeConfig) conversionService.convert(model, forObject(model), valueOf(OfferExcludeConfig.class)));
		model = convertToOfferExcludeConfigModel(offerExcludeConfig);
		model.setStatusMessage("SUCCESS::: OfferExcludeConfig Created Successfully.");
		
		return model;
	}

	private OfferExcludeConfigModel convertToOfferExcludeConfigModel(OfferExcludeConfig offerExcludeConfig) {
		return (OfferExcludeConfigModel) conversionService.convert(offerExcludeConfig, forObject(offerExcludeConfig),
				valueOf(OfferExcludeConfigModel.class));
	}

	private void validateModel(OfferExcludeConfigModel model) {
		
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, OfferExcludeConfigContext context) {

	}

	@Override
	public OfferExcludeConfigModel edit(IKeyBuilder<String> keyBuilder, OfferExcludeConfigModel model) {
		OfferExcludeConfig offerExcludeConfig = offerExcludeConfigService.getOfferExcludeConfig(keyBuilder.build().toString());
		
		offerExcludeConfig = offerExcludeConfigService.updateOfferExcludeConfig(offerExcludeConfig);
		
		return model;
	}

	@Override
	public OfferExcludeConfigModel getByKey(IKeyBuilder<String> keyBuilder, OfferExcludeConfigContext context) {
		OfferExcludeConfig offerExcludeConfig = offerExcludeConfigService.getOfferExcludeConfig(keyBuilder.build().toString());
		OfferExcludeConfigModel model = conversionService.convert(offerExcludeConfig, OfferExcludeConfigModel.class);
		return model;
	}

	@Override
	public Collection<OfferExcludeConfigModel> getCollection(OfferExcludeConfigContext context) {
		List<OfferExcludeConfig> offerExcludeConfig = new ArrayList<OfferExcludeConfig>();
		
		List<OfferExcludeConfigModel> offerExcludeConfigModels = (List<OfferExcludeConfigModel>) conversionService.convert(
				offerExcludeConfig, TypeDescriptor.forObject(offerExcludeConfig),
				TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(OfferExcludeConfigModel.class)));
		return offerExcludeConfigModels;
	}

	@Override
	public OfferExcludeConfigModel edit(IKeyBuilder<String> keyBuilder,
			OfferExcludeConfigModel model, OfferExcludeConfigContext context) {
		return null;
	}

	

}
