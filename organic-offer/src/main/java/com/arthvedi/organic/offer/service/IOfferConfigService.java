package com.arthvedi.organic.offer.service;

import java.util.List;

import com.arthvedi.organic.offer.domain.OfferConfig;

/*
 *@Author varma
 */
public interface IOfferConfigService {

	OfferConfig create(OfferConfig offerConfig);

	void deleteOfferConfig(String offerConfigId);

	OfferConfig getOfferConfig(String offerConfigId);

	List<OfferConfig> getAll();

	OfferConfig updateOfferConfig(OfferConfig offerConfig);
}
