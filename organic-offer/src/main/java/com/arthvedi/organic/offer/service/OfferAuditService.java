package com.arthvedi.organic.offer.service;

import static com.arthvedi.organic.enums.Status.ACTIVE;

import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.offer.domain.OfferAudit;
import com.arthvedi.organic.offer.repository.OfferAuditRepository;

/*
 *@Author varma
 */
@Component
public class OfferAuditService implements IOfferAuditService {

	@Autowired
	private OfferAuditRepository offerAuditRepository;

	@Override
	public OfferAudit create(OfferAudit offerAudit) {
		offerAudit.setUserCreated("Dinakar");
		offerAudit.setCreatedDate(new LocalDateTime());
		offerAudit.setStatus(ACTIVE.name());
		return offerAuditRepository.save(offerAudit);
	}

	@Override
	public void deleteOfferAudit(String offerAuditId) {

	}

	@Override
	public OfferAudit getOfferAudit(String offerAuditId) {
		return offerAuditRepository.findOne(offerAuditId);
	}

	@Override
	public List<OfferAudit> getAll() {
		return null;
	}

	@Override
	public OfferAudit updateOfferAudit(OfferAudit offerAudit) {
		return offerAuditRepository.save(offerAudit);
	}

}
