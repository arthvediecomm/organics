package com.arthvedi.organic.offer.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.offer.domain.OfferAudit;
import com.arthvedi.organic.offer.model.OfferAuditModel;

@Component("offerAuditToOfferAuditModelConverter")
public class OfferAuditToOfferAuditModelConverter implements
		Converter<OfferAudit, OfferAuditModel> {
	@Autowired
	private ObjectFactory<OfferAuditModel> offerAuditModelFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public OfferAuditModel convert(final OfferAudit source) {
		OfferAuditModel offerAuditModel = offerAuditModelFactory.getObject();
		BeanUtils.copyProperties(source, offerAuditModel);

		return offerAuditModel;
	}

	@Autowired
	public void setOfferAuditModelFactory(
			final ObjectFactory<OfferAuditModel> offerAuditModelFactory) {
		this.offerAuditModelFactory = offerAuditModelFactory;
	}
}
