package com.arthvedi.organic.offer.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;

/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class OfferFulfillmentContext implements IBusinessDelegateContext {

	private String offerFulfillmentId;
	private String offerFulfillmentList;
	private String all;


	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

	public String getOfferFulfillmentId() {
		return offerFulfillmentId;
	}

	public void setOfferFulfillmentId(String offerFulfillmentId) {
		this.offerFulfillmentId = offerFulfillmentId;
	}

	public String getOfferFulfillmentList() {
		return offerFulfillmentList;
	}

	public void setOfferFulfillmentList(String offerFulfillmentList) {
		this.offerFulfillmentList = offerFulfillmentList;
	}

}
