/**
 *
 */
package com.arthvedi.organic.offer.representation.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.offer.model.OfferAuditModel;
import com.arthvedi.organic.offer.model.OfferConfigModel;
import com.arthvedi.organic.offer.model.OfferExcludeConfigModel;
import com.arthvedi.organic.offer.model.OfferFulfillmentModel;
import com.arthvedi.organic.offer.representation.siren.OfferFulfillmentRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("offerFulfillmentRepresentationToOfferFulfillmentModelConverter")
public class OfferFulfillmentRepresentationToOfferFulfillmentModelConverter
		extends
		PropertyCopyingConverter<OfferFulfillmentRepresentation, OfferFulfillmentModel> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public OfferFulfillmentModel convert(
			final OfferFulfillmentRepresentation source) {
		OfferFulfillmentModel target = super.convert(source);
		if (CollectionUtils.isNotEmpty(source.getOfferConfigRepresentation())) {
			List<OfferConfigModel> converted = (List<OfferConfigModel>) conversionService
					.convert(source.getOfferConfigRepresentation(),
							TypeDescriptor.forObject(source
									.getOfferConfigRepresentation()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									OfferConfigModel.class));
			target.getOfferConfigModel().addAll(converted);
		}
		if (CollectionUtils.isNotEmpty(source.getOfferAuditRepresentation())) {
			List<OfferAuditModel> converted = (List<OfferAuditModel>) conversionService
					.convert(source.getOfferAuditRepresentation(),
							TypeDescriptor.forObject(source
									.getOfferAuditRepresentation()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									OfferAuditModel.class));
			target.getOfferAuditModel().addAll(converted);
		}
		if (CollectionUtils.isNotEmpty(source
				.getOfferExcludeConfigRepresentation())) {
			List<OfferExcludeConfigModel> converted = (List<OfferExcludeConfigModel>) conversionService
					.convert(source.getOfferExcludeConfigRepresentation(),
							TypeDescriptor.forObject(source
									.getOfferExcludeConfigRepresentation()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									OfferExcludeConfigModel.class));
			target.getOfferExcludeConfigModel().addAll(converted);
		}
		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(final ObjectFactory<OfferFulfillmentModel> factory) {
		super.setFactory(factory);
	}

}
