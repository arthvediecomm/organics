package com.arthvedi.organic.offer.service;

import java.util.List;

import com.arthvedi.organic.offer.domain.OfferExcludeConfig;

/*
 *@Author varma
 */
public interface IOfferExcludeConfigService {

	OfferExcludeConfig create(OfferExcludeConfig offerExcludeConfig);

	void deleteOfferExcludeConfig(String offerExcludeConfigId);

	OfferExcludeConfig getOfferExcludeConfig(String offerExcludeConfigId);

	List<OfferExcludeConfig> getAll();

	OfferExcludeConfig updateOfferExcludeConfig(
			OfferExcludeConfig offerExcludeConfig);
}
