package com.arthvedi.organic.offer.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.offer.businessdelegate.context.OfferAuditContext;
import com.arthvedi.organic.offer.model.OfferAuditModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/offeraudit", produces = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class OfferAuditController {

	private IBusinessDelegate<OfferAuditModel, OfferAuditContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<OfferAuditContext> offerAuditContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create")
	public ResponseEntity<OfferAuditModel> createOfferAudit(
			@RequestBody OfferAuditModel offerAuditModel) {
		offerAuditModel = businessDelegate.create(offerAuditModel);
		return new ResponseEntity<OfferAuditModel>(offerAuditModel,
				HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	public ResponseEntity<OfferAuditModel> edit(
			@PathVariable(value = "id") final String offerAuditId,
			@RequestBody OfferAuditModel offerAuditModel) {
		offerAuditModel = businessDelegate.edit(keyBuilderFactory.getObject()
				.withId(offerAuditId), offerAuditModel);
		return new ResponseEntity<OfferAuditModel>(offerAuditModel,
				HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all")
	public ResponseEntity<IModelWrapper<Collection<OfferAuditModel>>> getAll() {
		OfferAuditContext offerAuditContext = offerAuditContextFactory
				.getObject();
		offerAuditContext.setAll("all");
		Collection<OfferAuditModel> offerAuditModels = businessDelegate
				.getCollection(offerAuditContext);
		IModelWrapper<Collection<OfferAuditModel>> models = new CollectionModelWrapper<OfferAuditModel>(
				OfferAuditModel.class, offerAuditModels);
		return new ResponseEntity<IModelWrapper<Collection<OfferAuditModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = { "/offerAuditlist",
			"/sellerapp/offerAuditlist" })
	public ResponseEntity<IModelWrapper<Collection<OfferAuditModel>>> getAllOfferAuditList() {
		OfferAuditContext offerAuditContext = offerAuditContextFactory
				.getObject();
		offerAuditContext.setOfferAuditList("offerAuditList");
		Collection<OfferAuditModel> offerAuditModels = businessDelegate
				.getCollection(offerAuditContext);
		IModelWrapper<Collection<OfferAuditModel>> models = new CollectionModelWrapper<OfferAuditModel>(
				OfferAuditModel.class, offerAuditModels);
		return new ResponseEntity<IModelWrapper<Collection<OfferAuditModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<OfferAuditModel> getOfferAudit(
			@PathVariable(value = "id") final String offerAuditId) {
		OfferAuditContext offerAuditContext = offerAuditContextFactory
				.getObject();

		OfferAuditModel model = businessDelegate.getByKey(keyBuilderFactory
				.getObject().withId(offerAuditId), offerAuditContext);
		return new ResponseEntity<OfferAuditModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "offerAuditBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<OfferAuditModel, OfferAuditContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setOfferAuditObjectFactory(
			final ObjectFactory<OfferAuditContext> offerAuditContextFactory) {
		this.offerAuditContextFactory = offerAuditContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(
			final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}
}
