package com.arthvedi.organic.offer.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.offer.businessdelegate.context.OfferConfigContext;
import com.arthvedi.organic.offer.domain.OfferConfig;
import com.arthvedi.organic.offer.model.OfferConfigModel;
import com.arthvedi.organic.offer.service.IOfferConfigService;

/*
 *@Author varma
 */

@Service
public class OfferConfigBusinessDelegate
		implements
		IBusinessDelegate<OfferConfigModel, OfferConfigContext, IKeyBuilder<String>, String> {

	@Autowired
	private IOfferConfigService offerConfigService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public OfferConfigModel create(OfferConfigModel model) {
		validateModel(model);

		OfferConfig offerConfig = offerConfigService
				.create((OfferConfig) conversionService.convert(model,
						forObject(model), valueOf(OfferConfig.class)));
		model = convertToOfferConfigModel(offerConfig);
		model.setStatusMessage("SUCCESS::: OfferConfig Created Successfully.");

		return null;
	}

	private void validateModel(OfferConfigModel model) {

	}

	private OfferConfigModel convertToOfferConfigModel(OfferConfig offerConfig) {
		return (OfferConfigModel) conversionService.convert(offerConfig,
				forObject(offerConfig), valueOf(OfferConfigModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			OfferConfigContext context) {

	}

	@Override
	public OfferConfigModel edit(IKeyBuilder<String> keyBuilder,
			OfferConfigModel model) {
		OfferConfig offerConfig = offerConfigService.getOfferConfig(keyBuilder
				.build().toString());

		offerConfig = offerConfigService.updateOfferConfig(offerConfig);

		return model;
	}

	@Override
	public OfferConfigModel getByKey(IKeyBuilder<String> keyBuilder,
			OfferConfigContext context) {
		OfferConfig offerConfig = offerConfigService.getOfferConfig(keyBuilder
				.build().toString());
		OfferConfigModel model = conversionService.convert(offerConfig,
				OfferConfigModel.class);
		return model;
	}

	@Override
	public Collection<OfferConfigModel> getCollection(OfferConfigContext context) {
		List<OfferConfig> offerConfig = new ArrayList<OfferConfig>();

		List<OfferConfigModel> offerConfigModels = (List<OfferConfigModel>) conversionService
				.convert(
						offerConfig,
						TypeDescriptor.forObject(offerConfig),
						TypeDescriptor.collection(List.class,
								TypeDescriptor.valueOf(OfferConfigModel.class)));
		return offerConfigModels;
	}

	@Override
	public OfferConfigModel edit(IKeyBuilder<String> keyBuilder,
			OfferConfigModel model, OfferConfigContext context) {
		return null;
	}

}
