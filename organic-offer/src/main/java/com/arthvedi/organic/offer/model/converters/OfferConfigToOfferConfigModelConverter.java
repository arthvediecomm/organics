package com.arthvedi.organic.offer.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.offer.domain.OfferConfig;
import com.arthvedi.organic.offer.model.OfferConfigModel;

@Component("offerConfigToOfferConfigModelConverter")
public class OfferConfigToOfferConfigModelConverter implements
		Converter<OfferConfig, OfferConfigModel> {
	@Autowired
	private ObjectFactory<OfferConfigModel> offerConfigModelFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public OfferConfigModel convert(final OfferConfig source) {
		OfferConfigModel offerConfigModel = offerConfigModelFactory.getObject();
		BeanUtils.copyProperties(source, offerConfigModel);

		return offerConfigModel;
	}

	@Autowired
	public void setOfferConfigModelFactory(
			final ObjectFactory<OfferConfigModel> offerConfigModelFactory) {
		this.offerConfigModelFactory = offerConfigModelFactory;
	}
}
