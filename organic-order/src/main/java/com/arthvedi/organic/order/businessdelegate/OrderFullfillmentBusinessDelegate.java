package com.arthvedi.organic.order.businessdelegate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.order.businessdelegate.context.OrderFullfillmentContext;
import com.arthvedi.organic.order.domain.OrderFullfillment;
import com.arthvedi.organic.order.model.OrderFullfillmentModel;
import com.arthvedi.organic.order.service.IOrderFullfillmentService;

/*
*@Author varma
*/

@Service
public class OrderFullfillmentBusinessDelegate
		implements IBusinessDelegate<OrderFullfillmentModel, OrderFullfillmentContext, IKeyBuilder<String>, String> {

	@Autowired
	private IOrderFullfillmentService orderFullfillmentService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public OrderFullfillmentModel create(OrderFullfillmentModel model) {

		
		return null;
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, OrderFullfillmentContext context) {

	}

	@Override
	public OrderFullfillmentModel edit(IKeyBuilder<String> keyBuilder, OrderFullfillmentModel model) {
		OrderFullfillment orderFullfillment = orderFullfillmentService.getOrderFullfillment(keyBuilder.build().toString());
		
		orderFullfillment = orderFullfillmentService.updateOrderFullfillment(orderFullfillment);
		
		return model;
	}

	@Override
	public OrderFullfillmentModel getByKey(IKeyBuilder<String> keyBuilder, OrderFullfillmentContext context) {
		OrderFullfillment orderFullfillment = orderFullfillmentService.getOrderFullfillment(keyBuilder.build().toString());
		OrderFullfillmentModel model = conversionService.convert(orderFullfillment, OrderFullfillmentModel.class);
		return model;
	}

	@Override
	public Collection<OrderFullfillmentModel> getCollection(OrderFullfillmentContext context) {
		List<OrderFullfillment> orderFullfillment = new ArrayList<OrderFullfillment>();
		if (context.getAll() != null) {
			orderFullfillment = orderFullfillmentService.getAll();
		}
		List<OrderFullfillmentModel> orderFullfillmentModels = (List<OrderFullfillmentModel>) conversionService.convert(
				orderFullfillment, TypeDescriptor.forObject(orderFullfillment),
				TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(OrderFullfillmentModel.class)));
		return orderFullfillmentModels;
	}

	@Override
	public OrderFullfillmentModel edit(IKeyBuilder<String> keyBuilder,
			OrderFullfillmentModel model, OrderFullfillmentContext context) {
		return null;
	}

}
