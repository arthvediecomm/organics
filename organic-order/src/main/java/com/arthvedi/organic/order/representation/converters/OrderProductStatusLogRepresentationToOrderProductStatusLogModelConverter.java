/**
 *
 */
package com.arthvedi.organic.order.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.order.model.OrderProductStatusLogModel;
import com.arthvedi.organic.order.representation.siren.OrderProductStatusLogRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("orderProductStatusLogRepresentationToOrderProductStatusLogModelConverter")
public class OrderProductStatusLogRepresentationToOrderProductStatusLogModelConverter extends PropertyCopyingConverter<OrderProductStatusLogRepresentation, OrderProductStatusLogModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public OrderProductStatusLogModel convert(final OrderProductStatusLogRepresentation source) {

        OrderProductStatusLogModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<OrderProductStatusLogModel> factory) {
        super.setFactory(factory);
    }

}
