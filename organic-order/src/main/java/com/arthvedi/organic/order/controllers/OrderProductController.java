package com.arthvedi.organic.order.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.order.businessdelegate.context.OrderProductContext;
import com.arthvedi.organic.order.model.OrderProductModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
* @author Administrator
*
*/

@RestController
@RequestMapping(value = "/orderProduct", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class OrderProductController {

	private IBusinessDelegate<OrderProductModel, OrderProductContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<OrderProductContext> orderProductContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create")

	public ResponseEntity<OrderProductModel> createOrderProduct(@RequestBody final OrderProductModel orderProductModel) {
		businessDelegate.create(orderProductModel);
		return new ResponseEntity<OrderProductModel>(orderProductModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)

	public ResponseEntity<OrderProductModel> edit(@PathVariable(value = "id") final String orderProductId,
			@RequestBody final OrderProductModel orderProductModel) {
		
		businessDelegate.edit(keyBuilderFactory.getObject().withId(orderProductId), orderProductModel);
		return new ResponseEntity<OrderProductModel>(orderProductModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all")
	
	public ResponseEntity<IModelWrapper> getAll() {
		OrderProductContext orderProductContext = orderProductContextFactory.getObject();
		orderProductContext.setAll("all");
		Collection<OrderProductModel> orderProductModels = businessDelegate.getCollection(orderProductContext);
		IModelWrapper<Collection<OrderProductModel>> models = new CollectionModelWrapper<OrderProductModel>(OrderProductModel.class,
				orderProductModels);
		return new ResponseEntity<IModelWrapper>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	
	public ResponseEntity<OrderProductModel> getOrderProduct(@PathVariable(value = "id") final String orderProductId) {
		OrderProductContext orderProductContext = orderProductContextFactory.getObject();

		OrderProductModel model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(orderProductId),
				orderProductContext);
		return new ResponseEntity<OrderProductModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "orderProductBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<OrderProductModel, OrderProductContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setOrderProductObjectFactory(final ObjectFactory<OrderProductContext> orderProductContextFactory) {
		this.orderProductContextFactory = orderProductContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
