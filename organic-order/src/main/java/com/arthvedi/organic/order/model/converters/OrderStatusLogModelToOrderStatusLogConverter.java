/**
 *
 */
package com.arthvedi.organic.order.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.OrderStatusLog;
import com.arthvedi.organic.order.model.OrderStatusLogModel;

/**
 * @author Jay
 *
 */
@Component("orderStatusLogModelToOrderStatusLogConverter")
public class OrderStatusLogModelToOrderStatusLogConverter implements Converter<OrderStatusLogModel, OrderStatusLog> {
    @Autowired
    private ObjectFactory<OrderStatusLog> orderStatusLogFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public OrderStatusLog convert(final OrderStatusLogModel source) {
        OrderStatusLog orderStatusLog = orderStatusLogFactory.getObject();
        BeanUtils.copyProperties(source, orderStatusLog);

        return orderStatusLog;
    }

    @Autowired
    public void setOrderStatusLogFactory(final ObjectFactory<OrderStatusLog> orderStatusLogFactory) {
        this.orderStatusLogFactory = orderStatusLogFactory;
    }

}
