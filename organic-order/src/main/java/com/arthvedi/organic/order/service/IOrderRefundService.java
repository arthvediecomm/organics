package com.arthvedi.organic.order.service;

import java.util.List;

import com.arthvedi.organic.order.domain.OrderRefund;
/*
*@Author varma
*/
public interface IOrderRefundService {
	
	OrderRefund create(OrderRefund orderRefund);

	void deleteOrderRefund(String orderRefundId);

	OrderRefund getOrderRefund(String orderRefundId);

	List<OrderRefund> getAll();

	OrderRefund updateOrderRefund(OrderRefund orderRefund);
}
