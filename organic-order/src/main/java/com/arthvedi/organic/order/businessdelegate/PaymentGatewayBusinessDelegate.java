package com.arthvedi.organic.order.businessdelegate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.order.businessdelegate.context.PaymentGatewayContext;
import com.arthvedi.organic.order.domain.PaymentGateway;
import com.arthvedi.organic.order.model.PaymentGatewayModel;
import com.arthvedi.organic.order.service.IPaymentGatewayService;

/*
*@Author varma
*/

@Service
public class PaymentGatewayBusinessDelegate
		implements IBusinessDelegate<PaymentGatewayModel, PaymentGatewayContext, IKeyBuilder<String>, String> {

	@Autowired
	private IPaymentGatewayService paymentGatewayService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public PaymentGatewayModel create(PaymentGatewayModel model) {

		
		return null;
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, PaymentGatewayContext context) {

	}

	@Override
	public PaymentGatewayModel edit(IKeyBuilder<String> keyBuilder, PaymentGatewayModel model) {
		PaymentGateway paymentGateway = paymentGatewayService.getPaymentGateway(keyBuilder.build().toString());
		
		paymentGateway = paymentGatewayService.updatePaymentGateway(paymentGateway);
		
		return model;
	}

	@Override
	public PaymentGatewayModel getByKey(IKeyBuilder<String> keyBuilder, PaymentGatewayContext context) {
		PaymentGateway paymentGateway = paymentGatewayService.getPaymentGateway(keyBuilder.build().toString());
		PaymentGatewayModel model = conversionService.convert(paymentGateway, PaymentGatewayModel.class);
		return model;
	}

	@Override
	public Collection<PaymentGatewayModel> getCollection(PaymentGatewayContext context) {
		List<PaymentGateway> paymentGateway = new ArrayList<PaymentGateway>();
		if (context.getAll() != null) {
			paymentGateway = paymentGatewayService.getAll();
		}
		List<PaymentGatewayModel> paymentGatewayModels = (List<PaymentGatewayModel>) conversionService.convert(
				paymentGateway, TypeDescriptor.forObject(paymentGateway),
				TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(PaymentGatewayModel.class)));
		return paymentGatewayModels;
	}

	@Override
	public PaymentGatewayModel edit(IKeyBuilder<String> keyBuilder,
			PaymentGatewayModel model, PaymentGatewayContext context) {
		return null;
	}

}
