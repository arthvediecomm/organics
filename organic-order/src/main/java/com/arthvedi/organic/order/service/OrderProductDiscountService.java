package com.arthvedi.organic.order.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.OrderProductDiscount;
import com.arthvedi.organic.order.repository.OrderProductDiscountRepository;
/*
*@Author varma
*/
@Component
public class OrderProductDiscountService implements IOrderProductDiscountService{

	@Autowired
	private OrderProductDiscountRepository orderProductDiscountRepository;
	@Override
	public OrderProductDiscount create(OrderProductDiscount orderProductDiscount) {
		return orderProductDiscountRepository.save(orderProductDiscount);
	}

	@Override
	public void deleteOrderProductDiscount(String orderProductDiscountId) {
		
	}

	@Override
	public OrderProductDiscount getOrderProductDiscount(String orderProductDiscountId) {
		 return orderProductDiscountRepository.findOne(orderProductDiscountId);
	}

	@Override
	public List<OrderProductDiscount> getAll() {
		return null;
	}

	@Override
	public OrderProductDiscount updateOrderProductDiscount(OrderProductDiscount orderProductDiscount) {
	return orderProductDiscountRepository.save(orderProductDiscount);
	}

}
