package com.arthvedi.organic.order.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.order.businessdelegate.context.OrderProductTaxesContext;
import com.arthvedi.organic.order.model.OrderProductTaxesModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
* @author Administrator
*
*/

@RestController
@RequestMapping(value = "/orderProductTaxes", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class OrderProductTaxesController {

	private IBusinessDelegate<OrderProductTaxesModel, OrderProductTaxesContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<OrderProductTaxesContext> orderProductTaxesContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create")

	public ResponseEntity<OrderProductTaxesModel> createOrderProductTaxes(@RequestBody final OrderProductTaxesModel orderProductTaxesModel) {
		businessDelegate.create(orderProductTaxesModel);
		return new ResponseEntity<OrderProductTaxesModel>(orderProductTaxesModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)

	public ResponseEntity<OrderProductTaxesModel> edit(@PathVariable(value = "id") final String orderProductTaxesId,
			@RequestBody final OrderProductTaxesModel orderProductTaxesModel) {
		
		businessDelegate.edit(keyBuilderFactory.getObject().withId(orderProductTaxesId), orderProductTaxesModel);
		return new ResponseEntity<OrderProductTaxesModel>(orderProductTaxesModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all")
	
	public ResponseEntity<IModelWrapper> getAll() {
		OrderProductTaxesContext orderProductTaxesContext = orderProductTaxesContextFactory.getObject();
		orderProductTaxesContext.setAll("all");
		Collection<OrderProductTaxesModel> orderProductTaxesModels = businessDelegate.getCollection(orderProductTaxesContext);
		IModelWrapper<Collection<OrderProductTaxesModel>> models = new CollectionModelWrapper<OrderProductTaxesModel>(OrderProductTaxesModel.class,
				orderProductTaxesModels);
		return new ResponseEntity<IModelWrapper>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")

	public ResponseEntity<OrderProductTaxesModel> getOrderProductTaxes(@PathVariable(value = "id") final String orderProductTaxesId) {
		OrderProductTaxesContext orderProductTaxesContext = orderProductTaxesContextFactory.getObject();

		OrderProductTaxesModel model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(orderProductTaxesId),
				orderProductTaxesContext);
		return new ResponseEntity<OrderProductTaxesModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "orderProductTaxesBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<OrderProductTaxesModel, OrderProductTaxesContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setOrderProductTaxesObjectFactory(final ObjectFactory<OrderProductTaxesContext> orderProductTaxesContextFactory) {
		this.orderProductTaxesContextFactory = orderProductTaxesContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
