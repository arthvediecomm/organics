package com.arthvedi.organic.order.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.repository.SellerBranchInvoiceRepository;
import com.arthvedi.organic.seller.domain.SellerBranchInvoice;
/*
*@Author varma
*/
@Component
public class SellerBranchInvoiceService implements ISellerBranchInvoiceService{

	@Autowired
	private SellerBranchInvoiceRepository sellerBranchInvoiceRepository;
	@Override
	public SellerBranchInvoice create(SellerBranchInvoice sellerBranchInvoice) {
		return sellerBranchInvoiceRepository.save(sellerBranchInvoice);
	}

	@Override
	public void deleteSellerBranchInvoice(String sellerBranchInvoiceId) {
		
	}

	@Override
	public SellerBranchInvoice getSellerBranchInvoice(String sellerBranchInvoiceId) {
		 return sellerBranchInvoiceRepository.findOne(sellerBranchInvoiceId);
	}

	@Override
	public List<SellerBranchInvoice> getAll() {
		return null;
	}

	@Override
	public SellerBranchInvoice updateSellerBranchInvoice(SellerBranchInvoice sellerBranchInvoice) {
	return sellerBranchInvoiceRepository.save(sellerBranchInvoice);
	}

}
