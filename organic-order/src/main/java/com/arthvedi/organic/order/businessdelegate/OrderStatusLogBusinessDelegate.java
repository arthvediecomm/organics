package com.arthvedi.organic.order.businessdelegate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.order.businessdelegate.context.OrderStatusLogContext;
import com.arthvedi.organic.order.domain.OrderStatusLog;
import com.arthvedi.organic.order.model.OrderStatusLogModel;
import com.arthvedi.organic.order.service.IOrderStatusLogService;

/*
*@Author varma
*/

@Service
public class OrderStatusLogBusinessDelegate
		implements IBusinessDelegate<OrderStatusLogModel, OrderStatusLogContext, IKeyBuilder<String>, String> {

	@Autowired
	private IOrderStatusLogService orderStatusLogService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public OrderStatusLogModel create(OrderStatusLogModel model) {

		
		return null;
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, OrderStatusLogContext context) {

	}

	@Override
	public OrderStatusLogModel edit(IKeyBuilder<String> keyBuilder, OrderStatusLogModel model) {
		OrderStatusLog orderStatusLog = orderStatusLogService.getOrderStatusLog(keyBuilder.build().toString());
		
		orderStatusLog = orderStatusLogService.updateOrderStatusLog(orderStatusLog);
		
		return model;
	}

	@Override
	public OrderStatusLogModel getByKey(IKeyBuilder<String> keyBuilder, OrderStatusLogContext context) {
		OrderStatusLog orderStatusLog = orderStatusLogService.getOrderStatusLog(keyBuilder.build().toString());
		OrderStatusLogModel model = conversionService.convert(orderStatusLog, OrderStatusLogModel.class);
		return model;
	}

	@Override
	public Collection<OrderStatusLogModel> getCollection(OrderStatusLogContext context) {
		List<OrderStatusLog> orderStatusLog = new ArrayList<OrderStatusLog>();
		if (context.getAll() != null) {
			orderStatusLog = orderStatusLogService.getAll();
		}
		List<OrderStatusLogModel> orderStatusLogModels = (List<OrderStatusLogModel>) conversionService.convert(
				orderStatusLog, TypeDescriptor.forObject(orderStatusLog),
				TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(OrderStatusLogModel.class)));
		return orderStatusLogModels;
	}

	@Override
	public OrderStatusLogModel edit(IKeyBuilder<String> keyBuilder,
			OrderStatusLogModel model, OrderStatusLogContext context) {
		return null;
	}

}
