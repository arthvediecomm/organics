package com.arthvedi.organic.order.service;

import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.businessdelegate.context.EcommerceOrderContext;
import com.arthvedi.organic.order.domain.EcommerceOrder;
import com.arthvedi.organic.order.repository.EcommerceOrderRepository;
/*
*@Author varma
*/
@Component
public class EcommerceOrderService implements IEcommerceOrderService{

	@Autowired
	private EcommerceOrderRepository ecommerceOrderRepository;
	@Override
	public EcommerceOrder create(EcommerceOrder order) {
		order.setCreatedDate(new LocalDateTime());
		order.setUserCreated("Dinakar");
		return ecommerceOrderRepository.save(order);
	}

	

	

	private Specification byStatusChangedDateRange(String statusChangedDate, String fromDate, String toDate) {
		// TODO Auto-generated method stub
		return null;
	}

	private Specification byStatus(String status) {
		// TODO Auto-generated method stub
		return null;
	}

	private Specification bySellerBranch(String sellerBranchId) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public void deleteEcommerceOrder(String ecommerceOrderId) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public EcommerceOrder getEcommerceOrder(String ecommerceOrderId) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public List<EcommerceOrder> getAll() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public EcommerceOrder updateEcommerceOrder(EcommerceOrder ecommerceOrder) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public List<EcommerceOrder> getEcommerceOrderBySellerBranchFilters(EcommerceOrderContext context) {
		String statusChangedDate = context.getDateChangedStatus()+"Date";
		String sortValue = context.getSort()+"Date";
		Sort sort = null;
		if(context.getSortType().equals("asc")){
			sort = new Sort(Sort.Direction.ASC,sortValue);
		}else{
			sort = new Sort(Sort.Direction.DESC,sortValue);
		}
		
		
		return ecommerceOrderRepository.findAll(
				Specifications.where(bySellerBranch(context.getSellerBranchId())).and(byStatus(context.getStatus()))
						.and(byStatusChangedDateRange(statusChangedDate, context.getFromDate(), context.getToDate())));
	}

}
