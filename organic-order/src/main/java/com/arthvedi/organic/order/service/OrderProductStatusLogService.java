package com.arthvedi.organic.order.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.OrderProductStatusLog;
import com.arthvedi.organic.order.repository.OrderProductStatusLogRepository;
/*
*@Author varma
*/
@Component
public class OrderProductStatusLogService implements IOrderProductStatusLogService{

	@Autowired
	private OrderProductStatusLogRepository orderProductStatusLogRepository;
	@Override
	public OrderProductStatusLog create(OrderProductStatusLog orderProductStatusLog) {
		return orderProductStatusLogRepository.save(orderProductStatusLog);
	}

	@Override
	public void deleteOrderProductStatusLog(String orderProductStatusLogId) {
		
	}

	@Override
	public OrderProductStatusLog getOrderProductStatusLog(String orderProductStatusLogId) {
		 return orderProductStatusLogRepository.findOne(orderProductStatusLogId);
	}

	@Override
	public List<OrderProductStatusLog> getAll() {
		return null;
	}

	@Override
	public OrderProductStatusLog updateOrderProductStatusLog(OrderProductStatusLog orderProductStatusLog) {
	return orderProductStatusLogRepository.save(orderProductStatusLog);
	}

}
