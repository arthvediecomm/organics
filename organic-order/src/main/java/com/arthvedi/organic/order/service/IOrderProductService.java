package com.arthvedi.organic.order.service;

import java.util.List;

import com.arthvedi.organic.order.domain.OrderProduct;
/*
*@Author varma
*/
public interface IOrderProductService {
	
	OrderProduct create(OrderProduct orderProduct);

	void deleteOrderProduct(String orderProductId);

	OrderProduct getOrderProduct(String orderProductId);

	List<OrderProduct> getAll();

	OrderProduct updateOrderProduct(OrderProduct orderProduct);
}
