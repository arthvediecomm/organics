package com.arthvedi.organic.order.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.OrderProductFeatures;
import com.arthvedi.organic.order.model.OrderProductFeaturesModel;

@Component("orderProductFeaturesToOrderProductFeaturesModelConverter")
public class OrderProductFeaturesToOrderProductFeaturesModelConverter
        implements Converter<OrderProductFeatures, OrderProductFeaturesModel> {
    @Autowired
    private ObjectFactory<OrderProductFeaturesModel> orderProductFeaturesModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public OrderProductFeaturesModel convert(final OrderProductFeatures source) {
        OrderProductFeaturesModel orderProductFeaturesModel = orderProductFeaturesModelFactory.getObject();
        BeanUtils.copyProperties(source, orderProductFeaturesModel);

        return orderProductFeaturesModel;
    }

    @Autowired
    public void setOrderProductFeaturesModelFactory(
            final ObjectFactory<OrderProductFeaturesModel> orderProductFeaturesModelFactory) {
        this.orderProductFeaturesModelFactory = orderProductFeaturesModelFactory;
    }
}
