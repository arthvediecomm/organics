/**
 *
 */
package com.arthvedi.organic.order.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.order.model.OrderDiscountModel;
import com.arthvedi.organic.order.representation.siren.OrderDiscountRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("orderDiscountRepresentationToOrderDiscountModelConverter")
public class OrderDiscountRepresentationToOrderDiscountModelConverter extends PropertyCopyingConverter<OrderDiscountRepresentation, OrderDiscountModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public OrderDiscountModel convert(final OrderDiscountRepresentation source) {

        OrderDiscountModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<OrderDiscountModel> factory) {
        super.setFactory(factory);
    }

}
