/**
 *
 */
package com.arthvedi.organic.order.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.order.model.OrderProductFeaturesModel;
import com.arthvedi.organic.order.representation.siren.OrderProductFeaturesRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("orderProductFeaturesRepresentationToOrderProductFeaturesModelConverter")
public class OrderProductFeaturesRepresentationToOrderProductFeaturesModelConverter extends PropertyCopyingConverter<OrderProductFeaturesRepresentation, OrderProductFeaturesModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public OrderProductFeaturesModel convert(final OrderProductFeaturesRepresentation source) {

        OrderProductFeaturesModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<OrderProductFeaturesModel> factory) {
        super.setFactory(factory);
    }

}
