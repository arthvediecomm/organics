package com.arthvedi.organic.order.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.model.SellerBranchInvoiceModel;
import com.arthvedi.organic.seller.domain.SellerBranchInvoice;

@Component("sellerBranchInvoiceToSellerBranchInvoiceModelConverter")
public class SellerBranchInvoiceToSellerBranchInvoiceModelConverter
        implements Converter<SellerBranchInvoice, SellerBranchInvoiceModel> {
    @Autowired
    private ObjectFactory<SellerBranchInvoiceModel> sellerBranchInvoiceModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public SellerBranchInvoiceModel convert(final SellerBranchInvoice source) {
        SellerBranchInvoiceModel sellerBranchInvoiceModel = sellerBranchInvoiceModelFactory.getObject();
        BeanUtils.copyProperties(source, sellerBranchInvoiceModel);

        return sellerBranchInvoiceModel;
    }

    @Autowired
    public void setSellerBranchInvoiceModelFactory(
            final ObjectFactory<SellerBranchInvoiceModel> sellerBranchInvoiceModelFactory) {
        this.sellerBranchInvoiceModelFactory = sellerBranchInvoiceModelFactory;
    }
}
