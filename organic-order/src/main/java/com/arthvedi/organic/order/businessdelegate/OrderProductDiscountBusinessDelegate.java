package com.arthvedi.organic.order.businessdelegate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.order.businessdelegate.context.OrderProductDiscountContext;
import com.arthvedi.organic.order.domain.OrderProductDiscount;
import com.arthvedi.organic.order.model.OrderProductDiscountModel;
import com.arthvedi.organic.order.service.IOrderProductDiscountService;

/*
*@Author varma
*/

@Service
public class OrderProductDiscountBusinessDelegate
		implements IBusinessDelegate<OrderProductDiscountModel, OrderProductDiscountContext, IKeyBuilder<String>, String> {

	@Autowired
	private IOrderProductDiscountService orderProductDiscountService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public OrderProductDiscountModel create(OrderProductDiscountModel model) {

		
		return null;
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, OrderProductDiscountContext context) {

	}

	@Override
	public OrderProductDiscountModel edit(IKeyBuilder<String> keyBuilder, OrderProductDiscountModel model) {
		OrderProductDiscount orderProductDiscount = orderProductDiscountService.getOrderProductDiscount(keyBuilder.build().toString());
		
		orderProductDiscount = orderProductDiscountService.updateOrderProductDiscount(orderProductDiscount);
		
		return model;
	}

	@Override
	public OrderProductDiscountModel getByKey(IKeyBuilder<String> keyBuilder, OrderProductDiscountContext context) {
		OrderProductDiscount orderProductDiscount = orderProductDiscountService.getOrderProductDiscount(keyBuilder.build().toString());
		OrderProductDiscountModel model = conversionService.convert(orderProductDiscount, OrderProductDiscountModel.class);
		return model;
	}

	@Override
	public Collection<OrderProductDiscountModel> getCollection(OrderProductDiscountContext context) {
		List<OrderProductDiscount> orderProductDiscount = new ArrayList<OrderProductDiscount>();
		if (context.getAll() != null) {
			orderProductDiscount = orderProductDiscountService.getAll();
		}
		List<OrderProductDiscountModel> orderProductDiscountModels = (List<OrderProductDiscountModel>) conversionService.convert(
				orderProductDiscount, TypeDescriptor.forObject(orderProductDiscount),
				TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(OrderProductDiscountModel.class)));
		return orderProductDiscountModels;
	}

	@Override
	public OrderProductDiscountModel edit(IKeyBuilder<String> keyBuilder,
			OrderProductDiscountModel model, OrderProductDiscountContext context) {
		return null;
	}

}
