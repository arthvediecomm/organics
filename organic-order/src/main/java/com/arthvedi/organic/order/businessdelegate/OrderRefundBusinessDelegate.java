package com.arthvedi.organic.order.businessdelegate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.order.businessdelegate.context.OrderRefundContext;
import com.arthvedi.organic.order.domain.OrderRefund;
import com.arthvedi.organic.order.model.OrderRefundModel;
import com.arthvedi.organic.order.service.IOrderRefundService;

/*
*@Author varma
*/

@Service
public class OrderRefundBusinessDelegate
		implements IBusinessDelegate<OrderRefundModel, OrderRefundContext, IKeyBuilder<String>, String> {

	@Autowired
	private IOrderRefundService orderRefundService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public OrderRefundModel create(OrderRefundModel model) {

		
		return null;
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, OrderRefundContext context) {

	}

	@Override
	public OrderRefundModel edit(IKeyBuilder<String> keyBuilder, OrderRefundModel model) {
		OrderRefund orderRefund = orderRefundService.getOrderRefund(keyBuilder.build().toString());
		
		orderRefund = orderRefundService.updateOrderRefund(orderRefund);
		
		return model;
	}

	@Override
	public OrderRefundModel getByKey(IKeyBuilder<String> keyBuilder, OrderRefundContext context) {
		OrderRefund orderRefund = orderRefundService.getOrderRefund(keyBuilder.build().toString());
		OrderRefundModel model = conversionService.convert(orderRefund, OrderRefundModel.class);
		return model;
	}

	@Override
	public Collection<OrderRefundModel> getCollection(OrderRefundContext context) {
		List<OrderRefund> orderRefund = new ArrayList<OrderRefund>();
		if (context.getAll() != null) {
			orderRefund = orderRefundService.getAll();
		}
		List<OrderRefundModel> orderRefundModels = (List<OrderRefundModel>) conversionService.convert(
				orderRefund, TypeDescriptor.forObject(orderRefund),
				TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(OrderRefundModel.class)));
		return orderRefundModels;
	}

	@Override
	public OrderRefundModel edit(IKeyBuilder<String> keyBuilder,
			OrderRefundModel model, OrderRefundContext context) {
		return null;
	}

}
