package com.arthvedi.organic.order.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.order.businessdelegate.context.OrderFullfillmentContext;
import com.arthvedi.organic.order.model.OrderFullfillmentModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
* @author Administrator
*
*/

@RestController
@RequestMapping(value = "/orderFullfillment", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class OrderFullfillmentController {

	private IBusinessDelegate<OrderFullfillmentModel, OrderFullfillmentContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<OrderFullfillmentContext> orderFullfillmentContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create")
	
	public ResponseEntity<OrderFullfillmentModel> createOrderFullfillment(@RequestBody final OrderFullfillmentModel orderFullfillmentModel) {
		businessDelegate.create(orderFullfillmentModel);
		return new ResponseEntity<OrderFullfillmentModel>(orderFullfillmentModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	
	public ResponseEntity<OrderFullfillmentModel> edit(@PathVariable(value = "id") final String orderFullfillmentId,
			@RequestBody final OrderFullfillmentModel orderFullfillmentModel) {
		
		businessDelegate.edit(keyBuilderFactory.getObject().withId(orderFullfillmentId), orderFullfillmentModel);
		return new ResponseEntity<OrderFullfillmentModel>(orderFullfillmentModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all")
	
	public ResponseEntity<IModelWrapper> getAll() {
		OrderFullfillmentContext orderFullfillmentContext = orderFullfillmentContextFactory.getObject();
		orderFullfillmentContext.setAll("all");
		Collection<OrderFullfillmentModel> orderFullfillmentModels = businessDelegate.getCollection(orderFullfillmentContext);
		IModelWrapper<Collection<OrderFullfillmentModel>> models = new CollectionModelWrapper<OrderFullfillmentModel>(OrderFullfillmentModel.class,
				orderFullfillmentModels);
		return new ResponseEntity<IModelWrapper>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	
	public ResponseEntity<OrderFullfillmentModel> getOrderFullfillment(@PathVariable(value = "id") final String orderFullfillmentId) {
		OrderFullfillmentContext orderFullfillmentContext = orderFullfillmentContextFactory.getObject();

		OrderFullfillmentModel model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(orderFullfillmentId),
				orderFullfillmentContext);
		return new ResponseEntity<OrderFullfillmentModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "orderFullfillmentBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<OrderFullfillmentModel, OrderFullfillmentContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setOrderFullfillmentObjectFactory(final ObjectFactory<OrderFullfillmentContext> orderFullfillmentContextFactory) {
		this.orderFullfillmentContextFactory = orderFullfillmentContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
