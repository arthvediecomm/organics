/**
 *
 */
package com.arthvedi.organic.order.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.OrderRefund;
import com.arthvedi.organic.order.model.OrderRefundModel;

/**
 * @author Jay
 *
 */
@Component("orderRefundModelToOrderRefundConverter")
public class OrderRefundModelToOrderRefundConverter implements Converter<OrderRefundModel, OrderRefund> {
    @Autowired
    private ObjectFactory<OrderRefund> orderRefundFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public OrderRefund convert(final OrderRefundModel source) {
        OrderRefund orderRefund = orderRefundFactory.getObject();
        BeanUtils.copyProperties(source, orderRefund);

        return orderRefund;
    }

    @Autowired
    public void setOrderRefundFactory(final ObjectFactory<OrderRefund> orderRefundFactory) {
        this.orderRefundFactory = orderRefundFactory;
    }

}
