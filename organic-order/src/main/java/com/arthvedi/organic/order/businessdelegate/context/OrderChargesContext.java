package com.arthvedi.organic.order.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;
/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class OrderChargesContext implements IBusinessDelegateContext {

	private String all;
	private String orderChargesId;

	
	
	public String getOrderChargesId() {
		return orderChargesId;
	}

	public void setOrderChargesId(String orderChargesId) {
		this.orderChargesId = orderChargesId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

}
