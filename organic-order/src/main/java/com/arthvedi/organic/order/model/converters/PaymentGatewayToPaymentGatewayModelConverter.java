package com.arthvedi.organic.order.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.PaymentGateway;
import com.arthvedi.organic.order.model.PaymentGatewayModel;

@Component("paymentGatewayToPaymentGatewayModelConverter")
public class PaymentGatewayToPaymentGatewayModelConverter
        implements Converter<PaymentGateway, PaymentGatewayModel> {
    @Autowired
    private ObjectFactory<PaymentGatewayModel> paymentGatewayModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public PaymentGatewayModel convert(final PaymentGateway source) {
        PaymentGatewayModel paymentGatewayModel = paymentGatewayModelFactory.getObject();
        BeanUtils.copyProperties(source, paymentGatewayModel);

        return paymentGatewayModel;
    }

    @Autowired
    public void setPaymentGatewayModelFactory(
            final ObjectFactory<PaymentGatewayModel> paymentGatewayModelFactory) {
        this.paymentGatewayModelFactory = paymentGatewayModelFactory;
    }
}
