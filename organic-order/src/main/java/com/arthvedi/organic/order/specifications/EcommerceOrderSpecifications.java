package com.arthvedi.organic.order.specifications;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.data.jpa.domain.Specification;

import com.arthvedi.organic.order.domain.EcommerceOrder;

public class EcommerceOrderSpecifications {

	public static Specification<EcommerceOrder> byStatus(String status) {
		return new Specification<EcommerceOrder>() {
			@Override
			public Predicate toPredicate(Root<EcommerceOrder> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.get("status"), status);
			}

		};
	}

	public static Specification<EcommerceOrder> byUser(String userId) {
		return new Specification<EcommerceOrder>() {
			@Override
			public Predicate toPredicate(Root<EcommerceOrder> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.get("orderFullfillment").get("bnvUser").get("id"), userId);
			}

		};
	}

	public static Specification<EcommerceOrder> bySellerBranch(String sellerBranchId) {
		return new Specification<EcommerceOrder>() {

			@Override
			public Predicate toPredicate(Root<EcommerceOrder> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				
				return cb.equal(root.get("sellerBranch").get("id"), sellerBranchId);
			}

		};

	}

	public static Specification<EcommerceOrder> byStatusChangedDateRange(String statusName, String fromDate, String toDate) {
		DateTimeFormatter format = DateTimeFormat.forPattern("dd/MM/yyyy");
		DateTime fromDateTime = format.parseDateTime(fromDate);
		DateTime toDateTime = format.parseDateTime(toDate);
		return new Specification<EcommerceOrder>() {
			@Override
			public Predicate toPredicate(Root<EcommerceOrder> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.between(root.get(statusName), fromDateTime, toDateTime);
			}

		};
	}
}
