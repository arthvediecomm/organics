package com.arthvedi.organic.order.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.order.businessdelegate.context.OrderProductFeaturesContext;
import com.arthvedi.organic.order.model.OrderProductFeaturesModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
* @author Administrator
*
*/

@RestController
@RequestMapping(value = "/orderProductFeatures", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class OrderProductFeaturesController {

	private IBusinessDelegate<OrderProductFeaturesModel, OrderProductFeaturesContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<OrderProductFeaturesContext> orderProductFeaturesContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create")
	
	public ResponseEntity<OrderProductFeaturesModel> createOrderProductFeatures(@RequestBody final OrderProductFeaturesModel orderProductFeaturesModel) {
		businessDelegate.create(orderProductFeaturesModel);
		return new ResponseEntity<OrderProductFeaturesModel>(orderProductFeaturesModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	
	public ResponseEntity<OrderProductFeaturesModel> edit(@PathVariable(value = "id") final String orderProductFeaturesId,
			@RequestBody final OrderProductFeaturesModel orderProductFeaturesModel) {
		
		businessDelegate.edit(keyBuilderFactory.getObject().withId(orderProductFeaturesId), orderProductFeaturesModel);
		return new ResponseEntity<OrderProductFeaturesModel>(orderProductFeaturesModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all")
	
	public ResponseEntity<IModelWrapper> getAll() {
		OrderProductFeaturesContext orderProductFeaturesContext = orderProductFeaturesContextFactory.getObject();
		orderProductFeaturesContext.setAll("all");
		Collection<OrderProductFeaturesModel> orderProductFeaturesModels = businessDelegate.getCollection(orderProductFeaturesContext);
		IModelWrapper<Collection<OrderProductFeaturesModel>> models = new CollectionModelWrapper<OrderProductFeaturesModel>(OrderProductFeaturesModel.class,
				orderProductFeaturesModels);
		return new ResponseEntity<IModelWrapper>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")

	public ResponseEntity<OrderProductFeaturesModel> getOrderProductFeatures(@PathVariable(value = "id") final String orderProductFeaturesId) {
		OrderProductFeaturesContext orderProductFeaturesContext = orderProductFeaturesContextFactory.getObject();

		OrderProductFeaturesModel model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(orderProductFeaturesId),
				orderProductFeaturesContext);
		return new ResponseEntity<OrderProductFeaturesModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "orderProductFeaturesBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<OrderProductFeaturesModel, OrderProductFeaturesContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setOrderProductFeaturesObjectFactory(final ObjectFactory<OrderProductFeaturesContext> orderProductFeaturesContextFactory) {
		this.orderProductFeaturesContextFactory = orderProductFeaturesContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
