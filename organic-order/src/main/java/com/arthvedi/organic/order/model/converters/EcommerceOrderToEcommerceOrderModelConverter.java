package com.arthvedi.organic.order.model.converters;


import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.EcommerceOrder;
import com.arthvedi.organic.order.model.EcommerceOrderModel;

@Component("ecommerceOrderToEcommerceOrderModelConverter")
public class EcommerceOrderToEcommerceOrderModelConverter
        implements Converter<EcommerceOrder, EcommerceOrderModel> {
    @Autowired
    private ObjectFactory<EcommerceOrderModel> orderModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public EcommerceOrderModel convert(final EcommerceOrder source) {
    	EcommerceOrderModel ecommerceOrderModel = orderModelFactory.getObject();
        BeanUtils.copyProperties(source, ecommerceOrderModel);

        
        if(source.getActualAmount()!=null){
        	ecommerceOrderModel.setActualAmount(source.getActualAmount().toString());
        }
        if(source.getPayableAmount()!=null){
        	ecommerceOrderModel.setPayableAmount(source.getPayableAmount().toString());
        }
        return ecommerceOrderModel;
    }

    @Autowired
    public void setOrderModelFactory(
            final ObjectFactory<EcommerceOrderModel> orderModelFactory) {
        this.orderModelFactory = orderModelFactory;
    }
}
