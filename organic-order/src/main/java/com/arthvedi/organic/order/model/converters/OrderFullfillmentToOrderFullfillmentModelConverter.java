package com.arthvedi.organic.order.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.OrderFullfillment;
import com.arthvedi.organic.order.model.OrderFullfillmentModel;

@Component("orderFullfillmentToOrderFullfillmentModelConverter")
public class OrderFullfillmentToOrderFullfillmentModelConverter
        implements Converter<OrderFullfillment, OrderFullfillmentModel> {
    @Autowired
    private ObjectFactory<OrderFullfillmentModel> orderFullfillmentModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public OrderFullfillmentModel convert(final OrderFullfillment source) {
        OrderFullfillmentModel orderFullfillmentModel = orderFullfillmentModelFactory.getObject();
        BeanUtils.copyProperties(source, orderFullfillmentModel);

        return orderFullfillmentModel;
    }

    @Autowired
    public void setOrderFullfillmentModelFactory(
            final ObjectFactory<OrderFullfillmentModel> orderFullfillmentModelFactory) {
        this.orderFullfillmentModelFactory = orderFullfillmentModelFactory;
    }
}
