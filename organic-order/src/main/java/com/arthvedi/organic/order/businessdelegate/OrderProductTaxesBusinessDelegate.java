package com.arthvedi.organic.order.businessdelegate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.order.businessdelegate.context.OrderProductTaxesContext;
import com.arthvedi.organic.order.domain.OrderProductTaxes;
import com.arthvedi.organic.order.model.OrderProductTaxesModel;
import com.arthvedi.organic.order.service.IOrderProductTaxesService;

/*
*@Author varma
*/

@Service
public class OrderProductTaxesBusinessDelegate
		implements IBusinessDelegate<OrderProductTaxesModel, OrderProductTaxesContext, IKeyBuilder<String>, String> {

	@Autowired
	private IOrderProductTaxesService orderProductTaxesService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public OrderProductTaxesModel create(OrderProductTaxesModel model) {

		
		return null;
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, OrderProductTaxesContext context) {

	}

	@Override
	public OrderProductTaxesModel edit(IKeyBuilder<String> keyBuilder, OrderProductTaxesModel model) {
		OrderProductTaxes orderProductTaxes = orderProductTaxesService.getOrderProductTaxes(keyBuilder.build().toString());
		
		orderProductTaxes = orderProductTaxesService.updateOrderProductTaxes(orderProductTaxes);
		
		return model;
	}

	@Override
	public OrderProductTaxesModel getByKey(IKeyBuilder<String> keyBuilder, OrderProductTaxesContext context) {
		OrderProductTaxes orderProductTaxes = orderProductTaxesService.getOrderProductTaxes(keyBuilder.build().toString());
		OrderProductTaxesModel model = conversionService.convert(orderProductTaxes, OrderProductTaxesModel.class);
		return model;
	}

	@Override
	public Collection<OrderProductTaxesModel> getCollection(OrderProductTaxesContext context) {
		List<OrderProductTaxes> orderProductTaxes = new ArrayList<OrderProductTaxes>();
		if (context.getAll() != null) {
			orderProductTaxes = orderProductTaxesService.getAll();
		}
		List<OrderProductTaxesModel> orderProductTaxesModels = (List<OrderProductTaxesModel>) conversionService.convert(
				orderProductTaxes, TypeDescriptor.forObject(orderProductTaxes),
				TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(OrderProductTaxesModel.class)));
		return orderProductTaxesModels;
	}

	@Override
	public OrderProductTaxesModel edit(IKeyBuilder<String> keyBuilder,
			OrderProductTaxesModel model, OrderProductTaxesContext context) {
		return null;
	}

}
