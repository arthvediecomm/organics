/**
 *
 */
package com.arthvedi.organic.order.model.converters;

import java.math.BigDecimal;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.EcommerceOrder;
import com.arthvedi.organic.order.domain.OrderFullfillment;
import com.arthvedi.organic.order.model.EcommerceOrderModel;
import com.arthvedi.organic.seller.domain.SellerBranch;
import com.arthvedi.organic.seller.domain.SellerBranchInvoice;
import com.arthvedi.organic.seller.domain.SellerBranchUser;

/**
 * @author Jay
 *
 */
@Component("ecommerceOrderModelToEcommerceOrderConverter")
public class EcommerceOrderModelToEcommerceOrderConverter implements Converter<EcommerceOrderModel, EcommerceOrder> {
    @Autowired
    private ObjectFactory<EcommerceOrder> orderFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public EcommerceOrder convert(final EcommerceOrderModel source) {
    	EcommerceOrder ecommerceOrder = orderFactory.getObject();
        BeanUtils.copyProperties(source, ecommerceOrder);
        if(source.getDeliveryUserId()!=null){
        	SellerBranchUser sellerBranchUser = new SellerBranchUser();
        	sellerBranchUser.setId(source.getDeliveryUserId());
        	ecommerceOrder.setDeliveryUser(sellerBranchUser);
        }
        if(source.getOrderFullfillmentId()!=null){
        	OrderFullfillment orderFulfillment = new OrderFullfillment();
        	orderFulfillment.setId(source.getOrderFullfillmentId());
        	ecommerceOrder.setOrderFullfillment(orderFulfillment);
        }
        if(source.getSellerBranchId()!=null){
        	SellerBranch sellerBranch = new SellerBranch();
        	sellerBranch.setId(source.getSellerBranchId());
        	ecommerceOrder.setSellerBranch(sellerBranch);
        }
        if(source.getSellerBranchInvoiceId()!=null){
        	SellerBranchInvoice sellerBranchInvoice = new SellerBranchInvoice();
        	sellerBranchInvoice.setId(source.getSellerBranchInvoiceId());
        	ecommerceOrder.setSellerBranchInvoice(sellerBranchInvoice);
        }
        
        if(source.getActualAmount()!=null){
        	ecommerceOrder.setActualAmount(new BigDecimal(source.getActualAmount()));
        }
        if(source.getPayableAmount()!=null){
        	ecommerceOrder.setPayableAmount(new BigDecimal(source.getPayableAmount()));
        }
        return ecommerceOrder;
    }

    @Autowired
    public void setOrderFactory(final ObjectFactory<EcommerceOrder> orderFactory) {
        this.orderFactory = orderFactory;
    }

}
