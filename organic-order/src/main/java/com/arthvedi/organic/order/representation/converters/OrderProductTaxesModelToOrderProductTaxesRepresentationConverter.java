/**
 *
 */
package com.arthvedi.organic.order.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.order.model.OrderProductTaxesModel;
import com.arthvedi.organic.order.representation.siren.OrderProductTaxesRepresentation;

/**
 * @author varma
 *
 */
@Component("orderProductTaxesModelToOrderProductTaxesRepresentationConverter")
public class OrderProductTaxesModelToOrderProductTaxesRepresentationConverter extends PropertyCopyingConverter<OrderProductTaxesModel, OrderProductTaxesRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public OrderProductTaxesRepresentation convert(final OrderProductTaxesModel source) {

        OrderProductTaxesRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<OrderProductTaxesRepresentation> factory) {
        super.setFactory(factory);
    }

}
