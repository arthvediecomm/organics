package com.arthvedi.organic.order.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.OrderStatusLog;
import com.arthvedi.organic.order.repository.OrderStatusLogRepository;
/*
*@Author varma
*/
@Component
public class OrderStatusLogService implements IOrderStatusLogService{

	@Autowired
	private OrderStatusLogRepository orderStatusLogRepository;
	@Override
	public OrderStatusLog create(OrderStatusLog orderStatusLog) {
		return orderStatusLogRepository.save(orderStatusLog);
	}

	@Override
	public void deleteOrderStatusLog(String orderStatusLogId) {
		
	}

	@Override
	public OrderStatusLog getOrderStatusLog(String orderStatusLogId) {
		 return orderStatusLogRepository.findOne(orderStatusLogId);
	}

	@Override
	public List<OrderStatusLog> getAll() {
		return null;
	}

	@Override
	public OrderStatusLog updateOrderStatusLog(OrderStatusLog orderStatusLog) {
return orderStatusLogRepository.save(orderStatusLog);
	}

}
