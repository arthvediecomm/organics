package com.arthvedi.organic.order.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.OrderProductFeatures;
import com.arthvedi.organic.order.repository.OrderProductFeaturesRepository;
/*
*@Author varma
*/
@Component
public class OrderProductFeaturesService implements IOrderProductFeaturesService{

	@Autowired
	private OrderProductFeaturesRepository orderProductFeaturesRepository;
	@Override
	public OrderProductFeatures create(OrderProductFeatures orderProductFeatures) {
		return orderProductFeaturesRepository.save(orderProductFeatures);
	}

	@Override
	public void deleteOrderProductFeatures(String orderProductFeaturesId) {
		
	}

	@Override
	public OrderProductFeatures getOrderProductFeatures(String orderProductFeaturesId) {
		 return orderProductFeaturesRepository.findOne(orderProductFeaturesId);
	}

	@Override
	public List<OrderProductFeatures> getAll() {
		return null;
	}

	@Override
	public OrderProductFeatures updateOrderProductFeatures(OrderProductFeatures orderProductFeatures) {
	return orderProductFeaturesRepository.save(orderProductFeatures);
	}

}
