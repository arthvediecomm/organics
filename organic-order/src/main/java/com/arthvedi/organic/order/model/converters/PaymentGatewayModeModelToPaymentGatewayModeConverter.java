/**
 *
 */
package com.arthvedi.organic.order.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.PaymentGatewayMode;
import com.arthvedi.organic.order.model.PaymentGatewayModeModel;

/**
 * @author Jay
 *
 */
@Component("paymentGatewayModeModelToPaymentGatewayModeConverter")
public class PaymentGatewayModeModelToPaymentGatewayModeConverter implements Converter<PaymentGatewayModeModel, PaymentGatewayMode> {
    @Autowired
    private ObjectFactory<PaymentGatewayMode> paymentGatewayModeFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public PaymentGatewayMode convert(final PaymentGatewayModeModel source) {
        PaymentGatewayMode paymentGatewayMode = paymentGatewayModeFactory.getObject();
        BeanUtils.copyProperties(source, paymentGatewayMode);

        return paymentGatewayMode;
    }

    @Autowired
    public void setPaymentGatewayModeFactory(final ObjectFactory<PaymentGatewayMode> paymentGatewayModeFactory) {
        this.paymentGatewayModeFactory = paymentGatewayModeFactory;
    }

}
