package com.arthvedi.organic.order.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.OrderProductTaxes;
import com.arthvedi.organic.order.repository.OrderProductTaxesRepository;
/*
*@Author varma
*/
@Component
public class OrderProductTaxesService implements IOrderProductTaxesService{

	@Autowired
	private OrderProductTaxesRepository orderProductTaxesRepository;
	@Override
	public OrderProductTaxes create(OrderProductTaxes orderProductTaxes) {
		return orderProductTaxesRepository.save(orderProductTaxes);
	}

	@Override
	public void deleteOrderProductTaxes(String orderProductTaxesId) {
	}

	@Override
	public OrderProductTaxes getOrderProductTaxes(String orderProductTaxesId) {
		 return orderProductTaxesRepository.findOne(orderProductTaxesId);
	}

	@Override
	public List<OrderProductTaxes> getAll() {
		return null;
	}

	@Override
	public OrderProductTaxes updateOrderProductTaxes(OrderProductTaxes orderProductTaxes) {
	return orderProductTaxesRepository.save(orderProductTaxes);
	}

}
