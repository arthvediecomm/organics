package com.arthvedi.organic.order.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.order.businessdelegate.context.PaymentGatewayContext;
import com.arthvedi.organic.order.model.PaymentGatewayModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
* @author Administrator
*
*/

@RestController
@RequestMapping(value = "/paymentGateway", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class PaymentGatewayController {

	private IBusinessDelegate<PaymentGatewayModel, PaymentGatewayContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<PaymentGatewayContext> paymentGatewayContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create")
	public ResponseEntity<PaymentGatewayModel> createPaymentGateway(@RequestBody final PaymentGatewayModel paymentGatewayModel) {
		businessDelegate.create(paymentGatewayModel);
		return new ResponseEntity<PaymentGatewayModel>(paymentGatewayModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	public ResponseEntity<PaymentGatewayModel> edit(@PathVariable(value = "id") final String paymentGatewayId,
			@RequestBody final PaymentGatewayModel paymentGatewayModel) {
		
		businessDelegate.edit(keyBuilderFactory.getObject().withId(paymentGatewayId), paymentGatewayModel);
		return new ResponseEntity<PaymentGatewayModel>(paymentGatewayModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all")
	public ResponseEntity<IModelWrapper> getAll() {
		PaymentGatewayContext paymentGatewayContext = paymentGatewayContextFactory.getObject();
		paymentGatewayContext.setAll("all");
		Collection<PaymentGatewayModel> paymentGatewayModels = businessDelegate.getCollection(paymentGatewayContext);
		IModelWrapper<Collection<PaymentGatewayModel>> models = new CollectionModelWrapper<PaymentGatewayModel>(PaymentGatewayModel.class,
				paymentGatewayModels);
		return new ResponseEntity<IModelWrapper>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<PaymentGatewayModel> getPaymentGateway(@PathVariable(value = "id") final String paymentGatewayId) {
		PaymentGatewayContext paymentGatewayContext = paymentGatewayContextFactory.getObject();

		PaymentGatewayModel model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(paymentGatewayId),
				paymentGatewayContext);
		return new ResponseEntity<PaymentGatewayModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "paymentGatewayBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<PaymentGatewayModel, PaymentGatewayContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setPaymentGatewayObjectFactory(final ObjectFactory<PaymentGatewayContext> paymentGatewayContextFactory) {
		this.paymentGatewayContextFactory = paymentGatewayContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
