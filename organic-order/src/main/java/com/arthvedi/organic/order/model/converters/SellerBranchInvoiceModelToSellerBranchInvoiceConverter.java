/**
 *
 */
package com.arthvedi.organic.order.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.model.SellerBranchInvoiceModel;
import com.arthvedi.organic.seller.domain.SellerBranchInvoice;

/**
 * @author Jay
 *
 */
@Component("sellerBranchInvoiceModelToSellerBranchInvoiceConverter")
public class SellerBranchInvoiceModelToSellerBranchInvoiceConverter implements Converter<SellerBranchInvoiceModel, SellerBranchInvoice> {
    @Autowired
    private ObjectFactory<SellerBranchInvoice> sellerBranchInvoiceFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public SellerBranchInvoice convert(final SellerBranchInvoiceModel source) {
        SellerBranchInvoice sellerBranchInvoice = sellerBranchInvoiceFactory.getObject();
        BeanUtils.copyProperties(source, sellerBranchInvoice);

        return sellerBranchInvoice;
    }

    @Autowired
    public void setSellerBranchInvoiceFactory(final ObjectFactory<SellerBranchInvoice> sellerBranchInvoiceFactory) {
        this.sellerBranchInvoiceFactory = sellerBranchInvoiceFactory;
    }

}
