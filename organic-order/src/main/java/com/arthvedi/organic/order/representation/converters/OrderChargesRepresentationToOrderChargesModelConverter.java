/**
 *
 */
package com.arthvedi.organic.order.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.order.model.OrderChargesModel;
import com.arthvedi.organic.order.representation.siren.OrderChargesRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("orderChargesRepresentationToOrderChargesModelConverter")
public class OrderChargesRepresentationToOrderChargesModelConverter extends PropertyCopyingConverter<OrderChargesRepresentation, OrderChargesModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public OrderChargesModel convert(final OrderChargesRepresentation source) {

        OrderChargesModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<OrderChargesModel> factory) {
        super.setFactory(factory);
    }

}
