package com.arthvedi.organic.order.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.OrderFullfillmentDiscount;
import com.arthvedi.organic.order.model.OrderFullfillmentDiscountModel;

@Component("orderFullfillmentDiscountToOrderFullfillmentDiscountModelConverter")
public class OrderFullfillmentDiscountToOrderFullfillmentDiscountModelConverter
        implements Converter<OrderFullfillmentDiscount, OrderFullfillmentDiscountModel> {
    @Autowired
    private ObjectFactory<OrderFullfillmentDiscountModel> orderFullfillmentDiscountModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public OrderFullfillmentDiscountModel convert(final OrderFullfillmentDiscount source) {
        OrderFullfillmentDiscountModel orderFullfillmentDiscountModel = orderFullfillmentDiscountModelFactory.getObject();
        BeanUtils.copyProperties(source, orderFullfillmentDiscountModel);

        return orderFullfillmentDiscountModel;
    }

    @Autowired
    public void setOrderFullfillmentDiscountModelFactory(
            final ObjectFactory<OrderFullfillmentDiscountModel> orderFullfillmentDiscountModelFactory) {
        this.orderFullfillmentDiscountModelFactory = orderFullfillmentDiscountModelFactory;
    }
}
