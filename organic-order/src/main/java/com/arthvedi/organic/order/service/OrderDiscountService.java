package com.arthvedi.organic.order.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.OrderDiscount;
import com.arthvedi.organic.order.repository.OrderDiscountRepository;
/*
*@Author varma
*/
@Component
public class OrderDiscountService implements IOrderDiscountService{

	@Autowired
	private OrderDiscountRepository orderDiscountRepository;
	@Override
	public OrderDiscount create(OrderDiscount orderDiscount) {
		return orderDiscountRepository.save(orderDiscount);
	}

	@Override
	public void deleteOrderDiscount(String orderDiscountId) {
		
	}

	@Override
	public OrderDiscount getOrderDiscount(String orderDiscountId) {
		 return orderDiscountRepository.findOne(orderDiscountId);
	}

	@Override
	public List<OrderDiscount> getAll() {
		return null;
	}

	@Override
	public OrderDiscount updateOrderDiscount(OrderDiscount orderDiscount) {
	return orderDiscountRepository.save(orderDiscount);
	}

}
