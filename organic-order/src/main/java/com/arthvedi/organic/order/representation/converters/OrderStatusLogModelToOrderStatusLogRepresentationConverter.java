/**
 *
 */
package com.arthvedi.organic.order.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.order.model.OrderStatusLogModel;
import com.arthvedi.organic.order.representation.siren.OrderStatusLogRepresentation;

/**
 * @author varma
 *
 */
@Component("orderStatusLogModelToOrderStatusLogRepresentationConverter")
public class OrderStatusLogModelToOrderStatusLogRepresentationConverter extends PropertyCopyingConverter<OrderStatusLogModel, OrderStatusLogRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public OrderStatusLogRepresentation convert(final OrderStatusLogModel source) {

        OrderStatusLogRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<OrderStatusLogRepresentation> factory) {
        super.setFactory(factory);
    }

}
