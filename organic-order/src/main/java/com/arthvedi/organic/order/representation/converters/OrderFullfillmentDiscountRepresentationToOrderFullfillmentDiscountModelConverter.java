/**
 *
 */
package com.arthvedi.organic.order.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.order.model.OrderFullfillmentDiscountModel;
import com.arthvedi.organic.order.representation.siren.OrderFullfillmentDiscountRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("orderFullfillmentDiscountRepresentationToOrderFullfillmentDiscountModelConverter")
public class OrderFullfillmentDiscountRepresentationToOrderFullfillmentDiscountModelConverter extends PropertyCopyingConverter<OrderFullfillmentDiscountRepresentation, OrderFullfillmentDiscountModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public OrderFullfillmentDiscountModel convert(final OrderFullfillmentDiscountRepresentation source) {

        OrderFullfillmentDiscountModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<OrderFullfillmentDiscountModel> factory) {
        super.setFactory(factory);
    }

}
