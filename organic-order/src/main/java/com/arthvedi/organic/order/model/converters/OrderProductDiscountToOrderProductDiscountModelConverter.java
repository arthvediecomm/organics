package com.arthvedi.organic.order.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.OrderProductDiscount;
import com.arthvedi.organic.order.model.OrderProductDiscountModel;

@Component("orderProductDiscountToOrderProductDiscountModelConverter")
public class OrderProductDiscountToOrderProductDiscountModelConverter
        implements Converter<OrderProductDiscount, OrderProductDiscountModel> {
    @Autowired
    private ObjectFactory<OrderProductDiscountModel> orderProductDiscountModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public OrderProductDiscountModel convert(final OrderProductDiscount source) {
        OrderProductDiscountModel orderProductDiscountModel = orderProductDiscountModelFactory.getObject();
        BeanUtils.copyProperties(source, orderProductDiscountModel);

        return orderProductDiscountModel;
    }

    @Autowired
    public void setOrderProductDiscountModelFactory(
            final ObjectFactory<OrderProductDiscountModel> orderProductDiscountModelFactory) {
        this.orderProductDiscountModelFactory = orderProductDiscountModelFactory;
    }
}
