package com.arthvedi.organic.order.service;

import java.util.List;

import com.arthvedi.organic.seller.domain.SellerBranchInvoice;
/*
*@Author varma
*/
public interface ISellerBranchInvoiceService {
	
	SellerBranchInvoice create(SellerBranchInvoice sellerBranchInvoice);

	void deleteSellerBranchInvoice(String sellerBranchInvoiceId);

	SellerBranchInvoice getSellerBranchInvoice(String sellerBranchInvoiceId);

	List<SellerBranchInvoice> getAll();

	SellerBranchInvoice updateSellerBranchInvoice(SellerBranchInvoice sellerBranchInvoice);
}
