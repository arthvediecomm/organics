package com.arthvedi.organic.order.service;

import java.util.List;

import com.arthvedi.organic.order.businessdelegate.context.EcommerceOrderContext;
import com.arthvedi.organic.order.domain.EcommerceOrder;

/*
*@Author varma
*/
public interface IEcommerceOrderService {

	EcommerceOrder create(EcommerceOrder ecommerceOrder);

	void deleteEcommerceOrder(String ecommerceOrderId);

	EcommerceOrder getEcommerceOrder(String ecommerceOrderId);

	List<EcommerceOrder> getAll();

	EcommerceOrder updateEcommerceOrder(EcommerceOrder ecommerceOrder);

	
	List<EcommerceOrder> getEcommerceOrderBySellerBranchFilters(EcommerceOrderContext context);
}
