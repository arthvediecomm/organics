package com.arthvedi.organic.order.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.OrderRefund;
import com.arthvedi.organic.order.model.OrderRefundModel;

@Component("orderRefundToOrderRefundModelConverter")
public class OrderRefundToOrderRefundModelConverter
        implements Converter<OrderRefund, OrderRefundModel> {
    @Autowired
    private ObjectFactory<OrderRefundModel> orderRefundModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public OrderRefundModel convert(final OrderRefund source) {
        OrderRefundModel orderRefundModel = orderRefundModelFactory.getObject();
        BeanUtils.copyProperties(source, orderRefundModel);

        return orderRefundModel;
    }

    @Autowired
    public void setOrderRefundModelFactory(
            final ObjectFactory<OrderRefundModel> orderRefundModelFactory) {
        this.orderRefundModelFactory = orderRefundModelFactory;
    }
}
