package com.arthvedi.organic.order.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.OrderStatusLog;
import com.arthvedi.organic.order.model.OrderStatusLogModel;

@Component("orderStatusLogToOrderStatusLogModelConverter")
public class OrderStatusLogToOrderStatusLogModelConverter
        implements Converter<OrderStatusLog, OrderStatusLogModel> {
    @Autowired
    private ObjectFactory<OrderStatusLogModel> orderStatusLogModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public OrderStatusLogModel convert(final OrderStatusLog source) {
        OrderStatusLogModel orderStatusLogModel = orderStatusLogModelFactory.getObject();
        BeanUtils.copyProperties(source, orderStatusLogModel);

        return orderStatusLogModel;
    }

    @Autowired
    public void setOrderStatusLogModelFactory(
            final ObjectFactory<OrderStatusLogModel> orderStatusLogModelFactory) {
        this.orderStatusLogModelFactory = orderStatusLogModelFactory;
    }
}
