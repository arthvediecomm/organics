package com.arthvedi.organic.order.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.order.businessdelegate.context.OrderDiscountContext;
import com.arthvedi.organic.order.model.OrderDiscountModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
* @author Administrator
*
*/

@RestController
@RequestMapping(value = "/orderDiscount", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class OrderDiscountController {

	private IBusinessDelegate<OrderDiscountModel, OrderDiscountContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<OrderDiscountContext> orderDiscountContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create")
	
	public ResponseEntity<OrderDiscountModel> createOrderDiscount(@RequestBody final OrderDiscountModel orderDiscountModel) {
		businessDelegate.create(orderDiscountModel);
		return new ResponseEntity<OrderDiscountModel>(orderDiscountModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)

	public ResponseEntity<OrderDiscountModel> edit(@PathVariable(value = "id") final String orderDiscountId,
			@RequestBody final OrderDiscountModel orderDiscountModel) {
		
		businessDelegate.edit(keyBuilderFactory.getObject().withId(orderDiscountId), orderDiscountModel);
		return new ResponseEntity<OrderDiscountModel>(orderDiscountModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all")
	
	public ResponseEntity<IModelWrapper> getAll() {
		OrderDiscountContext orderDiscountContext = orderDiscountContextFactory.getObject();
		orderDiscountContext.setAll("all");
		Collection<OrderDiscountModel> orderDiscountModels = businessDelegate.getCollection(orderDiscountContext);
		IModelWrapper<Collection<OrderDiscountModel>> models = new CollectionModelWrapper<OrderDiscountModel>(OrderDiscountModel.class,
				orderDiscountModels);
		return new ResponseEntity<IModelWrapper>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	
	public ResponseEntity<OrderDiscountModel> getOrderDiscount(@PathVariable(value = "id") final String orderDiscountId) {
		OrderDiscountContext orderDiscountContext = orderDiscountContextFactory.getObject();

		OrderDiscountModel model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(orderDiscountId),
				orderDiscountContext);
		return new ResponseEntity<OrderDiscountModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "orderDiscountBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<OrderDiscountModel, OrderDiscountContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setOrderDiscountObjectFactory(final ObjectFactory<OrderDiscountContext> orderDiscountContextFactory) {
		this.orderDiscountContextFactory = orderDiscountContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
