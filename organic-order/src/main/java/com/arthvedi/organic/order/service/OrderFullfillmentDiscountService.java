package com.arthvedi.organic.order.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.OrderFullfillmentDiscount;
import com.arthvedi.organic.order.repository.OrderFullfillmentDiscountRepository;
/*
*@Author varma
*/
@Component
public class OrderFullfillmentDiscountService implements IOrderFullfillmentDiscountService{

	@Autowired
	private OrderFullfillmentDiscountRepository orderFullfillmentDiscountRepository;
	@Override
	public OrderFullfillmentDiscount create(OrderFullfillmentDiscount orderFullfillmentDiscount) {
		return orderFullfillmentDiscountRepository.save(orderFullfillmentDiscount);
	}

	@Override
	public void deleteOrderFullfillmentDiscount(String orderFullfillmentDiscountId) {
		
	}

	@Override
	public OrderFullfillmentDiscount getOrderFullfillmentDiscount(String orderFullfillmentDiscountId) {
		 return orderFullfillmentDiscountRepository.findOne(orderFullfillmentDiscountId);
	}

	@Override
	public List<OrderFullfillmentDiscount> getAll() {
		return null;
	}

	@Override
	public OrderFullfillmentDiscount updateOrderFullfillmentDiscount(OrderFullfillmentDiscount orderFullfillmentDiscount) {
	return orderFullfillmentDiscountRepository.save(orderFullfillmentDiscount);
	}

}
