/**
 *
 */
package com.arthvedi.organic.order.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.order.model.EcommerceOrderModel;
import com.arthvedi.organic.order.representation.siren.EcommerceOrderRepresentation;

/**
 * @author varma
 *
 */
@Component("ecommerceOrderModelToEcommerceOrderRepresentationConverter")
public class EcommerceOrderModelToEcommerceOrderRepresentationConverter extends PropertyCopyingConverter<EcommerceOrderModel, EcommerceOrderRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public EcommerceOrderRepresentation convert(final EcommerceOrderModel source) {

	   EcommerceOrderRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<EcommerceOrderRepresentation> factory) {
        super.setFactory(factory);
    }

}
