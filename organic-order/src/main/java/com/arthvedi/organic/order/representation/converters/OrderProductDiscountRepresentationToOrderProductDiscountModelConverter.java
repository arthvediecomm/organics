/**
 *
 */
package com.arthvedi.organic.order.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.order.model.OrderProductDiscountModel;
import com.arthvedi.organic.order.representation.siren.OrderProductDiscountRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("orderProductDiscountRepresentationToOrderProductDiscountModelConverter")
public class OrderProductDiscountRepresentationToOrderProductDiscountModelConverter extends PropertyCopyingConverter<OrderProductDiscountRepresentation, OrderProductDiscountModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public OrderProductDiscountModel convert(final OrderProductDiscountRepresentation source) {

        OrderProductDiscountModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<OrderProductDiscountModel> factory) {
        super.setFactory(factory);
    }

}
