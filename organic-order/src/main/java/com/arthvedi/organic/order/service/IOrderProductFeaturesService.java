package com.arthvedi.organic.order.service;

import java.util.List;

import com.arthvedi.organic.order.domain.OrderProductFeatures;

/*
*@Author varma
*/
public interface IOrderProductFeaturesService {

	OrderProductFeatures create(OrderProductFeatures orderProductFeatures);

	void deleteOrderProductFeatures(String orderProductFeaturesId);

	OrderProductFeatures getOrderProductFeatures(String orderProductFeaturesId);

	List<OrderProductFeatures> getAll();

	OrderProductFeatures updateOrderProductFeatures(OrderProductFeatures orderProductFeatures);
}
