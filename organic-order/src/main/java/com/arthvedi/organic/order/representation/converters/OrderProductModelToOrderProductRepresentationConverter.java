/**
 *
 */
package com.arthvedi.organic.order.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.order.model.OrderProductModel;
import com.arthvedi.organic.order.representation.siren.OrderProductRepresentation;

/**
 * @author varma
 *
 */
@Component("orderProductModelToOrderProductRepresentationConverter")
public class OrderProductModelToOrderProductRepresentationConverter extends PropertyCopyingConverter<OrderProductModel, OrderProductRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public OrderProductRepresentation convert(final OrderProductModel source) {

        OrderProductRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<OrderProductRepresentation> factory) {
        super.setFactory(factory);
    }

}
