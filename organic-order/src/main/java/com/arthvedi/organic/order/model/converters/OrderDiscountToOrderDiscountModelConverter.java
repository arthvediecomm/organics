package com.arthvedi.organic.order.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.OrderDiscount;
import com.arthvedi.organic.order.model.OrderDiscountModel;

@Component("orderDiscountToOrderDiscountModelConverter")
public class OrderDiscountToOrderDiscountModelConverter
        implements Converter<OrderDiscount, OrderDiscountModel> {
    @Autowired
    private ObjectFactory<OrderDiscountModel> orderDiscountModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public OrderDiscountModel convert(final OrderDiscount source) {
        OrderDiscountModel orderDiscountModel = orderDiscountModelFactory.getObject();
        BeanUtils.copyProperties(source, orderDiscountModel);

        return orderDiscountModel;
    }

    @Autowired
    public void setOrderDiscountModelFactory(
            final ObjectFactory<OrderDiscountModel> orderDiscountModelFactory) {
        this.orderDiscountModelFactory = orderDiscountModelFactory;
    }
}
