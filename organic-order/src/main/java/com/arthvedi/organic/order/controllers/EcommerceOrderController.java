package com.arthvedi.organic.order.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.order.businessdelegate.context.EcommerceOrderContext;
import com.arthvedi.organic.order.model.EcommerceOrderModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/ecommerceorder", produces = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class EcommerceOrderController {

	private IBusinessDelegate<EcommerceOrderModel, EcommerceOrderContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<EcommerceOrderContext> orderContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create")
	public ResponseEntity<EcommerceOrderModel> createOrder(
			@RequestBody final EcommerceOrderModel ecommerceOrderModel) {

		businessDelegate.create(ecommerceOrderModel);
		return new ResponseEntity<EcommerceOrderModel>(ecommerceOrderModel,
				HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	public ResponseEntity<EcommerceOrderModel> edit(
			@PathVariable(value = "id") final String ecommerceOrderId,
			@RequestBody final EcommerceOrderModel ecommerceOrderModel) {

		businessDelegate.edit(
				keyBuilderFactory.getObject().withId(ecommerceOrderId),
				ecommerceOrderModel);
		return new ResponseEntity<EcommerceOrderModel>(ecommerceOrderModel,
				HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all")
	public ResponseEntity<IModelWrapper> getAll() {
		EcommerceOrderContext ecommerceOrderContext = orderContextFactory
				.getObject();
		ecommerceOrderContext.setAll("all");
		Collection<EcommerceOrderModel> ecommerceOrderModels = businessDelegate
				.getCollection(ecommerceOrderContext);
		IModelWrapper<Collection<EcommerceOrderModel>> models = new CollectionModelWrapper<EcommerceOrderModel>(
				EcommerceOrderModel.class, ecommerceOrderModels);
		return new ResponseEntity<IModelWrapper>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/sellerapp/sellerbranch/{sellerBranchId}")
	public ResponseEntity<IModelWrapper> getAll(
			@PathVariable(value = "sellerBranchId") final String sellerBranchId,
			@RequestParam(value = "datestatus", defaultValue = "N-A") final String dateChangedStatus,
			@RequestParam(value = "status", defaultValue = "N-A") final String status,
			@RequestParam(value = "fromdate", defaultValue = "N-A") final String fromDate,
			@RequestParam(value = "todate", defaultValue = "N-A") final String toDate,
			@RequestParam(value = "sorttype", defaultValue = "N-A") final String sortType,
			@RequestParam(value = "sort", defaultValue = "N-A") final String sort,
			@RequestParam(value = "searchkey", defaultValue = "N-A") final String searchKey,
			@RequestParam(value = "searchvalue", defaultValue = "N-A") final String searchValue) {
		EcommerceOrderContext ecommerceOrderContext = orderContextFactory
				.getObject();
		ecommerceOrderContext.setSellerBranchId(sellerBranchId);
		ecommerceOrderContext.setDateChangedStatus(dateChangedStatus);
		ecommerceOrderContext.setStatus(status.toUpperCase());
		ecommerceOrderContext.setFromDate(fromDate);
		ecommerceOrderContext.setToDate(toDate);
		ecommerceOrderContext.setSort(sort);
		ecommerceOrderContext.setSortType(sortType);
		ecommerceOrderContext.setSearchKey(searchKey);
		ecommerceOrderContext.setSearchValue(searchValue);
		Collection<EcommerceOrderModel> ecommerceOrderModels = businessDelegate
				.getCollection(ecommerceOrderContext);
		IModelWrapper<Collection<EcommerceOrderModel>> models = new CollectionModelWrapper<EcommerceOrderModel>(
				EcommerceOrderModel.class, ecommerceOrderModels);
		return new ResponseEntity<IModelWrapper>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<EcommerceOrderModel> getOrder(
			@PathVariable(value = "id") final String ecommerceOrderId) {
		EcommerceOrderContext ecommerceOrderContext = orderContextFactory
				.getObject();

		EcommerceOrderModel model = businessDelegate.getByKey(keyBuilderFactory
				.getObject().withId(ecommerceOrderId), ecommerceOrderContext);
		return new ResponseEntity<EcommerceOrderModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "ecommerceOrderBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<EcommerceOrderModel, EcommerceOrderContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setOrderObjectFactory(
			final ObjectFactory<EcommerceOrderContext> orderContextFactory) {
		this.orderContextFactory = orderContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(
			final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
