package com.arthvedi.organic.order.service;

import java.util.List;

import com.arthvedi.organic.order.domain.OrderProductDiscount;

/*
*@Author varma
*/
public interface IOrderProductDiscountService {

	OrderProductDiscount create(OrderProductDiscount orderProductDiscount);

	void deleteOrderProductDiscount(String orderProductDiscountId);

	OrderProductDiscount getOrderProductDiscount(String orderProductDiscountId);

	List<OrderProductDiscount> getAll();

	OrderProductDiscount updateOrderProductDiscount(OrderProductDiscount orderProductDiscount);
}
