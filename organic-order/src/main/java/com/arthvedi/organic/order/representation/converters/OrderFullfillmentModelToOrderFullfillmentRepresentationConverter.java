/**
 *
 */
package com.arthvedi.organic.order.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.order.model.OrderFullfillmentModel;
import com.arthvedi.organic.order.representation.siren.OrderFullfillmentRepresentation;

/**
 * @author varma
 *
 */
@Component("orderFullfillmentModelToOrderFullfillmentRepresentationConverter")
public class OrderFullfillmentModelToOrderFullfillmentRepresentationConverter extends PropertyCopyingConverter<OrderFullfillmentModel, OrderFullfillmentRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public OrderFullfillmentRepresentation convert(final OrderFullfillmentModel source) {

        OrderFullfillmentRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<OrderFullfillmentRepresentation> factory) {
        super.setFactory(factory);
    }

}
