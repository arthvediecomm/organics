/**
 *
 */
package com.arthvedi.organic.order.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.order.model.OrderFullfillmentModel;
import com.arthvedi.organic.order.representation.siren.OrderFullfillmentRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("orderFullfillmentRepresentationToOrderFullfillmentModelConverter")
public class OrderFullfillmentRepresentationToOrderFullfillmentModelConverter extends PropertyCopyingConverter<OrderFullfillmentRepresentation, OrderFullfillmentModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public OrderFullfillmentModel convert(final OrderFullfillmentRepresentation source) {

        OrderFullfillmentModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<OrderFullfillmentModel> factory) {
        super.setFactory(factory);
    }

}
