/**
 *
 */
package com.arthvedi.organic.order.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.OrderProductFeatures;
import com.arthvedi.organic.order.model.OrderProductFeaturesModel;

/**
 * @author Jay
 *
 */
@Component("orderProductFeaturesModelToOrderProductFeaturesConverter")
public class OrderProductFeaturesModelToOrderProductFeaturesConverter implements Converter<OrderProductFeaturesModel, OrderProductFeatures> {
    @Autowired
    private ObjectFactory<OrderProductFeatures> orderProductFeaturesFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public OrderProductFeatures convert(final OrderProductFeaturesModel source) {
        OrderProductFeatures orderProductFeatures = orderProductFeaturesFactory.getObject();
        BeanUtils.copyProperties(source, orderProductFeatures);

        return orderProductFeatures;
    }

    @Autowired
    public void setOrderProductFeaturesFactory(final ObjectFactory<OrderProductFeatures> orderProductFeaturesFactory) {
        this.orderProductFeaturesFactory = orderProductFeaturesFactory;
    }

}
