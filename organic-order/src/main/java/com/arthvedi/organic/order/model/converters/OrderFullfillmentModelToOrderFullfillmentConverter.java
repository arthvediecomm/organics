/**
 *
 */
package com.arthvedi.organic.order.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.order.domain.OrderFullfillment;
import com.arthvedi.organic.order.model.OrderFullfillmentModel;

/**
 * @author Jay
 *
 */
@Component("orderFullfillmentModelToOrderFullfillmentConverter")
public class OrderFullfillmentModelToOrderFullfillmentConverter implements Converter<OrderFullfillmentModel, OrderFullfillment> {
    @Autowired
    private ObjectFactory<OrderFullfillment> orderFullfillmentFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public OrderFullfillment convert(final OrderFullfillmentModel source) {
        OrderFullfillment orderFullfillment = orderFullfillmentFactory.getObject();
        BeanUtils.copyProperties(source, orderFullfillment);

        return orderFullfillment;
    }

    @Autowired
    public void setOrderFullfillmentFactory(final ObjectFactory<OrderFullfillment> orderFullfillmentFactory) {
        this.orderFullfillmentFactory = orderFullfillmentFactory;
    }

}
