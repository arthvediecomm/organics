package com.arthvedi.organic.order.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.order.businessdelegate.context.OrderFullfillmentDiscountContext;
import com.arthvedi.organic.order.model.OrderFullfillmentDiscountModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
* @author Administrator
*
*/

@RestController
@RequestMapping(value = "/orderFullfillmentDiscount", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class OrderFullfillmentDiscountController {

	private IBusinessDelegate<OrderFullfillmentDiscountModel, OrderFullfillmentDiscountContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<OrderFullfillmentDiscountContext> orderFullfillmentDiscountContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create")

	public ResponseEntity<OrderFullfillmentDiscountModel> createOrderFullfillmentDiscount(@RequestBody final OrderFullfillmentDiscountModel orderFullfillmentDiscountModel) {
		businessDelegate.create(orderFullfillmentDiscountModel);
		return new ResponseEntity<OrderFullfillmentDiscountModel>(orderFullfillmentDiscountModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	
	public ResponseEntity<OrderFullfillmentDiscountModel> edit(@PathVariable(value = "id") final String orderFullfillmentDiscountId,
			@RequestBody final OrderFullfillmentDiscountModel orderFullfillmentDiscountModel) {
		
		businessDelegate.edit(keyBuilderFactory.getObject().withId(orderFullfillmentDiscountId), orderFullfillmentDiscountModel);
		return new ResponseEntity<OrderFullfillmentDiscountModel>(orderFullfillmentDiscountModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all")

	public ResponseEntity<IModelWrapper> getAll() {
		OrderFullfillmentDiscountContext orderFullfillmentDiscountContext = orderFullfillmentDiscountContextFactory.getObject();
		orderFullfillmentDiscountContext.setAll("all");
		Collection<OrderFullfillmentDiscountModel> orderFullfillmentDiscountModels = businessDelegate.getCollection(orderFullfillmentDiscountContext);
		IModelWrapper<Collection<OrderFullfillmentDiscountModel>> models = new CollectionModelWrapper<OrderFullfillmentDiscountModel>(OrderFullfillmentDiscountModel.class,
				orderFullfillmentDiscountModels);
		return new ResponseEntity<IModelWrapper>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	
	public ResponseEntity<OrderFullfillmentDiscountModel> getOrderFullfillmentDiscount(@PathVariable(value = "id") final String orderFullfillmentDiscountId) {
		OrderFullfillmentDiscountContext orderFullfillmentDiscountContext = orderFullfillmentDiscountContextFactory.getObject();

		OrderFullfillmentDiscountModel model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(orderFullfillmentDiscountId),
				orderFullfillmentDiscountContext);
		return new ResponseEntity<OrderFullfillmentDiscountModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "orderFullfillmentDiscountBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<OrderFullfillmentDiscountModel, OrderFullfillmentDiscountContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setOrderFullfillmentDiscountObjectFactory(final ObjectFactory<OrderFullfillmentDiscountContext> orderFullfillmentDiscountContextFactory) {
		this.orderFullfillmentDiscountContextFactory = orderFullfillmentDiscountContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
