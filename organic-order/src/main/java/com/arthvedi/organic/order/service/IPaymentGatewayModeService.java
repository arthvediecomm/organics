package com.arthvedi.organic.order.service;

import java.util.List;

import com.arthvedi.organic.order.domain.PaymentGatewayMode;
/*
*@Author varma
*/
public interface IPaymentGatewayModeService {
	
	PaymentGatewayMode create(PaymentGatewayMode paymentGatewayMode);

	void deletePaymentGatewayMode(String paymentGatewayModeId);

	PaymentGatewayMode getPaymentGatewayMode(String paymentGatewayModeId);

	List<PaymentGatewayMode> getAll();

	PaymentGatewayMode updatePaymentGatewayMode(PaymentGatewayMode paymentGatewayMode);
}
