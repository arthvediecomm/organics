package com.arthvedi.organic.order.businessdelegate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.order.businessdelegate.context.OrderFullfillmentDiscountContext;
import com.arthvedi.organic.order.domain.OrderFullfillmentDiscount;
import com.arthvedi.organic.order.model.OrderFullfillmentDiscountModel;
import com.arthvedi.organic.order.service.IOrderFullfillmentDiscountService;

/*
*@Author varma
*/

@Service
public class OrderFullfillmentDiscountBusinessDelegate
		implements IBusinessDelegate<OrderFullfillmentDiscountModel, OrderFullfillmentDiscountContext, IKeyBuilder<String>, String> {

	@Autowired
	private IOrderFullfillmentDiscountService orderFullfillmentDiscountService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public OrderFullfillmentDiscountModel create(OrderFullfillmentDiscountModel model) {

		
		return null;
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, OrderFullfillmentDiscountContext context) {

	}

	@Override
	public OrderFullfillmentDiscountModel edit(IKeyBuilder<String> keyBuilder, OrderFullfillmentDiscountModel model) {
		OrderFullfillmentDiscount orderFullfillmentDiscount = orderFullfillmentDiscountService.getOrderFullfillmentDiscount(keyBuilder.build().toString());
		
		orderFullfillmentDiscount = orderFullfillmentDiscountService.updateOrderFullfillmentDiscount(orderFullfillmentDiscount);
		
		return model;
	}

	@Override
	public OrderFullfillmentDiscountModel getByKey(IKeyBuilder<String> keyBuilder, OrderFullfillmentDiscountContext context) {
		OrderFullfillmentDiscount orderFullfillmentDiscount = orderFullfillmentDiscountService.getOrderFullfillmentDiscount(keyBuilder.build().toString());
		OrderFullfillmentDiscountModel model = conversionService.convert(orderFullfillmentDiscount, OrderFullfillmentDiscountModel.class);
		return model;
	}

	@Override
	public Collection<OrderFullfillmentDiscountModel> getCollection(OrderFullfillmentDiscountContext context) {
		List<OrderFullfillmentDiscount> orderFullfillmentDiscount = new ArrayList<OrderFullfillmentDiscount>();
		if (context.getAll() != null) {
			orderFullfillmentDiscount = orderFullfillmentDiscountService.getAll();
		}
		List<OrderFullfillmentDiscountModel> orderFullfillmentDiscountModels = (List<OrderFullfillmentDiscountModel>) conversionService.convert(
				orderFullfillmentDiscount, TypeDescriptor.forObject(orderFullfillmentDiscount),
				TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(OrderFullfillmentDiscountModel.class)));
		return orderFullfillmentDiscountModels;
	}

	@Override
	public OrderFullfillmentDiscountModel edit(IKeyBuilder<String> keyBuilder,
			OrderFullfillmentDiscountModel model,
			OrderFullfillmentDiscountContext context) {
		return null;
	}

}
