package com.arthvedi.organic.order.model;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

	@Component("orderRefundModel")
	@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public class OrderRefundModel extends AbstractModel {
		
		private String orderId;
		private String refundActualAmount;
		private String refundPayableAmount;
		private String refundDescription;
		private String refundStatus;
		private String refundPaidReferenceNo;
		private String refundPaidStatus;
		private String statusMessage;
		
		
		public String getStatusMessage() {
			return statusMessage;
		}
		public void setStatusMessage(String statusMessage) {
			this.statusMessage = statusMessage;
		}
		public String getOrderId() {
			
			
			
			return orderId;
		}
		public void setOrderId(String orderId) {
			this.orderId = orderId;
		}
		public String getRefundActualAmount() {
			return refundActualAmount;
		}
		public void setRefundActualAmount(String refundActualAmount) {
			this.refundActualAmount = refundActualAmount;
		}
		public String getRefundPayableAmount() {
			return refundPayableAmount;
		}
		public void setRefundPayableAmount(String refundPayableAmount) {
			this.refundPayableAmount = refundPayableAmount;
		}
		public String getRefundDescription() {
			return refundDescription;
		}
		public void setRefundDescription(String refundDescription) {
			this.refundDescription = refundDescription;
		}
		public String getRefundStatus() {
			return refundStatus;
		}
		public void setRefundStatus(String refundStatus) {
			this.refundStatus = refundStatus;
		}
		public String getRefundPaidReferenceNo() {
			return refundPaidReferenceNo;
		}
		public void setRefundPaidReferenceNo(String refundPaidReferenceNo) {
			this.refundPaidReferenceNo = refundPaidReferenceNo;
		}
		public String getRefundPaidStatus() {
			return refundPaidStatus;
		}
		public void setRefundPaidStatus(String refundPaidStatus) {
			this.refundPaidStatus = refundPaidStatus;
		}
		
		
}
