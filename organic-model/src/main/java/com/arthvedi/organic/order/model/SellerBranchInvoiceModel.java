package com.arthvedi.organic.order.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("sellerBranchInvoiceModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SellerBranchInvoiceModel extends AbstractModel {

	private String sellerBranchId;
	private String billingPeriodFrom;
	private String billingPeriodTo;
	private String amount;
	private String marginAmount;
	private String paidStatus;
	private String statusMessage;
	
	private List<EcommerceOrderModel> handcraftOrderModel = new ArrayList<EcommerceOrderModel>(0);

	
	
	public String getMarginAmount() {
		return marginAmount;
	}

	public void setMarginAmount(String marginAmount) {
		this.marginAmount = marginAmount;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getSellerBranchId() {
		return sellerBranchId;
	}

	public void setSellerBranchId(String sellerBranchId) {
		this.sellerBranchId = sellerBranchId;
	}

	public String getBillingPeriodFrom() {
		return billingPeriodFrom;
	}

	public void setBillingPeriodFrom(String billingPeriodFrom) {
		this.billingPeriodFrom = billingPeriodFrom;
	}

	public String getBillingPeriodTo() {
		return billingPeriodTo;
	}

	public void setBillingPeriodTo(String billingPeriodTo) {
		this.billingPeriodTo = billingPeriodTo;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	

	public String getPaidStatus() {
		return paidStatus;
	}

	public void setPaidStatus(String paidStatus) {
		this.paidStatus = paidStatus;
	}

	public List<EcommerceOrderModel> getHandcraftOrderModel() {
		return handcraftOrderModel;
	}

	public void setHandcraftOrderModel(List<EcommerceOrderModel> handcraftOrderModel) {
		this.handcraftOrderModel = handcraftOrderModel;
	}

	

}
