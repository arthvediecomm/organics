package com.arthvedi.organic.order.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("orderProductModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class OrderProductModel extends AbstractModel {

	private String orderId;
	private String sellerProductId;
	private String marketPrice;
	private String sellerPrice;
	private String finalUnitPrice;
	private String status;
	private String cartStatusFlag;
	private String noOfUnits;
	private String unitOfMeasurement;
	private String otherDiscounts;
	private String totalTaxes;

	private String statusMessage;
	private List<OrderProductTaxesModel> orderProductTaxesModel = new ArrayList<OrderProductTaxesModel>(0);
	private List<OrderProductStatusLogModel> orderProductStatusLogModel = new ArrayList<OrderProductStatusLogModel>(0);
	private List<OrderProductFeaturesModel> orderProductFeaturesModel = new ArrayList<OrderProductFeaturesModel>(0);
	private List<OrderProductDiscountModel> orderProductDiscountModel = new ArrayList<OrderProductDiscountModel>(0);

	
	
	
	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	
	
	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getSellerProductId() {
		return sellerProductId;
	}

	public void setSellerProductId(String sellerProductId) {
		this.sellerProductId = sellerProductId;
	}

	public String getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(String marketPrice) {
		this.marketPrice = marketPrice;
	}

	public String getSellerPrice() {
		return sellerPrice;
	}

	public void setSellerPrice(String sellerPrice) {
		this.sellerPrice = sellerPrice;
	}

	public String getFinalUnitPrice() {
		return finalUnitPrice;
	}

	public void setFinalUnitPrice(String finalUnitPrice) {
		this.finalUnitPrice = finalUnitPrice;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCartStatusFlag() {
		return cartStatusFlag;
	}

	public void setCartStatusFlag(String cartStatusFlag) {
		this.cartStatusFlag = cartStatusFlag;
	}

	public String getNoOfUnits() {
		return noOfUnits;
	}

	public void setNoOfUnits(String noOfUnits) {
		this.noOfUnits = noOfUnits;
	}

	public String getUnitOfMeasurement() {
		return unitOfMeasurement;
	}

	public void setUnitOfMeasurement(String unitOfMeasurement) {
		this.unitOfMeasurement = unitOfMeasurement;
	}

	public String getOtherDiscounts() {
		return otherDiscounts;
	}

	public void setOtherDiscounts(String otherDiscounts) {
		this.otherDiscounts = otherDiscounts;
	}

	public String getTotalTaxes() {
		return totalTaxes;
	}

	public void setTotalTaxes(String totalTaxes) {
		this.totalTaxes = totalTaxes;
	}

	public List<OrderProductTaxesModel> getOrderProductTaxesModel() {
		return orderProductTaxesModel;
	}

	public void setOrderProductTaxesModel(List<OrderProductTaxesModel> orderProductTaxesModel) {
		this.orderProductTaxesModel = orderProductTaxesModel;
	}

	public List<OrderProductStatusLogModel> getOrderProductStatusLogModel() {
		return orderProductStatusLogModel;
	}

	public void setOrderProductStatusLogModel(List<OrderProductStatusLogModel> orderProductStatusLogModel) {
		this.orderProductStatusLogModel = orderProductStatusLogModel;
	}

	public List<OrderProductFeaturesModel> getOrderProductFeaturesModel() {
		return orderProductFeaturesModel;
	}

	public void setOrderProductFeaturesModel(List<OrderProductFeaturesModel> orderProductFeaturesModel) {
		this.orderProductFeaturesModel = orderProductFeaturesModel;
	}

	public List<OrderProductDiscountModel> getOrderProductDiscountModel() {
		return orderProductDiscountModel;
	}

	public void setOrderProductDiscountModel(List<OrderProductDiscountModel> orderProductDiscountModel) {
		this.orderProductDiscountModel = orderProductDiscountModel;
	}



	
}
