package com.arthvedi.organic.order.model;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("orderChargesModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class OrderChargesModel extends AbstractModel {

	private String orderId;
	private String orderChargescol;
	private String chargesCode;
	private String chargesValue;
	private String StatusMessage;
	
	
	
	
	public String getStatusMessage() {
		return StatusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		StatusMessage = statusMessage;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderChargescol() {
		return orderChargescol;
	}

	public void setOrderChargescol(String orderChargescol) {
		this.orderChargescol = orderChargescol;
	}

	public String getChargesCode() {
		return chargesCode;
	}

	public void setChargesCode(String chargesCode) {
		this.chargesCode = chargesCode;
	}

	public String getChargesValue() {
		return chargesValue;
	}

	public void setChargesValue(String chargesValue) {
		this.chargesValue = chargesValue;
	}

}
