package com.arthvedi.organic.order.model;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("orderProductStatusLogModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class OrderProductStatusLogModel extends AbstractModel {

	private String orderProductId;
	private String orderProductStatusCode;
	private String orderProductStatusDate;
	private String orderProductStatusDescription;
private String StatusMessage;




	public String getStatusMessage() {
	return StatusMessage;
}

public void setStatusMessage(String statusMessage) {
	StatusMessage = statusMessage;
}

	public String getOrderProductId() {
		return orderProductId;
	}

	public void setOrderProductId(String orderProductId) {
		this.orderProductId = orderProductId;
	}

	public String getOrderProductStatusCode() {
		return orderProductStatusCode;
	}

	public void setOrderProductStatusCode(String orderProductStatusCode) {
		this.orderProductStatusCode = orderProductStatusCode;
	}

	public String getOrderProductStatusDate() {
		return orderProductStatusDate;
	}

	public void setOrderProductStatusDate(String orderProductStatusDate) {
		this.orderProductStatusDate = orderProductStatusDate;
	}

	public String getOrderProductStatusDescription() {
		return orderProductStatusDescription;
	}

	public void setOrderProductStatusDescription(String orderProductStatusDescription) {
		this.orderProductStatusDescription = orderProductStatusDescription;
	}

}
