package com.arthvedi.organic.order.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("orderFullfillmentModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class OrderFullfillmentModel extends AbstractModel {

	private String ecommUserId;
	private String paymentGatewayId;
	private String paymentGatewayModeId;
	private String code;
	private String paymentGatewayProductInfo;
	private String orderFullfillmentcol;
	private String actualAmount;
	private String payableAmount;
	private String status;
	private String createdTime;
	private String modifiedTime;
	private String line1;
	private String town;
	private String area;
	private String city;
	private String district;
	private String state;
	private String country;
	private String zipCode;
	private String mobileNo;
	private String statesMessage;
	private List<OrderFullfillmentDiscountModel> orderFullfillmentDiscountModels = new ArrayList<OrderFullfillmentDiscountModel>(
			0);
	private List<EcommerceOrderModel> bnvOrderModels = new ArrayList<EcommerceOrderModel>(0);
	
	
	public String getStatesMessage() {
		return statesMessage;
	}
	public void setStatesMessage(String statesMessage) {
		this.statesMessage = statesMessage;
	}
	
	public String getEcommUserId() {
		return ecommUserId;
	}
	public void setEcommUserId(String ecommUserId) {
		this.ecommUserId = ecommUserId;
	}
	public String getPaymentGatewayId() {
		return paymentGatewayId;
	}
	public void setPaymentGatewayId(String paymentGatewayId) {
		this.paymentGatewayId = paymentGatewayId;
	}
	public String getPaymentGatewayModeId() {
		return paymentGatewayModeId;
	}
	public void setPaymentGatewayModeId(String paymentGatewayModeId) {
		this.paymentGatewayModeId = paymentGatewayModeId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getPaymentGatewayProductInfo() {
		return paymentGatewayProductInfo;
	}
	public void setPaymentGatewayProductInfo(String paymentGatewayProductInfo) {
		this.paymentGatewayProductInfo = paymentGatewayProductInfo;
	}
	public String getOrderFullfillmentcol() {
		return orderFullfillmentcol;
	}
	public void setOrderFullfillmentcol(String orderFullfillmentcol) {
		this.orderFullfillmentcol = orderFullfillmentcol;
	}
	public String getActualAmount() {
		return actualAmount;
	}
	public void setActualAmount(String actualAmount) {
		this.actualAmount = actualAmount;
	}
	public String getPayableAmount() {
		return payableAmount;
	}
	public void setPayableAmount(String payableAmount) {
		this.payableAmount = payableAmount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}
	public String getModifiedTime() {
		return modifiedTime;
	}
	public void setModifiedTime(String modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	public String getLine1() {
		return line1;
	}
	public void setLine1(String line1) {
		this.line1 = line1;
	}
	public String getTown() {
		return town;
	}
	public void setTown(String town) {
		this.town = town;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public List<OrderFullfillmentDiscountModel> getOrderFullfillmentDiscountModels() {
		return orderFullfillmentDiscountModels;
	}
	public void setOrderFullfillmentDiscountModels(List<OrderFullfillmentDiscountModel> orderFullfillmentDiscountModels) {
		this.orderFullfillmentDiscountModels = orderFullfillmentDiscountModels;
	}
	public List<EcommerceOrderModel> getBnvOrderModels() {
		return bnvOrderModels;
	}
	public void setBnvOrderModels(List<EcommerceOrderModel> bnvOrderModels) {
		this.bnvOrderModels = bnvOrderModels;
	}

	
	
}
