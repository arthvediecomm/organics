package com.arthvedi.organic.order.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("paymentGatewayModeModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PaymentGatewayModeModel extends AbstractModel {
	private String name;
	private String code;
	private String type;
	private String statusMessage;
	private List<OrderFullfillmentModel> orderFullfillmentModels = new ArrayList<OrderFullfillmentModel>(0);

	
	
	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<OrderFullfillmentModel> getOrderFullfillmentModels() {
		return orderFullfillmentModels;
	}

	public void setOrderFullfillmentModels(List<OrderFullfillmentModel> orderFullfillmentModels) {
		this.orderFullfillmentModels = orderFullfillmentModels;
	}

}
