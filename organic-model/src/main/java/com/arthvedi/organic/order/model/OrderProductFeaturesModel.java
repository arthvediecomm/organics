package com.arthvedi.organic.order.model;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("orderProductFeaturesModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class OrderProductFeaturesModel extends AbstractModel {

	private String orderProductId;
	private String featureCode;
	private String featureName;
	private String featureValue;
	private String featureCharge;
	private String statusMessage;

	
	
	
	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getOrderProductId() {
		return orderProductId;
	}

	public void setOrderProductId(String orderProductId) {
		this.orderProductId = orderProductId;
	}

	public String getFeatureCode() {
		return featureCode;
	}

	public void setFeatureCode(String featureCode) {
		this.featureCode = featureCode;
	}

	public String getFeatureName() {
		return featureName;
	}

	public void setFeatureName(String featureName) {
		this.featureName = featureName;
	}

	public String getFeatureValue() {
		return featureValue;
	}

	public void setFeatureValue(String featureValue) {
		this.featureValue = featureValue;
	}

	public String getFeatureCharge() {
		return featureCharge;
	}

	public void setFeatureCharge(String featureCharge) {
		this.featureCharge = featureCharge;
	}

}
