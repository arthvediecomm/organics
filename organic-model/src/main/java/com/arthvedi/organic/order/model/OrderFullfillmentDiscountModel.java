package com.arthvedi.organic.order.model;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("orderFullfillmentDiscountModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class OrderFullfillmentDiscountModel extends AbstractModel {

	private String orderFullfillmentId;
	private String discountId;
private String statusMessage;


	public String getStatusMessage() {
	return statusMessage;
}

public void setStatusMessage(String statusMessage) {
	this.statusMessage = statusMessage;
}

	public String getOrderFullfillmentId() {
		return orderFullfillmentId;
	}

	public void setOrderFullfillmentId(String orderFullfillmentId) {
		this.orderFullfillmentId = orderFullfillmentId;
	}

	public String getDiscountId() {
		return discountId;
	}

	public void setDiscountId(String discountId) {
		this.discountId = discountId;
	}

}
