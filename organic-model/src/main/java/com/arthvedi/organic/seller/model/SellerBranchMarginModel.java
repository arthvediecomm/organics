package com.arthvedi.organic.seller.model;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("sellerBranchMarginModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SellerBranchMarginModel extends AbstractModel {

	private String sellerBranchId;
	private String margin;
	private String period;
	private String amount;
	private String amountType;

	

	public String getSellerBranchId() {
		return sellerBranchId;
	}

	public void setSellerBranchId(String sellerBranchId) {
		this.sellerBranchId = sellerBranchId;
	}

	public String getMargin() {
		return margin;
	}

	public void setMargin(String margin) {
		this.margin = margin;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getAmountType() {
		return amountType;
	}

	public void setAmountType(String amountType) {
		this.amountType = amountType;
	}

}
