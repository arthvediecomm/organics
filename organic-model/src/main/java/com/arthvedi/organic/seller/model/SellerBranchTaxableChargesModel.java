package com.arthvedi.organic.seller.model;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("sellerBranchTaxableChargesModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SellerBranchTaxableChargesModel extends AbstractModel {

	private String sellerBranchTaxId;
	private String sellerBranchChargesId;
	private String status;
	private String statusMessage;

	public String getSellerBranchTaxId() {
		return sellerBranchTaxId;
	}

	public void setSellerBranchTaxId(String sellerBranchTaxId) {
		this.sellerBranchTaxId = sellerBranchTaxId;
	}

	public String getSellerBranchChargesId() {
		return sellerBranchChargesId;
	}

	public void setSellerBranchChargesId(String sellerBranchChargesId) {
		this.sellerBranchChargesId = sellerBranchChargesId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

}