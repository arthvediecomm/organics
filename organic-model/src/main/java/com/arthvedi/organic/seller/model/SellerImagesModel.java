/**
 *
 */
package com.arthvedi.organic.seller.model;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

/**
 * @author venky
 *
 */

@Component("sellerImagesModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SellerImagesModel extends AbstractModel {

	private String sellerId;
	private String sellerName;
	private String imageFolder;

	private String sellerType;
	private String status;
	private String statusMessage;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getImageFolder() {
		return imageFolder;
	}

	public String getSellerId() {
		return sellerId;
	}

	public String getSellerName() {
		return sellerName;
	}

	public String getSellerType() {
		return sellerType;
	}

	public void setImageFolder(final String imageFolder) {
		this.imageFolder = imageFolder;
	}

	public void setSellerId(final String sellerId) {
		this.sellerId = sellerId;
	}

	public void setSellerName(final String sellerName) {
		this.sellerName = sellerName;
	}

	public void setSellerType(final String sellerType) {
		this.sellerType = sellerType;
	}

}
