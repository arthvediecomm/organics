package com.arthvedi.organic.seller.model;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("sellerBranchImagesModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SellerBranchImagesModel extends AbstractModel {

	private String sellerBranchId;
	private String imageName;
	private String imageType;
	private String imageLocation;
	private String status;
	private String imagesStatus;
	private String imageFolder;

	public String getImageFolder() {
		return imageFolder;
	}

	public String getImageLocation() {
		return imageLocation;
	}

	public String getImageName() {
		return imageName;
	}

	public String getImageType() {
		return imageType;
	}

	public String getSellerBranchId() {
		return sellerBranchId;
	}

	public void setImageFolder(final String imageFolder) {
		this.imageFolder = imageFolder;
	}

	public void setImageLocation(final String imageLocation) {
		this.imageLocation = imageLocation;
	}

	public void setImageName(final String imageName) {
		this.imageName = imageName;
	}

	public void setImageType(final String imageType) {
		this.imageType = imageType;
	}

	public void setSellerBranchId(final String sellerBranchId) {
		this.sellerBranchId = sellerBranchId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getImagesStatus() {
		return imagesStatus;
	}

	public void setImagesStatus(String imagesStatus) {
		this.imagesStatus = imagesStatus;
	}

}
