package com.arthvedi.organic.seller.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("sellerBranchTaxModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SellerBranchTaxModel extends AbstractModel {

	private String taxId;
	private String sellerBranchId;
	private String status;
	private String statusMessage;
	private String taxName;
	private List<SellerBranchTaxableChargesModel> sellerBranchTaxableChargesModels = new ArrayList<SellerBranchTaxableChargesModel>(
			0);

	public String getTaxId() {
		return taxId;
	}

	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	public String getSellerBranchId() {
		return sellerBranchId;
	}

	public void setSellerBranchId(String sellerBranchId) {
		this.sellerBranchId = sellerBranchId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<SellerBranchTaxableChargesModel> getSellerBranchTaxableChargesModels() {
		return sellerBranchTaxableChargesModels;
	}

	public void setSellerBranchTaxableChargesModels(
			List<SellerBranchTaxableChargesModel> sellerBranchTaxableChargesModels) {
		this.sellerBranchTaxableChargesModels = sellerBranchTaxableChargesModels;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getTaxName() {
		return taxName;
	}

	public void setTaxName(String taxName) {
		this.taxName = taxName;
	}

}
