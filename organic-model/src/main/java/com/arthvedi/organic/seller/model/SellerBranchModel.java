package com.arthvedi.organic.seller.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;
import com.arthvedi.organic.productcatalog.model.SellerProductModel;

@Component("sellerBranchModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SellerBranchModel extends AbstractModel {

	private String sellerId;
	private String branchName;
	private String sellerEmailId;
	private String sellerPhoneNo;
	private String branchStatus;
	private String landlineNo;
	private String minimumOrderAmount;
	private String tinNumber;
	private String statusMessage;

	private List<SellerBranchAddressModel> sellerBranchAddressesModel = new ArrayList<SellerBranchAddressModel>(
			0);
	private List<SellerBranchChargesModel> sellerBranchChargesesModel = new ArrayList<SellerBranchChargesModel>(
			0);
	private List<SellerBranchUserModel> sellerBranchUsersModel = new ArrayList<SellerBranchUserModel>(
			0);
	private List<SellerProductModel> sellerProductsModel = new ArrayList<SellerProductModel>(
			0);
	private List<SellerBranchBankAccountModel> sellerBranchBankAccountsModel = new ArrayList<SellerBranchBankAccountModel>(
			0);
	private List<SellerBranchMarginModel> sellerBranchMarginsModel = new ArrayList<SellerBranchMarginModel>(
			0);
	private List<SellerBranchImagesModel> sellerBranchImagesesModel = new ArrayList<SellerBranchImagesModel>(
			0);
	private Set<SellerBranchZoneModel> sellerBranchZonesModel = new HashSet<SellerBranchZoneModel>(
			0);

	public Set<SellerBranchZoneModel> getSellerBranchZonesModel() {
		return sellerBranchZonesModel;
	}

	public void setSellerBranchZonesModel(
			Set<SellerBranchZoneModel> sellerBranchZonesModel) {
		this.sellerBranchZonesModel = sellerBranchZonesModel;
	}

	public List<SellerBranchImagesModel> getSellerBranchImagesesModel() {
		return sellerBranchImagesesModel;
	}

	public void setSellerBranchImagesesModel(
			List<SellerBranchImagesModel> sellerBranchImagesesModel) {
		this.sellerBranchImagesesModel = sellerBranchImagesesModel;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getSellerId() {
		return sellerId;
	}

	public void setSellerId(String sellerId) {
		this.sellerId = sellerId;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getSellerEmailId() {
		return sellerEmailId;
	}

	public void setSellerEmailId(String sellerEmailId) {
		this.sellerEmailId = sellerEmailId;
	}

	public String getSellerPhoneNo() {
		return sellerPhoneNo;
	}

	public void setSellerPhoneNo(String sellerPhoneNo) {
		this.sellerPhoneNo = sellerPhoneNo;
	}

	public String getBranchStatus() {
		return branchStatus;
	}

	public void setBranchStatus(String branchStatus) {
		this.branchStatus = branchStatus;
	}

	public String getLandlineNo() {
		return landlineNo;
	}

	public void setLandlineNo(String landlineNo) {
		this.landlineNo = landlineNo;
	}

	public String getMinimumOrderAmount() {
		return minimumOrderAmount;
	}

	public void setMinimumOrderAmount(String minimumOrderAmount) {
		this.minimumOrderAmount = minimumOrderAmount;
	}

	public String getTinNumber() {
		return tinNumber;
	}

	public void setTinNumber(String tinNumber) {
		this.tinNumber = tinNumber;
	}

	public List<SellerBranchAddressModel> getSellerBranchAddressesModel() {
		return sellerBranchAddressesModel;
	}

	public void setSellerBranchAddressesModel(
			List<SellerBranchAddressModel> sellerBranchAddressesModel) {
		this.sellerBranchAddressesModel = sellerBranchAddressesModel;
	}

	public List<SellerBranchChargesModel> getSellerBranchChargesesModel() {
		return sellerBranchChargesesModel;
	}

	public void setSellerBranchChargesesModel(
			List<SellerBranchChargesModel> sellerBranchChargesesModel) {
		this.sellerBranchChargesesModel = sellerBranchChargesesModel;
	}

	public List<SellerBranchUserModel> getSellerBranchUsersModel() {
		return sellerBranchUsersModel;
	}

	public void setSellerBranchUsersModel(
			List<SellerBranchUserModel> sellerBranchUsersModel) {
		this.sellerBranchUsersModel = sellerBranchUsersModel;
	}

	public List<SellerProductModel> getSellerProductsModel() {
		return sellerProductsModel;
	}

	public void setSellerProductsModel(
			List<SellerProductModel> sellerProductsModel) {
		this.sellerProductsModel = sellerProductsModel;
	}

	public List<SellerBranchBankAccountModel> getSellerBranchBankAccountsModel() {
		return sellerBranchBankAccountsModel;
	}

	public void setSellerBranchBankAccountsModel(
			List<SellerBranchBankAccountModel> sellerBranchBankAccountsModel) {
		this.sellerBranchBankAccountsModel = sellerBranchBankAccountsModel;
	}

	public List<SellerBranchMarginModel> getSellerBranchMarginsModel() {
		return sellerBranchMarginsModel;
	}

	public void setSellerBranchMarginsModel(
			List<SellerBranchMarginModel> sellerBranchMarginsModel) {
		this.sellerBranchMarginsModel = sellerBranchMarginsModel;
	}

}
