package com.arthvedi.organic.seller.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("sellerModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SellerModel extends AbstractModel {

	private String sellerName;
	private String description;
	private String status;
	private String sellerType;
	private String statusMessage;
	private List<SellerImagesModel> sellerImagesesModel = new ArrayList<SellerImagesModel>(
			0);
	private List<SellerBranchModel> sellerBranchesModel = new ArrayList<SellerBranchModel>(
			0);

	
	
	public List<SellerImagesModel> getSellerImagesesModel() {
		return sellerImagesesModel;
	}

	public void setSellerImagesesModel(List<SellerImagesModel> sellerImagesesModel) {
		this.sellerImagesesModel = sellerImagesesModel;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getSellerName() {
		return sellerName;
	}

	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSellerType() {
		return sellerType;
	}

	public void setSellerType(String sellerType) {
		this.sellerType = sellerType;
	}

	public List<SellerBranchModel> getSellerBranchesModel() {
		return sellerBranchesModel;
	}

	public void setSellerBranchesModel(
			List<SellerBranchModel> sellerBranchesModel) {
		this.sellerBranchesModel = sellerBranchesModel;
	}

}
