package com.arthvedi.organic.requests.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("requestTypeAttributeModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RequestTypeAttributeModel extends AbstractModel {

	private String requestTypeId;
	private String requestTypeAttributeName;
	private String requestTypeAttributeDatatype;
	private String status;
	private String statusMessage;
	private List<RequestTypeAttributeValueModel> requestTypeAttributeValueModels = new ArrayList<RequestTypeAttributeValueModel>(
			0);

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getRequestTypeId() {
		return requestTypeId;
	}

	public void setRequestTypeId(String requestTypeId) {
		this.requestTypeId = requestTypeId;
	}

	public String getRequestTypeAttributeName() {
		return requestTypeAttributeName;
	}

	public void setRequestTypeAttributeName(String requestTypeAttributeName) {
		this.requestTypeAttributeName = requestTypeAttributeName;
	}

	public String getRequestTypeAttributeDatatype() {
		return requestTypeAttributeDatatype;
	}

	public void setRequestTypeAttributeDatatype(
			String requestTypeAttributeDatatype) {
		this.requestTypeAttributeDatatype = requestTypeAttributeDatatype;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<RequestTypeAttributeValueModel> getRequestTypeAttributeValueModels() {
		return requestTypeAttributeValueModels;
	}

	public void setRequestTypeAttributeValueModels(
			List<RequestTypeAttributeValueModel> requestTypeAttributeValueModels) {
		this.requestTypeAttributeValueModels = requestTypeAttributeValueModels;
	}

}
