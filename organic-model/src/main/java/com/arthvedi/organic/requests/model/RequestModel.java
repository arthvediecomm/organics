package com.arthvedi.organic.requests.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("requestModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RequestModel extends AbstractModel {

	private String ecommUserId;
	private String requestTypeName;
	private String requestDetails;
	private String userType;
	private String priority;
	private String status;
	private String requestMessage;
	private String statusMessage;
	private List<RequestAttributesModel> requestAttributesModels = new ArrayList<RequestAttributesModel>(
			0);
	private List<RequestStatusCodesModel> requestStatusCodesModels = new ArrayList<RequestStatusCodesModel>(
			0);

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getEcommUserId() {
		return ecommUserId;
	}

	public void setEcommUserId(String ecommUserId) {
		this.ecommUserId = ecommUserId;
	}

	public String getRequestTypeName() {
		return requestTypeName;
	}

	public void setRequestTypeName(String requestTypeName) {
		this.requestTypeName = requestTypeName;
	}

	public String getRequestDetails() {
		return requestDetails;
	}

	public void setRequestDetails(String requestDetails) {
		this.requestDetails = requestDetails;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRequestMessage() {
		return requestMessage;
	}

	public void setRequestMessage(String requestMessage) {
		this.requestMessage = requestMessage;
	}

	public List<RequestAttributesModel> getRequestAttributesModels() {
		return requestAttributesModels;
	}

	public void setRequestAttributesModels(
			List<RequestAttributesModel> requestAttributesModels) {
		this.requestAttributesModels = requestAttributesModels;
	}

	public List<RequestStatusCodesModel> getRequestStatusCodesModels() {
		return requestStatusCodesModels;
	}

	public void setRequestStatusCodesModels(
			List<RequestStatusCodesModel> requestStatusCodesModels) {
		this.requestStatusCodesModels = requestStatusCodesModels;
	}

}
