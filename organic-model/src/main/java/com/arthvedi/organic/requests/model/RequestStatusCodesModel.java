package com.arthvedi.organic.requests.model;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("requestStatusCodesModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RequestStatusCodesModel extends AbstractModel {

	private String requestAttributesId;
	private String assigneeId;
	private String status;
	private String description;
	private String statusMessage;

	
	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getRequestAttributesId() {
		return requestAttributesId;
	}

	public void setRequestAttributesId(String requestAttributesId) {
		this.requestAttributesId = requestAttributesId;
	}

	

	public String getAssigneeId() {
		return assigneeId;
	}

	public void setAssigneeId(String assigneeId) {
		this.assigneeId = assigneeId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
