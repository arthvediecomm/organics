package com.arthvedi.organic.requests.model;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("requestTypeAttributeValueModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RequestTypeAttributeValueModel extends AbstractModel {

	private String requestTypeAttributeId;
	private String requestTypeAttributeValue;
	private String status;
	private String statusMessage;

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getRequestTypeAttributeId() {
		return requestTypeAttributeId;
	}

	public void setRequestTypeAttributeId(String requestTypeAttributeId) {
		this.requestTypeAttributeId = requestTypeAttributeId;
	}

	public String getRequestTypeAttributeValue() {
		return requestTypeAttributeValue;
	}

	public void setRequestTypeAttributeValue(String requestTypeAttributeValue) {
		this.requestTypeAttributeValue = requestTypeAttributeValue;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
