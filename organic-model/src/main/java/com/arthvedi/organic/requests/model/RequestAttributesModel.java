package com.arthvedi.organic.requests.model;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("requestAttributesModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RequestAttributesModel extends AbstractModel {

	private String requestId;
	private String requestAttributeName;
	private String requestAttributeValue;
	private String status;
	private String statusMessage;

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getRequestAttributeName() {
		return requestAttributeName;
	}

	public void setRequestAttributeName(String requestAttributeName) {
		this.requestAttributeName = requestAttributeName;
	}

	public String getRequestAttributeValue() {
		return requestAttributeValue;
	}

	public void setRequestAttributeValue(String requestAttributeValue) {
		this.requestAttributeValue = requestAttributeValue;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

}
