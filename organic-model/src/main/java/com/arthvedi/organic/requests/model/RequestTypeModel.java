package com.arthvedi.organic.requests.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("requestTypeModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RequestTypeModel extends AbstractModel {

	private String name;
	private String priority;
	private String status;
	private String statusMessage;
	private List<RequestTypeAttributeModel> requestTypeAttributeModels = new ArrayList<RequestTypeAttributeModel>(
			0);

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<RequestTypeAttributeModel> getRequestTypeAttributeModels() {
		return requestTypeAttributeModels;
	}

	public void setRequestTypeAttributeModels(
			List<RequestTypeAttributeModel> requestTypeAttributeModels) {
		this.requestTypeAttributeModels = requestTypeAttributeModels;
	}

}
