package com.arthvedi.organic.enums;

public enum ProductType {
	COMBO, SINGLE;
}
