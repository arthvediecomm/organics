package com.arthvedi.organic.enums;

public enum SubscriptionStatus {
	SUBSCRIBED, UNSUBSCRIBED;
}
