package com.arthvedi.organic.commons.model;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("cityModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CityModel extends AbstractModel {

	private String languageId;
	private String cityName;
	private String status;
	private String geographicalName;
	private String statusMessage;

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getLanguageId() {
		return languageId;
	}

	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getGeographicalName() {
		return geographicalName;
	}

	public void setGeographicalName(String geographicalName) {
		this.geographicalName = geographicalName;
	}

}
