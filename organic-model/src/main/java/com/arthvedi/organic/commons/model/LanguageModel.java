package com.arthvedi.organic.commons.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;
import com.arthvedi.organic.productcatalog.model.CategoryNamesModel;
import com.arthvedi.organic.productcatalog.model.ProductNamesModel;

@Component("languageModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class LanguageModel extends AbstractModel {

	private String status;
	private String name;
	private String statusMessage;

	private List<CategoryNamesModel> categoryNamesesModel = new ArrayList<CategoryNamesModel>(
			0);
	private List<CityModel> citiesModel = new ArrayList<CityModel>(0);
	private List<ProductNamesModel> productNamesesModel = new ArrayList<ProductNamesModel>(
			0);

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<CategoryNamesModel> getCategoryNamesesModel() {
		return categoryNamesesModel;
	}

	public void setCategoryNamesesModel(
			List<CategoryNamesModel> categoryNamesesModel) {
		this.categoryNamesesModel = categoryNamesesModel;
	}

	public List<CityModel> getCitiesModel() {
		return citiesModel;
	}

	public void setCitiesModel(List<CityModel> citiesModel) {
		this.citiesModel = citiesModel;
	}

	public List<ProductNamesModel> getProductNamesesModel() {
		return productNamesesModel;
	}

	public void setProductNamesesModel(
			List<ProductNamesModel> productNamesesModel) {
		this.productNamesesModel = productNamesesModel;
	}

}
