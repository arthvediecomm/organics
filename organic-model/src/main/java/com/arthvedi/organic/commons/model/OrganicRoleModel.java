package com.arthvedi.organic.commons.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("organicRoleModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class OrganicRoleModel extends AbstractModel {

	private String roleName;
	private String status;

	private List<OrganicUserModel> organicUsersModel = new ArrayList<OrganicUserModel>(0);

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<OrganicUserModel> getOrganicUsersModel() {
		return organicUsersModel;
	}

	public void setOrganicUsersModel(List<OrganicUserModel> organicUsersModel) {
		this.organicUsersModel = organicUsersModel;
	}

}
