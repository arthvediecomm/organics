package com.arthvedi.organic.commons.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;
import com.arthvedi.organic.seller.model.SellerBranchAddressModel;

@Component("addressModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AddressModel extends AbstractModel {

	private String line1;
	private String town;
	private String city;
	private String district;
	private String contactPerson;
	private String mobileNo;
	private String zipcode;
	private String landmark;
	private String country;
	private String type;
	private String state;
	private String defaultStatus;
	private String statusMessage;

	private List<SellerBranchAddressModel> sellerBranchAddressesModel = new ArrayList<SellerBranchAddressModel>(
			0);

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getLine1() {
		return line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getLandmark() {
		return landmark;
	}

	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDefaultStatus() {
		return defaultStatus;
	}

	public void setDefaultStatus(String defaultStatus) {
		this.defaultStatus = defaultStatus;
	}

	public List<SellerBranchAddressModel> getSellerBranchAddressesModel() {
		return sellerBranchAddressesModel;
	}

	public void setSellerBranchAddressesModel(
			List<SellerBranchAddressModel> sellerBranchAddressesModel) {
		this.sellerBranchAddressesModel = sellerBranchAddressesModel;
	}

}
