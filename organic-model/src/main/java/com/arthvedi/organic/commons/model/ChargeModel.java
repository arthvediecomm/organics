package com.arthvedi.organic.commons.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;
import com.arthvedi.organic.seller.model.SellerBranchChargesModel;

@Component("chargeModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ChargeModel extends AbstractModel {

	private String chargeName;
	private String status;
	private String statusMessage;

	private List<SellerBranchChargesModel> sellerBranchChargesesModel = new ArrayList<SellerBranchChargesModel>(0);

	
	
	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getChargeName() {
		return chargeName;
	}

	public void setChargeName(String chargeName) {
		this.chargeName = chargeName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<SellerBranchChargesModel> getSellerBranchChargesesModel() {
		return sellerBranchChargesesModel;
	}

	public void setSellerBranchChargesesModel(List<SellerBranchChargesModel> sellerBranchChargesesModel) {
		this.sellerBranchChargesesModel = sellerBranchChargesesModel;
	}

}
