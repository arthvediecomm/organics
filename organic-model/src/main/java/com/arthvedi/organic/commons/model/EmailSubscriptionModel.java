package com.arthvedi.organic.commons.model;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("emailSubscriptionModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class EmailSubscriptionModel extends AbstractModel {

	private String userEmail;
	private String emailSubscriptioncol;
	private String subscriptionStatus;
	private String statusMessage;

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getEmailSubscriptioncol() {
		return emailSubscriptioncol;
	}

	public void setEmailSubscriptioncol(String emailSubscriptioncol) {
		this.emailSubscriptioncol = emailSubscriptioncol;
	}

	public String getSubscriptionStatus() {
		return subscriptionStatus;
	}

	public void setSubscriptionStatus(String subscriptionStatus) {
		this.subscriptionStatus = subscriptionStatus;
	}

}
