package com.arthvedi.organic.commons.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;
import com.arthvedi.organic.seller.model.SellerBranchUserModel;

@Component("organicUserModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class OrganicUserModel extends AbstractModel {

	private String languageId;
	private String organicRoleId;
	private String userName;
	private String userType;
	private String emailId;
	private String phoneNo;
	private String password;
	private String status;
	private String emailVerificationStatus;
	private String userRole;
	private String source;

	private List<SellerBranchUserModel> sellerBranchUsersModel = new ArrayList<SellerBranchUserModel>(0);

	public String getLanguageId() {
		return languageId;
	}

	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}

	public String getOrganicRoleId() {
		return organicRoleId;
	}

	public void setOrganicRoleId(String organicRoleId) {
		this.organicRoleId = organicRoleId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getEmailVerificationStatus() {
		return emailVerificationStatus;
	}

	public void setEmailVerificationStatus(String emailVerificationStatus) {
		this.emailVerificationStatus = emailVerificationStatus;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public List<SellerBranchUserModel> getSellerBranchUsersModel() {
		return sellerBranchUsersModel;
	}

	public void setSellerBranchUsersModel(List<SellerBranchUserModel> sellerBranchUsersModel) {
		this.sellerBranchUsersModel = sellerBranchUsersModel;
	}

}
