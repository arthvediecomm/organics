package com.arthvedi.organic.commons.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;
import com.arthvedi.organic.seller.model.SellerBranchZoneModel;

@Component("zoneModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ZoneModel extends AbstractModel {

	private String cityId;
	private String zoneName;
	private String geographicalZoneName;
	private String status;
	private String description;
	private String statusMessage;
	private List<SellerBranchZoneModel> sellerBranchZoneModels = new ArrayList<SellerBranchZoneModel>(
			0);
	private List<ZoneAreaModel> zoneAreaModels = new ArrayList<ZoneAreaModel>(0);

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	public String getGeographicalZoneName() {
		return geographicalZoneName;
	}

	public void setGeographicalZoneName(String geographicalZoneName) {
		this.geographicalZoneName = geographicalZoneName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<SellerBranchZoneModel> getSellerBranchZoneModels() {
		return sellerBranchZoneModels;
	}

	public void setSellerBranchZoneModels(
			List<SellerBranchZoneModel> sellerBranchZoneModels) {
		this.sellerBranchZoneModels = sellerBranchZoneModels;
	}

	public List<ZoneAreaModel> getZoneAreaModels() {
		return zoneAreaModels;
	}

	public void setZoneAreaModels(List<ZoneAreaModel> zoneAreaModels) {
		this.zoneAreaModels = zoneAreaModels;
	}

}
