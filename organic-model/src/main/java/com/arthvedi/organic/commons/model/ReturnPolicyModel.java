package com.arthvedi.organic.commons.model;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("returnPolicyModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ReturnPolicyModel extends AbstractModel {

	private String refundAmount;
	private String refundDescription;
	private String taxesInclude;
	private String chargesInclude;
	private String noOfDays;

	
	
	public String getNoOfDays() {
		return noOfDays;
	}

	public void setNoOfDays(String noOfDays) {
		this.noOfDays = noOfDays;
	}

	public String getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(String refundAmount) {
		this.refundAmount = refundAmount;
	}

	public String getRefundDescription() {
		return refundDescription;
	}

	public void setRefundDescription(String refundDescription) {
		this.refundDescription = refundDescription;
	}

	public String getTaxesInclude() {
		return taxesInclude;
	}

	public void setTaxesInclude(String taxesInclude) {
		this.taxesInclude = taxesInclude;
	}

	public String getChargesInclude() {
		return chargesInclude;
	}

	public void setChargesInclude(String chargesInclude) {
		this.chargesInclude = chargesInclude;
	}

}
