package com.arthvedi.organic.offer.model;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("offerAuditModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class OfferAuditModel extends AbstractModel {
	
	private String offerFulfillmentId;
	private String ecommUserId;
	private String usesCount;
	private String status;
	private String statusMessage;
	
	
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	public String getOfferFulfillmentId() {
		return offerFulfillmentId;
	}
	
	
	public void setOfferFulfillmentId(String offerFulfillmentId) {
		this.offerFulfillmentId = offerFulfillmentId;
	}
	public String getEcommUserId() {
		return ecommUserId;
	}
	public void setEcommUserId(String ecommUserId) {
		this.ecommUserId = ecommUserId;
	}
	public String getUsesCount() {
		return usesCount;
	}
	public void setUsesCount(String usesCount) {
		this.usesCount = usesCount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

}
