package com.arthvedi.organic.user.model;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("userWishlistModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UserWishlistModel extends AbstractModel {

	private String description;
	private String ecommerceUserId;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEcommerceUserId() {
		return ecommerceUserId;
	}

	public void setEcommerceUserId(String ecommerceUserId) {
		this.ecommerceUserId = ecommerceUserId;
	}

}
