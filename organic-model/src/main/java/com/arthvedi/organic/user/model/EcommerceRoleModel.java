package com.arthvedi.organic.user.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("ecommerceRoleModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class EcommerceRoleModel extends AbstractModel {
	private String roleName;
	private String status;

	private List<EcommerceUserModel> ecommerceUsersModel = new ArrayList<EcommerceUserModel>(
			0);

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<EcommerceUserModel> getEcommerceUsersModel() {
		return ecommerceUsersModel;
	}

	public void setEcommerceUsersModel(
			List<EcommerceUserModel> ecommerceUsersModel) {
		this.ecommerceUsersModel = ecommerceUsersModel;
	}

}
