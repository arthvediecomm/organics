package com.arthvedi.organic.user.model;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("userProductRatingModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UserProductRatingModel extends AbstractModel {

	private String rating;
	private String productRatingStatus;
	private String comments;

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getProductRatingStatus() {
		return productRatingStatus;
	}

	public void setProductRatingStatus(String productRatingStatus) {
		this.productRatingStatus = productRatingStatus;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

}
