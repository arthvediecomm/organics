package com.arthvedi.organic.user.model;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("userSellerProductRatingModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UserSellerProductRatingModel extends AbstractModel {

	private String description;
	private String rating;
	private String title;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
