package com.arthvedi.organic.productcatalog.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("productModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ProductModel extends AbstractModel {

	private String brandId;
	private String productName;
	private String description;
	private String status;
	private String mrp;
	private String measurementUnit;
	private String productType;
	private String statusMesage;
	private String quantity;
	private String baseUnit;
	private List<ComboProductModel> comboProductsForParentProductIdModel = new ArrayList<ComboProductModel>(
			0);
	private List<ComboProductModel> comboProductsForChildProductIdModel = new ArrayList<ComboProductModel>(
			0);
	private List<ProductNamesModel> productNamesesModel = new ArrayList<ProductNamesModel>(
			0);
	private List<ProductFeatureModel> productFeaturesModel = new ArrayList<ProductFeatureModel>(
			0);
	private List<SellerProductModel> sellerProductsModel = new ArrayList<SellerProductModel>(
			0);
	private List<ProductCategoryModel> productCategoriesModel = new ArrayList<ProductCategoryModel>(
			0);
	private List<ProductImagesModel> productImagesesModel = new ArrayList<ProductImagesModel>(
			0);

	public List<ProductImagesModel> getProductImagesesModel() {
		return productImagesesModel;
	}

	public void setProductImagesesModel(
			List<ProductImagesModel> productImagesesModel) {
		this.productImagesesModel = productImagesesModel;
	}

	public String getStatusMesage() {
		return statusMesage;
	}

	public void setStatusMesage(String statusMesage) {
		this.statusMesage = statusMesage;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getBrandId() {
		return brandId;
	}

	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMrp() {
		return mrp;
	}

	public void setMrp(String mrp) {
		this.mrp = mrp;
	}

	public String getMeasurementUnit() {
		return measurementUnit;
	}

	public void setMeasurementUnit(String measurementUnit) {
		this.measurementUnit = measurementUnit;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public List<ComboProductModel> getComboProductsForParentProductIdModel() {
		return comboProductsForParentProductIdModel;
	}

	public void setComboProductsForParentProductIdModel(
			List<ComboProductModel> comboProductsForParentProductIdModel) {
		this.comboProductsForParentProductIdModel = comboProductsForParentProductIdModel;
	}

	public List<ComboProductModel> getComboProductsForChildProductIdModel() {
		return comboProductsForChildProductIdModel;
	}

	public void setComboProductsForChildProductIdModel(
			List<ComboProductModel> comboProductsForChildProductIdModel) {
		this.comboProductsForChildProductIdModel = comboProductsForChildProductIdModel;
	}

	public List<ProductNamesModel> getProductNamesesModel() {
		return productNamesesModel;
	}

	public void setProductNamesesModel(
			List<ProductNamesModel> productNamesesModel) {
		this.productNamesesModel = productNamesesModel;
	}

	public List<ProductFeatureModel> getProductFeaturesModel() {
		return productFeaturesModel;
	}

	public void setProductFeaturesModel(
			List<ProductFeatureModel> productFeaturesModel) {
		this.productFeaturesModel = productFeaturesModel;
	}

	public List<SellerProductModel> getSellerProductsModel() {
		return sellerProductsModel;
	}

	public void setSellerProductsModel(
			List<SellerProductModel> sellerProductsModel) {
		this.sellerProductsModel = sellerProductsModel;
	}

	public List<ProductCategoryModel> getProductCategoriesModel() {
		return productCategoriesModel;
	}

	public void setProductCategoriesModel(
			List<ProductCategoryModel> productCategoriesModel) {
		this.productCategoriesModel = productCategoriesModel;
	}

}
