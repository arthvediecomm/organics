package com.arthvedi.organic.productcatalog.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("featureModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class FeatureModel extends AbstractModel {

	private String featureName;
	private String featureValue;
	private String code;
	private String status;

	private List<ProductFeatureModel> productFeaturesModel = new ArrayList<ProductFeatureModel>(0);
	private List<CategoryFeatureModel> categoryFeaturesModel = new ArrayList<CategoryFeatureModel>(0);

	public String getFeatureName() {
		return featureName;
	}

	public void setFeatureName(String featureName) {
		this.featureName = featureName;
	}

	public String getFeatureValue() {
		return featureValue;
	}

	public void setFeatureValue(String featureValue) {
		this.featureValue = featureValue;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<ProductFeatureModel> getProductFeaturesModel() {
		return productFeaturesModel;
	}

	public void setProductFeaturesModel(List<ProductFeatureModel> productFeaturesModel) {
		this.productFeaturesModel = productFeaturesModel;
	}

	public List<CategoryFeatureModel> getCategoryFeaturesModel() {
		return categoryFeaturesModel;
	}

	public void setCategoryFeaturesModel(List<CategoryFeatureModel> categoryFeaturesModel) {
		this.categoryFeaturesModel = categoryFeaturesModel;
	}

}
