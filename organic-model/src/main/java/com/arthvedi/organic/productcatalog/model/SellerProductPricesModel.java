package com.arthvedi.organic.productcatalog.model;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

/**
 * @author arthvedi10
 *
 */
@Component("sellerProductPricesModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SellerProductPricesModel extends AbstractModel {

	private String sellerProductId;
	private String quantity;
	private String sellerPrice;
	private String discount;
	private String offerPrice;
	private String sellingPrice;

	public String getSellerProductId() {
		return sellerProductId;
	}

	public void setSellerProductId(String sellerProductId) {
		this.sellerProductId = sellerProductId;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getSellerPrice() {
		return sellerPrice;
	}

	public void setSellerPrice(String sellerPrice) {
		this.sellerPrice = sellerPrice;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getOfferPrice() {
		return offerPrice;
	}

	public void setOfferPrice(String offerPrice) {
		this.offerPrice = offerPrice;
	}

	public String getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(String sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

}
