package com.arthvedi.organic.productcatalog.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("brandModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BrandModel extends AbstractModel {

	private String name;
	private String status;
	private String statusMessage;

	private List<ProductModel> productsModel = new ArrayList<>(0);
	private List<BrandImagesModel> brandImagesModels = new ArrayList<>(0);

	
	public List<BrandImagesModel> getBrandImagesModels() {
		return brandImagesModels;
	}

	public void setBrandImagesModels(List<BrandImagesModel> brandImagesModels) {
		this.brandImagesModels = brandImagesModels;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<ProductModel> getProductsModel() {
		return productsModel;
	}

	public void setProductsModel(List<ProductModel> productsModel) {
		this.productsModel = productsModel;
	}

}
