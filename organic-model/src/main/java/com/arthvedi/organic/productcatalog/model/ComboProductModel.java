package com.arthvedi.organic.productcatalog.model;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("comboProductModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ComboProductModel extends AbstractModel {

	private String productByParentProductId;
	private String productByChildProductId;
	private String status;
	private String price;
	private String units;
	private String statusMessage;

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getProductByParentProductId() {
		return productByParentProductId;
	}

	public void setProductByParentProductId(String productByParentProductId) {
		this.productByParentProductId = productByParentProductId;
	}

	public String getProductByChildProductId() {
		return productByChildProductId;
	}

	public void setProductByChildProductId(String productByChildProductId) {
		this.productByChildProductId = productByChildProductId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getUnits() {
		return units;
	}

	public void setUnits(String units) {
		this.units = units;
	}

}
