package com.arthvedi.organic.productcatalog.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("categoryModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CategoryModel extends AbstractModel {
	private String categoryId;
	private String categoryName;
	private String description;
	private String status;
	private String level;
	private String statusMessage;

	private List<CategoryModel> categoriesModel = new ArrayList<CategoryModel>(
			0);
	private List<ProductCategoryModel> productCategoriesModel = new ArrayList<ProductCategoryModel>(
			0);
	private List<CategoryFeatureModel> categoryFeaturesModel = new ArrayList<CategoryFeatureModel>(
			0);
	private List<CategoryNamesModel> categoryNamesesModel = new ArrayList<CategoryNamesModel>(
			0);
	private List<CategoryImagesModel> categoryImagesesModel = new ArrayList<CategoryImagesModel>(
			0);

	public List<CategoryImagesModel> getCategoryImagesesModel() {
		return categoryImagesesModel;
	}

	public void setCategoryImagesesModel(
			List<CategoryImagesModel> categoryImagesesModel) {
		this.categoryImagesesModel = categoryImagesesModel;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public List<CategoryModel> getCategoriesModel() {
		return categoriesModel;
	}

	public void setCategoriesModel(List<CategoryModel> categoriesModel) {
		this.categoriesModel = categoriesModel;
	}

	public List<ProductCategoryModel> getProductCategoriesModel() {
		return productCategoriesModel;
	}

	public void setProductCategoriesModel(
			List<ProductCategoryModel> productCategoriesModel) {
		this.productCategoriesModel = productCategoriesModel;
	}

	public List<CategoryFeatureModel> getCategoryFeaturesModel() {
		return categoryFeaturesModel;
	}

	public void setCategoryFeaturesModel(
			List<CategoryFeatureModel> categoryFeaturesModel) {
		this.categoryFeaturesModel = categoryFeaturesModel;
	}

	public List<CategoryNamesModel> getCategoryNamesesModel() {
		return categoryNamesesModel;
	}

	public void setCategoryNamesesModel(
			List<CategoryNamesModel> categoryNamesesModel) {
		this.categoryNamesesModel = categoryNamesesModel;
	}

}
