package com.arthvedi.organic.productcatalog.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("seoModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SeoModel extends AbstractModel {

	private String seoTitle;
	private String seoKeyword;
	private String seoDescription;

	private List<ProductNamesModel> productNamesesModel = new ArrayList<ProductNamesModel>(
			0);
	private List<CategoryNamesModel> categoryNamesesModel = new ArrayList<CategoryNamesModel>(
			0);

	public String getSeoKeyword() {
		return seoKeyword;
	}

	public void setSeoKeyword(String seoKeyword) {
		this.seoKeyword = seoKeyword;
	}

	public String getSeoTitle() {

		return seoTitle;
	}

	public void setSeoTitle(String seoTitle) {
		this.seoTitle = seoTitle;
	}

	public String getSeoDescription() {
		return seoDescription;
	}

	public void setSeoDescription(String seoDescription) {
		this.seoDescription = seoDescription;
	}

	public List<ProductNamesModel> getProductNamesesModel() {
		return productNamesesModel;
	}

	public void setProductNamesesModel(
			List<ProductNamesModel> productNamesesModel) {
		this.productNamesesModel = productNamesesModel;
	}

	public List<CategoryNamesModel> getCategoryNamesesModel() {
		return categoryNamesesModel;
	}

	public void setCategoryNamesesModel(
			List<CategoryNamesModel> categoryNamesesModel) {
		this.categoryNamesesModel = categoryNamesesModel;
	}

}
