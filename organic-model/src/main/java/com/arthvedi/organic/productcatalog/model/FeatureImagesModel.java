package com.arthvedi.organic.productcatalog.model;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("featureImagesModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class FeatureImagesModel extends AbstractModel {

}
