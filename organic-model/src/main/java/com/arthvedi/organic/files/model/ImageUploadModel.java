package com.arthvedi.organic.files.model;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;

@Component("imageUploadModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ImageUploadModel extends AbstractModel {

	private String imageName;
	private String imageUrl;
	private String imageEntityType;
	private String imageType;
	private String imageFolder;

	public String getImageFolder() {
		return imageFolder;
	}

	public void setImageFolder(String imageFolder) {
		this.imageFolder = imageFolder;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getImageEntityType() {
		return imageEntityType;
	}

	public void setImageEntityType(String imageEntityType) {
		this.imageEntityType = imageEntityType;
	}

	public String getImageType() {
		return imageType;
	}

	public void setImageType(String imageType) {
		this.imageType = imageType;
	}
}
