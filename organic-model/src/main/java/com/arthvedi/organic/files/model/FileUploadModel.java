package com.arthvedi.organic.files.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.arthvedi.core.model.AbstractModel;

@Component("fileUploadModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class FileUploadModel extends AbstractModel {

	private String fileName;
	private String uploadStatus;
	private MultipartFile multipartFile;
	private String entityType;

	private List<ImageUploadModel> imageModels= new ArrayList<ImageUploadModel>();
	
	public String getEntityType() {
		return entityType;
	}
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getUploadStatus() {
		return uploadStatus;
	}
	public void setUploadStatus(String uploadStatus) {
		this.uploadStatus = uploadStatus;
	}
	public List<ImageUploadModel> getImageModels() {
		return imageModels;
	}
	public void setImageModels(List<ImageUploadModel> imageModels) {
		this.imageModels = imageModels;
	}
	public MultipartFile getMultipartFile() {
		return multipartFile;
	}
	public void setMultipartFile(MultipartFile multipartFile) {
		this.multipartFile = multipartFile;
	} 
	
}
