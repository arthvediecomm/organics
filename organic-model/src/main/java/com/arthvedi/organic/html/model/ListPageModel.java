/**
 *
 */
package com.arthvedi.organic.html.model;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;
import com.arthvedi.organic.productcatalog.model.BrandModel;
import com.arthvedi.organic.productcatalog.model.CategoryModel;
import com.arthvedi.organic.productcatalog.model.FeatureModel;
import com.arthvedi.organic.productcatalog.model.ProductModel;
import com.arthvedi.organic.seller.model.SellerBranchModel;

/**
 * @author venky1
 *
 */
@Component("listPageModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ListPageModel extends AbstractModel {
    private CategoryModel categoryModel;
    private List<ProductModel> productModels;
    private List<SellerBranchModel> sellerBranchModels;
    private List<BrandModel> brandModels;
    private List<CategoryModel> subCategoryModels;
    private Map<String, List<FeatureModel>> featureModels;

    /**
     * @return the brandModels
     */
    public List<BrandModel> getBrandModels() {
        return brandModels;
    }

    /**
     * @return the categoryModel
     */
    public CategoryModel getCategoryModel() {
        return categoryModel;
    }

    /**
     * @return the featureModels
     */
    public Map<String, List<FeatureModel>> getFeatureModels() {
        return featureModels;
    }

    /**
     * @return the productModels
     */
    public List<ProductModel> getProductModels() {
        return productModels;
    }

    /**
     * @return the sellerBranchModels
     */
    public List<SellerBranchModel> getSellerBranchModels() {
        return sellerBranchModels;
    }

    /**
     * @return the subCategoryModels
     */
    public List<CategoryModel> getSubCategoryModels() {
        return subCategoryModels;
    }

    /**
     * @param brandModels
     *            the brandModels to set
     */
    public void setBrandModels(final List<BrandModel> brandModels) {
        this.brandModels = brandModels;
    }

    /**
     * @param categoryModel
     *            the categoryModel to set
     */
    public void setCategoryModel(final CategoryModel categoryModel) {
        this.categoryModel = categoryModel;
    }

    /**
     * @param featureModels
     *            the featureModels to set
     */
    public void setFeatureModels(final Map<String, List<FeatureModel>> featureModels) {
        this.featureModels = featureModels;
    }

    /**
     * @param productModels
     *            the productModels to set
     */
    public void setProductModels(final List<ProductModel> productModels) {
        this.productModels = productModels;
    }

    /**
     * @param sellerBranchModels
     *            the sellerBranchModels to set
     */
    public void setSellerBranchModels(final List<SellerBranchModel> sellerBranchModels) {
        this.sellerBranchModels = sellerBranchModels;
    }

    /**
     * @param subCategoryModels
     *            the subCategoryModels to set
     */
    public void setSubCategoryModels(final List<CategoryModel> subCategoryModels) {
        this.subCategoryModels = subCategoryModels;
    }

}
