/**
 *
 */
package com.arthvedi.organic.html.model;

import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;
import com.arthvedi.organic.productcatalog.model.BrandModel;
import com.arthvedi.organic.productcatalog.model.FeatureModel;
import com.arthvedi.organic.productcatalog.model.ProductModel;
import com.arthvedi.organic.productcatalog.model.SellerProductModel;

/**
 * @author venky1
 *
 */
@Component("singleProductPageModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SingleProductPageModel extends AbstractModel {

    private ProductModel productModel;
    private List<ProductModel> relatedProducts;
    private List<SellerProductModel> sellerProductModels;
    private List<FeatureModel> featureModels;
    private BrandModel brandModels;

    /**
     * @return the brandModels
     */

    /**
     * @return the brandModels
     */
    public BrandModel getBrandModels() {
        return brandModels;
    }

    /**
     * @return the featureModels
     */
    public List<FeatureModel> getFeatureModels() {
        return featureModels;
    }

    /**
     * @return the productModel
     */
    public ProductModel getProductModel() {
        return productModel;
    }

    /**
     * @return the relatedProducts
     */
    public List<ProductModel> getRelatedProducts() {
        return relatedProducts;
    }

    /**
     * @return the sellerProductModels
     */
    public List<SellerProductModel> getSellerProductModels() {
        return sellerProductModels;
    }

    /**
     * @param brandModels
     *            the brandModels to set
     */
    public void setBrandModels(final BrandModel brandModels) {
        this.brandModels = brandModels;
    }

    /**
     * @param featureModels
     *            the featureModels to set
     */
    public void setFeatureModels(final List<FeatureModel> featureModels) {
        this.featureModels = featureModels;
    }

    /**
     * @param productModel
     *            the productModel to set
     */
    public void setProductModel(final ProductModel productModel) {
        this.productModel = productModel;
    }

    /**
     * @param relatedProducts
     *            the relatedProducts to set
     */
    public void setRelatedProducts(final List<ProductModel> relatedProducts) {
        this.relatedProducts = relatedProducts;
    }

    /**
     * @param sellerProductModels
     *            the sellerProductModels to set
     */
    public void setSellerProductModels(final List<SellerProductModel> sellerProductModels) {
        this.sellerProductModels = sellerProductModels;
    }

}
