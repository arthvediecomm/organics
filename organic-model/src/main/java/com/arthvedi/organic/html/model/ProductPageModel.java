package com.arthvedi.organic.html.model;

import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.model.AbstractModel;
import com.arthvedi.organic.productcatalog.model.ProductModel;

@Component("productPageModel")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ProductPageModel extends AbstractModel{

	private ProductModel productModel;
	private List<ProductModel> relatedProducts;
	
	public ProductModel getProductModel() {
		return productModel;
	}
	public void setProductModel(ProductModel productModel) {
		this.productModel = productModel;
	}
	public List<ProductModel> getRelatedProducts() {
		return relatedProducts;
	}
	public void setRelatedProducts(List<ProductModel> relatedProducts) {
		this.relatedProducts = relatedProducts;
	}
	
}
