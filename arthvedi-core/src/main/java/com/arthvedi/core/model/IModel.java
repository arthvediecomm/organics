package com.arthvedi.core.model;

import java.io.Serializable;

import org.joda.time.LocalDateTime;

/**
 * @author varma
 *
 */
public interface IModel extends Serializable {

	/**
	 * @return
	 */
	String getId();

	/**
	 * @param id
	 */
	void setId(String id);

	public LocalDateTime getCreatedDate();

	public void setCreatedDate(LocalDateTime createdDate);

	public LocalDateTime getModifiedDate();

	public void setModifiedDate(LocalDateTime modifiedDate);

	public String getUserCreated();

	public void setUserCreated(String userCreated);

	public String getUserModified();

	public void setUserModified(String userModified);
}
