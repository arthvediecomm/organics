package com.arthvedi.core.model;

import org.joda.time.LocalDateTime;

/**
 * @author Jay
 *
 */
public class AbstractModel implements IModel {

	protected String id;
	protected String userCreated;
	protected String userModified;
	protected LocalDateTime createdDate;
	protected LocalDateTime modifiedDate;

	/**
	 * {@inheritDoc}
	 *
	 * @see com.meat.businessdelegate.domain.IModel#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see com.meat.businessdelegate.domain.IModel#setId(java.lang.String)
	 */
	@Override
	public void setId(final String id) {
		this.id = id;

	}

	public String getUserCreated() {
		return userCreated;
	}

	public void setUserCreated(String userCreated) {
		this.userCreated = userCreated;
	}

	public String getUserModified() {
		return userModified;
	}

	public void setUserModified(String userModified) {
		this.userModified = userModified;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public LocalDateTime getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(LocalDateTime modifiedDate) {
		this.modifiedDate = modifiedDate;
	}


}
