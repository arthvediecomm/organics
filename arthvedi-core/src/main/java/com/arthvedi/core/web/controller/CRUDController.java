package com.arthvedi.core.web.controller;

import java.io.Serializable;
import java.util.Collection;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.model.IModel;
import com.arthvedi.core.model.IModelWrapper;

/**
 * @author srikanthkanumuri
 *
 * @param <T>
 * @param <U>
 * @param <V>
 * @param <W>
 */
public abstract class CRUDController<T extends IModel, U extends IBusinessDelegateContext, V extends IKeyBuilder<W>, W extends Serializable> {

	private IBusinessDelegate<T, U, V, W> businessDelegate;

	/**
	 * @param businessDelegate
	 */
	public abstract void setBusinessDelegate(IBusinessDelegate<T, U, V, W> businessDelegate);

	/**
	 * @param id
	 * @param model
	 * @return
	 */
	public abstract ResponseEntity<T> edit(final W id, final T model);

	/**
	 * @return
	 */
	public abstract ResponseEntity<IModelWrapper<Collection<T>>> getAll();

	/**
	 * @param id
	 * @return
	 */
	public abstract ResponseEntity<T> getById(final W id);

	/**
	 * @param model
	 * @return
	 */
	public ResponseEntity<T> create(final T model) {
		businessDelegate.create(model);
		return new ResponseEntity<T>(model, HttpStatus.CREATED);
	}

}
