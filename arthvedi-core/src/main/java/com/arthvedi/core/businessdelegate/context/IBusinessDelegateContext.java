/**
 *
 */
package com.arthvedi.core.businessdelegate.context;

import java.io.Serializable;

/**
 * @author arthvedi1
 *
 */
public interface IBusinessDelegateContext extends Serializable {

}
