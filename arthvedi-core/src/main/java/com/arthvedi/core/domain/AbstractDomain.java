package com.arthvedi.core.domain;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

@MappedSuperclass
public abstract class AbstractDomain implements IDomain {

	protected String id;
	protected String userCreated;
	protected String userModified;
	protected LocalDateTime createdDate;
	protected LocalDateTime modifiedDate;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.waterapp.domain.IDomain#getId()
	 */
	@Override
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid")
	@Column(name = "id", unique = true, nullable = false, length = 100)
	public String getId() {
		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.waterapp.domain.IDomain#setId(java.lang.String)
	 */
	@Override
	public void setId(final String id) {
		this.id = id;
	}

	@Column(name = "user_created")
	public String getUserCreated() {
		return userCreated;
	}
	@Override
	public void setUserCreated(String userCreated) {
		this.userCreated = userCreated;
	}

	@Column(name = "user_modified")
	public String getUserModified() {
		return userModified;
	}
	@Override
	public void setUserModified(String userModified) {
		this.userModified = userModified;
	}

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "created_date", nullable = false, length = 19)
	public LocalDateTime getCreatedDate() {
		return createdDate;
	}
	@Override
	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "modified_date", nullable = false, length = 19)
	public LocalDateTime getModifiedDate() {
		return modifiedDate;
	}
	@Override
	public void setModifiedDate(LocalDateTime modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

}
