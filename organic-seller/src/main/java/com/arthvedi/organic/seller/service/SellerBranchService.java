package com.arthvedi.organic.seller.service;

import static com.arthvedi.organic.enums.Status.ACTIVE;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.seller.domain.SellerBranch;
import com.arthvedi.organic.seller.domain.SellerBranchAddress;
import com.arthvedi.organic.seller.domain.SellerBranchBankAccount;
import com.arthvedi.organic.seller.domain.SellerBranchCharges;
import com.arthvedi.organic.seller.domain.SellerBranchImages;
import com.arthvedi.organic.seller.domain.SellerBranchZone;
import com.arthvedi.organic.seller.repository.SellerBranchRepository;

@Component
public class SellerBranchService implements ISellerBranchService {
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private SellerBranchRepository sellerBranchRepository;
	@Autowired
	private ISellerBranchAddressService sellerBranchAddresService;
	@Autowired
	private ISellerBranchBankAccountService sellerBranchBankAccountService;
	@Autowired
	private ISellerBranchChargesService sellerBranchChargesService;
	@Autowired
	private ISellerBranchZoneService sellerBranchZoneService;
	@Autowired
	private ISellerBranchImagesService sellerBranchImagesService;

	@Override
	@Transactional
	public SellerBranch create(SellerBranch sellerBranch) {
		sellerBranch.setUserCreated("varma");
		sellerBranch.setCreatedDate(new LocalDateTime());
		sellerBranch.setBranchStatus(ACTIVE.name());
		sellerBranch = sellerBranchRepository.save(sellerBranch);
		if (sellerBranch.getId() != null
				&& CollectionUtils.isNotEmpty(sellerBranch
						.getSellerBranchAddresses())) {
			sellerBranch.setSellerBranchAddresses(addSellerBranchAddress(
					sellerBranch, sellerBranch.getSellerBranchAddresses()));

		}
		if (sellerBranch.getId() != null
				&& CollectionUtils.isNotEmpty(sellerBranch
						.getSellerBranchBankAccounts())) {
			sellerBranch
					.setSellerBranchBankAccounts(addingSellerBranchBankAccount(
							sellerBranch,
							sellerBranch.getSellerBranchBankAccounts()));

		}
		if (sellerBranch.getId() != null
				&& CollectionUtils.isNotEmpty(sellerBranch
						.getSellerBranchImageses())) {
			sellerBranch.setSellerBranchImageses(addSellerBranchImages(
					sellerBranch, sellerBranch.getSellerBranchImageses()));
		}
		if (sellerBranch.getId() != null
				&& CollectionUtils.isNotEmpty(sellerBranch
						.getSellerBranchZones())) {
			sellerBranch.setSellerBranchZones(addSellerBranchZone(sellerBranch,
					sellerBranch.getSellerBranchZones()));
		}
		if (sellerBranch.getId() != null
				&& CollectionUtils.isNotEmpty(sellerBranch
						.getSellerBranchChargeses())) {
			sellerBranch.setSellerBranchChargeses(addSellerBranchCharge(
					sellerBranch, sellerBranch.getSellerBranchChargeses()));
		}
		return sellerBranch;
	}
@Transactional
	private Set<SellerBranchCharges> addSellerBranchCharge(
			SellerBranch sellerBranch,
			Set<SellerBranchCharges> sellerBranchChargeses) {
		Set<SellerBranchCharges> sellerBranchCharges = new HashSet<SellerBranchCharges>();
		for (SellerBranchCharges sellerBranchChargess : sellerBranchChargeses) {
			SellerBranchCharges sellerBranchcar = sellerBranchChargess;
			sellerBranchcar.setSellerBranch(sellerBranch);
			sellerBranchcar = sellerBranchChargesService
					.create(sellerBranchcar);
			sellerBranchCharges.add(sellerBranchcar);
		}
		return sellerBranchCharges;
	}

	@Transactional
	private Set<SellerBranchZone> addSellerBranchZone(
			SellerBranch sellerBranch, Set<SellerBranchZone> sellerBranchZones) {
		Set<SellerBranchZone> sellerBranchZone = new HashSet<SellerBranchZone>();
		for (SellerBranchZone sellerBranchZon : sellerBranchZones) {
			SellerBranchZone selleBrancZone = sellerBranchZon;
			selleBrancZone.setSellerBranch(sellerBranch);
			selleBrancZone = sellerBranchZoneService.create(selleBrancZone);
			sellerBranchZone.add(selleBrancZone);
		}
		return sellerBranchZone;
	}
@Transactional 
	private Set<SellerBranchImages> addSellerBranchImages(
			SellerBranch sellerBranch,
			Set<SellerBranchImages> sellerBranchImageses) {
		Set<SellerBranchImages> sellerBranchImages = new HashSet<SellerBranchImages>();
		for (SellerBranchImages sellerBranchIma : sellerBranchImageses) {
			SellerBranchImages sellerBranchImage = sellerBranchIma;
			sellerBranchImage.setSellerBranch(sellerBranch);
			sellerBranchImage = sellerBranchImagesService
					.create(sellerBranchImage);
			sellerBranchImages.add(sellerBranchImage);
		}
		return sellerBranchImages;
	}

	private Set<SellerBranchBankAccount> addingSellerBranchBankAccount(
			SellerBranch sellerBranch,
			Set<SellerBranchBankAccount> sellerBranchBankAccounts) {
		Set<SellerBranchBankAccount> sellerBranchBankAccount = new HashSet<SellerBranchBankAccount>();
		for (SellerBranchBankAccount sellerBranchBankAc : sellerBranchBankAccounts) {
			SellerBranchBankAccount sBranchBankAcc = sellerBranchBankAc;
			sBranchBankAcc.setSellerBranch(sellerBranch);
			sBranchBankAcc = sellerBranchBankAccountService
					.create(sBranchBankAcc);
			sellerBranchBankAccount.add(sBranchBankAcc);
		}
		return sellerBranchBankAccount;
	}
@Transactional
	private Set<SellerBranchAddress> addSellerBranchAddress(
			SellerBranch sellerBranch,
			Set<SellerBranchAddress> sellerBranchAddresses) {
		Set<SellerBranchAddress> sellerBranchAddresss = new HashSet<SellerBranchAddress>();
		for (SellerBranchAddress sbAddress : sellerBranchAddresses) {
			SellerBranchAddress sbAddres = sbAddress;
			sbAddres.setSellerBranch(sellerBranch);
			sbAddres = sellerBranchAddresService.create(sbAddres);
			sellerBranchAddresss.add(sbAddres);
		}

		return sellerBranchAddresss;
	}

	@Override
	public void deleteSellerBranch(String sellerBranchId) {

	}

	@Override
	public SellerBranch getSellerBranch(String sellerBranchId) {
		return sellerBranchRepository.findOne(sellerBranchId);
	}

	@Override
	public List<SellerBranch> getAll() {

		return null;
	}

	@Override
	public SellerBranch updateSellerBranch(SellerBranch sellerBranch) {
		SellerBranch sellerBranchs = sellerBranchRepository
				.findOne(sellerBranch.getId());
		try {
			nonNullBeanUtils.copyProperties(sellerBranchs, sellerBranch);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		sellerBranchs.setUserModified("ADMIN");
		sellerBranchs.setModifiedDate(new LocalDateTime());
		sellerBranch = sellerBranchRepository.save(sellerBranchs);
		return sellerBranch;
	}

	@Override
	public boolean getSellerBranchByBranchName(String branchName) {
		return sellerBranchRepository.findByBranchName(branchName) != null;
	}

	@Override
	public List<SellerBranch> getSellerBranchList() {
		List<SellerBranch> sellerBranchs = (List<SellerBranch>) sellerBranchRepository
				.findAll();
		List<SellerBranch> sellerBranch = new ArrayList<SellerBranch>();
		for (SellerBranch sellerBran : sellerBranchs) {
			SellerBranch selBran = new SellerBranch();
			selBran.setLandlineNo(sellerBran.getLandlineNo());
			selBran.setMinimumOrderAmount(sellerBran.getMinimumOrderAmount());
			selBran.setSeller(sellerBran.getSeller());
			sellerBranch.add(selBran);
		}
		return sellerBranch;

	}

}
