package com.arthvedi.organic.seller.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;

/*
 *@Author varma
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SellerBranchBankAccountContext implements IBusinessDelegateContext {

	private String sellerBranchBankAccountId;
	private String all;
	private String sellerBranchId;

	public String getSellerBranchId() {
		return sellerBranchId;
	}

	public void setSellerBranchId(String sellerBranchId) {
		this.sellerBranchId = sellerBranchId;
	}

	public String getSellerBranchBankAccountId() {
		return sellerBranchBankAccountId;
	}

	public void setSellerBranchBankAccountId(String sellerBranchBankAccountId) {
		this.sellerBranchBankAccountId = sellerBranchBankAccountId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

}
