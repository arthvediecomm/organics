package com.arthvedi.organic.seller.model.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.organic.seller.domain.Seller;
import com.arthvedi.organic.seller.model.SellerBranchModel;
import com.arthvedi.organic.seller.model.SellerModel;

@Component("sellerToSellerModelConverter")
public class SellerToSellerModelConverter implements
		Converter<Seller, SellerModel> {
	@Autowired
	private ObjectFactory<SellerModel> sellerModelFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerModel convert(final Seller source) {
		SellerModel sellerModel = sellerModelFactory.getObject();
		BeanUtils.copyProperties(source, sellerModel);
		if (CollectionUtils.isNotEmpty(source.getSellerBranches())) {
			List<SellerBranchModel> converted = (List<SellerBranchModel>) conversionService
					.convert(source.getSellerBranches(), TypeDescriptor
							.forObject(source.getSellerBranches()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									SellerBranchModel.class));

			sellerModel.getSellerBranchesModel().addAll(converted);
		}
		return sellerModel;
	}

	@Autowired
	public void setSellerModelFactory(
			final ObjectFactory<SellerModel> sellerModelFactory) {
		this.sellerModelFactory = sellerModelFactory;
	}
}
