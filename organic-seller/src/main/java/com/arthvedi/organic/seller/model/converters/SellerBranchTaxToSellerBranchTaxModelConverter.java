package com.arthvedi.organic.seller.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.seller.domain.SellerBranchTax;
import com.arthvedi.organic.seller.model.SellerBranchTaxModel;

@Component("sellerBranchTaxToSellerBranchTaxModelConverter")
public class SellerBranchTaxToSellerBranchTaxModelConverter
		implements Converter<SellerBranchTax, SellerBranchTaxModel> {
	@Autowired
	private ObjectFactory<SellerBranchTaxModel> sellerBranchTaxModelFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerBranchTaxModel convert(final SellerBranchTax source) {
		SellerBranchTaxModel sellerBranchTaxModel = sellerBranchTaxModelFactory.getObject();
		BeanUtils.copyProperties(source, sellerBranchTaxModel);
		if(source.getTax()!=null){
		sellerBranchTaxModel.setTaxId(source.getTax().getId());
		sellerBranchTaxModel.setTaxName(source.getTax().getTaxName());
		}
		if(source.getSellerBranch()!=null){
			sellerBranchTaxModel.setSellerBranchId(source.getSellerBranch().getId());
		}
		return sellerBranchTaxModel;
	}

	@Autowired
	public void setSellerBranchTaxModelFactory(final ObjectFactory<SellerBranchTaxModel> sellerBranchTaxModelFactory) {
		this.sellerBranchTaxModelFactory = sellerBranchTaxModelFactory;
	}
}
