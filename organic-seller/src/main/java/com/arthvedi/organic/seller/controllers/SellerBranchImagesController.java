package com.arthvedi.organic.seller.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.seller.businessdelegate.context.SellerBranchImagesContext;
import com.arthvedi.organic.seller.model.SellerBranchImagesModel;
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/sellerbranchimages", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class SellerBranchImagesController {

	private IBusinessDelegate<SellerBranchImagesModel, SellerBranchImagesContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<SellerBranchImagesContext> sellerBranchImagesContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create")
	
	public ResponseEntity<SellerBranchImagesModel> createSellerBranchImages(
			@RequestBody final SellerBranchImagesModel sellerBranchImagesModel) {
		businessDelegate.create(sellerBranchImagesModel);
		return new ResponseEntity<SellerBranchImagesModel>(sellerBranchImagesModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)

	public ResponseEntity<SellerBranchImagesModel> edit(@PathVariable(value = "id") final String sellerBranchImagesId,
			@RequestBody final SellerBranchImagesModel sellerBranchImagesModel) {

		businessDelegate.edit(keyBuilderFactory.getObject().withId(sellerBranchImagesId), sellerBranchImagesModel);
		return new ResponseEntity<SellerBranchImagesModel>(sellerBranchImagesModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all")
	
	public ResponseEntity<IModelWrapper<Collection<SellerBranchImagesModel>>> getAll() {
		SellerBranchImagesContext sellerBranchImagesContext = sellerBranchImagesContextFactory.getObject();
		sellerBranchImagesContext.setAll("all");
		Collection<SellerBranchImagesModel> sellerBranchImagesModels = businessDelegate
				.getCollection(sellerBranchImagesContext);
		IModelWrapper<Collection<SellerBranchImagesModel>> models = new CollectionModelWrapper<SellerBranchImagesModel>(
				SellerBranchImagesModel.class, sellerBranchImagesModels);
		return new ResponseEntity<IModelWrapper<Collection<SellerBranchImagesModel>>>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	
	public ResponseEntity<SellerBranchImagesModel> getSellerBranchImages(
			@PathVariable(value = "id") final String sellerBranchImagesId) {
		SellerBranchImagesContext sellerBranchImagesContext = sellerBranchImagesContextFactory.getObject();

		SellerBranchImagesModel model = businessDelegate
				.getByKey(keyBuilderFactory.getObject().withId(sellerBranchImagesId), sellerBranchImagesContext);
		return new ResponseEntity<SellerBranchImagesModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "sellerBranchImagesBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<SellerBranchImagesModel, SellerBranchImagesContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setSellerBranchImagesObjectFactory(
			final ObjectFactory<SellerBranchImagesContext> sellerBranchImagesContextFactory) {
		this.sellerBranchImagesContextFactory = sellerBranchImagesContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
