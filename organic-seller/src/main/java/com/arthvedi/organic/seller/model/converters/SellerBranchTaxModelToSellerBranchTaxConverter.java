/**
 *
 */
package com.arthvedi.organic.seller.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.commons.domain.Tax;
import com.arthvedi.organic.seller.domain.SellerBranch;
import com.arthvedi.organic.seller.domain.SellerBranchTax;
import com.arthvedi.organic.seller.model.SellerBranchTaxModel;

/**
 * @author Jay
 *
 */
@Component("sellerBranchTaxModelToSellerBranchTaxConverter")
public class SellerBranchTaxModelToSellerBranchTaxConverter
		implements Converter<SellerBranchTaxModel, SellerBranchTax> {
	@Autowired
	private ObjectFactory<SellerBranchTax> sellerBranchTaxFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerBranchTax convert(final SellerBranchTaxModel source) {
		SellerBranchTax sellerBranchTax = sellerBranchTaxFactory.getObject();
		BeanUtils.copyProperties(source, sellerBranchTax);
		if(source.getSellerBranchId()!=null){
			SellerBranch slrB = new SellerBranch();
			slrB.setId(source.getSellerBranchId());
			sellerBranchTax.setSellerBranch(slrB);
		}
		if(source.getTaxId()!=null){
			Tax tax = new Tax();
			tax.setId(source.getTaxId());
			sellerBranchTax.setTax(tax);
		}
		return sellerBranchTax;
	}

	@Autowired
	public void setSellerBranchTaxFactory(final ObjectFactory<SellerBranchTax> sellerBranchTaxFactory) {
		this.sellerBranchTaxFactory = sellerBranchTaxFactory;
	}

}
