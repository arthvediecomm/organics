/**
 *
 */
package com.arthvedi.organic.seller.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.seller.model.SellerBranchBankAccountModel;
import com.arthvedi.organic.seller.representation.siren.SellerBranchBankAccountRepresentation;

/**
 * @author varma
 *
 */
@Component("sellerBranchBankAccountModelToSellerBranchBankAccountRepresentationConverter")
public class SellerBranchBankAccountModelToSellerBranchBankAccountRepresentationConverter extends PropertyCopyingConverter<SellerBranchBankAccountModel, SellerBranchBankAccountRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public SellerBranchBankAccountRepresentation convert(final SellerBranchBankAccountModel source) {

        SellerBranchBankAccountRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<SellerBranchBankAccountRepresentation> factory) {
        super.setFactory(factory);
    }

}
