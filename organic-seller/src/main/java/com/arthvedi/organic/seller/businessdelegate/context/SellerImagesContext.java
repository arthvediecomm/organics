package com.arthvedi.organic.seller.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;

/*
 *@Author varma
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SellerImagesContext implements IBusinessDelegateContext {

	private String all;
	private String sellerImagesId;

	public String getAll() {
		return all;
	}

	public String getSellerImagesId() {
		return sellerImagesId;
	}

	public void setAll(final String all) {
		this.all = all;
	}

	public void setSellerImagesId(final String sellerImagesId) {
		this.sellerImagesId = sellerImagesId;
	}

}
