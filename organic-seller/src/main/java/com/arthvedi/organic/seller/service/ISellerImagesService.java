package com.arthvedi.organic.seller.service;

import com.arthvedi.organic.seller.domain.SellerImages;

/*
 *@Author varma
 */
public interface ISellerImagesService {

	SellerImages create(SellerImages sellerImages);

	SellerImages getSellerImages(String sellerImagesId);

	SellerImages updateSellerImages(SellerImages sellerImages);
}
