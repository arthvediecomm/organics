package com.arthvedi.organic.seller.service;

import java.util.List;

import com.arthvedi.organic.seller.domain.SellerBranchUser;

/*
 *@Author varma
 */
public interface ISellerBranchUserService {

	SellerBranchUser create(SellerBranchUser sellerBranchUser);

	void deleteSellerBranchUser(String sellerBranchUserId);

	SellerBranchUser getSellerBranchUser(String sellerBranchUserId);

	List<SellerBranchUser> getAll();

	SellerBranchUser updateSellerBranchUser(SellerBranchUser sellerBranchUser);

	boolean getSellerBranchUserByEmailId(String emailId);

	boolean getSellerBranchUserByPhoneNo(String phoneNo);

	List<SellerBranchUser> getSellerBranchUserList();

	List<SellerBranchUser> getSellerBranchUserBySellerBranch(
			String sellerBranchId);
}
