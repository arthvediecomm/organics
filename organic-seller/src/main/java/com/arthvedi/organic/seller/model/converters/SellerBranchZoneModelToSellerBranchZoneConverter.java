/**
 *
 */
package com.arthvedi.organic.seller.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.commons.domain.Zone;
import com.arthvedi.organic.seller.domain.SellerBranch;
import com.arthvedi.organic.seller.domain.SellerBranchZone;
import com.arthvedi.organic.seller.model.SellerBranchZoneModel;

/**
 * @author Jay
 *
 */
@Component("sellerBranchZoneModelToSellerBranchZoneConverter")
public class SellerBranchZoneModelToSellerBranchZoneConverter
		implements Converter<SellerBranchZoneModel, SellerBranchZone> {
	@Autowired
	private ObjectFactory<SellerBranchZone> sellerBranchZoneFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerBranchZone convert(final SellerBranchZoneModel source) {
		SellerBranchZone sellerBranchZone = new SellerBranchZone();
		BeanUtils.copyProperties(source, sellerBranchZone);
		if (source.getSellerBranchId() != null) {
			SellerBranch sb = new SellerBranch();
			sb.setId(source.getSellerBranchId());
			sellerBranchZone.setSellerBranch(sb);
		}
		if (source.getZoneId() != null) {
			Zone zone = new Zone();
			zone.setId(source.getZoneId());
			sellerBranchZone.setZone(zone);
		}
		return sellerBranchZone;
	}

	@Autowired
	public void setSellerBranchZoneFactory(final ObjectFactory<SellerBranchZone> sellerBranchZoneFactory) {
		this.sellerBranchZoneFactory = sellerBranchZoneFactory;
	}

}
