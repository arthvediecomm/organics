package com.arthvedi.organic.seller.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.seller.businessdelegate.context.SellerContext;
import com.arthvedi.organic.seller.model.SellerModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/seller", produces = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class SellerController {

	private IBusinessDelegate<SellerModel, SellerContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<SellerContext> sellerContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<SellerModel> createSeller(
			@RequestBody SellerModel sellerModel) {
		sellerModel = businessDelegate.create(sellerModel);
		return new ResponseEntity<SellerModel>(sellerModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	public ResponseEntity<SellerModel> edit(
			@PathVariable(value = "id") final String sellerId,
			@RequestBody SellerModel sellerModel) {

		sellerModel = businessDelegate.edit(keyBuilderFactory.getObject()
				.withId(sellerId), sellerModel);
		return new ResponseEntity<SellerModel>(sellerModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<IModelWrapper<Collection<SellerModel>>> getAll() {
		SellerContext sellerContext = sellerContextFactory.getObject();
		sellerContext.setAll("all");
		Collection<SellerModel> sellerModels = businessDelegate
				.getCollection(sellerContext);
		IModelWrapper<Collection<SellerModel>> models = new CollectionModelWrapper<SellerModel>(
				SellerModel.class, sellerModels);
		return new ResponseEntity<IModelWrapper<Collection<SellerModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/sellerlist", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<IModelWrapper<Collection<SellerModel>>> getSellerList() {
		SellerContext sellerContext = sellerContextFactory.getObject();
		sellerContext.setSellerList("sellerList");
		Collection<SellerModel> sellerModels = businessDelegate
				.getCollection(sellerContext);
		IModelWrapper<Collection<SellerModel>> models = new CollectionModelWrapper<SellerModel>(
				SellerModel.class, sellerModels);
		return new ResponseEntity<IModelWrapper<Collection<SellerModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<SellerModel> getSeller(
			@PathVariable(value = "id") final String sellerId) {
		SellerContext sellerContext = sellerContextFactory.getObject();

		SellerModel model = businessDelegate.getByKey(keyBuilderFactory
				.getObject().withId(sellerId), sellerContext);
		return new ResponseEntity<SellerModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "sellerBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<SellerModel, SellerContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setSellerObjectFactory(
			final ObjectFactory<SellerContext> sellerContextFactory) {
		this.sellerContextFactory = sellerContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(
			final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
