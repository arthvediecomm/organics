package com.arthvedi.organic.seller.service;

import static com.arthvedi.organic.enums.Status.ACTIVE;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.seller.domain.Seller;
import com.arthvedi.organic.seller.domain.SellerBranch;
import com.arthvedi.organic.seller.domain.SellerImages;
import com.arthvedi.organic.seller.repository.SellerRepository;

@Component
public class SellerService implements ISellerService {
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private SellerRepository sellerRepository;
	@Autowired
	ISellerBranchService sellerBranchService;
	@Autowired
	ISellerImagesService sellerImgesService;

	@Override
	@Transactional
	public Seller create(Seller seller) {
		seller.setCreatedDate(new LocalDateTime());
		seller.setUserCreated("varma");
		seller.setStatus(ACTIVE.name());
		seller = sellerRepository.save(seller);
		if (seller.getId() != null
				&& CollectionUtils.isNotEmpty(seller.getSellerImageses())) {
			seller.setSellerImageses(addSellerImages(seller,
					seller.getSellerImageses()));
		}
		if (seller.getId() != null
				&& CollectionUtils.isNotEmpty(seller.getSellerBranches())) {
			Set<SellerBranch> sellerBranch = new HashSet<SellerBranch>();
			for (SellerBranch sellerBranches : seller.getSellerBranches()) {
				SellerBranch sellerBranchs = sellerBranches;
				sellerBranchs.setSeller(seller);
				sellerBranchs = sellerBranchService.create(sellerBranchs);
				sellerBranch.add(sellerBranchs);
			}

		}
		return seller;

	}

	private Set<SellerImages> addSellerImages(Seller seller,
			Set<SellerImages> sellerImageses) {
		Set<SellerImages> sellerImages = new HashSet<SellerImages>();
		for (SellerImages sellerImage : sellerImageses) {
			SellerImages sellerImag = sellerImage;
			sellerImag.setSeller(seller);
			sellerImag = sellerImgesService.create(sellerImag);

			sellerImages.add(sellerImag);
		}
		return sellerImages;
	}

	@Override
	public void deleteSeller(String sellerId) {

	}

	@Override
	public Seller getSeller(String sellerId) {
		return sellerRepository.findOne(sellerId);
	}

	@Override
	public List<Seller> getAll() {

		return null;
	}

	@Override
	public List<Seller> getSellerList() {

		List<Seller> sellers = (List<Seller>) sellerRepository.findAll();
		List<Seller> sellerss = new ArrayList<Seller>();
		for (Seller seller : sellers) {
			Seller sel = new Seller();
			sel.setSellerName(seller.getSellerName());
			sel.setSellerType(seller.getSellerType());
			sel.setDescription(seller.getDescription());
			sel.setStatus(seller.getStatus());
			sel.setUserCreated(seller.getUserCreated());
			sellerss.add(sel);
		}
		return sellerss;

	}

	@Override
	public Seller updateSeller(Seller seller) {
		Seller sellers = sellerRepository.findOne(seller.getId());

		try {
			nonNullBeanUtils.copyProperties(sellers, seller);
		} catch (IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}

		sellers.setModifiedDate(new LocalDateTime());
		sellers.setUserModified("admin");
		seller = sellerRepository.save(sellers);
		return seller;
	}

	@Override
	public boolean checkSellerNameExist(String sellerName) {

		return sellerRepository.findBySellerName(sellerName) != null;
	}

}
