package com.arthvedi.organic.seller.service;

import java.util.List;

import com.arthvedi.organic.seller.domain.SellerBranchAddress;
/*
*@Author varma
*/
public interface ISellerBranchAddressService {
	
	SellerBranchAddress create(SellerBranchAddress sellerBranchAddress);

	void deleteSellerBranchAddress(String sellerBranchAddressId);

	SellerBranchAddress getSellerBranchAddress(String sellerBranchAddressId);

	List<SellerBranchAddress> getAll();

	SellerBranchAddress updateSellerBranchAddress(SellerBranchAddress sellerBranchAddress);
}
