package com.arthvedi.organic.seller.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.seller.domain.SellerBranchCharges;
import com.arthvedi.organic.seller.model.SellerBranchChargesModel;

@Component("sellerBranchChargesToSellerBranchChargesModelConverter")
public class SellerBranchChargesToSellerBranchChargesModelConverter
        implements Converter<SellerBranchCharges, SellerBranchChargesModel> {
    @Autowired
    private ObjectFactory<SellerBranchChargesModel> sellerBranchChargesModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public SellerBranchChargesModel convert(final SellerBranchCharges source) {
        SellerBranchChargesModel sellerBranchChargesModel = sellerBranchChargesModelFactory.getObject();
        BeanUtils.copyProperties(source, sellerBranchChargesModel);

        return sellerBranchChargesModel;
    }

    @Autowired
    public void setSellerBranchChargesModelFactory(
            final ObjectFactory<SellerBranchChargesModel> sellerBranchChargesModelFactory) {
        this.sellerBranchChargesModelFactory = sellerBranchChargesModelFactory;
    }
}
