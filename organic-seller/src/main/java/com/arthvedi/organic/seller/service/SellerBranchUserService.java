package com.arthvedi.organic.seller.service;

import static com.arthvedi.organic.seller.specifications.SellerBranchUserSpecifications.sellerUserWithBranch;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.seller.domain.SellerBranchUser;
import com.arthvedi.organic.seller.repository.SellerBranchUserRepository;
import com.arthvedi.organic.user.domain.EcommerceRole;
import com.arthvedi.organic.user.domain.EcommerceUser;
import com.arthvedi.organic.user.service.IEcommerceRoleService;
import com.arthvedi.organic.user.service.IEcommerceUserService;

@Component
public class SellerBranchUserService implements ISellerBranchUserService {

	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;

	@Autowired
	private IEcommerceRoleService ecommerceRoleService;
	@Autowired
	private IEcommerceUserService ucommerceUserService;

	@Autowired
	private SellerBranchUserRepository sellerBranchUserRepository;

	@Override
	@Transactional
	public SellerBranchUser create(SellerBranchUser sellerBranchUser) {
		if (sellerBranchUser.getEcommerceUser() != null) {
			EcommerceUser ecommerceUser = new EcommerceUser();
			ecommerceUser = signupSellerUser(sellerBranchUser
					.getEcommerceUser());
			sellerBranchUser.setEcommerceUser(ecommerceUser);

			if (ecommerceUser != null) {
				EcommerceRole ecommerceRole = new EcommerceRole();
				if (ecommerceUser.getUserRole().equals("SELLER_ADMIN")
						&& ecommerceUser.getUserType().equals("SELLER_USER")) {
					ecommerceRole.setRoleName("SELLER_ADMIN");
					ecommerceRole.setRoleType("SELLER_ADMIN");
				}
				if (ecommerceUser.getUserRole().equals("SELLER_USER")
						&& ecommerceUser.getUserType().equals("SELLER_USER")) {
					ecommerceRole.setRoleName("SELLER_USER");
					ecommerceRole.setRoleType("EMPLOYEE");
				}
				ecommerceRole.setEcommerceUser(ecommerceUser);
				ecommerceRole = ecommerceRoleService.create(ecommerceRole);
			}
		}
		sellerBranchUser.setCreatedDate(new LocalDateTime());
		sellerBranchUser.setUserCreated("Varma");
		return sellerBranchUser;

	}

	@Transactional
	private EcommerceUser signupSellerUser(EcommerceUser ecommerceUser) {
		return ecommerceUser;

	}

	@Override
	public void deleteSellerBranchUser(String sellerBranchUserId) {

	}

	@Override
	public SellerBranchUser getSellerBranchUser(String sellerBranchUserId) {
		return sellerBranchUserRepository.findOne(sellerBranchUserId);
	}

	@Override
	public List<SellerBranchUser> getAll() {
		return null;
	}

	@Override
	public SellerBranchUser updateSellerBranchUser(
			SellerBranchUser sellerBranchUser) {
		SellerBranchUser sellerBranchUsers = sellerBranchUserRepository
				.findOne(sellerBranchUser.getId());
		try {
			nonNullBeanUtils
					.copyProperties(sellerBranchUsers, sellerBranchUser);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		sellerBranchUsers.setUserModified("admin");
		sellerBranchUsers.setModifiedDate(new LocalDateTime());
		sellerBranchUser = sellerBranchUserRepository.save(sellerBranchUsers);

		return sellerBranchUser;
	}

	@Override
	public boolean getSellerBranchUserByEmailId(String emailId) {
		return sellerBranchUserRepository
				.findSellerBranchUserByEmailId(emailId) != null;
	}

	@Override
	public boolean getSellerBranchUserByPhoneNo(String phoneNo) {
		return false;
	}

	@Override
	public List<SellerBranchUser> getSellerBranchUserList() {
		return null;
	}

	@Override
	public List<SellerBranchUser> getSellerBranchUserBySellerBranch(
			String sellerBranchId) {
		return sellerBranchUserRepository
				.findAll(sellerUserWithBranch(sellerBranchId));
	}

}
