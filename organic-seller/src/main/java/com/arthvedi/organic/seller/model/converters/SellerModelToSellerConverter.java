/**
 *
 */
package com.arthvedi.organic.seller.model.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.organic.seller.domain.Seller;
import com.arthvedi.organic.seller.domain.SellerBranch;
import com.arthvedi.organic.seller.domain.SellerImages;
import com.arthvedi.organic.seller.model.SellerModel;

/**
 * @author Jay
 *
 */
@Component("sellerModelToSellerConverter")
public class SellerModelToSellerConverter implements
		Converter<SellerModel, Seller> {
	@Autowired
	private ObjectFactory<Seller> sellerFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public Seller convert(final SellerModel source) {
		Seller seller = sellerFactory.getObject();
		BeanUtils.copyProperties(source, seller);

		if (CollectionUtils.isNotEmpty(source.getSellerBranchesModel())) {
			List<SellerBranch> converted = (List<SellerBranch>) conversionService
					.convert(source.getSellerBranchesModel(), TypeDescriptor
							.forObject(source.getSellerBranchesModel()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									SellerBranch.class));
			seller.getSellerBranches().addAll(converted);
		}
		if (CollectionUtils.isNotEmpty(source.getSellerImagesesModel())) {
			List<SellerImages> convert = (List<SellerImages>) conversionService
					.convert(source.getSellerImagesesModel(), TypeDescriptor
							.forObject(source.getSellerImagesesModel()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									SellerImages.class));
			seller.getSellerImageses().addAll(convert);
		}

		return seller;
	}

	@Autowired
	public void setSellerFactory(final ObjectFactory<Seller> sellerFactory) {
		this.sellerFactory = sellerFactory;
	}

}
