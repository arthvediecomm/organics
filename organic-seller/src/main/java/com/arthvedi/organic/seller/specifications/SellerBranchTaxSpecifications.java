package com.arthvedi.organic.seller.specifications;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.arthvedi.organic.seller.domain.SellerBranchTax;

public class SellerBranchTaxSpecifications {
	public static Specification<SellerBranchTax> sellerBranchTaxByBranch(String sellerBranchId) {
		return new Specification<SellerBranchTax>() {
			@Override
			public Predicate toPredicate(Root<SellerBranchTax> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.get("sellerBranch").get("id"), sellerBranchId);
			}

		};
	}
}
