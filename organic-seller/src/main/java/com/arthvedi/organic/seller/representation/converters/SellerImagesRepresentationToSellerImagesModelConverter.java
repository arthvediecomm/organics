/**
 *
 */
package com.arthvedi.organic.seller.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.seller.model.SellerImagesModel;
import com.arthvedi.organic.seller.representation.siren.SellerImagesRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("sellerImagesRepresentationToSellerImagesModelConverter")
public class SellerImagesRepresentationToSellerImagesModelConverter
		extends PropertyCopyingConverter<SellerImagesRepresentation, SellerImagesModel> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerImagesModel convert(final SellerImagesRepresentation source) {

		SellerImagesModel target = super.convert(source);

		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(final ObjectFactory<SellerImagesModel> factory) {
		super.setFactory(factory);
	}

}
