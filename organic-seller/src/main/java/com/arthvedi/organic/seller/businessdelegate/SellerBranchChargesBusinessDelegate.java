package com.arthvedi.organic.seller.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.seller.businessdelegate.context.SellerBranchChargesContext;
import com.arthvedi.organic.seller.domain.SellerBranchCharges;
import com.arthvedi.organic.seller.model.SellerBranchChargesModel;
import com.arthvedi.organic.seller.service.ISellerBranchChargesService;

/*
 *@Author varma
 */

@Service
public class SellerBranchChargesBusinessDelegate
		implements
		IBusinessDelegate<SellerBranchChargesModel, SellerBranchChargesContext, IKeyBuilder<String>, String> {

	@Autowired
	private ISellerBranchChargesService sellerBranchChargesService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerBranchChargesModel create(SellerBranchChargesModel model) {
		validateModel(model);
		boolean tax = checkSellerBranchChargesExists(model.getChargeId(),
				model.getSellerBranchId());
		if (tax) {
			model.setStatusMessage("SellerBranchCharges Already Exists");
			return model;
		} else {
			SellerBranchCharges sellerBranchCharges = sellerBranchChargesService
					.create((SellerBranchCharges) conversionService.convert(
							model, forObject(model),
							valueOf(SellerBranchCharges.class)));
			model = convertToSellerBranchChargesModel(sellerBranchCharges);
			model.setStatusMessage("SUCCESS::: SellerBranchCharges Created Successfully");
		}
		return model;
	}

	private boolean checkSellerBranchChargesExists(String chargeId,
			String sellerBranchId) {
		return sellerBranchChargesService.getSellerBranchChargesByChargesId(
				chargeId, sellerBranchId);
	}

	private void validateModel(SellerBranchChargesModel model) {

	}

	private SellerBranchChargesModel convertToSellerBranchChargesModel(
			SellerBranchCharges sellerBranchCharges) {
		return (SellerBranchChargesModel) conversionService.convert(
				sellerBranchCharges, forObject(sellerBranchCharges),
				valueOf(SellerBranchChargesModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			SellerBranchChargesContext context) {
		throw new UnsupportedOperationException("Operation not implemented yet");

	}

	@Override
	public SellerBranchChargesModel edit(IKeyBuilder<String> keyBuilder,
			SellerBranchChargesModel model) {
		SellerBranchCharges sellerBranchCharges = sellerBranchChargesService
				.getSellerBranchCharges(keyBuilder.build().toString());
		sellerBranchCharges = sellerBranchChargesService
				.updateSellerBranchCharges((SellerBranchCharges) conversionService
						.convert(model, forObject(model),
								valueOf(SellerBranchCharges.class)));
		model = convertToSellerBranchChargesModel(sellerBranchCharges);
		model.setStatusMessage("SUCCESS::: SellerBranchCharges Updated Successfully");

		return model;
	}

	@Override
	public SellerBranchChargesModel getByKey(IKeyBuilder<String> keyBuilder,
			SellerBranchChargesContext context) {
		SellerBranchCharges sellerBranchCharges = sellerBranchChargesService
				.getSellerBranchCharges(keyBuilder.build().toString());
		SellerBranchChargesModel model = conversionService.convert(
				sellerBranchCharges, SellerBranchChargesModel.class);
		return model;
	}

	@Override
	public Collection<SellerBranchChargesModel> getCollection(
			SellerBranchChargesContext context) {
		List<SellerBranchCharges> sellerBranchCharges = new ArrayList<SellerBranchCharges>();
		if (context.getSellerBranchId() != null) {
			sellerBranchCharges = sellerBranchChargesService
					.getSellerBranchChargesBySellereBranchId(context
							.getSellerBranchId());
		}
		List<SellerBranchChargesModel> sellerBranchChargesModels = (List<SellerBranchChargesModel>) conversionService
				.convert(sellerBranchCharges, TypeDescriptor
						.forObject(sellerBranchCharges), TypeDescriptor
						.collection(List.class, TypeDescriptor
								.valueOf(SellerBranchChargesModel.class)));
		return sellerBranchChargesModels;
	}

	@Override
	public SellerBranchChargesModel edit(IKeyBuilder<String> keyBuilder,
			SellerBranchChargesModel model, SellerBranchChargesContext context) {
		throw new UnsupportedOperationException("Operation not implemented yet");
	}

}
