package com.arthvedi.organic.seller.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.seller.businessdelegate.context.SellerBranchUserContext;
import com.arthvedi.organic.seller.domain.SellerBranchUser;
import com.arthvedi.organic.seller.model.SellerBranchUserModel;
import com.arthvedi.organic.seller.service.ISellerBranchUserService;

@Service
public class SellerBranchUserBusinessDelegate
		implements
		IBusinessDelegate<SellerBranchUserModel, SellerBranchUserContext, IKeyBuilder<String>, String> {

	@Autowired
	private ISellerBranchUserService sellerBranchUserService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerBranchUserModel create(SellerBranchUserModel model) {
		validateModel(model);
		boolean userPhoneNo = checkUserPhoneExists(model.getPhoneNo());
		boolean userEmail = checkUserMailExists(model.getEmailId());
		if (userEmail || userPhoneNo) {
			model.setStatusMessage("Failure::: " + model.getUserName()
					+ " Already Exists");
			return model;
		} else {
			SellerBranchUser sellerBranchUser = sellerBranchUserService
					.create((SellerBranchUser) conversionService.convert(model,
							forObject(model), valueOf(SellerBranchUser.class)));
			model = convertToSellerBranchUserModel(sellerBranchUser);
			model.setStatusMessage("Success::: " + model.getUserName()
					+ " Created Successfully");
		}
		return model;
	}

	private boolean checkUserMailExists(String emailId) {
		return sellerBranchUserService.getSellerBranchUserByEmailId(emailId);
	}

	private boolean checkUserPhoneExists(String phoneNo) {
		return sellerBranchUserService.getSellerBranchUserByPhoneNo(phoneNo);
	}

	private void validateModel(SellerBranchUserModel model) {

	}

	private SellerBranchUserModel convertToSellerBranchUserModel(
			SellerBranchUser sellerBranchUser) {
		return (SellerBranchUserModel) conversionService.convert(
				sellerBranchUser, forObject(sellerBranchUser),
				valueOf(SellerBranchUserModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			SellerBranchUserContext context) {
		throw new UnsupportedOperationException("Operation not implemented yet");

	}

	@Override
	public SellerBranchUserModel edit(IKeyBuilder<String> keyBuilder,
			SellerBranchUserModel model) {
		SellerBranchUser sellerBranchUser = sellerBranchUserService
				.getSellerBranchUser(keyBuilder.build().toString());

		sellerBranchUser = sellerBranchUserService
				.updateSellerBranchUser((SellerBranchUser) conversionService
						.convert(model, forObject(model),
								valueOf(SellerBranchUser.class)));
		model.setStatusMessage("Success::: " + model.getUserName()
				+ " Updated Successfully");
		return model = convertToSellerBranchUserModel(sellerBranchUser);

	}

	@Override
	public SellerBranchUserModel getByKey(IKeyBuilder<String> keyBuilder,
			SellerBranchUserContext context) {
		SellerBranchUser sellerBranchUser = sellerBranchUserService
				.getSellerBranchUser(keyBuilder.build().toString());
		SellerBranchUserModel model = conversionService.convert(
				sellerBranchUser, SellerBranchUserModel.class);
		return model;
	}

	@Override
	public Collection<SellerBranchUserModel> getCollection(
			SellerBranchUserContext context) {
		List<SellerBranchUser> sellerBranchUser = new ArrayList<SellerBranchUser>();
		if (context.getAll() != null) {
			sellerBranchUser = sellerBranchUserService.getAll();

		}
		if (context.getSellerBranchUserList() != null) {
			sellerBranchUser = sellerBranchUserService
					.getSellerBranchUserList();
		}
		if (context.getSellerBranchId() != null) {
			sellerBranchUser = sellerBranchUserService
					.getSellerBranchUserBySellerBranch(context
							.getSellerBranchId());
		}
		List<SellerBranchUserModel> sellerBranchUserModels = (List<SellerBranchUserModel>) conversionService
				.convert(sellerBranchUser, TypeDescriptor
						.forObject(sellerBranchUser), TypeDescriptor
						.collection(List.class, TypeDescriptor
								.valueOf(SellerBranchUserModel.class)));
		return sellerBranchUserModels;
	}

	@Override
	public SellerBranchUserModel edit(IKeyBuilder<String> keyBuilder,
			SellerBranchUserModel model, SellerBranchUserContext context) {
		throw new UnsupportedOperationException("Operation not implemented yet");
	}

}
