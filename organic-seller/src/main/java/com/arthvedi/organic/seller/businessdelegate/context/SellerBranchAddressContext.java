package com.arthvedi.organic.seller.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;
/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SellerBranchAddressContext implements IBusinessDelegateContext {

	private String sellerBranchAddressId;
private String all;                                                                                                     
	
	public String getSellerBranchAddressId() {
		return sellerBranchAddressId;
	}

	public void setSellerBranchAddressId(String sellerBranchAddressId) {
		this.sellerBranchAddressId = sellerBranchAddressId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

}
