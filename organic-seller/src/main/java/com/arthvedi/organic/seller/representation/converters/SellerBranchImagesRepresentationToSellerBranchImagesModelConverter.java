/**
 *
 */
package com.arthvedi.organic.seller.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.seller.model.SellerBranchImagesModel;
import com.arthvedi.organic.seller.representation.siren.SellerBranchImagesRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("sellerBranchImagesRepresentationToSellerBranchImagesModelConverter")
public class SellerBranchImagesRepresentationToSellerBranchImagesModelConverter
		extends PropertyCopyingConverter<SellerBranchImagesRepresentation, SellerBranchImagesModel> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerBranchImagesModel convert(final SellerBranchImagesRepresentation source) {

		SellerBranchImagesModel target = super.convert(source);

		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(final ObjectFactory<SellerBranchImagesModel> factory) {
		super.setFactory(factory);
	}

}
