/**
 *
 */
package com.arthvedi.organic.seller.representation.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.seller.model.SellerBranchImagesModel;
import com.arthvedi.organic.seller.model.SellerBranchModel;
import com.arthvedi.organic.seller.model.SellerImagesModel;
import com.arthvedi.organic.seller.model.SellerModel;
import com.arthvedi.organic.seller.representation.siren.SellerRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("sellerRepresentationToSellerModelConverter")
public class SellerRepresentationToSellerModelConverter extends
		PropertyCopyingConverter<SellerRepresentation, SellerModel> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerModel convert(final SellerRepresentation source) {

		SellerModel target = super.convert(source);
		if (CollectionUtils
				.isNotEmpty(source.getSellerBranchesRepresentation())) {
			List<SellerBranchModel> convert = (List<SellerBranchModel>) conversionService
					.convert(source.getSellerBranchesRepresentation(),
							TypeDescriptor.forObject(source
									.getSellerBranchesRepresentation()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									SellerBranchModel.class));
			target.getSellerBranchesModel().addAll(convert);
		}
		if (CollectionUtils
				.isNotEmpty(source.getSellerImagesesRepresentation())) {
			List<SellerImagesModel> convert = (List<SellerImagesModel>) conversionService
					.convert(source.getSellerImagesesRepresentation(),
							TypeDescriptor.forObject(source
									.getSellerImagesesRepresentation()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									SellerBranchImagesModel.class));
			target.getSellerImagesesModel().addAll(convert);
		}

		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(final ObjectFactory<SellerModel> factory) {
		super.setFactory(factory);
	}

}
