package com.arthvedi.organic.seller.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.seller.businessdelegate.context.SellerBranchTaxContext;
import com.arthvedi.organic.seller.model.SellerBranchTaxModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/sellerbranchtax", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class SellerBranchTaxController {

	private IBusinessDelegate<SellerBranchTaxModel, SellerBranchTaxContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<SellerBranchTaxContext> sellerBranchTaxContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = { "/create", "/sellerapp/create" })
	public ResponseEntity<SellerBranchTaxModel> createSellerBranchTax(
			@RequestBody SellerBranchTaxModel sellerBranchTaxModel) {
		sellerBranchTaxModel = businessDelegate.create(sellerBranchTaxModel);
		return new ResponseEntity<SellerBranchTaxModel>(sellerBranchTaxModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = { "/{id}/edit", "/sellerapp/{id}/edit" }, method = RequestMethod.PUT)

	public ResponseEntity<SellerBranchTaxModel> edit(@PathVariable(value = "id") final String sellerBranchTaxId,
			@RequestBody SellerBranchTaxModel sellerBranchTaxModel) {
		sellerBranchTaxModel = businessDelegate.edit(keyBuilderFactory.getObject().withId(sellerBranchTaxId),
				sellerBranchTaxModel);
		return new ResponseEntity<SellerBranchTaxModel>(sellerBranchTaxModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all")

	public ResponseEntity<IModelWrapper<Collection<SellerBranchTaxModel>>> getAll() {
		SellerBranchTaxContext sellerBranchTaxContext = sellerBranchTaxContextFactory.getObject();
		sellerBranchTaxContext.setAll("all");
		Collection<SellerBranchTaxModel> sellerBranchTaxModels = businessDelegate.getCollection(sellerBranchTaxContext);
		IModelWrapper<Collection<SellerBranchTaxModel>> models = new CollectionModelWrapper<SellerBranchTaxModel>(
				SellerBranchTaxModel.class, sellerBranchTaxModels);
		return new ResponseEntity<IModelWrapper<Collection<SellerBranchTaxModel>>>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = { "/sellerbranchtaxeslist",
			"/sellerapp/sellerbranchtaxeslist" })
	public ResponseEntity<IModelWrapper<Collection<SellerBranchTaxModel>>> getAllSellerBranchTaxes() {
		SellerBranchTaxContext sellerBranchTaxContext = sellerBranchTaxContextFactory.getObject();
		sellerBranchTaxContext.setSellerBranchTaxesList("SellerBranchTaxesList");
		Collection<SellerBranchTaxModel> sellerBranchTaxModels = businessDelegate.getCollection(sellerBranchTaxContext);
		IModelWrapper<Collection<SellerBranchTaxModel>> models = new CollectionModelWrapper<SellerBranchTaxModel>(
				SellerBranchTaxModel.class, sellerBranchTaxModels);
		return new ResponseEntity<IModelWrapper<Collection<SellerBranchTaxModel>>>(models, HttpStatus.OK);
	}
	@RequestMapping(method = RequestMethod.GET, value = { "/{sellerBranchId}/sellerbranchtaxes",
			"/sellerapp/{sellerBranchId}/sellerbranchtaxes"})
	public ResponseEntity<IModelWrapper<Collection<SellerBranchTaxModel>>> getSellerBranchTaxes(@PathVariable(value="sellerBranchId") final String sellerBranchId) {
		SellerBranchTaxContext sellerBranchTaxContext = sellerBranchTaxContextFactory.getObject();
		sellerBranchTaxContext.setSellerBranchId(sellerBranchId);
		Collection<SellerBranchTaxModel> sellerBranchTaxModels = businessDelegate.getCollection(sellerBranchTaxContext);
		IModelWrapper<Collection<SellerBranchTaxModel>> models = new CollectionModelWrapper<SellerBranchTaxModel>(
				SellerBranchTaxModel.class, sellerBranchTaxModels);
		return new ResponseEntity<IModelWrapper<Collection<SellerBranchTaxModel>>>(models, HttpStatus.OK);
	}
	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<SellerBranchTaxModel> getSellerBranchTax(
			@PathVariable(value = "id") final String sellerBranchTaxId) {
		SellerBranchTaxContext sellerBranchTaxContext = sellerBranchTaxContextFactory.getObject();

		SellerBranchTaxModel model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(sellerBranchTaxId),
				sellerBranchTaxContext);
		return new ResponseEntity<SellerBranchTaxModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "sellerBranchTaxBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<SellerBranchTaxModel, SellerBranchTaxContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setSellerBranchTaxObjectFactory(
			final ObjectFactory<SellerBranchTaxContext> sellerBranchTaxContextFactory) {
		this.sellerBranchTaxContextFactory = sellerBranchTaxContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
