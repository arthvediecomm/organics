/**
 *
 */
package com.arthvedi.organic.seller.model.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.organic.seller.domain.SellerBranch;
import com.arthvedi.organic.seller.domain.SellerBranchAddress;
import com.arthvedi.organic.seller.domain.SellerBranchCharges;
import com.arthvedi.organic.seller.domain.SellerBranchImages;
import com.arthvedi.organic.seller.domain.SellerBranchZone;
import com.arthvedi.organic.seller.model.SellerBranchModel;

@Component("sellerBranchModelToSellerBranchConverter")
public class SellerBranchModelToSellerBranchConverter implements
		Converter<SellerBranchModel, SellerBranch> {
	@Autowired
	private ObjectFactory<SellerBranch> sellerBranchFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerBranch convert(final SellerBranchModel source) {
		SellerBranch sellerBranch = sellerBranchFactory.getObject();
		BeanUtils.copyProperties(source, sellerBranch);
		if (CollectionUtils.isNotEmpty(source.getSellerBranchAddressesModel())) {
			List<SellerBranchAddress> convert = (List<SellerBranchAddress>) conversionService
					.convert(source.getSellerBranchAddressesModel(),
							TypeDescriptor.forObject(source
									.getSellerBranchAddressesModel()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									SellerBranchAddress.class));
			sellerBranch.getSellerBranchAddresses().addAll(convert);
		}
		if (CollectionUtils.isNotEmpty(source.getSellerBranchZonesModel())) {
			List<SellerBranchZone> convert = (List<SellerBranchZone>) conversionService
					.convert(source.getSellerBranchZonesModel(), TypeDescriptor
							.forObject(source.getSellerBranchZonesModel()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									SellerBranchZone.class));
			sellerBranch.getSellerBranchZones().addAll(convert);
		}
		if (CollectionUtils.isNotEmpty(source.getSellerBranchImagesesModel())) {
			List<SellerBranchImages> convert = (List<SellerBranchImages>) conversionService
					.convert(source.getSellerBranchImagesesModel(),
							TypeDescriptor.forObject(source
									.getSellerBranchImagesesModel()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									SellerBranchImages.class));
			sellerBranch.getSellerBranchImageses().addAll(convert);
		}
		if (CollectionUtils.isNotEmpty(source.getSellerBranchChargesesModel())) {
			List<SellerBranchCharges> convert = (List<SellerBranchCharges>) conversionService
					.convert(source.getSellerBranchChargesesModel(),
							TypeDescriptor.forObject(source
									.getSellerBranchChargesesModel()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									SellerBranchCharges.class));
			sellerBranch.getSellerBranchChargeses().addAll(convert);
		}
		return sellerBranch;
	}

	@Autowired
	public void setSellerBranchFactory(
			final ObjectFactory<SellerBranch> sellerBranchFactory) {
		this.sellerBranchFactory = sellerBranchFactory;
	}

}
