package com.arthvedi.organic.seller.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.arthvedi.organic.seller.domain.SellerBranchImages;
import com.arthvedi.organic.seller.model.SellerBranchImagesModel;

@Component("sellerBranchImagesToSellerBranchImagesModelConverter")
public class SellerBranchImagesToSellerBranchImagesModelConverter
		implements Converter<SellerBranchImages, SellerBranchImagesModel> {
	@Autowired
	private ObjectFactory<SellerBranchImagesModel> sellerBranchImagesModelFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerBranchImagesModel convert(final SellerBranchImages source) {
		SellerBranchImagesModel sellerBranchImagesModel = sellerBranchImagesModelFactory.getObject();
		BeanUtils.copyProperties(source, sellerBranchImagesModel);
		if(source.getImageName()!=null){
			sellerBranchImagesModel.setImageName(StringUtils.capitalize(source.getImageName()));
		}

		return sellerBranchImagesModel;
	}

	@Autowired
	public void setSellerBranchImagesModelFactory(
			final ObjectFactory<SellerBranchImagesModel> sellerBranchImagesModelFactory) {
		this.sellerBranchImagesModelFactory = sellerBranchImagesModelFactory;
	}
}
