/**
 *
 */
package com.arthvedi.organic.seller.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.seller.domain.SellerBranchTax;
import com.arthvedi.organic.seller.domain.SellerBranchTaxableCharges;
import com.arthvedi.organic.seller.model.SellerBranchTaxableChargesModel;

/**
 * @author Jay
 *
 */
@Component("sellerBranchTaxableChargesModelToSellerBranchTaxableChargesConverter")
public class SellerBranchTaxableChargesModelToSellerBranchTaxableChargesConverter
		implements Converter<SellerBranchTaxableChargesModel, SellerBranchTaxableCharges> {
	@Autowired
	private ObjectFactory<SellerBranchTaxableCharges> sellerBranchTaxableChargesFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerBranchTaxableCharges convert(final SellerBranchTaxableChargesModel source) {
		SellerBranchTaxableCharges sellerBranchTaxableCharges = sellerBranchTaxableChargesFactory.getObject();
		BeanUtils.copyProperties(source, sellerBranchTaxableCharges);
		if(source.getSellerBranchTaxId()!=null){
			SellerBranchTax slrBTax = new SellerBranchTax();
			slrBTax.setId(source.getSellerBranchTaxId());
			sellerBranchTaxableCharges.setSellerBranchTax(slrBTax);
		}
		return sellerBranchTaxableCharges;
	}

	@Autowired
	public void setSellerBranchTaxableChargesFactory(
			final ObjectFactory<SellerBranchTaxableCharges> sellerBranchTaxableChargesFactory) {
		this.sellerBranchTaxableChargesFactory = sellerBranchTaxableChargesFactory;
	}

}
