package com.arthvedi.organic.seller.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.seller.businessdelegate.context.SellerImagesContext;
import com.arthvedi.organic.seller.domain.SellerBranchImages;
import com.arthvedi.organic.seller.domain.SellerImages;
import com.arthvedi.organic.seller.model.SellerImagesModel;
import com.arthvedi.organic.seller.service.ISellerImagesService;

@Service
public class SellerImagesBusinessDelegate
		implements
		IBusinessDelegate<SellerImagesModel, SellerImagesContext, IKeyBuilder<String>, String> {

	@Autowired
	private ISellerImagesService sellerImagesService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerImagesModel create(SellerImagesModel model) {
		SellerImages sellerImages = sellerImagesService
				.create((SellerImages) conversionService.convert(model,
						forObject(model), valueOf(SellerBranchImages.class)));
		return model = convertToSellerImagesModel(sellerImages);

	}

	private SellerImagesModel convertToSellerImagesModel(
			SellerImages sellerImages) {
		return (SellerImagesModel) conversionService.convert(sellerImages,
				forObject(sellerImages), valueOf(SellerImages.class));

	}

	@Override
	public void delete(final IKeyBuilder<String> keyBuilder,
			final SellerImagesContext context) {

	}

	@Override
	public SellerImagesModel edit(final IKeyBuilder<String> keyBuilder,
			SellerImagesModel model) {
		SellerImages sellerImages = sellerImagesService
				.getSellerImages(keyBuilder.build().toString());
		sellerImages = sellerImagesService
				.updateSellerImages((SellerImages) conversionService.convert(
						model, forObject(model), valueOf(SellerImages.class)));
		model = convertToSellerImagesModel(sellerImages);

		return model;
	}

	@Override
	public SellerImagesModel getByKey(final IKeyBuilder<String> keyBuilder,
			final SellerImagesContext context) {
		SellerImages sellerImages = sellerImagesService
				.getSellerImages(keyBuilder.build().toString());
		SellerImagesModel model = conversionService.convert(sellerImages,
				SellerImagesModel.class);
		return model;
	}

	@Override
	public Collection<SellerImagesModel> getCollection(
			final SellerImagesContext context) {
		List<SellerImages> sellerImages = new ArrayList<SellerImages>();
		List<SellerImagesModel> sellerImagesModels = (List<SellerImagesModel>) conversionService
				.convert(
						sellerImages,
						TypeDescriptor.forObject(sellerImages),
						TypeDescriptor.collection(List.class,
								TypeDescriptor.valueOf(SellerImagesModel.class)));
		return sellerImagesModels;
	}

	@Override
	public SellerImagesModel edit(IKeyBuilder<String> keyBuilder,
			SellerImagesModel model, SellerImagesContext context) {
		throw new UnsupportedOperationException("Operation not implemented yet");
	}

}
