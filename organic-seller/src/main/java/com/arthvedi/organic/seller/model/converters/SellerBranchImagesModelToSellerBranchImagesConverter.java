/**
 *
 */
package com.arthvedi.organic.seller.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.seller.domain.SellerBranch;
import com.arthvedi.organic.seller.domain.SellerBranchImages;
import com.arthvedi.organic.seller.model.SellerBranchImagesModel;

/**
 * @author Jay
 *
 */
@Component("sellerBranchImagesModelToSellerBranchImagesConverter")
public class SellerBranchImagesModelToSellerBranchImagesConverter
		implements Converter<SellerBranchImagesModel, SellerBranchImages> {
	@Autowired
	private ObjectFactory<SellerBranchImages> sellerBranchImagesFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerBranchImages convert(final SellerBranchImagesModel source) {
		SellerBranchImages sellerBranchImages = sellerBranchImagesFactory.getObject();
		BeanUtils.copyProperties(source, sellerBranchImages);
		if(source.getSellerBranchId()!=null){
			SellerBranch slrB = new SellerBranch();
			slrB.setId(source.getSellerBranchId());
			sellerBranchImages.setSellerBranch(slrB);
		}
		return sellerBranchImages;
	}

	@Autowired
	public void setSellerBranchImagesFactory(final ObjectFactory<SellerBranchImages> sellerBranchImagesFactory) {
		this.sellerBranchImagesFactory = sellerBranchImagesFactory;
	}

}
