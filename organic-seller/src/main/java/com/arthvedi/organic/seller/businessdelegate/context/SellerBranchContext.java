package com.arthvedi.organic.seller.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;
/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SellerBranchContext implements IBusinessDelegateContext {

	private String sellerBranchId;
	private String all;
	private String sellerBranchList;
	
	
	
	public String getSellerBranchList() {
		return sellerBranchList;
	}

	public void setSellerBranchList(String sellerBranchList) {
		this.sellerBranchList = sellerBranchList;
	}

	public String getSellerBranchId() {
		return sellerBranchId;
	}

	public void setSellerBranchId(String sellerBranchId) {
		this.sellerBranchId = sellerBranchId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

}
