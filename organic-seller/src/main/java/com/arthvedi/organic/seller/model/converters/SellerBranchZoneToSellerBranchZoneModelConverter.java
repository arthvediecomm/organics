package com.arthvedi.organic.seller.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.seller.domain.SellerBranchZone;
import com.arthvedi.organic.seller.model.SellerBranchZoneModel;

@Component("sellerBranchZoneToSellerBranchZoneModelConverter")
public class SellerBranchZoneToSellerBranchZoneModelConverter
		implements Converter<SellerBranchZone, SellerBranchZoneModel> {
	@Autowired
	private ObjectFactory<SellerBranchZoneModel> sellerBranchZoneModelFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerBranchZoneModel convert(final SellerBranchZone source) {
		SellerBranchZoneModel sellerBranchZoneModel = new SellerBranchZoneModel();
		BeanUtils.copyProperties(source, sellerBranchZoneModel);
		if (source.getSellerBranch() != null) {
			sellerBranchZoneModel.setSellerBranchId(source.getSellerBranch().getId());
			sellerBranchZoneModel.setSellerBranchName(source.getSellerBranch().getBranchName());
		}
		if(source.getZone()!=null){
			sellerBranchZoneModel.setZoneId(source.getZone().getId());
			sellerBranchZoneModel.setZoneName(source.getZone().getZoneName());
		}
		return sellerBranchZoneModel;
	}

	@Autowired
	public void setSellerBranchZoneModelFactory(
			final ObjectFactory<SellerBranchZoneModel> sellerBranchZoneModelFactory) {
		this.sellerBranchZoneModelFactory = sellerBranchZoneModelFactory;
	}
}
