package com.arthvedi.organic.seller.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;

/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SellerBranchTaxableChargesContext implements IBusinessDelegateContext {

	private String all;
	private String sellerBranchTaxableChargesId;

	public String getSellerBranchTaxableChargesId() {
		return sellerBranchTaxableChargesId;
	}

	public void setSellerBranchTaxableChargesId(String sellerBranchTaxableChargesId) {
		this.sellerBranchTaxableChargesId = sellerBranchTaxableChargesId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

}
