package com.arthvedi.organic.seller.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.seller.businessdelegate.context.SellerBranchMarginContext;
import com.arthvedi.organic.seller.domain.SellerBranchMargin;
import com.arthvedi.organic.seller.model.SellerBranchMarginModel;
import com.arthvedi.organic.seller.service.ISellerBranchMarginService;

/*
 *@Author varma
 */

@Service
public class SellerBranchMarginBusinessDelegate
		implements
		IBusinessDelegate<SellerBranchMarginModel, SellerBranchMarginContext, IKeyBuilder<String>, String> {

	@Autowired
	private ISellerBranchMarginService sellerBranchMarginService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerBranchMarginModel create(SellerBranchMarginModel model) {
		SellerBranchMargin sellerBranchMargin = sellerBranchMarginService
				.create((SellerBranchMargin) conversionService.convert(model,
						forObject(model), valueOf(SellerBranchMargin.class)));
		model = convertToSellerBranchMarginModel(sellerBranchMargin);
		return model;
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			SellerBranchMarginContext context) {
		throw new UnsupportedOperationException("Operation not implemented yet");

	}

	@Override
	public SellerBranchMarginModel edit(IKeyBuilder<String> keyBuilder,
			SellerBranchMarginModel model) {
		SellerBranchMargin sellerBranchMargin = sellerBranchMarginService
				.getSellerBranchMargin(keyBuilder.build().toString());
		sellerBranchMargin = sellerBranchMarginService
				.updateSellerBranchMargin((SellerBranchMargin) conversionService
						.convert(model, forObject(model),
								valueOf(SellerBranchMargin.class)));
		model = convertToSellerBranchMarginModel(sellerBranchMargin);
		return model;
	}

	private SellerBranchMarginModel convertToSellerBranchMarginModel(
			SellerBranchMargin sellerBranchMargin) {
		return (SellerBranchMarginModel) conversionService.convert(
				sellerBranchMargin, forObject(sellerBranchMargin),
				valueOf(SellerBranchMarginModel.class));
	}

	@Override
	public SellerBranchMarginModel getByKey(IKeyBuilder<String> keyBuilder,
			SellerBranchMarginContext context) {
		SellerBranchMargin sellerBranchMargin = sellerBranchMarginService
				.getSellerBranchMargin(keyBuilder.build().toString());
		SellerBranchMarginModel model = conversionService.convert(
				sellerBranchMargin, SellerBranchMarginModel.class);
		return model;
	}

	@Override
	public Collection<SellerBranchMarginModel> getCollection(
			SellerBranchMarginContext context) {
		List<SellerBranchMargin> sellerBranchMargin = new ArrayList<SellerBranchMargin>();
		if (context.getAll() != null) {
			sellerBranchMargin = sellerBranchMarginService.getAll();
		}
		List<SellerBranchMarginModel> sellerBranchMarginModels = (List<SellerBranchMarginModel>) conversionService
				.convert(sellerBranchMargin, TypeDescriptor
						.forObject(sellerBranchMargin), TypeDescriptor
						.collection(List.class, TypeDescriptor
								.valueOf(SellerBranchMarginModel.class)));
		return sellerBranchMarginModels;
	}

	@Override
	public SellerBranchMarginModel edit(IKeyBuilder<String> keyBuilder,
			SellerBranchMarginModel model, SellerBranchMarginContext context) {
		throw new UnsupportedOperationException("Operation not implemented yet");
	}

}
