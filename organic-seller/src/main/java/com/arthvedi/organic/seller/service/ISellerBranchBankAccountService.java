package com.arthvedi.organic.seller.service;

import java.util.List;

import com.arthvedi.organic.seller.domain.SellerBranchBankAccount;

/*
 *@Author varma
 */
public interface ISellerBranchBankAccountService {

	SellerBranchBankAccount create(
			SellerBranchBankAccount sellerBranchBankAccount);

	void deleteSellerBranchBankAccount(String sellerBranchBankAccountId);

	SellerBranchBankAccount getSellerBranchBankAccount(
			String sellerBranchBankAccountId);

	List<SellerBranchBankAccount> getAll();

	SellerBranchBankAccount updateSellerBranchBankAccount(
			SellerBranchBankAccount sellerBranchBankAccount);

	List<SellerBranchBankAccount> getSellerBranchBankAccountBySellerBranch(
			String sellerBranchId);
}
