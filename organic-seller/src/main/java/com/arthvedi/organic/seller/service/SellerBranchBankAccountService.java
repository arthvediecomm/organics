package com.arthvedi.organic.seller.service;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.seller.domain.SellerBranchBankAccount;
import com.arthvedi.organic.seller.repository.SellerBranchBankAccountRepository;

/*
 *@Author varma
 */
@Component
public class SellerBranchBankAccountService implements
		ISellerBranchBankAccountService {
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private SellerBranchBankAccountRepository sellerBranchBankAccountRepository;

	@Override
	public SellerBranchBankAccount create(
			SellerBranchBankAccount sellerBranchBankAccount) {
		sellerBranchBankAccount.setUserCreated("varma");
		sellerBranchBankAccount.setCreatedDate(new LocalDateTime());
		return sellerBranchBankAccountRepository.save(sellerBranchBankAccount);
	}

	@Override
	public void deleteSellerBranchBankAccount(String sellerBranchBankAccountId) {

	}

	@Override
	public SellerBranchBankAccount getSellerBranchBankAccount(
			String sellerBranchBankAccountId) {
		return sellerBranchBankAccountRepository
				.findOne(sellerBranchBankAccountId);
	}

	@Override
	public List<SellerBranchBankAccount> getAll() {
		return null;
	}

	@Override
	public SellerBranchBankAccount updateSellerBranchBankAccount(
			SellerBranchBankAccount sellerBranchBankAccount) {
		SellerBranchBankAccount sellerBranchBankAccounts = sellerBranchBankAccountRepository
				.findOne(sellerBranchBankAccount.getId());
		try {
			nonNullBeanUtils.copyProperties(sellerBranchBankAccount,
					sellerBranchBankAccount);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		sellerBranchBankAccounts.setModifiedDate(new LocalDateTime());
		sellerBranchBankAccounts.setUserModified("admin");
		sellerBranchBankAccount = sellerBranchBankAccountRepository
				.save(sellerBranchBankAccounts);
		return sellerBranchBankAccount;
	}

	@Override
	public List<SellerBranchBankAccount> getSellerBranchBankAccountBySellerBranch(
			String sellerBranchId) {
		Specification<SellerBranchBankAccount> getBanhAccountBySellerBranch = new Specification<SellerBranchBankAccount>() {

			@Override
			public Predicate toPredicate(Root<SellerBranchBankAccount> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.join("sellerBranch").get("id"),
						sellerBranchId);
			}

		};
		return sellerBranchBankAccountRepository
				.findAll(getBanhAccountBySellerBranch);

	}
}
