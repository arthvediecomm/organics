/**
 *
 */
package com.arthvedi.organic.seller.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.seller.model.SellerBranchImagesModel;
import com.arthvedi.organic.seller.representation.siren.SellerBranchImagesRepresentation;

/**
 * @author varma
 *
 */
@Component("sellerBranchImagesModelToSellerBranchImagesRepresentationConverter")
public class SellerBranchImagesModelToSellerBranchImagesRepresentationConverter
		extends PropertyCopyingConverter<SellerBranchImagesModel, SellerBranchImagesRepresentation> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerBranchImagesRepresentation convert(final SellerBranchImagesModel source) {

		SellerBranchImagesRepresentation target = super.convert(source);

		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(final ObjectFactory<SellerBranchImagesRepresentation> factory) {
		super.setFactory(factory);
	}

}
