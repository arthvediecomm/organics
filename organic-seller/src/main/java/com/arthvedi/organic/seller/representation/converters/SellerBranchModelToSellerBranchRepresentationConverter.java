/**
 *
 */
package com.arthvedi.organic.seller.representation.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.seller.model.SellerBranchModel;
import com.arthvedi.organic.seller.representation.siren.SellerBranchAddressRepresentation;
import com.arthvedi.organic.seller.representation.siren.SellerBranchImagesRepresentation;
import com.arthvedi.organic.seller.representation.siren.SellerBranchRepresentation;
import com.arthvedi.organic.seller.representation.siren.SellerBranchZoneRepresentation;

/**
 * @author varma
 *
 */
@Component("sellerBranchModelToSellerBranchRepresentationConverter")
public class SellerBranchModelToSellerBranchRepresentationConverter extends
		PropertyCopyingConverter<SellerBranchModel, SellerBranchRepresentation> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerBranchRepresentation convert(final SellerBranchModel source) {

		SellerBranchRepresentation target = super.convert(source);

		if (CollectionUtils.isNotEmpty(source.getSellerBranchAddressesModel())) {
			List<SellerBranchAddressRepresentation> convert = (List<SellerBranchAddressRepresentation>) conversionService
					.convert(source.getSellerBranchAddressesModel(),
							TypeDescriptor.forObject(source
									.getSellerBranchAddressesModel()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									SellerBranchAddressRepresentation.class));
			target.getSellerBranchAddressesRepresentation().addAll(convert);

		}
		if (CollectionUtils.isNotEmpty(source.getSellerBranchZonesModel())) {
			List<SellerBranchZoneRepresentation> convert = (List<SellerBranchZoneRepresentation>) conversionService
					.convert(source.getSellerBranchZonesModel(), TypeDescriptor
							.forObject(source.getSellerBranchZonesModel()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									SellerBranchZoneRepresentation.class));
			target.getSellerBranchZonesRepresentation().addAll(convert);
		}
		if (CollectionUtils.isNotEmpty(source.getSellerBranchImagesesModel())) {
			List<SellerBranchImagesRepresentation> convert = (List<SellerBranchImagesRepresentation>) conversionService
					.convert(source.getSellerBranchImagesesModel(),
							TypeDescriptor.forObject(source
									.getSellerBranchImagesesModel()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									SellerBranchImagesRepresentation.class));
			target.getSellerBranchImagesRepresentations().addAll(convert);
		}

		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(
			final ObjectFactory<SellerBranchRepresentation> factory) {
		super.setFactory(factory);
	}

}
