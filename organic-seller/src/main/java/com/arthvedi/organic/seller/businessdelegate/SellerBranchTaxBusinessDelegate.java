package com.arthvedi.organic.seller.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.seller.businessdelegate.context.SellerBranchTaxContext;
import com.arthvedi.organic.seller.domain.SellerBranchTax;
import com.arthvedi.organic.seller.model.SellerBranchTaxModel;
import com.arthvedi.organic.seller.service.ISellerBranchTaxService;

/*
 *@Author varma
 */

@Service
public class SellerBranchTaxBusinessDelegate
		implements
		IBusinessDelegate<SellerBranchTaxModel, SellerBranchTaxContext, IKeyBuilder<String>, String> {

	@Autowired
	private ISellerBranchTaxService sellerBranchTaxService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerBranchTaxModel create(SellerBranchTaxModel model) {
		validateModel(model);
		boolean tax = checkTaxExists(model.getTaxId(),
				model.getSellerBranchId());
		if (tax) {
			model.setStatusMessage("Tax Already Exists");
			return model;
		} else {
			SellerBranchTax sellerBranchTax = sellerBranchTaxService
					.create((SellerBranchTax) conversionService.convert(model,
							forObject(model), valueOf(SellerBranchTax.class)));
			model = convertToSellerBranchTaxModel(sellerBranchTax);
			model.setStatusMessage("Tax Added Successfully");
		}
		return model;
	}

	private SellerBranchTaxModel convertToSellerBranchTaxModel(
			SellerBranchTax sellerBranchTax) {
		return (SellerBranchTaxModel) conversionService.convert(
				sellerBranchTax, forObject(sellerBranchTax),
				valueOf(SellerBranchTaxModel.class));
	}

	private boolean checkTaxExists(String taxId, String sellerBranchId) {
		return sellerBranchTaxService.getSellerBranchTaxByTax(taxId,
				sellerBranchId);
	}

	private void validateModel(SellerBranchTaxModel model) {

	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			SellerBranchTaxContext context) {
		throw new UnsupportedOperationException("Operation not implemented yet");
	}

	@Override
	public SellerBranchTaxModel edit(IKeyBuilder<String> keyBuilder,
			SellerBranchTaxModel model) {
		SellerBranchTax sellerBranchTax = sellerBranchTaxService
				.getSellerBranchTax(keyBuilder.build().toString());
		sellerBranchTax = sellerBranchTaxService
				.updateSellerBranchTax((SellerBranchTax) conversionService
						.convert(model, forObject(model),
								valueOf(SellerBranchTax.class)));
		model = convertToSellerBranchTaxModel(sellerBranchTax);
		model.setStatusMessage("SUCCESS::: SellerBranchTax updated Successfully");
		return model;
	}

	@Override
	public SellerBranchTaxModel getByKey(IKeyBuilder<String> keyBuilder,
			SellerBranchTaxContext context) {
		SellerBranchTax sellerBranchTax = sellerBranchTaxService
				.getSellerBranchTax(keyBuilder.build().toString());
		SellerBranchTaxModel model = conversionService.convert(sellerBranchTax,
				SellerBranchTaxModel.class);
		return model;
	}

	@Override
	public Collection<SellerBranchTaxModel> getCollection(
			SellerBranchTaxContext context) {
		List<SellerBranchTax> sellerBranchTax = new ArrayList<SellerBranchTax>();
		if (context.getSellerBranchTaxesList() != null) {
			sellerBranchTax = sellerBranchTaxService.getSellerBranchTaxesList();
		}
		if (context.getSellerBranchId() != null) {
			sellerBranchTax = sellerBranchTaxService
					.getSellerBranchTaxesBySellerBranch(context
							.getSellerBranchId());
		}
		List<SellerBranchTaxModel> sellerBranchTaxModels = (List<SellerBranchTaxModel>) conversionService
				.convert(sellerBranchTax, TypeDescriptor
						.forObject(sellerBranchTax), TypeDescriptor.collection(
						List.class,
						TypeDescriptor.valueOf(SellerBranchTaxModel.class)));
		return sellerBranchTaxModels;
	}

	@Override
	public SellerBranchTaxModel edit(IKeyBuilder<String> keyBuilder,
			SellerBranchTaxModel model, SellerBranchTaxContext context) {
		throw new UnsupportedOperationException("Operation not implemented yet");
	}

}
