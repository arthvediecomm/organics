/**
 *
 */
package com.arthvedi.organic.seller.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.seller.model.SellerBranchTaxModel;
import com.arthvedi.organic.seller.representation.siren.SellerBranchTaxRepresentation;

/**
 * @author varma
 *
 */
@Component("sellerBranchTaxModelToSellerBranchTaxRepresentationConverter")
public class SellerBranchTaxModelToSellerBranchTaxRepresentationConverter
		extends PropertyCopyingConverter<SellerBranchTaxModel, SellerBranchTaxRepresentation> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerBranchTaxRepresentation convert(final SellerBranchTaxModel source) {

		SellerBranchTaxRepresentation target = super.convert(source);

		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(final ObjectFactory<SellerBranchTaxRepresentation> factory) {
		super.setFactory(factory);
	}

}
