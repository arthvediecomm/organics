package com.arthvedi.organic.seller.service;

import java.util.List;

import com.arthvedi.organic.seller.domain.SellerBranchMargin;
/*
*@Author varma
*/
public interface ISellerBranchMarginService {
	
	SellerBranchMargin create(SellerBranchMargin sellerBranchMargin);

	void deleteSellerBranchMargin(String sellerBranchMarginId);

	SellerBranchMargin getSellerBranchMargin(String sellerBranchMarginId);

	List<SellerBranchMargin> getAll();

	SellerBranchMargin updateSellerBranchMargin(SellerBranchMargin sellerBranchMargin);
}
