/**
 *
 */
package com.arthvedi.organic.seller.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.seller.model.SellerBranchTaxableChargesModel;
import com.arthvedi.organic.seller.representation.siren.SellerBranchTaxableChargesRepresentation;

/**
 * @author varma
 *
 */
@Component("sellerBranchTaxableChargesModelToSellerBranchTaxableChargesRepresentationConverter")
public class SellerBranchTaxableChargesModelToSellerBranchTaxableChargesRepresentationConverter
		extends PropertyCopyingConverter<SellerBranchTaxableChargesModel, SellerBranchTaxableChargesRepresentation> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerBranchTaxableChargesRepresentation convert(final SellerBranchTaxableChargesModel source) {

		SellerBranchTaxableChargesRepresentation target = super.convert(source);

		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(final ObjectFactory<SellerBranchTaxableChargesRepresentation> factory) {
		super.setFactory(factory);
	}

}
