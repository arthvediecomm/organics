/**
 *
 */
package com.arthvedi.organic.seller.representation.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.seller.model.SellerBranchAddressModel;
import com.arthvedi.organic.seller.model.SellerBranchChargesModel;
import com.arthvedi.organic.seller.model.SellerBranchImagesModel;
import com.arthvedi.organic.seller.model.SellerBranchModel;
import com.arthvedi.organic.seller.model.SellerBranchZoneModel;
import com.arthvedi.organic.seller.representation.siren.SellerBranchRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("sellerBranchRepresentationToSellerBranchModelConverter")
public class SellerBranchRepresentationToSellerBranchModelConverter extends
		PropertyCopyingConverter<SellerBranchRepresentation, SellerBranchModel> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerBranchModel convert(final SellerBranchRepresentation source) {

		SellerBranchModel target = super.convert(source);
		if (CollectionUtils.isNotEmpty(source
				.getSellerBranchAddressesRepresentation())) {
			List<SellerBranchAddressModel> convert = (List<SellerBranchAddressModel>) conversionService
					.convert(source.getSellerBranchAddressesRepresentation(),
							TypeDescriptor.forObject(source
									.getSellerBranchAddressesRepresentation()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									SellerBranchAddressModel.class));
			target.getSellerBranchAddressesModel().addAll(convert);

		}

		if (CollectionUtils.isNotEmpty(source
				.getSellerBranchZonesRepresentation())) {
			List<SellerBranchZoneModel> convert = (List<SellerBranchZoneModel>) conversionService
					.convert(source.getSellerBranchZonesRepresentation(),
							TypeDescriptor.forObject(source
									.getSellerBranchZonesRepresentation()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									SellerBranchZoneModel.class));
			target.getSellerBranchZonesModel().addAll(convert);
		}
		if (CollectionUtils.isNotEmpty(source
				.getSellerBranchImagesRepresentations())) {
			List<SellerBranchImagesModel> convert = (List<SellerBranchImagesModel>) conversionService
					.convert(source.getSellerBranchImagesRepresentations(),
							TypeDescriptor.forObject(source
									.getSellerBranchImagesRepresentations()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									SellerBranchImagesModel.class));
			target.getSellerBranchImagesesModel().addAll(convert);
		}
		if (CollectionUtils.isNotEmpty(source
				.getSellerBranchChargesesRepresentation())) {
			List<SellerBranchChargesModel> convert = (List<SellerBranchChargesModel>) conversionService
					.convert(source.getSellerBranchChargesesRepresentation(),
							TypeDescriptor.forObject(source
									.getSellerBranchChargesesRepresentation()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									SellerBranchChargesModel.class));
			target.getSellerBranchChargesesModel().addAll(convert);
		}
		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(final ObjectFactory<SellerBranchModel> factory) {
		super.setFactory(factory);
	}

}
