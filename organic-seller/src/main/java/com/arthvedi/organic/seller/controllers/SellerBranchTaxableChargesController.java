package com.arthvedi.organic.seller.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.seller.businessdelegate.context.SellerBranchTaxableChargesContext;
import com.arthvedi.organic.seller.model.SellerBranchTaxableChargesModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/sellerbranchtaxablecharges", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class SellerBranchTaxableChargesController {

	private IBusinessDelegate<SellerBranchTaxableChargesModel, SellerBranchTaxableChargesContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<SellerBranchTaxableChargesContext> sellerBranchTaxableChargesContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create")

	public ResponseEntity<SellerBranchTaxableChargesModel> createSellerBranchTaxableCharges(
			@RequestBody final SellerBranchTaxableChargesModel sellerBranchTaxableChargesModel) {
		businessDelegate.create(sellerBranchTaxableChargesModel);
		return new ResponseEntity<SellerBranchTaxableChargesModel>(sellerBranchTaxableChargesModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	
	public ResponseEntity<SellerBranchTaxableChargesModel> edit(
			@PathVariable(value = "id") final String sellerBranchTaxableChargesId,
			@RequestBody final SellerBranchTaxableChargesModel sellerBranchTaxableChargesModel) {

		businessDelegate.edit(keyBuilderFactory.getObject().withId(sellerBranchTaxableChargesId),
				sellerBranchTaxableChargesModel);
		return new ResponseEntity<SellerBranchTaxableChargesModel>(sellerBranchTaxableChargesModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all")

	public ResponseEntity<IModelWrapper<Collection<SellerBranchTaxableChargesModel>>> getAll() {
		SellerBranchTaxableChargesContext sellerBranchTaxableChargesContext = sellerBranchTaxableChargesContextFactory
				.getObject();
		sellerBranchTaxableChargesContext.setAll("all");
		Collection<SellerBranchTaxableChargesModel> sellerBranchTaxableChargesModels = businessDelegate
				.getCollection(sellerBranchTaxableChargesContext);
		IModelWrapper<Collection<SellerBranchTaxableChargesModel>> models = new CollectionModelWrapper<SellerBranchTaxableChargesModel>(
				SellerBranchTaxableChargesModel.class, sellerBranchTaxableChargesModels);
		return new ResponseEntity<IModelWrapper<Collection<SellerBranchTaxableChargesModel>>>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	
	public ResponseEntity<SellerBranchTaxableChargesModel> getSellerBranchTaxableCharges(
			@PathVariable(value = "id") final String sellerBranchTaxableChargesId) {
		SellerBranchTaxableChargesContext sellerBranchTaxableChargesContext = sellerBranchTaxableChargesContextFactory
				.getObject();

		SellerBranchTaxableChargesModel model = businessDelegate.getByKey(
				keyBuilderFactory.getObject().withId(sellerBranchTaxableChargesId), sellerBranchTaxableChargesContext);
		return new ResponseEntity<SellerBranchTaxableChargesModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "sellerBranchTaxableChargesBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<SellerBranchTaxableChargesModel, SellerBranchTaxableChargesContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setSellerBranchTaxableChargesObjectFactory(
			final ObjectFactory<SellerBranchTaxableChargesContext> sellerBranchTaxableChargesContextFactory) {
		this.sellerBranchTaxableChargesContextFactory = sellerBranchTaxableChargesContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
