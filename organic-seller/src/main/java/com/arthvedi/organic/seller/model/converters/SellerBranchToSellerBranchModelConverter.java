package com.arthvedi.organic.seller.model.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.organic.seller.domain.SellerBranch;
import com.arthvedi.organic.seller.model.SellerBranchAddressModel;
import com.arthvedi.organic.seller.model.SellerBranchImagesModel;
import com.arthvedi.organic.seller.model.SellerBranchModel;
import com.arthvedi.organic.seller.model.SellerBranchZoneModel;

@Component("sellerBranchToSellerBranchModelConverter")
public class SellerBranchToSellerBranchModelConverter implements
		Converter<SellerBranch, SellerBranchModel> {
	@Autowired
	private ObjectFactory<SellerBranchModel> sellerBranchModelFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerBranchModel convert(final SellerBranch source) {
		SellerBranchModel sellerBranchModel = sellerBranchModelFactory
				.getObject();
		BeanUtils.copyProperties(source, sellerBranchModel);
		if (CollectionUtils.isNotEmpty(source.getSellerBranchAddresses())) {
			List<SellerBranchAddressModel> convert = (List<SellerBranchAddressModel>) conversionService
					.convert(source.getSellerBranchAddresses(), TypeDescriptor
							.forObject(source.getSellerBranchAddresses()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									SellerBranchAddressModel.class));

			sellerBranchModel.getSellerBranchAddressesModel().addAll(convert);
		}
		if (CollectionUtils.isNotEmpty(source.getSellerBranchZones())) {
			List<SellerBranchZoneModel> convert = (List<SellerBranchZoneModel>) conversionService
					.convert(source.getSellerBranchZones(), TypeDescriptor
							.forObject(source.getSellerBranchZones()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									SellerBranchZoneModel.class));
			sellerBranchModel.getSellerBranchZonesModel().addAll(convert);
		}
		if (CollectionUtils.isNotEmpty(source.getSellerBranchImageses())) {
			List<SellerBranchImagesModel> convert = (List<SellerBranchImagesModel>) conversionService
					.convert(source.getSellerBranchImageses(), TypeDescriptor
							.forObject(source.getSellerBranchImageses()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									SellerBranchImagesModel.class));

			sellerBranchModel.getSellerBranchImagesesModel().addAll(convert);
		}

		return sellerBranchModel;
	}

	@Autowired
	public void setSellerBranchModelFactory(
			final ObjectFactory<SellerBranchModel> sellerBranchModelFactory) {
		this.sellerBranchModelFactory = sellerBranchModelFactory;
	}
}
