/**
 *
 */
package com.arthvedi.organic.seller.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.seller.model.SellerBranchBankAccountModel;
import com.arthvedi.organic.seller.representation.siren.SellerBranchBankAccountRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("sellerBranchBankAccountRepresentationToSellerBranchBankAccountModelConverter")
public class SellerBranchBankAccountRepresentationToSellerBranchBankAccountModelConverter extends PropertyCopyingConverter<SellerBranchBankAccountRepresentation, SellerBranchBankAccountModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public SellerBranchBankAccountModel convert(final SellerBranchBankAccountRepresentation source) {

        SellerBranchBankAccountModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<SellerBranchBankAccountModel> factory) {
        super.setFactory(factory);
    }

}
