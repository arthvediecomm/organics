package com.arthvedi.organic.seller.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.seller.domain.SellerBranchTaxableCharges;
import com.arthvedi.organic.seller.repository.SellerBranchTaxableChargesRepository;

@Component
public class SellerBranchTaxableChargesService implements
		ISellerBranchTaxableChargesService {

	@Autowired
	private SellerBranchTaxableChargesRepository sellerBranchTaxableChargesRepository;

	@Override
	public SellerBranchTaxableCharges create(
			SellerBranchTaxableCharges sellerBranchTaxableCharges) {
		return sellerBranchTaxableChargesRepository
				.save(sellerBranchTaxableCharges);
	}

	@Override
	public SellerBranchTaxableCharges getSellerBranchTaxableCharges(
			String sellerBranchTaxableChargesId) {
		return sellerBranchTaxableChargesRepository
				.findOne(sellerBranchTaxableChargesId);
	}

	@Override
	public SellerBranchTaxableCharges updateSellerBranchTaxableCharges(
			SellerBranchTaxableCharges sellerBranchTaxableCharges) {
		return sellerBranchTaxableChargesRepository
				.save(sellerBranchTaxableCharges);
	}

	@Override
	public boolean getSellerBranchTaxableChargesByTaxAndCharges(
			String sellerBranchTaxId, String sellerBranchChargesId) {
		return sellerBranchTaxableChargesRepository.findByTaxAndCharges(
				sellerBranchTaxId, sellerBranchChargesId) != null;
	}

}
