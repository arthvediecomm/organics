package com.arthvedi.organic.seller.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.seller.businessdelegate.context.SellerBranchZoneContext;
import com.arthvedi.organic.seller.model.SellerBranchZoneModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/sellerbranchzone", produces = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class SellerBranchZoneController {

	private IBusinessDelegate<SellerBranchZoneModel, SellerBranchZoneContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<SellerBranchZoneContext> sellerBranchZoneContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = { "/create",
			"/sellerapp/create" }, consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<SellerBranchZoneModel> createSellerBranchZone(
			@RequestBody SellerBranchZoneModel sellerBranchZoneModel) {
		sellerBranchZoneModel = businessDelegate.create(sellerBranchZoneModel);
		return new ResponseEntity<SellerBranchZoneModel>(sellerBranchZoneModel,
				HttpStatus.CREATED);
	}

	@RequestMapping(value = { "/{id}/edit", "/sellerapp/{id}/edit" }, method = RequestMethod.PUT)
	public ResponseEntity<SellerBranchZoneModel> edit(
			@PathVariable(value = "id") final String sellerBranchZoneId,
			@RequestBody SellerBranchZoneModel sellerBranchZoneModel) {
		sellerBranchZoneModel = businessDelegate.edit(keyBuilderFactory
				.getObject().withId(sellerBranchZoneId), sellerBranchZoneModel);
		return new ResponseEntity<SellerBranchZoneModel>(sellerBranchZoneModel,
				HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all")
	public ResponseEntity<IModelWrapper<Collection<SellerBranchZoneModel>>> getAll() {
		SellerBranchZoneContext sellerBranchZoneContext = sellerBranchZoneContextFactory
				.getObject();
		sellerBranchZoneContext.setAll("all");
		Collection<SellerBranchZoneModel> sellerBranchZoneModels = businessDelegate
				.getCollection(sellerBranchZoneContext);
		IModelWrapper<Collection<SellerBranchZoneModel>> models = new CollectionModelWrapper<SellerBranchZoneModel>(
				SellerBranchZoneModel.class, sellerBranchZoneModels);
		return new ResponseEntity<IModelWrapper<Collection<SellerBranchZoneModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = {
			"/{sellerBranchId}/sellerbranchzonesbysellerbranch",
			"/sellerapp/{sellerBranchId}/sellerbranchzonesbysellerbranch" })
	public ResponseEntity<IModelWrapper<Collection<SellerBranchZoneModel>>> getSellerBranchZonesBySellerBranch(
			@PathVariable(value = "sellerBranchId") final String sellerBranchId) {
		SellerBranchZoneContext sellerBranchZoneContext = sellerBranchZoneContextFactory
				.getObject();
		sellerBranchZoneContext.setSellerBranchId(sellerBranchId);
		Collection<SellerBranchZoneModel> sellerBranchZoneModels = businessDelegate
				.getCollection(sellerBranchZoneContext);
		IModelWrapper<Collection<SellerBranchZoneModel>> models = new CollectionModelWrapper<SellerBranchZoneModel>(
				SellerBranchZoneModel.class, sellerBranchZoneModels);
		return new ResponseEntity<IModelWrapper<Collection<SellerBranchZoneModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<SellerBranchZoneModel> getSellerBranchZone(
			@PathVariable(value = "id") final String sellerBranchZoneId) {
		SellerBranchZoneContext sellerBranchZoneContext = sellerBranchZoneContextFactory
				.getObject();

		SellerBranchZoneModel model = businessDelegate.getByKey(
				keyBuilderFactory.getObject().withId(sellerBranchZoneId),
				sellerBranchZoneContext);
		return new ResponseEntity<SellerBranchZoneModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "sellerBranchZoneBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<SellerBranchZoneModel, SellerBranchZoneContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setSellerBranchZoneObjectFactory(
			final ObjectFactory<SellerBranchZoneContext> sellerBranchZoneContextFactory) {
		this.sellerBranchZoneContextFactory = sellerBranchZoneContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(
			final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
