package com.arthvedi.organic.seller.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.seller.businessdelegate.context.SellerImagesContext;
import com.arthvedi.organic.seller.model.SellerImagesModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/sellerimages", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class SellerImagesController {

	private IBusinessDelegate<SellerImagesModel, SellerImagesContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<SellerImagesContext> sellerImagesContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create")

	public ResponseEntity<SellerImagesModel> createSellerImages(
			@RequestBody final SellerImagesModel sellerImagesModel) {
		businessDelegate.create(sellerImagesModel);
		return new ResponseEntity<SellerImagesModel>(sellerImagesModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	
	public ResponseEntity<SellerImagesModel> edit(@PathVariable(value = "id") final String sellerImagesId,
			@RequestBody final SellerImagesModel sellerImagesModel) {

		businessDelegate.edit(keyBuilderFactory.getObject().withId(sellerImagesId), sellerImagesModel);
		return new ResponseEntity<SellerImagesModel>(sellerImagesModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all")
	
	public ResponseEntity<IModelWrapper<Collection<SellerImagesModel>>> getAll() {
		SellerImagesContext sellerImagesContext = sellerImagesContextFactory.getObject();
		sellerImagesContext.setAll("all");
		Collection<SellerImagesModel> sellerImagesModels = businessDelegate.getCollection(sellerImagesContext);
		IModelWrapper<Collection<SellerImagesModel>> models = new CollectionModelWrapper<SellerImagesModel>(
				SellerImagesModel.class, sellerImagesModels);
		return new ResponseEntity<IModelWrapper<Collection<SellerImagesModel>>>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	
	public ResponseEntity<SellerImagesModel> getSellerImages(@PathVariable(value = "id") final String sellerImagesId) {
		SellerImagesContext sellerImagesContext = sellerImagesContextFactory.getObject();

		SellerImagesModel model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(sellerImagesId),
				sellerImagesContext);
		return new ResponseEntity<SellerImagesModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "sellerImagesBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<SellerImagesModel, SellerImagesContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

	@Autowired
	public void setSellerImagesObjectFactory(final ObjectFactory<SellerImagesContext> sellerImagesContextFactory) {
		this.sellerImagesContextFactory = sellerImagesContextFactory;
	}

}
