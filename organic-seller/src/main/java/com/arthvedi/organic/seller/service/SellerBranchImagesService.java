package com.arthvedi.organic.seller.service;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.arthvedi.organic.seller.domain.SellerBranchImages;
import com.arthvedi.organic.seller.repository.SellerBranchImagesRepository;

/*
 *@Author varma
 */
@Component
@Transactional
public class SellerBranchImagesService implements ISellerBranchImagesService {

	@Autowired
	private SellerBranchImagesRepository sellerBranchImagesRepository;

	@Override
	@Transactional
	public SellerBranchImages create(SellerBranchImages sellerBranchImages) {
		sellerBranchImages.setCreatedDate(new LocalDateTime());
		sellerBranchImages.setUserCreated("VARMA");
		return sellerBranchImagesRepository.save(sellerBranchImages);
	}

	@Override
	public SellerBranchImages getSellerBranchImages(String sellerBranchImagesId) {
		return sellerBranchImagesRepository.findOne(sellerBranchImagesId);
	}

	@Override
	public SellerBranchImages updateSellerBranchImages(
			SellerBranchImages sellerBranchImages) {
		return sellerBranchImagesRepository.save(sellerBranchImages);
	}

}
