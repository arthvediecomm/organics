/**
 *
 */
package com.arthvedi.organic.seller.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.seller.model.SellerBranchZoneModel;
import com.arthvedi.organic.seller.representation.siren.SellerBranchZoneRepresentation;

/**
 * @author varma
 *
 */
@Component("sellerBranchZoneModelToSellerBranchZoneRepresentationConverter")
public class SellerBranchZoneModelToSellerBranchZoneRepresentationConverter
		extends PropertyCopyingConverter<SellerBranchZoneModel, SellerBranchZoneRepresentation> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerBranchZoneRepresentation convert(final SellerBranchZoneModel source) {

		SellerBranchZoneRepresentation target = super.convert(source);

		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(final ObjectFactory<SellerBranchZoneRepresentation> factory) {
		super.setFactory(factory);
	}

}
