/**
 *
 */
package com.arthvedi.organic.seller.model.converters;

import java.math.BigDecimal;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.commons.domain.Charge;
import com.arthvedi.organic.seller.domain.SellerBranch;
import com.arthvedi.organic.seller.domain.SellerBranchCharges;
import com.arthvedi.organic.seller.model.SellerBranchChargesModel;

/**
 * @author Jay
 *
 */
@Component("sellerBranchChargesModelToSellerBranchChargesConverter")
public class SellerBranchChargesModelToSellerBranchChargesConverter implements
		Converter<SellerBranchChargesModel, SellerBranchCharges> {
	@Autowired
	private ObjectFactory<SellerBranchCharges> sellerBranchChargesFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerBranchCharges convert(final SellerBranchChargesModel source) {
		SellerBranchCharges sellerBranchCharges = sellerBranchChargesFactory
				.getObject();
		BeanUtils.copyProperties(source, sellerBranchCharges);
		if (source.getAmount() != null) {
			sellerBranchCharges.setAmount(new BigDecimal(source.getAmount()));
		}
		if (source.getOrderAmount() != null) {
			sellerBranchCharges.setOrderAmount(new BigDecimal(source
					.getOrderAmount()));
		}
		if (source.getChargeId() != null) {
			Charge charge = new Charge();
			charge.setId(source.getChargeId());
			sellerBranchCharges.setCharge(charge);
		}
		if (source.getSellerBranchId() != null) {
			SellerBranch sellerBranch = new SellerBranch();
			sellerBranch.setId(source.getSellerBranchId());
			sellerBranchCharges.setSellerBranch(sellerBranch);

		}
		return sellerBranchCharges;
	}

	@Autowired
	public void setSellerBranchChargesFactory(
			final ObjectFactory<SellerBranchCharges> sellerBranchChargesFactory) {
		this.sellerBranchChargesFactory = sellerBranchChargesFactory;
	}

}
