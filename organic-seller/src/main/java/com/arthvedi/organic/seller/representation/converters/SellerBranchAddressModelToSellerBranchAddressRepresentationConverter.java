/**
 *
 */
package com.arthvedi.organic.seller.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.seller.model.SellerBranchAddressModel;
import com.arthvedi.organic.seller.representation.siren.SellerBranchAddressRepresentation;

/**
 * @author varma
 *
 */
@Component("sellerBranchAddressModelToSellerBranchAddressRepresentationConverter")
public class SellerBranchAddressModelToSellerBranchAddressRepresentationConverter extends PropertyCopyingConverter<SellerBranchAddressModel, SellerBranchAddressRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public SellerBranchAddressRepresentation convert(final SellerBranchAddressModel source) {

        SellerBranchAddressRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<SellerBranchAddressRepresentation> factory) {
        super.setFactory(factory);
    }

}
