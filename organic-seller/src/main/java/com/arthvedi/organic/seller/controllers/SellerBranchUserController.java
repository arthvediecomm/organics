package com.arthvedi.organic.seller.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.seller.businessdelegate.context.SellerBranchUserContext;
import com.arthvedi.organic.seller.model.SellerBranchUserModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/sellerbranchuser", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class SellerBranchUserController {

	private IBusinessDelegate<SellerBranchUserModel, SellerBranchUserContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<SellerBranchUserContext> sellerBranchUserContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<SellerBranchUserModel> createSellerBranchUser(@RequestBody final SellerBranchUserModel sellerBranchUserModel) {
		businessDelegate.create(sellerBranchUserModel);
		return new ResponseEntity<SellerBranchUserModel>(sellerBranchUserModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<SellerBranchUserModel> edit(@PathVariable(value = "id") final String sellerBranchUserId,
			@RequestBody final SellerBranchUserModel sellerBranchUserModel) {

		businessDelegate.edit(keyBuilderFactory.getObject().withId(sellerBranchUserId), sellerBranchUserModel);
		return new ResponseEntity<SellerBranchUserModel>(sellerBranchUserModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<SellerBranchUserModel>>> getAll() {
		SellerBranchUserContext sellerBranchUserContext = sellerBranchUserContextFactory.getObject();
		sellerBranchUserContext.setAll("all");
		Collection<SellerBranchUserModel> sellerBranchUserModels = businessDelegate.getCollection(sellerBranchUserContext);
		IModelWrapper<Collection<SellerBranchUserModel>> models = new CollectionModelWrapper<SellerBranchUserModel>(
				SellerBranchUserModel.class, sellerBranchUserModels);
		return new ResponseEntity<IModelWrapper<Collection<SellerBranchUserModel>>>(models, HttpStatus.OK);
	}
	@RequestMapping(method = RequestMethod.GET, value = "/sellerbranchuserlist", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<SellerBranchUserModel>>> getSellerBranchUserList() {
		SellerBranchUserContext sellerBranchUserContext = sellerBranchUserContextFactory.getObject();
		sellerBranchUserContext.setSellerBranchUserList("sellerBranchUserList");
		Collection<SellerBranchUserModel> sellerBranchUserModels = businessDelegate.getCollection(sellerBranchUserContext);
		IModelWrapper<Collection<SellerBranchUserModel>> models = new CollectionModelWrapper<SellerBranchUserModel>(
				SellerBranchUserModel.class, sellerBranchUserModels);
		return new ResponseEntity<IModelWrapper<Collection<SellerBranchUserModel>>>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<SellerBranchUserModel> getSellerBranchUser(@PathVariable(value = "id") final String sellerBranchUserId) {
		SellerBranchUserContext sellerBranchUserContext = sellerBranchUserContextFactory.getObject();

		SellerBranchUserModel model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(sellerBranchUserId),
				sellerBranchUserContext);
		return new ResponseEntity<SellerBranchUserModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "sellerBranchUserBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<SellerBranchUserModel, SellerBranchUserContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setSellerBranchUserObjectFactory(final ObjectFactory<SellerBranchUserContext> sellerBranchUserContextFactory) {
		this.sellerBranchUserContextFactory = sellerBranchUserContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
