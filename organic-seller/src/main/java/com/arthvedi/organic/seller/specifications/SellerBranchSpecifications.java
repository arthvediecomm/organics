package com.arthvedi.organic.seller.specifications;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.arthvedi.organic.seller.domain.SellerBranch;

public class SellerBranchSpecifications {

	public static Specification<SellerBranch> branchesWithZone(String zoneId) {
		return new Specification<SellerBranch>() {
			@Override
			public Predicate toPredicate(Root<SellerBranch> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
			
				return cb.equal(root.join("sellerBranchZones").get("zone").get("id"), zoneId);
			}

		};
	}

	public static Specification<SellerBranch> branchesSupplyingCategory(String categoryId) {
		return new Specification<SellerBranch>() {
			@Override
			public Predicate toPredicate(Root<SellerBranch> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(
						root.join("sellerProducts").join("product").join("productCategories").get("category").get("id"),
						categoryId);
			}
		} ;
	}

	public static Specification<SellerBranch> branchesBySellerId(String sellerId) {
		return new Specification<SellerBranch>() {
			@Override
			public Predicate toPredicate(Root<SellerBranch> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.get("seller").get("id"), sellerId);
			}
		};
	}

	public static Specification<SellerBranch> branchesByStatus(String status) {
		return new Specification<SellerBranch>() {
			@Override
			public Predicate toPredicate(Root<SellerBranch> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.get("branchStatus"), status);
			}
		};

	}
}
