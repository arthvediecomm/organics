package com.arthvedi.organic.seller.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.seller.businessdelegate.context.SellerBranchContext;
import com.arthvedi.organic.seller.domain.SellerBranch;
import com.arthvedi.organic.seller.domain.SellerBranchCharges;
import com.arthvedi.organic.seller.domain.SellerBranchImages;
import com.arthvedi.organic.seller.domain.SellerBranchZone;
import com.arthvedi.organic.seller.model.SellerBranchChargesModel;
import com.arthvedi.organic.seller.model.SellerBranchImagesModel;
import com.arthvedi.organic.seller.model.SellerBranchModel;
import com.arthvedi.organic.seller.model.SellerBranchZoneModel;
import com.arthvedi.organic.seller.service.ISellerBranchChargesService;
import com.arthvedi.organic.seller.service.ISellerBranchImagesService;
import com.arthvedi.organic.seller.service.ISellerBranchService;
import com.arthvedi.organic.seller.service.ISellerBranchZoneService;

@Service
public class SellerBranchBusinessDelegate
		implements
		IBusinessDelegate<SellerBranchModel, SellerBranchContext, IKeyBuilder<String>, String> {

	@Autowired
	private ISellerBranchService sellerBranchService;
	@Autowired
	private ConversionService conversionService;
	@Autowired
	private ISellerBranchZoneService sellerBranchZoneService;
	@Autowired
	private ISellerBranchImagesService sellerBranchImagesService;
	@Autowired
	private ISellerBranchChargesService sellerBranchChargeService;

	@Override
	public SellerBranchModel create(SellerBranchModel model) {
		validateModel(model);
		boolean sellerBranchName = checkSellerBranchExists(model
				.getBranchName());
		if (sellerBranchName) {
			model.setStatusMessage("FAILURE:::  SellerBranch Already Exists");
			return model;
		} else {
			SellerBranch sellerBranch = sellerBranchService
					.create((SellerBranch) conversionService.convert(model,
							forObject(model), valueOf(SellerBranch.class)));
			model = convertToSellerBranchModel(sellerBranch);
			model.setStatusMessage("SUCCESS::: Seller Branch Created Successfully");
		}

		return model;
	}

	private boolean checkSellerBranchExists(String branchName) {
		return sellerBranchService.getSellerBranchByBranchName(branchName);
	}

	private void validateModel(SellerBranchModel model) {
		Validate.notNull(model, "Invalid Input");

	}

	private SellerBranchModel convertToSellerBranchModel(
			SellerBranch sellerBranch) {
		return (SellerBranchModel) conversionService.convert(sellerBranch,
				forObject(sellerBranch), valueOf(SellerBranchModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			SellerBranchContext context) {

	}

	@Override
	public SellerBranchModel edit(IKeyBuilder<String> keyBuilder,
			SellerBranchModel model) {
		SellerBranch sellerBranch = sellerBranchService
				.getSellerBranch(keyBuilder.build().toString());

		if (model.getId() != null
				&& !model.getSellerBranchImagesesModel().isEmpty()) {
			sellerBranch.setSellerBranchImageses(addSellerBranchImages(
					sellerBranch, model.getSellerBranchImagesesModel()));
		}
		if (model.getId() != null
				&& !model.getSellerBranchChargesesModel().isEmpty()) {
			sellerBranch.setSellerBranchChargeses(addSellerBranchCharges(
					sellerBranch, model.getSellerBranchChargesesModel()));
		}

		if (model.getId() != null
				&& !model.getSellerBranchZonesModel().isEmpty()) {
			sellerBranch.setSellerBranchZones(addSellerBranchZones(
					sellerBranch, model.getSellerBranchZonesModel()));
		}
		sellerBranch = sellerBranchService
				.updateSellerBranch((SellerBranch) conversionService.convert(
						model, forObject(model), valueOf(SellerBranch.class)));
		model = convertToSellerBranchModel(sellerBranch);
		model.setStatusMessage("SUCCESS::: Seller Branch Updated Successfully");
		return model;
	}

	@Transactional
	private Set<SellerBranchZone> addSellerBranchZones(
			SellerBranch sellerBranch,
			Set<SellerBranchZoneModel> sellerBranchZonesModel) {
		Set<SellerBranchZone> sellerBranchZones = new HashSet<SellerBranchZone>();
		for (SellerBranchZoneModel sellerBranchZoneModel : sellerBranchZonesModel) {
			sellerBranchZoneModel.setSellerBranchId(sellerBranch.getId());
			SellerBranchZone sellerBranchZone = sellerBranchZoneService
					.create((SellerBranchZone) conversionService.convert(
							sellerBranchZoneModel,
							forObject(sellerBranchZoneModel),
							valueOf(SellerBranchZone.class)));
			sellerBranchZones.add(sellerBranchZone);

		}

		return sellerBranchZones;
	}

	private Set<SellerBranchImages> addSellerBranchImages(
			SellerBranch sellerBranch,
			List<SellerBranchImagesModel> sellerBranchImagesesModel) {
		Set<SellerBranchImages> sellerBranchImages = new HashSet<SellerBranchImages>();
		for (SellerBranchImagesModel sellerBranchImagModel : sellerBranchImagesesModel) {
			sellerBranchImagModel.setSellerBranchId(sellerBranch.getId());
			SellerBranchImages sellerBranchImagess = sellerBranchImagesService
					.create((SellerBranchImages) conversionService.convert(
							sellerBranchImagModel,
							forObject(sellerBranchImagModel),
							valueOf(SellerBranchImages.class)));
			sellerBranchImages.add(sellerBranchImagess);
		}
		return sellerBranchImages;
	}

	private Set<SellerBranchCharges> addSellerBranchCharges(
			SellerBranch sellerBranch,
			List<SellerBranchChargesModel> sellerBranchChargesesModel) {
		Set<SellerBranchCharges> sellerBranchCharges = new HashSet<SellerBranchCharges>();
		for (SellerBranchChargesModel sellerBranchChargesModel : sellerBranchChargesesModel) {
			sellerBranchChargesModel.setSellerBranchId(sellerBranch.getId());
			SellerBranchCharges sellerBranchCharge = sellerBranchChargeService
					.create((SellerBranchCharges) conversionService.convert(
							sellerBranchChargesModel,
							forObject(sellerBranchChargesModel),
							valueOf(SellerBranchCharges.class)));
			sellerBranchCharges.add(sellerBranchCharge);
		}

		return sellerBranchCharges;
	}

	@Override
	public SellerBranchModel getByKey(IKeyBuilder<String> keyBuilder,
			SellerBranchContext context) {
		SellerBranch sellerBranch = sellerBranchService
				.getSellerBranch(keyBuilder.build().toString());
		SellerBranchModel model = conversionService.convert(sellerBranch,
				SellerBranchModel.class);
		return model;
	}

	@Override
	public Collection<SellerBranchModel> getCollection(
			SellerBranchContext context) {
		List<SellerBranch> sellerBranch = new ArrayList<SellerBranch>();
		if (context.getSellerBranchList() != null) {
			sellerBranch = sellerBranchService.getSellerBranchList();
		}
		List<SellerBranchModel> sellerBranchModels = (List<SellerBranchModel>) conversionService
				.convert(
						sellerBranch,
						TypeDescriptor.forObject(sellerBranch),
						TypeDescriptor.collection(List.class,
								TypeDescriptor.valueOf(SellerBranchModel.class)));
		return sellerBranchModels;
	}

	@Override
	public SellerBranchModel edit(IKeyBuilder<String> keyBuilder,
			SellerBranchModel model, SellerBranchContext context) {
		throw new UnsupportedOperationException("Operation not implemented yet");
	}

}
