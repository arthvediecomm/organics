package com.arthvedi.organic.seller.service;

import java.util.List;

import com.arthvedi.organic.seller.domain.Seller;
/*
*@Author varma
*/
public interface ISellerService {
	
	Seller create(Seller seller);

	void deleteSeller(String sellerId);

	Seller getSeller(String sellerId);

	List<Seller> getAll();

	Seller updateSeller(Seller seller);

List<Seller> getSellerList();

boolean checkSellerNameExist(String sellerName);
}
