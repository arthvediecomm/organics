package com.arthvedi.organic.seller.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;

/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SellerBranchZoneContext implements IBusinessDelegateContext {

	private String all;
	private String sellerBranchId;
	private String sellerBranchZoneId;

	public String getSellerBranchZoneId() {
		return sellerBranchZoneId;
	}

	public void setSellerBranchZoneId(String sellerBranchZoneId) {
		this.sellerBranchZoneId = sellerBranchZoneId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

	public String getSellerBranchId() {
		return sellerBranchId;
	}

	public void setSellerBranchId(String sellerBranchId) {
		this.sellerBranchId = sellerBranchId;
	}

}
