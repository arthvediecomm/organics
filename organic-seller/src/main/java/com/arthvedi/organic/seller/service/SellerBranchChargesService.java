package com.arthvedi.organic.seller.service;

import static com.arthvedi.organic.enums.Status.ACTIVE;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.seller.domain.SellerBranchCharges;
import com.arthvedi.organic.seller.repository.SellerBranchChargesRepository;

@Component
public class SellerBranchChargesService implements ISellerBranchChargesService {
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private SellerBranchChargesRepository sellerBranchChargesRepository;

	@Override
	public SellerBranchCharges create(SellerBranchCharges sellerBranchCharges) {
		sellerBranchCharges.setUserCreated("varma");
		sellerBranchCharges.setCreatedDate(new LocalDateTime());
		sellerBranchCharges.setStatus(ACTIVE.name());
		return sellerBranchChargesRepository.save(sellerBranchCharges);
	}

	@Override
	public void deleteSellerBranchCharges(String sellerBranchChargesId) {

	}

	@Override
	public SellerBranchCharges getSellerBranchCharges(
			String sellerBranchChargesId) {
		return sellerBranchChargesRepository.findOne(sellerBranchChargesId);
	}

	@Override
	public List<SellerBranchCharges> getAll() {
		return null;
	}

	@Override
	public SellerBranchCharges updateSellerBranchCharges(
			SellerBranchCharges sellerBranchCharges) {
		SellerBranchCharges sellerBranchChargess = sellerBranchChargesRepository
				.findOne(sellerBranchCharges.getId());
		try {
			nonNullBeanUtils.copyProperties(sellerBranchChargess,
					sellerBranchCharges);
		} catch (IllegalAccessException c) {
			c.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		sellerBranchChargess.setModifiedDate(new LocalDateTime());
		sellerBranchChargess.setUserModified("admin");

		sellerBranchCharges = sellerBranchChargesRepository
				.save(sellerBranchChargess);
		return sellerBranchCharges;
	}

	@Override
	public boolean getSellerBranchChargesByChargesId(String chargeId,
			String sellerBranchId) {
		return sellerBranchChargesRepository.findByChargesId(chargeId,
				sellerBranchId) != null;
	}

	@Override
	public List<SellerBranchCharges> getSellerBranchChargesBySellereBranchId(
			String sellerBranchId) {
		Specification<SellerBranchCharges> sellerBranchChargesBySellerBranch = new Specification<SellerBranchCharges>() {

			@Override
			public Predicate toPredicate(Root<SellerBranchCharges> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {

				return cb.equal(root.get("sellerBranch").get("id"),
						sellerBranchId);
			}
		};
		return sellerBranchChargesRepository
				.findAll(sellerBranchChargesBySellerBranch);
	}

}
