/**
 *
 */
package com.arthvedi.organic.seller.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.seller.model.SellerBranchMarginModel;
import com.arthvedi.organic.seller.representation.siren.SellerBranchMarginRepresentation;

/**
 * @author varma
 *
 */
@Component("sellerBranchMarginModelToSellerBranchMarginRepresentationConverter")
public class SellerBranchMarginModelToSellerBranchMarginRepresentationConverter extends PropertyCopyingConverter<SellerBranchMarginModel, SellerBranchMarginRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public SellerBranchMarginRepresentation convert(final SellerBranchMarginModel source) {

        SellerBranchMarginRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<SellerBranchMarginRepresentation> factory) {
        super.setFactory(factory);
    }

}
