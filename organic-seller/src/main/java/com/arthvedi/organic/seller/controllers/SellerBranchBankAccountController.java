package com.arthvedi.organic.seller.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.seller.businessdelegate.context.SellerBranchBankAccountContext;
import com.arthvedi.organic.seller.model.SellerBranchBankAccountModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/sellerbranchbankaccount", produces = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class SellerBranchBankAccountController {

	private IBusinessDelegate<SellerBranchBankAccountModel, SellerBranchBankAccountContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<SellerBranchBankAccountContext> sellerBranchBankAccountContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<SellerBranchBankAccountModel> createSellerBranchBankAccount(
			@RequestBody final SellerBranchBankAccountModel sellerBranchBankAccountModel) {
		businessDelegate.create(sellerBranchBankAccountModel);
		return new ResponseEntity<SellerBranchBankAccountModel>(
				sellerBranchBankAccountModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<SellerBranchBankAccountModel> edit(
			@PathVariable(value = "id") final String sellerBranchBankAccountId,
			@RequestBody final SellerBranchBankAccountModel sellerBranchBankAccountModel) {

		businessDelegate
				.edit(keyBuilderFactory.getObject().withId(
						sellerBranchBankAccountId),
						sellerBranchBankAccountModel);
		return new ResponseEntity<SellerBranchBankAccountModel>(
				sellerBranchBankAccountModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<SellerBranchBankAccountModel>>> getAll() {
		SellerBranchBankAccountContext sellerBranchBankAccountContext = sellerBranchBankAccountContextFactory
				.getObject();
		sellerBranchBankAccountContext.setAll("all");
		Collection<SellerBranchBankAccountModel> sellerBranchBankAccountModels = businessDelegate
				.getCollection(sellerBranchBankAccountContext);
		IModelWrapper<Collection<SellerBranchBankAccountModel>> models = new CollectionModelWrapper<SellerBranchBankAccountModel>(
				SellerBranchBankAccountModel.class,
				sellerBranchBankAccountModels);
		return new ResponseEntity<IModelWrapper<Collection<SellerBranchBankAccountModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/sellerbranchbankaccountbysellerbranch/{sellerBranchId}", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<SellerBranchBankAccountModel>>> getAll(
			@PathVariable(value = "sellerBranchId") final String sellerBranchId) {
		SellerBranchBankAccountContext sellerBranchBankAccountContext = sellerBranchBankAccountContextFactory
				.getObject();
		sellerBranchBankAccountContext.setSellerBranchId(sellerBranchId);
		Collection<SellerBranchBankAccountModel> sellerBranchBankAccountModels = businessDelegate
				.getCollection(sellerBranchBankAccountContext);
		IModelWrapper<Collection<SellerBranchBankAccountModel>> models = new CollectionModelWrapper<SellerBranchBankAccountModel>(
				SellerBranchBankAccountModel.class,
				sellerBranchBankAccountModels);
		return new ResponseEntity<IModelWrapper<Collection<SellerBranchBankAccountModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<SellerBranchBankAccountModel> getSellerBranchBankAccount(
			@PathVariable(value = "id") final String sellerBranchBankAccountId) {
		SellerBranchBankAccountContext sellerBranchBankAccountContext = sellerBranchBankAccountContextFactory
				.getObject();

		SellerBranchBankAccountModel model = businessDelegate
				.getByKey(
						keyBuilderFactory.getObject().withId(
								sellerBranchBankAccountId),
						sellerBranchBankAccountContext);
		return new ResponseEntity<SellerBranchBankAccountModel>(model,
				HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "sellerBranchBankAccountBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<SellerBranchBankAccountModel, SellerBranchBankAccountContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setSellerBranchBankAccountObjectFactory(
			final ObjectFactory<SellerBranchBankAccountContext> sellerBranchBankAccountContextFactory) {
		this.sellerBranchBankAccountContextFactory = sellerBranchBankAccountContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(
			final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
