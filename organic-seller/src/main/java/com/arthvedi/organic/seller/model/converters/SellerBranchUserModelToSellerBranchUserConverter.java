/**
 *
 */
package com.arthvedi.organic.seller.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.seller.domain.SellerBranchUser;
import com.arthvedi.organic.seller.model.SellerBranchUserModel;

/**
 * @author Jay
 *
 */
@Component("sellerBranchUserModelToSellerBranchUserConverter")
public class SellerBranchUserModelToSellerBranchUserConverter implements Converter<SellerBranchUserModel, SellerBranchUser> {
    @Autowired
    private ObjectFactory<SellerBranchUser> sellerBranchUserFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public SellerBranchUser convert(final SellerBranchUserModel source) {
        SellerBranchUser sellerBranchUser = sellerBranchUserFactory.getObject();
        BeanUtils.copyProperties(source, sellerBranchUser);

        return sellerBranchUser;
    }

    @Autowired
    public void setSellerBranchUserFactory(final ObjectFactory<SellerBranchUser> sellerBranchUserFactory) {
        this.sellerBranchUserFactory = sellerBranchUserFactory;
    }

}
