package com.arthvedi.organic.seller.service;

import java.util.List;

import com.arthvedi.organic.seller.domain.SellerBranchTax;

/*
*@Author varma
*/
public interface ISellerBranchTaxService {

	SellerBranchTax create(SellerBranchTax sellerBranchTax);

	SellerBranchTax getSellerBranchTax(String sellerBranchTaxId);

	SellerBranchTax updateSellerBranchTax(SellerBranchTax sellerBranchTax);

	boolean getSellerBranchTaxByTax(String taxId,String sellerBranchId);

	List<SellerBranchTax> getSellerBranchTaxesList();

	List<SellerBranchTax> getSellerBranchTaxesBySellerBranch(String sellerBranchId);
}
