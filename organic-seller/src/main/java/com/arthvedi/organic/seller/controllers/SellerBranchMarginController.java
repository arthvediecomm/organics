package com.arthvedi.organic.seller.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.seller.businessdelegate.context.SellerBranchMarginContext;
import com.arthvedi.organic.seller.model.SellerBranchMarginModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/sellerbranchmargin", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class SellerBranchMarginController {

	private IBusinessDelegate<SellerBranchMarginModel, SellerBranchMarginContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<SellerBranchMarginContext> sellerBranchMarginContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<SellerBranchMarginModel> createSellerBranchMargin(@RequestBody final SellerBranchMarginModel sellerBranchMarginModel) {
		businessDelegate.create(sellerBranchMarginModel);
		return new ResponseEntity<SellerBranchMarginModel>(sellerBranchMarginModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<SellerBranchMarginModel> edit(@PathVariable(value = "id") final String sellerBranchMarginId,
			@RequestBody final SellerBranchMarginModel sellerBranchMarginModel) {

		businessDelegate.edit(keyBuilderFactory.getObject().withId(sellerBranchMarginId), sellerBranchMarginModel);
		return new ResponseEntity<SellerBranchMarginModel>(sellerBranchMarginModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<SellerBranchMarginModel>>> getAll() {
		SellerBranchMarginContext sellerBranchMarginContext = sellerBranchMarginContextFactory.getObject();
		sellerBranchMarginContext.setAll("all");
		Collection<SellerBranchMarginModel> sellerBranchMarginModels = businessDelegate.getCollection(sellerBranchMarginContext);
		IModelWrapper<Collection<SellerBranchMarginModel>> models = new CollectionModelWrapper<SellerBranchMarginModel>(
				SellerBranchMarginModel.class, sellerBranchMarginModels);
		return new ResponseEntity<IModelWrapper<Collection<SellerBranchMarginModel>>>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<SellerBranchMarginModel> getSellerBranchMargin(@PathVariable(value = "id") final String sellerBranchMarginId) {
		SellerBranchMarginContext sellerBranchMarginContext = sellerBranchMarginContextFactory.getObject();

		SellerBranchMarginModel model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(sellerBranchMarginId),
				sellerBranchMarginContext);
		return new ResponseEntity<SellerBranchMarginModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "sellerBranchMarginBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<SellerBranchMarginModel, SellerBranchMarginContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setSellerBranchMarginObjectFactory(final ObjectFactory<SellerBranchMarginContext> sellerBranchMarginContextFactory) {
		this.sellerBranchMarginContextFactory = sellerBranchMarginContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
