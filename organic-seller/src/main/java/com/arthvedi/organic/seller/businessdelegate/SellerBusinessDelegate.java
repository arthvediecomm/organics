package com.arthvedi.organic.seller.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.seller.businessdelegate.context.SellerContext;
import com.arthvedi.organic.seller.domain.Seller;
import com.arthvedi.organic.seller.model.SellerModel;
import com.arthvedi.organic.seller.service.ISellerBranchService;
import com.arthvedi.organic.seller.service.ISellerService;

/*
 *@Author varma
 */

@Service
public class SellerBusinessDelegate
		implements
		IBusinessDelegate<SellerModel, SellerContext, IKeyBuilder<String>, String> {

	@Autowired
	private ISellerService sellerService;
	@Autowired
	private ConversionService conversionService;
	@Autowired
	private ISellerBranchService sellerBranchService;

	@Override
	public SellerModel create(SellerModel model) {
		validateModel(model);
		boolean sellerName = checkSellerNameExist(model.getSellerName());
		if (sellerName) {
			model.setStatusMessage("FAILURE:::" + model.getSellerName()
					+ "alredy exixts");
			return model;
		} else {
			Seller seller = sellerService.create((Seller) conversionService
					.convert(model, forObject(model), valueOf(Seller.class)));
			model = convertToSellerModel(seller);

			model.setStatusMessage("SUCCESS:::" + model.getSellerName()
					+ " successfully created");

		}

		return model;

	}

	private boolean checkSellerNameExist(String sellerName) {
		return sellerService.checkSellerNameExist(sellerName);
	}

	private void validateModel(SellerModel model) {

	}

	private SellerModel convertToSellerModel(Seller seller) {
		return (SellerModel) conversionService.convert(seller,
				forObject(seller), valueOf(SellerModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, SellerContext context) {

	}

	@Override
	public SellerModel edit(IKeyBuilder<String> keyBuilder, SellerModel model) {
		Seller seller = sellerService.getSeller(keyBuilder.build().toString());

		seller = sellerService.updateSeller((Seller) conversionService.convert(
				model, forObject(model), valueOf(Seller.class)));
		model = convertToSellerModel(seller);
		return model;
	}

	@Override
	public SellerModel getByKey(IKeyBuilder<String> keyBuilder,
			SellerContext context) {
		Seller seller = sellerService.getSeller(keyBuilder.build().toString());
		SellerModel model = conversionService
				.convert(seller, SellerModel.class);
		return model;
	}

	@Override
	public Collection<SellerModel> getCollection(SellerContext context) {
		List<Seller> seller = new ArrayList<Seller>();
		if (context.getAll() != null) {
			seller = sellerService.getAll();
		}
		if (context.getSellerList() != null) {
			seller = sellerService.getSellerList();
		}
		List<SellerModel> sellerModels = (List<SellerModel>) conversionService
				.convert(
						seller,
						TypeDescriptor.forObject(seller),
						TypeDescriptor.collection(List.class,
								TypeDescriptor.valueOf(SellerModel.class)));
		return sellerModels;
	}

	@Override
	public SellerModel edit(IKeyBuilder<String> keyBuilder, SellerModel model,
			SellerContext context) {

		throw new UnsupportedOperationException("Operation not implemented yet");

	}

}
