package com.arthvedi.organic.seller.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.seller.businessdelegate.context.SellerBranchZoneContext;
import com.arthvedi.organic.seller.domain.SellerBranchZone;
import com.arthvedi.organic.seller.model.SellerBranchZoneModel;
import com.arthvedi.organic.seller.service.ISellerBranchZoneService;

/*
*@Author varma
*/

@Service
public class SellerBranchZoneBusinessDelegate
		implements IBusinessDelegate<SellerBranchZoneModel, SellerBranchZoneContext, IKeyBuilder<String>, String> {

	@Autowired
	private ISellerBranchZoneService sellerBranchZoneService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerBranchZoneModel create(SellerBranchZoneModel model) {
		validateModel(model);
		boolean zone = checkZoneExists(model.getZoneId(),model.getSellerBranchId());
		if (zone) {
			model.setStatusMessage("FAILURE::: Zone Already Exists");
			return model;
		} else {
			SellerBranchZone sellerBranchZone = sellerBranchZoneService.create((SellerBranchZone) conversionService
					.convert(model, forObject(model), valueOf(SellerBranchZone.class)));
			model = convertToSellerBranchZoneModel(sellerBranchZone);
			model.setStatusMessage("SUCCESS::: Zone Assigned Successfully");
		}
		return model;
	}

	private SellerBranchZoneModel convertToSellerBranchZoneModel(SellerBranchZone sellerBranchZone) {
		return (SellerBranchZoneModel) conversionService.convert(sellerBranchZone, forObject(sellerBranchZone),
				valueOf(SellerBranchZoneModel.class));
	}

	private boolean checkZoneExists(String zoneId, String sellerBranchId) {
		return sellerBranchZoneService.getSellerBranchZoneByZone(zoneId, sellerBranchId);
	}

	private void validateModel(SellerBranchZoneModel model) {

	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, SellerBranchZoneContext context) {
		throw new UnsupportedOperationException("Operation not implemented yet");
	}

	@Override
	public SellerBranchZoneModel edit(IKeyBuilder<String> keyBuilder, SellerBranchZoneModel model) {
		SellerBranchZone sellerBranchZone = sellerBranchZoneService.getSellerBranchZone(keyBuilder.build().toString());
		sellerBranchZone = sellerBranchZoneService.updateSellerBranchZone(
				(SellerBranchZone) conversionService.convert(model, forObject(model), valueOf(SellerBranchZone.class)));
		model = convertToSellerBranchZoneModel(sellerBranchZone);
		model.setStatusMessage("SUUCESS::: Zone Updated Successfully");
		return model;
	}

	@Override
	public SellerBranchZoneModel getByKey(IKeyBuilder<String> keyBuilder, SellerBranchZoneContext context) {
		SellerBranchZone sellerBranchZone = sellerBranchZoneService.getSellerBranchZone(keyBuilder.build().toString());
		SellerBranchZoneModel model = conversionService.convert(sellerBranchZone, SellerBranchZoneModel.class);
		return model;
	}

	@Override
	public Collection<SellerBranchZoneModel> getCollection(SellerBranchZoneContext context) {
		List<SellerBranchZone> sellerBranchZones = new ArrayList<SellerBranchZone>();
		if(context.getSellerBranchId()!=null){
			sellerBranchZones = sellerBranchZoneService.getSellerBranchZoneBySellerBranch(context.getSellerBranchId());
		}
		List<SellerBranchZoneModel> sellerBranchZoneModels = (List<SellerBranchZoneModel>) conversionService.convert(
				sellerBranchZones, TypeDescriptor.forObject(sellerBranchZones),
				TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(SellerBranchZoneModel.class)));
		return sellerBranchZoneModels;
	}

	@Override
	public SellerBranchZoneModel edit(IKeyBuilder<String> keyBuilder,
			SellerBranchZoneModel model, SellerBranchZoneContext context) {
		throw new UnsupportedOperationException("Operation not implemented yet");
	}

}
