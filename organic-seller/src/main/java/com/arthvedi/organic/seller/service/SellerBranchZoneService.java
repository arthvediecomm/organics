package com.arthvedi.organic.seller.service;

import static com.arthvedi.organic.enums.Status.ACTIVE;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.seller.domain.SellerBranchZone;
import com.arthvedi.organic.seller.repository.SellerBranchZoneRepository;

@Component
@Transactional
public class SellerBranchZoneService implements ISellerBranchZoneService {
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private SellerBranchZoneRepository sellerBranchZoneRepository;

	@Override
	@Transactional
	public SellerBranchZone create(SellerBranchZone sellerBranchZone) {
		sellerBranchZone.setCreatedDate(new LocalDateTime());
		sellerBranchZone.setUserCreated("Dinakar");
		sellerBranchZone.setStatus(ACTIVE.name());
		return sellerBranchZoneRepository.save(sellerBranchZone);
	}

	@Override
	public SellerBranchZone getSellerBranchZone(String sellerBranchZoneId) {
		return sellerBranchZoneRepository.findOne(sellerBranchZoneId);
	}

	@Override
	public SellerBranchZone updateSellerBranchZone(
			SellerBranchZone sellerBranchZone) {
		SellerBranchZone sellerBranchZones = sellerBranchZoneRepository
				.findOne(sellerBranchZone.getId());
		try {
			nonNullBeanUtils
					.copyProperties(sellerBranchZones, sellerBranchZone);
		} catch (IllegalAccessException e) {
			e.printStackTrace();

		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		sellerBranchZones.setModifiedDate(new LocalDateTime());
		sellerBranchZones.setUserModified("Admin");
		sellerBranchZone = sellerBranchZoneRepository.save(sellerBranchZones);
		return sellerBranchZone;
	}

	@Override
	public boolean getSellerBranchZoneByZone(String zoneId,
			String sellerBranchId) {
		return sellerBranchZoneRepository.findSellerBranchZoneByZone(zoneId,
				sellerBranchId) != null;
	}

	@Override
	public List<SellerBranchZone> getSellerBranchZoneBySellerBranch(
			String sellerBranchId) {
		Specification<SellerBranchZone> sellerBranchZone = new Specification<SellerBranchZone>() {

			@Override
			public Predicate toPredicate(Root<SellerBranchZone> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.get("sellerBranch").get("id"),
						sellerBranchId);
			}
		};
		return sellerBranchZoneRepository.findAll(sellerBranchZone);
	}

}
