package com.arthvedi.organic.seller.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;

/*
 *@Author varma
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SellerBranchChargesContext implements IBusinessDelegateContext {

	private String sellerBranchChargesId;
	private String all;
	private String sellerBranchId;

	public String getSellerBranchId() {
		return sellerBranchId;
	}

	public void setSellerBranchId(String sellerBranchId) {
		this.sellerBranchId = sellerBranchId;
	}

	public String getSellerBranchChargesId() {
		return sellerBranchChargesId;
	}

	public void setSellerBranchChargesId(String sellerBranchChargesId) {
		this.sellerBranchChargesId = sellerBranchChargesId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

}
