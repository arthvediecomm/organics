package com.arthvedi.organic.seller.service;

import java.util.List;

import com.arthvedi.organic.seller.domain.SellerBranchZone;

/*
*@Author varma
*/
public interface ISellerBranchZoneService {

	SellerBranchZone create(SellerBranchZone sellerBranchZone);

	SellerBranchZone getSellerBranchZone(String sellerBranchZoneId);

	SellerBranchZone updateSellerBranchZone(SellerBranchZone sellerBranchZone);

	boolean getSellerBranchZoneByZone(String zoneId,String sellerBranchId);

	List<SellerBranchZone> getSellerBranchZoneBySellerBranch(String sellerBranchId);
}
