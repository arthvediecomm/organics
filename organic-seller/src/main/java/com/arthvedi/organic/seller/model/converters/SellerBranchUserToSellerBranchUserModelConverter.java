package com.arthvedi.organic.seller.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.seller.domain.SellerBranchUser;
import com.arthvedi.organic.seller.model.SellerBranchUserModel;

@Component("sellerBranchUserToSellerBranchUserModelConverter")
public class SellerBranchUserToSellerBranchUserModelConverter
        implements Converter<SellerBranchUser, SellerBranchUserModel> {
    @Autowired
    private ObjectFactory<SellerBranchUserModel> sellerBranchUserModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public SellerBranchUserModel convert(final SellerBranchUser source) {
        SellerBranchUserModel sellerBranchUserModel = sellerBranchUserModelFactory.getObject();
        BeanUtils.copyProperties(source, sellerBranchUserModel);

        return sellerBranchUserModel;
    }

    @Autowired
    public void setSellerBranchUserModelFactory(
            final ObjectFactory<SellerBranchUserModel> sellerBranchUserModelFactory) {
        this.sellerBranchUserModelFactory = sellerBranchUserModelFactory;
    }
}
