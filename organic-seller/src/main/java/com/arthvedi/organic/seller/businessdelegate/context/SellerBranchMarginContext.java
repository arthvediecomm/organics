package com.arthvedi.organic.seller.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;
/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SellerBranchMarginContext implements IBusinessDelegateContext {

	private String sellerBranchMarginId;

	private String all;
	
	public String getSellerBranchMarginId() {
		return sellerBranchMarginId;
	}

	public void setSellerBranchMarginId(String sellerBranchMarginId) {
		this.sellerBranchMarginId = sellerBranchMarginId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

}
