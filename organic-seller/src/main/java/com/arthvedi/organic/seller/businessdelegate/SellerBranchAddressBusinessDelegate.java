package com.arthvedi.organic.seller.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.seller.businessdelegate.context.SellerBranchAddressContext;
import com.arthvedi.organic.seller.domain.SellerBranchAddress;
import com.arthvedi.organic.seller.model.SellerBranchAddressModel;
import com.arthvedi.organic.seller.service.ISellerBranchAddressService;

/*
 *@Author varma
 */

@Service
public class SellerBranchAddressBusinessDelegate
		implements
		IBusinessDelegate<SellerBranchAddressModel, SellerBranchAddressContext, IKeyBuilder<String>, String> {

	@Autowired
	private ISellerBranchAddressService sellerBranchAddressService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerBranchAddressModel create(SellerBranchAddressModel model) {
		SellerBranchAddress sellerBranchAddress = sellerBranchAddressService
				.create((SellerBranchAddress) conversionService.convert(model,
						forObject(model), valueOf(SellerBranchAddress.class)));
		model = convertToSellerBranchAddressModel(sellerBranchAddress);
		return model;
	}

	private SellerBranchAddressModel convertToSellerBranchAddressModel(
			SellerBranchAddress sellerBranchAddress) {
		return (SellerBranchAddressModel) conversionService.convert(
				sellerBranchAddress, forObject(sellerBranchAddress),
				valueOf(SellerBranchAddressModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			SellerBranchAddressContext context) {

	}

	@Override
	public SellerBranchAddressModel edit(IKeyBuilder<String> keyBuilder,
			SellerBranchAddressModel model) {
		SellerBranchAddress sellerBranchAddress = sellerBranchAddressService
				.getSellerBranchAddress(keyBuilder.build().toString());

		sellerBranchAddress = sellerBranchAddressService
				.updateSellerBranchAddress((SellerBranchAddress) conversionService
						.convert(model, forObject(model),
								valueOf(SellerBranchAddress.class)));
		model = convertToSellerBranchAddressModel(sellerBranchAddress);
		return model;
	}

	@Override
	public SellerBranchAddressModel getByKey(IKeyBuilder<String> keyBuilder,
			SellerBranchAddressContext context) {
		SellerBranchAddress sellerBranchAddress = sellerBranchAddressService
				.getSellerBranchAddress(keyBuilder.build().toString());
		SellerBranchAddressModel model = conversionService.convert(
				sellerBranchAddress, SellerBranchAddressModel.class);
		return model;
	}

	@Override
	public Collection<SellerBranchAddressModel> getCollection(
			SellerBranchAddressContext context) {
		List<SellerBranchAddress> sellerBranchAddress = new ArrayList<SellerBranchAddress>();
		if (context.getAll() != null) {
			sellerBranchAddress = sellerBranchAddressService.getAll();
		}
		if (context.getAll() != null) {
			sellerBranchAddress = sellerBranchAddressService.getAll();
		}

		List<SellerBranchAddressModel> sellerBranchAddressModels = (List<SellerBranchAddressModel>) conversionService
				.convert(sellerBranchAddress, TypeDescriptor
						.forObject(sellerBranchAddress), TypeDescriptor
						.collection(List.class, TypeDescriptor
								.valueOf(SellerBranchAddressModel.class)));
		return sellerBranchAddressModels;
	}

	@Override
	public SellerBranchAddressModel edit(IKeyBuilder<String> keyBuilder,
			SellerBranchAddressModel model, SellerBranchAddressContext context) {
		throw new UnsupportedOperationException("Operation not implemented yet");
	}

}
