package com.arthvedi.organic.seller.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.seller.domain.SellerImages;
import com.arthvedi.organic.seller.model.SellerImagesModel;

@Component("sellerImagesToSellerImagesModelConverter")
public class SellerImagesToSellerImagesModelConverter implements Converter<SellerImages, SellerImagesModel> {
	@Autowired
	private ObjectFactory<SellerImagesModel> sellerImagesModelFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerImagesModel convert(final SellerImages source) {
		SellerImagesModel sellerImagesModel = sellerImagesModelFactory.getObject();
		BeanUtils.copyProperties(source, sellerImagesModel);
	
		return sellerImagesModel;
	}

	@Autowired
	public void setSellerImagesModelFactory(final ObjectFactory<SellerImagesModel> sellerImagesModelFactory) {
		this.sellerImagesModelFactory = sellerImagesModelFactory;
	}
}
