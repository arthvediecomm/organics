package com.arthvedi.organic.seller.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.seller.domain.SellerBranchTaxableCharges;
import com.arthvedi.organic.seller.model.SellerBranchTaxableChargesModel;

@Component("sellerBranchTaxableChargesToSellerBranchTaxableChargesModelConverter")
public class SellerBranchTaxableChargesToSellerBranchTaxableChargesModelConverter
		implements Converter<SellerBranchTaxableCharges, SellerBranchTaxableChargesModel> {
	@Autowired
	private ObjectFactory<SellerBranchTaxableChargesModel> sellerBranchTaxableChargesModelFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerBranchTaxableChargesModel convert(final SellerBranchTaxableCharges source) {
		SellerBranchTaxableChargesModel sellerBranchTaxableChargesModel = sellerBranchTaxableChargesModelFactory
				.getObject();
		BeanUtils.copyProperties(source, sellerBranchTaxableChargesModel);

		return sellerBranchTaxableChargesModel;
	}

	@Autowired
	public void setSellerBranchTaxableChargesModelFactory(
			final ObjectFactory<SellerBranchTaxableChargesModel> sellerBranchTaxableChargesModelFactory) {
		this.sellerBranchTaxableChargesModelFactory = sellerBranchTaxableChargesModelFactory;
	}
}
