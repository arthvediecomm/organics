/**
 *
 */
package com.arthvedi.organic.seller.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.seller.domain.SellerBranch;
import com.arthvedi.organic.seller.domain.SellerBranchBankAccount;
import com.arthvedi.organic.seller.model.SellerBranchBankAccountModel;

/**
 * @author Jay
 *
 */
@Component("sellerBranchBankAccountModelToSellerBranchBankAccountConverter")
public class SellerBranchBankAccountModelToSellerBranchBankAccountConverter implements Converter<SellerBranchBankAccountModel, SellerBranchBankAccount> {
    @Autowired
    private ObjectFactory<SellerBranchBankAccount> sellerBranchBankAccountFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public SellerBranchBankAccount convert(final SellerBranchBankAccountModel source) {
        SellerBranchBankAccount sellerBranchBankAccount = sellerBranchBankAccountFactory.getObject();
        BeanUtils.copyProperties(source, sellerBranchBankAccount);
        if(source.getSellerBranchId()!=null){
        	SellerBranch sellerBranch = new SellerBranch();
        	sellerBranch.setId(source.getSellerBranchId());
        	sellerBranchBankAccount.setSellerBranch(sellerBranch);
        }
        return sellerBranchBankAccount;
    }

    @Autowired
    public void setSellerBranchBankAccountFactory(final ObjectFactory<SellerBranchBankAccount> sellerBranchBankAccountFactory) {
        this.sellerBranchBankAccountFactory = sellerBranchBankAccountFactory;
    }

}
