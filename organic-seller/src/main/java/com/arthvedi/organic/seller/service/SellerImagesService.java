package com.arthvedi.organic.seller.service;

import java.lang.reflect.InvocationTargetException;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.seller.domain.SellerImages;
import com.arthvedi.organic.seller.repository.SellerImagesRepository;

/*
 *@Author varma
 */
@Component
public class SellerImagesService implements ISellerImagesService {
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private SellerImagesRepository sellerImagesRepository;

	@Override
	public SellerImages create(SellerImages sellerImages) {
		sellerImages.setCreatedDate(new LocalDateTime());
		sellerImages.setUserCreated("ADMIN");
		return sellerImagesRepository.save(sellerImages);
	}

	@Override
	public SellerImages getSellerImages(String sellerImagesId) {
		return sellerImagesRepository.findOne(sellerImagesId);
	}

	@Override
	public SellerImages updateSellerImages(SellerImages sellerImages) {
		SellerImages sellerImag = sellerImagesRepository.findOne(sellerImages
				.getId());
		try {
			nonNullBeanUtils.copyProperties(sellerImag, sellerImages);
		} catch (IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}
		sellerImag.setModifiedDate(new LocalDateTime());
		sellerImag.setUserModified("ADMIN");
		return sellerImagesRepository.save(sellerImag);
	}

}
