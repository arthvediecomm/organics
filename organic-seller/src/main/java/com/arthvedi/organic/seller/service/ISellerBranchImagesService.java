package com.arthvedi.organic.seller.service;

import com.arthvedi.organic.seller.domain.SellerBranchImages;

/*
*@Author varma
*/
public interface ISellerBranchImagesService {

	SellerBranchImages create(SellerBranchImages sellerBranchImages);

	SellerBranchImages getSellerBranchImages(String sellerBranchImagesId);

	SellerBranchImages updateSellerBranchImages(SellerBranchImages sellerBranchImages);
}
