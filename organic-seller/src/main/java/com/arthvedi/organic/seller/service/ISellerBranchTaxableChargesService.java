package com.arthvedi.organic.seller.service;

import com.arthvedi.organic.seller.domain.SellerBranchTaxableCharges;

/*
*@Author varma
*/
public interface ISellerBranchTaxableChargesService {

	SellerBranchTaxableCharges create(SellerBranchTaxableCharges sellerBranchTaxableCharges);

	SellerBranchTaxableCharges getSellerBranchTaxableCharges(String sellerBranchTaxableChargesId);

	SellerBranchTaxableCharges updateSellerBranchTaxableCharges(SellerBranchTaxableCharges sellerBranchTaxableCharges);

	boolean getSellerBranchTaxableChargesByTaxAndCharges(String sellerBranchTaxId, String sellerBranchChargesId);
}
