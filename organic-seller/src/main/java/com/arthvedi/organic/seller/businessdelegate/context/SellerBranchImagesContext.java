package com.arthvedi.organic.seller.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;

/*
 *@Author varma
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SellerBranchImagesContext implements IBusinessDelegateContext {

	private String all;
	private String sellerBranchImagesId;

	public String getAll() {
		return all;
	}

	public String getSellerBranchImagesId() {
		return sellerBranchImagesId;
	}

	public void setAll(final String all) {
		this.all = all;
	}

	public void setSellerBranchImagesId(final String sellerBranchImagesId) {
		this.sellerBranchImagesId = sellerBranchImagesId;
	}

}
