package com.arthvedi.organic.seller.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.seller.businessdelegate.context.SellerBranchChargesContext;
import com.arthvedi.organic.seller.model.SellerBranchChargesModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/sellerbranchcharges", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class SellerBranchChargesController {

	private IBusinessDelegate<SellerBranchChargesModel, SellerBranchChargesContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<SellerBranchChargesContext> sellerBranchChargesContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<SellerBranchChargesModel> createSellerBranchCharges(@RequestBody final SellerBranchChargesModel sellerBranchChargesModel) {
		businessDelegate.create(sellerBranchChargesModel);
		return new ResponseEntity<SellerBranchChargesModel>(sellerBranchChargesModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<SellerBranchChargesModel> edit(@PathVariable(value = "id") final String sellerBranchChargesId,
			@RequestBody final SellerBranchChargesModel sellerBranchChargesModel) {

		businessDelegate.edit(keyBuilderFactory.getObject().withId(sellerBranchChargesId), sellerBranchChargesModel);
		return new ResponseEntity<SellerBranchChargesModel>(sellerBranchChargesModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<SellerBranchChargesModel>>> getAll() {
		SellerBranchChargesContext sellerBranchChargesContext = sellerBranchChargesContextFactory.getObject();
		sellerBranchChargesContext.setAll("all");
		Collection<SellerBranchChargesModel> sellerBranchChargesModels = businessDelegate.getCollection(sellerBranchChargesContext);
		IModelWrapper<Collection<SellerBranchChargesModel>> models = new CollectionModelWrapper<SellerBranchChargesModel>(
				SellerBranchChargesModel.class, sellerBranchChargesModels);
		return new ResponseEntity<IModelWrapper<Collection<SellerBranchChargesModel>>>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<SellerBranchChargesModel> getSellerBranchCharges(@PathVariable(value = "id") final String sellerBranchChargesId) {
		SellerBranchChargesContext sellerBranchChargesContext = sellerBranchChargesContextFactory.getObject();

		SellerBranchChargesModel model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(sellerBranchChargesId),
				sellerBranchChargesContext);
		return new ResponseEntity<SellerBranchChargesModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "sellerBranchChargesBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<SellerBranchChargesModel, SellerBranchChargesContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setSellerBranchChargesObjectFactory(final ObjectFactory<SellerBranchChargesContext> sellerBranchChargesContextFactory) {
		this.sellerBranchChargesContextFactory = sellerBranchChargesContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
