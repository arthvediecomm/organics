package com.arthvedi.organic.seller.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.seller.businessdelegate.context.SellerBranchImagesContext;
import com.arthvedi.organic.seller.domain.SellerBranchImages;
import com.arthvedi.organic.seller.model.SellerBranchImagesModel;
import com.arthvedi.organic.seller.service.ISellerBranchImagesService;

@Service
public class SellerBranchImagesBusinessDelegate
		implements
		IBusinessDelegate<SellerBranchImagesModel, SellerBranchImagesContext, IKeyBuilder<String>, String> {

	@Autowired
	private ISellerBranchImagesService sellerBranchImagesService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerBranchImagesModel create(SellerBranchImagesModel model) {
		SellerBranchImages sellerBranchImages = sellerBranchImagesService
				.create((SellerBranchImages) conversionService.convert(model,
						forObject(model), valueOf(SellerBranchImages.class)));
		model = convertToSellerBranchImagesModel(sellerBranchImages);
		return model;
	}

	private SellerBranchImagesModel convertToSellerBranchImagesModel(
			SellerBranchImages sellerBranchImages) {
		return (SellerBranchImagesModel) conversionService.convert(
				sellerBranchImages, forObject(sellerBranchImages),
				valueOf(SellerBranchImagesModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			SellerBranchImagesContext context) {
		throw new UnsupportedOperationException("Operation not implemented yet");
	}

	@Override
	public SellerBranchImagesModel edit(IKeyBuilder<String> keyBuilder,
			SellerBranchImagesModel model) {
		SellerBranchImages sellerBranchImages = sellerBranchImagesService
				.getSellerBranchImages(keyBuilder.build().toString());
		sellerBranchImages = sellerBranchImagesService
				.updateSellerBranchImages(sellerBranchImages);

		return model;
	}

	@Override
	public SellerBranchImagesModel getByKey(IKeyBuilder<String> keyBuilder,
			SellerBranchImagesContext context) {
		SellerBranchImages sellerBranchImages = sellerBranchImagesService
				.getSellerBranchImages(keyBuilder.build().toString());
		SellerBranchImagesModel model = conversionService.convert(
				sellerBranchImages, SellerBranchImagesModel.class);
		return model;
	}

	@Override
	public Collection<SellerBranchImagesModel> getCollection(
			SellerBranchImagesContext context) {
		List<SellerBranchImages> sellerBranchImages = new ArrayList<SellerBranchImages>();
		List<SellerBranchImagesModel> sellerBranchImagesModels = (List<SellerBranchImagesModel>) conversionService
				.convert(sellerBranchImages, TypeDescriptor
						.forObject(sellerBranchImages), TypeDescriptor
						.collection(List.class, TypeDescriptor
								.valueOf(SellerBranchImagesModel.class)));
		return sellerBranchImagesModels;
	}

	@Override
	public SellerBranchImagesModel edit(IKeyBuilder<String> keyBuilder,
			SellerBranchImagesModel model, SellerBranchImagesContext context) {
		throw new UnsupportedOperationException("Operation not implemented yet");
	}

}
