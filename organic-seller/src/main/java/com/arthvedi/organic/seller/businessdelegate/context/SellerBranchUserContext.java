package com.arthvedi.organic.seller.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;

/*
 *@Author varma
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SellerBranchUserContext implements IBusinessDelegateContext {

	private String sellerBranchUserId;
	private String all;
	private String sellerBranchUserList;
	private String sellerBranchId;

	public String getSellerBranchId() {
		return sellerBranchId;
	}

	public void setSellerBranchId(String sellerBranchId) {
		this.sellerBranchId = sellerBranchId;
	}

	public String getSellerBranchUserList() {
		return sellerBranchUserList;
	}

	public void setSellerBranchUserList(String sellerBranchUserList) {
		this.sellerBranchUserList = sellerBranchUserList;
	}

	public String getSellerBranchUserId() {
		return sellerBranchUserId;
	}

	public void setSellerBranchUserId(String sellerBranchUserId) {
		this.sellerBranchUserId = sellerBranchUserId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

}
