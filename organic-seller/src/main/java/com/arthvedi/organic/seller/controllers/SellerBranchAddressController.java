package com.arthvedi.organic.seller.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.seller.businessdelegate.context.SellerBranchAddressContext;
import com.arthvedi.organic.seller.model.SellerBranchAddressModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/sellerbranchaddress", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class SellerBranchAddressController {

	private IBusinessDelegate<SellerBranchAddressModel, SellerBranchAddressContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<SellerBranchAddressContext> sellerBranchAddressContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<SellerBranchAddressModel> createSellerBranchAddress(
			@RequestBody final SellerBranchAddressModel sellerBranchAddressModel) {
		businessDelegate.create(sellerBranchAddressModel);
		return new ResponseEntity<SellerBranchAddressModel>(sellerBranchAddressModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<SellerBranchAddressModel> edit(@PathVariable(value = "id") final String sellerBranchAddressId,
			@RequestBody final SellerBranchAddressModel sellerBranchAddressModel) {

		businessDelegate.edit(keyBuilderFactory.getObject().withId(sellerBranchAddressId), sellerBranchAddressModel);
		return new ResponseEntity<SellerBranchAddressModel>(sellerBranchAddressModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<SellerBranchAddressModel>>> getAll() {
		SellerBranchAddressContext sellerBranchAddressContext = sellerBranchAddressContextFactory.getObject();
		sellerBranchAddressContext.setAll("all");
		Collection<SellerBranchAddressModel> sellerBranchAddressModels = businessDelegate
				.getCollection(sellerBranchAddressContext);
		IModelWrapper<Collection<SellerBranchAddressModel>> models = new CollectionModelWrapper<SellerBranchAddressModel>(
				SellerBranchAddressModel.class, sellerBranchAddressModels);
		return new ResponseEntity<IModelWrapper<Collection<SellerBranchAddressModel>>>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<SellerBranchAddressModel> getSellerBranchAddress(
			@PathVariable(value = "id") final String sellerBranchAddressId) {
		SellerBranchAddressContext sellerBranchAddressContext = sellerBranchAddressContextFactory.getObject();

		SellerBranchAddressModel model = businessDelegate
				.getByKey(keyBuilderFactory.getObject().withId(sellerBranchAddressId), sellerBranchAddressContext);
		return new ResponseEntity<SellerBranchAddressModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "sellerBranchAddressBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<SellerBranchAddressModel, SellerBranchAddressContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setSellerBranchAddressObjectFactory(
			final ObjectFactory<SellerBranchAddressContext> sellerBranchAddressContextFactory) {
		this.sellerBranchAddressContextFactory = sellerBranchAddressContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
