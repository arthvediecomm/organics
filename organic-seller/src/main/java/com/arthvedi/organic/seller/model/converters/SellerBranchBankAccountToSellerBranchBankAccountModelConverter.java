package com.arthvedi.organic.seller.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.seller.domain.SellerBranchBankAccount;
import com.arthvedi.organic.seller.model.SellerBranchBankAccountModel;

@Component("sellerBranchBankAccountToSellerBranchBankAccountModelConverter")
public class SellerBranchBankAccountToSellerBranchBankAccountModelConverter
        implements Converter<SellerBranchBankAccount, SellerBranchBankAccountModel> {
    @Autowired
    private ObjectFactory<SellerBranchBankAccountModel> sellerBranchBankAccountModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public SellerBranchBankAccountModel convert(final SellerBranchBankAccount source) {
        SellerBranchBankAccountModel sellerBranchBankAccountModel = sellerBranchBankAccountModelFactory.getObject();
        BeanUtils.copyProperties(source, sellerBranchBankAccountModel);

        return sellerBranchBankAccountModel;
    }

    @Autowired
    public void setSellerBranchBankAccountModelFactory(
            final ObjectFactory<SellerBranchBankAccountModel> sellerBranchBankAccountModelFactory) {
        this.sellerBranchBankAccountModelFactory = sellerBranchBankAccountModelFactory;
    }
}
