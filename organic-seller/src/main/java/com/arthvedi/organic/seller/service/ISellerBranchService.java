package com.arthvedi.organic.seller.service;

import java.util.List;

import com.arthvedi.organic.seller.domain.SellerBranch;

public interface ISellerBranchService {

	SellerBranch create(SellerBranch sellerBranch);

	void deleteSellerBranch(String sellerBranchId);

	SellerBranch getSellerBranch(String sellerBranchId);

	List<SellerBranch> getAll();

	SellerBranch updateSellerBranch(SellerBranch sellerBranch);

	boolean getSellerBranchByBranchName(String branchName);

	List<SellerBranch> getSellerBranchList();
}
