/**
 *
 */
package com.arthvedi.organic.seller.representation.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.seller.model.SellerModel;
import com.arthvedi.organic.seller.representation.siren.SellerBranchRepresentation;
import com.arthvedi.organic.seller.representation.siren.SellerImagesRepresentation;
import com.arthvedi.organic.seller.representation.siren.SellerRepresentation;

/**
 * @author varma
 *
 */
@Component("sellerModelToSellerRepresentationConverter")
public class SellerModelToSellerRepresentationConverter extends
		PropertyCopyingConverter<SellerModel, SellerRepresentation> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerRepresentation convert(final SellerModel source) {

		SellerRepresentation target = super.convert(source);
		if (CollectionUtils.isNotEmpty(source.getSellerBranchesModel())) {
			List<SellerBranchRepresentation> convert = (List<SellerBranchRepresentation>) conversionService
					.convert(source.getSellerBranchesModel(), TypeDescriptor
							.forObject(source.getSellerBranchesModel()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									SellerBranchRepresentation.class));

			target.getSellerBranchesRepresentation().addAll(convert);

		}
		if (CollectionUtils.isNotEmpty(source.getSellerImagesesModel())) {
			List<SellerImagesRepresentation> convertr = (List<SellerImagesRepresentation>) conversionService
					.convert(source.getSellerBranchesModel(), TypeDescriptor
							.forObject(source.getSellerImagesesModel()),
							CollectionTypeDescriptor.forType(
									TypeDescriptor.valueOf(List.class),
									SellerImagesRepresentation.class));
			target.getSellerImagesesRepresentation().addAll(convertr);
		}

		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(final ObjectFactory<SellerRepresentation> factory) {
		super.setFactory(factory);
	}

}
