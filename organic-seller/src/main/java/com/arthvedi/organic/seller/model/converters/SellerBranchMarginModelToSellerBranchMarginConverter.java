/**
 *
 */
package com.arthvedi.organic.seller.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.seller.domain.SellerBranchMargin;
import com.arthvedi.organic.seller.model.SellerBranchMarginModel;

/**
 * @author Jay
 *
 */
@Component("sellerBranchMarginModelToSellerBranchMarginConverter")
public class SellerBranchMarginModelToSellerBranchMarginConverter implements Converter<SellerBranchMarginModel, SellerBranchMargin> {
    @Autowired
    private ObjectFactory<SellerBranchMargin> sellerBranchMarginFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public SellerBranchMargin convert(final SellerBranchMarginModel source) {
        SellerBranchMargin sellerBranchMargin = sellerBranchMarginFactory.getObject();
        BeanUtils.copyProperties(source, sellerBranchMargin);

        return sellerBranchMargin;
    }

    @Autowired
    public void setSellerBranchMarginFactory(final ObjectFactory<SellerBranchMargin> sellerBranchMarginFactory) {
        this.sellerBranchMarginFactory = sellerBranchMarginFactory;
    }

}
