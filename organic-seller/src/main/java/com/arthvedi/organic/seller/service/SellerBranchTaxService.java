package com.arthvedi.organic.seller.service;

import static com.arthvedi.organic.enums.Status.ACTIVE;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.seller.domain.SellerBranchTax;
import com.arthvedi.organic.seller.repository.SellerBranchTaxRepository;

@Component
@Transactional
public class SellerBranchTaxService implements ISellerBranchTaxService {
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private SellerBranchTaxRepository sellerBranchTaxRepository;

	@Override
	@Transactional
	public SellerBranchTax create(SellerBranchTax sellerBranchTax) {
		sellerBranchTax.setStatus(ACTIVE.name());
		sellerBranchTax.setCreatedDate(new LocalDateTime());
		sellerBranchTax.setUserCreated("Varma");
		return sellerBranchTaxRepository.save(sellerBranchTax);
	}

	@Override
	public SellerBranchTax getSellerBranchTax(String sellerBranchTaxId) {
		return sellerBranchTaxRepository.findOne(sellerBranchTaxId);
	}

	@Override
	@Transactional
	public SellerBranchTax updateSellerBranchTax(SellerBranchTax sellerBranchTax) {
		SellerBranchTax sbc = sellerBranchTaxRepository.findOne(sellerBranchTax
				.getId());
		try {
			nonNullBeanUtils.copyProperties(sbc, sellerBranchTax);
		} catch (IllegalAccessException e) {
			e.printStackTrace();

		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		sbc.setModifiedDate(new LocalDateTime());
		sbc.setUserModified("Admin");
		sellerBranchTax = sellerBranchTaxRepository.save(sbc);
		return sellerBranchTax;
	}

	@Override
	public boolean getSellerBranchTaxByTax(String taxId, String sellerBranchId) {
		return sellerBranchTaxRepository.findSellerBranchTaxByTax(taxId,
				sellerBranchId) != null;
	}

	@Override
	public List<SellerBranchTax> getSellerBranchTaxesList() {
		List<SellerBranchTax> sellerBranchTaxs = (List<SellerBranchTax>) sellerBranchTaxRepository
				.findAll();
		List<SellerBranchTax> sellerBranchTaxes = new ArrayList<SellerBranchTax>();
		for (SellerBranchTax sbt : sellerBranchTaxs) {
			SellerBranchTax sellerBranchTax = new SellerBranchTax();
			sellerBranchTax.setId(sbt.getId());
			sellerBranchTax.setSellerBranch(sbt.getSellerBranch());
			sellerBranchTax.setTax(sbt.getTax());
			sellerBranchTax.setCreatedDate(sbt.getCreatedDate());
			sellerBranchTax.setModifiedDate(sbt.getModifiedDate());
			sellerBranchTax.setUserCreated(sbt.getUserCreated());
			sellerBranchTax.setUserModified(sbt.getUserModified());
			sellerBranchTax.setStatus(sbt.getStatus());
			sellerBranchTaxes.add(sellerBranchTax);
		}
		return sellerBranchTaxes;
	}

	@Override
	public List<SellerBranchTax> getSellerBranchTaxesBySellerBranch(
			String sellerBranchId) {
		List<SellerBranchTax> sellerBranchTaxs = (List<SellerBranchTax>) sellerBranchTaxRepository
				.findAll(sellerBranchTaxByBranch(sellerBranchId));
		List<SellerBranchTax> sellerBranchTaxes = new ArrayList<SellerBranchTax>();
		for (SellerBranchTax sbt : sellerBranchTaxs) {
			SellerBranchTax sellerBranchTax = new SellerBranchTax();
			sellerBranchTax.setId(sbt.getId());
			sellerBranchTax.setSellerBranch(sbt.getSellerBranch());
			sellerBranchTax.setTax(sbt.getTax());
			sellerBranchTax.setCreatedDate(sbt.getCreatedDate());
			sellerBranchTax.setModifiedDate(sbt.getModifiedDate());
			sellerBranchTax.setUserCreated(sbt.getUserCreated());
			sellerBranchTax.setUserModified(sbt.getUserModified());
			sellerBranchTax.setStatus(sbt.getStatus());
			sellerBranchTaxes.add(sellerBranchTax);
		}
		return sellerBranchTaxes;
	}

	private Sort sellerBranchTaxByBranch(String sellerBranchId) {
		return null;
	}

}
