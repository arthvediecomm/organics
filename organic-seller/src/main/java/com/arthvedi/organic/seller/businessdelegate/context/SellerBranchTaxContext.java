package com.arthvedi.organic.seller.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;

/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SellerBranchTaxContext implements IBusinessDelegateContext {

	private String all;
	private String sellerBranchTaxId;
	private String sellerBranchTaxesList;
	private String sellerBranchId;

	public String getSellerBranchId() {
		return sellerBranchId;
	}

	public void setSellerBranchId(String sellerBranchId) {
		this.sellerBranchId = sellerBranchId;
	}

	public String getSellerBranchTaxId() {
		return sellerBranchTaxId;
	}

	public void setSellerBranchTaxId(String sellerBranchTaxId) {
		this.sellerBranchTaxId = sellerBranchTaxId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

	public String getSellerBranchTaxesList() {
		return sellerBranchTaxesList;
	}

	public void setSellerBranchTaxesList(String sellerBranchTaxesList) {
		this.sellerBranchTaxesList = sellerBranchTaxesList;
	}

}
