package com.arthvedi.organic.seller.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.seller.domain.SellerBranchMargin;
import com.arthvedi.organic.seller.model.SellerBranchMarginModel;

@Component("sellerBranchMarginToSellerBranchMarginModelConverter")
public class SellerBranchMarginToSellerBranchMarginModelConverter
        implements Converter<SellerBranchMargin, SellerBranchMarginModel> {
    @Autowired
    private ObjectFactory<SellerBranchMarginModel> sellerBranchMarginModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public SellerBranchMarginModel convert(final SellerBranchMargin source) {
        SellerBranchMarginModel sellerBranchMarginModel = sellerBranchMarginModelFactory.getObject();
        BeanUtils.copyProperties(source, sellerBranchMarginModel);

        return sellerBranchMarginModel;
    }

    @Autowired
    public void setSellerBranchMarginModelFactory(
            final ObjectFactory<SellerBranchMarginModel> sellerBranchMarginModelFactory) {
        this.sellerBranchMarginModelFactory = sellerBranchMarginModelFactory;
    }
}
