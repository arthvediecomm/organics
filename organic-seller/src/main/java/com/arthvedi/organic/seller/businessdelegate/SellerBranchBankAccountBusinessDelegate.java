package com.arthvedi.organic.seller.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.seller.businessdelegate.context.SellerBranchBankAccountContext;
import com.arthvedi.organic.seller.domain.SellerBranchBankAccount;
import com.arthvedi.organic.seller.model.SellerBranchBankAccountModel;
import com.arthvedi.organic.seller.service.ISellerBranchBankAccountService;

/*
 *@Author varma
 */

@Service
public class SellerBranchBankAccountBusinessDelegate
		implements
		IBusinessDelegate<SellerBranchBankAccountModel, SellerBranchBankAccountContext, IKeyBuilder<String>, String> {

	@Autowired
	private ISellerBranchBankAccountService sellerBranchBankAccountService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerBranchBankAccountModel create(
			SellerBranchBankAccountModel model) {
		SellerBranchBankAccount sellerBranchBankAccount = sellerBranchBankAccountService
				.create((SellerBranchBankAccount) conversionService.convert(
						model, forObject(model),
						valueOf(SellerBranchBankAccount.class)));
		model = convertToSellerBranchBankAccount(sellerBranchBankAccount);
		return model;
	}

	private SellerBranchBankAccountModel convertToSellerBranchBankAccount(
			SellerBranchBankAccount sellerBranchBankAccount) {
		return (SellerBranchBankAccountModel) conversionService.convert(
				sellerBranchBankAccount, forObject(sellerBranchBankAccount),
				valueOf(SellerBranchBankAccountModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			SellerBranchBankAccountContext context) {

	}

	@Override
	public SellerBranchBankAccountModel edit(IKeyBuilder<String> keyBuilder,
			SellerBranchBankAccountModel model) {
		SellerBranchBankAccount sellerBranchBankAccount = sellerBranchBankAccountService
				.getSellerBranchBankAccount(keyBuilder.build().toString());

		sellerBranchBankAccount = sellerBranchBankAccountService
				.updateSellerBranchBankAccount((SellerBranchBankAccount) conversionService
						.convert(model, forObject(model),
								valueOf(SellerBranchBankAccount.class)));
		model = convertToSellerBranchBankAccount(sellerBranchBankAccount);
		return model;
	}

	@Override
	public SellerBranchBankAccountModel getByKey(
			IKeyBuilder<String> keyBuilder,
			SellerBranchBankAccountContext context) {
		SellerBranchBankAccount sellerBranchBankAccount = sellerBranchBankAccountService
				.getSellerBranchBankAccount(keyBuilder.build().toString());
		SellerBranchBankAccountModel model = conversionService.convert(
				sellerBranchBankAccount, SellerBranchBankAccountModel.class);
		return model;
	}

	@Override
	public Collection<SellerBranchBankAccountModel> getCollection(
			SellerBranchBankAccountContext context) {
		List<SellerBranchBankAccount> sellerBranchBankAccount = new ArrayList<SellerBranchBankAccount>();
		if (context.getAll() != null) {
			sellerBranchBankAccount = sellerBranchBankAccountService.getAll();
		}
		if (context.getSellerBranchId() != null) {
			sellerBranchBankAccount = sellerBranchBankAccountService
					.getSellerBranchBankAccountBySellerBranch(context
							.getSellerBranchId());
		}
		List<SellerBranchBankAccountModel> sellerBranchBankAccountModels = (List<SellerBranchBankAccountModel>) conversionService
				.convert(sellerBranchBankAccount, TypeDescriptor
						.forObject(sellerBranchBankAccount), TypeDescriptor
						.collection(List.class, TypeDescriptor
								.valueOf(SellerBranchBankAccountModel.class)));
		return sellerBranchBankAccountModels;
	}

	@Override
	public SellerBranchBankAccountModel edit(IKeyBuilder<String> keyBuilder,
			SellerBranchBankAccountModel model,
			SellerBranchBankAccountContext context) {
		throw new UnsupportedOperationException("Operation not implemented yet");
	}

}
