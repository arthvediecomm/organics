package com.arthvedi.organic.seller.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.seller.businessdelegate.context.SellerBranchContext;
import com.arthvedi.organic.seller.model.SellerBranchModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/sellerbranch", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class SellerBranchController {

	private IBusinessDelegate<SellerBranchModel, SellerBranchContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<SellerBranchContext> sellerBranchContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<SellerBranchModel> createSellerBranch(@RequestBody final SellerBranchModel sellerBranchModel) {
		businessDelegate.create(sellerBranchModel);
		return new ResponseEntity<SellerBranchModel>(sellerBranchModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<SellerBranchModel> edit(@PathVariable(value = "id") final String sellerBranchId,
			@RequestBody final SellerBranchModel sellerBranchModel) {

		businessDelegate.edit(keyBuilderFactory.getObject().withId(sellerBranchId), sellerBranchModel);
		return new ResponseEntity<SellerBranchModel>(sellerBranchModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<SellerBranchModel>>> getAll() {
		SellerBranchContext sellerBranchContext = sellerBranchContextFactory.getObject();
		sellerBranchContext.setAll("all");
		Collection<SellerBranchModel> sellerBranchModels = businessDelegate.getCollection(sellerBranchContext);
		IModelWrapper<Collection<SellerBranchModel>> models = new CollectionModelWrapper<SellerBranchModel>(
				SellerBranchModel.class, sellerBranchModels);
		return new ResponseEntity<IModelWrapper<Collection<SellerBranchModel>>>(models, HttpStatus.OK);
	}
	@RequestMapping(method = RequestMethod.GET, value = "/sellerbranchlist", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<SellerBranchModel>>> getSellerBranchList() {
		SellerBranchContext sellerBranchContext = sellerBranchContextFactory.getObject();
		sellerBranchContext.setSellerBranchList("sellerBranchList");
		Collection<SellerBranchModel> sellerBranchModels = businessDelegate.getCollection(sellerBranchContext);
		IModelWrapper<Collection<SellerBranchModel>> models = new CollectionModelWrapper<SellerBranchModel>(
				SellerBranchModel.class, sellerBranchModels);
		return new ResponseEntity<IModelWrapper<Collection<SellerBranchModel>>>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<SellerBranchModel> getSellerBranch(@PathVariable(value = "id") final String sellerBranchId) {
		SellerBranchContext sellerBranchContext = sellerBranchContextFactory.getObject();

		SellerBranchModel model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(sellerBranchId),
				sellerBranchContext);
		return new ResponseEntity<SellerBranchModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "sellerBranchBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<SellerBranchModel, SellerBranchContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setSellerBranchObjectFactory(final ObjectFactory<SellerBranchContext> sellerBranchContextFactory) {
		this.sellerBranchContextFactory = sellerBranchContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
