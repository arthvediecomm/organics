/**
 *
 */
package com.arthvedi.organic.seller.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.seller.model.SellerBranchUserModel;
import com.arthvedi.organic.seller.representation.siren.SellerBranchUserRepresentation;

/**
 * @author varma
 *
 */
@Component("sellerBranchUserModelToSellerBranchUserRepresentationConverter")
public class SellerBranchUserModelToSellerBranchUserRepresentationConverter extends PropertyCopyingConverter<SellerBranchUserModel, SellerBranchUserRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public SellerBranchUserRepresentation convert(final SellerBranchUserModel source) {

        SellerBranchUserRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<SellerBranchUserRepresentation> factory) {
        super.setFactory(factory);
    }

}
