package com.arthvedi.organic.seller.service;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.seller.domain.SellerBranchMargin;
import com.arthvedi.organic.seller.repository.SellerBranchMarginRepository;

/*
*@Author varma
*/
@Component
public class SellerBranchMarginService implements ISellerBranchMarginService {
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private SellerBranchMarginRepository sellerBranchMarginRepository;

	@Override
	public SellerBranchMargin create(SellerBranchMargin sellerBranchMargin) {
		sellerBranchMargin.setUserCreated("varma");
		sellerBranchMargin.setCreatedDate(new LocalDateTime());
		sellerBranchMargin = sellerBranchMarginRepository.save(sellerBranchMargin);
		return sellerBranchMargin;
	}

	@Override
	public void deleteSellerBranchMargin(String sellerBranchMarginId) {

	}

	@Override
	public SellerBranchMargin getSellerBranchMargin(String sellerBranchMarginId) {
		return sellerBranchMarginRepository.findOne(sellerBranchMarginId);
	}

	@Override
	public List<SellerBranchMargin> getAll() {
		return null;
	}

	@Override
	public SellerBranchMargin updateSellerBranchMargin(SellerBranchMargin sellerBranchMargin) {
		SellerBranchMargin sellerBranchMargins = sellerBranchMarginRepository.findOne(sellerBranchMargin.getId());
		try {
			nonNullBeanUtils.copyProperties(sellerBranchMargins, sellerBranchMargin);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		sellerBranchMargins.setModifiedDate(new LocalDateTime());
		sellerBranchMargins.setUserModified("admin");
		sellerBranchMargin = sellerBranchMarginRepository.save(sellerBranchMargins);
		return sellerBranchMargin;
	}
}
