package com.arthvedi.organic.seller.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.seller.domain.SellerBranchAddress;
import com.arthvedi.organic.seller.model.SellerBranchAddressModel;

@Component("sellerBranchAddressToSellerBranchAddressModelConverter")
public class SellerBranchAddressToSellerBranchAddressModelConverter
        implements Converter<SellerBranchAddress, SellerBranchAddressModel> {
    @Autowired
    private ObjectFactory<SellerBranchAddressModel> sellerBranchAddressModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public SellerBranchAddressModel convert(final SellerBranchAddress source) {
        SellerBranchAddressModel sellerBranchAddressModel = sellerBranchAddressModelFactory.getObject();
        BeanUtils.copyProperties(source, sellerBranchAddressModel);

        return sellerBranchAddressModel;
    }

    @Autowired
    public void setSellerBranchAddressModelFactory(
            final ObjectFactory<SellerBranchAddressModel> sellerBranchAddressModelFactory) {
        this.sellerBranchAddressModelFactory = sellerBranchAddressModelFactory;
    }
}
