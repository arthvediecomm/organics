/**
 *
 */
package com.arthvedi.organic.seller.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.commons.domain.Address;
import com.arthvedi.organic.seller.domain.SellerBranch;
import com.arthvedi.organic.seller.domain.SellerBranchAddress;
import com.arthvedi.organic.seller.model.SellerBranchAddressModel;

/**
 * @author Jay
 *
 */
@Component("sellerBranchAddressModelToSellerBranchAddressConverter")
public class SellerBranchAddressModelToSellerBranchAddressConverter implements
		Converter<SellerBranchAddressModel, SellerBranchAddress> {
	@Autowired
	private ObjectFactory<SellerBranchAddress> sellerBranchAddressFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerBranchAddress convert(final SellerBranchAddressModel source) {
		SellerBranchAddress sellerBranchAddress = new SellerBranchAddress();
		BeanUtils.copyProperties(source, sellerBranchAddress);

		if (source.getCity() != null) {
			Address ads = new Address();
			BeanUtils.copyProperties(source, ads);
			if (source.getAddressId() != null) {
				ads.setId(source.getAddressId());
			}
			sellerBranchAddress.setAddress(ads);
		}

		if (source.getSellerBranchId() != null) {
			SellerBranch sb = new SellerBranch();
			sb.setId(source.getSellerBranchId());
			sellerBranchAddress.setSellerBranch(sb);
		}
		return sellerBranchAddress;
	}

	@Autowired
	public void setSellerBranchAddressFactory(
			final ObjectFactory<SellerBranchAddress> sellerBranchAddressFactory) {
		this.sellerBranchAddressFactory = sellerBranchAddressFactory;
	}

}
