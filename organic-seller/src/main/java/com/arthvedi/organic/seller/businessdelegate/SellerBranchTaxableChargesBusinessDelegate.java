package com.arthvedi.organic.seller.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.seller.businessdelegate.context.SellerBranchTaxableChargesContext;
import com.arthvedi.organic.seller.domain.SellerBranchTaxableCharges;
import com.arthvedi.organic.seller.model.SellerBranchTaxableChargesModel;
import com.arthvedi.organic.seller.service.ISellerBranchTaxableChargesService;

/*
 *@Author varma
 */

@Service
public class SellerBranchTaxableChargesBusinessDelegate
		implements
		IBusinessDelegate<SellerBranchTaxableChargesModel, SellerBranchTaxableChargesContext, IKeyBuilder<String>, String> {

	@Autowired
	private ISellerBranchTaxableChargesService sellerBranchTaxableChargesService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerBranchTaxableChargesModel create(
			SellerBranchTaxableChargesModel model) {
		validateModel(model);
		boolean tax = checkTaxableChargesExists(model.getSellerBranchTaxId(),
				model.getSellerBranchChargesId());
		if (tax) {
			model.setStatusMessage("tax Already Exists");
			return model;
		} else {
			SellerBranchTaxableCharges sellerBranchTaxableCharges = sellerBranchTaxableChargesService
					.create((SellerBranchTaxableCharges) conversionService
							.convert(model, forObject(model),
									valueOf(SellerBranchTaxableCharges.class)));
			model = convertToSellerBranchTaxableChargesModel(sellerBranchTaxableCharges);
		}
		return model;
	}

	private SellerBranchTaxableChargesModel convertToSellerBranchTaxableChargesModel(
			SellerBranchTaxableCharges sellerBranchTaxableCharges) {
		return (SellerBranchTaxableChargesModel) conversionService.convert(
				sellerBranchTaxableCharges,
				forObject(sellerBranchTaxableCharges),
				valueOf(SellerBranchTaxableChargesModel.class));
	}

	private void validateModel(SellerBranchTaxableChargesModel model) {

	}

	private boolean checkTaxableChargesExists(String sellerBranchTaxId,
			String sellerBranchChargesId) {
		return sellerBranchTaxableChargesService
				.getSellerBranchTaxableChargesByTaxAndCharges(
						sellerBranchTaxId, sellerBranchChargesId);
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			SellerBranchTaxableChargesContext context) {
		throw new UnsupportedOperationException("Operation not implemented yet");
	}

	@Override
	public SellerBranchTaxableChargesModel edit(IKeyBuilder<String> keyBuilder,
			SellerBranchTaxableChargesModel model) {
		SellerBranchTaxableCharges sellerBranchTaxableCharges = sellerBranchTaxableChargesService
				.getSellerBranchTaxableCharges(keyBuilder.build().toString());

		sellerBranchTaxableCharges = sellerBranchTaxableChargesService
				.updateSellerBranchTaxableCharges((SellerBranchTaxableCharges) conversionService
						.convert(sellerBranchTaxableCharges,
								forObject(sellerBranchTaxableCharges),
								valueOf(SellerBranchTaxableCharges.class)));

		return model = convertToSellerBranchTaxableChargesModel(sellerBranchTaxableCharges);
	}

	@Override
	public SellerBranchTaxableChargesModel getByKey(
			IKeyBuilder<String> keyBuilder,
			SellerBranchTaxableChargesContext context) {
		SellerBranchTaxableCharges sellerBranchTaxableCharges = sellerBranchTaxableChargesService
				.getSellerBranchTaxableCharges(keyBuilder.build().toString());
		SellerBranchTaxableChargesModel model = conversionService.convert(
				sellerBranchTaxableCharges,
				SellerBranchTaxableChargesModel.class);
		return model;
	}

	@Override
	public Collection<SellerBranchTaxableChargesModel> getCollection(
			SellerBranchTaxableChargesContext context) {
		List<SellerBranchTaxableCharges> sellerBranchTaxableCharges = new ArrayList<SellerBranchTaxableCharges>();
		List<SellerBranchTaxableChargesModel> sellerBranchTaxableChargesModels = (List<SellerBranchTaxableChargesModel>) conversionService
				.convert(
						sellerBranchTaxableCharges,
						TypeDescriptor.forObject(sellerBranchTaxableCharges),
						TypeDescriptor.collection(List.class, TypeDescriptor
								.valueOf(SellerBranchTaxableChargesModel.class)));
		return sellerBranchTaxableChargesModels;
	}

	@Override
	public SellerBranchTaxableChargesModel edit(IKeyBuilder<String> keyBuilder,
			SellerBranchTaxableChargesModel model,
			SellerBranchTaxableChargesContext context) {
		throw new UnsupportedOperationException("Operation not implemented yet");
	}

}
