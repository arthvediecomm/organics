package com.arthvedi.organic.seller.service;

import java.util.List;

import com.arthvedi.organic.seller.domain.SellerBranchCharges;

/*
 *@Author varma
 */
public interface ISellerBranchChargesService {

	SellerBranchCharges create(SellerBranchCharges sellerBranchCharges);

	void deleteSellerBranchCharges(String sellerBranchChargesId);

	SellerBranchCharges getSellerBranchCharges(String sellerBranchChargesId);

	List<SellerBranchCharges> getAll();

	SellerBranchCharges updateSellerBranchCharges(
			SellerBranchCharges sellerBranchCharges);

	boolean getSellerBranchChargesByChargesId(String chargeId,
			String sellerBranchId);

	List<SellerBranchCharges> getSellerBranchChargesBySellereBranchId(
			String sellerBranchId);
}
