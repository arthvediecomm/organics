package com.arthvedi.organic.seller.service;

import static com.arthvedi.organic.enums.Status.ACTIVE;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.commons.domain.Address;
import com.arthvedi.organic.commons.service.AddressService;
import com.arthvedi.organic.commons.service.IAddressService;
import com.arthvedi.organic.seller.domain.SellerBranchAddress;
import com.arthvedi.organic.seller.repository.SellerBranchAddressRepository;

/*
 *@Author varma
 */
@Component
public class SellerBranchAddressService implements ISellerBranchAddressService {
	@Autowired
	NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private IAddressService addressService;
	@Autowired
	private SellerBranchAddressRepository sellerBranchAddressRepository;

	@Override
	public SellerBranchAddress create(SellerBranchAddress sellerBranchAddress) {
		if (sellerBranchAddress.getAddress() != null) {
			Address addres = new Address();
			BeanUtils.copyProperties(sellerBranchAddress.getAddress(), addres);
			addres = addressService.create(addres);
			sellerBranchAddress.setAddress(addres);
		}
		sellerBranchAddress.setCreatedDate(new LocalDateTime());
		sellerBranchAddress.setUserCreated("admin");
		sellerBranchAddress.setStatus(ACTIVE.name());
		sellerBranchAddress = sellerBranchAddressRepository
				.save(sellerBranchAddress);
		return sellerBranchAddress;
	}

	@Override
	public void deleteSellerBranchAddress(String sellerBranchAddressId) {

	}

	@Override
	public SellerBranchAddress getSellerBranchAddress(
			String sellerBranchAddressId) {
		return sellerBranchAddressRepository.findOne(sellerBranchAddressId);
	}

	@Override
	public List<SellerBranchAddress> getAll() {
		return null;
	}

	@Override
	public SellerBranchAddress updateSellerBranchAddress(
			SellerBranchAddress sellerBranchAddress) {
		SellerBranchAddress sellerBranchAddresss = sellerBranchAddressRepository
				.findOne(sellerBranchAddress.getId());
		if (sellerBranchAddress.getAddress().getId() != null) {
			Address address = addressService.updateAddress(sellerBranchAddress
					.getAddress());
			sellerBranchAddresss.setAddress(address);
		}
		try {
			nonNullBeanUtils.copyProperties(sellerBranchAddresss,
					sellerBranchAddress);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		sellerBranchAddresss.setUserModified("admin");
		sellerBranchAddresss.setModifiedDate(new LocalDateTime());
		sellerBranchAddress = sellerBranchAddressRepository
				.save(sellerBranchAddresss);
		return sellerBranchAddress;
	}

}
