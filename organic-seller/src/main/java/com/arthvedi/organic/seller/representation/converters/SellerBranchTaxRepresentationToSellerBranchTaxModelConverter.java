/**
 *
 */
package com.arthvedi.organic.seller.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.seller.model.SellerBranchTaxModel;
import com.arthvedi.organic.seller.representation.siren.SellerBranchTaxRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("sellerBranchTaxRepresentationToSellerBranchTaxModelConverter")
public class SellerBranchTaxRepresentationToSellerBranchTaxModelConverter
		extends PropertyCopyingConverter<SellerBranchTaxRepresentation, SellerBranchTaxModel> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public SellerBranchTaxModel convert(final SellerBranchTaxRepresentation source) {

		SellerBranchTaxModel target = super.convert(source);

		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(final ObjectFactory<SellerBranchTaxModel> factory) {
		super.setFactory(factory);
	}

}
