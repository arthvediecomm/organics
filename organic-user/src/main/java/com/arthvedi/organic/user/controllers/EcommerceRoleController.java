package com.arthvedi.organic.user.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.user.businessdelegate.context.EcommerceRoleContext;
import com.arthvedi.organic.user.model.EcommerceRoleModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/ecommercerole", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class EcommerceRoleController {

	private IBusinessDelegate<EcommerceRoleModel, EcommerceRoleContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<EcommerceRoleContext> ecommerceRoleContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<EcommerceRoleModel> createEcommerceRole(@RequestBody final EcommerceRoleModel ecommerceRoleModel) {
		businessDelegate.create(ecommerceRoleModel);
		return new ResponseEntity<EcommerceRoleModel>(ecommerceRoleModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	public ResponseEntity<EcommerceRoleModel> edit(@PathVariable(value = "id") final String ecommerceRoleId,
			@RequestBody final EcommerceRoleModel ecommerceRoleModel) {

		businessDelegate.edit(keyBuilderFactory.getObject().withId(ecommerceRoleId), ecommerceRoleModel);
		return new ResponseEntity<EcommerceRoleModel>(ecommerceRoleModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<IModelWrapper<Collection<EcommerceRoleModel>>> getAll() {
		EcommerceRoleContext ecommerceRoleContext = ecommerceRoleContextFactory.getObject();
		ecommerceRoleContext.setAll("all");
		Collection<EcommerceRoleModel> ecommerceRoleModels = businessDelegate.getCollection(ecommerceRoleContext);
		IModelWrapper<Collection<EcommerceRoleModel>> models = new CollectionModelWrapper<EcommerceRoleModel>(
				EcommerceRoleModel.class, ecommerceRoleModels);
		return new ResponseEntity<IModelWrapper<Collection<EcommerceRoleModel>>>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<EcommerceRoleModel> getEcommerceRole(@PathVariable(value = "id") final String ecommerceRoleId) {
		EcommerceRoleContext ecommerceRoleContext = ecommerceRoleContextFactory.getObject();

		EcommerceRoleModel model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(ecommerceRoleId),
				ecommerceRoleContext);
		return new ResponseEntity<EcommerceRoleModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "ecommerceRoleBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<EcommerceRoleModel, EcommerceRoleContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setEcommerceRoleObjectFactory(final ObjectFactory<EcommerceRoleContext> ecommerceRoleContextFactory) {
		this.ecommerceRoleContextFactory = ecommerceRoleContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
