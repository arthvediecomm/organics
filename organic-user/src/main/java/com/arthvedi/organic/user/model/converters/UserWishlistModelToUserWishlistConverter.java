/**
 *
 */
package com.arthvedi.organic.user.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.user.domain.UserWishlist;
import com.arthvedi.organic.user.model.UserWishlistModel;

/**
 * @author Jay
 *
 */
@Component("userWishlistModelToUserWishlistConverter")
public class UserWishlistModelToUserWishlistConverter implements Converter<UserWishlistModel, UserWishlist> {
    @Autowired
    private ObjectFactory<UserWishlist> userWishlistFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public UserWishlist convert(final UserWishlistModel source) {
        UserWishlist userWishlist = userWishlistFactory.getObject();
        BeanUtils.copyProperties(source, userWishlist);

        return userWishlist;
    }

    @Autowired
    public void setUserWishlistFactory(final ObjectFactory<UserWishlist> userWishlistFactory) {
        this.userWishlistFactory = userWishlistFactory;
    }

}
