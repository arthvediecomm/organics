/**
 *
 */
package com.arthvedi.organic.user.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.user.model.UserProductRatingModel;
import com.arthvedi.organic.user.representation.siren.UserProductRatingRepresentation;

/**
 * @author varma
 *
 */
@Component("userProductRatingModelToUserProductRatingRepresentationConverter")
public class UserProductRatingModelToUserProductRatingRepresentationConverter
		extends
		PropertyCopyingConverter<UserProductRatingModel, UserProductRatingRepresentation> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public UserProductRatingRepresentation convert(
			final UserProductRatingModel source) {

		UserProductRatingRepresentation target = super.convert(source);

		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(
			final ObjectFactory<UserProductRatingRepresentation> factory) {
		super.setFactory(factory);
	}

}
