package com.arthvedi.organic.user.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.user.businessdelegate.context.EcommerceUserContext;
import com.arthvedi.organic.user.model.EcommerceUserModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/ecommerceuser", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class EcommerceUserController {

	private IBusinessDelegate<EcommerceUserModel, EcommerceUserContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<EcommerceUserContext> ecommerceUserContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<EcommerceUserModel> createEcommerceUser(@RequestBody final EcommerceUserModel ecommerceUserModel) {
		businessDelegate.create(ecommerceUserModel);
		return new ResponseEntity<EcommerceUserModel>(ecommerceUserModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	public ResponseEntity<EcommerceUserModel> edit(@PathVariable(value = "id") final String ecommerceUserId,
			@RequestBody final EcommerceUserModel ecommerceUserModel) {

		businessDelegate.edit(keyBuilderFactory.getObject().withId(ecommerceUserId), ecommerceUserModel);
		return new ResponseEntity<EcommerceUserModel>(ecommerceUserModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<IModelWrapper<Collection<EcommerceUserModel>>> getAll() {
		EcommerceUserContext ecommerceUserContext = ecommerceUserContextFactory.getObject();
		ecommerceUserContext.setAll("all");
		Collection<EcommerceUserModel> ecommerceUserModels = businessDelegate.getCollection(ecommerceUserContext);
		IModelWrapper<Collection<EcommerceUserModel>> models = new CollectionModelWrapper<EcommerceUserModel>(
				EcommerceUserModel.class, ecommerceUserModels);
		return new ResponseEntity<IModelWrapper<Collection<EcommerceUserModel>>>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<EcommerceUserModel> getEcommerceUser(@PathVariable(value = "id") final String ecommerceUserId) {
		EcommerceUserContext ecommerceUserContext = ecommerceUserContextFactory.getObject();

		EcommerceUserModel model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(ecommerceUserId),
				ecommerceUserContext);
		return new ResponseEntity<EcommerceUserModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "ecommerceUserBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<EcommerceUserModel, EcommerceUserContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setEcommerceUserObjectFactory(final ObjectFactory<EcommerceUserContext> ecommerceUserContextFactory) {
		this.ecommerceUserContextFactory = ecommerceUserContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
