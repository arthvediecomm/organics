package com.arthvedi.organic.user.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.user.domain.UserSellerProductRating;
import com.arthvedi.organic.user.model.UserSellerProductRatingModel;

@Component("userSellerProductRatingToUserSellerProductRatingModelConverter")
public class UserSellerProductRatingToUserSellerProductRatingModelConverter
        implements Converter<UserSellerProductRating, UserSellerProductRatingModel> {
    @Autowired
    private ObjectFactory<UserSellerProductRatingModel> userSellerProductRatingModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public UserSellerProductRatingModel convert(final UserSellerProductRating source) {
        UserSellerProductRatingModel userSellerProductRatingModel = userSellerProductRatingModelFactory.getObject();
        BeanUtils.copyProperties(source, userSellerProductRatingModel);

        return userSellerProductRatingModel;
    }

    @Autowired
    public void setUserSellerProductRatingModelFactory(
            final ObjectFactory<UserSellerProductRatingModel> userSellerProductRatingModelFactory) {
        this.userSellerProductRatingModelFactory = userSellerProductRatingModelFactory;
    }
}
