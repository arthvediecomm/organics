package com.arthvedi.organic.user.service;

import java.util.List;

import com.arthvedi.organic.user.domain.EcommerceRole;

public interface IEcommerceRoleService {

	EcommerceRole create(EcommerceRole ecommerceRole);

	void deleteEcommerceRole(String ecommerceRoleId);

	EcommerceRole getEcommerceRole(String ecommerceRoleId);

	List<EcommerceRole> getAll();

	EcommerceRole updateEcommerceRole(EcommerceRole ecommerceRole);
}
