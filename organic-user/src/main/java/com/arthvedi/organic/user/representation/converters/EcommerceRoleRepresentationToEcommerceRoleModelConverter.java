/**
 *
 */
package com.arthvedi.organic.user.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.user.model.EcommerceRoleModel;
import com.arthvedi.organic.user.representation.siren.EcommerceRoleRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("ecommerceRoleRepresentationToEcommerceRoleModelConverter")
public class EcommerceRoleRepresentationToEcommerceRoleModelConverter
		extends
		PropertyCopyingConverter<EcommerceRoleRepresentation, EcommerceRoleModel> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public EcommerceRoleModel convert(final EcommerceRoleRepresentation source) {

		EcommerceRoleModel target = super.convert(source);

		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(final ObjectFactory<EcommerceRoleModel> factory) {
		super.setFactory(factory);
	}

}
