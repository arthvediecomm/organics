package com.arthvedi.organic.user.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.user.businessdelegate.context.UserWishlistContext;
import com.arthvedi.organic.user.model.UserWishlistModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/userwishlist", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class UserWishlistController {

	private IBusinessDelegate<UserWishlistModel, UserWishlistContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<UserWishlistContext> userWishlistContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<UserWishlistModel> createUserWishlist(@RequestBody final UserWishlistModel userWishlistModel) {
		businessDelegate.create(userWishlistModel);
		return new ResponseEntity<UserWishlistModel>(userWishlistModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	public ResponseEntity<UserWishlistModel> edit(@PathVariable(value = "id") final String userWishlistId,
			@RequestBody final UserWishlistModel userWishlistModel) {

		businessDelegate.edit(keyBuilderFactory.getObject().withId(userWishlistId), userWishlistModel);
		return new ResponseEntity<UserWishlistModel>(userWishlistModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<IModelWrapper<Collection<UserWishlistModel>>> getAll() {
		UserWishlistContext userWishlistContext = userWishlistContextFactory.getObject();
		userWishlistContext.setAll("all");
		Collection<UserWishlistModel> userWishlistModels = businessDelegate.getCollection(userWishlistContext);
		IModelWrapper<Collection<UserWishlistModel>> models = new CollectionModelWrapper<UserWishlistModel>(
				UserWishlistModel.class, userWishlistModels);
		return new ResponseEntity<IModelWrapper<Collection<UserWishlistModel>>>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<UserWishlistModel> getUserWishlist(@PathVariable(value = "id") final String userWishlistId) {
		UserWishlistContext userWishlistContext = userWishlistContextFactory.getObject();

		UserWishlistModel model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(userWishlistId),
				userWishlistContext);
		return new ResponseEntity<UserWishlistModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "userWishlistBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<UserWishlistModel, UserWishlistContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setUserWishlistObjectFactory(final ObjectFactory<UserWishlistContext> userWishlistContextFactory) {
		this.userWishlistContextFactory = userWishlistContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
