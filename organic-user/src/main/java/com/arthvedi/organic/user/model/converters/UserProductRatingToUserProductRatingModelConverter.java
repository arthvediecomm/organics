package com.arthvedi.organic.user.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.user.domain.UserProductRating;
import com.arthvedi.organic.user.model.UserProductRatingModel;

@Component("userProductRatingToUserProductRatingModelConverter")
public class UserProductRatingToUserProductRatingModelConverter
        implements Converter<UserProductRating, UserProductRatingModel> {
    @Autowired
    private ObjectFactory<UserProductRatingModel> userProductRatingModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public UserProductRatingModel convert(final UserProductRating source) {
        UserProductRatingModel userProductRatingModel = userProductRatingModelFactory.getObject();
        BeanUtils.copyProperties(source, userProductRatingModel);

        return userProductRatingModel;
    }

    @Autowired
    public void setUserProductRatingModelFactory(
            final ObjectFactory<UserProductRatingModel> userProductRatingModelFactory) {
        this.userProductRatingModelFactory = userProductRatingModelFactory;
    }
}
