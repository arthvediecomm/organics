package com.arthvedi.organic.user.specifications;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.user.domain.EcommerceRole;

@Component
public class EcommerceRoleSpecifications {
	public static Specification<EcommerceRole> byEcommerceUser(String bnvUserId) {
		return new Specification<EcommerceRole>() {

			@Override
			public Predicate toPredicate(Root<EcommerceRole> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.get("ecommerceUser").get("id"), bnvUserId);
			}

		};
	}
}
