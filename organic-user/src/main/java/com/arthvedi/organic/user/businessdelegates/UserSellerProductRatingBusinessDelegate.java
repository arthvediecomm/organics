package com.arthvedi.organic.user.businessdelegates;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.user.businessdelegate.context.UserSellerProductRatingContext;
import com.arthvedi.organic.user.domain.UserSellerProductRating;
import com.arthvedi.organic.user.model.UserSellerProductRatingModel;
import com.arthvedi.organic.user.service.IUserSellerProductRatingService;

/*
 *@Author varma
 */

@Service
public class UserSellerProductRatingBusinessDelegate
		implements
		IBusinessDelegate<UserSellerProductRatingModel, UserSellerProductRatingContext, IKeyBuilder<String>, String> {

	@Autowired
	private IUserSellerProductRatingService userSellerProductRatingService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public UserSellerProductRatingModel create(
			UserSellerProductRatingModel model) {
		UserSellerProductRating userSellerProductRating = userSellerProductRatingService
				.create((UserSellerProductRating) conversionService.convert(
						model, forObject(model),
						valueOf(UserSellerProductRating.class)));
		model = convertToUserSellerProductRatingModel(userSellerProductRating);
		return model;
	}

	private UserSellerProductRatingModel convertToUserSellerProductRatingModel(
			UserSellerProductRating userSellerProductRating) {
		return (UserSellerProductRatingModel) conversionService.convert(
				userSellerProductRating, forObject(userSellerProductRating),
				valueOf(UserSellerProductRatingModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			UserSellerProductRatingContext context) {

	}

	@Override
	public UserSellerProductRatingModel edit(IKeyBuilder<String> keyBuilder,
			UserSellerProductRatingModel model) {
		UserSellerProductRating userSellerProductRating = userSellerProductRatingService
				.getUserSellerProductRating(keyBuilder.build().toString());

		userSellerProductRating = userSellerProductRatingService
				.updateUserSellerProductRating((UserSellerProductRating) conversionService
						.convert(model, forObject(model),
								valueOf(UserSellerProductRating.class)));
		model = convertToUserSellerProductRatingModel(userSellerProductRating);
		return model;
	}

	@Override
	public UserSellerProductRatingModel getByKey(
			IKeyBuilder<String> keyBuilder,
			UserSellerProductRatingContext context) {
		UserSellerProductRating userSellerProductRating = userSellerProductRatingService
				.getUserSellerProductRating(keyBuilder.build().toString());
		UserSellerProductRatingModel model = conversionService.convert(
				userSellerProductRating, UserSellerProductRatingModel.class);
		return model;
	}

	@Override
	public Collection<UserSellerProductRatingModel> getCollection(
			UserSellerProductRatingContext context) {
		List<UserSellerProductRating> userSellerProductRating = new ArrayList<UserSellerProductRating>();

		List<UserSellerProductRatingModel> userSellerProductRatingModels = (List<UserSellerProductRatingModel>) conversionService
				.convert(userSellerProductRating, TypeDescriptor
						.forObject(userSellerProductRating), TypeDescriptor
						.collection(List.class, TypeDescriptor
								.valueOf(UserSellerProductRatingModel.class)));
		return userSellerProductRatingModels;
	}

	@Override
	public UserSellerProductRatingModel edit(IKeyBuilder<String> keyBuilder,
			UserSellerProductRatingModel model,
			UserSellerProductRatingContext context) {
		return null;
	}

}
