package com.arthvedi.organic.user.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.user.businessdelegate.context.UserProductRatingContext;
import com.arthvedi.organic.user.model.UserProductRatingModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/userproductrating", produces = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class UserProductRatingController {

	private IBusinessDelegate<UserProductRatingModel, UserProductRatingContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<UserProductRatingContext> userProductRatingContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<UserProductRatingModel> createUserProductRating(
			@RequestBody final UserProductRatingModel userProductRatingModel) {
		businessDelegate.create(userProductRatingModel);
		return new ResponseEntity<UserProductRatingModel>(
				userProductRatingModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	public ResponseEntity<UserProductRatingModel> edit(
			@PathVariable(value = "id") final String userProductRatingId,
			@RequestBody final UserProductRatingModel userProductRatingModel) {

		businessDelegate.edit(
				keyBuilderFactory.getObject().withId(userProductRatingId),
				userProductRatingModel);
		return new ResponseEntity<UserProductRatingModel>(
				userProductRatingModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<IModelWrapper<Collection<UserProductRatingModel>>> getAll() {
		UserProductRatingContext userProductRatingContext = userProductRatingContextFactory
				.getObject();
		userProductRatingContext.setAll("all");
		Collection<UserProductRatingModel> userProductRatingModels = businessDelegate
				.getCollection(userProductRatingContext);
		IModelWrapper<Collection<UserProductRatingModel>> models = new CollectionModelWrapper<UserProductRatingModel>(
				UserProductRatingModel.class, userProductRatingModels);
		return new ResponseEntity<IModelWrapper<Collection<UserProductRatingModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<UserProductRatingModel> getUserProductRating(
			@PathVariable(value = "id") final String userProductRatingId) {
		UserProductRatingContext userProductRatingContext = userProductRatingContextFactory
				.getObject();

		UserProductRatingModel model = businessDelegate.getByKey(
				keyBuilderFactory.getObject().withId(userProductRatingId),
				userProductRatingContext);
		return new ResponseEntity<UserProductRatingModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "userProductRatingBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<UserProductRatingModel, UserProductRatingContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setUserProductRatingObjectFactory(
			final ObjectFactory<UserProductRatingContext> userProductRatingContextFactory) {
		this.userProductRatingContextFactory = userProductRatingContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(
			final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
