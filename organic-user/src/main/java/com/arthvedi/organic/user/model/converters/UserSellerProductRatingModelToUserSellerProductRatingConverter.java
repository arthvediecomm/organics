/**
 *
 */
package com.arthvedi.organic.user.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.user.domain.UserSellerProductRating;
import com.arthvedi.organic.user.model.UserSellerProductRatingModel;

/**
 * @author Jay
 *
 */
@Component("userSellerProductRatingModelToUserSellerProductRatingConverter")
public class UserSellerProductRatingModelToUserSellerProductRatingConverter implements Converter<UserSellerProductRatingModel, UserSellerProductRating> {
    @Autowired
    private ObjectFactory<UserSellerProductRating> userSellerProductRatingFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public UserSellerProductRating convert(final UserSellerProductRatingModel source) {
        UserSellerProductRating userSellerProductRating = userSellerProductRatingFactory.getObject();
        BeanUtils.copyProperties(source, userSellerProductRating);

        return userSellerProductRating;
    }

    @Autowired
    public void setUserSellerProductRatingFactory(final ObjectFactory<UserSellerProductRating> userSellerProductRatingFactory) {
        this.userSellerProductRatingFactory = userSellerProductRatingFactory;
    }

}
