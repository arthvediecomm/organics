/**
 *
 */
package com.arthvedi.organic.user.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.user.domain.EcommerceUser;
import com.arthvedi.organic.user.model.EcommerceUserModel;

/**
 * @author Jay
 *
 */
@Component("ecommerceUserModelToEcommerceUserConverter")
public class EcommerceUserModelToEcommerceUserConverter implements
		Converter<EcommerceUserModel, EcommerceUser> {
	@Autowired
	private ObjectFactory<EcommerceUser> ecommerceUserFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public EcommerceUser convert(final EcommerceUserModel source) {
		EcommerceUser ecommerceUser = ecommerceUserFactory.getObject();
		BeanUtils.copyProperties(source, ecommerceUser);

		return ecommerceUser;
	}

	@Autowired
	public void setEcommerceUserFactory(
			final ObjectFactory<EcommerceUser> ecommerceUserFactory) {
		this.ecommerceUserFactory = ecommerceUserFactory;
	}

}
