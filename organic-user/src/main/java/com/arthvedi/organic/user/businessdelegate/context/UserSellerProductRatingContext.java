package com.arthvedi.organic.user.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;
/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UserSellerProductRatingContext implements IBusinessDelegateContext {

	private String userSellerProductRatingId;
	private String all;
	
	public String getUserSellerProductRatingId() {
		return userSellerProductRatingId;
	}

	public void setUserSellerProductRatingId(String userSellerProductRatingId) {
		this.userSellerProductRatingId = userSellerProductRatingId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

}
