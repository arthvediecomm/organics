package com.arthvedi.organic.user.service;

import java.util.List;

import com.arthvedi.organic.user.domain.UserProductRating;
/*
*@Author varma
*/
public interface IUserProductRatingService {
	
	UserProductRating create(UserProductRating userProductRating);

	void deleteUserProductRating(String userProductRatingId);

	UserProductRating getUserProductRating(String userProductRatingId);

	List<UserProductRating> getAll();

	UserProductRating updateUserProductRating(UserProductRating userProductRating);
}
