/**
 *
 */
package com.arthvedi.organic.user.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.user.model.UserSellerProductRatingModel;
import com.arthvedi.organic.user.representation.siren.UserSellerProductRatingRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("userSellerProductRatingRepresentationToUserSellerProductRatingModelConverter")
public class UserSellerProductRatingRepresentationToUserSellerProductRatingModelConverter
		extends
		PropertyCopyingConverter<UserSellerProductRatingRepresentation, UserSellerProductRatingModel> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public UserSellerProductRatingModel convert(
			final UserSellerProductRatingRepresentation source) {

		UserSellerProductRatingModel target = super.convert(source);

		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(
			final ObjectFactory<UserSellerProductRatingModel> factory) {
		super.setFactory(factory);
	}

}
