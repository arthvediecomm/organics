package com.arthvedi.organic.user.specifications;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.user.domain.EcommerceUser;
@Component
public class EcommerceUserSpecifications {
	public Specification<EcommerceUser> byUserName(String userName){// email id or phone no
		return new Specification<EcommerceUser>(){
			@Override
			public Predicate toPredicate(Root<EcommerceUser> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.or(cb.equal(root.get("emailId"), userName), cb.equal(root.get("phoneNo"),userName));
			}
		};
	}
}
