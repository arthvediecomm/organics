package com.arthvedi.organic.user.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.user.businessdelegate.context.UserSellerProductRatingContext;
import com.arthvedi.organic.user.model.UserSellerProductRatingModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/usersellerproductrating", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class UserSellerProductRatingController {

	private IBusinessDelegate<UserSellerProductRatingModel, UserSellerProductRatingContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<UserSellerProductRatingContext> userSellerProductRatingContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<UserSellerProductRatingModel> createUserSellerProductRating(@RequestBody final UserSellerProductRatingModel userSellerProductRatingModel) {
		businessDelegate.create(userSellerProductRatingModel);
		return new ResponseEntity<UserSellerProductRatingModel>(userSellerProductRatingModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	public ResponseEntity<UserSellerProductRatingModel> edit(@PathVariable(value = "id") final String userSellerProductRatingId,
			@RequestBody final UserSellerProductRatingModel userSellerProductRatingModel) {

		businessDelegate.edit(keyBuilderFactory.getObject().withId(userSellerProductRatingId), userSellerProductRatingModel);
		return new ResponseEntity<UserSellerProductRatingModel>(userSellerProductRatingModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<IModelWrapper<Collection<UserSellerProductRatingModel>>> getAll() {
		UserSellerProductRatingContext userSellerProductRatingContext = userSellerProductRatingContextFactory.getObject();
		userSellerProductRatingContext.setAll("all");
		Collection<UserSellerProductRatingModel> userSellerProductRatingModels = businessDelegate.getCollection(userSellerProductRatingContext);
		IModelWrapper<Collection<UserSellerProductRatingModel>> models = new CollectionModelWrapper<UserSellerProductRatingModel>(
				UserSellerProductRatingModel.class, userSellerProductRatingModels);
		return new ResponseEntity<IModelWrapper<Collection<UserSellerProductRatingModel>>>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<UserSellerProductRatingModel> getUserSellerProductRating(@PathVariable(value = "id") final String userSellerProductRatingId) {
		UserSellerProductRatingContext userSellerProductRatingContext = userSellerProductRatingContextFactory.getObject();

		UserSellerProductRatingModel model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(userSellerProductRatingId),
				userSellerProductRatingContext);
		return new ResponseEntity<UserSellerProductRatingModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "userSellerProductRatingBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<UserSellerProductRatingModel, UserSellerProductRatingContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setUserSellerProductRatingObjectFactory(final ObjectFactory<UserSellerProductRatingContext> userSellerProductRatingContextFactory) {
		this.userSellerProductRatingContextFactory = userSellerProductRatingContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
