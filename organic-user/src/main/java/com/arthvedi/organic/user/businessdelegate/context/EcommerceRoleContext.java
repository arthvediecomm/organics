package com.arthvedi.organic.user.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;
/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class EcommerceRoleContext implements IBusinessDelegateContext {

	private String ecommerceRoleId;
	private String all;
	
	public String getEcommerceRoleId() {
		return ecommerceRoleId;
	}

	public void setEcommerceRoleId(String ecommerceRoleId) {
		this.ecommerceRoleId = ecommerceRoleId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

}
