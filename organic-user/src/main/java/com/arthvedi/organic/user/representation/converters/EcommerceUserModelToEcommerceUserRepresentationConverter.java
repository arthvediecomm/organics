/**
 *
 */
package com.arthvedi.organic.user.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.user.model.EcommerceUserModel;
import com.arthvedi.organic.user.representation.siren.EcommerceUserRepresentation;

/**
 * @author varma
 *
 */
@Component("ecommerceUserModelToEcommerceUserRepresentationConverter")
public class EcommerceUserModelToEcommerceUserRepresentationConverter
		extends
		PropertyCopyingConverter<EcommerceUserModel, EcommerceUserRepresentation> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public EcommerceUserRepresentation convert(final EcommerceUserModel source) {

		EcommerceUserRepresentation target = super.convert(source);

		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(
			final ObjectFactory<EcommerceUserRepresentation> factory) {
		super.setFactory(factory);
	}

}
