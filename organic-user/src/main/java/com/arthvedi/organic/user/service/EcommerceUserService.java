package com.arthvedi.organic.user.service;

import static com.arthvedi.organic.enums.Status.ACTIVE;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.user.domain.EcommerceUser;
import com.arthvedi.organic.user.repository.EcommerceUserRepository;

/*
 *@Author varma
 */
@Component
public class EcommerceUserService implements IEcommerceUserService {
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private EcommerceUserRepository ecommerceUserRepository;

	@Override
	public EcommerceUser create(EcommerceUser ecommerceUser) {
		ecommerceUser.setCreatedDate(new LocalDateTime());
		ecommerceUser.setUserCreated("varma");
		ecommerceUser.setStatus(ACTIVE.name());
		ecommerceUser = ecommerceUserRepository.save(ecommerceUser);
		return ecommerceUser;
	}

	@Override
	public void deleteEcommerceUser(String ecommerceUserId) {

	}

	@Override
	public EcommerceUser getEcommerceUser(String ecommerceUserId) {
		return ecommerceUserRepository.findOne(ecommerceUserId);
	}

	@Override
	public List<EcommerceUser> getAll() {
		return null;
	}

	@Override
	public EcommerceUser updateEcommerceUser(EcommerceUser ecommerceUser) {
		EcommerceUser ecommerceUsers = ecommerceUserRepository
				.findOne(ecommerceUser.getId());
		try {
			nonNullBeanUtils.copyProperties(ecommerceUsers, ecommerceUser);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		ecommerceUsers.setUserModified("admin");
		ecommerceUsers.setModifiedDate(new LocalDateTime());
		ecommerceUser = ecommerceUserRepository.save(ecommerceUsers);
		return ecommerceUser;
	}

}
