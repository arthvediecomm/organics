package com.arthvedi.organic.user.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;
/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class EcommerceUserContext implements IBusinessDelegateContext {

	private String ecommerceUserId;
	private String all;
	
	public String getEcommerceUserId() {
		return ecommerceUserId;
	}

	public void setEcommerceUserId(String ecommerceUserId) {
		this.ecommerceUserId = ecommerceUserId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

}
