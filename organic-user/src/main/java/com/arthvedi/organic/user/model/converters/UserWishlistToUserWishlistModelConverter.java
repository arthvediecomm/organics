package com.arthvedi.organic.user.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.user.domain.UserWishlist;
import com.arthvedi.organic.user.model.UserWishlistModel;

@Component("userWishlistToUserWishlistModelConverter")
public class UserWishlistToUserWishlistModelConverter
        implements Converter<UserWishlist, UserWishlistModel> {
    @Autowired
    private ObjectFactory<UserWishlistModel> userWishlistModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public UserWishlistModel convert(final UserWishlist source) {
        UserWishlistModel userWishlistModel = userWishlistModelFactory.getObject();
        BeanUtils.copyProperties(source, userWishlistModel);

        return userWishlistModel;
    }

    @Autowired
    public void setUserWishlistModelFactory(
            final ObjectFactory<UserWishlistModel> userWishlistModelFactory) {
        this.userWishlistModelFactory = userWishlistModelFactory;
    }
}
