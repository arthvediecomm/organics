package com.arthvedi.organic.user.businessdelegates;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.user.businessdelegate.context.EcommerceUserContext;
import com.arthvedi.organic.user.domain.EcommerceUser;
import com.arthvedi.organic.user.model.EcommerceUserModel;
import com.arthvedi.organic.user.service.IEcommerceUserService;

/*
 *@Author varma
 */

@Service
public class EcommerceUserBusinessDelegate
		implements
		IBusinessDelegate<EcommerceUserModel, EcommerceUserContext, IKeyBuilder<String>, String> {

	@Autowired
	private IEcommerceUserService ecommerceUserService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public EcommerceUserModel create(EcommerceUserModel model) {
		EcommerceUser ecommerceUser = ecommerceUserService
				.create((EcommerceUser) conversionService.convert(model,
						forObject(model), valueOf(EcommerceUser.class)));
		model = convertToEcommerceUserModel(ecommerceUser);
		return model;
	}

	private EcommerceUserModel convertToEcommerceUserModel(
			EcommerceUser ecommerceUser) {
		return (EcommerceUserModel) conversionService.convert(ecommerceUser,
				forObject(ecommerceUser), valueOf(EcommerceUserModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			EcommerceUserContext context) {

	}

	@Override
	public EcommerceUserModel edit(IKeyBuilder<String> keyBuilder,
			EcommerceUserModel model) {
		EcommerceUser ecommerceUser = ecommerceUserService
				.getEcommerceUser(keyBuilder.build().toString());

		ecommerceUser = ecommerceUserService
				.updateEcommerceUser((EcommerceUser) conversionService.convert(
						model, forObject(model), valueOf(EcommerceUser.class)));
		model = convertToEcommerceUserModel(ecommerceUser);
		return model;
	}

	@Override
	public EcommerceUserModel getByKey(IKeyBuilder<String> keyBuilder,
			EcommerceUserContext context) {
		EcommerceUser ecommerceUser = ecommerceUserService
				.getEcommerceUser(keyBuilder.build().toString());
		EcommerceUserModel model = conversionService.convert(ecommerceUser,
				EcommerceUserModel.class);
		return model;
	}

	@Override
	public Collection<EcommerceUserModel> getCollection(
			EcommerceUserContext context) {
		List<EcommerceUser> ecommerceUser = new ArrayList<EcommerceUser>();

		List<EcommerceUserModel> ecommerceUserModels = (List<EcommerceUserModel>) conversionService
				.convert(ecommerceUser,
						TypeDescriptor.forObject(ecommerceUser), TypeDescriptor
								.collection(List.class, TypeDescriptor
										.valueOf(EcommerceUserModel.class)));
		return ecommerceUserModels;
	}

	@Override
	public EcommerceUserModel edit(IKeyBuilder<String> keyBuilder,
			EcommerceUserModel model, EcommerceUserContext context) {
		// TODO Auto-generated method stub
		return null;
	}

}
