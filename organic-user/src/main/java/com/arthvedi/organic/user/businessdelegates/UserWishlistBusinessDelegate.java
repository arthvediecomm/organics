package com.arthvedi.organic.user.businessdelegates;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.user.businessdelegate.context.UserWishlistContext;
import com.arthvedi.organic.user.domain.UserWishlist;
import com.arthvedi.organic.user.model.UserWishlistModel;
import com.arthvedi.organic.user.service.IUserWishlistService;

/*
 *@Author varma
 */

@Service
public class UserWishlistBusinessDelegate
		implements
		IBusinessDelegate<UserWishlistModel, UserWishlistContext, IKeyBuilder<String>, String> {

	@Autowired
	private IUserWishlistService userWishlistService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public UserWishlistModel create(UserWishlistModel model) {
		UserWishlist userWishlist = userWishlistService
				.create((UserWishlist) conversionService.convert(model,
						forObject(model), valueOf(UserWishlist.class)));
		model = convertToUserWishlistModel(userWishlist);
		return model;
	}

	private UserWishlistModel convertToUserWishlistModel(
			UserWishlist userWishlist) {
		return (UserWishlistModel) conversionService.convert(userWishlist,
				forObject(userWishlist), valueOf(UserWishlistModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			UserWishlistContext context) {

	}

	@Override
	public UserWishlistModel edit(IKeyBuilder<String> keyBuilder,
			UserWishlistModel model) {
		UserWishlist userWishlist = userWishlistService
				.getUserWishlist(keyBuilder.build().toString());

		userWishlist = userWishlistService
				.updateUserWishlist((UserWishlist) conversionService.convert(
						model, forObject(model), valueOf(UserWishlist.class)));
		model = convertToUserWishlistModel(userWishlist);
		return model;
	}

	@Override
	public UserWishlistModel getByKey(IKeyBuilder<String> keyBuilder,
			UserWishlistContext context) {
		UserWishlist userWishlist = userWishlistService
				.getUserWishlist(keyBuilder.build().toString());
		UserWishlistModel model = conversionService.convert(userWishlist,
				UserWishlistModel.class);
		return model;
	}

	@Override
	public Collection<UserWishlistModel> getCollection(
			UserWishlistContext context) {
		List<UserWishlist> userWishlist = new ArrayList<UserWishlist>();

		List<UserWishlistModel> userWishlistModels = (List<UserWishlistModel>) conversionService
				.convert(
						userWishlist,
						TypeDescriptor.forObject(userWishlist),
						TypeDescriptor.collection(List.class,
								TypeDescriptor.valueOf(UserWishlistModel.class)));
		return userWishlistModels;
	}

	@Override
	public UserWishlistModel edit(IKeyBuilder<String> keyBuilder,
			UserWishlistModel model, UserWishlistContext context) {
		// TODO Auto-generated method stub
		return null;
	}

}
