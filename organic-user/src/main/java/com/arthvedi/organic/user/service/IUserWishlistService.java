package com.arthvedi.organic.user.service;

import java.util.List;

import com.arthvedi.organic.user.domain.UserWishlist;
/*
*@Author varma
*/
public interface IUserWishlistService {
	
	UserWishlist create(UserWishlist userWishlist);

	void deleteUserWishlist(String userWishlistId);

	UserWishlist getUserWishlist(String userWishlistId);

	List<UserWishlist> getAll();

	UserWishlist updateUserWishlist(UserWishlist userWishlist);
}
