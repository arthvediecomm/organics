package com.arthvedi.organic.user.businessdelegates;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.user.businessdelegate.context.UserProductRatingContext;
import com.arthvedi.organic.user.domain.UserProductRating;
import com.arthvedi.organic.user.model.UserProductRatingModel;
import com.arthvedi.organic.user.service.IUserProductRatingService;

/*
 *@Author varma
 */

@Service
public class UserProductRatingBusinessDelegate
		implements
		IBusinessDelegate<UserProductRatingModel, UserProductRatingContext, IKeyBuilder<String>, String> {

	@Autowired
	private IUserProductRatingService userProductRatingService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public UserProductRatingModel create(UserProductRatingModel model) {
		UserProductRating userProductRating = userProductRatingService
				.create((UserProductRating) conversionService.convert(model,
						forObject(model), valueOf(UserProductRating.class)));
		model = convertToUserProductRating(userProductRating);

		return model;
	}

	private UserProductRatingModel convertToUserProductRating(
			UserProductRating userProductRating) {
		return (UserProductRatingModel) conversionService.convert(
				userProductRating, forObject(userProductRating),
				valueOf(UserProductRatingModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			UserProductRatingContext context) {

	}

	@Override
	public UserProductRatingModel edit(IKeyBuilder<String> keyBuilder,
			UserProductRatingModel model) {
		UserProductRating userProductRating = userProductRatingService
				.getUserProductRating(keyBuilder.build().toString());

		userProductRating = userProductRatingService
				.updateUserProductRating((UserProductRating) conversionService
						.convert(model, forObject(model),
								valueOf(UserProductRating.class)));
		model = convertToUserProductRating(userProductRating);
		return model;
	}

	@Override
	public UserProductRatingModel getByKey(IKeyBuilder<String> keyBuilder,
			UserProductRatingContext context) {
		UserProductRating userProductRating = userProductRatingService
				.getUserProductRating(keyBuilder.build().toString());
		UserProductRatingModel model = conversionService.convert(
				userProductRating, UserProductRatingModel.class);
		return model;
	}

	@Override
	public Collection<UserProductRatingModel> getCollection(
			UserProductRatingContext context) {
		List<UserProductRating> userProductRating = new ArrayList<UserProductRating>();

		List<UserProductRatingModel> userProductRatingModels = (List<UserProductRatingModel>) conversionService
				.convert(userProductRating, TypeDescriptor
						.forObject(userProductRating), TypeDescriptor
						.collection(List.class, TypeDescriptor
								.valueOf(UserProductRatingModel.class)));
		return userProductRatingModels;
	}

	@Override
	public UserProductRatingModel edit(IKeyBuilder<String> keyBuilder,
			UserProductRatingModel model, UserProductRatingContext context) {
		// TODO Auto-generated method stub
		return null;
	}

}
