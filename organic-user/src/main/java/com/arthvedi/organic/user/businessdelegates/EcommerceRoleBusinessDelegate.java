package com.arthvedi.organic.user.businessdelegates;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.user.businessdelegate.context.EcommerceRoleContext;
import com.arthvedi.organic.user.domain.EcommerceRole;
import com.arthvedi.organic.user.model.EcommerceRoleModel;
import com.arthvedi.organic.user.service.IEcommerceRoleService;

/*
 *@Author varma
 */

@Service
public class EcommerceRoleBusinessDelegate
		implements
		IBusinessDelegate<EcommerceRoleModel, EcommerceRoleContext, IKeyBuilder<String>, String> {

	@Autowired
	private IEcommerceRoleService ecommerceRoleService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public EcommerceRoleModel create(EcommerceRoleModel model) {
		EcommerceRole ecommerceRole = ecommerceRoleService
				.create((EcommerceRole) conversionService.convert(model,
						forObject(model), valueOf(EcommerceRole.class)));
		model = convertToEcommerceRoleModel(ecommerceRole);
		return model;
	}

	private EcommerceRoleModel convertToEcommerceRoleModel(
			EcommerceRole ecommerceRole) {
		return (EcommerceRoleModel) conversionService.convert(ecommerceRole,
				forObject(ecommerceRole), valueOf(EcommerceRoleModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder,
			EcommerceRoleContext context) {

	}

	@Override
	public EcommerceRoleModel edit(IKeyBuilder<String> keyBuilder,
			EcommerceRoleModel model) {
		EcommerceRole ecommerceRole = ecommerceRoleService
				.getEcommerceRole(keyBuilder.build().toString());

		ecommerceRole = ecommerceRoleService
				.updateEcommerceRole((EcommerceRole) conversionService.convert(
						model, forObject(model), valueOf(EcommerceRole.class)));
		model = convertToEcommerceRoleModel(ecommerceRole);
		return model;
	}

	@Override
	public EcommerceRoleModel getByKey(IKeyBuilder<String> keyBuilder,
			EcommerceRoleContext context) {
		EcommerceRole ecommerceRole = ecommerceRoleService
				.getEcommerceRole(keyBuilder.build().toString());
		EcommerceRoleModel model = conversionService.convert(ecommerceRole,
				EcommerceRoleModel.class);
		return model;
	}

	@Override
	public Collection<EcommerceRoleModel> getCollection(
			EcommerceRoleContext context) {
		List<EcommerceRole> ecommerceRole = new ArrayList<EcommerceRole>();

		List<EcommerceRoleModel> ecommerceRoleModels = (List<EcommerceRoleModel>) conversionService
				.convert(ecommerceRole,
						TypeDescriptor.forObject(ecommerceRole), TypeDescriptor
								.collection(List.class, TypeDescriptor
										.valueOf(EcommerceRoleModel.class)));
		return ecommerceRoleModels;
	}

	@Override
	public EcommerceRoleModel edit(IKeyBuilder<String> keyBuilder,
			EcommerceRoleModel model, EcommerceRoleContext context) {
		return null;
	}

}
