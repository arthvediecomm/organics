/**
 *
 */
package com.arthvedi.organic.user.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.user.model.UserWishlistModel;
import com.arthvedi.organic.user.representation.siren.UserWishlistRepresentation;

@Component("userWishlistModelToUserWishlistRepresentationConverter")
public class UserWishlistModelToUserWishlistRepresentationConverter extends
		PropertyCopyingConverter<UserWishlistModel, UserWishlistRepresentation> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public UserWishlistRepresentation convert(final UserWishlistModel source) {

		UserWishlistRepresentation target = super.convert(source);

		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(
			final ObjectFactory<UserWishlistRepresentation> factory) {
		super.setFactory(factory);
	}

}
