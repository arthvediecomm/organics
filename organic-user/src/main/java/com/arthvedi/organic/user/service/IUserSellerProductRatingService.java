package com.arthvedi.organic.user.service;

import java.util.List;

import com.arthvedi.organic.user.domain.UserSellerProductRating;
/*
*@Author varma
*/
public interface IUserSellerProductRatingService {
	
	UserSellerProductRating create(UserSellerProductRating userSellerProductRating);

	void deleteUserSellerProductRating(String userSellerProductRatingId);

	UserSellerProductRating getUserSellerProductRating(String userSellerProductRatingId);

	List<UserSellerProductRating> getAll();

	UserSellerProductRating updateUserSellerProductRating(UserSellerProductRating userSellerProductRating);
}
