package com.arthvedi.organic.user.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;
/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UserProductRatingContext implements IBusinessDelegateContext {

	private String userProductRatingId;
	private String all;
	
	public String getUserProductRatingId() {
		return userProductRatingId;
	}

	public void setUserProductRatingId(String userProductRatingId) {
		this.userProductRatingId = userProductRatingId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

}
