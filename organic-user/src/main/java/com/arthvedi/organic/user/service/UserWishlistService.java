package com.arthvedi.organic.user.service;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.user.domain.UserWishlist;
import com.arthvedi.organic.user.repository.UserWishlistRepository;

@Component
public class UserWishlistService implements IUserWishlistService {
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private UserWishlistRepository userWishlistRepository;

	@Override
	public UserWishlist create(UserWishlist userWishlist) {
		userWishlist.setUserCreated("varma");
		userWishlist.setCreatedDate(new LocalDateTime());
		userWishlist = userWishlistRepository.save(userWishlist);
		return userWishlist;
	}

	@Override
	public void deleteUserWishlist(String userWishlistId) {

	}

	@Override
	public UserWishlist getUserWishlist(String userWishlistId) {
		return userWishlistRepository.findOne(userWishlistId);
	}

	@Override
	public List<UserWishlist> getAll() {
		return null;
	}

	@Override
	public UserWishlist updateUserWishlist(UserWishlist userWishlist) {
		UserWishlist userWishlists = userWishlistRepository
				.findOne(userWishlist.getId());
		try {
			nonNullBeanUtils.copyProperties(userWishlists, userWishlist);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

		userWishlists.setUserModified("admin");
		userWishlists.setModifiedDate(new LocalDateTime());
		userWishlist = userWishlistRepository.save(userWishlists);

		return userWishlist;
	}

}
