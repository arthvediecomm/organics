package com.arthvedi.organic.user.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;
/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UserWishlistContext implements IBusinessDelegateContext {

	private String userWishlistId;
	private String all;
	
	public String getUserWishlistId() {
		return userWishlistId;
	}

	public void setUserWishlistId(String userWishlistId) {
		this.userWishlistId = userWishlistId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

}
