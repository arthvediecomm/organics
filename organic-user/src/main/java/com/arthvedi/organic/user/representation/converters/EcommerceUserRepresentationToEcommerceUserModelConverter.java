/**
 *
 */
package com.arthvedi.organic.user.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.user.model.EcommerceUserModel;
import com.arthvedi.organic.user.representation.siren.EcommerceUserRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("ecommerceUserRepresentationToEcommerceUserModelConverter")
public class EcommerceUserRepresentationToEcommerceUserModelConverter
		extends
		PropertyCopyingConverter<EcommerceUserRepresentation, EcommerceUserModel> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public EcommerceUserModel convert(final EcommerceUserRepresentation source) {

		EcommerceUserModel target = super.convert(source);

		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(final ObjectFactory<EcommerceUserModel> factory) {
		super.setFactory(factory);
	}

}
