package com.arthvedi.organic.user.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.user.domain.EcommerceUser;
import com.arthvedi.organic.user.model.EcommerceUserModel;

@Component("ecommerceUserToEcommerceUserModelConverter")
public class EcommerceUserToEcommerceUserModelConverter implements
		Converter<EcommerceUser, EcommerceUserModel> {
	@Autowired
	private ObjectFactory<EcommerceUserModel> ecommerceUserModelFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public EcommerceUserModel convert(final EcommerceUser source) {
		EcommerceUserModel ecommerceUserModel = ecommerceUserModelFactory
				.getObject();
		BeanUtils.copyProperties(source, ecommerceUserModel);

		return ecommerceUserModel;
	}

	@Autowired
	public void setEcommerceUserModelFactory(
			final ObjectFactory<EcommerceUserModel> ecommerceUserModelFactory) {
		this.ecommerceUserModelFactory = ecommerceUserModelFactory;
	}
}
