package com.arthvedi.organic.user.service;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.user.domain.UserSellerProductRating;
import com.arthvedi.organic.user.repository.UserSellerProductRatingRepository;

@Component
public class UserSellerProductRatingService implements
		IUserSellerProductRatingService {
	@Autowired
	private NullAwareBeanUtilsBean NonNullBeanUtils;
	@Autowired
	private UserSellerProductRatingRepository userSellerProductRatingRepository;

	@Override
	public UserSellerProductRating create(
			UserSellerProductRating userSellerProductRating) {
		userSellerProductRating.setUserCreated("varma");
		userSellerProductRating.setModifiedDate(new LocalDateTime());
		userSellerProductRating = userSellerProductRatingRepository
				.save(userSellerProductRating);
		return userSellerProductRating;
	}

	@Override
	public void deleteUserSellerProductRating(String userSellerProductRatingId) {

	}

	@Override
	public UserSellerProductRating getUserSellerProductRating(
			String userSellerProductRatingId) {
		return userSellerProductRatingRepository
				.findOne(userSellerProductRatingId);
	}

	@Override
	public List<UserSellerProductRating> getAll() {
		return null;
	}

	@Override
	public UserSellerProductRating updateUserSellerProductRating(
			UserSellerProductRating userSellerProductRating) {
		UserSellerProductRating userSellerProductRatings = userSellerProductRatingRepository
				.findOne(userSellerProductRating.getId());
		try {
			NonNullBeanUtils.copyProperties(userSellerProductRatings,
					userSellerProductRating);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		userSellerProductRatings.setUserModified("admin");
		userSellerProductRatings.setModifiedDate(new LocalDateTime());
		userSellerProductRating = userSellerProductRatingRepository
				.save(userSellerProductRating);
		return userSellerProductRating;
	}

}
