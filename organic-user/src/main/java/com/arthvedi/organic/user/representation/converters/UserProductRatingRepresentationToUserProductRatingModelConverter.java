/**
 *
 */
package com.arthvedi.organic.user.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.user.model.UserProductRatingModel;
import com.arthvedi.organic.user.representation.siren.UserProductRatingRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("userProductRatingRepresentationToUserProductRatingModelConverter")
public class UserProductRatingRepresentationToUserProductRatingModelConverter
		extends
		PropertyCopyingConverter<UserProductRatingRepresentation, UserProductRatingModel> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public UserProductRatingModel convert(
			final UserProductRatingRepresentation source) {

		UserProductRatingModel target = super.convert(source);

		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(final ObjectFactory<UserProductRatingModel> factory) {
		super.setFactory(factory);
	}

}
