package com.arthvedi.organic.user.service;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.user.domain.UserProductRating;
import com.arthvedi.organic.user.repository.UserProductRatingRepository;

@Component
public class UserProductRatingService implements IUserProductRatingService {

	@Autowired
	private UserProductRatingRepository userProductRatingRepository;
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;

	@Override
	public UserProductRating create(UserProductRating userProductRating) {
		userProductRating.setCreatedDate(new LocalDateTime());
		userProductRating.setUserCreated("varma");
		userProductRating = userProductRatingRepository.save(userProductRating);
		return userProductRating;
	}

	@Override
	public void deleteUserProductRating(String userProductRatingId) {

	}

	@Override
	public UserProductRating getUserProductRating(String userProductRatingId) {
		return userProductRatingRepository.findOne(userProductRatingId);
	}

	@Override
	public List<UserProductRating> getAll() {
		return null;
	}

	@Override
	public UserProductRating updateUserProductRating(
			UserProductRating userProductRating) {
		UserProductRating userProductRatings = userProductRatingRepository
				.findOne(userProductRating.getId());
		try {
			nonNullBeanUtils.copyProperties(userProductRatings,
					userProductRating);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		userProductRatings.setUserModified("admin");
		userProductRatings.setModifiedDate(new LocalDateTime());
		userProductRating = userProductRatingRepository
				.save(userProductRatings);
		return userProductRating;
	}

}
