package com.arthvedi.organic.user.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.user.domain.EcommerceRole;
import com.arthvedi.organic.user.model.EcommerceRoleModel;

@Component("ecommerceRoleToEcommerceRoleModelConverter")
public class EcommerceRoleToEcommerceRoleModelConverter implements
		Converter<EcommerceRole, EcommerceRoleModel> {
	@Autowired
	private ObjectFactory<EcommerceRoleModel> ecommerceRoleModelFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public EcommerceRoleModel convert(final EcommerceRole source) {
		EcommerceRoleModel ecommerceRoleModel = ecommerceRoleModelFactory
				.getObject();
		BeanUtils.copyProperties(source, ecommerceRoleModel);

		return ecommerceRoleModel;
	}

	@Autowired
	public void setEcommerceRoleModelFactory(
			final ObjectFactory<EcommerceRoleModel> ecommerceRoleModelFactory) {
		this.ecommerceRoleModelFactory = ecommerceRoleModelFactory;
	}
}
