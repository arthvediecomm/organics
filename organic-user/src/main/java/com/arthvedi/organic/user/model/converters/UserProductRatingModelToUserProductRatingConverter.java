/**
 *
 */
package com.arthvedi.organic.user.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.user.domain.UserProductRating;
import com.arthvedi.organic.user.model.UserProductRatingModel;

/**
 * @author Jay
 *
 */
@Component("userProductRatingModelToUserProductRatingConverter")
public class UserProductRatingModelToUserProductRatingConverter implements Converter<UserProductRatingModel, UserProductRating> {
    @Autowired
    private ObjectFactory<UserProductRating> userProductRatingFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public UserProductRating convert(final UserProductRatingModel source) {
        UserProductRating userProductRating = userProductRatingFactory.getObject();
        BeanUtils.copyProperties(source, userProductRating);

        return userProductRating;
    }

    @Autowired
    public void setUserProductRatingFactory(final ObjectFactory<UserProductRating> userProductRatingFactory) {
        this.userProductRatingFactory = userProductRatingFactory;
    }

}
