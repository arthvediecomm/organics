package com.arthvedi.organic.user.service;

import java.util.List;

import com.arthvedi.organic.user.domain.EcommerceUser;

public interface IEcommerceUserService {

	EcommerceUser create(EcommerceUser ecommerceUser);

	void deleteEcommerceUser(String ecommerceUserId);

	EcommerceUser getEcommerceUser(String ecommerceUserId);

	List<EcommerceUser> getAll();

	EcommerceUser updateEcommerceUser(EcommerceUser ecommerceUser);
}
