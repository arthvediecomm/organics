/**
 *
 */
package com.arthvedi.organic.user.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.user.domain.EcommerceRole;
import com.arthvedi.organic.user.model.EcommerceRoleModel;

/**
 * @author Jay
 *
 */
@Component("ecommerceRoleModelToEcommerceRoleConverter")
public class EcommerceRoleModelToEcommerceRoleConverter implements
		Converter<EcommerceRoleModel, EcommerceRole> {
	@Autowired
	private ObjectFactory<EcommerceRole> ecommerceRoleFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public EcommerceRole convert(final EcommerceRoleModel source) {
		EcommerceRole ecommerceRole = ecommerceRoleFactory.getObject();
		BeanUtils.copyProperties(source, ecommerceRole);

		return ecommerceRole;
	}

	@Autowired
	public void setEcommerceRoleFactory(
			final ObjectFactory<EcommerceRole> ecommerceRoleFactory) {
		this.ecommerceRoleFactory = ecommerceRoleFactory;
	}

}
