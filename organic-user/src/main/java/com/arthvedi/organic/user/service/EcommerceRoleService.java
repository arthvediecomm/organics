package com.arthvedi.organic.user.service;

import static com.arthvedi.organic.enums.Status.ACTIVE;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.user.domain.EcommerceRole;
import com.arthvedi.organic.user.repository.EcommerceRoleRepository;

@Component
public class EcommerceRoleService implements IEcommerceRoleService {
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private EcommerceRoleRepository ecommerceRoleRepository;

	@Override
	public EcommerceRole create(EcommerceRole ecommerceRole) {
		ecommerceRole.setUserCreated("varma");
		ecommerceRole.setCreatedDate(new LocalDateTime());
		ecommerceRole.setStatus(ACTIVE.name());
		ecommerceRole = ecommerceRoleRepository.save(ecommerceRole);
		return ecommerceRole;

	}

	@Override
	public void deleteEcommerceRole(String ecommerceRoleId) {

	}

	@Override
	public EcommerceRole getEcommerceRole(String ecommerceRoleId) {
		return ecommerceRoleRepository.findOne(ecommerceRoleId);
	}

	@Override
	public List<EcommerceRole> getAll() {
		return null;
	}
	@Override
	public EcommerceRole updateEcommerceRole(EcommerceRole ecommerceRole) {
		EcommerceRole ecommerceRoles = ecommerceRoleRepository
				.findOne(ecommerceRole.getId());
		try {
			nonNullBeanUtils.copyProperties(ecommerceRoles, ecommerceRole);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		ecommerceRoles.setUserModified("admin");
		ecommerceRoles.setModifiedDate(new LocalDateTime());
		ecommerceRole = ecommerceRoleRepository.save(ecommerceRoles);
		return ecommerceRole;
	}

}
