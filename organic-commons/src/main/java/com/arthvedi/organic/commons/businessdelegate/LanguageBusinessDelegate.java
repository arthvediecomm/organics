package com.arthvedi.organic.commons.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.commons.businessdelegate.context.LanguageContext;
import com.arthvedi.organic.commons.domain.Language;
import com.arthvedi.organic.commons.model.LanguageModel;
import com.arthvedi.organic.commons.service.ILanguageService;

/*
 *@Author varma
 */

@Service
public class LanguageBusinessDelegate
		implements
		IBusinessDelegate<LanguageModel, LanguageContext, IKeyBuilder<String>, String> {

	@Autowired
	private ILanguageService languageService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public LanguageModel create(LanguageModel model) {
		validateModel(model);
		boolean languageName = checkLanguageNameExists(model.getName()
				.toLowerCase());
		if (languageName) {
			model.setStatusMessage("FAILURE::: " + model.getName()
					+ " Already Exists..!!");
			return model;
		} else {

			Language language = languageService
					.create((Language) conversionService.convert(model,
							forObject(model), valueOf(Language.class)));
			model = convertToLanguageModel(language);
		}
		return model;
	}

	private boolean checkLanguageNameExists(String name) {
		return languageService.checkLanguageByName(name);
	}

	private void validateModel(LanguageModel model) {

		Validate.notNull(model, "Invalid Input");
		Validate.notBlank(model.getName(), "Invalid LanguageName");
	}

	private LanguageModel convertToLanguageModel(Language language) {
		return (LanguageModel) conversionService.convert(language,
				forObject(language), valueOf(LanguageModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, LanguageContext context) {

	}

	@Override
	public LanguageModel edit(IKeyBuilder<String> keyBuilder,
			LanguageModel model) {
		Language language = languageService.getLanguage(keyBuilder.build()
				.toString());
		if (language == null || model.getId().equals(language.getId())) {
			language = languageService
					.updateLanguage((Language) conversionService.convert(model,
							forObject(model), valueOf(Language.class)));
			model = convertToLanguageModel(language);
			model.setStatusMessage("SUCCESS:::Language '" + model.getName()
					+ "' updated Successfully");
		} else {
			model.setStatusMessage("FAILURE:::Language with name '"
					+ model.getName() + "' Already Exists, Please Try Another");
		}
		return model;
	}

	@Override
	public LanguageModel getByKey(IKeyBuilder<String> keyBuilder,
			LanguageContext context) {
		Language language = languageService.getLanguage(keyBuilder.build()
				.toString());
		LanguageModel model = conversionService.convert(language,
				LanguageModel.class);
		return model;
	}

	@Override
	public Collection<LanguageModel> getCollection(LanguageContext context) {
		List<Language> language = new ArrayList<Language>();
		if (context.getLanguageList() != null) {
			language = languageService.getLanguageList();
		}
		List<LanguageModel> languageModels = (List<LanguageModel>) conversionService
				.convert(
						language,
						TypeDescriptor.forObject(language),
						TypeDescriptor.collection(List.class,
								TypeDescriptor.valueOf(LanguageModel.class)));
		return languageModels;
	}

	@Override
	public LanguageModel edit(IKeyBuilder<String> keyBuilder,
			LanguageModel model, LanguageContext context) {
		throw new UnsupportedOperationException("Operation not implemented yet");
	}

}
