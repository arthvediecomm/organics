package com.arthvedi.organic.commons.businessdelegate;


import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.commons.businessdelegate.context.EmailSubscriptionContext;
import com.arthvedi.organic.commons.domain.EmailSubscription;
import com.arthvedi.organic.commons.model.EmailSubscriptionModel;
import com.arthvedi.organic.commons.service.IEmailSubscriptionService;

/*
*@Author varma
*/

@Service
public class EmailSubscriptionBusinessDelegate
		implements IBusinessDelegate<EmailSubscriptionModel, EmailSubscriptionContext, IKeyBuilder<String>, String> {

	@Autowired
	private IEmailSubscriptionService emailSubscriptionService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public EmailSubscriptionModel create(EmailSubscriptionModel model) {
		validateModel(model);
		EmailSubscription emailSubscrption = checkUserEmailByEmail(model.getUserEmail());
		if (emailSubscrption != null) {
			switch (emailSubscrption.getSubscriptionStatus()) {
			case "SUBSCRIBED":
				model.setStatusMessage("ALSUBSCRIBED");
				break;
			case "INACTIVE":
				model.setStatusMessage("INACTIVE");
				break;
			case "UNSUBSCRIBED":
				model.setStatusMessage("UNSUBSCRIBED");
			}
			return model;
		}
		else{
			
EmailSubscription emailSubscription =emailSubscriptionService.create((EmailSubscription)conversionService.convert(model, forObject(model),valueOf(EmailSubscription.class)));
model=convertToEmailSubscriptionModel(emailSubscription);
model.setSubscriptionStatus("SUCCESS:::  Successfully Subscribed.");

		}
		return model;
	}

	private EmailSubscription checkUserEmailByEmail(String userEmail) {
		return emailSubscriptionService.getEmailSubscriptionByUserEmail(userEmail);
	}

	private EmailSubscriptionModel convertToEmailSubscriptionModel(EmailSubscription emailSubscription) {
		return (EmailSubscriptionModel)conversionService .convert(emailSubscription, forObject(emailSubscription), valueOf(EmailSubscriptionModel.class));
	}

	private void validateModel(EmailSubscriptionModel model) {
		
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, EmailSubscriptionContext context) {

	}

	@Override
	public EmailSubscriptionModel edit(IKeyBuilder<String> keyBuilder, EmailSubscriptionModel model) {
		EmailSubscription emailSubscription = emailSubscriptionService.getEmailSubscription(keyBuilder.build().toString());
		
		emailSubscription = emailSubscriptionService.updateEmailSubscription((EmailSubscription)conversionService .convert(model, forObject(model), valueOf(EmailSubscription .class)));
		model=convertToEmailSubscriptionModel(emailSubscription);
		return model;
	}

	@Override
	public EmailSubscriptionModel getByKey(IKeyBuilder<String> keyBuilder, EmailSubscriptionContext context) {
		EmailSubscription emailSubscription = emailSubscriptionService.getEmailSubscription(keyBuilder.build().toString());
		EmailSubscriptionModel model = conversionService.convert(emailSubscription, EmailSubscriptionModel.class);
		return model;
	}

	@Override
	public Collection<EmailSubscriptionModel> getCollection(EmailSubscriptionContext context) {
		List<EmailSubscription> emailSubscription = new ArrayList<EmailSubscription>();
		
		List<EmailSubscriptionModel> emailSubscriptionModels = (List<EmailSubscriptionModel>) conversionService.convert(
				emailSubscription, TypeDescriptor.forObject(emailSubscription),
				TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(EmailSubscriptionModel.class)));
		return emailSubscriptionModels;
	}

	@Override
	public EmailSubscriptionModel edit(IKeyBuilder<String> keyBuilder,
			EmailSubscriptionModel model, EmailSubscriptionContext context) {
		throw new UnsupportedOperationException("Operation not implemented yet");
	}

}
