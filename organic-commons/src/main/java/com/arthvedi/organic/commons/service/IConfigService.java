package com.arthvedi.organic.commons.service;

import java.util.List;

import com.arthvedi.organic.commons.domain.Config;

/*
 *@Author varma
 */
public interface IConfigService {

	Config create(Config config);

	void deleteConfig(String configId);

	Config getConfig(String configId);

	List<Config> getAll();

	boolean getConfigByConfigName(String configName);

	Config updateConfig(Config config);
}
