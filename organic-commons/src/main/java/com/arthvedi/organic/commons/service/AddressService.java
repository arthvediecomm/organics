package com.arthvedi.organic.commons.service;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.commons.domain.Address;
import com.arthvedi.organic.commons.repository.AddressRepository;

/*
 *@Author varma
 */
@Component
public class AddressService implements IAddressService {
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;

	@Autowired
	private AddressRepository addressRepository;

	@Override
	@Transactional
	public Address create(Address address) {
		address.setCreatedDate(new LocalDateTime());
		address.setUserCreated("varma");
		address = addressRepository.save(address);
		return address;
	}

	@Override
	public void deleteAddress(String addressId) {

	}

	@Override
	public Address getAddress(String addressId) {
		return addressRepository.findOne(addressId);
	}

	@Override
	public List<Address> getAll() {

		return null;
	}

	@Override
	public Address updateAddress(Address address) {
		Address addresss = addressRepository.findOne(address.getId());
		try {
			nonNullBeanUtils.copyProperties(addresss, address);
		} catch (IllegalAccessException e) {
			e.printStackTrace();

		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		addresss.setModifiedDate(new LocalDateTime());
		addresss.setUserModified("admin");
		address = addressRepository.save(addresss);
		return address;
	}

}
