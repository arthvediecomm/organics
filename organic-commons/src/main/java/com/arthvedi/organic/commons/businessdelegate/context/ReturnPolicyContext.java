package com.arthvedi.organic.commons.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;
/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ReturnPolicyContext implements IBusinessDelegateContext {

	private String returnPolicyId;
	private String all;
	
	
	public String getReturnPolicyId() {
		return returnPolicyId;
	}

	public void setReturnPolicyId(String returnPolicyId) {
		this.returnPolicyId = returnPolicyId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

}
