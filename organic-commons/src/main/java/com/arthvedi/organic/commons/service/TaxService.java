package com.arthvedi.organic.commons.service;

import static com.arthvedi.organic.enums.Status.ACTIVE;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.commons.domain.Tax;
import com.arthvedi.organic.commons.repository.TaxRepository;

/*
 *@Author varma
 */
@Component
public class TaxService implements ITaxService {
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private TaxRepository taxRepository;

	@Override
	public Tax create(Tax tax) {
		tax.setCreatedDate(new LocalDateTime());
		tax.setUserCreated("varma");
		tax.setStatus(ACTIVE.name());
		return taxRepository.save(tax);
	}

	@Override
	public void deleteTax(String taxId) {

	}

	@Override
	public Tax getTax(String taxId) {
		return taxRepository.findOne(taxId);
	}

	@Override
	public List<Tax> getAll() {
		return null;
	}

	@Override
	public Tax updateTax(Tax tax) {
		Tax taxs = taxRepository.findOne(tax.getId());
		try {
			nonNullBeanUtils.copyProperties(taxs, tax);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		taxs.setModifiedDate(new LocalDateTime());
		taxs.setUserModified("admin");
		tax = taxRepository.save((taxs));
		return tax;
	}

	@Override
	public boolean checkTaxNameExists(String taxName) {
		return taxRepository.findByTaxName(taxName) != null;
	}

	@Override
	public Tax getTaxByTaxName(String taxName) {
		return taxRepository.findByTaxName(taxName);
	}

	@Override
	public List<Tax> getTaxList() {
		List<Tax> taxs = (List<Tax>) taxRepository.findAll();
		List<Tax> taxes = new ArrayList<Tax>();
		for (Tax t : taxs) {
			Tax ta = new Tax();
			ta.setStatus(t.getStatus());
			ta.setTaxName(t.getTaxName());
			taxes.add(ta);
		}
		return taxes;
	}

}
