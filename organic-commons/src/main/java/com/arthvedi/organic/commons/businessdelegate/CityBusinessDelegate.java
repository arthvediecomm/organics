package com.arthvedi.organic.commons.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.commons.businessdelegate.context.CityContext;
import com.arthvedi.organic.commons.domain.City;
import com.arthvedi.organic.commons.model.CityModel;
import com.arthvedi.organic.commons.service.ICityService;

/*
 *@Author varma
 */

@Service
public class CityBusinessDelegate implements
		IBusinessDelegate<CityModel, CityContext, IKeyBuilder<String>, String> {

	@Autowired
	private ICityService cityService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public CityModel create(CityModel model) {
		validateModel(model);
		boolean cityName = checkCityExist(model.getCityName().toLowerCase());
		if (cityName) {
			model.setStatusMessage("FAILURE::: " + model.getCityName()
					+ " Already Exists..!!");
			return model;
		} else {
			City city = cityService.create((City) conversionService.convert(
					model, forObject(model), valueOf(City.class)));
			model = convertToCityModel(city);
			model.setStatusMessage("SUCCESS::: " + model.getCityName()
					+ " Created Successfully.");

		}

		return model;
	}

	private boolean checkCityExist(String cityName) {

		return cityService.getCityByCityName(cityName) != null;
	}

	private CityModel convertToCityModel(City city) {
		return (CityModel) conversionService.convert(city, forObject(city),
				valueOf(CityModel.class));
	}

	private void validateModel(final CityModel model) {
		Validate.notNull(model, "Invalid Input");
		Validate.notEmpty(model.getCityName(), "Invalid CityName");
		Validate.isTrue(!model.getCityName().trim().equals(""),
				"City Name should be no Empty String");

	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, CityContext context) {

	}

	@Override
	public CityModel edit(IKeyBuilder<String> keyBuilder, CityModel model) {
		validateModel(model);
		if (model.getCityName() != null) {
			City cit = cityService.getCityByCityName(model.getCityName()
					.toLowerCase());
			if (cit == null || model.getId().equals(cit.getId())) {
				City city = cityService.updateCity((City) conversionService
						.convert(model, forObject(model), valueOf(City.class)));
				model = convertToCityModel(city);
				model.setStatusMessage("SUCCESS::: City '"
						+ model.getCityName() + "' Edited Successfully'");
			} else {
				model.setStatusMessage("FAILURE::: City Name'"
						+ model.getCityName() + "' Already Exists");
			}
		}
		return model;
	}

	@Override
	public CityModel getByKey(IKeyBuilder<String> keyBuilder,
			CityContext context) {
		City city = cityService.getCity(keyBuilder.build().toString());
		CityModel model = conversionService.convert(city, CityModel.class);
		return model;
	}

	@Override
	public Collection<CityModel> getCollection(CityContext context) {
		List<City> city = new ArrayList<City>();
		if (context.getCityList() != null) {
			city = cityService.getCityList();
		}
		List<CityModel> cityModels = (List<CityModel>) conversionService
				.convert(
						city,
						TypeDescriptor.forObject(city),
						TypeDescriptor.collection(List.class,
								TypeDescriptor.valueOf(CityModel.class)));
		return cityModels;
	}

	@Override
	public CityModel edit(IKeyBuilder<String> keyBuilder, CityModel model,
			CityContext context) {
		throw new UnsupportedOperationException("Operation not implemented yet");
	}

}
