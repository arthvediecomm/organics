/**
 *
 */
package com.arthvedi.organic.commons.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.commons.model.CityModel;
import com.arthvedi.organic.commons.representation.siren.CityRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("cityRepresentationToCityModelConverter")
public class CityRepresentationToCityModelConverter extends PropertyCopyingConverter<CityRepresentation, CityModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public CityModel convert(final CityRepresentation source) {

        CityModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<CityModel> factory) {
        super.setFactory(factory);
    }

}
