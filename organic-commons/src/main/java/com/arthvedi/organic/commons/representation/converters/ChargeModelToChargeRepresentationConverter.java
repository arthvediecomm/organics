/**
 *
 */
package com.arthvedi.organic.commons.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.commons.model.ChargeModel;
import com.arthvedi.organic.commons.representation.siren.ChargeRepresentation;

/**
 * @author varma
 *
 */
@Component("chargeModelToChargeRepresentationConverter")
public class ChargeModelToChargeRepresentationConverter extends PropertyCopyingConverter<ChargeModel, ChargeRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public ChargeRepresentation convert(final ChargeModel source) {

        ChargeRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<ChargeRepresentation> factory) {
        super.setFactory(factory);
    }

}
