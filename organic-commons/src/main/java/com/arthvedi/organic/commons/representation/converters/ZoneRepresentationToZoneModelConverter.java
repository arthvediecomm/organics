/**
 *
 */
package com.arthvedi.organic.commons.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.commons.model.ZoneModel;
import com.arthvedi.organic.commons.representation.siren.ZoneRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("zoneRepresentationToZoneModelConverter")
public class ZoneRepresentationToZoneModelConverter extends PropertyCopyingConverter<ZoneRepresentation, ZoneModel> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public ZoneModel convert(final ZoneRepresentation source) {
		ZoneModel target = super.convert(source);

		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(final ObjectFactory<ZoneModel> factory) {
		super.setFactory(factory);
	}

}
