package com.arthvedi.organic.commons.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.commons.businessdelegate.context.CityContext;
import com.arthvedi.organic.commons.model.CityModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/city", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class CityController {

	private IBusinessDelegate<CityModel, CityContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<CityContext> cityContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create")
	public ResponseEntity<CityModel> createCity(
			@RequestBody final CityModel cityModel) {
		businessDelegate.create(cityModel);
		return new ResponseEntity<CityModel>(cityModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	public ResponseEntity<CityModel> edit(
			@PathVariable(value = "id") final String cityId,
			@RequestBody final CityModel cityModel) {

		businessDelegate.edit(keyBuilderFactory.getObject().withId(cityId),
				cityModel);
		return new ResponseEntity<CityModel>(cityModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<IModelWrapper<Collection<CityModel>>> getAll() {
		CityContext cityContext = cityContextFactory.getObject();
		cityContext.setAll("all");
		Collection<CityModel> cityModels = businessDelegate
				.getCollection(cityContext);
		IModelWrapper<Collection<CityModel>> models = new CollectionModelWrapper<CityModel>(
				CityModel.class, cityModels);
		return new ResponseEntity<IModelWrapper<Collection<CityModel>>>(models,
				HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/citylist", consumes = { MediaType.ALL_VALUE })
	public ResponseEntity<IModelWrapper<Collection<CityModel>>> getCityList() {
		CityContext cityContext = new CityContext();
		cityContext.setCityList("cityList");
		Collection<CityModel> cityModels = businessDelegate
				.getCollection(cityContext);
		IModelWrapper<Collection<CityModel>> models = new CollectionModelWrapper<CityModel>(
				CityModel.class, cityModels);
		return new ResponseEntity<IModelWrapper<Collection<CityModel>>>(models,
				HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<CityModel> getCity(
			@PathVariable(value = "id") final String cityId) {
		CityContext cityContext = cityContextFactory.getObject();

		CityModel model = businessDelegate.getByKey(keyBuilderFactory
				.getObject().withId(cityId), cityContext);
		return new ResponseEntity<CityModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "cityBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<CityModel, CityContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setCityObjectFactory(
			final ObjectFactory<CityContext> cityContextFactory) {
		this.cityContextFactory = cityContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(
			final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
