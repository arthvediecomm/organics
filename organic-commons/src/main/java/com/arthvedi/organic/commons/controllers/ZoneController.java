package com.arthvedi.organic.commons.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.commons.businessdelegate.context.ZoneContext;
import com.arthvedi.organic.commons.model.ZoneModel;
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/zone", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class ZoneController {

	private IBusinessDelegate<ZoneModel, ZoneContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<ZoneContext> zoneContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create")
	public ResponseEntity<ZoneModel> createZone(@RequestBody ZoneModel zoneModel) {
		zoneModel = businessDelegate.create(zoneModel);
		return new ResponseEntity<ZoneModel>(zoneModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	public ResponseEntity<ZoneModel> edit(
			@PathVariable(value = "id") final String zoneId,
			@RequestBody ZoneModel zoneModel) {

		zoneModel = businessDelegate.edit(
				keyBuilderFactory.getObject().withId(zoneId), zoneModel);
		return new ResponseEntity<ZoneModel>(zoneModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all")
	public ResponseEntity<IModelWrapper<Collection<ZoneModel>>> getAll() {
		ZoneContext zoneContext = zoneContextFactory.getObject();
		zoneContext.setAll("all");
		Collection<ZoneModel> zoneModels = businessDelegate
				.getCollection(zoneContext);
		IModelWrapper<Collection<ZoneModel>> models = new CollectionModelWrapper<ZoneModel>(
				ZoneModel.class, zoneModels);
		return new ResponseEntity<IModelWrapper<Collection<ZoneModel>>>(models,
				HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = { "/zonelist",
			"/sellerapp/zonelist" })
	public ResponseEntity<IModelWrapper<Collection<ZoneModel>>> getAllZoneLists() {

		ZoneContext zoneContext = zoneContextFactory.getObject();
		zoneContext.setZoneList("zoneLists");
		Collection<ZoneModel> zoneModels = businessDelegate
				.getCollection(zoneContext);
		IModelWrapper<Collection<ZoneModel>> models = new CollectionModelWrapper<ZoneModel>(
				ZoneModel.class, zoneModels);

		return new ResponseEntity<IModelWrapper<Collection<ZoneModel>>>(models,
				HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = {
			"/{cityId}/zonesbycity"})
	public ResponseEntity<IModelWrapper<Collection<ZoneModel>>> getAll(
			@PathVariable(value = "cityId") final String cityId) {
		ZoneContext zoneContext = zoneContextFactory.getObject();
		zoneContext.setCityId(cityId);
		Collection<ZoneModel> zoneModels = businessDelegate
				.getCollection(zoneContext);
		IModelWrapper<Collection<ZoneModel>> models = new CollectionModelWrapper<ZoneModel>(
				ZoneModel.class, zoneModels);

		return new ResponseEntity<IModelWrapper<Collection<ZoneModel>>>(models,
				HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<ZoneModel> getZone(
			@PathVariable(value = "id") final String zoneId) {
		ZoneContext zoneContext = zoneContextFactory.getObject();

		ZoneModel model = businessDelegate.getByKey(keyBuilderFactory
				.getObject().withId(zoneId), zoneContext);
		return new ResponseEntity<ZoneModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "zoneBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<ZoneModel, ZoneContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setZoneObjectFactory(
			final ObjectFactory<ZoneContext> zoneContextFactory) {
		this.zoneContextFactory = zoneContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(
			final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
