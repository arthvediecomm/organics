package com.arthvedi.organic.commons.service;

import java.util.List;

import com.arthvedi.organic.commons.domain.Language;

/*
 *@Author varma
 */
public interface ILanguageService {

	Language create(Language language);

	void deleteLanguage(String languageId);

	Language getLanguage(String languageId);

	List<Language> getAll();

	Language updateLanguage(Language language);

	List<Language> getLanguageList();

	boolean checkLanguageByName(String name);
}
