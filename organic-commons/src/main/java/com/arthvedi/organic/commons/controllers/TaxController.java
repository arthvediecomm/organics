package com.arthvedi.organic.commons.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.commons.businessdelegate.context.TaxContext;
import com.arthvedi.organic.commons.model.TaxModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/tax", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class TaxController {

	private IBusinessDelegate<TaxModel, TaxContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<TaxContext> taxContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<TaxModel> createTax(
			@RequestBody final TaxModel taxModel) {
		businessDelegate.create(taxModel);
		return new ResponseEntity<TaxModel>(taxModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<TaxModel> edit(
			@PathVariable(value = "id") final String taxId,
			@RequestBody final TaxModel taxModel) {

		businessDelegate.edit(keyBuilderFactory.getObject().withId(taxId),
				taxModel);
		return new ResponseEntity<TaxModel>(taxModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<TaxModel>>> getAll() {
		TaxContext taxContext = taxContextFactory.getObject();
		taxContext.setAll("all");
		Collection<TaxModel> taxModels = businessDelegate
				.getCollection(taxContext);
		IModelWrapper<Collection<TaxModel>> models = new CollectionModelWrapper<TaxModel>(
				TaxModel.class, taxModels);
		return new ResponseEntity<IModelWrapper<Collection<TaxModel>>>(models,
				HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/taxlist", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<TaxModel>>> getTaxList() {
		TaxContext taxContext = taxContextFactory.getObject();
		taxContext.setTaxList("taxList");
		Collection<TaxModel> taxModels = businessDelegate
				.getCollection(taxContext);
		IModelWrapper<Collection<TaxModel>> models = new CollectionModelWrapper<TaxModel>(
				TaxModel.class, taxModels);
		return new ResponseEntity<IModelWrapper<Collection<TaxModel>>>(models,
				HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<TaxModel> getTax(
			@PathVariable(value = "id") final String taxId) {
		TaxContext taxContext = taxContextFactory.getObject();

		TaxModel model = businessDelegate.getByKey(keyBuilderFactory
				.getObject().withId(taxId), taxContext);
		return new ResponseEntity<TaxModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "taxBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<TaxModel, TaxContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setTaxObjectFactory(
			final ObjectFactory<TaxContext> taxContextFactory) {
		this.taxContextFactory = taxContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(
			final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
