/**
 *
 */
package com.arthvedi.organic.commons.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.commons.domain.ReturnPolicy;
import com.arthvedi.organic.commons.model.ReturnPolicyModel;

/**
 * @author Jay
 *
 */
@Component("returnPolicyModelToReturnPolicyConverter")
public class ReturnPolicyModelToReturnPolicyConverter implements Converter<ReturnPolicyModel, ReturnPolicy> {
    @Autowired
    private ObjectFactory<ReturnPolicy> returnPolicyFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public ReturnPolicy convert(final ReturnPolicyModel source) {
        ReturnPolicy returnPolicy = returnPolicyFactory.getObject();
        BeanUtils.copyProperties(source, returnPolicy);

        return returnPolicy;
    }

    @Autowired
    public void setReturnPolicyFactory(final ObjectFactory<ReturnPolicy> returnPolicyFactory) {
        this.returnPolicyFactory = returnPolicyFactory;
    }

}
