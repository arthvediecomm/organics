package com.arthvedi.organic.commons.service;

import static com.arthvedi.organic.enums.Status.ACTIVE;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.commons.domain.Language;
import com.arthvedi.organic.commons.repository.LanguageRepository;

/*
 *@Author varma
 */
@Component
public class LanguageService implements ILanguageService {
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private LanguageRepository languageRepository;

	@Override
	public Language create(Language language) {
		language.setCreatedDate(new LocalDateTime());
		language.setUserCreated("varma");
		language.setStatus(ACTIVE.name());
		language = languageRepository.save(language);
		return language;
	}

	@Override
	public void deleteLanguage(String languageId) {

	}

	@Override
	public Language getLanguage(String languageId) {
		return languageRepository.findOne(languageId);
	}

	@Override
	public List<Language> getAll() {
		return null;
	}

	@Override
	public Language updateLanguage(Language language) {
		Language languages = languageRepository.findOne(language.getId());
		try {
			nonNullBeanUtils.copyProperties(languages, language);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		languages.setUserModified("admin");
		languages.setModifiedDate(new LocalDateTime());
		language = languageRepository.save(languages);
		return language;
	}

	@Override
	public List<Language> getLanguageList() {
		List<Language> languages = (List<Language>) languageRepository
				.findAll();
		List<Language> languagess = new ArrayList<Language>();
		for (Language language : languages) {
			Language lan = new Language();
			lan.setName(language.getName());
			lan.setStatus(language.getStatus());
			lan.setId(language.getId());
			languagess.add(lan);
		}
		return languagess;
	}

	@Override
	public boolean checkLanguageByName(String name) {
		return languageRepository.findByLanguageName(name) != null;
	}

}
