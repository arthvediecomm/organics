package com.arthvedi.organic.commons.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.commons.domain.ReturnPolicy;
import com.arthvedi.organic.commons.model.ReturnPolicyModel;

@Component("returnPolicyToReturnPolicyModelConverter")
public class ReturnPolicyToReturnPolicyModelConverter
        implements Converter<ReturnPolicy, ReturnPolicyModel> {
    @Autowired
    private ObjectFactory<ReturnPolicyModel> returnPolicyModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public ReturnPolicyModel convert(final ReturnPolicy source) {
        ReturnPolicyModel returnPolicyModel = returnPolicyModelFactory.getObject();
        BeanUtils.copyProperties(source, returnPolicyModel);

        return returnPolicyModel;
    }

    @Autowired
    public void setReturnPolicyModelFactory(
            final ObjectFactory<ReturnPolicyModel> returnPolicyModelFactory) {
        this.returnPolicyModelFactory = returnPolicyModelFactory;
    }
}
