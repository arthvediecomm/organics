package com.arthvedi.organic.commons.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.commons.domain.EmailSubscription;
import com.arthvedi.organic.commons.model.EmailSubscriptionModel;

@Component("emailSubscriptionToEmailSubscriptionModelConverter")
public class EmailSubscriptionToEmailSubscriptionModelConverter
        implements Converter<EmailSubscription, EmailSubscriptionModel> {
    @Autowired
    private ObjectFactory<EmailSubscriptionModel> emailSubscriptionModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public EmailSubscriptionModel convert(final EmailSubscription source) {
        EmailSubscriptionModel emailSubscriptionModel = emailSubscriptionModelFactory.getObject();
        BeanUtils.copyProperties(source, emailSubscriptionModel);

        return emailSubscriptionModel;
    }

    @Autowired
    public void setEmailSubscriptionModelFactory(
            final ObjectFactory<EmailSubscriptionModel> emailSubscriptionModelFactory) {
        this.emailSubscriptionModelFactory = emailSubscriptionModelFactory;
    }
}
