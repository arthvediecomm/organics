package com.arthvedi.organic.commons.service;

import java.util.List;

import com.arthvedi.organic.commons.domain.EmailSubscription;

/*
 *@Author varma
 */
public interface IEmailSubscriptionService {

	EmailSubscription create(EmailSubscription emailSubscription);

	void deleteEmailSubscription(String emailSubscriptionId);

	EmailSubscription getEmailSubscription(String emailSubscriptionId);

	List<EmailSubscription> getAll();

	EmailSubscription updateEmailSubscription(
			EmailSubscription emailSubscription);

	EmailSubscription getEmailSubscriptionByUserEmail(String userEmail);
}
