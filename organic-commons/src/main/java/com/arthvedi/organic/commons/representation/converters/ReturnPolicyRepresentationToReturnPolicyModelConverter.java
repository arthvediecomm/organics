/**
 *
 */
package com.arthvedi.organic.commons.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.commons.model.ReturnPolicyModel;
import com.arthvedi.organic.commons.representation.siren.ReturnPolicyRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("returnPolicyRepresentationToReturnPolicyModelConverter")
public class ReturnPolicyRepresentationToReturnPolicyModelConverter extends PropertyCopyingConverter<ReturnPolicyRepresentation, ReturnPolicyModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public ReturnPolicyModel convert(final ReturnPolicyRepresentation source) {

        ReturnPolicyModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<ReturnPolicyModel> factory) {
        super.setFactory(factory);
    }

}
