package com.arthvedi.organic.commons.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.commons.domain.ZoneArea;
import com.arthvedi.organic.commons.model.ZoneAreaModel;

@Component("zoneAreaToZoneAreaModelConverter")
public class ZoneAreaToZoneAreaModelConverter implements Converter<ZoneArea, ZoneAreaModel> {
	@Autowired
	private ObjectFactory<ZoneAreaModel> zoneAreaModelFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public ZoneAreaModel convert(final ZoneArea source) {
		ZoneAreaModel zoneAreaModel = zoneAreaModelFactory.getObject();
		BeanUtils.copyProperties(source, zoneAreaModel);
		if (source.getZone() != null) {
			zoneAreaModel.setZoneId(source.getZone().getId());
			zoneAreaModel.setZoneName(source.getZone().getZoneName());
		}
		return zoneAreaModel;
	}

	@Autowired
	public void setZoneAreaModelFactory(final ObjectFactory<ZoneAreaModel> zoneAreaModelFactory) {
		this.zoneAreaModelFactory = zoneAreaModelFactory;
	}
}
