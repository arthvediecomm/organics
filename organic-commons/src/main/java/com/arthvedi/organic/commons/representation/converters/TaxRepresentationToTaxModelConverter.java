/**
 *
 */
package com.arthvedi.organic.commons.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.commons.model.TaxModel;
import com.arthvedi.organic.commons.representation.siren.TaxRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("taxRepresentationToTaxModelConverter")
public class TaxRepresentationToTaxModelConverter extends PropertyCopyingConverter<TaxRepresentation, TaxModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public TaxModel convert(final TaxRepresentation source) {

        TaxModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<TaxModel> factory) {
        super.setFactory(factory);
    }

}
