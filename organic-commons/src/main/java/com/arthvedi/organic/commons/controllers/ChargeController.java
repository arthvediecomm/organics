package com.arthvedi.organic.commons.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.commons.businessdelegate.context.ChargeContext;
import com.arthvedi.organic.commons.model.ChargeModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/charge", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class ChargeController {

	private IBusinessDelegate<ChargeModel, ChargeContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<ChargeContext> chargeContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<ChargeModel> createCharge(@RequestBody final ChargeModel chargeModel) {
		businessDelegate.create(chargeModel);
		return new ResponseEntity<ChargeModel>(chargeModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<ChargeModel> edit(@PathVariable(value = "id") final String chargeId,
			@RequestBody final ChargeModel chargeModel) {

		businessDelegate.edit(keyBuilderFactory.getObject().withId(chargeId), chargeModel);
		return new ResponseEntity<ChargeModel>(chargeModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<ChargeModel>>> getAll() {
		ChargeContext chargeContext = chargeContextFactory.getObject();
		chargeContext.setAll("all");
		Collection<ChargeModel> chargeModels = businessDelegate.getCollection(chargeContext);
		IModelWrapper<Collection<ChargeModel>> models = new CollectionModelWrapper<ChargeModel>(
				ChargeModel.class, chargeModels);
		return new ResponseEntity<IModelWrapper<Collection<ChargeModel>>>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/chargelist", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<ChargeModel>>> getChargeList() {
		ChargeContext chargeContext = chargeContextFactory.getObject();
		chargeContext.setChargeList("chargeList");
		Collection<ChargeModel> chargeModels = businessDelegate.getCollection(chargeContext);
		IModelWrapper<Collection<ChargeModel>> models = new CollectionModelWrapper<ChargeModel>(
				ChargeModel.class, chargeModels);
		return new ResponseEntity<IModelWrapper<Collection<ChargeModel>>>(models, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<ChargeModel> getCharge(@PathVariable(value = "id") final String chargeId) {
		ChargeContext chargeContext = chargeContextFactory.getObject();

		ChargeModel model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(chargeId),
				chargeContext);
		return new ResponseEntity<ChargeModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "chargeBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<ChargeModel, ChargeContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setChargeObjectFactory(final ObjectFactory<ChargeContext> chargeContextFactory) {
		this.chargeContextFactory = chargeContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
