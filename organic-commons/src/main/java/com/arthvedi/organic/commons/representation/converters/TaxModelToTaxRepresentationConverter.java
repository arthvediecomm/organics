/**
 *
 */
package com.arthvedi.organic.commons.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.commons.model.TaxModel;
import com.arthvedi.organic.commons.representation.siren.TaxRepresentation;

/**
 * @author varma
 *
 */
@Component("taxModelToTaxRepresentationConverter")
public class TaxModelToTaxRepresentationConverter extends PropertyCopyingConverter<TaxModel, TaxRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public TaxRepresentation convert(final TaxModel source) {

        TaxRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<TaxRepresentation> factory) {
        super.setFactory(factory);
    }

}
