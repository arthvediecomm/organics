/**
 *
 */
package com.arthvedi.organic.commons.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.commons.domain.Charge;
import com.arthvedi.organic.commons.model.ChargeModel;

/**
 * @author Jay
 *
 */
@Component("chargeModelToChargeConverter")
public class ChargeModelToChargeConverter implements Converter<ChargeModel, Charge> {
    @Autowired
    private ObjectFactory<Charge> chargeFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public Charge convert(final ChargeModel source) {
        Charge charge = chargeFactory.getObject();
        BeanUtils.copyProperties(source, charge);

        return charge;
    }

    @Autowired
    public void setChargeFactory(final ObjectFactory<Charge> chargeFactory) {
        this.chargeFactory = chargeFactory;
    }

}
