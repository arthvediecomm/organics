package com.arthvedi.organic.commons.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.commons.businessdelegate.context.ConfigContext;
import com.arthvedi.organic.commons.domain.Config;
import com.arthvedi.organic.commons.model.ConfigModel;
import com.arthvedi.organic.commons.service.IConfigService;

/*
 *@Author varma
 */

@Service
public class ConfigBusinessDelegate
		implements IBusinessDelegate<ConfigModel, ConfigContext, IKeyBuilder<String>, String> {

	@Autowired
	private IConfigService configService;
	@Autowired
	private ConversionService conversionService;

	private ConfigModel convertToConfigModel(final Config config) {
		return (ConfigModel) conversionService.convert(config, forObject(config), valueOf(ConfigModel.class));
	}

	@Override
	public ConfigModel create(ConfigModel model) {
		validateModel(model);
		Config config = configService
				.create((Config) conversionService.convert(model, forObject(model), valueOf(Config.class)));
		model = convertToConfigModel(config);
		return model;
	}

	@Override
	public void delete(final IKeyBuilder<String> keyBuilder, final ConfigContext context) {

	}

	@Override
	public ConfigModel edit(final IKeyBuilder<String> keyBuilder, ConfigModel model) {

		Config config = configService
				.updateConfig((Config) conversionService.convert(model, forObject(model), valueOf(Config.class)));
		model = convertToConfigModel(config);

		return model;
	}

	@Override
	public ConfigModel getByKey(final IKeyBuilder<String> keyBuilder, final ConfigContext context) {
		Config config = configService.getConfig(keyBuilder.build().toString());
		ConfigModel model = conversionService.convert(config, ConfigModel.class);
		return model;
	}

	@Override
	public Collection<ConfigModel> getCollection(final ConfigContext context) {
		List<Config> config = new ArrayList<Config>();
		List<ConfigModel> configModels = (List<ConfigModel>) conversionService.convert(config,
				TypeDescriptor.forObject(config),
				TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(ConfigModel.class)));
		return configModels;
	}

	private void validateModel(final ConfigModel model) {
		Validate.notNull(model, "Invalid Input");
		Validate.notBlank(model.getConfigName(), "Invalid CityName");

	}

	@Override
	public ConfigModel edit(IKeyBuilder<String> keyBuilder, ConfigModel model, ConfigContext context) {
		throw new UnsupportedOperationException("Operation not implemented yet");
	}

}
