package com.arthvedi.organic.commons.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.commons.businessdelegate.context.ConfigContext;
import com.arthvedi.organic.commons.model.ConfigModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/config", produces = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class ConfigController {

	private IBusinessDelegate<ConfigModel, ConfigContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<ConfigContext> configContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create")
	
	public ResponseEntity<ConfigModel> createConfig(@RequestBody final ConfigModel configModel) {
		businessDelegate.create(configModel);
		return new ResponseEntity<ConfigModel>(configModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	
	public ResponseEntity<ConfigModel> edit(@PathVariable(value = "id") final String configId,
			@RequestBody ConfigModel configModel) {
		configModel = businessDelegate.edit(keyBuilderFactory.getObject().withId(configId), configModel);
		return new ResponseEntity<ConfigModel>(configModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all")
	
	public ResponseEntity<IModelWrapper<Collection<ConfigModel>>> getAll() {
		ConfigContext configContext = configContextFactory.getObject();
		configContext.setAll("all");
		Collection<ConfigModel> configModels = businessDelegate.getCollection(configContext);
		IModelWrapper<Collection<ConfigModel>> models = new CollectionModelWrapper<ConfigModel>(ConfigModel.class,
				configModels);
		return new ResponseEntity<IModelWrapper<Collection<ConfigModel>>>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	
	public ResponseEntity<ConfigModel> getConfig(@PathVariable(value = "id") final String configId) {
		ConfigContext configContext = configContextFactory.getObject();

		ConfigModel model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(configId), configContext);
		return new ResponseEntity<ConfigModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "configBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<ConfigModel, ConfigContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setConfigObjectFactory(final ObjectFactory<ConfigContext> configContextFactory) {
		this.configContextFactory = configContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
