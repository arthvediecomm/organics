/**
 *
 */
package com.arthvedi.organic.commons.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.commons.model.LanguageModel;
import com.arthvedi.organic.commons.representation.siren.LanguageRepresentation;

/**
 * @author varma
 *
 */
@Component("languageModelToLanguageRepresentationConverter")
public class LanguageModelToLanguageRepresentationConverter extends PropertyCopyingConverter<LanguageModel, LanguageRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public LanguageRepresentation convert(final LanguageModel source) {

        LanguageRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<LanguageRepresentation> factory) {
        super.setFactory(factory);
    }

}
