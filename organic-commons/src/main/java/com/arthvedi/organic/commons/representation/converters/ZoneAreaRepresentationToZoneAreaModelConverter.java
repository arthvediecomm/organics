/**
 *
 */
package com.arthvedi.organic.commons.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.commons.model.ZoneAreaModel;
import com.arthvedi.organic.commons.representation.siren.ZoneAreaRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("zoneAreaRepresentationToZoneAreaModelConverter")
public class ZoneAreaRepresentationToZoneAreaModelConverter
		extends PropertyCopyingConverter<ZoneAreaRepresentation, ZoneAreaModel> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public ZoneAreaModel convert(final ZoneAreaRepresentation source) {

		ZoneAreaModel target = super.convert(source);

		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(final ObjectFactory<ZoneAreaModel> factory) {
		super.setFactory(factory);
	}

}
