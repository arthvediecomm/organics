/**
 *
 */
package com.arthvedi.organic.commons.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.commons.model.CityModel;
import com.arthvedi.organic.commons.representation.siren.CityRepresentation;

/**
 * @author varma
 *
 */
@Component("cityModelToCityRepresentationConverter")
public class CityModelToCityRepresentationConverter extends PropertyCopyingConverter<CityModel, CityRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public CityRepresentation convert(final CityModel source) {

        CityRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<CityRepresentation> factory) {
        super.setFactory(factory);
    }

}
