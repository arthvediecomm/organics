package com.arthvedi.organic.commons.service;

import static com.arthvedi.organic.enums.Status.ACTIVE;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.commons.domain.Charge;
import com.arthvedi.organic.commons.repository.ChargeRepository;

@Component
public class ChargeService implements IChargeService {
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private ChargeRepository chargeRepository;

	@Override
	public Charge create(Charge charge) {
		charge.setCreatedDate(new LocalDateTime());
		charge.setUserCreated("varma");
		charge.setStatus(ACTIVE.name());
		charge = chargeRepository.save(charge);
		return charge;
	}

	@Override
	public void deleteCharge(String chargeId) {

	}

	@Override
	public Charge getCharge(String chargeId) {
		return chargeRepository.findOne(chargeId);
	}

	@Override
	public List<Charge> getAll() {
		return null;
	}

	@Override
	public Charge updateCharge(Charge charge) {
		Charge charges = chargeRepository.findOne(charge.getId());
		try {
			nonNullBeanUtils.copyProperties(charges, charge);

		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

		charges.setModifiedDate(new LocalDateTime());
		charges.setUserModified("admin");
		charge = chargeRepository.save(charges);
		return charge;
	}

	@Override
	public List<Charge> getChargeList() {
		List<Charge> charge = (List<Charge>) chargeRepository.findAll();
		List<Charge> charges = new ArrayList<Charge>();
		for (Charge charg : charge) {
			Charge chrg = new Charge();

			chrg.setChargeName(charg.getChargeName().toLowerCase());
			chrg.setId(charg.getId());
			chrg.setSellerBranchChargeses(charg.getSellerBranchChargeses());
			charges.add(chrg);
		}
		return charges;
	}

	@Override
	public boolean checkChargeExistsByName(String chargeName) {
		return chargeRepository.findByChargeName(chargeName) != null;
	}

	@Override
	public Charge getChargeByName(String lowerCase) {
		return null;
	}

}
