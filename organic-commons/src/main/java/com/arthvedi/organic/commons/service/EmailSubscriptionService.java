package com.arthvedi.organic.commons.service;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.commons.domain.EmailSubscription;
import com.arthvedi.organic.commons.repository.EmailSubscriptionRepository;
/*
*@Author varma
*/
@Component
public class EmailSubscriptionService implements IEmailSubscriptionService{
@Autowired 
NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private EmailSubscriptionRepository emailSubscriptionRepository;
	@Override
	public EmailSubscription create(EmailSubscription emailSubscription) {

		emailSubscription.setCreatedDate(new LocalDateTime());
		emailSubscription.setUserCreated("varma");
		return emailSubscriptionRepository.save(emailSubscription);
	}

	@Override
	public void deleteEmailSubscription(String emailSubscriptionId) {
		
	}

	@Override
	public EmailSubscription getEmailSubscription(String emailSubscriptionId) {
		 return emailSubscriptionRepository.findOne(emailSubscriptionId);
	}

	@Override
	public List<EmailSubscription> getAll() {
		return null;
	}

	@Override
	public EmailSubscription updateEmailSubscription(EmailSubscription emailSubscription) {
EmailSubscription emailSubscriptions=emailSubscriptionRepository.findOne(emailSubscription.getId());
try{
	nonNullBeanUtils.copyProperties(emailSubscriptions, emailSubscription);
}catch(IllegalAccessException e){
	e.printStackTrace();
}catch(InvocationTargetException e){
	e.printStackTrace();
}
emailSubscriptions.setModifiedDate(new LocalDateTime());
emailSubscriptions.setUserModified("admin");
emailSubscription =emailSubscriptionRepository.save(emailSubscriptions);
		return emailSubscription;
	}

	@Override
	public EmailSubscription getEmailSubscriptionByUserEmail(String userEmail) {
		return null;
	}

}
