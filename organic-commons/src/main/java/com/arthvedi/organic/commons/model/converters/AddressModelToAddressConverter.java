/**
 *
 */
package com.arthvedi.organic.commons.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.commons.domain.Address;
import com.arthvedi.organic.commons.model.AddressModel;

/**
 * @author Jay
 *
 */
@Component("addressModelToAddressConverter")
public class AddressModelToAddressConverter implements
		Converter<AddressModel, Address> {
	@Autowired
	private ObjectFactory<Address> addressFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public Address convert(final AddressModel source) {
		Address address = addressFactory.getObject();
		BeanUtils.copyProperties(source, address);

		return address;
	}

	@Autowired
	public void setAddressFactory(final ObjectFactory<Address> addressFactory) {
		this.addressFactory = addressFactory;
	}

}
