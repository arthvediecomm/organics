package com.arthvedi.organic.commons.service;

import java.util.List;

import com.arthvedi.organic.commons.domain.ZoneArea;

/*
*@Author varma
*/
public interface IZoneAreaService {

	ZoneArea create(ZoneArea zoneArea);

	void deleteZoneArea(String zoneAreaId);

	ZoneArea getZoneArea(String zoneAreaId);

	List<ZoneArea> getAll();

	ZoneArea updateZoneArea(ZoneArea zoneArea);

	ZoneArea getZoneAreaByZoneAreaName(String zoneArea);

	List<ZoneArea> getZoneAreaList();

	boolean checkZoneAreaExistsByZoneAreaName(String lowerCase);

	List<ZoneArea> getZoneAreaListByZone(String zoneId);
}
