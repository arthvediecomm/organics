package com.arthvedi.organic.commons.service;

import java.util.List;

import com.arthvedi.organic.commons.domain.City;
/*
*@Author varma
*/
public interface ICityService {
	
	City create(City city);

	void deleteCity(String cityId);

	City getCity(String cityId);

	List<City> getAll();

	City updateCity(City city);

	List<City> getCityList();

	City getCityByCityName(String cityName);
}
