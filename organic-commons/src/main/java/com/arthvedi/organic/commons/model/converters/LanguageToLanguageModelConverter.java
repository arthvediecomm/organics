package com.arthvedi.organic.commons.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.commons.domain.Language;
import com.arthvedi.organic.commons.model.LanguageModel;

@Component("languageToLanguageModelConverter")
public class LanguageToLanguageModelConverter
        implements Converter<Language, LanguageModel> {
    @Autowired
    private ObjectFactory<LanguageModel> languageModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public LanguageModel convert(final Language source) {
        LanguageModel languageModel = languageModelFactory.getObject();
        BeanUtils.copyProperties(source, languageModel);

        return languageModel;
    }

    @Autowired
    public void setLanguageModelFactory(
            final ObjectFactory<LanguageModel> languageModelFactory) {
        this.languageModelFactory = languageModelFactory;
    }
}
