package com.arthvedi.organic.commons.service;

import java.util.List;

import com.arthvedi.organic.commons.domain.Tax;

/*
 *@Author varma
 */
public interface ITaxService {

	Tax create(Tax tax);

	void deleteTax(String taxId);

	Tax getTax(String taxId);

	List<Tax> getAll();

	Tax updateTax(Tax tax);

	boolean checkTaxNameExists(String taxName);

	Tax getTaxByTaxName(String lowerCase);

	List<Tax> getTaxList();
}
