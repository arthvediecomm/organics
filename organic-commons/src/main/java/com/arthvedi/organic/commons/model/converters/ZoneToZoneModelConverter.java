package com.arthvedi.organic.commons.model.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.organic.commons.domain.Zone;
import com.arthvedi.organic.commons.model.ZoneAreaModel;
import com.arthvedi.organic.commons.model.ZoneModel;

@Component("zoneToZoneModelConverter")
public class ZoneToZoneModelConverter implements Converter<Zone, ZoneModel> {
	@Autowired
	private ObjectFactory<ZoneModel> zoneModelFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public ZoneModel convert(final Zone source) {
		ZoneModel zoneModel = zoneModelFactory.getObject();
		BeanUtils.copyProperties(source, zoneModel);
		if (source.getCity() != null) {
			zoneModel.setCityId(source.getCity().getId());
		}
		if(source.getZoneName()!=null){
			zoneModel.setZoneName(StringUtils.capitalize(source.getZoneName()));
		}
		if (CollectionUtils.isNotEmpty(source.getZoneAreas())) {
			List<ZoneAreaModel> converted = (List<ZoneAreaModel>) conversionService.convert(source.getZoneAreas(),
					TypeDescriptor.forObject(source.getZoneAreas()),
					CollectionTypeDescriptor.forType(TypeDescriptor.valueOf(List.class), ZoneAreaModel.class));
			zoneModel.getZoneAreaModels().addAll(converted);
		}
		return zoneModel;
	}

	@Autowired
	public void setZoneModelFactory(final ObjectFactory<ZoneModel> zoneModelFactory) {
		this.zoneModelFactory = zoneModelFactory;
	}
}
