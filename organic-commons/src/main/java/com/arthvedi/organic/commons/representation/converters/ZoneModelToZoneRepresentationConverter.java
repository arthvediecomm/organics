/**
 *
 */
package com.arthvedi.organic.commons.representation.converters;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.CollectionTypeDescriptor;
import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.commons.model.ZoneModel;
import com.arthvedi.organic.commons.representation.siren.ZoneAreaRepresentation;
import com.arthvedi.organic.commons.representation.siren.ZoneRepresentation;

/**
 * @author varma
 *
 */
@Component("zoneModelToZoneRepresentationConverter")
public class ZoneModelToZoneRepresentationConverter extends PropertyCopyingConverter<ZoneModel, ZoneRepresentation> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public ZoneRepresentation convert(final ZoneModel source) {
		ZoneRepresentation target = super.convert(source);
		
		if (CollectionUtils.isNotEmpty(source.getZoneAreaModels())) {
			List<ZoneAreaRepresentation> converted = (List<ZoneAreaRepresentation>) conversionService
					.convert(source.getZoneAreaModels(),
							TypeDescriptor.forObject(source.getZoneAreaModels()),
							CollectionTypeDescriptor.forType(TypeDescriptor.valueOf(List.class),
									ZoneAreaRepresentation.class));
			target.getZoneAreaRepresentations().addAll(converted);
		}

		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(final ObjectFactory<ZoneRepresentation> factory) {
		super.setFactory(factory);
	}

}
