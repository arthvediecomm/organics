package com.arthvedi.organic.commons.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;

/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ZoneAreaContext implements IBusinessDelegateContext {

	private String all;
	private String zoneId;
	private String zoneAreaId;
	private String zoneAreaList;

	public String getZoneAreaList() {
		return zoneAreaList;
	}

	public void setZoneAreaList(String zoneAreaList) {
		this.zoneAreaList = zoneAreaList;
	}

	public String getZoneAreaId() {
		return zoneAreaId;
	}

	public void setZoneAreaId(String zoneAreaId) {
		this.zoneAreaId = zoneAreaId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

	public String getZoneId() {
		return zoneId;
	}

	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}

}
