package com.arthvedi.organic.commons.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.commons.businessdelegate.context.ZoneContext;
import com.arthvedi.organic.commons.domain.Zone;
import com.arthvedi.organic.commons.domain.ZoneArea;
import com.arthvedi.organic.commons.model.ZoneAreaModel;
import com.arthvedi.organic.commons.model.ZoneModel;
import com.arthvedi.organic.commons.service.IZoneAreaService;
import com.arthvedi.organic.commons.service.IZoneService;

@Service
public class ZoneBusinessDelegate implements
		IBusinessDelegate<ZoneModel, ZoneContext, IKeyBuilder<String>, String> {

	@Autowired
	private IZoneService zoneService;
	@Autowired
	private IZoneAreaService zoneAreaService;
	@Autowired
	private ConversionService conversionService;

	@Override
	@Transactional
	public ZoneModel create(ZoneModel model) {
		validateModel(model);
		boolean zoneName = checkZoneExists(model.getZoneName());
		if (zoneName) {
			model.setStatusMessage("FAILURE::: " + model.getZoneName()
					+ " Already Exists");
			return model;
		} else {
			Zone zone = zoneService.create((Zone) conversionService.convert(
					model, forObject(model), valueOf(Zone.class)));
			if (zone.getId() != null && !model.getZoneAreaModels().isEmpty()) {
				zone.setZoneAreas(addZoneAreas(zone, model.getZoneAreaModels()));
			}
			model = convertToZoneModel(zone);
			model.setStatusMessage("SUCCESS::: " + model.getZoneName()
					+ " Created SuccessFully");
		}
		return model;
	}

	@Transactional
	private Set<ZoneArea> addZoneAreas(Zone zone,
			List<ZoneAreaModel> zoneAreaModels) {
		Set<ZoneArea> zoneAreas = new HashSet<ZoneArea>();
		for (ZoneAreaModel zoneAreaModel : zoneAreaModels) {
			zoneAreaModel.setZoneId(zone.getId());
			ZoneArea zoneArea = zoneAreaService
					.create((ZoneArea) conversionService.convert(zoneAreaModel,
							forObject(zoneAreaModel), valueOf(ZoneArea.class)));
			zoneAreas.add(zoneArea);
		}
		return zoneAreas;
	}

	private ZoneModel convertToZoneModel(Zone zone) {
		return (ZoneModel) conversionService.convert(zone, forObject(zone),
				valueOf(ZoneModel.class));
	}

	private boolean checkZoneExists(String zoneName) {
		return zoneService.checkZoneExistsByZoneName(zoneName);
	}

	private void validateModel(ZoneModel model) {
		Validate.notNull(model, "Invalid Input");
		Validate.notEmpty(model.getZoneName(), "Invalid ZoneName");

	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, ZoneContext context) {

	}

	@Override
	public ZoneModel edit(IKeyBuilder<String> keyBuilder, ZoneModel model) {
		validateModel(model);
		if (model.getZoneName() != null) {
			Zone zon = zoneService.getZoneByZoneName(model.getZoneName()
					.toLowerCase());
			if (zon == null || model.getId().equals(zon.getId())) {
				Zone zone = zoneService.updateZone((Zone) conversionService
						.convert(model, forObject(model), valueOf(Zone.class)));
				model = convertToZoneModel(zone);
				model.setStatusMessage("SUCCESS::: Zone with name '"
						+ model.getZoneName() + "' created successfully");
			} else {
				model.setStatusMessage("FAILURE ::: Zone with name '"
						+ model.getZoneName() + "' already exists");
			}
		}
		return model;
	}

	@Override
	public ZoneModel getByKey(IKeyBuilder<String> keyBuilder,
			ZoneContext context) {
		Zone zone = zoneService.getZone(keyBuilder.build().toString());
		ZoneModel model = conversionService.convert(zone, ZoneModel.class);
		return model;
	}

	@Override
	public Collection<ZoneModel> getCollection(ZoneContext context) {
		List<Zone> zone = new ArrayList<Zone>();
		if (context.getZoneList() != null) {
			zone = zoneService.getZoneList();
		}
		if (context.getCityId() != null) {
			zone = zoneService.getZoneByCityId(context.getCityId());
		}
		List<ZoneModel> zoneModels = (List<ZoneModel>) conversionService
				.convert(
						zone,
						TypeDescriptor.forObject(zone),
						TypeDescriptor.collection(List.class,
								TypeDescriptor.valueOf(ZoneModel.class)));
		return zoneModels;
	}

	@Override
	public ZoneModel edit(IKeyBuilder<String> keyBuilder, ZoneModel model,
			ZoneContext context) {
		throw new UnsupportedOperationException("Operation not implemented yet");
	}

}
