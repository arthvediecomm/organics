package com.arthvedi.organic.commons.service;

import static com.arthvedi.organic.enums.Status.ACTIVE;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.commons.domain.City;
import com.arthvedi.organic.commons.repository.CityRepository;

/*
 *@Author varma
 */
@Component
public class CityService implements ICityService {
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private CityRepository cityRepository;

	@Override
	public City create(City city) {
		city.setCityName(city.getCityName().toLowerCase());
		city.setStatus(ACTIVE.name());
		city.setUserCreated("varma");
		city.setCreatedDate(new LocalDateTime());
		return cityRepository.save(city);
	}

	@Override
	public void deleteCity(String cityId) {

	}

	@Override
	public City getCity(String cityId) {
		return cityRepository.findOne(cityId);
	}

	@Override
	public List<City> getAll() {
		return null;
	}

	@Override
	public City updateCity(City city) {
		City citys = cityRepository.findOne(city.getId());
		try {
			nonNullBeanUtils.copyProperties(citys, city);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		citys.setModifiedDate(new LocalDateTime());
		citys.setUserModified("admin");
		city = cityRepository.save(citys);
		return city;
	}

	@Override
	public List<City> getCityList() {
		List<City> city = (List<City>) cityRepository.findAll();
		List<City> cities = new ArrayList<City>();
		for (City cits : city) {
			City ciy = new City();
			ciy.setCityName(cits.getCityName());
			ciy.setGeographicalName(cits.getGeographicalName());
			ciy.setLanguage(cits.getLanguage());
			ciy.setId(cits.getId());
			ciy.setStatus(cits.getStatus());
			cities.add(ciy);
		}
		return cities;
	}

	@Override
	public City getCityByCityName(String cityName) {
		return cityRepository.findByCityName(cityName);
	}

}
