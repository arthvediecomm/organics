package com.arthvedi.organic.commons.service;

import static com.arthvedi.organic.enums.Status.ACTIVE;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.commons.domain.Config;
import com.arthvedi.organic.commons.repository.ConfigRepository;

@Component
public class ConfigService implements IConfigService {

	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private ConfigRepository configRepository;

	@Override
	public Config create(final Config config) {

		config.setConfigName(config.getConfigName().toLowerCase());
		config.setConfigValue(config.getConfigValue());
		config.setCreatedDate(new LocalDateTime());
		config.setStatus(ACTIVE.name());
		config.setUserCreated("Admin");
		return configRepository.save(config);
	}

	@Override
	public void deleteConfig(final String configId) {
	}

	@Override
	public List<Config> getAll() {
		return null;
	}

	@Override
	public Config getConfig(final String configId) {
		return null;
	}

	@Override
	public boolean getConfigByConfigName(final String configName) {
		return false;
	}

	@Override
	public Config updateConfig(final Config config) {

		Config con = configRepository.findOne(config.getId());
		try {
			nonNullBeanUtils.copyProperties(con, config);
		} catch (IllegalAccessException e) {
			e.printStackTrace();

		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		con.setConfigName(config.getConfigName().toLowerCase());
		con.setModifiedDate(new LocalDateTime());
		con.setUserModified("Admin");
		con.setStatus(ACTIVE.name());

		return configRepository.save(con);
	}
}
