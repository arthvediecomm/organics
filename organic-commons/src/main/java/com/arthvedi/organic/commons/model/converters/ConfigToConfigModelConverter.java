package com.arthvedi.organic.commons.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.commons.domain.Config;
import com.arthvedi.organic.commons.model.ConfigModel;

@Component("configToConfigModelConverter")
public class ConfigToConfigModelConverter implements Converter<Config, ConfigModel> {
	@Autowired
	private ObjectFactory<ConfigModel> configModelFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public ConfigModel convert(final Config source) {
		ConfigModel configModel = configModelFactory.getObject();
		BeanUtils.copyProperties(source, configModel);

		return configModel;
	}

	@Autowired
	public void setconfigModelFactory(final ObjectFactory<ConfigModel> configModelFactory) {
		this.configModelFactory = configModelFactory;
	}
}
