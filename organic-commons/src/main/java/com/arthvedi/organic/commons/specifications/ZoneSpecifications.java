package com.arthvedi.organic.commons.specifications;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.arthvedi.organic.commons.domain.Zone;

public class ZoneSpecifications {

	public static Specification<Zone> zonesByCityId(String cityId) {
		return new Specification<Zone>() {

			@Override
			public Predicate toPredicate(Root<Zone> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.get("city").get("id"), cityId);
			}
		};

	}
}
