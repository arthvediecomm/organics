/**
 *
 */
package com.arthvedi.organic.commons.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.commons.model.EmailSubscriptionModel;
import com.arthvedi.organic.commons.representation.siren.EmailSubscriptionRepresentation;

/**
 * @author varma
 *
 */
@Component("emailSubscriptionModelToEmailSubscriptionRepresentationConverter")
public class EmailSubscriptionModelToEmailSubscriptionRepresentationConverter extends PropertyCopyingConverter<EmailSubscriptionModel, EmailSubscriptionRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public EmailSubscriptionRepresentation convert(final EmailSubscriptionModel source) {

        EmailSubscriptionRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<EmailSubscriptionRepresentation> factory) {
        super.setFactory(factory);
    }

}
