/**
 *
 */
package com.arthvedi.organic.commons.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.commons.model.ConfigModel;
import com.arthvedi.organic.commons.representation.siren.ConfigRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("configRepresentationToConfigModelConverter")
public class ConfigRepresentationToConfigModelConverter extends
		PropertyCopyingConverter<ConfigRepresentation, ConfigModel> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public ConfigModel convert(final ConfigRepresentation source) {

		ConfigModel target = super.convert(source);

		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(final ObjectFactory<ConfigModel> factory) {
		super.setFactory(factory);
	}

}
