package com.arthvedi.organic.commons.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.commons.domain.Config;
import com.arthvedi.organic.commons.model.ConfigModel;

@Component("configModelToConfigConverter")
public class ConfigModelToConfigConverter implements Converter<ConfigModel, Config> {

	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private ObjectFactory<Config> configFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public Config convert(final ConfigModel source) {
		Config config = configFactory.getObject();
		BeanUtils.copyProperties(source, config);

		return config;
	}

	@Autowired
	public void setconfigFactory(final ObjectFactory<Config> configFactory) {
		this.configFactory = configFactory;
	}
}
