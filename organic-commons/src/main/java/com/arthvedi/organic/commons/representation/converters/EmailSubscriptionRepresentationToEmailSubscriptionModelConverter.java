/**
 *
 */
package com.arthvedi.organic.commons.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.commons.model.EmailSubscriptionModel;
import com.arthvedi.organic.commons.representation.siren.EmailSubscriptionRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("emailSubscriptionRepresentationToEmailSubscriptionModelConverter")
public class EmailSubscriptionRepresentationToEmailSubscriptionModelConverter extends PropertyCopyingConverter<EmailSubscriptionRepresentation, EmailSubscriptionModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public EmailSubscriptionModel convert(final EmailSubscriptionRepresentation source) {

        EmailSubscriptionModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<EmailSubscriptionModel> factory) {
        super.setFactory(factory);
    }

}
