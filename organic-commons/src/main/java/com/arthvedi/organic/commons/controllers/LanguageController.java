package com.arthvedi.organic.commons.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.commons.businessdelegate.context.LanguageContext;
import com.arthvedi.organic.commons.model.LanguageModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/language", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class LanguageController {

	private IBusinessDelegate<LanguageModel, LanguageContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<LanguageContext> languageContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<LanguageModel> createLanguage(@RequestBody final LanguageModel languageModel) {
		businessDelegate.create(languageModel);
		return new ResponseEntity<LanguageModel>(languageModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<LanguageModel> edit(@PathVariable(value = "id") final String languageId,
			@RequestBody final LanguageModel languageModel) {

		businessDelegate.edit(keyBuilderFactory.getObject().withId(languageId), languageModel);
		return new ResponseEntity<LanguageModel>(languageModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<LanguageModel>>> getAll() {
		LanguageContext languageContext = languageContextFactory.getObject();
		languageContext.setAll("all");
		Collection<LanguageModel> languageModels = businessDelegate.getCollection(languageContext);
		IModelWrapper<Collection<LanguageModel>> models = new CollectionModelWrapper<LanguageModel>(
				LanguageModel.class, languageModels);
		return new ResponseEntity<IModelWrapper<Collection<LanguageModel>>>(models, HttpStatus.OK);
	}
	@RequestMapping(method = RequestMethod.GET, value = "/languagelist", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<LanguageModel>>> getLanguageList() {
		LanguageContext languageContext = languageContextFactory.getObject();
		languageContext.setLanguageList("languageList");
		Collection<LanguageModel> languageModels = businessDelegate.getCollection(languageContext);
		IModelWrapper<Collection<LanguageModel>> models = new CollectionModelWrapper<LanguageModel>(
				LanguageModel.class, languageModels);
		return new ResponseEntity<IModelWrapper<Collection<LanguageModel>>>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<LanguageModel> getLanguage(@PathVariable(value = "id") final String languageId) {
		LanguageContext languageContext = languageContextFactory.getObject();

		LanguageModel model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(languageId),
				languageContext);
		return new ResponseEntity<LanguageModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "languageBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<LanguageModel, LanguageContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setLanguageObjectFactory(final ObjectFactory<LanguageContext> languageContextFactory) {
		this.languageContextFactory = languageContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
