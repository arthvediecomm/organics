/**
 *
 */
package com.arthvedi.organic.commons.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.commons.domain.City;
import com.arthvedi.organic.commons.domain.Zone;
import com.arthvedi.organic.commons.model.ZoneModel;

/**
 * @author Jay
 *
 */
@Component("zoneModelToZoneConverter")
public class ZoneModelToZoneConverter implements Converter<ZoneModel, Zone> {
	@Autowired
	private ObjectFactory<Zone> zoneFactory;
	@Autowired
	private ConversionService conversionService;

	@Autowired
	private NullAwareBeanUtilsBean nonNullAwareBeanUtils;

	@Override
	public Zone convert(final ZoneModel source) {
		Zone zone = zoneFactory.getObject();
		BeanUtils.copyProperties(source, zone);
		if (source.getZoneName() != null) {
			zone.setZoneName(source.getZoneName().toLowerCase());
		}
		if (source.getCityId() != null) {
			City city = new City();
			city.setId(source.getCityId());
			zone.setCity(city);

		}

		return zone;
	}

	@Autowired
	public void setZoneFactory(final ObjectFactory<Zone> zoneFactory) {
		this.zoneFactory = zoneFactory;
	}

}
