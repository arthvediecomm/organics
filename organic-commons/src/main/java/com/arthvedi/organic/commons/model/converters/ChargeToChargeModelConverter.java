package com.arthvedi.organic.commons.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.commons.domain.Charge;
import com.arthvedi.organic.commons.model.ChargeModel;

@Component("chargeToChargeModelConverter")
public class ChargeToChargeModelConverter
        implements Converter<Charge, ChargeModel> {
    @Autowired
    private ObjectFactory<ChargeModel> chargeModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public ChargeModel convert(final Charge source) {
        ChargeModel chargeModel = chargeModelFactory.getObject();
        BeanUtils.copyProperties(source, chargeModel);

        return chargeModel;
    }

    @Autowired
    public void setChargeModelFactory(
            final ObjectFactory<ChargeModel> chargeModelFactory) {
        this.chargeModelFactory = chargeModelFactory;
    }
}
