package com.arthvedi.organic.commons.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.commons.businessdelegate.context.ReturnPolicyContext;
import com.arthvedi.organic.commons.model.ReturnPolicyModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/returnpolicy", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class ReturnPolicyController {

	private IBusinessDelegate<ReturnPolicyModel, ReturnPolicyContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<ReturnPolicyContext> returnPolicyContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<ReturnPolicyModel> createReturnPolicy(@RequestBody final ReturnPolicyModel returnPolicyModel) {
		businessDelegate.create(returnPolicyModel);
		return new ResponseEntity<ReturnPolicyModel>(returnPolicyModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<ReturnPolicyModel> edit(@PathVariable(value = "id") final String returnPolicyId,
			@RequestBody final ReturnPolicyModel returnPolicyModel) {

		businessDelegate.edit(keyBuilderFactory.getObject().withId(returnPolicyId), returnPolicyModel);
		return new ResponseEntity<ReturnPolicyModel>(returnPolicyModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<ReturnPolicyModel>>> getAll() {
		ReturnPolicyContext returnPolicyContext = returnPolicyContextFactory.getObject();
		returnPolicyContext.setAll("all");
		Collection<ReturnPolicyModel> returnPolicyModels = businessDelegate.getCollection(returnPolicyContext);
		IModelWrapper<Collection<ReturnPolicyModel>> models = new CollectionModelWrapper<ReturnPolicyModel>(
				ReturnPolicyModel.class, returnPolicyModels);
		return new ResponseEntity<IModelWrapper<Collection<ReturnPolicyModel>>>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<ReturnPolicyModel> getReturnPolicy(@PathVariable(value = "id") final String returnPolicyId) {
		ReturnPolicyContext returnPolicyContext = returnPolicyContextFactory.getObject();

		ReturnPolicyModel model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(returnPolicyId),
				returnPolicyContext);
		return new ResponseEntity<ReturnPolicyModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "returnPolicyBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<ReturnPolicyModel, ReturnPolicyContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setReturnPolicyObjectFactory(final ObjectFactory<ReturnPolicyContext> returnPolicyContextFactory) {
		this.returnPolicyContextFactory = returnPolicyContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
