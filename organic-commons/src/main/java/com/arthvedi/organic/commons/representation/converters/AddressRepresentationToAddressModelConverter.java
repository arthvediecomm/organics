/**
 *
 */
package com.arthvedi.organic.commons.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.commons.model.AddressModel;
import com.arthvedi.organic.commons.representation.siren.AddressRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("addressRepresentationToAddressModelConverter")
public class AddressRepresentationToAddressModelConverter extends
		PropertyCopyingConverter<AddressRepresentation, AddressModel> {

	@Autowired
	private ConversionService conversionService;

	@Override
	public AddressModel convert(final AddressRepresentation source) {
		AddressModel target = super.convert(source);

		return target;
	}

	@Autowired
	public void setConversionService(final ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	@Override
	@Autowired
	public void setFactory(final ObjectFactory<AddressModel> factory) {
		super.setFactory(factory);
	}

}
