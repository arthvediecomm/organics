package com.arthvedi.organic.commons.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.commons.domain.Address;
import com.arthvedi.organic.commons.model.AddressModel;

@Component("addressToAddressModelConverter")
public class AddressToAddressModelConverter
        implements Converter<Address, AddressModel> {
    @Autowired
    private ObjectFactory<AddressModel> addressModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public AddressModel convert(final Address source) {
        AddressModel addressModel = addressModelFactory.getObject();
        BeanUtils.copyProperties(source, addressModel);

        return addressModel;
    }

    @Autowired
    public void setAddressModelFactory(
            final ObjectFactory<AddressModel> addressModelFactory) {
        this.addressModelFactory = addressModelFactory;
    }
}
