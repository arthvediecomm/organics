/**
 *
 */
package com.arthvedi.organic.commons.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.commons.domain.Tax;
import com.arthvedi.organic.commons.model.TaxModel;

/**
 * @author Jay
 *
 */
@Component("taxModelToTaxConverter")
public class TaxModelToTaxConverter implements Converter<TaxModel, Tax> {
    @Autowired
    private ObjectFactory<Tax> taxFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public Tax convert(final TaxModel source) {
        Tax tax = taxFactory.getObject();
        BeanUtils.copyProperties(source, tax);

        return tax;
    }

    @Autowired
    public void setTaxFactory(final ObjectFactory<Tax> taxFactory) {
        this.taxFactory = taxFactory;
    }

}
