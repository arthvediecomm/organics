/**
 *
 */
package com.arthvedi.organic.commons.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.commons.domain.City;
import com.arthvedi.organic.commons.domain.Language;
import com.arthvedi.organic.commons.model.CityModel;

/**
 * @author Jay
 *
 */
@Component("cityModelToCityConverter")
public class CityModelToCityConverter implements Converter<CityModel, City> {
	@Autowired
	private ObjectFactory<City> cityFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public City convert(final CityModel source) {
		City city = cityFactory.getObject();
		BeanUtils.copyProperties(source, city);
		if (source.getLanguageId() != null) {
			Language language = new Language();
			language.setId(source.getLanguageId());
			city.setLanguage(language);
		}
		return city;
	}

	@Autowired
	public void setCityFactory(final ObjectFactory<City> cityFactory) {
		this.cityFactory = cityFactory;
	}

}
