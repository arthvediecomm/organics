package com.arthvedi.organic.commons.service;

import static com.arthvedi.organic.enums.Status.ACTIVE;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.aspectj.apache.bcel.Constants;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.commons.domain.ZoneArea;
import com.arthvedi.organic.commons.repository.ZoneAreaRepository;

/*
 *@Author varma
 */
@Component
public class ZoneAreaService implements IZoneAreaService, Constants {

	@Autowired
	private NullAwareBeanUtilsBean nonNullAwareBeanUtils;
	@Autowired
	private ZoneAreaRepository zoneAreaRepository;

	@Override
	@Transactional
	public ZoneArea create(ZoneArea zoneArea) {
		zoneArea.setAreaName(zoneArea.getAreaName().toLowerCase());
		zoneArea.setCreatedDate(new LocalDateTime());
		zoneArea.setStatus(ACTIVE.name());
		zoneArea.setUserCreated("Naveen");
		return zoneAreaRepository.save(zoneArea);
	}

	@Override
	public void deleteZoneArea(String zoneAreaId) {

	}

	@Override
	public ZoneArea getZoneArea(String zoneAreaId) {
		return zoneAreaRepository.findOne(zoneAreaId);
	}

	@Override
	public List<ZoneArea> getAll() {
		return null;
	}

	@Override
	@Transactional
	public ZoneArea updateZoneArea(ZoneArea zoneArea) {
		ZoneArea zonearea = zoneAreaRepository.findOne(zoneArea.getId());
		zoneArea.getZone();
		try {
			nonNullAwareBeanUtils.copyProperties(zonearea, zoneArea);
		} catch (IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}

		zonearea.setModifiedDate(new LocalDateTime());
		zonearea.setUserModified("ADMIN");

		return zoneAreaRepository.save(zonearea);
	}

	@Override
	public ZoneArea getZoneAreaByZoneAreaName(String zoneAreaName) {
		return zoneAreaRepository.findByZoneAreaName(zoneAreaName);
	}

	@Override
	public List<ZoneArea> getZoneAreaList() {

		List<ZoneArea> zoneAreas = (List<ZoneArea>) zoneAreaRepository
				.findAll();
		List<ZoneArea> zoneAreases = new ArrayList<ZoneArea>();
		for (ZoneArea z : zoneAreas) {
			ZoneArea zoneArea = new ZoneArea();
			zoneArea.setId(z.getId());
			zoneArea.setAreaName(z.getAreaName());
			zoneArea.setStatus(z.getStatus());
			zoneArea.setZone(z.getZone());
			zoneAreases.add(zoneArea);
		}
		return zoneAreases;

	}

	@Override
	public boolean checkZoneAreaExistsByZoneAreaName(String zoneAreaName) {
		return zoneAreaRepository.findByZoneAreaName(zoneAreaName) != null;
	}

	@Override
	public List<ZoneArea> getZoneAreaListByZone(String zoneId) {
		Specification<ZoneArea> zoneAreasByZone = new Specification<ZoneArea>() {

			@Override
			public Predicate toPredicate(Root<ZoneArea> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.get("zone").get("id"), zoneId);
			}
		};
		return zoneAreaRepository.findAll(zoneAreasByZone);
	}

}
