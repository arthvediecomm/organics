package com.arthvedi.organic.commons.service;

import java.util.List;

import com.arthvedi.organic.commons.domain.ReturnPolicy;
/*
*@Author varma
*/
public interface IReturnPolicyService {
	
	ReturnPolicy create(ReturnPolicy returnPolicy);

	void deleteReturnPolicy(String returnPolicyId);

	ReturnPolicy getReturnPolicy(String returnPolicyId);

	List<ReturnPolicy> getAll();

	ReturnPolicy updateReturnPolicy(ReturnPolicy returnPolicy);
}
