package com.arthvedi.organic.commons.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.commons.businessdelegate.context.AddressContext;
import com.arthvedi.organic.commons.model.AddressModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/address", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class AddressController {

	private IBusinessDelegate<AddressModel, AddressContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<AddressContext> addressContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<AddressModel> createAddress(@RequestBody final AddressModel addressModel) {
		businessDelegate.create(addressModel);
		return new ResponseEntity<AddressModel>(addressModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<AddressModel> edit(@PathVariable(value = "id") final String addressId,
			@RequestBody final AddressModel addressModel) {

		businessDelegate.edit(keyBuilderFactory.getObject().withId(addressId), addressModel);
		return new ResponseEntity<AddressModel>(addressModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<AddressModel>>> getAll() {
		AddressContext addressContext = addressContextFactory.getObject();
		addressContext.setAll("all");
		Collection<AddressModel> addressModels = businessDelegate.getCollection(addressContext);
		IModelWrapper<Collection<AddressModel>> models = new CollectionModelWrapper<AddressModel>(
				AddressModel.class, addressModels);
		return new ResponseEntity<IModelWrapper<Collection<AddressModel>>>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<AddressModel> getAddress(@PathVariable(value = "id") final String addressId) {
		AddressContext addressContext = addressContextFactory.getObject();

		AddressModel model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(addressId),
				addressContext);
		return new ResponseEntity<AddressModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "addressBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<AddressModel, AddressContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setAddressObjectFactory(final ObjectFactory<AddressContext> addressContextFactory) {
		this.addressContextFactory = addressContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
