package com.arthvedi.organic.commons.service;

import static com.arthvedi.organic.commons.specifications.ZoneSpecifications.zonesByCityId;
import static com.arthvedi.organic.enums.Status.ACTIVE;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.aspectj.apache.bcel.Constants;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.commons.domain.Zone;
import com.arthvedi.organic.commons.repository.ZoneRepository;

@Component
@Transactional
public class ZoneService implements IZoneService, Constants {

	@Autowired
	private NullAwareBeanUtilsBean nonNullAwareBeanUtils;
	@Autowired
	private ZoneRepository zoneRepository;

	@Override
	@Transactional
	public Zone create(Zone zone) {
		zone.setZoneName(zone.getZoneName().toString());
		zone.setUserCreated("Naveen");
		zone.setCreatedDate(new LocalDateTime());
		zone.setStatus(ACTIVE.name());
		return zoneRepository.save(zone);
	}

	@Override
	public void deleteZone(String zoneId) {

	}

	@Override
	public Zone getZone(String zoneId) {
		return zoneRepository.findOne(zoneId);
	}

	@Override
	public Zone updateZone(Zone zone) {

		Zone zones = zoneRepository.findOne(zone.getId());
		try {
			nonNullAwareBeanUtils.copyProperties(zones, zone);
		} catch (IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}
		if (zone.getZoneName() != null) {
			zones.setZoneName(zone.getZoneName().toLowerCase());
		}
		zones.setModifiedDate(new LocalDateTime());
		zones.setUserModified("ADMIN");
		return zoneRepository.save(zones);
	}

	@Override
	public boolean checkZoneExistsByZoneName(String zoneName) {
		return zoneRepository.findByZoneName(zoneName) != null;
	}

	@Override
	public List<Zone> getZoneList() {
		List<Zone> zones = (List<Zone>) zoneRepository.findAll();
		List<Zone> zoneses = new ArrayList<Zone>();
		for (Zone z : zones) {
			Zone zone = new Zone();
			zone.setId(z.getId());
			zone.setZoneName(z.getZoneName());
			zone.setCity(z.getCity());
			zone.setStatus(z.getStatus());
			zone.setDescription(z.getDescription());
			zone.setGeographicalZoneName(z.getGeographicalZoneName());
			zoneses.add(zone);
		}
		return zoneses;
	}

	@Override
	public Zone getZoneByZoneName(String zoneName) {
		return zoneRepository.findByZoneName(zoneName);
	}

	@Override
	public List<Zone> getZoneByCityId(String cityId) {
		return zoneRepository.findAll(zonesByCityId(cityId));
	}

}
