/**
 *
 */
package com.arthvedi.organic.commons.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.commons.model.OrganicRoleModel;
import com.arthvedi.organic.commons.representation.siren.OrganicRoleRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("organicRoleRepresentationToOrganicRoleModelConverter")
public class OrganicRoleRepresentationToOrganicRoleModelConverter extends PropertyCopyingConverter<OrganicRoleRepresentation, OrganicRoleModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public OrganicRoleModel convert(final OrganicRoleRepresentation source) {

        OrganicRoleModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<OrganicRoleModel> factory) {
        super.setFactory(factory);
    }

}
