package com.arthvedi.organic.commons.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.commons.businessdelegate.context.ReturnPolicyContext;
import com.arthvedi.organic.commons.domain.ReturnPolicy;
import com.arthvedi.organic.commons.model.ReturnPolicyModel;
import com.arthvedi.organic.commons.service.IReturnPolicyService;

/*
*@Author varma
*/

@Service
public class ReturnPolicyBusinessDelegate
		implements IBusinessDelegate<ReturnPolicyModel, ReturnPolicyContext, IKeyBuilder<String>, String> {

	@Autowired
	private IReturnPolicyService returnPolicyService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public ReturnPolicyModel create(ReturnPolicyModel model) {
ReturnPolicy returnPolicy =returnPolicyService.create((ReturnPolicy)conversionService .convert(model, forObject(model), valueOf(ReturnPolicy.class)));
		model=convertToReturnPoliceModel(returnPolicy);
		return model;
	}

	private ReturnPolicyModel convertToReturnPoliceModel(ReturnPolicy returnPolicy) {
		return (ReturnPolicyModel)conversionService.convert(returnPolicy, forObject(returnPolicy), valueOf(ReturnPolicyModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, ReturnPolicyContext context) {

	}

	@Override
	public ReturnPolicyModel edit(IKeyBuilder<String> keyBuilder, ReturnPolicyModel model) {
		ReturnPolicy returnPolicy = returnPolicyService.getReturnPolicy(keyBuilder.build().toString());
		
		returnPolicy = returnPolicyService.updateReturnPolicy((ReturnPolicy)conversionService .convert(model, forObject(model),valueOf(ReturnPolicy.class)));
		model=convertToReturnPoliceModel(returnPolicy);
		return model;
	}

	@Override
	public ReturnPolicyModel getByKey(IKeyBuilder<String> keyBuilder, ReturnPolicyContext context) {
		ReturnPolicy returnPolicy = returnPolicyService.getReturnPolicy(keyBuilder.build().toString());
		ReturnPolicyModel model = conversionService.convert(returnPolicy, ReturnPolicyModel.class);
		return model;
	}

	@Override
	public Collection<ReturnPolicyModel> getCollection(ReturnPolicyContext context) {
		List<ReturnPolicy> returnPolicy = new ArrayList<ReturnPolicy>();
		
		List<ReturnPolicyModel> returnPolicyModels = (List<ReturnPolicyModel>) conversionService.convert(
				returnPolicy, TypeDescriptor.forObject(returnPolicy),
				TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(ReturnPolicyModel.class)));
		return returnPolicyModels;
	}

	@Override
	public ReturnPolicyModel edit(IKeyBuilder<String> keyBuilder,
			ReturnPolicyModel model, ReturnPolicyContext context) {
		throw new UnsupportedOperationException("Operation not implemented yet");
	}

}
