/**
 *
 */
package com.arthvedi.organic.commons.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.commons.domain.Language;
import com.arthvedi.organic.commons.model.LanguageModel;

/**
 * @author Jay
 *
 */
@Component("languageModelToLanguageConverter")
public class LanguageModelToLanguageConverter implements Converter<LanguageModel, Language> {
    @Autowired
    private ObjectFactory<Language> languageFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public Language convert(final LanguageModel source) {
        Language language = languageFactory.getObject();
        BeanUtils.copyProperties(source, language);

        return language;
    }

    @Autowired
    public void setLanguageFactory(final ObjectFactory<Language> languageFactory) {
        this.languageFactory = languageFactory;
    }

}
