package com.arthvedi.organic.commons.service;

import java.util.List;

import com.arthvedi.organic.commons.domain.Address;
/*
*@Author varma
*/
public interface IAddressService {
	
	Address create(Address address);

	void deleteAddress(String addressId);

	Address getAddress(String addressId);

	List<Address> getAll();

	Address updateAddress(Address address);
}
