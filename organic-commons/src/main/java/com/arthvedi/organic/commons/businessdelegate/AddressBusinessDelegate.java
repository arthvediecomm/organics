package com.arthvedi.organic.commons.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.commons.businessdelegate.context.AddressContext;
import com.arthvedi.organic.commons.domain.Address;
import com.arthvedi.organic.commons.model.AddressModel;
import com.arthvedi.organic.commons.service.IAddressService;

/*
 *@Author varma
 */

@Service
public class AddressBusinessDelegate
		implements
		IBusinessDelegate<AddressModel, AddressContext, IKeyBuilder<String>, String> {

	@Autowired
	private IAddressService addressService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public AddressModel create(AddressModel model) {
		validateModel(model);
		Address address = addressService.create((Address) conversionService
				.convert(model, forObject(model), valueOf(Address.class)));
		model = convertToAddressModel(address);
		model.setStatusMessage("SUCCESS::: Address Created Successfully.");
		return model;
	}

	private void validateModel(AddressModel model) {

	}

	private AddressModel convertToAddressModel(Address address) {
		return (AddressModel) conversionService.convert(address,
				forObject(address), valueOf(AddressModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, AddressContext context) {

	}

	@Override
	public AddressModel edit(IKeyBuilder<String> keyBuilder, AddressModel model) {
		Address address = addressService.getAddress(keyBuilder.build()
				.toString());

		address = addressService.updateAddress((Address) conversionService
				.convert(model, forObject(model), valueOf(Address.class)));
		model = convertToAddressModel(address);
		model.setStatusMessage("SUCCESS::: Address Updated Successfully.");

		return model;
	}

	@Override
	public AddressModel getByKey(IKeyBuilder<String> keyBuilder,
			AddressContext context) {
		Address address = addressService.getAddress(keyBuilder.build()
				.toString());
		AddressModel model = conversionService.convert(address,
				AddressModel.class);
		return model;
	}

	@Override
	public Collection<AddressModel> getCollection(AddressContext context) {
		List<Address> address = new ArrayList<Address>();

		List<AddressModel> addressModels = (List<AddressModel>) conversionService
				.convert(
						address,
						TypeDescriptor.forObject(address),
						TypeDescriptor.collection(List.class,
								TypeDescriptor.valueOf(AddressModel.class)));
		return addressModels;
	}

	@Override
	public AddressModel edit(IKeyBuilder<String> keyBuilder,
			AddressModel model, AddressContext context) {
		throw new UnsupportedOperationException("Operation not implemented yet");
	}

}
