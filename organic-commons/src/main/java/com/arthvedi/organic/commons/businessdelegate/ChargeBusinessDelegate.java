package com.arthvedi.organic.commons.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.commons.businessdelegate.context.ChargeContext;
import com.arthvedi.organic.commons.domain.Charge;
import com.arthvedi.organic.commons.model.ChargeModel;
import com.arthvedi.organic.commons.service.IChargeService;

/*
 *@Author varma
 */

@Service
public class ChargeBusinessDelegate 
		implements
		IBusinessDelegate<ChargeModel, ChargeContext, IKeyBuilder<String>, String> {
	@Autowired
	private IChargeService chargeService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public ChargeModel create(ChargeModel model) {
		validateModel(model);
		boolean chargeName = checkChargeNameExists(model.getChargeName()
				.toLowerCase());

		if (chargeName) {
			model.setStatusMessage("FAILURE::: " + model.getChargeName()
					+ " Already Exists..!!");
			return model;
		} else {

			Charge charge = chargeService.create((Charge) conversionService
					.convert(model, forObject(model), valueOf(Charge.class)));
			model = convertToChargeModel(charge);
		}

		return model;
	}

	private boolean checkChargeNameExists(String chargeName) {
		return chargeService.checkChargeExistsByName(chargeName);
	}

	private ChargeModel convertToChargeModel(Charge charge) {
		return (ChargeModel) conversionService.convert(charge,
				forObject(charge), valueOf(ChargeModel.class));
	}

	private void validateModel(ChargeModel model) {
		Validate.notNull(model, "Invalid Input");
		Validate.notBlank(model.getChargeName(), "Invalid ChargeName");

	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, ChargeContext context) {

	}

	@Override
	public ChargeModel edit(IKeyBuilder<String> keyBuilder, ChargeModel model) {
		Charge charge = chargeService.getCharge(keyBuilder.build().toString());
		validateEditModel(model);
		if (model.getChargeName() != null) {
			Charge charg = chargeService.getChargeByName(model.getChargeName()
					.toLowerCase());

			if (charg == null || model.getId().equals(charg.getId())) {
				Charge charges = chargeService
						.updateCharge((Charge) conversionService.convert(model,
								forObject(model), valueOf(Charge.class)));
				model = convertToChargeModel(charges);
				model.setStatusMessage("SUCCESS::: Charge with name'"
						+ model.getChargeName() + "' successfully updated");
			} else {
				model.setStatusMessage("FAILURE::: Charge with name'"
						+ model.getChargeName() + "' already exists");
			}

		}
		return model;
	}

	private void validateEditModel(ChargeModel model) {

	}

	@Override
	public ChargeModel getByKey(IKeyBuilder<String> keyBuilder,
			ChargeContext context) {
		Charge charge = chargeService.getCharge(keyBuilder.build().toString());
		ChargeModel model = conversionService
				.convert(charge, ChargeModel.class);
		return model;
	}

	@Override
	public Collection<ChargeModel> getCollection(ChargeContext context) {
		List<Charge> charge = new ArrayList<Charge>();
		if (context.getAll() != null) {
			charge = chargeService.getAll();
		}
		if (context.getChargeList() != null) {
			charge = chargeService.getChargeList();
		}
		List<ChargeModel> chargeModels = (List<ChargeModel>) conversionService
				.convert(
						charge,
						TypeDescriptor.forObject(charge),
						TypeDescriptor.collection(List.class,
								TypeDescriptor.valueOf(ChargeModel.class)));
		return chargeModels;
	}

	@Override
	public ChargeModel edit(IKeyBuilder<String> keyBuilder, ChargeModel model,
			ChargeContext context) {
		throw new UnsupportedOperationException("Operation not implemented yet");
	}

}
