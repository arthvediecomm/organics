package com.arthvedi.organic.commons.businessdelegate.context;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.arthvedi.core.businessdelegate.context.IBusinessDelegateContext;
/*
*@Author varma
*/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class EmailSubscriptionContext implements IBusinessDelegateContext {

	private String emailSubscriptionId;
	private String all;
	
	
	public String getEmailSubscriptionId() {
		return emailSubscriptionId;
	}

	public void setEmailSubscriptionId(String emailSubscriptionId) {
		this.emailSubscriptionId = emailSubscriptionId;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

}
