package com.arthvedi.organic.commons.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.commons.businessdelegate.context.ZoneAreaContext;
import com.arthvedi.organic.commons.model.ZoneAreaModel;
/**
 *
 */
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/zonearea", produces = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class ZoneAreaController {

	private IBusinessDelegate<ZoneAreaModel, ZoneAreaContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<ZoneAreaContext> zoneAreaContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create")
	public ResponseEntity<ZoneAreaModel> createZoneArea(
			@RequestBody ZoneAreaModel zoneAreaModel) {
		zoneAreaModel = businessDelegate.create(zoneAreaModel);
		return new ResponseEntity<ZoneAreaModel>(zoneAreaModel,
				HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	public ResponseEntity<ZoneAreaModel> edit(
			@PathVariable(value = "id") final String zoneAreaId,
			@RequestBody ZoneAreaModel zoneAreaModel) {
		zoneAreaModel = businessDelegate.edit(keyBuilderFactory.getObject()
				.withId(zoneAreaId), zoneAreaModel);
		return new ResponseEntity<ZoneAreaModel>(zoneAreaModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all")
	public ResponseEntity<IModelWrapper<Collection<ZoneAreaModel>>> getAll() {
		ZoneAreaContext zoneAreaContext = zoneAreaContextFactory.getObject();
		zoneAreaContext.setAll("all");
		Collection<ZoneAreaModel> zoneAreaModels = businessDelegate
				.getCollection(zoneAreaContext);
		IModelWrapper<Collection<ZoneAreaModel>> models = new CollectionModelWrapper<ZoneAreaModel>(
				ZoneAreaModel.class, zoneAreaModels);
		return new ResponseEntity<IModelWrapper<Collection<ZoneAreaModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = { "/zonearealist",
			"/sellerapp/zonearealist" })
	public ResponseEntity<IModelWrapper<Collection<ZoneAreaModel>>> getZoneAreaList() {
		ZoneAreaContext zoneAreaContext = zoneAreaContextFactory.getObject();
		zoneAreaContext.setZoneAreaList("zoneAreaList");
		Collection<ZoneAreaModel> zoneAreaModel = businessDelegate
				.getCollection(zoneAreaContext);
		IModelWrapper<Collection<ZoneAreaModel>> models = new CollectionModelWrapper<ZoneAreaModel>(
				ZoneAreaModel.class, zoneAreaModel);
		return new ResponseEntity<IModelWrapper<Collection<ZoneAreaModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = {
			"/{zoneId}/zonearealist", "/sellerapp/{zoneId}/zonearealist" })
	public ResponseEntity<IModelWrapper<Collection<ZoneAreaModel>>> getAll(
			@PathVariable(value = "zoneId") final String zoneId) {
		ZoneAreaContext zoneAreaContext = zoneAreaContextFactory.getObject();
		zoneAreaContext.setZoneId(zoneId);
		Collection<ZoneAreaModel> zoneAreaModel = businessDelegate
				.getCollection(zoneAreaContext);
		IModelWrapper<Collection<ZoneAreaModel>> models = new CollectionModelWrapper<ZoneAreaModel>(
				ZoneAreaModel.class, zoneAreaModel);
		return new ResponseEntity<IModelWrapper<Collection<ZoneAreaModel>>>(
				models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<ZoneAreaModel> getZoneArea(
			@PathVariable(value = "id") final String zoneAreaId) {
		ZoneAreaContext zoneAreaContext = zoneAreaContextFactory.getObject();

		ZoneAreaModel model = businessDelegate.getByKey(keyBuilderFactory
				.getObject().withId(zoneAreaId), zoneAreaContext);
		return new ResponseEntity<ZoneAreaModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "zoneAreaBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<ZoneAreaModel, ZoneAreaContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setZoneAreaObjectFactory(
			final ObjectFactory<ZoneAreaContext> zoneAreaContextFactory) {
		this.zoneAreaContextFactory = zoneAreaContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(
			final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
