/**
 *
 */
package com.arthvedi.organic.commons.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.commons.model.AddressModel;
import com.arthvedi.organic.commons.representation.siren.AddressRepresentation;

/**
 * @author varma
 *
 */
@Component("addressModelToAddressRepresentationConverter")
public class AddressModelToAddressRepresentationConverter extends PropertyCopyingConverter<AddressModel, AddressRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public AddressRepresentation convert(final AddressModel source) {

        AddressRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<AddressRepresentation> factory) {
        super.setFactory(factory);
    }

}
