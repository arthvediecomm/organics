/**
 *
 */
package com.arthvedi.organic.commons.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.commons.model.ChargeModel;
import com.arthvedi.organic.commons.representation.siren.ChargeRepresentation;

/**
 * @author arthvedi
 *
 */
@Component("chargeRepresentationToChargeModelConverter")
public class ChargeRepresentationToChargeModelConverter extends PropertyCopyingConverter<ChargeRepresentation, ChargeModel> {

    @Autowired
    private ConversionService conversionService;

    @Override
    public ChargeModel convert(final ChargeRepresentation source) {

        ChargeModel target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<ChargeModel> factory) {
        super.setFactory(factory);
    }

}
