package com.arthvedi.organic.commons.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.commons.domain.City;
import com.arthvedi.organic.commons.model.CityModel;

@Component("cityToCityModelConverter")
public class CityToCityModelConverter
        implements Converter<City, CityModel> {
    @Autowired
    private ObjectFactory<CityModel> cityModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public CityModel convert(final City source) {
        CityModel cityModel = cityModelFactory.getObject();
        BeanUtils.copyProperties(source, cityModel);
if(source.getLanguage()!=null){
	cityModel.setLanguageId(source.getLanguage().getId());
}
        return cityModel;
    }

    @Autowired
    public void setCityModelFactory(
            final ObjectFactory<CityModel> cityModelFactory) {
        this.cityModelFactory = cityModelFactory;
    }
}
