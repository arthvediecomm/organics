package com.arthvedi.organic.commons.service;

import java.util.List;

import com.arthvedi.organic.commons.domain.Charge;

/*
 *@Author varma
 */
public interface IChargeService  {

	Charge create(Charge charge);

	void deleteCharge(String chargeId);

	Charge getCharge(String chargeId);

	boolean checkChargeExistsByName(String chargeName);

	List<Charge> getAll();

	Charge updateCharge(Charge charge);

	List<Charge> getChargeList();

	Charge getChargeByName(String lowerCase);
}
