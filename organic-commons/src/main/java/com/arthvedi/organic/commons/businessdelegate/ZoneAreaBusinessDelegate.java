package com.arthvedi.organic.commons.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.commons.businessdelegate.context.ZoneAreaContext;
import com.arthvedi.organic.commons.domain.ZoneArea;
import com.arthvedi.organic.commons.model.ZoneAreaModel;
import com.arthvedi.organic.commons.service.IZoneAreaService;

/*
 *@Author varma
 */

@Service
public class ZoneAreaBusinessDelegate
		implements
		IBusinessDelegate<ZoneAreaModel, ZoneAreaContext, IKeyBuilder<String>, String> {

	@Autowired
	private IZoneAreaService zoneAreaService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public ZoneAreaModel create(ZoneAreaModel model) {

		validateZone(model);
		boolean zoneAreaName = CheckZoneAreaExists(model.getAreaName()
				.toLowerCase());
		if (zoneAreaName) {
			model.setStatusMessage("FAILURE::: " + model.getAreaName()
					+ " Already Exists");
			return model;
		} else {
			ZoneArea zoneArea = zoneAreaService
					.create((ZoneArea) conversionService.convert(model,
							forObject(model), valueOf(ZoneArea.class)));
			model = convertToZoneAreaModel(zoneArea);
			model.setStatusMessage("SUCCESS::: " + model.getAreaName()
					+ " Created SuccessFully");
		}
		return model;
	}

	private ZoneAreaModel convertToZoneAreaModel(ZoneArea zoneArea) {
		return (ZoneAreaModel) conversionService.convert(zoneArea,
				forObject(zoneArea), valueOf(ZoneAreaModel.class));
	}

	private boolean CheckZoneAreaExists(String zoneAreaName) {
		return zoneAreaService.checkZoneAreaExistsByZoneAreaName(zoneAreaName);
	}

	private void validateZone(ZoneAreaModel model) {
		Validate.notNull(model, "Input Invalid");
		Validate.notBlank(model.getAreaName(), "Invalid ZoneAreaName");

	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, ZoneAreaContext context) {

	}

	@Override
	public ZoneAreaModel edit(IKeyBuilder<String> keyBuilder,
			ZoneAreaModel model) {
		if (model.getAreaName() != null) {
			ZoneArea zoneAr = zoneAreaService.getZoneAreaByZoneAreaName(model
					.getAreaName().toLowerCase());
			if (zoneAr == null || model.getId().equals(zoneAr.getId())) {
				ZoneArea zoneArea = zoneAreaService
						.updateZoneArea((ZoneArea) conversionService.convert(
								model, forObject(model),
								valueOf(ZoneArea.class)));
				model = convertToZoneAreaModel(zoneArea);
				model.setStatusMessage("SUCCESS::: ZoneArea with name '"
						+ model.getAreaName() + "' updated successfully");
			} else {
				model.setStatusMessage("FAILURE::: ZoneArea with name '"
						+ model.getAreaName() + "' already exists");
			}
		}

		return model;
	}

	@Override
	public ZoneAreaModel getByKey(IKeyBuilder<String> keyBuilder,
			ZoneAreaContext context) {
		ZoneArea zoneArea = zoneAreaService.getZoneArea(keyBuilder.build()
				.toString());
		ZoneAreaModel model = conversionService.convert(zoneArea,
				ZoneAreaModel.class);
		return model;
	}

	@Override
	public Collection<ZoneAreaModel> getCollection(ZoneAreaContext context) {
		List<ZoneArea> zoneArea = new ArrayList<ZoneArea>();
		if (context.getZoneAreaList() != null) {
			zoneArea = zoneAreaService.getZoneAreaList();
		}
		if (context.getZoneId() != null) {
			zoneArea = zoneAreaService.getZoneAreaListByZone(context
					.getZoneId());
		}
		List<ZoneAreaModel> zoneAreaModels = (List<ZoneAreaModel>) conversionService
				.convert(
						zoneArea,
						TypeDescriptor.forObject(zoneArea),
						TypeDescriptor.collection(List.class,
								TypeDescriptor.valueOf(ZoneAreaModel.class)));
		return zoneAreaModels;
	}

	@Override
	public ZoneAreaModel edit(IKeyBuilder<String> keyBuilder,
			ZoneAreaModel model, ZoneAreaContext context) {
		throw new UnsupportedOperationException("Operation not implemented yet");
	}

}
