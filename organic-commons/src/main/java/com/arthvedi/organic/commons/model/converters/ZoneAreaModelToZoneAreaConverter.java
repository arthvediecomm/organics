/**
 *
 */
package com.arthvedi.organic.commons.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.commons.domain.Zone;
import com.arthvedi.organic.commons.domain.ZoneArea;
import com.arthvedi.organic.commons.model.ZoneAreaModel;

/**
 * @author Jay
 *
 */
@Component("zoneAreaModelToZoneAreaConverter")
public class ZoneAreaModelToZoneAreaConverter implements Converter<ZoneAreaModel, ZoneArea> {
	@Autowired
	private ObjectFactory<ZoneArea> zoneAreaFactory;
	@Autowired
	private ConversionService conversionService;

	@Override
	public ZoneArea convert(final ZoneAreaModel source) {
		ZoneArea zoneArea = zoneAreaFactory.getObject();
		BeanUtils.copyProperties(source, zoneArea);
		if (source.getZoneId() != null) {
			Zone zone = new Zone();
			zone.setId(source.getZoneId());
			zoneArea.setZone(zone);

		}

		return zoneArea;
	}

	@Autowired
	public void setZoneAreaFactory(final ObjectFactory<ZoneArea> zoneAreaFactory) {
		this.zoneAreaFactory = zoneAreaFactory;
	}

}
