package com.arthvedi.organic.commons.service;

import java.util.List;

import com.arthvedi.organic.commons.domain.Zone;

/*
*@Author varma
*/
public interface IZoneService {

	Zone create(Zone zone);

	void deleteZone(String zoneId);

	Zone getZone(String zoneId);

	Zone updateZone(Zone zone);

	boolean checkZoneExistsByZoneName(String zoneName);

	List<Zone> getZoneList();

	Zone getZoneByZoneName(String zoneName);

	List<Zone> getZoneByCityId(String cityId);

}
