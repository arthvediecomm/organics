package com.arthvedi.organic.commons.service;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.NullAwareBeanUtilsBean;
import com.arthvedi.organic.commons.domain.ReturnPolicy;
import com.arthvedi.organic.commons.repository.ReturnPolicyRepository;

/*
*@Author varma
*/
@Component
public class ReturnPolicyService implements IReturnPolicyService {
	@Autowired
	private NullAwareBeanUtilsBean nonNullBeanUtils;
	@Autowired
	private ReturnPolicyRepository returnPolicyRepository;

	@Override
	public ReturnPolicy create(ReturnPolicy returnPolicy) {
		returnPolicy.setUserCreated("varma");
		returnPolicy.setCreatedDate(new LocalDateTime());
		returnPolicy = returnPolicyRepository.save(returnPolicy);
		return returnPolicy;
	}

	@Override
	public void deleteReturnPolicy(String returnPolicyId) {

	}

	@Override
	public ReturnPolicy getReturnPolicy(String returnPolicyId) {
		return returnPolicyRepository.findOne(returnPolicyId);
	}

	@Override
	public List<ReturnPolicy> getAll() {
		return null;
	}

	@Override
	public ReturnPolicy updateReturnPolicy(ReturnPolicy returnPolicy) {
		ReturnPolicy returnPolicys = returnPolicyRepository.findOne(returnPolicy.getId());
		try {
			nonNullBeanUtils.copyProperties(returnPolicys, returnPolicy);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		returnPolicys.setUserModified("admin");
		returnPolicys.setModifiedDate(new LocalDateTime());
		returnPolicy = returnPolicyRepository.save(returnPolicy);
		return returnPolicy;
	}

}
