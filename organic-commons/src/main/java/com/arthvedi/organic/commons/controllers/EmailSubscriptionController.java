package com.arthvedi.organic.commons.controllers;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.core.businessdelegate.model.SimpleIdKeyBuilder;
import com.arthvedi.core.model.CollectionModelWrapper;
import com.arthvedi.core.model.IModelWrapper;
import com.arthvedi.organic.commons.businessdelegate.context.EmailSubscriptionContext;
import com.arthvedi.organic.commons.model.EmailSubscriptionModel;
/**
*
*/
import com.google.code.siren4j.Siren4J;

/**
 * @author Administrator
 *
 */

@RestController
@RequestMapping(value = "/emailSubscription", produces = { MediaType.APPLICATION_JSON_VALUE,
		Siren4J.JSON_MEDIATYPE }, consumes = { MediaType.APPLICATION_JSON_VALUE, Siren4J.JSON_MEDIATYPE })
public class EmailSubscriptionController {

	private IBusinessDelegate<EmailSubscriptionModel, EmailSubscriptionContext, IKeyBuilder<String>, String> businessDelegate;
	private ObjectFactory<SimpleIdKeyBuilder> keyBuilderFactory;
	private ObjectFactory<EmailSubscriptionContext> emailSubscriptionContextFactory;

	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<EmailSubscriptionModel> createEmailSubscription(@RequestBody final EmailSubscriptionModel emailSubscriptionModel) {
		businessDelegate.create(emailSubscriptionModel);
		return new ResponseEntity<EmailSubscriptionModel>(emailSubscriptionModel, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<EmailSubscriptionModel> edit(@PathVariable(value = "id") final String emailSubscriptionId,
			@RequestBody final EmailSubscriptionModel emailSubscriptionModel) {

		businessDelegate.edit(keyBuilderFactory.getObject().withId(emailSubscriptionId), emailSubscriptionModel);
		return new ResponseEntity<EmailSubscriptionModel>(emailSubscriptionModel, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/all", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<IModelWrapper<Collection<EmailSubscriptionModel>>> getAll() {
		EmailSubscriptionContext emailSubscriptionContext = emailSubscriptionContextFactory.getObject();
		emailSubscriptionContext.setAll("all");
		Collection<EmailSubscriptionModel> emailSubscriptionModels = businessDelegate.getCollection(emailSubscriptionContext);
		IModelWrapper<Collection<EmailSubscriptionModel>> models = new CollectionModelWrapper<EmailSubscriptionModel>(
				EmailSubscriptionModel.class, emailSubscriptionModels);
		return new ResponseEntity<IModelWrapper<Collection<EmailSubscriptionModel>>>(models, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = { MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<EmailSubscriptionModel> getEmailSubscription(@PathVariable(value = "id") final String emailSubscriptionId) {
		EmailSubscriptionContext emailSubscriptionContext = emailSubscriptionContextFactory.getObject();

		EmailSubscriptionModel model = businessDelegate.getByKey(keyBuilderFactory.getObject().withId(emailSubscriptionId),
				emailSubscriptionContext);
		return new ResponseEntity<EmailSubscriptionModel>(model, HttpStatus.OK);
	}

	/**
	 * @param businessDelegate
	 */
	@Resource(name = "emailSubscriptionBusinessDelegate")
	public void setBusinessDelegate(
			final IBusinessDelegate<EmailSubscriptionModel, EmailSubscriptionContext, IKeyBuilder<String>, String> businessDelegate) {
		this.businessDelegate = businessDelegate;
	}

	@Autowired
	public void setEmailSubscriptionObjectFactory(final ObjectFactory<EmailSubscriptionContext> emailSubscriptionContextFactory) {
		this.emailSubscriptionContextFactory = emailSubscriptionContextFactory;
	}

	/**
	 * @param factory
	 */
	@Resource
	public void setKeyBuilderFactory(final ObjectFactory<SimpleIdKeyBuilder> factory) {
		keyBuilderFactory = factory;
	}

}
