package com.arthvedi.organic.commons.model.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.arthvedi.organic.commons.domain.Tax;
import com.arthvedi.organic.commons.model.TaxModel;

@Component("taxToTaxModelConverter")
public class TaxToTaxModelConverter
        implements Converter<Tax, TaxModel> {
    @Autowired
    private ObjectFactory<TaxModel> taxModelFactory;
    @Autowired
    private ConversionService conversionService;

    @Override
    public TaxModel convert(final Tax source) {
        TaxModel taxModel = taxModelFactory.getObject();
        BeanUtils.copyProperties(source, taxModel);

        return taxModel;
    }

    @Autowired
    public void setTaxModelFactory(
            final ObjectFactory<TaxModel> taxModelFactory) {
        this.taxModelFactory = taxModelFactory;
    }
}
