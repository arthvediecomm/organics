package com.arthvedi.organic.commons.businessdelegate;

import static org.springframework.core.convert.TypeDescriptor.forObject;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import com.arthvedi.core.businessdelegate.IBusinessDelegate;
import com.arthvedi.core.businessdelegate.model.IKeyBuilder;
import com.arthvedi.organic.commons.businessdelegate.context.TaxContext;
import com.arthvedi.organic.commons.domain.Tax;
import com.arthvedi.organic.commons.model.TaxModel;
import com.arthvedi.organic.commons.service.ITaxService;

/*
 *@Author varma
 */

@Service
public class TaxBusinessDelegate implements
		IBusinessDelegate<TaxModel, TaxContext, IKeyBuilder<String>, String> {

	@Autowired
	private ITaxService taxService;
	@Autowired
	private ConversionService conversionService;

	@Override
	public TaxModel create(TaxModel model) {
		validateTax(model);
		boolean taxName = CheckTaxNameExists(model.getTaxName());
		if (taxName) {
			model.setStatusMessage("FAILURE::: Tax with name "
					+ model.getTaxName() + " Already Exists");
			return model;
		} else {

			Tax tax = taxService.create((Tax) conversionService.convert(model,
					forObject(model), valueOf(Tax.class)));
			model = convertToTaxModel(tax);
			model.setStatusMessage("SUCCESS::: Tax with name"
					+ model.getTaxName() + " Created SuccessFully");
		}
		return null;
	}

	private boolean CheckTaxNameExists(String taxName) {
		return taxService.checkTaxNameExists(taxName);

	}

	private void validateTax(TaxModel model) {
		Validate.notNull(model, "input invalid");
		Validate.notBlank(model.getTaxName(), "Invalid TaxName");

	}

	private TaxModel convertToTaxModel(Tax tax) {
		return (TaxModel) conversionService.convert(tax, forObject(tax),
				valueOf(TaxModel.class));
	}

	@Override
	public void delete(IKeyBuilder<String> keyBuilder, TaxContext context) {

	}

	@Override
	public TaxModel edit(IKeyBuilder<String> keyBuilder, TaxModel model) {
		if (model.getTaxName() != null) {
			Tax tax = taxService.getTaxByTaxName(model.getTaxName()
					.toLowerCase());
			if (tax == null || model.getId().equals(tax.getId()))

				tax = taxService.updateTax((Tax) conversionService.convert(
						model, forObject(model), valueOf(Tax.class)));
			model = convertToTaxModel(tax);
			model.setStatusMessage("SUCCESS::: Tax updated successfully");
		} else {
			model.setStatusMessage("FAILURE::: Tax with name '"
					+ model.getTaxName() + "' already exists");
		}
		return model;
	}

	@Override
	public TaxModel getByKey(IKeyBuilder<String> keyBuilder, TaxContext context) {
		Tax tax = taxService.getTax(keyBuilder.build().toString());
		TaxModel model = conversionService.convert(tax, TaxModel.class);
		return model;
	}

	@Override
	public Collection<TaxModel> getCollection(TaxContext context) {
		List<Tax> tax = new ArrayList<Tax>();
if(context.getTaxList()!=null){
	tax=taxService.getTaxList();
}
		List<TaxModel> taxModels = (List<TaxModel>) conversionService.convert(
				tax,
				TypeDescriptor.forObject(tax),
				TypeDescriptor.collection(List.class,
						TypeDescriptor.valueOf(TaxModel.class)));
		return taxModels;
	}

	@Override
	public TaxModel edit(IKeyBuilder<String> keyBuilder, TaxModel model,
			TaxContext context) {
		throw new UnsupportedOperationException("Operation not implemented yet");
	}

}
