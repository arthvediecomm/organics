/**
 *
 */
package com.arthvedi.organic.commons.representation.converters;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.arthvedi.core.converter.PropertyCopyingConverter;
import com.arthvedi.organic.commons.model.ReturnPolicyModel;
import com.arthvedi.organic.commons.representation.siren.ReturnPolicyRepresentation;

/**
 * @author varma
 *
 */
@Component("returnPolicyModelToReturnPolicyRepresentationConverter")
public class ReturnPolicyModelToReturnPolicyRepresentationConverter extends PropertyCopyingConverter<ReturnPolicyModel, ReturnPolicyRepresentation> {

    @Autowired
    private ConversionService conversionService;

   @Override
    public ReturnPolicyRepresentation convert(final ReturnPolicyModel source) {

        ReturnPolicyRepresentation target = super.convert(source);

        return target;
    }

    @Autowired
    public void setConversionService(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    @Autowired
    public void setFactory(final ObjectFactory<ReturnPolicyRepresentation> factory) {
        super.setFactory(factory);
    }

}
