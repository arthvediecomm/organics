package com.arthvedi.organic.commons.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.commons.domain.ZoneArea;

/*
 *@Author Naveen
 */
public interface ZoneAreaRepository extends
		PagingAndSortingRepository<ZoneArea, Serializable>,
		JpaSpecificationExecutor<ZoneArea> {

	@Query("SELECT DISTINCT za FROM ZoneArea za WHERE za.areaName=?1")
	ZoneArea findByZoneAreaName(String zoneArea);

}
