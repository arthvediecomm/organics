package com.arthvedi.organic.commons.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.commons.domain.Zone;

/*
 *@Author varma
 */
public interface ZoneRepository extends
		PagingAndSortingRepository<Zone, Serializable>,
		JpaSpecificationExecutor<Zone> {

	@Query("SELECT DISTINCT z FROM Zone z WHERE z.zoneName=?1")
	Zone findByZoneName(String zoneName);

}
