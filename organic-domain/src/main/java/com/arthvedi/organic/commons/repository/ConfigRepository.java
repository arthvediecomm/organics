package com.arthvedi.organic.commons.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.commons.domain.Config;

public interface ConfigRepository extends
		PagingAndSortingRepository<Config, Serializable> {

}
