package com.arthvedi.organic.commons.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.commons.domain.Address;
/*
*@Author varma
*/
public interface AddressRepository extends PagingAndSortingRepository<Address, Serializable>{

}
