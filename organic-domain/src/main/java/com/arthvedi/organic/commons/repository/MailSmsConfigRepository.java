package com.arthvedi.organic.commons.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.commons.domain.MailSmsConfig;

/*
*@Author varma
*/
public interface MailSmsConfigRepository extends PagingAndSortingRepository<MailSmsConfig, Serializable> {

	@Query("SELECT msc FROM  MailSmsConfig msc WHERE msc.configAttributeValue=?1")
	MailSmsConfig findByName(String configAttributeName);

}
