package com.arthvedi.organic.commons.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.commons.domain.Charge;
/*
*@Author varma
*/
public interface ChargeRepository extends PagingAndSortingRepository<Charge, Serializable>{

	@Query("SELECT charg FROM Charge charg WHERE charg.chargeName=?1")
	Charge findByChargeName(String chargeName);

}
