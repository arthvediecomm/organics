package com.arthvedi.organic.commons.domain;

// Generated Feb 17, 2017 7:05:49 PM by Hibernate Tools 3.6.0.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.joda.time.LocalDateTime;
import org.springframework.stereotype.Component;

import com.arthvedi.core.domain.AbstractDomain;

/**
 * Config generated by hbm2java
 */
@Entity
@Component
@Table(name = "config", catalog = "organic")
public class Config extends AbstractDomain implements java.io.Serializable {

	private String configName;
	private String configValue;
	private String status;

	public Config() {
	}

	public Config(String id, String configName, String configValue,
			LocalDateTime createdDate, String userCreated) {
		this.id = id;
		this.configName = configName;
		this.configValue = configValue;
		this.createdDate = createdDate;
		this.userCreated = userCreated;
	}

	public Config(String id, String configName, String configValue,
			LocalDateTime createdDate, LocalDateTime modifiedDate,
			String userCreated, String userModified, String status) {
		this.id = id;
		this.configName = configName;
		this.configValue = configValue;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.userCreated = userCreated;
		this.userModified = userModified;
		this.status = status;
	}

	@Column(name = "config_name", nullable = false, length = 45)
	public String getConfigName() {
		return this.configName;
	}

	public void setConfigName(String configName) {
		this.configName = configName;
	}

	@Column(name = "config_value", nullable = false, length = 45)
	public String getConfigValue() {
		return this.configValue;
	}

	public void setConfigValue(String configValue) {
		this.configValue = configValue;
	}

	@Column(name = "status", length = 45)
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
