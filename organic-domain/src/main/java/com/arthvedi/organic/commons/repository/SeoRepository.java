package com.arthvedi.organic.commons.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.commons.domain.Seo;

/*
*@Author varma
*/
public interface SeoRepository extends PagingAndSortingRepository<Seo, Serializable> {

	@Query("SELECT s FROM Seo s where s.id = ?1")
	Seo findSeoBySeoId(String id);

}
