package com.arthvedi.organic.commons.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.commons.domain.ReturnPolicy;
/*
*@Author varma
*/
public interface ReturnPolicyRepository extends PagingAndSortingRepository<ReturnPolicy, Serializable>{

}
