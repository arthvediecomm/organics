package com.arthvedi.organic.commons.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.commons.domain.City;

/*
 *@Author varma
 */
public interface CityRepository extends
		PagingAndSortingRepository<City, Serializable> {

	@Query("SELECT ci FROM City ci WHERE ci.cityName = ?1")
	City findByCityName(String cityName);

}
