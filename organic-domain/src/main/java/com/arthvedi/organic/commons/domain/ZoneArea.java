package com.arthvedi.organic.commons.domain;

// Generated Feb 17, 2017 7:05:49 PM by Hibernate Tools 3.6.0.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.joda.time.LocalDateTime;
import org.springframework.stereotype.Component;

import com.arthvedi.core.domain.AbstractDomain;

/**
 * ZoneArea generated by hbm2java
 */
@Entity
@Component
@Table(name = "zone_area", catalog = "organic")
public class ZoneArea extends AbstractDomain implements java.io.Serializable {

	private Zone zone;
	private String areaName;
	private String status;

	public ZoneArea() {
	}

	public ZoneArea(String id, Zone zone, String areaName, String status,
			LocalDateTime createdDate, String userCreated) {
		this.id = id;
		this.zone = zone;
		this.areaName = areaName;
		this.status = status;
		this.createdDate = createdDate;
		this.userCreated = userCreated;
	}

	public ZoneArea(String id, Zone zone, String areaName, String status,
			LocalDateTime createdDate, LocalDateTime modifiedDate,
			String userCreated, String userModified) {
		this.id = id;
		this.zone = zone;
		this.areaName = areaName;
		this.status = status;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.userCreated = userCreated;
		this.userModified = userModified;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "zone_id", nullable = false)
	public Zone getZone() {
		return this.zone;
	}

	public void setZone(Zone zone) {
		this.zone = zone;
	}

	@Column(name = "area_name", nullable = false, length = 45)
	public String getAreaName() {
		return this.areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	@Column(name = "status", nullable = false, length = 45)
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
