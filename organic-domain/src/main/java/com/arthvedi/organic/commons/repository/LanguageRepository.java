package com.arthvedi.organic.commons.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.commons.domain.Language;

/*
 *@Author varma
 */
public interface LanguageRepository extends
		PagingAndSortingRepository<Language, Serializable> {
	@Query("SELECT lang FROM Language lang WHERE lang.name=?1")
	Language findByLanguageName(String name);

}
