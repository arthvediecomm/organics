package com.arthvedi.organic.commons.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.commons.domain.EmailSubscription;
/*
*@Author varma
*/
public interface EmailSubscriptionRepository extends PagingAndSortingRepository<EmailSubscription, Serializable>{

}
