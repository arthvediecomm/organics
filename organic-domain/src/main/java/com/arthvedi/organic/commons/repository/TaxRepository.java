package com.arthvedi.organic.commons.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.commons.domain.Tax;

/*
 *@Author varma
 */
public interface TaxRepository extends
		PagingAndSortingRepository<Tax, Serializable> {
	@Query("SELECT DISTINCT tx FROM Tax tx WHERE tx.taxName=?1")
	Tax findByTaxName(String taxName);

}
