package com.arthvedi.organic.offer.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.offer.domain.SellerProductApplicableOffers;

/*
 *@Author varma
 */
public interface SellerProductApplicableOffersRepository extends
		PagingAndSortingRepository<SellerProductApplicableOffers, Serializable> {

}
