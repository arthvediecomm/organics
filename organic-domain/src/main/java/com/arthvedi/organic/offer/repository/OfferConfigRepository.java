package com.arthvedi.organic.offer.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.offer.domain.OfferConfig;
/*
*@Author varma
*/
public interface OfferConfigRepository extends PagingAndSortingRepository<OfferConfig, Serializable>{

}
