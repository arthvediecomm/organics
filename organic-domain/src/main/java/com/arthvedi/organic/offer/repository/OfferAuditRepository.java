package com.arthvedi.organic.offer.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.offer.domain.OfferAudit;
/*
*@Author varma
*/
public interface OfferAuditRepository extends PagingAndSortingRepository<OfferAudit, Serializable>{

}
