package com.arthvedi.organic.offer.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.offer.domain.OfferExcludeConfig;
/*
*@Author varma
*/
public interface OfferExcludeConfigRepository extends PagingAndSortingRepository<OfferExcludeConfig, Serializable>{

}
