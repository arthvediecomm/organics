package com.arthvedi.organic.offer.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.joda.time.LocalDateTime;
import org.springframework.stereotype.Component;

import com.arthvedi.core.domain.AbstractDomain;
import com.arthvedi.organic.productcatalog.domain.SellerProduct;

@Entity
@Component
@Table(name = "seller_product_applicable_offers", catalog = "meatapp_ref")
public class SellerProductApplicableOffers extends AbstractDomain implements java.io.Serializable {

	private SellerProduct sellerProduct;
	private OfferFulfillment offerFulfillment;

	public SellerProductApplicableOffers() {
	}

	public SellerProductApplicableOffers(String id, SellerProduct sellerProduct, OfferFulfillment offerFulfillment,
			String userCreated, LocalDateTime createdDate) {
		this.id = id;
		this.sellerProduct = sellerProduct;
		this.offerFulfillment = offerFulfillment;
		this.userCreated = userCreated;
		this.createdDate = createdDate;
	}

	public SellerProductApplicableOffers(String id, SellerProduct sellerProduct, OfferFulfillment offerFulfillment,
			LocalDateTime createdDate, LocalDateTime modifiedDate, String userCreated, String userModified) {
		this.id = id;
		this.sellerProduct = sellerProduct;
		this.offerFulfillment = offerFulfillment;
		this.userCreated = userCreated;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.userModified = userModified;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "seller_product_id", nullable = false)
	public SellerProduct getSellerProduct() {
		return sellerProduct;
	}

	public void setSellerProduct(SellerProduct sellerProduct) {
		this.sellerProduct = sellerProduct;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "offer_fulfillment_id", nullable = false)
	public OfferFulfillment getOfferFulfillment() {
		return offerFulfillment;
	}

	public void setOfferFulfillment(OfferFulfillment offerFulfillment) {
		this.offerFulfillment = offerFulfillment;
	}

}
