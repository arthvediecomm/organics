package com.arthvedi.organic.offer.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.offer.domain.OfferFulfillment;
/*
*@Author varma
*/
public interface OfferFulfillmentRepository extends PagingAndSortingRepository<OfferFulfillment, Serializable>{

}
