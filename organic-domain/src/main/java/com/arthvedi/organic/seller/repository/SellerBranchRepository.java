package com.arthvedi.organic.seller.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.seller.domain.SellerBranch;
/*
*@Author varma
*/
public interface SellerBranchRepository extends PagingAndSortingRepository<SellerBranch, Serializable>{

	@Query("SELECT sb FROM SellerBranch sb WHERE sb.branchName=?1")
	SellerBranch findByBranchName(String branchName);

}
