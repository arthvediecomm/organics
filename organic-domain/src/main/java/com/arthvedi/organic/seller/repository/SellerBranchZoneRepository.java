package com.arthvedi.organic.seller.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.seller.domain.SellerBranchZone;

/*
*@Author varma
*/
public interface SellerBranchZoneRepository extends PagingAndSortingRepository<SellerBranchZone, Serializable>,JpaSpecificationExecutor<SellerBranchZone> {

	@Query("SELECT DISTINCT sbz FROM SellerBranchZone sbz WHERE sbz.zone.id=?1 and sbz.sellerBranch.id=?2")
	SellerBranchZone findSellerBranchZoneByZone(String zoneId, String sellerBranchId);

}
