package com.arthvedi.organic.seller.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.seller.domain.SellerBranchTax;

/*
*@Author varma
*/
public interface SellerBranchTaxRepository extends PagingAndSortingRepository<SellerBranchTax, Serializable>,JpaSpecificationExecutor<SellerBranchTax> {

	@Query("SELECT sbt FROM SellerBranchTax sbt WHERE sbt.tax.id=?1 AND sbt.sellerBranch.id=?2")
	SellerBranchTax findSellerBranchTaxByTax(String taxId,String sellerBranchId);

}
