package com.arthvedi.organic.seller.domain;

// Generated Feb 17, 2017 7:05:49 PM by Hibernate Tools 3.6.0.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.joda.time.LocalDateTime;
import org.springframework.stereotype.Component;

import com.arthvedi.core.domain.AbstractDomain;
import com.arthvedi.organic.user.domain.EcommerceUser;

/**
 * SellerBranchUser generated by hbm2java
 */
@Entity
@Component
@Table(name = "seller_branch_user", catalog = "organic")
public class SellerBranchUser extends AbstractDomain implements
		java.io.Serializable {

	private EcommerceUser ecommerceUser;
	private SellerBranch sellerBranch;
	private String sellerUserType;
	private String userRoleType;

	public SellerBranchUser() {
	}

	public SellerBranchUser(String id, EcommerceUser ecommerceUser,
			SellerBranch sellerBranch, String sellerUserType,
			String userRoleType, LocalDateTime createdDate, String userCreated) {
		this.id = id;
		this.ecommerceUser = ecommerceUser;
		this.sellerBranch = sellerBranch;
		this.sellerUserType = sellerUserType;
		this.userRoleType = userRoleType;
		this.createdDate = createdDate;
		this.userCreated = userCreated;
	}

	public SellerBranchUser(String id, EcommerceUser ecommerceUser,
			SellerBranch sellerBranch, String sellerUserType,
			String userRoleType, LocalDateTime createdDate, String userCreated,
			LocalDateTime modifiedDate, String userModified) {
		this.id = id;
		this.ecommerceUser = ecommerceUser;
		this.sellerBranch = sellerBranch;
		this.sellerUserType = sellerUserType;
		this.userRoleType = userRoleType;
		this.createdDate = createdDate;
		this.userCreated = userCreated;
		this.modifiedDate = modifiedDate;
		this.userModified = userModified;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ecommerce_user_id", nullable = false)
	public EcommerceUser getEcommerceUser() {
		return this.ecommerceUser;
	}

	public void setEcommerceUser(EcommerceUser ecommerceUser) {
		this.ecommerceUser = ecommerceUser;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "seller_branch_id", nullable = false)
	public SellerBranch getSellerBranch() {
		return this.sellerBranch;
	}

	public void setSellerBranch(SellerBranch sellerBranch) {
		this.sellerBranch = sellerBranch;
	}

	@Column(name = "seller_user_type", nullable = false, length = 45)
	public String getSellerUserType() {
		return this.sellerUserType;
	}

	public void setSellerUserType(String sellerUserType) {
		this.sellerUserType = sellerUserType;
	}

	@Column(name = "user_role_type", nullable = false, length = 45)
	public String getUserRoleType() {
		return this.userRoleType;
	}

	public void setUserRoleType(String userRoleType) {
		this.userRoleType = userRoleType;
	}

}
