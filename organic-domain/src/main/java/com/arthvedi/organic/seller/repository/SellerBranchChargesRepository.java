package com.arthvedi.organic.seller.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.seller.domain.SellerBranchCharges;

/*
 *@Author varma
 */
public interface SellerBranchChargesRepository extends
		JpaSpecificationExecutor<SellerBranchCharges>,
		PagingAndSortingRepository<SellerBranchCharges, Serializable> {

	@Query("SELECT sbc FROM SellerBranchCharges sbc WHERE sbc.charge.id=?1 AND sbc.sellerBranch.id=?2")
	SellerBranchCharges findByChargesId(String chargeId, String sellerBranchId);

}
