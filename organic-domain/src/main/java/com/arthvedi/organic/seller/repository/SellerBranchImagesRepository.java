package com.arthvedi.organic.seller.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.seller.domain.SellerBranchImages;

/*
*@Author varma
*/
public interface SellerBranchImagesRepository extends PagingAndSortingRepository<SellerBranchImages, Serializable> {

}
