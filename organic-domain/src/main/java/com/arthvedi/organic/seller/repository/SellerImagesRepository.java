package com.arthvedi.organic.seller.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.seller.domain.SellerImages;

/*
 *@Author varma
 */
public interface SellerImagesRepository extends PagingAndSortingRepository<SellerImages, Serializable> {

}
