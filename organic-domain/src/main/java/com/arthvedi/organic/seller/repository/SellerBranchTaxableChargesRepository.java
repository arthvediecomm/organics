package com.arthvedi.organic.seller.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.seller.domain.SellerBranchTaxableCharges;

/*
 *@Author varma
 */
public interface SellerBranchTaxableChargesRepository extends
		PagingAndSortingRepository<SellerBranchTaxableCharges, Serializable> {

	@Query("SELECT sbtc FROM SellerBranchTaxableCharges sbtc WHERE sbtc.sellerBranchTax.id=?1 AND sbtc.sellerBranchCharges.id=?2")
	SellerBranchTaxableCharges findByTaxAndCharges(String sellerBranchTaxId,
			String sellerBranchChargesId);

}
