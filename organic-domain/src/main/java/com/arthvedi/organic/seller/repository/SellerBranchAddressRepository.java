package com.arthvedi.organic.seller.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.seller.domain.SellerBranchAddress;

/*
 *@Author varma
 */
public interface SellerBranchAddressRepository extends
		PagingAndSortingRepository<SellerBranchAddress, Serializable> {

}
