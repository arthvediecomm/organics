package com.arthvedi.organic.seller.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.seller.domain.SellerBranchUser;

/*
 *@Author varma
 */
public interface SellerBranchUserRepository extends
		PagingAndSortingRepository<SellerBranchUser, Serializable>,
		JpaSpecificationExecutor<SellerBranchUser> {

	@Query("SELECT DISTINCT sbu FROM SellerBranchUser sbu JOIN sbu.ecommerceUser bu WHERE sbu.ecommerceUser.emailId=?1")
	SellerBranchUser findSellerBranchUserByEmailId(String emailId);

	@Query("SELECT DISTINCT sbu FROM SellerBranchUser sbu JOIN sbu.ecommerceUser bu WHERE sbu.ecommerceUser.phoneNo=?1")
	SellerBranchUser findSellerBranchUserByPhoneNo(String phoneNo);

}
