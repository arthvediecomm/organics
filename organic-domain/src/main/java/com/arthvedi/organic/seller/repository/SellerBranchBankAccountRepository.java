package com.arthvedi.organic.seller.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.seller.domain.SellerBranchBankAccount;
/*
*@Author varma
*/
public interface SellerBranchBankAccountRepository extends JpaSpecificationExecutor<SellerBranchBankAccount>, PagingAndSortingRepository<SellerBranchBankAccount, Serializable>{

}
