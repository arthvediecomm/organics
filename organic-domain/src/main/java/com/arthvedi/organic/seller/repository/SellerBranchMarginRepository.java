package com.arthvedi.organic.seller.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.seller.domain.SellerBranchMargin;
/*
*@Author varma
*/
public interface SellerBranchMarginRepository extends PagingAndSortingRepository<SellerBranchMargin, Serializable>{

}
