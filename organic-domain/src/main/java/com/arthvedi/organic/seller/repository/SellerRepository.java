package com.arthvedi.organic.seller.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.seller.domain.Seller;

/*
 *@Author varma
 */
public interface SellerRepository extends
		PagingAndSortingRepository<Seller, Serializable> {
	@Query("SELECT s FROM Seller s WHERE s.sellerName=?1")
	Seller findBySellerName(String sellerName);
}
