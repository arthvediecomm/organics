package com.arthvedi.organic.user.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.user.domain.UserSellerProductRating;
/*
*@Author varma
*/
public interface UserSellerProductRatingRepository extends PagingAndSortingRepository<UserSellerProductRating, Serializable>{

}
