package com.arthvedi.organic.user.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.user.domain.EcommerceRole;
/*
*@Author varma
*/
public interface EcommerceRoleRepository extends PagingAndSortingRepository<EcommerceRole, Serializable>{

}
