package com.arthvedi.organic.user.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.user.domain.UserWishlist;
/*
*@Author varma
*/
public interface UserWishlistRepository extends PagingAndSortingRepository<UserWishlist, Serializable>{

}
