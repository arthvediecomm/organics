package com.arthvedi.organic.user.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.user.domain.EcommerceUser;
/*
*@Author varma
*/
public interface EcommerceUserRepository extends PagingAndSortingRepository<EcommerceUser, Serializable>{

}
