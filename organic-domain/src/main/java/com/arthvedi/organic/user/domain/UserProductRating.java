package com.arthvedi.organic.user.domain;

// Generated Feb 7, 2017 6:51:47 PM by Hibernate Tools 4.3.1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.joda.time.LocalDateTime;
import org.springframework.stereotype.Component;

import com.arthvedi.core.domain.AbstractDomain;

/**
 * UserProductRating generated by hbm2java
 */
@Entity
@Component
@Table(name = "user_product_rating", catalog = "organic")
public class UserProductRating extends AbstractDomain implements
		java.io.Serializable {

	private String rating;
	private String productRatingStatus;
	private String comments;

	public UserProductRating() {
	}

	public UserProductRating(String id, LocalDateTime createdDate,
			String userCreated) {
		this.id = id;
		this.createdDate = createdDate;
		this.userCreated = userCreated;
	}

	public UserProductRating(String id, String rating,
			String productRatingStatus, String comments,
			LocalDateTime createdDate, LocalDateTime modifiedDate,
			String userCreated, String userModified) {
		this.id = id;
		this.rating = rating;
		this.productRatingStatus = productRatingStatus;
		this.comments = comments;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.userCreated = userCreated;
		this.userModified = userModified;
	}

	@Column(name = "rating", length = 45)
	public String getRating() {
		return this.rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	@Column(name = "product_rating_status", length = 45)
	public String getProductRatingStatus() {
		return this.productRatingStatus;
	}

	public void setProductRatingStatus(String productRatingStatus) {
		this.productRatingStatus = productRatingStatus;
	}

	@Column(name = "comments", length = 45)
	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

}
