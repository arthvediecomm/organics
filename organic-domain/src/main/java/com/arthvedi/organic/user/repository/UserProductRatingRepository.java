package com.arthvedi.organic.user.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.user.domain.UserProductRating;
/*
*@Author varma
*/
public interface UserProductRatingRepository extends PagingAndSortingRepository<UserProductRating, Serializable>{

}
