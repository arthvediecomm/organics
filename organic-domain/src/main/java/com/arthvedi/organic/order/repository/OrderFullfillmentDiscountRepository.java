package com.arthvedi.organic.order.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.order.domain.OrderFullfillmentDiscount;
/*
*@Author varma
*/
public interface OrderFullfillmentDiscountRepository extends PagingAndSortingRepository<OrderFullfillmentDiscount, Serializable>{

}
