package com.arthvedi.organic.order.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.order.domain.OrderStatusLog;
/*
*@Author varma
*/
public interface OrderStatusLogRepository extends PagingAndSortingRepository<OrderStatusLog, Serializable>{

}
