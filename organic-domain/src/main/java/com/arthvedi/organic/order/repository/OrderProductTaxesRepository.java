package com.arthvedi.organic.order.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.order.domain.OrderProductTaxes;
/*
*@Author varma
*/
public interface OrderProductTaxesRepository extends PagingAndSortingRepository<OrderProductTaxes, Serializable>{

}
