package com.arthvedi.organic.order.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.order.domain.OrderProduct;
/*
*@Author varma
*/
public interface OrderProductRepository extends PagingAndSortingRepository<OrderProduct, Serializable>{

}
