package com.arthvedi.organic.order.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.order.domain.OrderCharges;
/*
*@Author varma
*/
public interface OrderChargesRepository extends PagingAndSortingRepository<OrderCharges, Serializable>{

}
