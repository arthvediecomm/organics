package com.arthvedi.organic.order.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.order.domain.OrderProductStatusLog;
/*
*@Author varma
*/
public interface OrderProductStatusLogRepository extends PagingAndSortingRepository<OrderProductStatusLog, Serializable>{

}
