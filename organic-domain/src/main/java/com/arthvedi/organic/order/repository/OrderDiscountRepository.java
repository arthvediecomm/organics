package com.arthvedi.organic.order.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.order.domain.OrderDiscount;
/*
*@Author varma
*/
public interface OrderDiscountRepository extends PagingAndSortingRepository<OrderDiscount, Serializable>{

}
