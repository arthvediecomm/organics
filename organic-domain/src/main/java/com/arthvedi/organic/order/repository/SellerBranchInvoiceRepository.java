package com.arthvedi.organic.order.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.seller.domain.SellerBranchInvoice;
/*
*@Author varma
*/
public interface SellerBranchInvoiceRepository extends PagingAndSortingRepository<SellerBranchInvoice, Serializable>{

}
