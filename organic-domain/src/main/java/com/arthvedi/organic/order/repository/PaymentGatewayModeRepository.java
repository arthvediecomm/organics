package com.arthvedi.organic.order.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.order.domain.PaymentGatewayMode;
/*
*@Author varma
*/
public interface PaymentGatewayModeRepository extends PagingAndSortingRepository<PaymentGatewayMode, Serializable>{

}
