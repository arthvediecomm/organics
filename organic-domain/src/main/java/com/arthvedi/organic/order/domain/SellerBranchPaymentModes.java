package com.arthvedi.organic.order.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.joda.time.LocalDateTime;
import org.springframework.stereotype.Component;

import com.arthvedi.core.domain.AbstractDomain;
import com.arthvedi.organic.seller.domain.SellerBranch;

@Entity
@Component
@Table(name = "seller_branch_payment_modes", catalog = "organic")
public class SellerBranchPaymentModes extends AbstractDomain implements java.io.Serializable {

	private SellerBranch sellerBranch;
	private PaymentMode paymentMode;
	private String status;

	public SellerBranchPaymentModes() {
	}

	public SellerBranchPaymentModes(String id, SellerBranch sellerBranch, PaymentMode paymentMode, String status,
			LocalDateTime createdDate) {
		this.id = id;
		this.sellerBranch = sellerBranch;
		this.paymentMode = paymentMode;
		this.status = status;
		this.createdDate = createdDate;
	}

	public SellerBranchPaymentModes(String id, SellerBranch sellerBranch, PaymentMode paymentMode, String status,
			LocalDateTime createdDate, LocalDateTime modifiedDate, String userCreated, String userModified) {
		this.id = id;
		this.sellerBranch = sellerBranch;
		this.paymentMode = paymentMode;
		this.status = status;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.userCreated = userCreated;
		this.userModified = userModified;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "seller_branch_id", nullable = false)
	public SellerBranch getSellerBranch() {
		return sellerBranch;
	}

	public void setSellerBranch(SellerBranch sellerBranch) {
		this.sellerBranch = sellerBranch;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "payment_mode", nullable = false)
	public PaymentMode getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(PaymentMode paymentMode) {
		this.paymentMode = paymentMode;
	}

	@Column(name = "status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
