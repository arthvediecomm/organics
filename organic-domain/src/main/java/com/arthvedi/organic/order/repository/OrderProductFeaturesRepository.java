package com.arthvedi.organic.order.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.order.domain.OrderProductFeatures;
/*
*@Author varma
*/
public interface OrderProductFeaturesRepository extends PagingAndSortingRepository<OrderProductFeatures, Serializable>{

}
