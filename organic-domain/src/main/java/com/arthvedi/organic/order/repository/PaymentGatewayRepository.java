package com.arthvedi.organic.order.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.order.domain.PaymentGateway;
/*
*@Author varma
*/
public interface PaymentGatewayRepository extends PagingAndSortingRepository<PaymentGateway, Serializable>{

}
