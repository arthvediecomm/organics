package com.arthvedi.organic.order.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.order.domain.EcommerceOrder;



/*
*@Author varma
*/
public interface EcommerceOrderRepository extends PagingAndSortingRepository<EcommerceOrder, Serializable>,JpaSpecificationExecutor<EcommerceOrder>{

}
