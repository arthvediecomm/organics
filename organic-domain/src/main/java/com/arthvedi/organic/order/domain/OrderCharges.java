package com.arthvedi.organic.order.domain;

// Generated Feb 17, 2017 7:05:49 PM by Hibernate Tools 3.6.0.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.joda.time.LocalDateTime;

import com.arthvedi.core.domain.AbstractDomain;

/**
 * OrderCharges generated by hbm2java
 */
@Entity
@Table(name = "order_charges", catalog = "organic")
public class OrderCharges extends AbstractDomain implements
		java.io.Serializable {

	private EcommerceOrder ecommerceOrder;
	private String orderCharges;
	private String chargesCode;
	private String chargesValue;

	public OrderCharges() {
	}

	public OrderCharges(String id, EcommerceOrder ecommerceOrder,
			String orderCharges, String chargesCode, String chargesValue,
			LocalDateTime createdDate, String userCreated) {
		this.id = id;
		this.ecommerceOrder = ecommerceOrder;
		this.orderCharges = orderCharges;
		this.chargesCode = chargesCode;
		this.chargesValue = chargesValue;
		this.createdDate = createdDate;
		this.userCreated = userCreated;
	}

	public OrderCharges(String id, EcommerceOrder ecommerceOrder,
			String orderCharges, String chargesCode, String chargesValue,
			LocalDateTime createdDate, LocalDateTime modifiedDate,
			String userCreated, String userModified) {
		this.id = id;
		this.ecommerceOrder = ecommerceOrder;
		this.orderCharges = orderCharges;
		this.chargesCode = chargesCode;
		this.chargesValue = chargesValue;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.userCreated = userCreated;
		this.userModified = userModified;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "e_order_id", nullable = false)
	public EcommerceOrder getEcommerceOrder() {
		return this.ecommerceOrder;
	}

	public void setEcommerceOrder(EcommerceOrder ecommerceOrder) {
		this.ecommerceOrder = ecommerceOrder;
	}

	@Column(name = "order_charges", nullable = false, length = 45)
	public String getOrderCharges() {
		return this.orderCharges;
	}

	public void setOrderCharges(String orderCharges) {
		this.orderCharges = orderCharges;
	}

	@Column(name = "charges_code", nullable = false, length = 45)
	public String getChargesCode() {
		return this.chargesCode;
	}

	public void setChargesCode(String chargesCode) {
		this.chargesCode = chargesCode;
	}

	@Column(name = "charges_value", nullable = false, length = 45)
	public String getChargesValue() {
		return this.chargesValue;
	}

	public void setChargesValue(String chargesValue) {
		this.chargesValue = chargesValue;
	}

}
