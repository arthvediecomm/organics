package com.arthvedi.organic.order.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.order.domain.OrderRefund;
/*
*@Author varma
*/
public interface OrderRefundRepository extends PagingAndSortingRepository<OrderRefund, Serializable>{

}
