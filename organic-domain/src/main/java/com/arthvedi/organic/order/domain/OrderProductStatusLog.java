package com.arthvedi.organic.order.domain;

// Generated Feb 17, 2017 7:05:49 PM by Hibernate Tools 3.6.0.Final

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.joda.time.LocalDateTime;

import com.arthvedi.core.domain.AbstractDomain;

/**
 * OrderProductStatusLog generated by hbm2java
 */
@Entity
@Table(name = "order_product_status_log", catalog = "organic")
public class OrderProductStatusLog extends AbstractDomain implements
		java.io.Serializable {

	private OrderProduct orderProduct;
	private String orderProductStatusCode;
	private Date orderProductStatusDate;
	private String orderProductStatusDescription;

	public OrderProductStatusLog() {
	}

	public OrderProductStatusLog(String id, OrderProduct orderProduct,
			String orderProductStatusCode, Date orderProductStatusDate,
			String orderProductStatusDescription, LocalDateTime createdDate,
			String userCreated, String userModified) {
		this.id = id;
		this.orderProduct = orderProduct;
		this.orderProductStatusCode = orderProductStatusCode;
		this.orderProductStatusDate = orderProductStatusDate;
		this.orderProductStatusDescription = orderProductStatusDescription;
		this.createdDate = createdDate;
		this.userCreated = userCreated;
		this.userModified = userModified;
	}

	public OrderProductStatusLog(String id, OrderProduct orderProduct,
			String orderProductStatusCode, Date orderProductStatusDate,
			String orderProductStatusDescription, LocalDateTime createdDate,
			LocalDateTime modifiedDate, String userCreated, String userModified) {
		this.id = id;
		this.orderProduct = orderProduct;
		this.orderProductStatusCode = orderProductStatusCode;
		this.orderProductStatusDate = orderProductStatusDate;
		this.orderProductStatusDescription = orderProductStatusDescription;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.userCreated = userCreated;
		this.userModified = userModified;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "order_product_id", nullable = false)
	public OrderProduct getOrderProduct() {
		return this.orderProduct;
	}

	public void setOrderProduct(OrderProduct orderProduct) {
		this.orderProduct = orderProduct;
	}

	@Column(name = "order_product_status_code", nullable = false, length = 45)
	public String getOrderProductStatusCode() {
		return this.orderProductStatusCode;
	}

	public void setOrderProductStatusCode(String orderProductStatusCode) {
		this.orderProductStatusCode = orderProductStatusCode;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "order_product_status_date", nullable = false, length = 19)
	public Date getOrderProductStatusDate() {
		return this.orderProductStatusDate;
	}

	public void setOrderProductStatusDate(Date orderProductStatusDate) {
		this.orderProductStatusDate = orderProductStatusDate;
	}

	@Column(name = "order_product_status_description", nullable = false, length = 45)
	public String getOrderProductStatusDescription() {
		return this.orderProductStatusDescription;
	}

	public void setOrderProductStatusDescription(
			String orderProductStatusDescription) {
		this.orderProductStatusDescription = orderProductStatusDescription;
	}

}
