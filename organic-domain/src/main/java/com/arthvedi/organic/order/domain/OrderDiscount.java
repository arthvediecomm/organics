package com.arthvedi.organic.order.domain;

// Generated Feb 17, 2017 7:05:49 PM by Hibernate Tools 3.6.0.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.joda.time.LocalDateTime;

import com.arthvedi.core.domain.AbstractDomain;

/**
 * OrderDiscount generated by hbm2java
 */
@Entity
@Table(name = "order_discount", catalog = "organic")
public class OrderDiscount extends AbstractDomain implements
		java.io.Serializable {

	private EcommerceOrder ecommerceOrder;
	private String discountId;

	public OrderDiscount() {
	}

	public OrderDiscount(String id, EcommerceOrder ecommerceOrder,
			LocalDateTime createdDate, String userCreated) {
		this.id = id;
		this.ecommerceOrder = ecommerceOrder;
		this.createdDate = createdDate;
		this.userCreated = userCreated;
	}

	public OrderDiscount(String id, EcommerceOrder ecommerceOrder,
			String discountId, LocalDateTime createdDate,
			LocalDateTime modifiedDate, String userCreated, String userModified) {
		this.id = id;
		this.ecommerceOrder = ecommerceOrder;
		this.discountId = discountId;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.userCreated = userCreated;
		this.userModified = userModified;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "order_id", nullable = false)
	public EcommerceOrder getEcommerceOrder() {
		return this.ecommerceOrder;
	}

	public void setEcommerceOrder(EcommerceOrder ecommerceOrder) {
		this.ecommerceOrder = ecommerceOrder;
	}

	@Column(name = "discount_id", length = 100)
	public String getDiscountId() {
		return this.discountId;
	}

	public void setDiscountId(String discountId) {
		this.discountId = discountId;
	}

}
