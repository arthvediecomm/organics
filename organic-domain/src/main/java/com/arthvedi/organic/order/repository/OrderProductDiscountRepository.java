package com.arthvedi.organic.order.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.order.domain.OrderProductDiscount;
/*
*@Author varma
*/
public interface OrderProductDiscountRepository extends PagingAndSortingRepository<OrderProductDiscount, Serializable>{

}
