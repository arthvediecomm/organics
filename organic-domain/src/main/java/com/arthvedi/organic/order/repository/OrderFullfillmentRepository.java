package com.arthvedi.organic.order.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.order.domain.OrderFullfillment;
/*
*@Author varma
*/
public interface OrderFullfillmentRepository extends PagingAndSortingRepository<OrderFullfillment, Serializable>,JpaSpecificationExecutor<OrderFullfillment>{

}
