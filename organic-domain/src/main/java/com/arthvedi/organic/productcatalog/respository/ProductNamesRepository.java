package com.arthvedi.organic.productcatalog.respository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.productcatalog.domain.ProductNames;
/*
*@Author varma
*/
public interface ProductNamesRepository extends JpaSpecificationExecutor<ProductNames>, PagingAndSortingRepository<ProductNames, Serializable>{

}
