package com.arthvedi.organic.productcatalog.respository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.productcatalog.domain.CategoryNames;
/*
*@Author varma
*/
public interface CategoryNamesRepository extends PagingAndSortingRepository<CategoryNames, Serializable>{

}
