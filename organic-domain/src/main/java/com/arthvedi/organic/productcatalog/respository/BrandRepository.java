package com.arthvedi.organic.productcatalog.respository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.productcatalog.domain.Brand;

/*
 *@Author varma
 */
public interface BrandRepository extends
		PagingAndSortingRepository<Brand, Serializable> {

	@Query("SELECT DISTINCT b FROM Brand b WHERE b.name=?1")
	Brand findByBrandName(String name);

}
