package com.arthvedi.organic.productcatalog.respository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.productcatalog.domain.BrandImages;
/*
*@Author varma
*/
public interface BrandImagesRepository extends PagingAndSortingRepository<BrandImages, Serializable>{

}
