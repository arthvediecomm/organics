package com.arthvedi.organic.productcatalog.respository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.productcatalog.domain.SellerProductPrices;
/*
*@Author varma
*/
public interface SellerProductPricesRepository extends PagingAndSortingRepository<SellerProductPrices, Serializable>{

}
