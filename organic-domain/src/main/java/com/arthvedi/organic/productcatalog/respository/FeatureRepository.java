package com.arthvedi.organic.productcatalog.respository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.productcatalog.domain.Feature;

/*
 *@Author varma
 */
public interface FeatureRepository extends JpaSpecificationExecutor<Feature>,
		PagingAndSortingRepository<Feature, Serializable> {

}
