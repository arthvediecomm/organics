package com.arthvedi.organic.productcatalog.respository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.productcatalog.domain.CategoryImages;
/*
*@Author varma
*/
public interface CategoryImagesRepository extends PagingAndSortingRepository<CategoryImages, Serializable>{

}
