package com.arthvedi.organic.productcatalog.respository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.productcatalog.domain.Seo;
/*
*@Author varma
*/
public interface SeoRepository extends PagingAndSortingRepository<Seo, Serializable>{

}
