package com.arthvedi.organic.productcatalog.domain;

// Generated Jan 19, 2017 6:54:25 PM by Hibernate Tools 3.6.0.Final

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.joda.time.LocalDateTime;
import org.springframework.stereotype.Component;

import com.arthvedi.core.domain.AbstractDomain;
import com.arthvedi.organic.commons.domain.ReturnPolicy;
import com.arthvedi.organic.seller.domain.SellerBranch;

/**
 * SellerProduct generated by hbm2java
 */
@Entity
@Component
@Table(name = "seller_product", catalog = "organic")
public class SellerProduct extends AbstractDomain implements
		java.io.Serializable {
	private BigDecimal offerPrice;
	private ReturnPolicy returnPolicy;
	private Product product;
	private BigDecimal sellerPrice;
	private SellerBranch sellerBranch;
	private String status;
	private String description;
	private String minQuantity;
	private String maxQuantity;
	private String sellerStock;
	private BigDecimal sellingPrice;
	private String stockableStatus;
	private String baseUnit;
	private String measurementUnit;
	private String quantity;
	private String sellerProductName;
	private BigDecimal productMrp;

	private Set<SellerProductPrices> sellerProductPriceses = new HashSet<SellerProductPrices>(
			0);

	public SellerProduct() {
	}

	public SellerProduct(String id, ReturnPolicy returnPolicy, Product product,
			SellerBranch sellerBranch, String status, String description,
			BigDecimal offerPrice, String minQuantity, String maxQuantity,
			String sellerStock, BigDecimal sellingPrice,
			String stockableStatus, String baseUnit, String measurementUnit,
			String quantity, String userCreated, LocalDateTime createdDate,
			String sellerProductName, BigDecimal productMrp) {
		this.id = id;
		this.returnPolicy = returnPolicy;
		this.product = product;
		this.sellerBranch = sellerBranch;
		this.status = status;
		this.description = description;
		this.offerPrice = offerPrice;
		this.minQuantity = minQuantity;
		this.maxQuantity = maxQuantity;
		this.sellerStock = sellerStock;
		this.stockableStatus = stockableStatus;
		this.baseUnit = baseUnit;
		this.sellingPrice = sellingPrice;
		this.measurementUnit = measurementUnit;
		this.quantity = quantity;
		this.userCreated = userCreated;
		this.createdDate = createdDate;
		this.sellerProductName = sellerProductName;
		this.productMrp = productMrp;
	}

	public SellerProduct(String id, ReturnPolicy returnPolicy, Product product,
			SellerBranch sellerBranch, String status, String description,
			String minQuantity, String maxQuantity, String sellerStock,
			String stockableStatus, String baseUnit, String measurementUnit,
			String quantity, String userCreated, String userModified,
			LocalDateTime createdDate, LocalDateTime modifiedDate,
			String sellerProductName, BigDecimal productMrp,
			BigDecimal offerPrice, BigDecimal sellingPrice,
			Set<SellerProductPrices> sellerProductPriceses) {
		this.id = id;
		this.returnPolicy = returnPolicy;
		this.product = product;
		this.sellerBranch = sellerBranch;
		this.status = status;
		this.description = description;
		this.minQuantity = minQuantity;
		this.maxQuantity = maxQuantity;
		this.sellerStock = sellerStock;
		this.stockableStatus = stockableStatus;
		this.baseUnit = baseUnit;
		this.offerPrice = offerPrice;
		this.measurementUnit = measurementUnit;
		this.quantity = quantity;
		this.userCreated = userCreated;
		this.userModified = userModified;
		this.createdDate = createdDate;
		this.sellingPrice = sellingPrice;
		this.modifiedDate = modifiedDate;
		this.sellerProductName = sellerProductName;
		this.productMrp = productMrp;
		this.sellerProductPriceses = sellerProductPriceses;
	}

	@Column(name = "offer_price", nullable = false, precision = 7)
	public BigDecimal getOfferPrice() {
		return offerPrice;
	}

	public void setOfferPrice(BigDecimal offerPrice) {
		this.offerPrice = offerPrice;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "return_policy_id", nullable = false)
	public ReturnPolicy getReturnPolicy() {
		return this.returnPolicy;
	}

	public void setReturnPolicy(ReturnPolicy returnPolicy) {
		this.returnPolicy = returnPolicy;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "product_id", nullable = false)
	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "seller_branch_id", nullable = false)
	public SellerBranch getSellerBranch() {
		return this.sellerBranch;
	}

	public void setSellerBranch(SellerBranch sellerBranch) {
		this.sellerBranch = sellerBranch;
	}

	@Column(name = "status", nullable = false, length = 45)
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "selling_price", nullable = false, length = 45)
	public BigDecimal getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(BigDecimal sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	@Column(name = "description", nullable = false, length = 500)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "min_quantity", nullable = false, length = 45)
	public String getMinQuantity() {
		return this.minQuantity;
	}

	public void setMinQuantity(String minQuantity) {
		this.minQuantity = minQuantity;
	}

	@Column(name = "max_quantity", nullable = false, length = 45)
	public String getMaxQuantity() {
		return this.maxQuantity;
	}

	public void setMaxQuantity(String maxQuantity) {
		this.maxQuantity = maxQuantity;
	}

	@Column(name = "seller_stock", nullable = false, length = 45)
	public String getSellerStock() {
		return this.sellerStock;
	}

	public void setSellerStock(String sellerStock) {
		this.sellerStock = sellerStock;
	}

	@Column(name = "stockable_status", nullable = false, length = 45)
	public String getStockableStatus() {
		return this.stockableStatus;
	}

	public void setStockableStatus(String stockableStatus) {
		this.stockableStatus = stockableStatus;
	}

	@Column(name = "base_unit", nullable = false, length = 45)
	public String getBaseUnit() {
		return this.baseUnit;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	@Column(name = "measurement_unit", nullable = false, length = 45)
	public String getMeasurementUnit() {
		return this.measurementUnit;
	}

	public void setMeasurementUnit(String measurementUnit) {
		this.measurementUnit = measurementUnit;
	}

	@Column(name = "quantity", nullable = false, length = 45)
	public String getQuantity() {
		return this.quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	@Column(name = "seller_product_name", nullable = false, length = 45)
	public String getSellerProductName() {
		return this.sellerProductName;
	}

	public void setSellerProductName(String sellerProductName) {
		this.sellerProductName = sellerProductName;
	}

	@Column(name = "product_mrp", nullable = false, precision = 7)
	public BigDecimal getProductMrp() {
		return this.productMrp;
	}

	public void setProductMrp(BigDecimal productMrp) {
		this.productMrp = productMrp;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "sellerProduct")
	public Set<SellerProductPrices> getSellerProductPriceses() {
		return this.sellerProductPriceses;
	}

	public void setSellerProductPriceses(
			Set<SellerProductPrices> sellerProductPriceses) {
		this.sellerProductPriceses = sellerProductPriceses;
	}

	@Column(name = "seller_price", nullable = false, precision = 7)
	public BigDecimal getSellerPrice() {
		return sellerPrice;
	}

	public void setSellerPrice(BigDecimal sellerPrice) {
		this.sellerPrice = sellerPrice;
	}

}
