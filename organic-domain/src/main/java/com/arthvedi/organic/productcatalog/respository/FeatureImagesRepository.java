package com.arthvedi.organic.productcatalog.respository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.productcatalog.domain.FeatureImages;
/*
*@Author varma
*/
public interface FeatureImagesRepository extends PagingAndSortingRepository<FeatureImages, Serializable>{

}
