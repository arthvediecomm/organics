package com.arthvedi.organic.productcatalog.respository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.productcatalog.domain.SellerProductFeature;

/*
 *@Author varma
 */
public interface SellerProductFeatureRepository extends
		PagingAndSortingRepository<SellerProductFeature, Serializable>,
		JpaSpecificationExecutor<SellerProductFeature> {

}
