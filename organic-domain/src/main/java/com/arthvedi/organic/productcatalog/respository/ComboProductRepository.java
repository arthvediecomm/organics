package com.arthvedi.organic.productcatalog.respository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.productcatalog.domain.ComboProduct;
/*
*@Author varma
*/
public interface ComboProductRepository extends PagingAndSortingRepository<ComboProduct, Serializable>{

}
