package com.arthvedi.organic.productcatalog.respository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.productcatalog.domain.Category;

/*
 *@Author varma
 */
public interface CategoryRepository extends
		PagingAndSortingRepository<Category, Serializable> {
	@Query("SELECT DISTINCT c FROM Category c WHERE c.categoryName =?1")
	Category findByCategoryName(String categoryName);

}
