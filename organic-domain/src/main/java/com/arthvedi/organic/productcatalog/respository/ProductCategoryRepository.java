package com.arthvedi.organic.productcatalog.respository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.productcatalog.domain.ProductCategory;
/*
*@Author varma
*/
public interface ProductCategoryRepository extends PagingAndSortingRepository<ProductCategory, Serializable>{

}
