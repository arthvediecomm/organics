package com.arthvedi.organic.productcatalog.domain;

// Generated Jan 19, 2017 6:54:25 PM by Hibernate Tools 3.6.0.Final

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.joda.time.LocalDateTime;
import org.springframework.stereotype.Component;

import com.arthvedi.core.domain.AbstractDomain;

/**
 * Seo generated by hbm2java
 */
@Entity
@Component
@Table(name = "seo", catalog = "organic")
public class Seo extends AbstractDomain implements java.io.Serializable {

	private String seoTitle;
	private String seoKeyword;
	private String seoDescription;

	private Set<ProductNames> productNameses = new HashSet<ProductNames>(0);
	private Set<CategoryNames> categoryNameses = new HashSet<CategoryNames>(0);

	public Seo() {
	}

	public Seo(String id, String seoTitle, String seoKeyword,
			String seoDescription, LocalDateTime createdDate, String userCreated) {
		this.id = id;
		this.seoTitle = seoTitle;
		this.seoKeyword = seoKeyword;
		this.seoDescription = seoDescription;
		this.createdDate = createdDate;
		this.userCreated = userCreated;
	}

	public Seo(String id, String seoTitle, String seoKeywords,
			String seoDescription, LocalDateTime createdDate,
			LocalDateTime modifiedDate, String userCreated,
			String userModified, Set<ProductNames> productNameses,
			Set<CategoryNames> categoryNameses, String seoKeyword) {
		this.id = id;
		this.seoTitle = seoTitle;
		this.seoKeyword = seoKeyword;
		this.seoDescription = seoDescription;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.userCreated = userCreated;
		this.userModified = userModified;
		this.productNameses = productNameses;
		this.categoryNameses = categoryNameses;
	}

	@Column(name = "seo_title", nullable = false, length = 45)
	public String getSeoTitle() {
		return this.seoTitle;
	}

	public void setSeoTitle(String seoTitle) {
		this.seoTitle = seoTitle;
	}

	@Column(name = "seo_keyword", nullable = false, length = 45)
	public String getSeoKeyword() {
		return this.seoKeyword;
	}

	public void setSeoKeyword(String seoKeyword) {
		this.seoKeyword = seoKeyword;
	}

	@Column(name = "seo_description", nullable = false, length = 500)
	public String getSeoDescription() {
		return this.seoDescription;
	}

	public void setSeoDescription(String seoDescription) {
		this.seoDescription = seoDescription;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "seo")
	public Set<ProductNames> getProductNameses() {
		return this.productNameses;
	}

	public void setProductNameses(Set<ProductNames> productNameses) {
		this.productNameses = productNameses;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "seo")
	public Set<CategoryNames> getCategoryNameses() {
		return this.categoryNameses;
	}

	public void setCategoryNameses(Set<CategoryNames> categoryNameses) {
		this.categoryNameses = categoryNameses;
	}

}
