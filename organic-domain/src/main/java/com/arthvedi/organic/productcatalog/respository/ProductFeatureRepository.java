package com.arthvedi.organic.productcatalog.respository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.productcatalog.domain.ProductFeature;
/*
*@Author varma
*/
public interface ProductFeatureRepository extends PagingAndSortingRepository<ProductFeature, Serializable>{

}
