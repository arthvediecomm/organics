package com.arthvedi.organic.productcatalog.domain;

// Generated Feb 12, 2017 12:57:13 PM by Hibernate Tools 4.3.1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.joda.time.LocalDateTime;
import org.springframework.stereotype.Component;

import com.arthvedi.core.domain.AbstractDomain;

/**
 * ProductImages generated by hbm2java
 */
@Entity
@Component
@Table(name = "product_images", catalog = "organic")
public class ProductImages extends AbstractDomain implements
		java.io.Serializable {

	private Product product;
	private String imageName;
	private String imageFolder;
	private String imageType;

	public ProductImages() {
	}

	public ProductImages(String id, Product product, String imageName,
			String imageFolder, String imageType, LocalDateTime createdDate,
			String userCreated) {
		this.id = id;
		this.product = product;
		this.imageName = imageName;
		this.imageFolder = imageFolder;
		this.imageType = imageType;
		this.createdDate = createdDate;
		this.userCreated = userCreated;
	}

	public ProductImages(String id, Product product, String imageName,
			String imageFolder, String imageType, LocalDateTime createdDate,
			LocalDateTime modifiedDate, String userCreated, String userModified) {
		this.id = id;
		this.product = product;
		this.imageName = imageName;
		this.imageFolder = imageFolder;
		this.imageType = imageType;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.userCreated = userCreated;
		this.userModified = userModified;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "product_id", nullable = false)
	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Column(name = "image_name", nullable = false, length = 45)
	public String getImageName() {
		return this.imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	@Column(name = "image_folder", nullable = false, length = 45)
	public String getImageFolder() {
		return this.imageFolder;
	}

	public void setImageFolder(String imageFolder) {
		this.imageFolder = imageFolder;
	}

	@Column(name = "image_type", nullable = false, length = 45)
	public String getImageType() {
		return this.imageType;
	}

	public void setImageType(String imageType) {
		this.imageType = imageType;
	}

}
