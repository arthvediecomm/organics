package com.arthvedi.organic.productcatalog.respository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.productcatalog.domain.ProductImages;
/*
*@Author varma
*/
public interface ProductImagesRepository extends PagingAndSortingRepository<ProductImages, Serializable>{

}
