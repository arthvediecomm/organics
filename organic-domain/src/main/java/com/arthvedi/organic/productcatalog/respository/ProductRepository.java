package com.arthvedi.organic.productcatalog.respository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.productcatalog.domain.Product;
/*
*@Author varma
*/
public interface ProductRepository extends JpaSpecificationExecutor<Product>, PagingAndSortingRepository<Product, Serializable>{
	@Query("SELECT DISTINCT c FROM Product c WHERE c.productName =?1")
	Product findProductNameExist(String productName);


}
