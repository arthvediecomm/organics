package com.arthvedi.organic.requests.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.requests.domain.RequestAttributes;

/*
*@Author varma
*/
public interface RequestAttributesRepository extends  PagingAndSortingRepository<RequestAttributes, Serializable>{

}
