package com.arthvedi.organic.requests.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.requests.domain.Request;
/*
*@Author varma
*/
public interface RequestRepository extends PagingAndSortingRepository<Request, Serializable>{

}
