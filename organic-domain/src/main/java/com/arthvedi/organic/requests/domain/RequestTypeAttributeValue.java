package com.arthvedi.organic.requests.domain;

// Generated Jan 26, 2017 7:51:08 PM by Hibernate Tools 5.1.0.Alpha1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.joda.time.LocalDateTime;
import org.springframework.stereotype.Component;

import com.arthvedi.core.domain.AbstractDomain;

/**
 * RequestTypeAttributeValue generated by hbm2java
 */
@Entity
@Component
@Table(name = "request_type_attribute_value", catalog = "new_handcraft_final")
public class RequestTypeAttributeValue extends AbstractDomain implements
		java.io.Serializable {

	private RequestTypeAttribute requestTypeAttribute;
	private String requestTypeAttributeValue;
	private String status;

	public RequestTypeAttributeValue() {
	}

	public RequestTypeAttributeValue(String id,
			RequestTypeAttribute requestTypeAttribute,
			String requestTypeAttributeValue, String status,
			LocalDateTime createdDate) {
		this.id = id;
		this.requestTypeAttribute = requestTypeAttribute;
		this.requestTypeAttributeValue = requestTypeAttributeValue;
		this.status = status;
		this.createdDate = createdDate;
	}

	public RequestTypeAttributeValue(String id,
			RequestTypeAttribute requestTypeAttribute,
			String requestTypeAttributeValue, String status,
			LocalDateTime createdDate, LocalDateTime modifiedDate,
			String userCreated, String userModified) {
		this.id = id;
		this.requestTypeAttribute = requestTypeAttribute;
		this.requestTypeAttributeValue = requestTypeAttributeValue;
		this.status = status;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.userCreated = userCreated;
		this.userModified = userModified;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "request_type_attribute_id", nullable = false)
	public RequestTypeAttribute getRequestTypeAttribute() {
		return this.requestTypeAttribute;
	}

	public void setRequestTypeAttribute(
			RequestTypeAttribute requestTypeAttribute) {
		this.requestTypeAttribute = requestTypeAttribute;
	}

	@Column(name = "request_type_attribute_value", nullable = false, length = 45)
	public String getRequestTypeAttributeValue() {
		return this.requestTypeAttributeValue;
	}

	public void setRequestTypeAttributeValue(String requestTypeAttributeValue) {
		this.requestTypeAttributeValue = requestTypeAttributeValue;
	}

	@Column(name = "status", nullable = false, length = 45)
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
