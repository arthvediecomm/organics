package com.arthvedi.organic.requests.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.requests.domain.RequestType;
/*
*@Author varma
*/
public interface RequestTypeRepository extends PagingAndSortingRepository<RequestType, Serializable>{

}
