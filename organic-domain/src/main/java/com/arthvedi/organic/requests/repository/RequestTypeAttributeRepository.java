package com.arthvedi.organic.requests.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.requests.domain.RequestTypeAttribute;
/*
*@Author varma
*/
public interface RequestTypeAttributeRepository extends PagingAndSortingRepository<RequestTypeAttribute, Serializable>{

}
