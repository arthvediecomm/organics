package com.arthvedi.organic.requests.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.requests.domain.RequestStatusCodes;
/*
*@Author varma
*/
public interface RequestStatusCodesRepository extends PagingAndSortingRepository<RequestStatusCodes, Serializable>{

}
