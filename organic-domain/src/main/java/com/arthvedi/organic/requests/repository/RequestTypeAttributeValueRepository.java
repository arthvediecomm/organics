package com.arthvedi.organic.requests.repository;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.arthvedi.organic.requests.domain.RequestTypeAttributeValue;
/*
*@Author varma
*/
public interface RequestTypeAttributeValueRepository extends PagingAndSortingRepository<RequestTypeAttributeValue, Serializable>{

}
